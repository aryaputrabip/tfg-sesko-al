using System.Collections;
using System.Collections.Generic;
using GlobalVariables;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AreaCalculationController : MonoBehaviour
{
    #region INSTANCE
    public static AreaCalculationController instance;
    #endregion

    public double lineMult, polyMult;
    public TMP_Text lineText, polyText;
    public GameObject prefabArea; 

    //Drawing Tools
    private float dis;
    private Vector2 pos;
    private List<Vector2> coords;
    private int n;
    private OnlineMapsDrawingLine aLine;
    private OnlineMapsDrawingPoly aPoly;
    private List<OnlineMapsMarker> lineMarkers;
    private OnlineMapsMarker polyMarker;
    private List<OnlineMapsDrawingLine> listLine;
    private List<OnlineMapsDrawingPoly> listPoly;
    private string type;
    private OnlineMapsDrawingPoly activePoly;

    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
            n = 0;
            listLine = new List<OnlineMapsDrawingLine>();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    // KM, M, NM
    public void setUnitLine()
    {
        switch (lineText.text)
        {
            case "KM":
                lineMult = 1000;
                lineText.text = "M";
                break;

            case "M":
                lineMult = 0.539957;
                lineText.text = "NM";
                break;

            case "NM":
                lineMult = 1;
                lineText.text = "KM";
                break;
        }
    }

    // KM2, M2, HA
    public void setUnitPoly()
    {
        switch (polyText.text)
        {
            case "KM2":
                polyMult = 1000000;
                polyText.text = "M2";
                break;

            case "M2":
                polyMult = 100;
                polyText.text = "HA";
                break;

            case "HA":
                polyMult = 1;
                polyText.text = "KM2";
                break;
        }
    }

    public void performDrawLine()
    {
        if (type == null && type != "polygon")
        {
            coords = new List<Vector2>();
            OnlineMapsControlBase.instance.OnMapClick += addPolyLine;
            // DrawingManager.Instance.isDeleting = false;
            GameObject.Find("Modal - Confirm Draw").GetComponent<Animator>().Play("FadeIn");
            type = "polyline";
        }
    }

    private void addPolyLine()
    {
        double lng, lat;
        OnlineMapsControlBase.instance.GetCoords(out lng, out lat);
        coords.Add(new Vector2((float)lng, (float)lat));
        if (n < 1)
        {
            var man = OnlineMapsDrawingElementManager.AddItem(new OnlineMapsDrawingLine(coords, (SessionUser.nama_asisten == "ASOPS") ? Color.blue : Color.red, 0.25f));
            aLine = (OnlineMapsDrawingLine)man;
            pos = new Vector2((float)lng, (float)lat);
            lineMarkers = new List<OnlineMapsMarker>();
            aLine["markers"] = lineMarkers;
            dis = 0;
        }
        else
        {
            dis = OnlineMapsUtils.DistanceBetweenPoints(pos, new Vector2((float)lng, (float)lat)).magnitude * (float) lineMult;
            pos = new Vector2((float)lng, (float)lat);
        }
        OnlineMapsMarker mm = OnlineMapsMarkerManager.CreateItem(pos.x, pos.y, OnlineMapsMarkerManager.instance.defaultTexture, "Jarak: " + dis + " " + lineText.text);
        lineMarkers.Add(mm);
        mm["line"] = aLine;
        mm["index"] = n;
        mm.OnPress += MarkerLinePress;
        n++;
    }

    public void clearMarkers(OnlineMapsDrawingLine ln)
    {
        var markers = (List<OnlineMapsMarker>)ln["markers"];
        foreach (OnlineMapsMarker m in markers)
        {
            OnlineMapsMarkerManagerBase<OnlineMapsMarkerManager, OnlineMapsMarker>.RemoveItem(m, true);
            m.Dispose();
        }
        markers.Clear();
    }

    private void spawnMarkers(OnlineMapsDrawingLine ln)
    {
        int i = 0;
        var markers = new List<OnlineMapsMarker>();
        var pos = new Vector2();
        float dis = 0;
        var ls = (List<Vector2>)ln.points;
        foreach (Vector2 pts in ln.points as List<Vector2>)
        {
            if (i > 0)
            {
                dis = OnlineMapsUtils.DistanceBetweenPoints(pos, new Vector2(pts.x, pts.y)).magnitude * (float)lineMult;
            }
            pos = new Vector2(pts.x, pts.y);
            OnlineMapsMarker mm = OnlineMapsMarkerManager.CreateItem(pts.x, pts.y, OnlineMapsMarkerManager.instance.defaultTexture, "Jarak: " + dis + " " + lineText.text);
            markers.Add(mm);
            mm["line"] = ln;
            mm["index"] = i;
            mm.OnPress += MarkerLinePress;
            i++;
        }
        ln["markers"] = markers;
    }

    private void MarkerLinePress(OnlineMapsMarkerBase marker)
    {
        var line = (OnlineMapsDrawingLine)marker["line"];
        var ls = (List<Vector2>)line.points;
        ls.RemoveAt((int)marker["index"]);
        if (ls.Count > 1)
        {
            clearMarkers(line);
            spawnMarkers(line);
        } else
        {
            clearMarkers(line);
            line.Dispose();
        }
    }

    // For Polygon

    public void performDrawPoly()
    {
        if(type == null && type != "polyline")
        {
            coords = new List<Vector2>();
            OnlineMapsControlBase.instance.OnMapClick += addPolygon;
            // DrawingManager.Instance.isDeleting = false;
            GameObject.Find("Modal - Confirm Draw").GetComponent<Animator>().Play("FadeIn");
            type = "polygon";
        }
    }

    private void addPolygon()
    {
        double lng, lat;
        OnlineMapsControlBase.instance.GetCoords(out lng, out lat);
        coords.Add(new Vector2((float)lng, (float)lat));
        if (n < 1)
        {
            Color clr = (SessionUser.nama_asisten == "ASOPS") ? Color.blue : Color.red;
            clr.a = 0.1f;
            var man = OnlineMapsDrawingElementManager.AddItem(new OnlineMapsDrawingPoly(coords, Color.white, 0.25f, clr));
            aPoly = (OnlineMapsDrawingPoly)man;
            pos = new Vector2((float)lng, (float)lat);
            //polyMarker = OnlineMapsMarker;
            //aPoly["markers"] = polyMarker;
            //dis = 0;
        }
        else
        {
            //dis = OnlineMapsUtils.DistanceBetweenPoints(pos, new Vector2((float)lng, (float)lat)).magnitude * (float)lineMult;
            pos = new Vector2((float)lng, (float)lat);
        }
        //OnlineMapsMarker mm = OnlineMapsMarkerManager.CreateItem(pos.x, pos.y, OnlineMapsMarkerManager.instance.defaultTexture, "Jarak: " + dis + " " + lineText.text);
        //lineMarkers.Add(mm);
        //mm["line"] = aPoly;
        //mm["index"] = n;
        //mm.OnPress += MarkerPolyPress;
        n++;
    }

    private void spawn3DMarker(OnlineMapsDrawingPoly op)
    {
        OnlineMapsMarker3D marker = OnlineMapsMarker3DManager.CreateItem(op.center, prefabArea);
        Metadata met = marker.instance.GetComponent<Metadata>();
        met.FindParameter("polygon").parameter = marker.instance;
        met.FindParameter("info").parameter.GetComponent<TMP_Text>().text += calculateArea((List<Vector2>)op.points, 1) + " KM2" + System.Environment.NewLine;
        met.FindParameter("info").parameter.GetComponent<TMP_Text>().text += calculateArea((List<Vector2>)op.points, 1000000) + " M2" + System.Environment.NewLine;
        met.FindParameter("info").parameter.GetComponent<TMP_Text>().text += calculateArea((List<Vector2>)op.points, 100) + " HA";

    }

    private double calculateArea(List<Vector2> coords, double mult)
    {
        // Calculates area of ??the polygon.
        // Important: this algorithm works correctly only if the lines do not intersect.
        double area = 0;

        // Triangulate points.
        int[] indexes = OnlineMapsUtils.Triangulate(coords).ToArray();

        // Calculate the area of each triangle.
        for (int i = 0; i < indexes.Length / 3; i++)
        {
            // Get the points of the triangle.
            Vector2 p1 = coords[indexes[i * 3]];
            Vector2 p2 = coords[indexes[i * 3 + 1]];
            Vector2 p3 = coords[indexes[i * 3 + 2]];

            // Calculate the distance between points.
            float d1 = OnlineMapsUtils.DistanceBetweenPoints(p1, p2).magnitude;
            float d2 = OnlineMapsUtils.DistanceBetweenPoints(p2, p3).magnitude;
            float d3 = OnlineMapsUtils.DistanceBetweenPoints(p3, p1).magnitude;

            // Calculate the area.
            float p = (d1 + d2 + d3) / 2;
            area += (double) Mathf.Sqrt(p * (p - d1) * (p - d2) * (p - d3));
        }

        return (area * mult);
    }

    public void confirmDraw()
    {
        OnlineMaps.instance.Redraw();
        OnlineMapsControlBase.instance.OnMapClick = null;
        GameObject.Find("Modal - Confirm Draw").GetComponent<Animator>().Play("FadeOut");
        if (n > 0)
        {
            if(type == "polygon")
            {
                var dis =  calculateArea((List<Vector2>)aPoly.points, polyMult);
                aPoly.OnClick += onPolyClick;
                //spawn3DMarker(aPoly);
                //aPoly["marker"] = mm;
                //mm["poly"] = aPoly;
                //mm["index"] = n;
                //mm.OnPress += MarkerPolyPress;
            }
            n = 0;
            dis = 0;
            type = null;
        }
    }

    private void onPolyClick(OnlineMapsDrawingElement pol)
    {
        activePoly = (OnlineMapsDrawingPoly) pol;
        prefabArea.GetComponent<Animator>().Play("FadeIn");
        Metadata met = prefabArea.GetComponent<Metadata>();
        //met.FindParameter("polygon").parameter = activePoly.instance;
        met.FindParameter("info").parameter.GetComponent<TMP_Text>().text = calculateArea((List<Vector2>)activePoly.points, 1).ToString() + " KM2" + System.Environment.NewLine;
        met.FindParameter("info").parameter.GetComponent<TMP_Text>().text += calculateArea((List<Vector2>)activePoly.points, 1000000).ToString() + " M2" + System.Environment.NewLine;
        met.FindParameter("info").parameter.GetComponent<TMP_Text>().text += calculateArea((List<Vector2>)activePoly.points, 100).ToString() + " HA";
        //met.FindParameter("btnCenter").parameter.GetComponent<Button>().onClick = null;
        //met.FindParameter("btnCenter").parameter.GetComponent<Button>().onClick.AddListener(this.centerPoly);
        //met.FindParameter("btnDelete").parameter.GetComponent<Button>().onClick = null;
        //met.FindParameter("btnDelete").parameter.GetComponent<Button>().onClick.AddListener(this.deletePoly);

    }

    public void centerPoly()
    {
        OnlineMaps.instance.position = activePoly.center;
        prefabArea.GetComponent<Animator>().Play("FadeOut");
    }

    public void deletePoly()
    {
        activePoly.Dispose();
        prefabArea.GetComponent<Animator>().Play("FadeOut");
    }

    private void MarkerPolyPress(OnlineMapsMarkerBase marker)
    {
        var poly = (OnlineMapsDrawingPoly)marker["poly"];
        var ls = (List<Vector2>)poly.points;
        poly.Dispose();
        OnlineMapsMarkerManagerBase<OnlineMapsMarkerManager, OnlineMapsMarker>.RemoveItem((OnlineMapsMarker) aPoly["marker"], true);
        marker.Dispose();
    }
}