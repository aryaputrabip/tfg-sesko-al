using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System;

[Serializable]
public class ConnectionString
{
    [JsonIgnore] [SerializeField] string _server;
    [JsonIgnore] [SerializeField] string _port;
    [JsonIgnore] [SerializeField] string _database;
    [JsonIgnore] [SerializeField] string _userID;
    [JsonIgnore] [SerializeField] string _password;
    public string server { get { return _server; } set { _server = value; } }
    public string port { get { return _port; } set { _port = value; } }
    public string database { get { return _database; } set { _database = value; } }
    public string userID { get { return _userID; } set { _userID = value; } }
    public string password { get { return _password; } set { _password = value; } }

    ///<summary>
    ///Deserialize Object
    ///</summary>
    public static ConnectionString FromJson(string json) => JsonConvert.DeserializeObject<ConnectionString>(json, HelperConverter.Converter.Settings);
    
    ///<summary>
    ///Serialize Object
    ///</summary>
    public static string ToString(ConnectionString json) => JsonConvert.SerializeObject(json, Formatting.Indented);
}
