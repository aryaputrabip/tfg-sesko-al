using System;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.Threading.Tasks;
using HelperPlotting;
using System.Linq;

namespace Data.Connect
{
    public class DataConnect : MonoBehaviour
    {
        public static DataConnect instance;
        readonly string serverConfigLocation = "Data/conn.json";
        public PasukanLists pasukanList;
        public RadarLists radarList;

        void Awake()
        {
            if(instance == null)
            instance = this;
            else
            Destroy(gameObject);
        }

        public async Task<bool> GetListRadar(string json)
        {
            radarList.listRadar = await Task.Run(() => Radar.FromJson(json));
            return true;
        }

        public async Task<bool> GetListRadarDarat(string json)
        {
            radarList.listRadarSatuanDarat = await Task.Run(() => ListRadarSatuan.FromJson(json));
            return true;
        }
        
        public async Task<bool> GetListRadarLaut(string json)
        {
            radarList.listRadarSatuanLaut = await Task.Run(() => ListRadarSatuan.FromJson(json));
            return true;
        }
        
        public async Task<bool> GetListRadarUdara(string json)
        {
            radarList.listRadarSatuanUdara = await Task.Run(() => ListRadarSatuan.FromJson(json));
            return true;
        }

        public async Task<bool> GetListPasukan(string json)
        {
            pasukanList.listPasukan = await Task.Run(() => ListPasukanV2.FromJson(json));
            return true;
        }
    }

    [Serializable]
    public class RadarLists
    {
        public List<Radar>           listRadar = new();
        public List<ListRadarSatuan> listRadarSatuanDarat = new();
        public List<ListRadarSatuan> listRadarSatuanLaut = new();
        public List<ListRadarSatuan> listRadarSatuanUdara = new();
    }

    [Serializable]
    public class PasukanLists
    {
        public List<ListPasukanV2> listPasukan = new();
    }
}
