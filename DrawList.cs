using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace HelperPlotting
{
    [Serializable]
    public class DrawingGeometriesPoly
    {
        [JsonIgnore] [SerializeField] string _type;
        [JsonIgnore] [SerializeField] List<List<List<float>>> _coordinates;
        public string type { get{ return _type; } set{ _type = value; } }
        public List<List<List<float>>> coordinates { get{ return _coordinates; } set{ _coordinates = value; } }
        
        public static DrawingGeometriesPoly FromJson(string json) => JsonConvert.DeserializeObject<DrawingGeometriesPoly>(json);
        public static string ToString(DrawingGeometriesPoly json) => JsonConvert.SerializeObject(json, Formatting.Indented);
    }

    [Serializable]
    public class DrawingProperties
    {
        [JsonIgnore] [SerializeField] bool _stroke;
        [JsonIgnore] [SerializeField] string _color;
        [JsonIgnore] [SerializeField] float _weight;
        [JsonIgnore] [SerializeField] double _opacity;
        [JsonIgnore] [SerializeField] bool _fill;
        [JsonIgnore] [SerializeField] string _fillColor;
        [JsonIgnore] [SerializeField] double _fillOpacity;
        [JsonIgnore] [SerializeField] bool _clickable;
        [JsonIgnore] [SerializeField] string _id_point;
        public bool stroke { get{ return _stroke; } set{ _stroke = value; } }
        public string color { get{ return _color; } set{ _color = value; } }
        public float weight { get{ return _weight; } set{ _weight = value; } }
        public double opacity { get{ return _opacity; } set{ _opacity = value; } }
        public bool fill { get{ return _fill; } set{ _fill = value; } }
        public string fillColor { get{ return _fillColor; } set{ _fillColor = value; } }
        public double fillOpacity { get{ return _fillOpacity; } set{ _fillOpacity = value; } }
        public bool clickable { get{ return _clickable; } set{ _clickable = value; } }
        public string id_point { get{ return _id_point; } set{ _id_point = value; } }
        
        public static DrawingProperties FromJson(string json) => JsonConvert.DeserializeObject<DrawingProperties>(json);
        public static string ToString(DrawingProperties json) => JsonConvert.SerializeObject(json);
    }

    [Serializable]
    public class EntityToolV2
    {
        [JsonIgnore] [SerializeField] int _id;
        [JsonIgnore] [SerializeField] int _id_user;
        [JsonIgnore] [SerializeField] string _dokumen;
        [JsonIgnore] [SerializeField] string _nama;
        [JsonIgnore] [SerializeField] [TextArea(15, 20)] string _geometry;
        [JsonIgnore] [SerializeField] DrawingProperties _properties;
        [JsonIgnore] [SerializeField] string _type;
        public int id { get{ return _id; } set{ _id = value; } }
        public int id_user { get{ return _id_user; } set{ _id_user = value; } }
        public string dokumen { get{ return _dokumen; } set{ _dokumen = value; } }
        public string nama { get{ return _nama; } set{ _nama = value; } }
        public string geometry { get{ return _geometry; } set{ _geometry = value; } }
        public DrawingProperties properties { get{ return _properties; } set{ _properties = value; } }
        public string type { get{ return _type; } set{ _type = value; } }

        [JsonConstructor]
        public EntityToolV2(string geometry, string properties)
        {
            var jObj        = JObject.Parse(geometry);
            this.geometry   = jObj.ToString();
            this.properties = DrawingProperties.FromJson(properties);
        }
        public EntityToolV2(){}

        public static EntityToolV2 FromJson(string json) => JsonConvert.DeserializeObject<EntityToolV2>(json);
        public static string ToString(EntityToolV2 json) => JsonConvert.SerializeObject(json, Formatting.Indented);
    }
}
