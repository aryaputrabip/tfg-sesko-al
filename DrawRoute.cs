using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using System;

namespace Plot.FormasiPlot
{
    public class DrawRoute : MonoBehaviour
    {
        public float GetAngleOfLineBetweenTwoPoints(Vector2 p1, Vector2 p2)
        {
            // var data = Math.Atan2((p2.x - p1.x), (p2.y - p1.y)) * (180 / Math.PI);

            // Calculate the distance in km between locations.
            float distance = OnlineMapsUtils.DistanceBetweenPoints(p1, p2).magnitude;

            int zoom = 15;
            int maxX = 1 << (zoom - 1);

            // Calculate the tile position of locations.
            double userTileX, userTileY, markerTileX, markerTileY;
            OnlineMaps.instance.projection.CoordinatesToTile(p1.x, p1.y, zoom, out userTileX, out userTileY);
            OnlineMaps.instance.projection.CoordinatesToTile(p2.x, p2.y, zoom, out markerTileX, out markerTileY);

            // Calculate the angle between locations.
            double angle = OnlineMapsUtils.Angle2D(userTileX, userTileY, markerTileX, markerTileY);
            if (Math.Abs(userTileX - markerTileX) > maxX) angle = 360 - angle;

            return (float)angle;
        }

        public List<Vector2> Formasi1(List<float> jarakLng, List<float> jarakLat, Vector2 origin, List<Vector2> points)
        {
            var newPoints = new List<Vector2>();
            Vector2 p1 = points[0];
            Vector2 p2 = new Vector2();
            Vector2 p3 = new Vector2();
            Vector2 p4 = new Vector2();

            for(int i = 1; i < (points.Count + 1); i++)
            {
                var ox = origin.x;
                var oy = origin.y;
                var px = 0f;
                var py = 0f;
                
                px = ox + jarakLng[i - 1];
                py = oy + jarakLat[i - 1];
                points[i - 1] = new Vector2(px, py);

                if(i % 4 == 1)
                {
                    if(i != 1)
                    {
                        px = p1.x + jarakLng[i - 1];
                        py = p1.y + jarakLat[i - 1];
                        points[i - 1] = new Vector2(px, py);
                        p1 = points[i - 1];
                    }
                    else
                    {
                        p1 = points[i - 1];
                    }
                }
                else if(i % 4 == 2)
                {
                    if(i != 2)
                    {
                        px = p2.x + jarakLng[i - 1];
                        py = p2.y + jarakLat[i - 1];
                        points[i - 1] = new Vector2(px, py);
                        p2 = points[i - 1];
                    }
                    else
                    {
                        p2 = points[i - 1];
                    }
                }
                else if(i % 4 == 3)
                {
                    if(i != 3)
                    {
                        px = p3.x + jarakLng[i - 1];
                        py = p3.y + jarakLat[i - 1];
                        points[i - 1] = new Vector2(px, py);
                        p3 = points[i - 1];
                    }
                    else
                    {
                        p3 = points[i - 1];
                    }
                }
                else if(i % 4 == 0)
                {
                    if(i != 4)
                    {
                        px = p4.x + jarakLng[i - 1];
                        py = p4.y + jarakLat[i - 1];
                        points[i - 1] = new Vector2(px, py);
                        p4 = points[i - 1];
                    }
                    else
                    {
                        p4 = points[i - 1];
                    }
                }

                newPoints.Add(points[i - 1]);
            }
            
            return newPoints;
        }

        public List<Vector2> Formasi2(List<float> jarakLng, List<float> jarakLat, Vector2 origin, List<Vector2> points)
        {
            var newPoints = new List<Vector2>();
            Vector2 p1 = points[0];
            Vector2 p2 = new Vector2();

            for(int i = 1; i < (points.Count + 1); i++)
            {
                var ox = origin.x;
                var oy = origin.y;
                var px = ox + jarakLng[i - 1];
                var py = oy + jarakLat[i - 1];
                points[i - 1] = new Vector2(px, py);

                if(i % 2 == 1)
                {
                    if(i == 1)
                    p1 = points[0];
                    else
                    {
                        px = p1.x + jarakLng[i - 1];
                        py = p1.y + jarakLat[i - 1];
                        points[i - 1] = new Vector2(px, py);
                        p1 = points[i - 1];
                    }
                }
                else if(i % 2 == 0)
                {
                    if(i == 2)
                    p2 = points[1];
                    else
                    {
                        px = p2.x + jarakLng[i - 1];
                        py = p2.y + jarakLat[i - 1];
                        points[i - 1] = new Vector2(px, py);
                        p2 = points[i - 1];
                    }
                }

                newPoints.Add(points[i - 1]);
            }
            
            return newPoints;
        }
        
        public List<Vector2> Formasi3(List<float> jarakLng, List<float> jarakLat, Vector2 origin, List<Vector2> points)
        {
            var newPoints = new List<Vector2>();
            Vector2 p1 = points[0];
            Vector2 p2 = new Vector2();

            for(int i = 1; i < (points.Count + 1); i++)
            {
                var ox = origin.x;
                var oy = origin.y;
                var px = 0f;
                var py = 0f;
                
                px = ox + jarakLng[i - 1];
                py = oy + jarakLat[i - 1];
                points[i - 1] = new Vector2(px, py);

                if(i % 4 == 1 || i % 4 == 2)
                {
                    if(i != 1)
                    {
                        px = p1.x + jarakLng[i - 1];
                        py = p1.y + jarakLat[i - 1];
                        points[i - 1] = new Vector2(px, py);
                        p1 = points[i - 1];
                    }
                    else
                    p1 = points[i - 1];
                }
                else if(i % 4 == 3 || i % 4 == 0)
                {
                    if(i != 3)
                    {
                        px = p2.x + jarakLng[i - 1];
                        py = p2.y + jarakLat[i - 1];
                        points[i - 1] = new Vector2(px, py);
                        p2 = points[i - 1];
                    }
                    else
                    p2 = points[i - 1];
                }
                newPoints.Add(points[i - 1]);
            }
            
            return newPoints;
        }
        
        public List<Vector2> Formasi4(List<float> jarakLng, List<float> jarakLat, Vector2 origin, List<Vector2> points)
        {
            var newPoints = new List<Vector2>();
            Vector2 p1 = points[0];
            Vector2 p2 = new Vector2();

            for(int i = 1; i < (points.Count + 1); i++)
            {
                var ox = origin.x;
                var oy = origin.y;
                var px = 0f;
                var py = 0f;
                
                px = ox + jarakLng[i - 1];
                py = oy + jarakLat[i - 1];
                points[i - 1] = new Vector2(px, py);

                if(i % 2 == 1)
                {
                    if(i != 1)
                    {
                        px = p1.x + jarakLng[i - 1];
                        py = p1.y + jarakLat[i - 1];
                        points[i - 1] = new Vector2(px, py);
                        p1 = points[i - 1];
                    }
                    else
                    p1 = points[i - 1];
                }
                else if(i % 2 == 0)
                {
                    if(i != 2)
                    {
                        px = p2.x + jarakLng[i - 1];
                        py = p2.y + jarakLat[i - 1];
                        points[i - 1] = new Vector2(px, py);
                        p2 = points[i - 1];
                    }
                    else
                    p2 = points[i - 1];
                }
                newPoints.Add(points[i - 1]);
            }
            
            return newPoints;
        }
        
        public List<Vector2> Formasi5(List<float> jarakLng, List<float> jarakLat, Vector2 origin, List<Vector2> points)
        {
            var newPoints = new List<Vector2>();
            Vector2 p1 = points[0];
            Vector2 p2 = new Vector2();
            Vector2 p3 = new Vector2();
            Vector2 p4 = new Vector2();

            for(int i = 1; i < (points.Count + 1); i++)
            {
                var ox = origin.x;
                var oy = origin.y;
                var px = 0f;
                var py = 0f;
                
                px = ox + jarakLng[i - 1];
                py = oy + jarakLat[i - 1];
                points[i - 1] = new Vector2(px, py);

                if(i % 5 == 1)
                {
                    if(i != 1)
                    {
                        px = p1.x + jarakLng[i - 1];
                        py = p1.y + jarakLat[i - 1];
                        points[i - 1] = new Vector2(px, py);
                        p1 = points[i - 1];
                    }
                    else
                    p1 = points[i - 1];
                }
                else if(i % 5 == 2)
                {
                    if(i != 2)
                    {
                        px = p2.x + jarakLng[i - 1];
                        py = p2.y + jarakLat[i - 1];
                        points[i - 1] = new Vector2(px, py);
                        p2 = points[i - 1];
                    }
                    else
                    p2 = points[i - 1];
                }
                else if(i % 5 == 3 || i % 5 == 0)
                {
                    if(i != 3)
                    {
                        px = p3.x + jarakLng[i - 1];
                        py = p3.y + jarakLat[i - 1];
                        points[i - 1] = new Vector2(px, py);
                        p3 = points[i - 1];
                    }
                    else
                    p3 = points[i - 1];
                }
                else if(i % 5 == 4)
                {
                    if(i != 4)
                    {
                        px = p4.x + jarakLng[i - 1];
                        py = p4.y + jarakLat[i - 1];
                        points[i - 1] = new Vector2(px, py);
                        p4 = points[i - 1];
                    }
                    else
                    p4 = points[i - 1];
                }

                newPoints.Add(points[i - 1]);
            }
            
            return newPoints;
        }
        
        public List<Vector2> Formasi6(List<float> jarakLng, List<float> jarakLat, Vector2 origin, List<Vector2> points)
        {
            var newPoints = new List<Vector2>();
            
            var listAPoint = new List<Vector2>();
            var listAJrkLng = new List<float>();
            var listAJrkLat = new List<float>();

            var listBPoint = new List<Vector2>();
            var listBJrkLng = new List<float>();
            var listBJrkLat = new List<float>();
            
            for(int i = 1; i < points.Count + 1; i++)
            {
                if (i == 1 || i == 2 || i == 3 || i == 4)
                {
                    listAPoint.Add(points[i - 1]);
                    listAJrkLng.Add(jarakLng[i - 1]);
                    listAJrkLat.Add(jarakLat[i - 1]);
                }
                else
                {
                    listBPoint.Add(points[i - 1]);
                    listBJrkLng.Add(jarakLng[i - 1]);
                    listBJrkLat.Add(jarakLat[i - 1]);
                }
            }

            for(int i = 0; i < listAPoint.Count; i++)
            {
                var px = listAPoint[i].x + listAJrkLng[i];
                var py = listAPoint[i].y + listAJrkLat[i];

                listAPoint[i] = new Vector2(px, py);
            }

            var p3 = listAPoint[2];
            for(int i = 0; i < listBPoint.Count; i++)
            {
                var px = listBPoint[i].x + listBJrkLng[i];
                var py = listBPoint[i].y + listBJrkLat[i];

                listBPoint[i] = new Vector2(px, py);
            }

            newPoints.AddRange(listAPoint);
            newPoints.AddRange(listBPoint);
                
            return newPoints;
        }
        
        public List<Vector2> Formasi7(List<float> jarakLng, List<float> jarakLat, Vector2 origin, List<Vector2> points)
        {
            var newPoints = new List<Vector2>();
            Vector2 p1 = points[0];

            for(int i = 1; i < (points.Count + 1); i++)
            {
                var ox = origin.x;
                var oy = origin.y;
                var px = 0f;
                var py = 0f;
                
                px = ox + jarakLng[i - 1];
                py = oy + jarakLat[i - 1];
                points[i - 1] = new Vector2(px, py);
                
                newPoints.Add(points[i - 1]);
            }

            return newPoints;
        }
    }
}