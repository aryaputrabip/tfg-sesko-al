using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using static ListOfSimpleFunctions.Functions;
using System.Drawing;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class ElevationViaCoords : MonoBehaviour
{
    [SerializeField] Point obj;
    [SerializeField] string jsonFolder;
    [SerializeField] string jsonFile;

    /// <summary>
    /// The location of the coordinate where the mouse is.
    /// </summary>
    [SerializeField] Vector2 location;
    
    /// <summary>
    /// The elevation of the coordinate where the mouse is.
    /// </summary>
    public float elevation;
    
    /// <summary>
    /// Bing API
    /// </summary>
    string bingAPI => OnlineMapsKeyManager.BingMaps();
    float beforeElevation;

    void Start()
    {
        // loadJsonFile();
    }

    void Update()
    {
        double lng, lat;
        if(OnlineMapsControlBase.instance.GetCoords(out lng, out lat)){location = new((float)lng, (float)lat); GetLocation(location); beforeElevation = elevation;}
    }

    ///<param name = "coords"> The input coordinate to get the elevation </param>
    public void GetLocation(Vector2 coords)
    {
        OnlineMapsBingMapsElevation.GetElevationByPoints(bingAPI, new Vector2[]{coords}).OnComplete += OnComplete;
    }

    void OnComplete(string response)
    {
        // 1. Load result object
        OnlineMapsBingMapsElevationResult result = OnlineMapsBingMapsElevation.GetResult(response, OnlineMapsBingMapsElevation.Output.json);
        
        // 2. Log the elevations
        if (result != null) elevation = result.resourceSets[0].resources[0].elevations[0];
    }
    
    private void loadJsonFile()
    {
        var jFol = Path.Combine(Directory.GetCurrentDirectory(), jsonFolder);
        var jFil = Path.Combine(jFol, jsonFile);
        Debug.Log("Opening " + jFil.ToString() + "!");
        if (File.Exists(jFil))
        {
            Debug.Log("File Found! " + jFil.ToString());
            try
            {
                using (StreamReader r = new StreamReader(jFil))
                {
                    string JSON = r.ReadToEnd();
                    obj = JsonConvert.DeserializeObject<Point>(JSON);
                }
            }
            catch(Exception ex)
            {
                Debug.LogError("File Cannot Be Parsed!!!" + "\n" + ex);
            }
        }
        else
        Debug.LogError("File Not Found!!!");
    }
}
