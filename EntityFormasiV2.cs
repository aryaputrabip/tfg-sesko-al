using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace HelperPlotting
{
    [Serializable]
    public class IconFormasiSatuanV2
    {
        [JsonIgnore] [SerializeField] string _nama;
        [JsonIgnore] [SerializeField] string _index;
        [JsonIgnore] [SerializeField] string _keterangan;
        [JsonIgnore] [SerializeField] int _grup;
        [JsonIgnore] [SerializeField] string _entity_name;
        [JsonIgnore] [SerializeField] string _entity_kategori;
        [JsonIgnore] [SerializeField] string _jenis_role;
        [JsonIgnore] [SerializeField] List<LabelStylePasukan> _label_style;
        public string nama { get{ return _nama; } set{ _nama = value; } }
        public string index { get{ return _index; } set{ _index = value; } }
        public string keterangan { get{ return _keterangan; } set{ _keterangan = value; } }
        public int grup { get{ return _grup; } set{ _grup = value; } }
        public string entity_name { get{ return _entity_name; } set{ _entity_name = value; } }
        public string entity_kategori { get{ return _entity_kategori; } set{ _entity_kategori = value; } }
        public string jenis_role { get{ return _jenis_role; } set{ _jenis_role = value; } }
        public List<LabelStylePasukan> label_style { get{ return _label_style; } set{ _label_style = value; } }

        [JsonConstructor]
        public IconFormasiSatuanV2(string label_style)
        {
            this.label_style = LabelStylePasukan.FromJson(label_style);
        }
        public IconFormasiSatuanV2(){}
        
        public static IconFormasiSatuanV2 FromJson(string json) => JsonConvert.DeserializeObject<IconFormasiSatuanV2>(json);
        public static string ToString(IconFormasiSatuanV2 json) => JsonConvert.SerializeObject(json);
    }

    [Serializable]
    public class InfoFormasiV2
    {
        [JsonIgnore] [SerializeField] string _nama_formasi;
        [JsonIgnore] [SerializeField] string _warna;
        [JsonIgnore] [SerializeField] string _size;
        [JsonIgnore] [SerializeField] string _id_point;
        [JsonIgnore] [SerializeField] int _arah;
        [JsonIgnore] [SerializeField] List<int> _arrArah;
        [JsonIgnore] [SerializeField] List<SatuanFormasiV2> _satuan_formasi;
        public string nama_formasi { get{ return _nama_formasi; } set{ _nama_formasi = value; } }
        public string warna { get{ return _warna; } set{ _warna = value; } }
        public string size { get{ return _size; } set{ _size = value; } }
        public string id_point { get{ return _id_point; } set{ _id_point = value; } }
        public int arah { get{ return _arah; } set{ _arah = value; } }
        public List<int> arrArah { get{ return _arrArah; } set{ _arrArah = value; } }
        public List<SatuanFormasiV2> satuan_formasi { get{ return _satuan_formasi; } set{ _satuan_formasi = value; } }

        public static InfoFormasiV2 FromJson(string json) => JsonConvert.DeserializeObject<InfoFormasiV2>(json);
        public static string ToString(InfoFormasiV2 json) => JsonConvert.SerializeObject(json);
    }

    [Serializable]
    public class EntityFormasiV2
    {
        [JsonIgnore] [SerializeField] string _id;
        [JsonIgnore] [SerializeField] string _id_user;
        [JsonIgnore] [SerializeField] string _dokumen;
        [JsonIgnore] [SerializeField] string _nama;
        [JsonIgnore] [SerializeField] object _lat_y;
        [JsonIgnore] [SerializeField] object _lng_x;
        [JsonIgnore] [SerializeField] InfoFormasiV2 _info_formasi;
        [JsonIgnore] [SerializeField] object _tgl_formasi;
        [JsonIgnore] [SerializeField] object _kecepatan;
        [JsonIgnore] [SerializeField] string _size;
        [JsonIgnore] [SerializeField] string _jenis_formasi;
        [JsonIgnore] [SerializeField] object _symbol;
        [JsonIgnore] [SerializeField] string _warna;
        public string id { get{ return _id; } set{ _id = value; } }
        public string id_user { get{ return _id_user; } set{ _id_user = value; } }
        public string dokumen { get{ return _dokumen; } set{ _dokumen = value; } }
        public string nama { get{ return _nama; } set{ _nama = value; } }
        public object lat_y { get{ return _lat_y; } set{ _lat_y = value; } }
        public object lng_x { get{ return _lng_x; } set{ _lng_x = value; } }
        public InfoFormasiV2 info_formasi { get{ return _info_formasi; } set{ _info_formasi = value; } }

        [JsonConstructor]
        public EntityFormasiV2(string info_formasi)
        {
            this.info_formasi = InfoFormasiV2.FromJson(info_formasi);
        }
        public EntityFormasiV2(){}

        public object tgl_formasi { get{ return _tgl_formasi; } set{ _tgl_formasi = value; } }
        public object kecepatan { get{ return _kecepatan; } set{ _kecepatan = value; } }
        public string size { get{ return _size; } set{ _size = value; } }
        public string jenis_formasi { get{ return _jenis_formasi; } set{ _jenis_formasi = value; } }
        public object symbol { get{ return _symbol; } set{ _symbol = value; } }
        public string warna { get{ return _warna; } set{ _warna = value; } }

        public static EntityFormasiV2 FromJson(string json) => JsonConvert.DeserializeObject<EntityFormasiV2>(json);
        public static string ToString(EntityFormasiV2 json) => JsonConvert.SerializeObject(json);
        public static string ToString(List<EntityFormasiV2> json) => JsonConvert.SerializeObject(json, Formatting.Indented);
    }

    [Serializable]
    public class SatuanFormasiV2
    {
        [JsonIgnore] [SerializeField] string _id_point;
        [JsonIgnore] [SerializeField] string _nama;
        [JsonIgnore] [SerializeField] IconFormasiSatuanV2 _icon;
        [JsonIgnore] [SerializeField] string _color;
        [JsonIgnore] [SerializeField] double _lat;
        [JsonIgnore] [SerializeField] double _lng;
        [JsonIgnore] [SerializeField] int _inti;
        [JsonIgnore] [SerializeField] int _id;
        [JsonIgnore] [SerializeField] string _jarak;
        [JsonIgnore] [SerializeField] int _sudut;
        [JsonIgnore] [SerializeField] string _satuan;
        [JsonIgnore] [SerializeField] double _new_jarak;
        public string id_point { get{ return _id_point; } set{ _id_point = value; } }
        public string nama { get{ return _nama; } set{ _nama = value; } }
        public IconFormasiSatuanV2 icon { get{ return _icon; } set{ _icon = value; } }
        public string color { get{ return _color; } set{ _color = value; } }
        public double lat { get{ return _lat; } set{ _lat = value; } }
        public double lng { get{ return _lng; } set{ _lng = value; } }
        public int inti { get{ return _inti; } set{ _inti = value; } }
        public int id { get{ return _id; } set{ _id = value; } }
        public string jarak { get{ return _jarak; } set{ _jarak = value; } }
        public int sudut { get{ return _sudut; } set{ _sudut = value; } }
        public string satuan { get{ return _satuan; } set{ _satuan = value; } }
        public double new_jarak { get{ return _new_jarak; } set{ _new_jarak = value; } }

        public static List<SatuanFormasiV2> FromJson(string json) => JsonConvert.DeserializeObject<List<SatuanFormasiV2>>(json);
        public static string ToString(SatuanFormasiV2 json) => JsonConvert.SerializeObject(json);
    }
}