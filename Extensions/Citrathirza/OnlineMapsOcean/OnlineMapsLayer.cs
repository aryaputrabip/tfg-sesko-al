using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlineMapsLayer : MonoBehaviour
{
    //    #region LAYER ALPHA CONFIGURATION
    //    [Range(0, 1)]
    //    public float alpha = 1;
    //    public float _alphaRange = 0.05f;
    //    #endregion

    private Texture2D texture;
    public static OnlineMapsLayer instance;
    private string url_layer;

    private void Start()
    {
        if (instance != null)
        {
            return;
        }

        instance = this;

        //if (OnlineMapsCache.instance != null)
        //{
        //    // Subscribe to the cache events.
        //    OnlineMapsCache.instance.OnLoadedFromCache += LoadTileOverlay;
        //}

        ////OnlineMaps.instance.OnChangeZoom += OnChangeZoom;
        //OnlineMapsTileManager.OnStartDownloadTile = OnStartDownloadTile;
        ////OnlineMapsTileManager.OnPreloadTiles += OnPreloadTile;
    }

    public void startLayer(string link, string layer_name)
    {
        url_layer = link + "?service=WMS&version=1.1.0&request=GetMap&layers=" + layer_name + "&bbox=";

        if (OnlineMapsCache.instance != null)
        {
            OnlineMapsCache.instance.OnLoadedFromCache += LoadTileOverlay;
        }

        OnlineMapsTileManager.OnStartDownloadTile = OnStartDownloadTile;
    }

    private void LoadTileOverlay(OnlineMapsTile tile)
    {
        int zoom = (OnlineMaps.instance.zoom > 16) ? 16 : tile.zoom;
        string param = tile.topLeft.x.ToString(OnlineMapsUtils.numberFormat) + "," + tile.bottomRight.y.ToString(OnlineMapsUtils.numberFormat) + "," + tile.bottomRight.x.ToString(OnlineMapsUtils.numberFormat) + "," + tile.topLeft.y.ToString(OnlineMapsUtils.numberFormat);
        //var url = "http://192.168.27.8:8080/geoserver/wms?service=WMS&version=1.1.0&request=GetMap&layers=Peta_Udara&bbox=" + param + "&transparent=true&width=256&height=256&srs=EPSG%3A4326&styles=&format=image%2Fpng";
        var url = url_layer + param + "&transparent=true&width=256&height=256&srs=EPSG%3A4326&styles=&format=image%2Fpng";

        Debug.Log("URL Basemap : " + url);
        OnlineMapsWWW www = new OnlineMapsWWW(url);
        www.OnComplete += OnDownloadComplete;
        www.customFields["tile"] = tile;
    }

    private void OnDownloadComplete(OnlineMapsWWW www)
    {
        OnlineMapsTile tile = www.customFields["tile"] as OnlineMapsTile;
        if (tile == null || tile.status == OnlineMapsTileStatus.disposed || tile.status == OnlineMapsTileStatus.error) return;

        texture = new Texture2D(256, 256);
        www.LoadImageIntoTexture(texture);
        tile.overlayFrontTexture = texture;

        ///www.Dispose();
        //Resources.UnloadUnusedAssets();
    }

    private void OnStartDownloadTile(OnlineMapsTile tile)
    {
        //tile.OnReplaceURLToken += OnReplaceUrlToken;
        tile.status = OnlineMapsTileStatus.loading;

        // Load overlay for tile.
        LoadTileOverlay(tile);

        // Load the tile using a standard loader.
        OnlineMapsTileManager.StartDownloadTile(tile);
    }

    //private void OnChangeZoom()
    //{
    //    if (OnlineMaps.instance.zoom >= 10)
    //    {
    //        var ss = (OnlineMaps.instance.floatZoom - 14 + 2) / 2;

    //        alpha = (ss < 0.35) ? 0 : (OnlineMaps.instance.floatZoom - 14 + 2) / 2;

    //        alpha = (OnlineMaps.instance.floatZoom - 12 + 2) / 2;
    //        alpha = alpha > 0.55f ? 0.55f : alpha;
    //    }
    //    else
    //    {
    //        alpha = 0;
    //    }

    //    if (alpha >= 0.5f)
    //    {
    //        _alphaRange = 0.52f;
    //    }
    //    else if (alpha >= 0.35f)
    //    {
    //        _alphaRange = 0.72f;
    //    }
    //    else
    //    {
    //        _alphaRange = 0;
    //    }
    //}

    //private void OnPreloadTile()
    //{
    //    foreach (OnlineMapsTile ss in OnlineMaps.instance.tileManager.tiles)
    //    {
    //        ss.overlayBackAlpha = alpha;
    //        ss.oceanTiling = (OnlineMaps.instance.zoom == 20) ? 21 : OnlineMaps.instance.zoom;
    //        ss.oceanAlphaRange = _alphaRange;
    //    }
    //}
}
