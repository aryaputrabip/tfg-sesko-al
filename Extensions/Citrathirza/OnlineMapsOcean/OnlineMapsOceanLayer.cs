using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlineMapsOceanLayer : MonoBehaviour
{
    #region LAYER ALPHA CONFIGURATION
    [Range(0, 1)]
    public float alpha = 1;
    public float _alphaRange = 0.05f;
    #endregion

    private Texture2D texture;

    private void Start()
    {
        if (OnlineMapsCache.instance != null)
        {
            // Subscribe to the cache events.
            OnlineMapsCache.instance.OnLoadedFromCache += LoadTileOverlay;
        }

        OnlineMaps.instance.OnChangeZoom += OnChangeZoom;
        OnlineMapsTileManager.OnStartDownloadTile = OnStartDownloadTile;
        OnlineMapsTileManager.OnPreloadTiles += OnPreloadTile;
    }

    private void LoadTileOverlay(OnlineMapsTile tile)
    {
        int zoom = (OnlineMaps.instance.zoom > 16) ? 16 : tile.zoom;
        var url = "https://a.basemaps.cartocdn.com/dark_nolabels/" + tile.zoom + "/" + tile.x + "/" + tile.y + ".png";
        //var url = "http://tile.mtbmap.cz/mtbmap_tiles/" + tile.zoom+"/"+tile.x+"/"+tile.y+".png";
        //var url = "https://services.arcgisonline.com/arcgis/rest/services/Canvas/World_Dark_Gray_Base/MapServer/tile/" + zoom + "/" + tile.y + "/" + tile.x + ".png";

        OnlineMapsWWW www = new OnlineMapsWWW(url);
        www.OnComplete += OnDownloadComplete;
        www.customFields["tile"] = tile;
    }

    private void OnDownloadComplete(OnlineMapsWWW www)
    {
        OnlineMapsTile tile = www.customFields["tile"] as OnlineMapsTile;
        if (tile == null || tile.status == OnlineMapsTileStatus.disposed || tile.status == OnlineMapsTileStatus.error) return;

        texture = new Texture2D(256, 256);
        www.LoadImageIntoTexture(texture);
        tile.overlayBackTexture = texture;

        ///www.Dispose();
        //Resources.UnloadUnusedAssets();
    }

    private void OnStartDownloadTile(OnlineMapsTile tile)
    {
        tile.status = OnlineMapsTileStatus.loading;

        // Load overlay for tile.
        LoadTileOverlay(tile);

        // Load the tile using a standard loader.
        OnlineMapsTileManager.StartDownloadTile(tile);
    }

    private void OnChangeZoom()
    {
        if (OnlineMaps.instance.zoom >= 10)
        {
            var ss = (OnlineMaps.instance.floatZoom - 14 + 2) / 2;

            alpha = (ss < 0.35) ? 0 : (OnlineMaps.instance.floatZoom - 14 + 2) / 2;

            alpha = (OnlineMaps.instance.floatZoom - 12 + 2) / 2;
            alpha = alpha > 0.55f ? 0.55f : alpha;
        }
        else
        {
            alpha = 0;
        }

        if (alpha >= 0.5f)
        {
            _alphaRange = 0.52f;
        }
        else if (alpha >= 0.35f)
        {
            _alphaRange = 0.72f;
        }
        else
        {
            _alphaRange = 0;
        }
    }

    private void OnPreloadTile()
    {
        foreach (OnlineMapsTile ss in OnlineMaps.instance.tileManager.tiles)
        {
            ss.overlayBackAlpha = alpha;
            ss.oceanTiling = (OnlineMaps.instance.zoom == 20) ? 21 : OnlineMaps.instance.zoom;
            ss.oceanAlphaRange = _alphaRange;
        }
    }
}
