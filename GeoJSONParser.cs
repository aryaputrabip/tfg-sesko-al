using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

public class GeoJSONParser : MonoBehaviour
{
    public string JSONString;
    public string JSONStringCZ;
    List<Geodata> listGeo;
    Geodata geodat;
    // Start is called before the first frame update
    void Start()
    {
        if (JSONString != null)
            listGeo = JsonConvert.DeserializeObject<List<Geodata>>(JSONString);

        Debug.Log("GEOJSONParser! Number of indexes : "+ listGeo.Count);
        JSONStringCZ = generateCZ(listGeo);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private string generateCZ(List<Geodata> geo)
    {
        List<ParsedGeoData> pg = new List<ParsedGeoData>();
        foreach (Geodata ge in geo)
        {
            JArray ja = JArray.FromObject(ge.geometry.coordinates[0]);
            if(ge.geometry.type == "MultiPolygon")
            {
                ja = (JArray) ja[0];
            }

            Debug.Log("Poly Kocag : " + ja.ToString());

            Vector2 center;
            int zoom;
            List<Vector2> listCr = new List<Vector2>();
            List<List<double>> kocag = ja.ToObject<List<List<double>>>();

            foreach (List<double> cr in kocag)
            {
                listCr.Add(new Vector2((float)cr[0], (float)cr[1]));
            }

            Vector2[] postions = listCr.ToArray();

            OnlineMapsUtils.GetCenterPointAndZoom(postions, out center, out zoom);

            ParsedGeoData pgeo = new ParsedGeoData();

            pgeo.namaKota = ge.properties.Name;
            pgeo.centerLng = center.x;
            pgeo.centerLat = center.y;
            pgeo.zoomLevel = zoom - 3;
            pg.Add(pgeo);
        }

        string res = (pg.Count > 0) ? JsonConvert.SerializeObject(pg) : null;

        return res;
    }

    // Root myDeserializedClass = JsonConvert.DeserializeObject<List<Root>>(myJsonResponse);
    public class Geometry
    {
        public string type { get; set; }
        public List<List<object>> coordinates { get; set; }
    }

    public class Properties
    {
        public string Kind { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Year { get; set; }
        public string Source { get; set; }
        public int Parent { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
    }

    public class Geodata
    {
        public string type { get; set; }
        public int code { get; set; }
        public Properties properties { get; set; }
        public Geometry geometry { get; set; }
    }

    public class ParsedGeoData
    {
        public string namaKota { get; set; }
        public double centerLng { get; set; }
        public double centerLat { get; set; }
        public int zoomLevel { get; set; }
    }

    public class Poly
    {
        List<List<double>> coords { get; set; }
    }
}
