using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using UnityEngine;

public class KekuatanStat : MonoBehaviour
{
    public string nama;
    public string namaSatuan;
    public string warna;
    public string pemilikKekuatan;
    public string keterangan;
    public List<GameObject> satuanList;
    public List<string> matraSatuan;
    public string infoKekuatan;
    public string rincian;
    public double lat_y;
    public double lng_x;
    [HideInInspector] public OnlineMapsMarker3D marker3D;
}
