using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GlobalVariables;
using SickscoreGames.HUDNavigationSystem;
using UnityEngine;

namespace ListOfSimpleFunctions
{
    public static class Functions
    {
        ///<summary>
        ///Creating a 2D Marker using OnlineMapsMarkerManager with variable of double longitude, double latitude, and 2D Texture
        ///</summary>
        public static OnlineMapsMarker Create2D(double lng, double lat, Texture2D texture) => OnlineMapsMarkerManager.CreateItem(lng, lat, texture);

        ///<summary>
        ///Creating a 3D Marker using OnlineMapsMarker3DManager with variable of double longitude, double latitude, and 3D Prefab
        ///</summary>
        public static OnlineMapsMarker3D Create3D(double lng, double lat, GameObject prefab) => OnlineMapsMarker3DManager.CreateItem(lng, lat, prefab);
        
        ///<summary>
        ///Creating a 3D Marker using OnlineMapsMarker3DManager with variables of Vector2 position and 3D Prefab
        ///</summary>
        public static OnlineMapsMarker3D Create3D(Vector2 position, GameObject prefab) => OnlineMapsMarker3DManager.CreateItem(position, prefab);

        ///<summary>
        ///Drawing a Line using OnlineMapsDrawingElementManager with variables of IEnumarable points
        ///</summary>
        public static OnlineMapsDrawingLine DrawLine(IEnumerable points) => new(points);
        
        ///<summary>
        ///Drawing a Line using OnlineMapsDrawingElementManager with variables of IEnumarable points and Color
        ///</summary>
        public static OnlineMapsDrawingLine DrawLine(IEnumerable points, Color color) => new(points, color);
        
        ///<summary>
        ///Drawing a Line using OnlineMapsDrawingElementManager with variables of IEnumarable points, Color, and width
        ///</summary>
        public static OnlineMapsDrawingLine DrawLine(IEnumerable points, Color color, float width) => new(points, color, width);

        ///<summary>
        ///Drawing a Rect using OnlineMapsDrawingElementManager with variables of double x, double y, width, and heigth
        ///</summary>
        public static OnlineMapsDrawingRect DrawRect(double x, double y, double width, double heigth) => new(x, y, width, heigth);

        ///<summary>
        ///Drawing a Rect using OnlineMapsDrawingElementManager with variables of Vector2 position and Vector2 size
        ///</summary>
        public static OnlineMapsDrawingRect DrawRect(OnlineMapsVector2d position, OnlineMapsVector2d size) => new(position, size);

        ///<summary>
        ///Drawing a Rect using OnlineMapsDrawingElementManager with variables of Rect
        ///</summary>
        public static OnlineMapsDrawingRect DrawRect(Rect rect) => new(rect);

        ///<summary>
        ///Drawing a Poly using OnlineMapsDrawingElementManager with variables of Vector2 points
        ///</summary>
        public static OnlineMapsDrawingPoly DrawPoly(IEnumerable points) => new(points);

        ///<summary>
        ///Drawing a Poly using OnlineMapsDrawingElementManager with variables of Vector2 points and Color
        ///</summary>
        public static OnlineMapsDrawingPoly DrawPoly(IEnumerable points, Color color) => new(points, color);

        ///<summary>
        ///Drawing a Poly using OnlineMapsDrawingElementManager with variables of Vector2 points, Color borderColor, and float borderWidth
        ///</summary>
        public static OnlineMapsDrawingPoly DrawPoly(IEnumerable points, Color borderColor, float borderWidth) => new(points, borderColor, borderWidth);

        ///<summary>
        ///Drawing a Poly using OnlineMapsDrawingElementManager with variables of Vector2 points, Color borderColor, and float borderWidth
        ///</summary>
        public static OnlineMapsDrawingPoly DrawPoly(IEnumerable points, Color borderColor, float borderWidth, Color backgroundColor) => new(points, borderColor, borderWidth, backgroundColor);

        ///<summary>
        ///Add the Drawing Element into the Drawing List
        ///</summary>
        public static OnlineMapsDrawingElement AddElement(OnlineMapsDrawingElement element) => OnlineMapsDrawingElementManager.AddItem(element);

        ///<summary>
        ///Create a Random Number between x and y
        ///</summary>
        public static int Randomizer(int x, int y) => Random.Range(x, y);
        
        ///<summary>
        ///Create a Random Number between x and y with decimals
        ///</summary>
        public static float Randomizer(float x, float y) => Random.Range(x, y);
        
        ///<summary>
        ///Checks a Vector if it is Inside of Polygon or Not
        ///</summary>
        public static bool IsPointInPoly(List<Vector2> poly, float x, float y) => OnlineMapsUtils.IsPointInPolygon(poly, x, y);
        
        ///<summary>
        ///Checks a Vector if it is Inside of Polygon or Not
        ///</summary>
        public static bool IsPointInPoly(List<Vector2> poly, Vector2 point) => OnlineMapsUtils.IsPointInPolygon(poly, point.x, point.y);

        ///<summary>
        ///Create a Parent for Marker
        ///</summary>
        public static Transform CreateMarkerParent(string name, GameObject parent = null)
        {
            parent = GameObject.Find(name);
            if(parent == null)
            {
                parent = new GameObject(name);
                parent.transform.parent = OnlineMaps.instance.transform;
                parent.transform.localPosition = Vector3.zero;
                parent.transform.localRotation = Quaternion.identity;
                parent.transform.localScale = Vector3.one;
            }

            return parent.transform;
        }

        ///<summary>
        ///Create a name for the Satuan entity
        ///</summary>
        public static string CreateNameSatuan(string type = "")
        {
            var index = 0;

            var entity = OnlineMapsMarker3DManager.instance;
            foreach(OnlineMapsMarker3D item in entity.Cast<OnlineMapsMarker3D>())
            if(item.instance.tag == "Plot_Pasukan" || item.instance.tag == "entity-satuan")
            index++;

            var name = type + "_" + index + "_" + SessionUser.id + "_" + Randomizer(1, 999);
            return name;
        }
        
        ///<summary>
        ///Create a name for the entity
        ///</summary>
        public static string CreateName(string type = "", string tag = "")
        {
            var index = 0;

            var entity = OnlineMapsMarker3DManager.instance;
            foreach(OnlineMapsMarker3D item in entity.Cast<OnlineMapsMarker3D>())
            if(item.instance.tag == tag)
            index++;

            var name = type + "_" + index + "_" + SessionUser.id + "_" + Randomizer(1, 999);
            return name;
        }
        
        ///<summary>
        ///Create a name for the entity
        ///</summary>
        public static string CreateDrawingName(string type = "", string tag = "")
        {
            var index = 0;

            var entity = GameObject.FindGameObjectsWithTag(tag);
            foreach(var item in entity)
            if(item.tag == tag)
            index++;

            var name = type + "_" + index + "_" + SessionUser.id + "_" + Randomizer(1, 999);
            return name;
        }

        ///<summary>
        ///Create a HUD for the entity
        ///</summary>
        public static GameObject SetHUD(OnlineMapsMarker3D marker, string name, string color, string fontFamily, string fontIndex, GameObject HUD = null)
        {
            var _HUD = marker.transform.Find("HUD");
            if(_HUD != null)
            GameObject.Destroy(_HUD.gameObject);

            HUD = new GameObject("HUD");
            HUD.transform.parent = marker.transform;
            HUD.transform.localPosition = Vector3.zero;

            // Add Simbol Taktis Component
            var HUDObject = AssetPackageManager.Instance.defaultHUD;
            var parentHUD = new GameObject("HUD_ELEMENT");
            parentHUD.transform.parent = HUD.transform;
            parentHUD.transform.localPosition = new Vector3(0, marker.instance.tag == "entity-satuan" || marker.instance.tag == "entity-musuh" ? marker.scale * 6.25f : 0, 0);

            parentHUD.tag = "entity-simboltaktis";

            var HUDElement = GameObject.Instantiate(HUDObject.prefabHUDElement.gameObject, parentHUD.transform).GetComponent<HUDNavigationElement>();
            HUDElement.Prefabs.IndicatorPrefab = GameObject.Instantiate(HUDElement.Prefabs.IndicatorPrefab, HUD.transform);
            HUDElement.Prefabs.IndicatorPrefab.name = "-- simbol taktis --";

            fontIndex = ((char)int.Parse(fontIndex)).ToString();
            HUDElement.Prefabs.IndicatorPrefab.ChangeFontTaktis(fontIndex, fontFamily);
            HUDElement.Prefabs.IndicatorPrefab.ChangeFontTaktisColor(color == "blue" ? Color.blue : Color.red);
            HUDElement.Prefabs.IndicatorPrefab.ChangeEntityInfo(name);

            return HUD;
        }
    }
}
