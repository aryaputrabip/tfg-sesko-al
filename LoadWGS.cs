using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Data.Connect;
using GlobalVariables;
using HelperPlotting;
using HelperUser;
using Newtonsoft.Json.Linq;
using ObjectStat;
using Plot.Pasukan;
using UnityEngine;
using WGS.View;
using static ListOfSimpleFunctions.Functions;
using static Walker.Path;
public class LoadWGS : MonoBehaviour
{
    static LoadWGS instance;
    [SerializeField] [TextArea(15,20)] private string userJSON;
    [SerializeField] private bool debugUser;
    [SerializeField] private bool loadMusuh, showMusuh, showRadarDrawing; bool _showRadarDrawing, _showMusuh;
    [SerializeField] private string serviceLogin;
    [SerializeField] private string serviceCB;
    [SerializeField] private string serverColyseus;
    List<GameObject> enemies = new();
    [SerializeField] List<GameObject> radars = new();
    private EntityRouteController _route;

    void Awake()
    {
        if(instance == null) instance = this; 
        else Destroy(gameObject);
    }

    // Start is called before the first frame update
    async void Start()
    {
        _route = FindObjectOfType<EntityRouteController>();
        if(debugUser)
        await TestLogUser();
    }

    // Update is called once per frame
    void Update()
    {
        if(_showMusuh == showMusuh && (!_showMusuh || !showMusuh))
        {
            foreach(var enemy in enemies)
            {
                var enemyStat = enemy.GetComponent<StatV2>();
                if(enemyStat.containedAsEnemyInList <= 0) enemy.SetActive(false);
            }
            _showMusuh = !showMusuh;
        }
        else if(_showMusuh == showMusuh && (_showMusuh || showMusuh))
        {
            foreach(var enemy in enemies) enemy.SetActive(true);
            _showMusuh = !showMusuh;
        }

        if(_showRadarDrawing == showRadarDrawing && (_showRadarDrawing || showRadarDrawing))
        {
            foreach(var radar in radars) radar.GetComponent<DetailRadar>().radar2D.visible = true;
            _showRadarDrawing = !_showRadarDrawing;
        }
        else if(_showRadarDrawing == showRadarDrawing && (!_showRadarDrawing || !showRadarDrawing))
        {
            foreach(var radar in radars) radar.GetComponent<DetailRadar>().radar2D.visible = false;
            _showRadarDrawing = !_showRadarDrawing;
        }
        
        if(!showMusuh)
        {
            foreach(var radar in radars)
            {
                var detail             = GetComponent<StatV2>() == null ? radar.GetComponent<DetailRadar>() : null;
                var radarPolygon       = detail.radar2D as OnlineMapsDrawingPoly;

                // Debug.Log(radarPolygon.name);

                foreach(var enemy in enemies)
                {
                    var enemyStat     = enemy.GetComponent<StatV2>();
                    var isOnRadar     = OnlineMapsUtils.IsPointInPolygon(radarPolygon.points, enemyStat.pasukan.lng_x, enemyStat.pasukan.lat_y);
                    var showRadar     = detail.showRadar;
                    var enemiesInView = detail.enemiesInView;
                    var isContained   = enemiesInView.Contains(enemy);
                    
                    if(showRadar)
                    {
                        if(isOnRadar)
                        {
                            if(!isContained) { enemyStat.containedAsEnemyInList++; enemiesInView.Add(enemy); }
                            enemiesInView.Find(child => child == enemy).SetActive(true);
                        }
                        else
                        {
                            if(isContained) { 
                                if(enemyStat.containedAsEnemyInList <= 1) enemiesInView.Find(child => child == enemy).SetActive(false);
                                enemyStat.containedAsEnemyInList--;
                                enemiesInView.Remove(enemy); }
                        }
                    }
                    else
                    {
                        foreach(var child in enemiesInView)
                        {
                            var childStat = child.GetComponent<StatV2>();
                            var childMarker = childStat.markers.marker3D;
                            if(childStat.containedAsEnemyInList <= 1) child.SetActive(false);
                            childStat.containedAsEnemyInList--;
                        }
                        enemiesInView.Clear();
                    }
                }
            }
        }
    }

    async Task LoadListDataObject()
    {
        // Load List Pasukan Ke Dalam Plot Radar
        var list_pasukan = await API.Instance.GetListAlutsista("pasukan");
        await DataConnect.instance.GetListPasukan(list_pasukan);

        // Load List Radar Ke Dalam Plot Radar
        var list_radar = "";
        list_radar = await API.Instance.GetListAlutsista("radar");
        await DataConnect.instance.GetListRadar(list_radar);
        list_radar = await API.Instance.GetListAlutsista("data_radar_darat");
        await DataConnect.instance.GetListRadarDarat(list_radar);
        list_radar = await API.Instance.GetListAlutsista("data_radar_laut");
        await DataConnect.instance.GetListRadarLaut(list_radar);
        list_radar = await API.Instance.GetListAlutsista("data_radar_udara");
        await DataConnect.instance.GetListRadarUdara(list_radar);

        // await PlotPasukanControllerV2.instance.GrabList();
    }

    async Task TestLogUser()
    {
        User[] user = User.FromJson(userJSON);

        new SessionUser(user[0]);

        Config.SERVICE_LOGIN = (serviceLogin != null) ? serviceLogin : "http://localhost/eppkm/public";
        Config.SERVICE_CB = (serviceCB != null) ? serviceCB : "http://localhost/eppkm_simulasi";
        Config.SERVER_COLYSEUS = (serverColyseus != null) ? serverColyseus : "ws://localhost:2567";

        await API.Instance.GetSkenarioAktif();
        await API.Instance.GetCBTerbaik(SessionUser.id_bagian.ToString());

        await PrepareGameplay();
    }

    async Task PrepareGameplay()
    {
        if(SessionUser.id != 1)
        {
            await LoadListDataObject();
            await LoadDataCB(SessionUser.id.ToString());
        }
    }

    async Task LoadDataCB(string id_user = null)
    {   
        var plotSendiri = await API.Instance.LoadDataCB(false, id_user, CBSendiri.id_kogas, CBSendiri.nama_document);

        //Load Satuan
        await SpawnSatuan(setJArrayResult(plotSendiri, 0));
        //Load Rute Misi
        await SpawnRouteMisi(setJArrayResult(plotSendiri, 13));
        //Load Radar
        await SpawnEntityRadar(setJArrayResult(plotSendiri, 7));

        if(loadMusuh)
        {
            var plotMusuh = await API.Instance.LoadDataCB(true, "116", CBMusuh.id_kogas, CBMusuh.nama_document);
            //Load Satuan Musuh
            await SpawnSatuan(setJArrayResult(plotMusuh, 0), true);
        } 
    }

    async Task<bool> SpawnSatuan(JArray satuan, bool enemy = false)
    {
        if (isJArrayNull(satuan)) return false;
        if (satuan != null) if (satuan.Count == 0) return false;

        int indexLoop = (satuan != null) ? satuan.Count : 2;

        for(int i = 0; i < indexLoop; i++)
        {
            var entity = EntityPasukanV2.FromJson(satuan[i].ToString());
            var info   = entity.info;
            var style  = entity.style;
            
            GameObject prefab   = null;
            Transform parent    = null;
            var listRadar       = new List<ListRadarSatuan>();
            var tipe_tni        = "";
            var matraPas        = "";
            switch(style.grup)
            {
                case 1:
                    prefab    = getAssetSatuan(info.nama_satuan, entity.object3D, "vehicle");
                    parent    = CreateMarkerParent("Angkatan Darat");
                    matraPas  = "Darat";
                    tipe_tni  = "AD";
                    listRadar = DataConnect.instance.radarList.listRadarSatuanDarat;
                    break;
                case 2:
                    prefab    = getAssetSatuan(info.nama_satuan, entity.object3D, "ship");
                    parent    = CreateMarkerParent("Angkatan Laut");
                    matraPas  = "Laut";
                    tipe_tni  = "AL";
                    listRadar = DataConnect.instance.radarList.listRadarSatuanLaut;
                    break;
                case 3:
                    prefab    = getAssetSatuan(info.nama_satuan, entity.object3D, "aircraft");
                    parent    = CreateMarkerParent("Angkatan Udara");
                    matraPas  = "Udara";
                    tipe_tni  = "AU";
                    listRadar = DataConnect.instance.radarList.listRadarSatuanUdara;
                    break;
                case 10:
                    prefab    = AssetPackageManager.Instance.defaultPasukan;
                    parent    = CreateMarkerParent("Pasukan");
                    matraPas  = DataConnect.instance.pasukanList.listPasukan.Find(child => child.id == entity.id_symbol).matra_pasukan;
                    tipe_tni  = matraPas == "Darat" ? "AD" : matraPas == "Laut" ? "AL" : "AU";
                    break;
            }
            
            Debug.Log(satuan[i].ToString());
            Debug.Log(entity.nama);
            var marker                = Create3D(entity.lng_x, entity.lat_y, prefab);
            marker["markerSource"]    = marker;
            marker.label              = info.nama_satuan;
            marker["data"]            = new ShowMarkerLabelsByZoomItem(marker.label, new OnlineMapsRange(1, 19));
            marker.sizeType           = OnlineMapsMarker3D.SizeType.meters;
            marker.rotationY          = int.Parse(info.heading);
            marker.instance.name      = entity.nama;
            marker.transform.tag      = enemy == false ? "entity-satuan" : "entity-musuh"; if(enemy) {enemies.Add(marker.instance); marker.instance.SetActive(false);}
            marker.transform.parent   = enemy ? CreateMarkerParent("Satuan Musuh") : parent;
            marker.checkMapBoundaries = marker.instance.tag != "entity-musuh";
            marker.OnClick            += ViewSatuanController.instance.OnMarkerClick;

            Destroy(marker.instance.GetComponent<Stat>());
            Destroy(marker.instance.GetComponent<PFormasi>());
            
            var pas              = marker.instance.AddComponent<StatV2>();
            pas.pasukan          = entity;
            pas.pasukan.tipe_tni = tipe_tni;
            pas.pasukan.matra    = matraPas;
            pas.isEnemy          = enemy;
            pas.json             = new()
            {
                String = EntityPasukanV2.ToString(pas.pasukan)
            };
            pas.markers          = new()
            {
                marker3D = marker
            };
            
            var removeChar    = (char)92;
            pas.json.String   = pas.json.String.Replace(removeChar.ToString(), "");

            removeChar        = (char)34;
            pas.json.String   = pas.json.String.Replace(removeChar.ToString() + "[", "[");
            pas.json.String   = pas.json.String.Replace("]" + removeChar.ToString(), "]");
            
            if(marker.transform.tag != "entity-musuh")
            {
                foreach(var child in listRadar)
                {
                    var childID = child.AIRCRAFT_ID != null ? child.AIRCRAFT_ID : child.SHIP_ID != null ? child.SHIP_ID : child.VEHICLE_ID;
                    
                    if(pas.pasukan.id_symbol == childID)
                    {
                        Debug.Log(pas.pasukan.id_symbol + "_radar_" + childID);
                        pas.radars = child.RADARS;

                        for(int j = 0; j < child.RADARS.Count; j++)
                        {
                            var _child = child.RADARS[j];
                            var radar = Instantiate(AssetPackageManager.Instance.prefabRadar, marker.transform);
                            radar.transform.localScale *= _child.RADAR_DET_RANGE * 1000;
                            radar.name = "radar3D_" + marker.instance.name + "_" + _child.RADAR_NAME;
                            radar.transform.Find("Sphere").gameObject.SetActive(false);
                            
                            var detail = radar.AddComponent<DetailRadar>();
                            detail.radar = new()
                            {
                                id = string.Empty,
                                id_user = SessionUser.id.ToString(),
                                dokumen = pas.pasukan.dokumen,
                                nama = radar.name,
                                lat = marker.position.y,
                                lng = marker.position.x,
                                info_radar = new()
                                {
                                    nama = radar.name,
                                    judul = _child.RADAR_NAME,
                                    radius = (int)_child.RADAR_DET_RANGE,
                                    size = 30,
                                    warna = "Blue",
                                    id_user = pas.pasukan.id_user,
                                    id_symbol = new()
                                    {
                                        id = "635",
                                        nama = "TAKTIS_GABUNGAN",
                                        index = "163",
                                        keterangan = "Stasion Radar.",
                                        grup = "4",
                                        entity_name = string.Empty,
                                        entity_kategori = string.Empty,
                                        jenis_role = string.Empty,
                                        health = null
                                    },
                                    jenis_radar = "Biasa",
                                    jml_peluru = "0",
                                    new_index = "163",
                                    armor = 500
                                },
                                symbol = new()
                                {
                                    className = "my-div-icon",
                                    html = "<div style='margin-top:-15px; width: 30px;color: blue; font-size: 30px; font-family: TAKTIS_GABUNGAN'>£</div>",
                                    iconSize = new(){20, 20},
                                    iconAnchor = new(){20, 5},
                                    id_point = radar.name,
                                    id_radar = radar.name
                                },
                                info_symbol = string.Empty
                            };
                            detail.json = RadarEntity.ToString(detail.radar);
                            pas.radars3D.Add(radar);
                            radars.Add(radar);
                            
                            var color = SessionUser.nama_asisten == "ASOPS" ? Color.blue : Color.red;
                            color.a = .5f;

                            var points  = ArrowManeuver.CircleNormal(marker.position.x, marker.position.y, _child.RADAR_DET_RANGE / 2);
                            var radar2D = AddElement(DrawPoly(points, Color.blue, 1f));
                            radar2D.radiusKM = _child.RADAR_DET_RANGE / 2;
                            radar2D.DrawOnTileset(OnlineMapsTileSetControl.instance, OnlineMapsDrawingElementManager.instance.Count - 1);
                            radar2D.instance.transform.localPosition = Vector3.up;
                            radar2D.name    = "radar2D_" + marker.instance.name + "_" + _child.RADAR_NAME;
                            radar2D.visible = false;
                            radar2D.instance.transform.parent = marker.transform.parent;
                            pas.radars2D.Add(radar2D);
                            detail.radar2D  = radar2D;
                        }
                    }
                }
            }
            
            //Set HUD untuk marker
            SetHUD(marker, info.nama_satuan, info.warna, style.nama, style.index);
        }

        return true;
    }
    
    private GameObject getAssetSatuan(string nama_satuan, string nama_obj3D, string jenis_satuan)
    {
        Debug.Log(nama_obj3D + " | " + jenis_satuan);
        // Dapatkan Asset 3D Satuan dari Asset Package Berdasarkan Nama Satuan atau Nama Object 3D Satuannya
        int index = AssetPackageManager.Instance.ASSETS_NAME.IndexOf(nama_satuan);
        if(index == -1) index = AssetPackageManager.Instance.ASSETS_NAME.IndexOf(nama_obj3D);

        // Jika Tidak Terdapat Asset 3D dalam Asset Package, Gunakan Asset Default Berdasarkan Jenis Satuannya
        if (index == -1) return jenis_satuan == "vehicle" ? AssetPackageManager.Instance.defaultVehicle :
                                jenis_satuan == "ship" ? AssetPackageManager.Instance.defaultShip :
                                jenis_satuan == "aircraft" ? AssetPackageManager.Instance.defaultAircraft :
                                jenis_satuan == "pasukan" ? AssetPackageManager.Instance.defaultPasukan :
                                AssetPackageManager.Instance.defaultVehicle;

        return AssetPackageManager.Instance.ASSETS[index];
    }
    

    public async Task<bool> SpawnRouteMisi(JArray arrayMisi, string id_user = null)
    {
        if (arrayMisi.Count == 0) return false;
        
        // Add Rute Per-Misi
        for (int i = 0; i < arrayMisi.Count; i++)
        {
            var misi = EntityMisi.FromJson(arrayMisi[i].ToString());
            
            // Set Property Misi (Jump to Next Index if It's Null)
            if (misi.properties == null) continue;
            var misi_properties = misi.properties;

            if (misi_properties.jalur == null) continue;

            // Create Route Path Points
            List<Vector3> paths = new List<Vector3>();
            double jarak_jalur = 0;

            for (int j = 0; j < misi_properties.jalur.Count; j++)
            {
                paths.Add(new Vector3(misi_properties.jalur[j].lng, misi_properties.jalur[j].lat, (float)misi_properties.jalur[j].alt));
                if(j > 0)
                {
                    jarak_jalur += OnlineMapsUtils.DistanceBetweenPointsD(new Vector2(misi_properties.jalur[j - 1].lng, misi_properties.jalur[j - 1].lat), new Vector2(misi_properties.jalur[j].lng, misi_properties.jalur[j].lat));
                }
            }
            Debug.Log(misi.id_object + " misi: " + misi.id_mission);
            var distance1 = (float)GetDistance(paths);
            var menit = RouteController.estimateTimeFMin(distance1, misi_properties.kecepatan, misi_properties.type);
            Debug.Log("Distance: " + distance1 + " Kecepatan: " + RouteController.GetKecepatanByType(misi_properties.kecepatan, misi_properties.type) + "|Kilometer");

            paths = SmoothPath(paths);
            var distance2 = (float)GetDistance(paths);
            var speedAfter = (float)(distance2 / menit * 60);
            Debug.Log("New Distance: " + distance2 + " New Kecepatan: " + speedAfter + "|Kilometer");

            // Set Rute Misi Ke Satuannya
            var newRoute = await _route.AddNewRoute(paths, misi, misi_properties, id_user, speedAfter);

            // Buat atau Gunakan Container Drawing Route Untuk Menyimpan Data Drawing Berdasarkan ID Usernya
            // (Disimpan per-user agar ketika user tersebut logout seluruh data drawing user tersebut dapat terhapus)
            MetadataDrawRoute newDrawRoute = null;
            var objDrawRoutes = _route.drawRouteContainer.Find((id_user) == null ? SessionUser.id.ToString() : id_user);
            if (objDrawRoutes == null)
            {
                newDrawRoute = Instantiate(_route.prefabDrawRoute, _route.drawRouteContainer);
                newDrawRoute.name = (id_user == null) ? SessionUser.id.ToString() : id_user;
            }
            else
            {
                newDrawRoute = objDrawRoutes.GetComponent<MetadataDrawRoute>();
            }
        }

        return true;
    }
    
    public async Task<bool> SpawnEntityRadar(JArray arrayRadar)
    {
        var assetPackageManager = AssetPackageManager.Instance;

        if (arrayRadar == null) return false;
        for (int i = 0; i < arrayRadar.Count; i++)
        {
            Debug.Log("Spawn Radar " + i + "\n" + arrayRadar[i].ToString());
            var radar = RadarEntity.FromJson(arrayRadar[i].ToString());
            var info_radar = radar.info_radar;
            var objPosition = new Vector2(radar.lng, radar.lat);

            // Create Marker 3D
            var marker3D = Create3D(objPosition, assetPackageManager.prefabRadar);
            marker3D.instance.name = radar.nama;
            marker3D.instance.tag = "entity-radar";
            marker3D.instance.transform.parent = CreateMarkerParent("Radars");

            // Add & Set Radar Properties
            var detail = marker3D.instance.gameObject.AddComponent<DetailRadar>();
            detail.radar = radar;
            detail.json = RadarEntity.ToString(radar);

            var color = SessionUser.nama_asisten == "ASOPS" ? Color.blue : Color.red;
            color.a = .5f;

            // Draw 2D Radar Circle
            var points  = new ArrowManeuver().DrawCircle(objPosition, info_radar.radius / 1000);
            var element = AddElement(DrawPoly(points, Color.blue, 1f));
            element.DrawOnTileset(OnlineMapsTileSetControl.instance, OnlineMapsDrawingElementManager.instance.Count - 1);
            element.instance.transform.parent = CreateMarkerParent("Radars");
            element.instance.transform.position = Vector3.up;
            element.name = "draw_" + radar.nama;
            element.visible = false;
            detail.showRadar = true;
            detail.radar2D = element;
            radars.Add(marker3D.instance);

            // Set Ukuran Radar
            marker3D.sizeType = OnlineMapsMarker3D.SizeType.meters;
            marker3D.scale = info_radar.radius * 2;
            
            //Set HUD untuk marker
            SetHUD(marker3D, info_radar.judul, info_radar.warna, info_radar.id_symbol.nama, info_radar.id_symbol.index);
        }

        return true;
    }
    
    public JArray setJArrayResult(JArray arrayResult, int index)
    {
        if (arrayResult[index].ToString() != "False")
        {
            return JArray.Parse(arrayResult[index].ToString());
        }
        else
        {
            return null;
        }
    }
    
    public bool isJArrayNull(JArray array)
    {
        if (array == null)
        {
            return true;
        }
        else
        {
            if (array.Count == 0) return true;
        }

        return false;
    }
}
