using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using ObjectStat;
using GlobalVariables;

namespace Plot.InputHandler
{
    public class MultipleSelect : MonoBehaviour
    {
        public static MultipleSelect instance;

        [Header ("OnlineMaps")]
        [Space(5)]
        public Transform map;
        public OnlineMaps onlineMaps;

        [Header ("Canvas ScrollView")]
        [Space(5)]
        [SerializeField] SidemenuToggler toggler;
        public GameObject scrollView;
        public GameObject selectionPrefab;
        public GameObject content;
        public GameObject selectMenu;
        public Image selectImage;
        public Image selectBGText;

        [Header ("Canvas Unit Setting")]
        [Space(5)]
        public GameObject canvasUS;
        public TMP_InputField user;
        public TMP_InputField nama;
        public TMP_Dropdown unitType;
        public TMP_InputField keterangan;
        public TMP_InputField lng;
        public TMP_InputField lat;
        public Button edit;
        public Button move;
        public Button delete;
        public Button close;

        [Header ("Marker Edit")]
        [Space(5)]
        OnlineMapsMarker3D edit2D;
        OnlineMapsMarker3D edit3D;

        [Header("Lain - Lain")]
        [Space(5)]
        public bool canMultiselect = false;
        public List<Transform> selectedUnits = new List<Transform>();
        int u = 0;
        bool isDragging = false;
        Vector3 mousePos;
        RaycastHit hit;
        Vector2 prevWorldPos;
        Vector2 newWorldPos;
        bool btnPressed = false;
        bool isMoving = false;
        bool isDragMove = false;

        void Awake()
        {
            if(instance == null)
            instance = this;
            else
            Destroy(gameObject);
        }

        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
            if(!btnPressed)
            {
                if(Input.GetKeyDown(KeyCode.LeftControl))
                {
                    canMultiselect = true;
                    onlineMaps.blockAllInteractions = true;
                    Debug.Log("MultiSelect Sedang Dilakukan...");
                }

                if(Input.GetKeyUp(KeyCode.LeftControl))
                {
                    isDragging = false;
                    canMultiselect = false;
                    onlineMaps.blockAllInteractions = false;
                    Debug.Log("MultiSelect Selesai...");
                }
            }

            for(var i = selectedUnits.Count - 1; i > -1; i--)
            {
                if (selectedUnits[i] == null || !selectedUnits[i].gameObject.activeSelf)
                {
                    selectedUnits.RemoveAt(i);
                    u = 0;
                }
            }

            if(canMultiselect)
            {
                if(Input.GetMouseButtonDown(0))
                {
                    mousePos = Input.mousePosition;

                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);        

                    if(Physics.Raycast(ray, out hit))
                    {
                        LayerMask layerHit = hit.transform.gameObject.layer;

                        switch (layerHit.value)
                        {
                            case 5:
                                break;
                            case 7:
                                if(!selectedUnits.Contains(hit.transform))
                                SelectUnit(hit.transform);
                                break;
                            default:
                                isDragging = true;
                                break;
                        }
                    }        
                }

                if(Input.GetMouseButtonUp(0))
                {
                    foreach(Transform child in map)
                    {
                        if(child.name == "3D Markers" || child.name == "Markers" || child.name == "Marker 2D")
                        foreach(Transform units in child)
                        {
                            if(units.gameObject.activeSelf == true)
                            if(units.gameObject.layer == LayerMask.NameToLayer("PlayerUnit"))
                            if(IsWithinSelectionBounds(units))
                            if(!selectedUnits.Contains(units))
                            SelectUnit(units);
                        }
                    }

                    isDragging = false;
                }    
            }

            if(u < selectedUnits.Count)
            {
                foreach(Transform child in content.transform)
                GameObject.Destroy(child.gameObject);

                for(int i = 0; i < selectedUnits.Count; i++)
                {
                    var pF = selectedUnits[i].GetComponent<PFormasiV2>();

                    selectImage.enabled = false;
                    selectBGText.gameObject.SetActive(true);

                    selectBGText.transform.Find("Text (Legacy)").GetComponent<Text>().text = ((char)int.Parse(pF.index)).ToString();
                    selectBGText.transform.Find("Text (Legacy)").GetComponent<Text>().font = Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + pF.fontTaktis);

                    selectBGText.transform.Find("Text (Legacy)").GetComponent<Text>().color = SessionUser.nama_asisten == "ASOPS" ? Color.blue : Color.red;
                    
                    GameObject copy = Instantiate(selectMenu);
                    copy.SetActive(true);

                    // Image copyImage = copy.transform.Find("Image").GetComponent<Image>();
                    Button butSetting = copy.transform.Find("Setting").GetComponent<Button>();
                    Button delSetting = copy.transform.Find("Delete").GetComponent<Button>();
                    
                    butSetting.onClick.AddListener(delegate {UnitSetting(pF, copy);});
                    // delSetting.onClick.AddListener(delegate {Remove(pForm, copy);});
                
                    copy.transform.SetParent(content.transform);
                    copy.transform.localScale = selectMenu.transform.localScale;
                    copy.transform.localPosition = Vector3.zero;
                }
                u++;
            }
            else if(u > selectedUnits.Count)
            {
                u = 0;
            }

            if(selectedUnits.Count > 0)
            scrollView.SetActive(true);
            else if(selectedUnits.Count == 0) 
            {
                foreach(Transform child in content.transform)
                GameObject.Destroy(child.gameObject);
                
                u = 0;
                scrollView.SetActive(false);
            }

            Move();
        }

        public void OnBtnPressed()
        {
            btnPressed = true;
            canMultiselect = true;
            onlineMaps.blockAllInteractions = true;
        }

        public void OnBtnReleased()
        {
            btnPressed = false;
            isDragging = false;
            onlineMaps.blockAllInteractions = false;
            canMultiselect = false;
        }
        
        private void OnGUI()
        {
            if (isDragging)
            {
                Rect rect = DragGUI.GetScreenRect(mousePos, Input.mousePosition);
                DragGUI.DrawScreenRect(rect, new Color(0f, 0f, 0f, 0.25f));
                DragGUI.DrawScreenRectBorder(rect, 3, Color.blue);
            }
        }

        public void SelectUnit(Transform unit)
        {
            if(!canMultiselect)
            DeselectUnit();

            selectedUnits.Add(unit);
            var select = Instantiate(selectionPrefab, unit);
        }

        public void DeselectUnit()
        {
            for (int i = 0; i < selectedUnits.Count; i++)
            {
                GameObject.Destroy(selectedUnits[i].Find("Selection(Clone)").gameObject);
            }

            selectedUnits.Clear();
        }
        
        private bool HaveSelectedUnit()
        {
            if (selectedUnits.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsWithinSelectionBounds(Transform tf)
        {
            if(!isDragging)
            {
                return false;
            }

            Camera cam = Camera.main;
            Bounds vpBounds = DragGUI.GetVPBounds(cam, mousePos, Input.mousePosition);
            return vpBounds.Contains(cam.WorldToViewportPoint(tf.position));
        }

        public void UnitSetting(PFormasiV2 pForm, GameObject multiObjOnList)
        {
            toggler.toggleOff();
            canvasUS.SetActive(true);
            canvasUS.GetComponent<Animator>().Play("SlideIn");

            user.text = SessionUser.id.ToString();
            nama.text = pForm.namaSatuan;

            keterangan.text = pForm.keterangan;
            lng.text = pForm.markers.marker3D.position.x.ToString();
            lat.text = pForm.markers.marker3D.position.y.ToString();

            edit3D = pForm.markers.marker3D;

            // edit.onClick.RemoveAllListeners();
            // edit.onClick.AddListener(delegate {Edit(pForm);});

            // move.onClick.RemoveAllListeners();
            // move.onClick.AddListener(delegate {MoveOn(pForm);});

            // delete.onClick.RemoveAllListeners();
            // delete.onClick.AddListener(delegate {Delete(pForm, multiObjOnList);});
            
            // close.onClick.RemoveAllListeners();
            // close.onClick.AddListener(delegate {CloseCanvasUS(pForm);});
        }

        public void Remove(PFormasi pForm, GameObject multiObjOnList)
        {
            GameObject.Destroy(multiObjOnList);
            selectedUnits.Remove(pForm.transform);
        }

        public void Edit(PFormasi pForm)
        {
            if(nama.text == "")
            Debug.Log("Nama Tidak Boleh Kosong!!!");
            else if(keterangan.text == "")
            Debug.Log("Keterangan Tidak Boleh Kosong");
            else
            {
                if(pForm.stat != null)
                {
                    Stat oS = pForm.stat.marker2D.instance.GetComponent<Stat>();
                    Stat oSS = pForm.stat.marker3D.instance.GetComponent<Stat>();

                    oS.nama = nama.text;
                    oS.keterangan = keterangan.text;

                    oSS.nama = oS.nama;
                    oSS.keterangan = oS.keterangan;

                    // pForm.marker2D.label = oS.nama;
                }
                else
                {
                    JSONDataSatuan jStat = pForm.jStat;
                    jStat.nama = nama.text;
                    jStat.deskripsi = keterangan.text;
                }
                // pForm.marker3D.label = nama.text;

                CloseCanvasUS(pForm);
            }
        }

        public void MoveOn(PFormasi pForm)
        {
            isMoving = !isMoving;

            // onlineMaps.blockAllInteractions = !onlineMaps.blockAllInteractions;

            if(isMoving)
            {
                Debug.Log("Pemindahan Unit Sedang Dilakukan, Anda Tidak Dapat Berinteraksi Dengan Map Selama Pemindahan Berlangsung....");
                
                if(pForm.stat != null)
                {
                    edit2D.OnPress += OnMarkerPress;
                    edit2D.OnRelease += OnMarkerRelease;
                }

                edit3D.OnPress += OnMarkerPress;
                edit3D.OnRelease += OnMarkerRelease;
            }
            else
            {
                Debug.Log("Pemindahan Selesai");
                
                if(pForm.stat != null)
                {
                    edit2D.OnPress -= OnMarkerPress;
                    edit2D.OnRelease -= OnMarkerRelease;
                }

                edit3D.OnPress -= OnMarkerPress;
                edit3D.OnRelease -= OnMarkerRelease;
            }
        }

        public void Move()
        {
            if(isMoving)
            {
                if(isDragMove)
                {
                    double lng, lat;
                    if(OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
                    newWorldPos = new Vector2((float)lng, (float)lat);
                    
                    if(newWorldPos != prevWorldPos)
                    {
                        if(edit2D != null)
                        edit2D.position = newWorldPos;
                        edit3D.position = newWorldPos;
                    }
                }
            }
        }

        public void OnMarkerPress(OnlineMapsMarkerBase markerBase)
        {
            OnlineMapsMarker3D marker3D = markerBase as OnlineMapsMarker3D;

            if(isMoving)
            {
                OnlineMaps.instance.GetComponent<OnlineMapsTileSetControl>().allowUserControl = false;
                isDragMove = true;

                double lng, lat;
                if(OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
                prevWorldPos = new Vector2((float)lng, (float)lat);
            }
        }

        public void OnMarkerRelease(OnlineMapsMarkerBase markerBase)
        {
            OnlineMapsMarker3D marker3D = markerBase as OnlineMapsMarker3D;

            if(isMoving)
            {
                OnlineMaps.instance.GetComponent<OnlineMapsTileSetControl>().allowUserControl = true;
                isDragMove = false;
            }
        }

        public void Delete(PFormasi pForm, GameObject multiObjOnList)
        {
            CloseCanvasUS(pForm);

            GameObject.Destroy(multiObjOnList);

            selectedUnits.Remove(pForm.transform);
            
            if(pForm.stat != null)
            OnlineMapsMarker3DManager.RemoveItem(edit2D);
            OnlineMapsMarker3DManager.RemoveItem(edit3D);
        }

        public void CloseCanvasUS(PFormasi pForm)
        {
            toggler.toggleOn();
            canvasUS.GetComponent<Animator>().Play("SlideOut");
            
            onlineMaps.blockAllInteractions = false;

            if(isMoving)
            Debug.Log("Pemindahan Selesai");

            isMoving = false;
        }
    }
}
