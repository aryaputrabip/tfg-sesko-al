using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ObjectStat;
using Plot.Pasukan;
using UnityEngine;
using UnityEngine.UI;

namespace Plot.InputHandler
{
    public class MultipleSelectV2 : MonoBehaviour
    {
        public static MultipleSelectV2 instance;

        [Header("Canvas")]
        [Space(5)]
        public CanvasMultiSelect canvas;
        
        [Header("List")]
        [Space(5)]
        public GameObject prefabList;
        public List<Transform> selectedUnits = new();
        List<GameObject> contentList = new();
        int u;

        [Header("Vectors")]
        [Space(5)]
        Vector3 mousePos;

        [Header("Bool")]
        [Space(5)]
        bool isSelecting;
        bool isDragging;

        [Header("Others")]
        [Space(5)]
        RaycastHit hit;

        void Awake()
        {
            if(instance != null) Destroy(gameObject);
            else instance = this;
        }

        void Update()
        {
            MultipleSelect();
        }

        void MultipleSelect()
        {
            if(isSelecting)
            {
                if(Input.GetMouseButtonDown(0))
                {
                    mousePos = Input.mousePosition;

                    Ray ray = Camera.main.ScreenPointToRay(mousePos);

                    if (Physics.Raycast(ray, out hit))
                    {
                        var unitHit = hit.transform;
                        var layerMask = unitHit.gameObject.layer;
                        
                        switch (layerMask)
                        {
                            case 7:
                                if (!selectedUnits.Contains(unitHit))
                                    SelectUnit(unitHit, true);
                                break;
                            default:
                                isDragging = true;
                                break;
                        }
                    }
                }

                if(Input.GetMouseButtonUp(0))
                {
                    foreach(OnlineMapsMarker3D child in OnlineMapsMarker3DManager.instance.Cast<OnlineMapsMarker3D>())
                    {
                        var units = child.instance.transform;
                        var pF = units.gameObject.GetComponent<PFormasiV2>() ?? null;
                        if (pF != null)
                        {
                            if (IsWithinSelectionBounds(units))
                            if (units.gameObject.activeSelf == true)
                                SelectUnit(units, true);
                        }
                    }

                    isDragging = false;
                }
            }

            if(u < selectedUnits.Count)
            {
                for(var i = 0; i < selectedUnits.Count; i++)
                {
                    var unit = selectedUnits[i];
                    var pF = unit.GetComponent<PFormasiV2>();

                    var copy = Instantiate(prefabList, canvas.content);
                    Component FindPara<Component>(string param) => copy.GetComponent<Metadata>().FindParameter(param).parameter.GetComponent<Component>();
                    FindPara<Image>("Icon").sprite = pF.stat != null ? pF.stat.image : pF.jStat.icon;
                    FindPara<Button>("Setting").onClick.AddListener(delegate{UnitSetting(pF.markers.marker3D, pF.stat);});
                    FindPara<Button>("Delete").onClick.AddListener(delegate{Remove(unit, copy);});

                    contentList.Add(copy);
                    u++;
                }
            }
            else if (u > selectedUnits.Count)
            {
                DestroyContent();
                u = 0;
            }
            
            if(u == 0) canvas.canvas.SetActive(false);
            else canvas.canvas.SetActive(true);

            if(selectedUnits.Count == 0)
            DestroyContent();
        }
        
        private bool IsWithinSelectionBounds(Transform tf)
        {
            if (!isDragging)
            {
                return false;
            }

            Camera cam = Camera.main;
            Bounds vpBounds = DragGUI.GetVPBounds(cam, mousePos, Input.mousePosition);
            return vpBounds.Contains(cam.WorldToViewportPoint(tf.position));
        }
        
        private void OnGUI()
        {
            if (isDragging)
            {
                Rect rect = DragGUI.GetScreenRect(mousePos, Input.mousePosition);
                DragGUI.DrawScreenRect(rect, new Color(0f, 0f, 0f, 0.25f));
                DragGUI.DrawScreenRectBorder(rect, 3, Color.blue);
            }
        }

        void SelectUnit(Transform unit, bool canMultiSelect)
        {
            if(!canMultiSelect) DeselectUnit();

            if(!selectedUnits.Contains(unit)) selectedUnits.Add(unit);
        }

        void DeselectUnit()
        {
            selectedUnits.Clear();
            DestroyContent();
        }

        void DestroyContent()
        {
            foreach(var item in contentList)
                Destroy(item);
            contentList.Clear();
        }
        
        void UnitSetting(OnlineMapsMarker3D marker, bool isPas)
        {
            if(isPas) PlotPasukanControllerV2.instance.OnMarkerClick(marker);
            else PlotSatuanController.Instance.EditObject(marker);
        }

        void Remove(Transform unit, GameObject unitInList)
        {
            selectedUnits.Remove(unit);
            Destroy(unitInList);
        }

        public void SelectingToggle(bool toggle)
        {
            isSelecting = toggle;
            OnlineMapsTileSetControl.instance.allowUserControl = !toggle;
        }

        public void Clear()
        {
            DeselectUnit();
        }
    }
}

[Serializable]
public class CanvasMultiSelect
{
    [Header("Canvas")]
    public GameObject canvas;
    public Transform content;

    [Header("Toggler")]
    public SidemenuToggler toggler;
}