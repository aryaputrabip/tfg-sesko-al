using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace HelperPlotting
{
    [Serializable]
    public class InfoObstacle
    {
        [JsonIgnore] [SerializeField] string _nama;
        [JsonIgnore] [SerializeField] string _font;
        [JsonIgnore] [SerializeField] string _warna;
        [JsonIgnore] [SerializeField] int _size;
        [JsonIgnore] [SerializeField] string _info;
        [JsonIgnore] [SerializeField] string _index;
        [JsonIgnore] [SerializeField] int _id_user;
        public string nama { get{ return _nama; } set{ _nama = value; } }
        public string font { get{ return _font; } set{ _font = value; } }
        public string warna { get{ return _warna; } set{ _warna = value; } }
        public int size { get{ return _size; } set{ _size = value; } }
        public string info { get{ return _info; } set{ _info = value; } }
        public string index { get{ return _index; } set{ _index = value; } }
        public int id_user { get{ return _id_user; } set{ _id_user = value; } }
        
        public static InfoObstacle FromJson(string json) => JsonConvert.DeserializeObject<InfoObstacle>(json);
        public static string ToString(InfoObstacle json) => JsonConvert.SerializeObject(json);
    }

    [Serializable]
    public class EntityObstacleV2
    {
        [JsonIgnore] [SerializeField] int _id;
        [JsonIgnore] [SerializeField] int _id_user;
        [JsonIgnore] [SerializeField] string _dokumen;
        [JsonIgnore] [SerializeField] string _nama;
        [JsonIgnore] [SerializeField] float _lat_y;
        [JsonIgnore] [SerializeField] float _lng_x;
        [JsonIgnore] [SerializeField] InfoObstacle _info_obstacle;
        [JsonIgnore] [SerializeField] Symbol _symbol;
        public int id { get{ return _id; } set{ _id = value; } }
        public int id_user { get{ return _id_user; } set{ _id_user = value; } }
        public string dokumen { get{ return _dokumen; } set{ _dokumen = value; } }
        public string nama { get{ return _nama; } set{ _nama = value; } }
        public float lat_y { get{ return _lat_y; } set{ _lat_y = value; } }
        public float lng_x { get{ return _lng_x; } set{ _lng_x = value; } }
        public InfoObstacle info_obstacle { get{ return _info_obstacle; } set{ _info_obstacle = value; } }
        public Symbol symbol { get{ return _symbol; } set{ _symbol = value; } }

        [JsonConstructor]
        public EntityObstacleV2(string info_obstacle, string symbol)
        {
            this.info_obstacle = InfoObstacle.FromJson(info_obstacle);
            this.symbol = Symbol.FromJson(symbol);
        }
        public EntityObstacleV2(){}
        
        public static EntityObstacleV2 FromJson(string json) => JsonConvert.DeserializeObject<EntityObstacleV2>(json);
        public static string ToString(EntityObstacleV2 json) => JsonConvert.SerializeObject(json);
    }

    [Serializable]
    public class Symbol
    {
        [JsonIgnore] [SerializeField] string _className;
        [JsonIgnore] [SerializeField] string _html;
        [JsonIgnore] [SerializeField] object _iconSize;
        [JsonIgnore] [SerializeField] string _id_point;
        public string className { get{ return _className; } set{ _className = value; } }
        public string html { get{ return _html; } set{ _html = value; } }
        public object iconSize { get{ return _iconSize; } set{ _iconSize = value; } }
        public string id_point { get{ return _id_point; } set{ _id_point = value; } }
        
        public static Symbol FromJson(string json) => JsonConvert.DeserializeObject<Symbol>(json);
        public static string ToString(Symbol json) => JsonConvert.SerializeObject(json);
    }
}
