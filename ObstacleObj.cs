using System.Collections;
using System.Collections.Generic;
using HelperPlotting;
using UnityEngine;

namespace Plot.Obstacle
{
    public class ObstacleObj : MonoBehaviour
    {
        public List<EntityObstacleV2> obstacleList;
        public OnlineMapsDrawingElement element;
    }
}