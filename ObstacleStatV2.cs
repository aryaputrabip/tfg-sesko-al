using System.Collections;
using System.Collections.Generic;
using HelperPlotting;
using UnityEngine;

public class ObstacleStatV2 : MonoBehaviour
{
    [Header("Entity")]
    [Space(5)]
    public EntityObstacleV2 entity;
    
    [Header("Polygon")]
    [Space(5)]
    public string polygon;
    
    [Header("Index")]
    [Space(5)]
    public int index;
}
