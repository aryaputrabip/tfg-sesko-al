using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using HelperPlotting;

namespace ObjectStat
{
    public class PFormasi : MonoBehaviour
    {
        public Stat stat;
        public JSONDataSatuan jStat;
        public JObject jStatStyle;
        public JObject jStatInfo;

        public enum satuanJarak
        {
            Nautical_Miles,
            Kilometer,
            Feet,
            Yard,
            Meter
        }

        public enum formasiType
        {
            None,
            Belah_Ketupat,
            Flank_Kiri,
            Flank_Kanan,
            Sejajar,
            Serial,
            Panah,
            Kerucut,
            Custom
        }

        [Header("Formasi")]
        public string nama = "";
        public string namaFormasi;
        public string labelFormasi;
        public int urutanFormasi;
        public float jarak;
        public float sudut;
        public satuanJarak satuan;
        public formasiType formasi;
        public List<Transform> listObjekFormasi;
        public List<Vector2> routePoints;
        public string info;

        [Header("Marker")]
        public OnlineMapsMarker3D marker2D, marker3D;

        void Start()
        {
            if (GetComponent<JSONDataSatuan>() != null)
            {
                jStat = GetComponent<JSONDataSatuan>();
                jStatStyle = JObject.Parse(jStat.style);
                jStatInfo = JObject.Parse(jStat.info);
                marker3D = jStat.marker;
            }
            else
            {
                stat = GetComponent<Stat>();
                marker2D = stat.marker2D;
                marker3D = stat.marker3D;
            }
        }
    }
}