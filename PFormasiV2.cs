using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HelperPlotting;
using System;

namespace ObjectStat
{
    public class PFormasiV2 : MonoBehaviour
    {
        [Header("Formasi")]
        [Space(5)]
        public FormasiSatuan formasi = new();

        [Header("Satuan")]
        [Space(5)]
        public string namaSatuan;
        public string fontTaktis;
        public string index;
        public string keterangan;
        public int grup;
        [HideInInspector] public List<LabelStylePasukan> labelStylePasukan;
        [HideInInspector] public StatV2 stat;
        [HideInInspector] public JSONDataSatuan jStat;
        public Markers markers = new();

        [Header("Misi")]
        [Space(5)]
        public List<Vector2> routePoints = new();

        void Awake()
        {
            stat = GetComponent<StatV2>();
            jStat = GetComponent<JSONDataSatuan>();

            var infoJ = new InfoPasukan();
            var styleJ = new StylePasukan();

            if(jStat != null)
            {
                infoJ = InfoPasukan.FromJson(jStat.info);
                styleJ = StylePasukan.FromJson(jStat.style);
            }

            namaSatuan = stat != null ? stat.pasukan.info.nama_satuan : infoJ.nama_satuan;
            fontTaktis = stat != null ? stat.pasukan.style.nama : styleJ.nama;
            index = stat != null ? stat.pasukan.style.index : styleJ.index;
            keterangan = stat != null ? stat.pasukan.style.keterangan : styleJ.keterangan;
            grup = stat != null ? stat.pasukan.style.grup : styleJ.grup;
            labelStylePasukan = stat != null ? stat.pasukan.style.label_style : styleJ.label_style;

            if(stat != null)
            markers.marker3D = stat.markers.marker3D;
            
            if(jStat != null)
            markers.marker3D = jStat.marker;
        }

        void Start()
        {

        }

        void Update()
        {

        }
    }
}

namespace HelperPlotting
{
    public static class FormasiType
    {
        public enum satuanJarak
        {
            NauticalMiles,
            Kilometer,
            Feet,
            Yard,
            Meter
        }

        public enum formasiType
        {
            None,
            Belah_Ketupat,
            Flank_Kiri,
            Flank_Kanan,
            Sejajar,
            Serial,
            Panah,
            Kerucut,
            Custom
        }
    }
    
    [Serializable]
    public class FormasiSatuan
    {
        public int id;
        public string nama = string.Empty;
        public string namaFormasi;
        public FormasiType.formasiType type;
        public FormasiType.satuanJarak satuan;
        public int arah;
        public int arahUtama;
        public float jarak;
        public float jarakAntarSatuan;
        public List<Transform> unitsInFormasi;
    }
}
