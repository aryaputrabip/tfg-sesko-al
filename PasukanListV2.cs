using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using HelperPlotting;
using Newtonsoft.Json;
using UnityEngine;

namespace HelperPlotting
{
    [Serializable]
    public class ListPasukanV2
    {
        public int id { get; set; }
        public string nomer_satuan { get; set; }
        public string nomer_atasan { get; set; }
        public string name { get; set; }
        public string matra_pasukan { get; set; }
        public string pasukan_sym_id { get; set; }
        public string speed_pasukan { get; set; }
        public string health_pasukan { get; set; }
        public string jumlah_bintara { get; set; }
        public string jumlah_tamtama { get; set; }
        public string jumlah_perwira { get; set; }
        public string icon_wgs { get; set; }
        public int? id_wgs { get; set; } 
        public string style_symbol { get; set; }
        public string deskripsi { get; set; }
        public string titan { get; set; }
        public string nama { get; set; }
        public string index { get; set; }
        public string keterangan { get; set; }

        ///<summary>
        ///Deserialize Object
        ///</summary>
        public static List<ListPasukanV2> FromJson(string json) => JsonConvert.DeserializeObject<List<ListPasukanV2>>(json, HelperConverter.Converter.Settings);
        
        ///<summary>
        ///Serialize Object
        ///</summary>
        public static string ToString(ListPasukanV2 json) => JsonConvert.SerializeObject(json, Formatting.Indented);
    }

    [Serializable]
    public class EntityPasukanV2
    {
        [JsonIgnore] [SerializeField] int _id;
        [JsonIgnore] [SerializeField] int _id_user;
        [JsonIgnore] [SerializeField] int _id_symbol;
        [JsonIgnore] [SerializeField] string _dokumen;
        [JsonIgnore] [SerializeField] string _nama;
        [JsonIgnore] [SerializeField] float _lat_y;
        [JsonIgnore] [SerializeField] float _lng_x;
        [JsonIgnore] [SerializeField] string _symbol;
        [JsonIgnore] [SerializeField] int? _id_kegiatan;
        [JsonIgnore] [SerializeField] string? _isi_logistik;
        [JsonIgnore] [SerializeField] string _tipe_tni;
        [JsonIgnore] [SerializeField] string _matra;
        [JsonIgnore] [SerializeField] StylePasukan _style;
        [JsonIgnore] [SerializeField] InfoPasukan _info;
        public string path_object_3d { get; set; }
        public string object3D { get; set; }
        public int id { get{return _id;} set{_id = value;} }
        public int id_user { get{return _id_user;} set{_id_user = value;} }
        public int id_symbol { get{return _id_symbol;} set{_id_symbol = value;} }
        public string dokumen { get{return _dokumen;} set{_dokumen = value;} }
        public string nama { get{return _nama;} set{_nama = value;} }
        public float lat_y { get{return _lat_y;} set{_lat_y = value;} }
        public float lng_x { get{return _lng_x;} set{_lng_x = value;} }
        public string tipe_tni { get{return _tipe_tni;} set{_tipe_tni = value;} }
        public string matra { get{return _matra;} set{_matra = value;} }
        public StylePasukan style { get{return _style;} set{ _style = value;} }
        public InfoPasukan info { get{return _info;} set{ _info = value;} }

        [JsonConstructor]
        public EntityPasukanV2(string style, string info)
        {
            this.style = JsonConvert.DeserializeObject<StylePasukan>(style);
            this.info  = JsonConvert.DeserializeObject<InfoPasukan>(info);
        }
        public EntityPasukanV2(){}

        // [JsonConverter(typeof(StyleStringConverter))] public StylePasukan style;
        // [JsonConverter(typeof(InfoStringConverter))] public InfoPasukan info;

        public string symbol { get{return _symbol;} set{_symbol = value;} }
        public int? id_kegiatan { get{return _id_kegiatan;} set{_id_kegiatan = value;} }
        public string? isi_logistik { get{return _isi_logistik;} set{_isi_logistik = value;} }
        
        ///<summary>
        ///Deserialize Object
        ///</summary>
        public static EntityPasukanV2 FromJson(string json) => JsonConvert.DeserializeObject<EntityPasukanV2>(json, HelperConverter.Converter.Settings);
        
        ///<summary>
        ///Serialize Object
        ///</summary>
        public static string ToString(EntityPasukanV2 json) => JsonConvert.SerializeObject(json, Formatting.Indented);
        
        ///<summary>
        ///Serialize Object
        ///</summary>
        public static string ToString(List<EntityPasukanV2> json) => JsonConvert.SerializeObject(json, Formatting.Indented);
    }

    [Serializable]
    public class StylePasukan
    {
        [JsonIgnore] [SerializeField] string _nama;
        [JsonIgnore] [SerializeField] string _index;
        [JsonIgnore] [SerializeField] string _keterangan;
        [JsonIgnore] [SerializeField] int _grup;
        [JsonIgnore] [SerializeField] List<LabelStylePasukan> _label_style;
        public string nama { get{return _nama;} set{_nama = value;} }
        public string index { get{return _index;} set{_index = value;} }
        public string keterangan { get{return _keterangan;} set{_keterangan = value;} }
        public int grup { get{return _grup;} set{_grup = value;} }
        public List<LabelStylePasukan> label_style { get{return _label_style;} set{_label_style = value;} }

        [JsonConstructor]
        public StylePasukan(string label_style)
        {
            this.label_style = JsonConvert.DeserializeObject<List<LabelStylePasukan>>(label_style);
        }
        public StylePasukan(){}

        // [JsonConverter(typeof(LabelStringConverter))] public List<LabelStylePasukan> label_style { get{return _label_style;} set{_label_style = value;} }
        
        ///<summary>
        ///Serialize Object
        ///</summary>
        public static string ToString(StylePasukan json) => JsonConvert.SerializeObject(json, Formatting.Indented);
                
        ///<summary>
        ///Deserialize Object
        ///</summary>
        public static StylePasukan FromJson(string json) => JsonConvert.DeserializeObject<StylePasukan>(json, HelperConverter.Converter.Settings);
    }

    [Serializable]
    public class StylePasukanForSave
    {
        [JsonIgnore] [SerializeField] string _nama;
        [JsonIgnore] [SerializeField] string _index;
        [JsonIgnore] [SerializeField] string _keterangan;
        [JsonIgnore] [SerializeField] int _grup;
        [JsonIgnore] [SerializeField] string _label_style;
        public string nama { get{return _nama;} set{_nama = value;} }
        public string index { get{return _index;} set{_index = value;} }
        public string keterangan { get{return _keterangan;} set{_keterangan = value;} }
        public int grup { get{return _grup;} set{_grup = value;} }
        public string label_style { get{return _label_style;} set{_label_style = value;} }

        // [JsonConverter(typeof(LabelStringConverter))] public List<LabelStylePasukan> label_style { get{return _label_style;} set{_label_style = value;} }
        
        ///<summary>
        ///Serialize Object
        ///</summary>
        public static string ToString(StylePasukanForSave json) => JsonConvert.SerializeObject(json, Formatting.Indented);
                
        ///<summary>
        ///Deserialize Object
        ///</summary>
        public static StylePasukanForSave FromJson(string json) => JsonConvert.DeserializeObject<StylePasukanForSave>(json, HelperConverter.Converter.Settings);
    }

    [Serializable]
    public class LabelStylePasukan
    {
        public object? label_symbol { get; set; }
        public int margin_bottom { get; set; }
        public int margin_right { get; set; }
        public int margin_top { get; set; }
        public object? label_symbol_tab2 { get; set; }
        public int margin_bottom_tab2 { get; set; }
        public int margin_right_tab2 { get; set; }
        public int margin_top_tab2 { get; set; }
        public object? label_symbol_tab3 { get; set; }
        public int margin_bottom_tab3 { get; set; }
        public int margin_right_tab3 { get; set; }
        public int margin_top_tab3 { get; set; }
        public object? label_symbol_tab4 { get; set; }
        public int margin_bottom_tab4 { get; set; }
        public int margin_right_tab4 { get; set; }
        public int margin_top_tab4 { get; set; }
        
        ///<summary>
        ///Serialize Object
        ///</summary>
        public static string ToString(List<LabelStylePasukan> json) => JsonConvert.SerializeObject(json, Formatting.Indented);

        ///<summary>
        ///Serialize Object
        ///</summary>
        public static List<LabelStylePasukan> FromJson(string json) => JsonConvert.DeserializeObject<List<LabelStylePasukan>>(json, HelperConverter.Converter.Settings);
    }

    [Serializable]
    public class InfoPasukan
    {
        [JsonIgnore] [SerializeField] string _kecepatan;
        [JsonIgnore] [SerializeField] string _nomer_satuan;
        [JsonIgnore] [SerializeField] string _nama_satuan;
        [JsonIgnore] [SerializeField] string _nomer_atasan;
        [JsonIgnore] [SerializeField] string _tgl_selesai;
        [JsonIgnore] [SerializeField] string _warna;
        [JsonIgnore] [SerializeField] int _size;
        [JsonIgnore] [SerializeField] string _weapon;
        [JsonIgnore] [SerializeField] List<string> _waypoint;
        [JsonIgnore] [SerializeField] List<string> _list_embarkasi;
        [JsonIgnore] [SerializeField] int _armor;
        [JsonIgnore] [SerializeField] bool _id_dislokasi;
        [JsonIgnore] [SerializeField] bool _id_dislokasi_obj;
        [JsonIgnore] [SerializeField] int _bahan_bakar;
        [JsonIgnore] [SerializeField] int _bahan_bakar_load;
        [JsonIgnore] [SerializeField] string _kecepatan_maks;
        [JsonIgnore] [SerializeField] string _heading;
        [JsonIgnore] [SerializeField] string _ket_satuan;
        [JsonIgnore] [SerializeField] string _nama_icon_satuan;
        [JsonIgnore] [SerializeField] string _width_icon_satuan;
        [JsonIgnore] [SerializeField] string _height_icon_satuan;
        [JsonIgnore] [SerializeField] int _personil;
        [JsonIgnore] [SerializeField] bool _tfg;
        [JsonIgnore] [SerializeField] bool _hidewgs;
        public string kecepatan { get{return _kecepatan;} set{_kecepatan = value;} }
        public string nomer_satuan { get{return _nomer_satuan;} set{_nomer_satuan = value;} }
        public string nama_satuan { get{return _nama_satuan;} set{_nama_satuan = value;} }
        public string nomer_atasan { get{return _nomer_atasan;} set{_nomer_atasan = value;} }
        public string tgl_selesai { get{return _tgl_selesai;} set{_tgl_selesai = value;} }
        public string warna { get{return _warna;} set{_warna = value;} }
        public int size { get{return _size;} set{_size = value;} }
        public string weapon { get{return _weapon;} set{_weapon = value;} }        
        public List<string> waypoint { get{return _waypoint;} set{_waypoint = value;} }
        public List<string> list_embarkasi { get{return _list_embarkasi;} set{_list_embarkasi = value;} }
        public int armor { get{return _armor;} set{_armor = value;} }
        public bool id_dislokasi { get{return _id_dislokasi;} set{_id_dislokasi = value;} }
        public bool id_dislokasi_obj { get{return _id_dislokasi_obj;} set{_id_dislokasi_obj = value;} }
        public int bahan_bakar { get{return _bahan_bakar;} set{_bahan_bakar = value;} }
        public int bahan_bakar_load { get{return _bahan_bakar_load;} set{_bahan_bakar_load = value;} }
        public string kecepatan_maks { get{return _kecepatan_maks;} set{_kecepatan_maks = value;} }
        public string heading { get{return _heading;} set{_heading = value;} }
        public string ket_satuan { get{return _ket_satuan;} set{_ket_satuan = value;} }
        public string nama_icon_satuan { get{return _nama_icon_satuan;} set{_nama_icon_satuan = value;} }
        public string width_icon_satuan { get{return _width_icon_satuan;} set{_width_icon_satuan = value;} }
        public string height_icon_satuan { get{return _height_icon_satuan;} set{_height_icon_satuan = value;} }
        public int personil { get{return _personil;} set{_personil = value;} }
        public bool tfg { get{return _tfg;} set{_tfg = value;} }
        public bool hidewgs { get{return _hidewgs;} set{_hidewgs = value;} }
        
        ///<summary>
        ///Serialize Object
        ///</summary>
        public static string ToString(InfoPasukan json) => JsonConvert.SerializeObject(json, Formatting.Indented);
        
        ///<summary>
        ///Deserialize Object
        ///</summary>
        public static InfoPasukan FromJson(string json) => JsonConvert.DeserializeObject<InfoPasukan>(json, HelperConverter.Converter.Settings);
    }

    [Serializable]
    public class Weapon
    {
        [JsonIgnore] [SerializeField] int _index;
        [JsonIgnore] [SerializeField] int _id;
        [JsonIgnore] [SerializeField] string _jenis;
        public int index { get{ return _index; } set{ _index = value; } }
        public int id { get{ return _id; } set{ _id = value; } }
        public string jenis { get{ return _jenis; } set{ _jenis = value; } }
        
        ///<summary>
        ///Serialize Object
        ///</summary>
        public static string ToString(List<List<Weapon>> json) => JsonConvert.SerializeObject(json, Formatting.Indented);
        
        ///<summary>
        ///Deserialize Object
        ///</summary>
        public static List<List<Weapon>> FromJson(string json) => JsonConvert.DeserializeObject<List<List<Weapon>>>(json, HelperConverter.Converter.Settings);
    }
    
    public class ShowMarkerLabelsByZoomItem
    {
        /// <summary>
        /// Zoom range where you need to show the label.
        /// </summary>
        public OnlineMapsRange zoomRange;

        /// <summary>
        /// Label.
        /// </summary>
        public string label;

        public ShowMarkerLabelsByZoomItem(string label, OnlineMapsRange zoomRange)
        {
            this.label = label;
            this.zoomRange = zoomRange;
        }
    }

    public class StyleStringConverter : JsonConverter<StylePasukan>
    {
        public override void WriteJson(JsonWriter writer, StylePasukan value, JsonSerializer serializer)
        {
            writer.WriteRawValue(JsonConvert.SerializeObject(value, Formatting.Indented));
        }

        public override StylePasukan ReadJson(JsonReader reader, Type objectType, StylePasukan existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            string s = (string)reader.Value;

            return JsonConvert.DeserializeObject<StylePasukan>(s);
        }
    }
    
    public class InfoStringConverter : JsonConverter<InfoPasukan>
    {
        public override void WriteJson(JsonWriter writer, InfoPasukan value, JsonSerializer serializer)
        {
            writer.WriteRawValue(JsonConvert.SerializeObject(value, Formatting.Indented));
        }

        public override InfoPasukan ReadJson(JsonReader reader, Type objectType, InfoPasukan existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            string s = (string)reader.Value;

            return JsonConvert.DeserializeObject<InfoPasukan>(s);
        }
    }

    public class LabelStringConverter : JsonConverter<List<LabelStylePasukan>>
    {
        public override void WriteJson(JsonWriter writer, List<LabelStylePasukan> value, JsonSerializer serializer)
        {
            writer.WriteRawValue(JsonConvert.SerializeObject(value, Formatting.Indented));
        }

        public override List<LabelStylePasukan> ReadJson(JsonReader reader, Type objectType, List<LabelStylePasukan> existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            string s = (string)reader.Value;

            return JsonConvert.DeserializeObject<List<LabelStylePasukan>>(s);
        }
    }
}
