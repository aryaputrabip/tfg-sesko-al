using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using ObjectStat;
using HelperPlotting;
using HelperEditor;
using Plot.Pasukan;
using GlobalVariables;

namespace Plot.FormasiPlot
{
    public class PerhitunganFormasi
    {
        public List<float> KonversiJarak(List<float> jarak, int jarakD)
        {
            for (int i = 0; i < jarak.Count; i++)
            {
                if (jarakD == 0)
                    jarak[i] = jarak[i] / 60f;
                else if (jarakD == 1)
                    jarak[i] = jarak[i] / 1.852f / 60f;
                else if (jarakD == 2)
                    jarak[i] = jarak[i] / 6076f / 60f;
                else if (jarakD == 3)
                    jarak[i] = jarak[i] / 2025f / 60f;
                else if (jarakD == 4)
                    jarak[i] = jarak[i] / 1852f / 60f;
            }
            return jarak;
        }

        public List<float> Jarak(List<float> jarak, float arah, int formasiD, string degree)
        {
            float heading = 0;

            float[] hArr = new float[] { 0, 90, 180, 270 };
            float[] hArr2 = new float[] { 45, 135, 225, 315 };

            float newJarak = 0;
            List<float> newJarakList = new List<float>();

            for (int i = 0; i < jarak.Count; i++)
            {
                int j = i + 1;

                if (formasiD == 1)
                {
                    if (j % 4 == 1)
                        heading = hArr[0];
                    else if (j % 4 == 2)
                        heading = hArr[1];
                    else if (j % 4 == 3)
                        heading = hArr[2];
                    else if (j % 4 == 0)
                        heading = hArr[3];
                }
                else if (formasiD == 2)
                {
                    if (j % 2 == 1)
                        heading = hArr2[3];
                    else if (j % 2 == 0)
                        heading = hArr2[1];
                }
                else if (formasiD == 3)
                {
                    if (j % 2 == 1)
                        heading = hArr2[2];
                    else if (j % 2 == 0)
                        heading = hArr2[0];
                }
                else if (formasiD == 4)
                {
                    if (j % 4 == 1 || j % 4 == 2)
                        heading = hArr[1];
                    else if (j % 4 == 3 || j % 4 == 0)
                        heading = hArr[3];
                }
                else if (formasiD == 5)
                {
                    if (j % 2 == 1)
                        heading = hArr[0];
                    else if (j % 2 == 0)
                        heading = hArr[2];
                }
                else if (formasiD == 6)
                {
                    if (j % 5 == 1)
                        heading = hArr[0];
                    else if (j % 5 == 2)
                        heading = hArr[1];
                    else if (j % 5 == 3)
                        heading = hArr[2];
                    else if (j % 5 == 4)
                        heading = hArr[3];
                    else if (j % 5 == 0)
                        heading = hArr[2];
                }
                else if (formasiD == 7)
                {
                    if (j % 8 == 1)
                        heading = hArr[0];
                    else if (j % 8 == 2)
                        heading = hArr[1];
                    else if (j % 8 == 3)
                        heading = hArr[2];
                    else if (j % 8 == 4)
                        heading = hArr[3];
                    else if (j % 8 == 5)
                        heading = hArr[1];
                    else if (j % 8 == 6)
                        heading = hArr[3];
                    else if (j % 8 == 7)
                        heading = hArr[1];
                    else if (j % 8 == 0)
                        heading = hArr[3];
                }

                heading += arah;

                // PlotFormasiController.instance.arrArray = new List<float>();
                if(degree == "Latitude")
                PlotFormasiController.instance.arrArray.Add(heading);

                if (degree == "Latitude")
                    newJarak = (float)(jarak[i] * Math.Cos(heading * (Math.PI / 180)));
                else
                    newJarak = (float)(jarak[i] * Math.Sin(heading * (Math.PI / 180)));

                Debug.Log("urutan jarak: " + i + " heading: " + heading + " new jarak: " + newJarak + " degree: " + degree);

                newJarakList.Add(newJarak);
            }

            return newJarakList;
        }

        public List<float> Jarak(List<float> jarak, List<float> arahArr, float arah, string degree)
        {
            float heading = 0;

            float newJarak = 0;
            List<float> newJarakList = new List<float>();

            for (int i = 0; i < jarak.Count; i++)
            {
                heading = arahArr[i];

                heading += arah;

                // PlotFormasiController.instance.arrArray = new List<float>();

                if(degree == "Latitude")
                PlotFormasiController.instance.arrArray.Add(heading);

                if (degree == "Latitude")
                    newJarak = (float)(jarak[i] * Math.Cos(heading * (Math.PI / 180)));
                else
                    newJarak = (float)(jarak[i] * Math.Sin(heading * (Math.PI / 180)));

                Debug.Log("urutan jarak: " + i + " heading: " + heading + " new jarak: " + newJarak + " degree: " + degree);

                newJarakList.Add(newJarak);
            }

            return newJarakList;
        }

        public static Satuan_Formasi SetDataPerSatuanFormasi(PFormasi pF, PFormasi.satuanJarak satuan, float heading, float jarak)
        {
            var sat = new Satuan_Formasi();
            var icon = new Icon_Formasi();

            if (pF.jStat != null)
            {
                var jStat = pF.jStat;
                var info = EntitySatuanInfo.FromJson(jStat.info);
                var style = EntitySatuanStyle.FromJson(jStat.style);
                sat.id_point = jStat.nama;
                sat.nama = info.nama_satuan;
                sat.color = info.warna;
                icon.grup = int.Parse(style.grup);
                icon.keterangan = style.keterangan;
                icon.nama = style.nama;
                icon.index = style.index.ToString();
                icon.label_style = style.label_style;
            }
            else
            {
                var stat = pF.stat;
                var style = Style_Pasukan.FromJson(stat.style);
                sat.id_point = stat.namaSatuan;
                sat.nama = stat.nama;
                sat.color = stat.warna;

                var listPas = PlotPasukanController.instance.lPas;
                if(listPas != null)
                for (int i = 0; i < listPas.Count; i++)
                    if (listPas[i].name == sat.nama)
                        icon.keterangan = listPas[i].keterangan;

                icon.grup = 10;
                icon.nama = stat.fontType;
                icon.index = ((int)char.Parse(stat.font)).ToString();
                icon.label_style = style.label_style;
            }

            sat.lng = pF.marker3D.position.x;
            sat.lat = pF.marker3D.position.y;

            sat.id = pF.urutanFormasi;
            
            if(sat.id == 0)
            sat.inti = 1;
            else
            sat.inti = 0;

            if (sat.id > 0)
                sat.jarak = pF.jarak;
            else
                sat.jarak = 0;

            if (satuan == PFormasi.satuanJarak.Nautical_Miles)
            {
                sat.satuan = "nm";
                sat.new_jarak = pF.jarak / 60f;
            }
            else if (satuan == PFormasi.satuanJarak.Kilometer)
            {
                sat.satuan = "km";
                sat.new_jarak = pF.jarak / 60f / 1852f;
            }
            else if (satuan == PFormasi.satuanJarak.Meter)
            {
                sat.satuan = "m";
                sat.new_jarak = pF.jarak / 60f / 2025f;
            }
            else if (satuan == PFormasi.satuanJarak.Feet)
            {
                sat.satuan = "foot";
                sat.new_jarak = pF.jarak / 60f / 6076f;
            }
            else if (satuan == PFormasi.satuanJarak.Yard)
            {
                sat.satuan = "yard";
                sat.new_jarak = pF.jarak / 60f / 1.852f;
            }
            
            var arrArah = PlotFormasiController.instance.arrArray;

            if(sat.id != 0)
            sat.sudut = arrArah[sat.id - 1];

            sat.icon = icon;
            return sat;
        }

        //Belah Ketupat
        public static void Formasi1(List<Transform> selectedUnits, string nama, float heading, PFormasi.formasiType tipeFormasi, PFormasi.satuanJarak satuan, List<float> jarak, List<float> jarakSatuanLng, List<float> jarakSatuanLat)
        {
            float jarakNow = 0;
            var indexFormasi = PlotFormasiController.instance.indexFormasi;
            var pF0 = selectedUnits[0].GetComponent<PFormasi>();
            var pF1 = selectedUnits[1].GetComponent<PFormasi>();
            PFormasi pF2 = null;
            PFormasi pF3 = null;
            PFormasi pF4 = null;
            var listSatuanFormasi = new List<Satuan_Formasi>();
            string namaSatuan = "";

            if(pF0.nama == "")
            namaSatuan = "formasi_" + indexFormasi + "_" + SessionUser.id + "_" + UnityEngine.Random.Range(1, 1000);
            else
            namaSatuan = pF0.nama;

            for (int i = 0; i < selectedUnits.Count; i++)
            {
                if (selectedUnits[i].GetComponent<PFormasi>() != null)
                {
                    if (i == 2)
                        pF2 = selectedUnits[2].GetComponent<PFormasi>();
                    if (i == 3)
                        pF3 = selectedUnits[3].GetComponent<PFormasi>();
                    if (i == 4)
                        pF4 = selectedUnits[4].GetComponent<PFormasi>();
                }
            }

            var m2D0 = pF0.marker3D;
            var pos2D0 = m2D0.position;

            for (int i = 0; i < selectedUnits.Count; i++)
            {
                var m2D1 = pF1.marker3D;
                var pos2D1 = m2D1.position;

                OnlineMapsMarker3D m2D2 = null;
                OnlineMapsMarker3D m2D3 = null;
                OnlineMapsMarker3D m2D4 = null;
                Vector2 pos2D2 = new Vector2();
                Vector2 pos2D3 = new Vector2();
                Vector2 pos2D4 = new Vector2();

                var pF = selectedUnits[i].GetComponent<PFormasi>();
                var pMarker2D = pF.marker2D;
                var pMarker3D = pF.marker3D;
                pF.nama = namaSatuan;
                pF.namaFormasi = nama;

                if (pF.jStat == null)
                {
                    var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                    
                    pF2D.formasi = tipeFormasi;
                    pF2D.satuan = satuan;
                    // pF2D.jarak = jarak[i - 1];
                    pF2D.urutanFormasi = i;
                    pF2D.nama = namaSatuan;
                    pF2D.namaFormasi = nama;

                    pF2D.listObjekFormasi = new List<Transform>();
                    for (int j = 0; j < selectedUnits.Count; j++)
                    pF2D.listObjekFormasi.Add(selectedUnits[j].GetComponent<PFormasi>().marker3D.instance.transform);
                }
                var pF3D = pMarker3D.instance.GetComponent<PFormasi>();

                Vector2 position2D = new Vector2();
                if (pF.jStat == null)
                    position2D = pMarker2D.position;

                var position3D = pMarker3D.position;

                
                pF3D.formasi = tipeFormasi;
                pF3D.satuan = satuan;
                pF3D.urutanFormasi = i;
                pF3D.nama = namaSatuan;
                pF3D.namaFormasi = nama;

                pF3D.listObjekFormasi = new List<Transform>();
                for (int j = 0; j < selectedUnits.Count; j++)
                    pF3D.listObjekFormasi.Add(selectedUnits[j].GetComponent<PFormasi>().marker3D.instance.transform);

                if (i != 0)
                {
                    if (pF.jStat == null)
                    {
                        var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                        pF2D.jarak = jarak[i - 1];
                    }
                    pF3D.jarak = jarak[i - 1];

                    if (pF.jStat == null)
                    {
                        position2D.x = pos2D0.x + jarakSatuanLng[i - 1];
                        position2D.y = pos2D0.y + jarakSatuanLat[i - 1];
                    }

                    position3D.x = pos2D0.x + jarakSatuanLng[i - 1];
                    position3D.y = pos2D0.y + jarakSatuanLat[i - 1];

                    if (i % 4 == 1)
                    {
                        if (i != 1)
                        {
                            if (pF.jStat == null)
                            {
                                position2D.x = pos2D1.x + jarakSatuanLng[i - 1];
                                position2D.y = pos2D1.y + jarakSatuanLat[i - 1];
                            }

                            position3D.x = pos2D1.x + jarakSatuanLng[i - 1];
                            position3D.y = pos2D1.y + jarakSatuanLat[i - 1];
                            
                            if (pF.jStat == null)
                            {
                                var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                                pF2D.jarak += pF1.jarak;
                            }
                            pF.jarak += pF1.jarak;

                            pF1 = pF;
                        }
                    }
                    else if (i % 4 == 2)
                    {
                        m2D2 = pF2.marker3D;
                        pos2D2 = m2D2.position;
                        if (i != 2)
                        {
                            if (pF.jStat == null)
                            {
                                position2D.x = pos2D2.x + jarakSatuanLng[i - 1];
                                position2D.y = pos2D2.y + jarakSatuanLat[i - 1];
                            }

                            position3D.x = pos2D2.x + jarakSatuanLng[i - 1];
                            position3D.y = pos2D2.y + jarakSatuanLat[i - 1];

                            if (pF.jStat == null)
                            {
                                var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                                pF2D.jarak += pF2.jarak;
                            }
                            pF.jarak += pF2.jarak;

                            pF2 = pF;
                        }
                    }
                    else if (i % 4 == 3)
                    {
                        m2D3 = pF3.marker3D;
                        pos2D3 = m2D3.position;
                        if (i != 3)
                        {
                            if (pF.jStat == null)
                            {
                                position2D.x = pos2D3.x + jarakSatuanLng[i - 1];
                                position2D.y = pos2D3.y + jarakSatuanLat[i - 1];
                            }

                            position3D.x = pos2D3.x + jarakSatuanLng[i - 1];
                            position3D.y = pos2D3.y + jarakSatuanLat[i - 1];

                            if (pF.jStat == null)
                            {
                                var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                                pF2D.jarak += pF3.jarak;
                            }
                            pF.jarak += pF3.jarak;

                            pF3 = pF;
                        }
                    }
                    else if (i % 4 == 0)
                    {
                        m2D4 = pF4.marker3D;
                        pos2D4 = m2D4.position;
                        if (i != 4)
                        {
                            if (pF.jStat == null)
                            {
                                position2D.x = pos2D4.x + jarakSatuanLng[i - 1];
                                position2D.y = pos2D4.y + jarakSatuanLat[i - 1];
                            }

                            position3D.x = pos2D4.x + jarakSatuanLng[i - 1];
                            position3D.y = pos2D4.y + jarakSatuanLat[i - 1];

                            if (pF.jStat == null)
                            {
                                var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                                pF2D.jarak += pF4.jarak;
                            }
                            pF.jarak += pF4.jarak;

                            pF4 = pF;
                            Debug.Log(position2D);
                        }
                    }

                    if (pF.jStat == null)
                        pMarker2D.position = position2D;
                    pMarker3D.position = position3D;
                    jarakNow = jarak[i - 1];
                }
                pMarker3D.rotation = Quaternion.Euler(Vector3.up * heading);
                var satuanFormasi = SetDataPerSatuanFormasi(pF, satuan, heading, jarakNow);
                listSatuanFormasi.Add(satuanFormasi);
            }

            for (int i = 0; i < selectedUnits.Count; i++)
            {
                var pF = selectedUnits[i].GetComponent<PFormasi>();
                var infoFor = new Info_Formasi();
                infoFor.size = 20;
                infoFor.nama_formasi = nama;
                infoFor.arah = heading;
                infoFor.arrArah = PlotFormasiController.instance.arrArray;
                infoFor.satuan_formasi = listSatuanFormasi;

                if (pF.jStat != null)
                {
                    var info = EntitySatuanInfo.FromJson(pF.jStat.info);
                    infoFor.warna = info.warna;
                }
                else
                    infoFor.warna = pF.stat.warna;

                if(pF.stat != null)
                {
                    var pF2D = pF.marker2D.instance.GetComponent<PFormasi>();
                    pF2D.info = Info_Formasi.ToString(infoFor);
                }

                var pF3D = pF.marker3D.instance.GetComponent<PFormasi>();
                pF3D.info = Info_Formasi.ToString(infoFor);
            }
        }

        //Flank Kanan, Flank Kiri, Serial
        public static void Formasi2(List<Transform> selectedUnits, string nama, float heading, PFormasi.formasiType tipeFormasi, PFormasi.satuanJarak satuan, List<float> jarak, List<float> jarakSatuanLng, List<float> jarakSatuanLat)
        {
            float jarakNow = 0;
            var indexFormasi = PlotFormasiController.instance.indexFormasi;
            var pF0 = selectedUnits[0].GetComponent<PFormasi>();
            var pF1 = selectedUnits[1].GetComponent<PFormasi>();
            PFormasi pF2 = null;
            var listSatuanFormasi = new List<Satuan_Formasi>();
            string namaSatuan = "";

            if(pF0.nama == "")
            namaSatuan = "formasi_" + indexFormasi + "_" + SessionUser.id + "_" + UnityEngine.Random.Range(1, 1000);
            else
            namaSatuan = pF0.nama;

            for (int i = 0; i < selectedUnits.Count; i++)
            {
                if (selectedUnits[i].GetComponent<PFormasi>() != null)
                {
                    if (i == 2)
                        pF2 = selectedUnits[2].GetComponent<PFormasi>();
                }
            }

            var m2D0 = pF0.marker3D;
            var pos2D0 = m2D0.position;

            for (int i = 0; i < selectedUnits.Count; i++)
            {
                var m2D1 = pF1.marker3D;
                var pos2D1 = m2D1.position;

                OnlineMapsMarker3D m2D2 = null;
                Vector2 pos2D2 = new Vector2();

                var pF = selectedUnits[i].GetComponent<PFormasi>();
                var pMarker2D = pF.marker2D;
                var pMarker3D = pF.marker3D;
                pF.nama = namaSatuan;
                pF.namaFormasi = nama;

                if (pF.jStat == null)
                {
                    var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                    
                    pF2D.formasi = tipeFormasi;
                    pF2D.satuan = satuan;
                    pF2D.urutanFormasi = i;
                    pF2D.nama = namaSatuan;
                    pF2D.namaFormasi = nama;

                    pF2D.listObjekFormasi = new List<Transform>();
                    for (int j = 0; j < selectedUnits.Count; j++)
                    pF2D.listObjekFormasi.Add(selectedUnits[j].GetComponent<PFormasi>().marker3D.instance.transform);
                }
                var pF3D = pMarker3D.instance.GetComponent<PFormasi>();

                Vector2 position2D = new Vector2();
                if (pF.jStat == null)
                    position2D = pMarker2D.position;

                var position3D = pMarker3D.position;

                
                pF3D.formasi = tipeFormasi;
                pF3D.satuan = satuan;
                pF3D.urutanFormasi = i;
                pF3D.nama = namaSatuan;
                pF3D.namaFormasi = nama;

                pF3D.listObjekFormasi = new List<Transform>();
                for (int j = 0; j < selectedUnits.Count; j++)
                    pF3D.listObjekFormasi.Add(selectedUnits[j].GetComponent<PFormasi>().marker3D.instance.transform);

                if (i != 0)
                {
                    if (pF.jStat == null)
                    {
                        var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                        pF2D.jarak = jarak[i - 1];
                    }
                    pF3D.jarak = jarak[i - 1];

                    if (pF.jStat == null)
                    {
                        position2D.x = pos2D0.x + jarakSatuanLng[i - 1];
                        position2D.y = pos2D0.y + jarakSatuanLat[i - 1];
                    }

                    position3D.x = pos2D0.x + jarakSatuanLng[i - 1];
                    position3D.y = pos2D0.y + jarakSatuanLat[i - 1];

                    if (i % 2 == 1)
                    {
                        if (i != 1)
                        {
                            if (pF.jStat == null)
                            {
                                position2D.x = pos2D1.x + jarakSatuanLng[i - 1];
                                position2D.y = pos2D1.y + jarakSatuanLat[i - 1];
                            }

                            position3D.x = pos2D1.x + jarakSatuanLng[i - 1];
                            position3D.y = pos2D1.y + jarakSatuanLat[i - 1];

                            if (pF.jStat == null)
                            {
                                var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                                pF2D.jarak += pF1.jarak;
                            }
                            pF.jarak += pF1.jarak;

                            pF1 = pF;
                        }
                    }
                    else if (i % 2 == 0)
                    {
                        m2D2 = pF2.marker3D;
                        pos2D2 = m2D2.position;
                        if (i != 2)
                        {
                            if (pF.jStat == null)
                            {
                                position2D.x = pos2D2.x + jarakSatuanLng[i - 1];
                                position2D.y = pos2D2.y + jarakSatuanLat[i - 1];
                            }

                            position3D.x = pos2D2.x + jarakSatuanLng[i - 1];
                            position3D.y = pos2D2.y + jarakSatuanLat[i - 1];

                            if (pF.jStat == null)
                            {
                                var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                                pF2D.jarak += pF2.jarak;
                            }
                            pF.jarak += pF2.jarak;

                            pF2 = pF;
                        }
                    }

                    if (pF.jStat == null)
                        pMarker2D.position = position2D;
                    pMarker3D.position = position3D;
                    jarakNow = jarak[i - 1];
                }
                pMarker3D.rotation = Quaternion.Euler(Vector3.up * heading);
                var satuanFormasi = SetDataPerSatuanFormasi(pF, satuan, heading, jarakNow);

                if (pF.jStat == null)
                {
                    var pF2D = pF.marker2D.instance.GetComponent<PFormasi>();
                    pF2D.sudut = satuanFormasi.sudut;
                }
                pF3D.sudut = satuanFormasi.sudut;

                listSatuanFormasi.Add(satuanFormasi);
            }

            for (int i = 0; i < selectedUnits.Count; i++)
            {
                var pF = selectedUnits[i].GetComponent<PFormasi>();
                var infoFor = new Info_Formasi();
                infoFor.size = 20;
                infoFor.nama_formasi = nama;
                infoFor.arah = heading;
                infoFor.arrArah = PlotFormasiController.instance.arrArray;
                infoFor.satuan_formasi = listSatuanFormasi;

                if (pF.jStat != null)
                {
                    var info = EntitySatuanInfo.FromJson(pF.jStat.info);
                    infoFor.warna = info.warna;
                }
                else
                    infoFor.warna = pF.stat.warna;

                if(pF.stat != null)
                {
                    var pF2D = pF.marker2D.instance.GetComponent<PFormasi>();
                    pF2D.info = Info_Formasi.ToString(infoFor);
                }

                var pF3D = pF.marker3D.instance.GetComponent<PFormasi>();
                pF3D.info = Info_Formasi.ToString(infoFor);
            }
        }

        //Sejajar
        public static void Formasi3(List<Transform> selectedUnits, string nama, float heading, PFormasi.formasiType tipeFormasi, PFormasi.satuanJarak satuan, List<float> jarak, List<float> jarakSatuanLng, List<float> jarakSatuanLat)
        {
            float jarakNow = 0;
            var indexFormasi = PlotFormasiController.instance.indexFormasi;
            var pF0 = selectedUnits[0].GetComponent<PFormasi>();
            var pF1 = selectedUnits[1].GetComponent<PFormasi>();
            PFormasi pF2 = null;
            var listSatuanFormasi = new List<Satuan_Formasi>();
            string namaSatuan = "";

            if(pF0.nama == "")
            namaSatuan = "formasi_" + indexFormasi + "_" + SessionUser.id + "_" + UnityEngine.Random.Range(1, 1000);
            else
            namaSatuan = pF0.nama;

            for (int i = 0; i < selectedUnits.Count; i++)
            {
                if (selectedUnits[i].GetComponent<PFormasi>() != null)
                {
                    if (i == 3)
                        pF2 = selectedUnits[3].GetComponent<PFormasi>();
                }
            }

            var m2D0 = pF0.marker3D;
            var pos2D0 = m2D0.position;

            for (int i = 0; i < selectedUnits.Count; i++)
            {
                var m2D1 = pF1.marker3D;
                var pos2D1 = m2D1.position;

                OnlineMapsMarker3D m2D2 = null;
                Vector2 pos2D2 = new Vector2();

                var pF = selectedUnits[i].GetComponent<PFormasi>();
                var pMarker2D = pF.marker2D;
                var pMarker3D = pF.marker3D;
                pF.nama = namaSatuan;
                pF.namaFormasi = nama;

                if (pF.jStat == null)
                {
                    var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                    
                    pF2D.formasi = tipeFormasi;
                    pF2D.satuan = satuan;
                    pF2D.urutanFormasi = i;
                    pF2D.nama = namaSatuan;
                    pF2D.namaFormasi = nama;

                    pF2D.listObjekFormasi = new List<Transform>();
                    for (int j = 0; j < selectedUnits.Count; j++)
                    pF2D.listObjekFormasi.Add(selectedUnits[j].GetComponent<PFormasi>().marker3D.instance.transform);
                }
                var pF3D = pMarker3D.instance.GetComponent<PFormasi>();

                Vector2 position2D = new Vector2();
                if (pF.jStat == null)
                    position2D = pMarker2D.position;

                var position3D = pMarker3D.position;

                
                pF3D.formasi = tipeFormasi;
                pF3D.satuan = satuan;
                pF3D.urutanFormasi = i;
                pF3D.nama = namaSatuan;
                pF3D.namaFormasi = nama;

                pF3D.listObjekFormasi = new List<Transform>();
                for (int j = 0; j < selectedUnits.Count; j++)
                    pF3D.listObjekFormasi.Add(selectedUnits[j].GetComponent<PFormasi>().marker3D.instance.transform);

                if (i != 0)
                {
                    if (pF.jStat == null)
                    {
                        var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                        pF2D.jarak = jarak[i - 1];
                    }
                    pF3D.jarak = jarak[i - 1];

                    if (pF.jStat == null)
                    {
                        position2D.x = pos2D0.x + jarakSatuanLng[i - 1];
                        position2D.y = pos2D0.y + jarakSatuanLat[i - 1];
                    }

                    position3D.x = pos2D0.x + jarakSatuanLng[i - 1];
                    position3D.y = pos2D0.y + jarakSatuanLat[i - 1];

                    if (i % 4 == 1 || i % 4 == 2)
                    {
                        if (i != 1)
                        {
                            if (pF.jStat == null)
                            {
                                position2D.x = pos2D1.x + jarakSatuanLng[i - 1];
                                position2D.y = pos2D1.y + jarakSatuanLat[i - 1];
                            }

                            position3D.x = pos2D1.x + jarakSatuanLng[i - 1];
                            position3D.y = pos2D1.y + jarakSatuanLat[i - 1];

                            if (pF.jStat == null)
                            {
                                var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                                pF2D.jarak += pF1.jarak;
                            }
                            pF.jarak += pF1.jarak;

                            pF1 = pF;
                        }
                    }
                    else if (i % 4 == 3 || i % 4 == 0)
                    {
                        m2D2 = pF2.marker3D;
                        pos2D2 = m2D2.position;
                        if (i != 3)
                        {
                            if (pF.jStat == null)
                            {
                                position2D.x = pos2D2.x + jarakSatuanLng[i - 1];
                                position2D.y = pos2D2.y + jarakSatuanLat[i - 1];
                            }

                            position3D.x = pos2D2.x + jarakSatuanLng[i - 1];
                            position3D.y = pos2D2.y + jarakSatuanLat[i - 1];

                            if (pF.jStat == null)
                            {
                                var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                                pF2D.jarak += pF2.jarak;
                            }
                            pF.jarak += pF2.jarak;

                            pF2 = pF;
                        }
                    }

                    if (pF.jStat == null)
                        pMarker2D.position = position2D;
                    pMarker3D.position = position3D;
                    jarakNow = jarak[i - 1];
                }
                pMarker3D.rotation = Quaternion.Euler(Vector3.up * heading);
                var satuanFormasi = SetDataPerSatuanFormasi(pF, satuan, heading, jarakNow);

                if (pF.jStat == null)
                {
                    var pF2D = pF.marker2D.instance.GetComponent<PFormasi>();
                    pF2D.sudut = satuanFormasi.sudut;
                }
                pF3D.sudut = satuanFormasi.sudut;

                listSatuanFormasi.Add(satuanFormasi);
            }

            for (int i = 0; i < selectedUnits.Count; i++)
            {
                var pF = selectedUnits[i].GetComponent<PFormasi>();
                var infoFor = new Info_Formasi();
                infoFor.size = 20;
                infoFor.nama_formasi = nama;
                infoFor.arah = heading;
                infoFor.arrArah = PlotFormasiController.instance.arrArray;
                infoFor.satuan_formasi = listSatuanFormasi;

                if (pF.jStat != null)
                {
                    var info = EntitySatuanInfo.FromJson(pF.jStat.info);
                    infoFor.warna = info.warna;
                }
                else
                    infoFor.warna = pF.stat.warna;

                if(pF.stat != null)
                {
                    var pF2D = pF.marker2D.instance.GetComponent<PFormasi>();
                    pF2D.info = Info_Formasi.ToString(infoFor);
                }

                var pF3D = pF.marker3D.instance.GetComponent<PFormasi>();
                pF3D.info = Info_Formasi.ToString(infoFor);
            }
        }

        public static void Formasi4(List<Transform> selectedUnits, string nama, float heading, PFormasi.formasiType tipeFormasi, PFormasi.satuanJarak satuan, List<float> jarak, List<float> jarakSatuanLng, List<float> jarakSatuanLat)
        {
            float jarakNow = 0;
            var indexFormasi = PlotFormasiController.instance.indexFormasi;
            var pF0 = selectedUnits[0].GetComponent<PFormasi>();
            var pF1 = selectedUnits[1].GetComponent<PFormasi>();
            PFormasi pF2 = null;
            var listSatuanFormasi = new List<Satuan_Formasi>();
            string namaSatuan = "";

            if(pF0.nama == "")
            namaSatuan = "formasi_" + indexFormasi + "_" + SessionUser.id + "_" + UnityEngine.Random.Range(1, 1000);
            else
            namaSatuan = pF0.nama;

            for (int i = 0; i < selectedUnits.Count; i++)
            {
                if (selectedUnits[i].GetComponent<PFormasi>() != null)
                {
                    if (i == 2)
                        pF2 = selectedUnits[2].GetComponent<PFormasi>();
                }
            }

            var m2D0 = pF0.marker3D;
            var pos2D0 = m2D0.position;

            for (int i = 0; i < selectedUnits.Count; i++)
            {
                var m2D1 = pF1.marker3D;
                var pos2D1 = m2D1.position;

                OnlineMapsMarker3D m2D2 = null;
                Vector2 pos2D2 = new Vector2();

                var pF = selectedUnits[i].GetComponent<PFormasi>();
                var pMarker2D = pF.marker2D;
                var pMarker3D = pF.marker3D;
                pF.nama = namaSatuan;
                pF.namaFormasi = nama;

                if (pF.jStat == null)
                {
                    var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                    
                    pF2D.formasi = tipeFormasi;
                    pF2D.satuan = satuan;
                    pF2D.urutanFormasi = i;
                    pF2D.nama = namaSatuan;
                    pF2D.namaFormasi = nama;

                    pF2D.listObjekFormasi = new List<Transform>();
                    for (int j = 0; j < selectedUnits.Count; j++)
                        pF2D.listObjekFormasi.Add(selectedUnits[j].GetComponent<PFormasi>().marker3D.instance.transform);
                }
                var pF3D = pMarker3D.instance.GetComponent<PFormasi>();

                Vector2 position2D = new Vector2();
                if (pF.jStat == null)
                    position2D = pMarker2D.position;

                var position3D = pMarker3D.position;

                
                pF3D.formasi = tipeFormasi;
                pF3D.satuan = satuan;
                pF3D.urutanFormasi = i;
                pF3D.nama = namaSatuan;
                pF3D.namaFormasi = nama;

                pF3D.listObjekFormasi = new List<Transform>();
                for (int j = 0; j < selectedUnits.Count; j++)
                    pF3D.listObjekFormasi.Add(selectedUnits[j].GetComponent<PFormasi>().marker3D.instance.transform);

                if (i != 0)
                {
                    if (pF.jStat == null)
                    {
                        var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                        pF2D.jarak = jarak[i - 1];
                    }
                    pF3D.jarak = jarak[i - 1];

                    if (pF.jStat == null)
                    {
                        position2D.x = pos2D0.x + jarakSatuanLng[i - 1];
                        position2D.y = pos2D0.y + jarakSatuanLat[i - 1];
                    }

                    position3D.x = pos2D0.x + jarakSatuanLng[i - 1];
                    position3D.y = pos2D0.y + jarakSatuanLat[i - 1];

                    if (i % 2 == 1)
                    {
                        if (i != 1)
                        {
                            if (pF.jStat == null)
                            {
                                position2D.x = pos2D1.x + jarakSatuanLng[i - 1];
                                position2D.y = pos2D1.y + jarakSatuanLat[i - 1];
                            }

                            position3D.x = pos2D1.x + jarakSatuanLng[i - 1];
                            position3D.y = pos2D1.y + jarakSatuanLat[i - 1];

                            if (pF.jStat == null)
                            {
                                var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                                pF2D.jarak += pF1.jarak;
                            }
                            pF.jarak += pF1.jarak;

                            pF1 = pF;
                        }
                    }
                    else if (i % 2 == 0)
                    {
                        m2D2 = pF2.marker3D;
                        pos2D2 = m2D2.position;
                        if (i != 2)
                        {
                            if (pF.jStat == null)
                            {
                                position2D.x = pos2D2.x + jarakSatuanLng[i - 1];
                                position2D.y = pos2D2.y + jarakSatuanLat[i - 1];
                            }

                            position3D.x = pos2D2.x + jarakSatuanLng[i - 1];
                            position3D.y = pos2D2.y + jarakSatuanLat[i - 1];

                            if (pF.jStat == null)
                            {
                                var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                                pF2D.jarak += pF2.jarak;
                            }
                            pF.jarak += pF2.jarak;

                            pF2 = pF;
                        }
                    }

                    if (pF.jStat == null)
                        pMarker2D.position = position2D;
                    pMarker3D.position = position3D;
                    jarakNow = jarak[i - 1];
                }
                pMarker3D.rotation = Quaternion.Euler(Vector3.up * heading);
                var satuanFormasi = SetDataPerSatuanFormasi(pF, satuan, heading, jarakNow);

                if (pF.jStat == null)
                {
                    var pF2D = pF.marker2D.instance.GetComponent<PFormasi>();
                    pF2D.sudut = satuanFormasi.sudut;
                }
                pF3D.sudut = satuanFormasi.sudut;

                listSatuanFormasi.Add(satuanFormasi);
            }

            for (int i = 0; i < selectedUnits.Count; i++)
            {
                var pF = selectedUnits[i].GetComponent<PFormasi>();
                var infoFor = new Info_Formasi();
                infoFor.size = 20;
                infoFor.nama_formasi = nama;
                infoFor.arah = heading;
                infoFor.arrArah = PlotFormasiController.instance.arrArray;
                infoFor.satuan_formasi = listSatuanFormasi;

                if (pF.jStat != null)
                {
                    var info = EntitySatuanInfo.FromJson(pF.jStat.info);
                    infoFor.warna = info.warna;
                }
                else
                    infoFor.warna = pF.stat.warna;

                if(pF.stat != null)
                {
                    var pF2D = pF.marker2D.instance.GetComponent<PFormasi>();
                    pF2D.info = Info_Formasi.ToString(infoFor);
                }

                var pF3D = pF.marker3D.instance.GetComponent<PFormasi>();
                pF3D.info = Info_Formasi.ToString(infoFor);
            }
        }

        public static void Formasi5(List<Transform> selectedUnits, string nama, float heading, PFormasi.formasiType tipeFormasi, PFormasi.satuanJarak satuan, List<float> jarak, List<float> jarakSatuanLng, List<float> jarakSatuanLat)
        {
            float jarakNow = 0;
            var indexFormasi = PlotFormasiController.instance.indexFormasi;
            var pF0 = selectedUnits[0].GetComponent<PFormasi>();
            var pF1 = selectedUnits[1].GetComponent<PFormasi>();
            PFormasi pF2 = null;
            PFormasi pF3 = null;
            PFormasi pF4 = null;
            var listSatuanFormasi = new List<Satuan_Formasi>();
            string namaSatuan = "";

            if(pF0.nama == "")
            namaSatuan = "formasi_" + indexFormasi + "_" + SessionUser.id + "_" + UnityEngine.Random.Range(1, 1000);
            else
            namaSatuan = pF0.nama;

            for (int i = 0; i < selectedUnits.Count; i++)
            {
                if (selectedUnits[i].GetComponent<PFormasi>() != null)
                {
                    if (i == 2)
                        pF2 = selectedUnits[2].GetComponent<PFormasi>();
                    if (i == 3)
                        pF3 = selectedUnits[3].GetComponent<PFormasi>();
                    if (i == 4)
                        pF4 = selectedUnits[4].GetComponent<PFormasi>();
                }
            }

            var m2D0 = pF0.marker3D;
            var pos2D0 = m2D0.position;

            for (int i = 0; i < selectedUnits.Count; i++)
            {
                var m2D1 = pF1.marker3D;
                var pos2D1 = m2D1.position;

                OnlineMapsMarker3D m2D2 = null;
                OnlineMapsMarker3D m2D3 = null;
                OnlineMapsMarker3D m2D4 = null;
                Vector2 pos2D2 = new Vector2();
                Vector2 pos2D3 = new Vector2();
                Vector2 pos2D4 = new Vector2();

                var pF = selectedUnits[i].GetComponent<PFormasi>();
                var pMarker2D = pF.marker2D;
                var pMarker3D = pF.marker3D;
                pF.nama = namaSatuan;
                pF.namaFormasi = nama;

                if (pF.jStat == null)
                {
                    var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                    
                    pF2D.formasi = tipeFormasi;
                    pF2D.satuan = satuan;
                    pF2D.urutanFormasi = i;
                    pF2D.nama = namaSatuan;
                    pF2D.namaFormasi = nama;

                    pF2D.listObjekFormasi = new List<Transform>();
                    for (int j = 0; j < selectedUnits.Count; j++)
                        pF2D.listObjekFormasi.Add(selectedUnits[j].GetComponent<PFormasi>().marker3D.instance.transform);
                }
                var pF3D = pMarker3D.instance.GetComponent<PFormasi>();

                Vector2 position2D = new Vector2();
                if (pF.jStat == null)
                    position2D = pMarker2D.position;

                var position3D = pMarker3D.position;

                
                pF3D.formasi = tipeFormasi;
                pF3D.satuan = satuan;
                pF3D.urutanFormasi = i;
                pF3D.nama = namaSatuan;
                pF3D.namaFormasi = nama;

                pF3D.listObjekFormasi = new List<Transform>();
                for (int j = 0; j < selectedUnits.Count; j++)
                    pF3D.listObjekFormasi.Add(selectedUnits[j].GetComponent<PFormasi>().marker3D.instance.transform);

                if (i != 0)
                {
                    if (pF.jStat == null)
                    {
                        var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                        pF2D.jarak = jarak[i - 1];
                    }
                    pF3D.jarak = jarak[i - 1];

                    if (pF.jStat == null)
                    {
                        position2D.x = pos2D0.x + jarakSatuanLng[i - 1];
                        position2D.y = pos2D0.y + jarakSatuanLat[i - 1];
                    }

                    position3D.x = pos2D0.x + jarakSatuanLng[i - 1];
                    position3D.y = pos2D0.y + jarakSatuanLat[i - 1];

                    if (i % 5 == 1)
                    {
                        if (i != 1)
                        {
                            if (pF.jStat == null)
                            {
                                position2D.x = pos2D1.x + jarakSatuanLng[i - 1];
                                position2D.y = pos2D1.y + jarakSatuanLat[i - 1];
                            }

                            position3D.x = pos2D1.x + jarakSatuanLng[i - 1];
                            position3D.y = pos2D1.y + jarakSatuanLat[i - 1];

                            if (pF.jStat == null)
                            {
                                var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                                pF2D.jarak += pF1.jarak;
                            }
                            pF.jarak += pF1.jarak;

                            pF1 = pF;
                        }
                    }
                    else if (i % 5 == 2)
                    {
                        m2D2 = pF2.marker3D;
                        pos2D2 = m2D2.position;
                        if (i != 2)
                        {
                            if (pF.jStat == null)
                            {
                                position2D.x = pos2D2.x + jarakSatuanLng[i - 1];
                                position2D.y = pos2D2.y + jarakSatuanLat[i - 1];
                            }

                            position3D.x = pos2D2.x + jarakSatuanLng[i - 1];
                            position3D.y = pos2D2.y + jarakSatuanLat[i - 1];

                            if (pF.jStat == null)
                            {
                                var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                                pF2D.jarak += pF2.jarak;
                            }
                            pF.jarak += pF2.jarak;

                            pF2 = pF;
                        }
                    }
                    else if (i % 5 == 3 || i % 5 == 0)
                    {
                        m2D3 = pF3.marker3D;
                        pos2D3 = m2D3.position;
                        if (i != 3)
                        {
                            if (pF.jStat == null)
                            {
                                position2D.x = pos2D3.x + jarakSatuanLng[i - 1];
                                position2D.y = pos2D3.y + jarakSatuanLat[i - 1];
                            }

                            position3D.x = pos2D3.x + jarakSatuanLng[i - 1];
                            position3D.y = pos2D3.y + jarakSatuanLat[i - 1];

                            if (pF.jStat == null)
                            {
                                var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                                pF2D.jarak += pF3.jarak;
                            }
                            pF.jarak += pF3.jarak;

                            pF3 = pF;
                        }
                    }
                    else if (i % 5 == 4)
                    {
                        m2D4 = pF4.marker3D;
                        pos2D4 = m2D4.position;
                        if (i != 4)
                        {
                            if (pF.jStat == null)
                            {
                                position2D.x = pos2D4.x + jarakSatuanLng[i - 1];
                                position2D.y = pos2D4.y + jarakSatuanLat[i - 1];
                            }

                            position3D.x = pos2D4.x + jarakSatuanLng[i - 1];
                            position3D.y = pos2D4.y + jarakSatuanLat[i - 1];

                            if (pF.jStat == null)
                            {
                                var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                                pF2D.jarak += pF4.jarak;
                            }
                            pF.jarak += pF4.jarak;

                            pF4 = pF;
                            Debug.Log(position2D);
                        }
                    }

                    if (pF.jStat == null)
                        pMarker2D.position = position2D;
                    pMarker3D.position = position3D;
                    jarakNow = jarak[i - 1];
                }
                pMarker3D.rotation = Quaternion.Euler(Vector3.up * heading);
                var satuanFormasi = SetDataPerSatuanFormasi(pF, satuan, heading, jarakNow);

                if (pF.jStat == null)
                {
                    var pF2D = pF.marker2D.instance.GetComponent<PFormasi>();
                    pF2D.sudut = satuanFormasi.sudut;
                }
                pF3D.sudut = satuanFormasi.sudut;

                listSatuanFormasi.Add(satuanFormasi);
            }

            for (int i = 0; i < selectedUnits.Count; i++)
            {
                var pF = selectedUnits[i].GetComponent<PFormasi>();
                var infoFor = new Info_Formasi();
                infoFor.size = 20;
                infoFor.nama_formasi = nama;
                infoFor.arah = heading;
                infoFor.arrArah = PlotFormasiController.instance.arrArray;
                infoFor.satuan_formasi = listSatuanFormasi;

                if (pF.jStat != null)
                {
                    var info = EntitySatuanInfo.FromJson(pF.jStat.info);
                    infoFor.warna = info.warna;
                }
                else
                    infoFor.warna = pF.stat.warna;

                if(pF.stat != null)
                {
                    var pF2D = pF.marker2D.instance.GetComponent<PFormasi>();
                    pF2D.info = Info_Formasi.ToString(infoFor);
                }

                var pF3D = pF.marker3D.instance.GetComponent<PFormasi>();
                pF3D.info = Info_Formasi.ToString(infoFor);
            }
        }

        public static void Formasi6(List<Transform> selectedUnits, string nama, float heading, PFormasi.formasiType tipeFormasi, PFormasi.satuanJarak satuan, List<float> jarak, List<float> jarakSatuanLng, List<float> jarakSatuanLat)
        {
            float jarakNow = 0;
            var pF0 = selectedUnits[0].GetComponent<PFormasi>();
            var indexFormasi = PlotFormasiController.instance.indexFormasi;
            var listAObj = new List<Transform>();
            var listAJrkLng = new List<float>();
            var listAJrkLat = new List<float>();

            var listBObj = new List<Transform>();
            var listBJrkLng = new List<float>();
            var listBJrkLat = new List<float>();

            var zero = selectedUnits[0].GetComponent<PFormasi>().marker3D;
            var zeroTwo = selectedUnits[3].GetComponent<PFormasi>().marker3D;

            var listSatuanFormasi = new List<Satuan_Formasi>();
            string namaSatuan = "";

            if(pF0.nama == "")
            namaSatuan = "formasi_" + indexFormasi + "_" + SessionUser.id + "_" + UnityEngine.Random.Range(1, 1000);
            else
            namaSatuan = pF0.nama;

            for (int i = 0; i < selectedUnits.Count; i++)
            {
                var pF = selectedUnits[i].GetComponent<PFormasi>();

                if (pF.jStat == null)
                {
                    var pF2D = pF.marker2D.instance.GetComponent<PFormasi>();
                    pF2D.nama = namaSatuan;
                    pF2D.namaFormasi = nama;
                    pF2D.formasi = tipeFormasi;
                    pF2D.satuan = satuan;
                    pF2D.urutanFormasi = i;

                    foreach(var _obj in selectedUnits)
                    pF2D.listObjekFormasi.Add(_obj.GetComponent<PFormasi>().marker2D.instance.transform);
                }

                var pF3D = pF.marker3D.instance.GetComponent<PFormasi>();
                pF3D.nama = namaSatuan;
                pF3D.namaFormasi = nama;
                pF3D.formasi = tipeFormasi;
                pF3D.satuan = satuan;
                pF3D.urutanFormasi = i;

                foreach(var _obj in selectedUnits)
                pF3D.listObjekFormasi.Add(_obj.GetComponent<PFormasi>().marker2D.instance.transform);

                if (i != 0)
                {
                    jarakNow = jarak[i - 1];
                    if (i == 1 || i == 2 || i == 3 || i == 4)
                    {
                        listAObj.Add(selectedUnits[i]);
                        listAJrkLng.Add(jarakSatuanLng[i - 1]);
                        listAJrkLat.Add(jarakSatuanLat[i - 1]);
                    }
                    else
                    {
                        listBObj.Add(selectedUnits[i]);
                        listBJrkLng.Add(jarakSatuanLng[i - 1]);
                        listBJrkLat.Add(jarakSatuanLat[i - 1]);
                    }
                }
            }

            for (int i = 0; i < listAObj.Count; i++)
            {
                var pF = listAObj[i].GetComponent<PFormasi>();

                OnlineMapsMarker3D m2D = pF.marker2D;
                Vector2 pos2D = m2D.position;

                if (pF.jStat == null)
                    m2D = pF.marker2D;
                var m3D = pF.marker3D;

                if (pF.jStat == null)
                {
                    pos2D.x = zero.position.x + listAJrkLng[i];
                    pos2D.y = zero.position.y + listAJrkLat[i];
                }

                var pos3D = m3D.position;
                pos3D.x = zero.position.x + listAJrkLng[i];
                pos3D.y = zero.position.y + listAJrkLat[i];

                m2D.position = pos2D;
                m3D.position = pos3D;
                var satuanFormasi = SetDataPerSatuanFormasi(pF, satuan, heading, jarakNow);

                if (pF.jStat == null)
                {
                    var pF2D = pF.marker2D.instance.GetComponent<PFormasi>();
                    pF2D.sudut = satuanFormasi.sudut;
                }
                var pF3D = pF.marker3D.instance.GetComponent<PFormasi>();
                pF3D.sudut = satuanFormasi.sudut;

                listSatuanFormasi.Add(satuanFormasi);
            }

            var zero3 = listBObj[0];
            var zero4 = listBObj[2];
            for (int i = 1; i < listBObj.Count - 1; i++)
            {
                var pF = listBObj[i].GetComponent<PFormasi>();

                OnlineMapsMarker3D m2D = pF.marker2D;
                Vector2 pos2D = m2D.position;

                if (pF.jStat == null)
                    m2D = pF.marker2D;
                var m3D = pF.marker3D;

                if (pF.jStat == null)
                {
                    pos2D.x = zeroTwo.position.x + listBJrkLng[i];
                    pos2D.y = zeroTwo.position.y + listBJrkLat[i];
                }

                var pos3D = m3D.position;
                pos3D.x = zeroTwo.position.x + listBJrkLng[i];
                pos3D.y = zeroTwo.position.y + listBJrkLat[i];

                m2D.position = pos2D;
                m3D.position = pos3D;
                var satuanFormasi = SetDataPerSatuanFormasi(pF, satuan, heading, jarakNow);

                if (pF.jStat == null)
                {
                    var pF2D = pF.marker2D.instance.GetComponent<PFormasi>();
                    pF2D.sudut = satuanFormasi.sudut;
                }
                var pF3D = pF.marker3D.instance.GetComponent<PFormasi>();
                pF3D.sudut = satuanFormasi.sudut;

                listSatuanFormasi.Add(satuanFormasi);
            }

            for (int i = 0; i < selectedUnits.Count; i++)
            {
                var pF = selectedUnits[i].GetComponent<PFormasi>();
                var infoFor = new Info_Formasi();
                infoFor.size = 20;
                infoFor.nama_formasi = nama;
                infoFor.arah = heading;
                infoFor.arrArah = PlotFormasiController.instance.arrArray;
                infoFor.satuan_formasi = listSatuanFormasi;

                if (pF.jStat != null)
                {
                    var info = EntitySatuanInfo.FromJson(pF.jStat.info);
                    infoFor.warna = info.warna;
                }
                else
                    infoFor.warna = pF.stat.warna;

                if(pF.stat != null)
                {
                    var pF2D = pF.marker2D.instance.GetComponent<PFormasi>();
                    pF2D.info = Info_Formasi.ToString(infoFor);
                }

                var pF3D = pF.marker3D.instance.GetComponent<PFormasi>();
                pF3D.info = Info_Formasi.ToString(infoFor);
            }
        }
        
        public static void Formasi7(List<Transform> selectedUnits, string nama, float heading, PFormasi.formasiType tipeFormasi, PFormasi.satuanJarak satuan, List<float> jarak, List<float> jarakSatuanLng, List<float> jarakSatuanLat)
        {
            float jarakNow = 0;
            var pF0 = selectedUnits[0].GetComponent<PFormasi>();
            var indexFormasi = PlotFormasiController.instance.indexFormasi;
            var m2D0 = pF0.marker3D;
            var pos2D0 = m2D0.position;

            var listSatuanFormasi = new List<Satuan_Formasi>();
            string namaSatuan = "";

            if(pF0.nama == "")
            namaSatuan = "formasi_" + indexFormasi + "_" + SessionUser.id + "_" + UnityEngine.Random.Range(1, 1000);
            else
            namaSatuan = pF0.nama;

            for (int i = 0; i < selectedUnits.Count; i++)
            {
                var pF = selectedUnits[i].GetComponent<PFormasi>();
                var pMarker2D = pF.marker2D;
                var pMarker3D = pF.marker3D;
                pF.nama = namaSatuan;
                pF.namaFormasi = nama;

                if (pF.jStat == null)
                {
                    var pF2D = pF.marker2D.instance.GetComponent<PFormasi>();
                    pF2D.nama = namaSatuan;
                    pF2D.namaFormasi = nama;
                    pF2D.formasi = tipeFormasi;
                    pF2D.satuan = satuan;
                    pF2D.urutanFormasi = i;

                    foreach(var _obj in selectedUnits)
                    pF2D.listObjekFormasi.Add(_obj.GetComponent<PFormasi>().marker2D.instance.transform);
                }

                var pF3D = pF.marker3D.instance.GetComponent<PFormasi>();
                pF3D.nama = namaSatuan;
                pF3D.namaFormasi = nama;
                pF3D.formasi = tipeFormasi;
                pF3D.satuan = satuan;
                pF3D.urutanFormasi = i;

                foreach(var _obj in selectedUnits)
                pF3D.listObjekFormasi.Add(_obj.GetComponent<PFormasi>().marker2D.instance.transform);
                
                Vector2 position2D = new Vector2();
                if (pF.jStat == null)
                    position2D = pMarker2D.position;

                var position3D = pMarker3D.position;
                
                if (i != 0)
                {
                    if (pF.jStat == null)
                    {
                        var pF2D = pMarker2D.instance.GetComponent<PFormasi>();
                        pF2D.jarak = jarak[i - 1];
                    }
                    pF3D.jarak = jarak[i - 1];

                    if (pF.jStat == null)
                    {
                        position2D.x = pos2D0.x + jarakSatuanLng[i - 1];
                        position2D.y = pos2D0.y + jarakSatuanLat[i - 1];
                    }

                    position3D.x = pos2D0.x + jarakSatuanLng[i - 1];
                    position3D.y = pos2D0.y + jarakSatuanLat[i - 1];
                    
                    if (pF.jStat == null)
                        pMarker2D.position = position2D;
                    pMarker3D.position = position3D;
                }
                pMarker3D.rotation = Quaternion.Euler(Vector3.up * heading);
                var satuanFormasi = SetDataPerSatuanFormasi(pF, satuan, heading, jarakNow);

                if (pF.jStat == null)
                {
                    var pF2D = pF.marker2D.instance.GetComponent<PFormasi>();
                    pF2D.sudut = satuanFormasi.sudut;
                }
                pF3D.sudut = satuanFormasi.sudut;

                listSatuanFormasi.Add(satuanFormasi);
            }

            for (int i = 0; i < selectedUnits.Count; i++)
            {
                var pF = selectedUnits[i].GetComponent<PFormasi>();
                var infoFor = new Info_Formasi();
                infoFor.size = 20;
                infoFor.nama_formasi = nama;
                infoFor.arah = heading;
                infoFor.arrArah = PlotFormasiController.instance.arrArray;
                infoFor.satuan_formasi = listSatuanFormasi;

                if (pF.jStat != null)
                {
                    var info = EntitySatuanInfo.FromJson(pF.jStat.info);
                    infoFor.warna = info.warna;
                }
                else
                    infoFor.warna = pF.stat.warna;

                if(pF.stat != null)
                {
                    var pF2D = pF.marker2D.instance.GetComponent<PFormasi>();
                    pF2D.info = Info_Formasi.ToString(infoFor);
                }

                var pF3D = pF.marker3D.instance.GetComponent<PFormasi>();
                pF3D.info = Info_Formasi.ToString(infoFor);
            }
        }
    }
}