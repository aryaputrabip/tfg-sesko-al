using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GlobalVariables;
using HelperPlotting;
using ObjectStat;
using UnityEngine;

using static HelperPlotting.FormasiType;
using static ListOfSimpleFunctions.Functions;
namespace Plot.FormasiPlot
{
    public static class PerhitunganFormasiV2
    {
        ///<summary>
        ///Konversi Jarak untuk menjadi Degree -- Izzan
        ///</summary>
        ///<param name = "satuanJarak">
        ///Satuan Jarak yang digunakan
        ///</param>
        ///<param name = "jarak">
        ///List Jarak yang telah di Input
        ///</param>
        public static List<float> KonversiJarak(List<float> jarak, satuanJarak satuanJarak)
        {
            var newJarak = new float[jarak.Count];
            for (int i = 0; i < jarak.Count; i++)
            {
                if (satuanJarak == satuanJarak.NauticalMiles)
                    newJarak[i] = jarak[i] / 60f;
                else if (satuanJarak == satuanJarak.Kilometer)
                    newJarak[i] = jarak[i] / 1.852f / 60f;
                else if (satuanJarak == satuanJarak.Feet)
                    newJarak[i] = jarak[i] / 6076f / 60f;
                else if (satuanJarak == satuanJarak.Yard)
                    newJarak[i] = jarak[i] / 2025f / 60f;
                else if (satuanJarak == satuanJarak.Meter)
                    newJarak[i] = jarak[i] / 1852f / 60f;
            }
            return newJarak.ToList();
        }

        ///<summary>
        ///Konversi Jarak untuk menjadi Degree -- Izzan
        ///</summary>
        ///<param name = "satuanJarak">
        ///Satuan Jarak yang digunakan
        ///</param>
        ///<param name = "jarak">
        ///Jarak yang ingin di Konversi
        ///</param>
        public static float KonversiJarak(float jarak, satuanJarak satuanJarak)
        {
            if (satuanJarak == satuanJarak.NauticalMiles)
                jarak = jarak / 60f;
            else if (satuanJarak == satuanJarak.Kilometer)
                jarak = jarak / 1.852f / 60f;
            else if (satuanJarak == satuanJarak.Feet)
                jarak = jarak / 6076f / 60f;
            else if (satuanJarak == satuanJarak.Yard)
                jarak = jarak / 2025f / 60f;
            else if (satuanJarak == satuanJarak.Meter)
                jarak = jarak / 1852f / 60f;

            return jarak;
        }

        ///<summary>
        ///Perhitungan Jarak untuk Mendapatkan Degree Longitude dan Latitude -- Izzan
        ///</summary>
        ///<param name = "formasi">
        ///Tipe Formasi yang Digunakan
        ///</param>
        ///<param name = "jarak">
        ///List Jarak yang telah di Input
        ///</param>
        ///<param name = "arah">
        ///Arah yang dihadap Marker Utama
        ///</param>
        ///<param name = "degree">
        ///Tipe Degree yang di Butuhkan
        ///</param>
        public static List<float> Jarak(List<float> jarak, float arah, formasiType formasi, string degree, List<int> arrArah = null)
        {
            arrArah ??= Heading(jarak.Count, formasi, arah);

            float newJarak = 0;
            List<float> newJarakList = new();

            for (int i = 0; i < jarak.Count; i++)
            {
                var heading = arrArah[i];
                if (degree == "Latitude")
                    newJarak = jarak[i] * Mathf.Cos(heading * Mathf.Deg2Rad);
                else if (degree == "Longitude")
                    newJarak = jarak[i] * Mathf.Sin(heading * Mathf.Deg2Rad);

                Debug.Log("urutan jarak: " + i + " heading: " + heading + " new jarak: " + newJarak + " degree: " + degree);

                newJarakList.Add(newJarak);
            }

            return newJarakList;
        }

        ///<summary>
        ///Perhitungan Arah setiap Marker sesuai dengan Formasi -- Izzan
        ///</summary>
        ///<param name = "formasi">
        ///Tipe Formasi yang Digunakan
        ///</param>
        ///<param name = "unitsCount">
        ///Banyaknya Unit yang ada di dalam Formasi
        ///</param>
        ///<param name = "arah">
        ///Arah yang dihadap Marker Utama
        ///</param>
        ///<param name = "heading">
        ///Hasil Akhir
        ///</param>
        public static List<int> Heading(float unitsCount, formasiType formasi, float arah, float heading = 0)
        {
            float[] hArr = new float[] { 0, 90, 180, 270 };
            float[] hArr2 = new float[] { 45, 135, 225, 315 };

            List<int> arrArah = new();
            for(int i = 0; i < unitsCount; i++)
            {
                int j = i + 1;

                if (formasi == formasiType.Belah_Ketupat)
                {
                    if (j % 4 == 1)
                        heading = hArr[0];
                    else if (j % 4 == 2)
                        heading = hArr[1];
                    else if (j % 4 == 3)
                        heading = hArr[2];
                    else if (j % 4 == 0)
                        heading = hArr[3];
                }
                else if (formasi == formasiType.Flank_Kanan)
                {
                    if (j % 2 == 1)
                        heading = hArr2[3];
                    else if (j % 2 == 0)
                        heading = hArr2[1];
                }
                else if (formasi == formasiType.Flank_Kiri)
                {
                    if (j % 2 == 1)
                        heading = hArr2[2];
                    else if (j % 2 == 0)
                        heading = hArr2[0];
                }
                else if (formasi == formasiType.Sejajar)
                {
                    if (j % 4 == 1 || j % 4 == 2)
                        heading = hArr[1];
                    else if (j % 4 == 3 || j % 4 == 0)
                        heading = hArr[3];
                }
                else if (formasi == formasiType.Serial)
                {
                    if (j % 2 == 1)
                        heading = hArr[0];
                    else if (j % 2 == 0)
                        heading = hArr[2];
                }
                else if (formasi == formasiType.Panah)
                {
                    if (j % 5 == 1)
                        heading = hArr[0];
                    else if (j % 5 == 2)
                        heading = hArr[1];
                    else if (j % 5 == 3)
                        heading = hArr[2];
                    else if (j % 5 == 4)
                        heading = hArr[3];
                    else if (j % 5 == 0)
                        heading = hArr[2];
                }
                else if (formasi == formasiType.Kerucut)
                {
                    if (j % 8 == 1)
                        heading = hArr[0];
                    else if (j % 8 == 2)
                        heading = hArr[1];
                    else if (j % 8 == 3)
                        heading = hArr[2];
                    else if (j % 8 == 4)
                        heading = hArr[3];
                    else if (j % 8 == 5)
                        heading = hArr[1];
                    else if (j % 8 == 6)
                        heading = hArr[3];
                    else if (j % 8 == 7)
                        heading = hArr[1];
                    else if (j % 8 == 0)
                        heading = hArr[3];
                }

                heading += arah;
                arrArah.Add((int)heading);
            }

            return arrArah;
        }

        ///<summary>
        ///Sorting Jarak Berdasarkan Formasi -- Izzan
        ///</summary>
        ///<param name = "formasi">
        ///Tipe Formasi yang Digunakan
        ///</param>
        ///<param name = "unitsCount">
        ///Banyaknya Unit yang ada di dalam Formasi
        ///</param>
        ///<param name = "jarakList">
        ///List Jarak yang telah di Input
        ///</param>
        public static List<float> Jarak(float unitsCount, formasiType formasi, List<float> jarakList)
        {
            List<float> arrJarak = new();
            float p1 = 0, p2 = 0, p3 = 0, p4 = 0, p5 = 0, p6 = 0, p7 = 0, p8 = 0, p9 = 0;
            for(int i = 0; i < unitsCount - 1; i++)
            {
                int j = i + 1;
                float jarak = jarakList[i];
                float newJarak = 0;

                if (formasi == formasiType.Belah_Ketupat)
                {
                    if (j % 4 == 1)
                    {
                        newJarak = jarak + p1;
                        p1 = newJarak;
                    }
                    else if (j % 4 == 2)
                    {
                        newJarak = jarak + p2;
                        p2 = newJarak;
                    }
                    else if (j % 4 == 3)
                    {
                        newJarak = jarak + p3;
                        p3 = newJarak;
                    }
                    else if (j % 4 == 0)
                    {
                        newJarak = jarak + p4;
                        p4 = newJarak;
                    }
                }
                else if (formasi == formasiType.Flank_Kanan)
                {
                    if (j % 2 == 1)
                    {
                        newJarak = jarak + p1;
                        p1 = newJarak;
                    }
                    else if (j % 2 == 0)
                    {
                        newJarak = jarak + p2;
                        p2 = newJarak;
                    }
                }
                else if (formasi == formasiType.Flank_Kiri)
                {
                    if (j % 2 == 1)
                    {
                        newJarak = jarak + p1;
                        p1 = newJarak;
                    }
                    else if (j % 2 == 0)
                    {
                        newJarak = jarak + p2;
                        p2 = newJarak;
                    }
                }
                else if (formasi == formasiType.Sejajar)
                {
                    if (j % 4 == 1 || j % 4 == 2)
                    {
                        newJarak = jarak + p1;
                        p1 = newJarak;
                    }
                    else if (j % 4 == 3 || j % 4 == 0)
                    {
                        newJarak = jarak + p2;
                        p2 = newJarak;
                    }
                }
                else if (formasi == formasiType.Serial)
                {
                    if (j % 2 == 1)
                    {
                        newJarak = jarak + p1;
                        p1 = newJarak;
                    }
                    else if (j % 2 == 0)
                    {
                        newJarak = jarak + p2;
                        p2 = newJarak;
                    }
                }
                else if (formasi == formasiType.Panah)
                {
                    if (j % 5 == 1)
                    {
                        newJarak = jarak + p1;
                        p1 = newJarak;
                    }
                    else if (j % 5 == 2)
                    {
                        newJarak = jarak + p2;
                        p2 = newJarak;
                    }
                    else if (j % 5 == 3 || j % 5 == 0)
                    {
                        newJarak = jarak + p3;
                        p3 = newJarak;
                    }
                    else if (j % 5 == 4)
                    {
                        newJarak = jarak + p4;
                        p4 = newJarak;
                    }
                }
                else if (formasi == formasiType.Kerucut)
                {
                    newJarak = jarak;
                }

                arrJarak.Add((int)newJarak);
            }

            return arrJarak;
        }

        ///<summary>
        ///Set Data tiap Satuan dalam Formasi -- Izzan
        ///</summary>
        ///<param name = "selectedUnits">
        ///Units Dalam Formasi
        ///</param>
        ///<param name = "type">
        ///Tipe Formasi yang Digunakan
        ///</param>
        ///<param name = "heading">
        ///Arah yang dihadap Marker Utama
        ///</param>
        ///<param name = "arrArah">
        ///List Arah tiap Marker
        ///</param>
        ///<param name = "jarakList">
        ///List Jarak tiap Marker
        ///</param>
        ///<param name = "nama">
        ///Nama Formasi
        ///</param>
        ///<param name = "namaFormasi">
        ///Nama Satuan Formasi
        ///</param>
        public static void SetDataFormasi(List<Transform> selectedUnits, formasiType type, satuanJarak satuan, string nama, string namaFormasi, float heading, List<int> arrArah, List<float> jarakList)
        {
            var newJarakList = Jarak(selectedUnits.Count, type, jarakList);
            for(int i = 0; i < selectedUnits.Count; i++)
            {
                var pF = selectedUnits[i].GetComponent<PFormasiV2>();
                // Debug.Log(pF.markers.marker3D.rotationY);
                pF.markers.marker3D.rotationY = 180 + heading;
                
                var fS = pF.formasi;
                fS.id = i;
                fS.nama = nama;
                fS.namaFormasi = namaFormasi;
                fS.type = type;
                fS.satuan = satuan;
                fS.arah = i == 0 ? (int)heading : arrArah[i - 1];
                fS.arahUtama = (int)heading;
                fS.jarak = i == 0 ? 0 : newJarakList[i - 1];
                fS.jarakAntarSatuan = i == 0 ? 0 : jarakList[i - 1];
                fS.unitsInFormasi = new();
                fS.unitsInFormasi.AddRange(selectedUnits);
            }
        }

        ///<summary>
        ///Create Entity Formasi -- Izzan
        ///</summary>
        ///<param name = "selectedUnits">
        ///Units Dalam Formasi
        ///</param>
        ///<param name = "type">
        ///Tipe Formasi yang Digunakan
        ///</param>
        ///<param name = "arrArah">
        ///List Arah tiap Marker
        ///</param>
        ///<param name = "nama">
        ///Nama Formasi
        ///</param>
        ///<param name = "namaSatuan">
        ///Nama Satuan Formasi
        ///</param>
        public static EntityFormasiV2 CreateEntityFormasi(string nama, string namaSatuan, formasiType type, List<int> arrArah, List<Transform> selectedUnits)
        {
            EntityFormasiV2 entity = new()
            {
                id_user = SessionUser.id.ToString(),
                dokumen = SkenarioAktif.ID_DOCUMENT,
                nama    = namaSatuan,
                info_formasi = new()
                {
                    nama_formasi = nama,
                    warna = SessionUser.nama_asisten == "ASOPS" ? "blue" : "red",
                    size = "20",
                    id_point = namaSatuan,
                    arrArah = arrArah,
                    satuan_formasi = SetDataSatuan(selectedUnits)
                },
                size    = "20",
                jenis_formasi = JenisFormasi(type).ToString(),
                warna = SessionUser.nama_asisten == "ASOPS" ? "blue" : "red"
            };

            return entity;
        }

        ///<summary>
        ///Set Data tiap Satuan dalam Formasi untuk Konversi menjadi Json -- Izzan
        ///</summary>
        ///<param name = "selectedUnits">
        ///Units Dalam Formasi
        ///</param>
        public static List<SatuanFormasiV2> SetDataSatuan(List<Transform> selectedUnits)
        {
            List<SatuanFormasiV2> satuanList = new();
            for(int i = 0; i < selectedUnits.Count; i++)
            {
                var pF = selectedUnits[i].GetComponent<PFormasiV2>();
                var fS = pF.formasi;
                var satuan = new SatuanFormasiV2()
                {
                    id_point = selectedUnits[i].name,
                    nama = pF.namaSatuan,
                    icon = new()
                    {
                        nama = pF.fontTaktis,
                        index = pF.index,
                        keterangan = pF.keterangan,
                        grup = pF.grup,
                        label_style = pF.labelStylePasukan
                    },
                    color = SessionUser.nama_asisten == "ASOPS" ? "blue" : "red",
                    lat = pF.markers.marker3D.position.y,
                    lng = pF.markers.marker3D.position.x,
                    inti = i == 0 ? 1 : 0,
                    id = i,
                    jarak = fS.jarak.ToString(),
                    sudut = fS.arah,
                    satuan = JenisSatuanJarak(fS.satuan),
                    new_jarak = i == 0 ? 0 : KonversiJarak(fS.jarak, fS.satuan)
                };
                satuanList.Add(satuan);
            }

            return satuanList;
        }

        ///<summary>
        ///Sorting Satuan Jarak menjadi string -- Izzan
        ///</summary>
        ///<param name = "satuan">
        ///Tipe Satuan Jarak berdasarkan enum
        ///</param>
        ///<param name = "_satuan">
        ///Tipe Satuan Jarak berdasarkan string
        ///</param>
        public static string JenisSatuanJarak(satuanJarak satuan, string _satuan = "")
        {            
            switch(satuan)
            {
                case satuanJarak.NauticalMiles:
                    _satuan = "nm";
                    break;
                case satuanJarak.Kilometer:
                    _satuan = "km";
                    break;
                case satuanJarak.Feet:
                    _satuan = "foot";
                    break;
                case satuanJarak.Yard:
                    _satuan = "yard";
                    break;
                case satuanJarak.Meter:
                    _satuan = "m";
                    break;
            }
            return _satuan;
        }

        ///<summary>
        ///Sorting Satuan Jarak menjadi string -- Izzan
        ///</summary>
        ///<param name = "satuan">
        ///Tipe Satuan Jarak berdasarkan enum
        ///</param>
        ///<param name = "_satuan">
        ///Tipe Satuan Jarak berdasarkan string
        ///</param>
        public static satuanJarak JenisSatuanJarakString(string _satuan, satuanJarak satuan = satuanJarak.NauticalMiles)
        {
            switch(_satuan)
            {
                case "nm":
                    satuan = satuanJarak.NauticalMiles;
                    break;
                case "km":
                    satuan = satuanJarak.Kilometer;
                    break;
                case "foot":
                    satuan = satuanJarak.Feet;
                    break;
                case "yard":
                    satuan = satuanJarak.Yard;
                    break;
                case "m":
                    satuan = satuanJarak.Meter;
                    break;
            }
            return satuan;
        }

        ///<summary>
        ///Sorting Satuan Jarak menjadi int -- Izzan
        ///</summary>
        ///<param name = "satuan">
        ///Tipe Satuan Jarak berdasarkan enum
        ///</param>
        ///<param name = "_satuan">
        ///Tipe Satuan Jarak berdasarkan string
        ///</param>
        public static int JenisSatuanJarak(satuanJarak satuan, bool isInt, int _satuan = 0)
        {
            switch(satuan)
            {
                case satuanJarak.NauticalMiles:
                    _satuan = 1;
                    break;
                case satuanJarak.Kilometer:
                    _satuan = 2;
                    break;
                case satuanJarak.Feet:
                    _satuan = 3;
                    break;
                case satuanJarak.Yard:
                    _satuan = 4;
                    break;
                case satuanJarak.Meter:
                    _satuan = 5;
                    break;
            }
            return _satuan;
        }

        ///<summary>
        ///Sorting Formasi menjadi int -- Izzan
        ///</summary>
        ///<param name = "type">
        ///Tipe Formasi berdasarkan enum
        ///</param>
        ///<param name = "_type">
        ///Tipe Formasi berdasarkan int
        ///</param>
        public static int JenisFormasi(formasiType type, int _type = 0)
        {
            switch(type)
            {
                case formasiType.Belah_Ketupat:
                    _type = 1;
                    break;
                case formasiType.Flank_Kanan:
                    _type = 2;
                    break;
                case formasiType.Flank_Kiri:
                    _type = 3;
                    break;
                case formasiType.Sejajar:
                    _type = 4;
                    break;
                case formasiType.Serial:
                    _type = 5;
                    break;
                case formasiType.Panah:
                    _type = 6;
                    break;
                case formasiType.Kerucut:
                    _type = 7;
                    break;
            }

            return _type;
        }

        ///<summary>
        ///Sorting untuk Mendapatkan Perhitungan sesuai Formasi yang di Pakai -- Izzan
        ///</summary>
        ///<param name = "selectedUnits">
        ///Units Dalam Formasi
        ///</param>
        ///<param name = "jarakDegLng">
        ///Jarak Degree Yang Sudah Terkonversi ke Longitude
        ///</param>
        ///<param name = "jarakDegLng">
        ///Jarak Degree Yang Sudah Terkonversi ke Latitude
        ///</param>
        ///<param name = "type">
        ///Tipe Formasi yang Digunakan
        ///</param>
        public static void ToFormation(List<Transform> selectedUnits, List<float> jarakDegLng, List<float> jarakDegLat, formasiType type)
        {
            switch(type)
            {
                case formasiType.Belah_Ketupat:
                    Formasi1(selectedUnits, jarakDegLng, jarakDegLat);
                    break;
                case formasiType.Flank_Kanan: case formasiType.Flank_Kiri: case formasiType.Serial:
                    Formasi2(selectedUnits, jarakDegLng, jarakDegLat);
                    break;
                case formasiType.Sejajar:
                    Formasi3(selectedUnits, jarakDegLng, jarakDegLat);
                    break;
                case formasiType.Panah:
                    Formasi4(selectedUnits, jarakDegLng, jarakDegLat);
                    break;
                case formasiType.Kerucut:
                    Formasi5(selectedUnits, jarakDegLng, jarakDegLat);
                    break;
                case formasiType.Custom:
                    Formasi6(selectedUnits, jarakDegLng, jarakDegLat);
                    break;
            }
        }

        ///<summary>
        ///Perhitungan Formasi Belah Ketupat V2 -- Izzan
        ///</summary>
        ///<param name = "selectedUnits">
        ///Units Dalam Formasi
        ///</param>
        ///<param name = "jarakDegLng">
        ///Jarak Degree Yang Sudah Terkonversi ke Longitude
        ///</param>
        ///<param name = "jarakDegLng">
        ///Jarak Degree Yang Sudah Terkonversi ke Latitude
        ///</param>
        public static void Formasi1(List<Transform> selectedUnits, List<float> jarakDegLng, List<float> jarakDegLat)
        {
            var marker0 = selectedUnits[0].GetComponent<PFormasiV2>().markers.marker3D;
            OnlineMapsMarker3D m1 = null;
            OnlineMapsMarker3D m2 = null;
            OnlineMapsMarker3D m3 = null;
            OnlineMapsMarker3D m4 = null;            

            for(int i = 0; i < selectedUnits.Count; i++)
            {
                var pF = selectedUnits[i].GetComponent<PFormasiV2>();
                var marker = pF.markers.marker3D;

                if(i != 0)
                {
                    var j = i - 1;
                    Vector2 newPosition = new()
                    {
                        x = jarakDegLng[j],
                        y = jarakDegLat[j]
                    };
                    Debug.Log(jarakDegLng[j] + ", " + jarakDegLat[j]);

                    switch(i % 4)
                    {
                        case 1:
                            marker.position = (i != 1 ? m1.position : marker0.position) + newPosition;
                            m1 = marker;
                            break;
                        case 2:
                            marker.position = (i != 2 ? m2.position : marker0.position) + newPosition;
                            m2 = marker;
                            break;
                        case 3:
                            marker.position = (i != 3 ? m3.position : marker0.position) + newPosition;
                            m3 = marker;
                            break;
                        case 0:
                            marker.position = (i != 4 ? m4.position : marker0.position) + newPosition;
                            m4 = marker;
                            break;
                    }
                }
            }
        }

        ///<summary>
        ///Perhitungan Formasi Flank Kanan, Flank Kiri, Serial V2 -- Izzan
        ///</summary>
        ///<param name = "selectedUnits">
        ///Units Dalam Formasi
        ///</param>
        ///<param name = "jarakDegLng">
        ///Jarak Degree Yang Sudah Terkonversi ke Longitude
        ///</param>
        ///<param name = "jarakDegLng">
        ///Jarak Degree Yang Sudah Terkonversi ke Latitude
        ///</param>
        public static void Formasi2(List<Transform> selectedUnits, List<float> jarakDegLng, List<float> jarakDegLat)
        {
            var marker0 = selectedUnits[0].GetComponent<PFormasiV2>().markers.marker3D;
            OnlineMapsMarker3D m1 = null;
            OnlineMapsMarker3D m2 = null;           

            for(int i = 0; i < selectedUnits.Count; i++)
            {
                var pF = selectedUnits[i].GetComponent<PFormasiV2>();
                var marker = pF.markers.marker3D;

                if(i != 0)
                {
                    var j = i - 1;
                    Vector2 newPosition = new()
                    {
                        x = jarakDegLng[j],
                        y = jarakDegLat[j]
                    };
                    Debug.Log(jarakDegLng[j] + ", " + jarakDegLat[j]);

                    switch(i % 2)
                    {
                        case 1:
                            marker.position = (i != 1 ? m1.position : marker0.position) + newPosition;
                            m1 = marker;
                            break;
                        case 0:
                            marker.position = (i != 2 ? m2.position : marker0.position) + newPosition;
                            m2 = marker;
                            break;
                    }
                }
            }
        }

        ///<summary>
        ///Perhitungan Formasi Sejajar -- Izzan
        ///</summary>
        ///<param name = "selectedUnits">
        ///Units Dalam Formasi
        ///</param>
        ///<param name = "jarakDegLng">
        ///Jarak Degree Yang Sudah Terkonversi ke Longitude
        ///</param>
        ///<param name = "jarakDegLng">
        ///Jarak Degree Yang Sudah Terkonversi ke Latitude
        ///</param>
        public static void Formasi3(List<Transform> selectedUnits, List<float> jarakDegLng, List<float> jarakDegLat)
        {
            var marker0 = selectedUnits[0].GetComponent<PFormasiV2>().markers.marker3D;
            OnlineMapsMarker3D m1 = null;
            OnlineMapsMarker3D m2 = null;           

            for(int i = 0; i < selectedUnits.Count; i++)
            {
                var pF = selectedUnits[i].GetComponent<PFormasiV2>();
                var marker = pF.markers.marker3D;

                if(i != 0)
                {
                    var j = i - 1;
                    Vector2 newPosition = new()
                    {
                        x = jarakDegLng[j],
                        y = jarakDegLat[j]
                    };
                    Debug.Log(jarakDegLng[j] + ", " + jarakDegLat[j]);

                    switch(i % 4)
                    {
                        case 1: case 2:
                            marker.position = (i != 1 ? m1.position : marker0.position) + newPosition;
                            m1 = marker;
                            break;
                        case 3: case 0:
                            marker.position = (i != 3 ? m2.position : marker0.position) + newPosition;
                            m2 = marker;
                            break;
                    }
                }
            }
        }

        ///<summary>
        ///Perhitungan Formasi Panah -- Izzan
        ///</summary>
        ///<param name = "selectedUnits">
        ///Units Dalam Formasi
        ///</param>
        ///<param name = "jarakDegLng">
        ///Jarak Degree Yang Sudah Terkonversi ke Longitude
        ///</param>
        ///<param name = "jarakDegLng">
        ///Jarak Degree Yang Sudah Terkonversi ke Latitude
        ///</param>
        public static void Formasi4(List<Transform> selectedUnits, List<float> jarakDegLng, List<float> jarakDegLat)
        {
            var marker0 = selectedUnits[0].GetComponent<PFormasiV2>().markers.marker3D;
            OnlineMapsMarker3D m1 = null;
            OnlineMapsMarker3D m2 = null;
            OnlineMapsMarker3D m3 = null;
            OnlineMapsMarker3D m4 = null;

            for(int i = 0; i < selectedUnits.Count; i++)
            {
                var pF = selectedUnits[i].GetComponent<PFormasiV2>();
                var marker = pF.markers.marker3D;

                if(i != 0)
                {
                    var j = i - 1;
                    Vector2 newPosition = new()
                    {
                        x = jarakDegLng[j],
                        y = jarakDegLat[j]
                    };
                    Debug.Log(jarakDegLng[j] + ", " + jarakDegLat[j]);

                    switch(i % 5)
                    {
                        case 1:
                            marker.position = (i != 1 ? m1.position : marker0.position) + newPosition;
                            m1 = marker;
                            break;
                        case 2:
                            marker.position = (i != 2 ? m2.position : marker0.position) + newPosition;
                            m2 = marker;
                            break;
                        case 3: case 0:
                            marker.position = (i != 3 ? m3.position : marker0.position) + newPosition;
                            m3 = marker;
                            break;
                        case 4:
                            marker.position = (i != 4 ? m4.position : marker0.position) + newPosition;
                            m4 = marker;
                            break;
                    }
                }
            }
        }

        ///<summary>
        ///Perhitungan Formasi Kerucut -- Izzan
        ///</summary>
        ///<param name = "selectedUnits">
        ///Units Dalam Formasi
        ///</param>
        ///<param name = "jarakDegLng">
        ///Jarak Degree Yang Sudah Terkonversi ke Longitude
        ///</param>
        ///<param name = "jarakDegLng">
        ///Jarak Degree Yang Sudah Terkonversi ke Latitude
        ///</param>
        public static void Formasi5(List<Transform> selectedUnits, List<float> jarakDegLng, List<float> jarakDegLat)
        {
            var marker0 = selectedUnits[0].GetComponent<PFormasiV2>().markers.marker3D;
            var list1 = new List<Transform>();
            var list2 = new List<Transform>();
            var listVector1 = new List<Vector2>();
            var listVector2 = new List<Vector2>();
            OnlineMapsMarker3D m1 = null;

            for(int i = 0; i < selectedUnits.Count; i++)
            {
                var pF = selectedUnits[i].GetComponent<PFormasiV2>();
                var marker = pF.markers.marker3D;

                if(i != 0)
                {
                    var j = i - 1;
                    Vector2 newPosition = new()
                    {
                        x = jarakDegLng[j],
                        y = jarakDegLat[j]
                    };
                    Debug.Log(jarakDegLng[j] + ", " + jarakDegLat[j]);

                    switch(i % 8)
                    {
                        case 1: case 2: case 3: case 4:
                            list1.Add(marker.transform);
                            listVector1.Add(newPosition);
                            if(i == 3) m1 = marker;
                            break;
                        case 5: case 6: case 7: case 0:
                            list2.Add(marker.transform);
                            listVector2.Add(newPosition);
                            break;
                    }
                }
            }

            for(int i = 0; i < list1.Count; i++)
            {
                var pF = list1[i].GetComponent<PFormasiV2>();
                var marker = pF.markers.marker3D;
                Vector2 newPosition = listVector1[i];
                marker.position = marker0.position + newPosition;
            }
            
            var _list1 = new List<Transform>(list1);
            for(int i = 0; i < list2.Count; i++)
            {
                var pF = list2[i].GetComponent<PFormasiV2>();
                var marker = pF.markers.marker3D;
                var j = i + 1;
                Vector2 newPosition = listVector2[i];
                marker.position = m1.position + newPosition;
                if(j % 4 == 0)
                {
                    if(_list1.Count > 3) _list1.RemoveRange(0, 3);
                    var _m1 = _list1.Count >= 3 ? _list1[3].GetComponent<PFormasiV2>().markers.marker3D : null;
                    m1 = _m1 ?? m1;
                }
            }
        }

        ///<summary>
        ///Perhitungan Formasi Custom -- Izzan
        ///</summary>
        ///<param name = "selectedUnits">
        ///Units Dalam Formasi
        ///</param>
        ///<param name = "jarakDegLng">
        ///Jarak Degree Yang Sudah Terkonversi ke Longitude
        ///</param>
        ///<param name = "jarakDegLng">
        ///Jarak Degree Yang Sudah Terkonversi ke Latitude
        ///</param>
        public static void Formasi6(List<Transform> selectedUnits, List<float> jarakDegLng, List<float> jarakDegLat)
        {
            var marker0 = selectedUnits[0].GetComponent<PFormasiV2>().markers.marker3D;

            for(int i = 0; i < selectedUnits.Count; i++)
            {
                var pF = selectedUnits[i].GetComponent<PFormasiV2>();
                var marker = pF.markers.marker3D;

                if(i != 0)
                {
                    var j = i - 1;
                    Vector2 newPosition = new()
                    {
                        x = jarakDegLng[j],
                        y = jarakDegLat[j]
                    };
                    Debug.Log(jarakDegLng[j] + ", " + jarakDegLat[j]);

                    marker.position = marker0.position + newPosition;
                }
            }
        }
    }
}
