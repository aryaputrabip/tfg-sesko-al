using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ObjectStat;
using UnityEngine;
using static HelperPlotting.FormasiType;

namespace Plot.FormasiPlot
{
    public static class PerhitunganMisiFormasi
    {
        public static float GetAngle(Vector2 point1, Vector2 point2)
        {
            int zoom = OnlineMaps.instance.zoom;
            int maxX = 1 << (zoom - 1);
            // Calculate the tile position of locations.
            double userTileX, userTileY, markerTileX, markerTileY;
            OnlineMaps.instance.projection.CoordinatesToTile(point1.x, point1.y, zoom, out userTileX, out userTileY);
            OnlineMaps.instance.projection.CoordinatesToTile(point2.x, point2.y, zoom, out markerTileX, out markerTileY);

            // Calculate the angle between locations.
            double angle = OnlineMapsUtils.Angle2D(userTileX, userTileY, markerTileX, markerTileY);
            if (Math.Abs(userTileX - markerTileX) > maxX) angle = 360 - angle;

            return (float)angle;
        }

        public static List<int> ArahList(List<Transform> unitsInFormasi)
        {
            List<int> arahList = new();

            for(int i = 0; i < unitsInFormasi.Count; i++)
            {
                var pF = unitsInFormasi[i].GetComponent<PFormasiV2>();
                var fS = pF.formasi;
                if(i != 0)
                arahList.Add(fS.arah);
            }

            return arahList;
        }
        public static List<float> JarakList(List<Transform> unitsInFormasi)
        {
            List<float> jarakList = new();

            for(int i = 0; i < unitsInFormasi.Count; i++)
            {
                var pF = unitsInFormasi[i].GetComponent<PFormasiV2>();
                var fS = pF.formasi;
                if(i != 0)
                jarakList.Add(fS.jarakAntarSatuan);
            }

            return jarakList;
        }

        ///<summary>
        ///Sorting untuk Mendapatkan Perhitungan sesuai Formasi yang di Pakai -- Izzan
        ///</summary>
        ///<param name = "selectedUnits">
        ///Units Dalam Formasi
        ///</param>
        ///<param name = "jarakDegLng">
        ///Jarak Degree Yang Sudah Terkonversi ke Longitude
        ///</param>
        ///<param name = "jarakDegLng">
        ///Jarak Degree Yang Sudah Terkonversi ke Latitude
        ///</param>
        ///<param name = "type">
        ///Tipe Formasi yang Digunakan
        ///</param>
        public static List<Vector2> ToMission(Vector2 originPoint, int numberOfUnits, List<float> jarakDegLng, List<float> jarakDegLat, formasiType type)
        {
            var routePoint = new List<Vector2>();
            switch(type)
            {
                case formasiType.Belah_Ketupat:
                    routePoint = Misi1(originPoint, numberOfUnits, jarakDegLng, jarakDegLat);
                    break;
                case formasiType.Flank_Kanan: case formasiType.Flank_Kiri: case formasiType.Serial:
                    routePoint = Misi2(originPoint, numberOfUnits, jarakDegLng, jarakDegLat);
                    break;
                case formasiType.Sejajar:
                    routePoint = Misi3(originPoint, numberOfUnits, jarakDegLng, jarakDegLat);
                    break;
                case formasiType.Panah:
                    routePoint = Misi4(originPoint, numberOfUnits, jarakDegLng, jarakDegLat);
                    break;
                case formasiType.Kerucut:
                    routePoint = Misi5(originPoint, numberOfUnits, jarakDegLng, jarakDegLat);
                    break;
                case formasiType.Custom:
                    routePoint = Misi6(originPoint, numberOfUnits, jarakDegLng, jarakDegLat);
                    break;
            }

            return routePoint;
        }

        ///<summary>
        ///Perhitungan Formasi Belah Ketupat V2 -- Izzan
        ///</summary>
        ///<param name = "originPoint">
        ///Titik Inti Point
        ///</param>
        ///<param name = "numberOfUnits">
        ///Jumlah Unit dalam Formasi
        ///</param>
        ///<param name = "jarakDegLng">
        ///Jarak Degree Yang Sudah Terkonversi ke Longitude
        ///</param>
        ///<param name = "jarakDegLng">
        ///Jarak Degree Yang Sudah Terkonversi ke Latitude
        ///</param>
        public static List<Vector2> Misi1(Vector2 originPoint, int numberOfUnits, List<float> jarakDegLng, List<float> jarakDegLat)
        {
            Vector2 p1 = new();
            Vector2 p2 = new();
            Vector2 p3 = new();
            Vector2 p4 = new();
            Vector2[] routePoint = new Vector2[numberOfUnits];
            routePoint[0] = originPoint;

            for (int i = 0; i < routePoint.Length; i++)
            {
                if(i != 0)
                {
                    var j = i - 1;
                    Vector2 newPosition = new()
                    {
                        x = jarakDegLng[j],
                        y = jarakDegLat[j]
                    };

                    switch(i % 4)
                    {
                        case 1:
                            routePoint[i] = (i != 1 ? p1 : originPoint) + newPosition;
                            p1 = routePoint[i];
                            break;
                        case 2:
                            routePoint[i] = (i != 2 ? p2 : originPoint) + newPosition;
                            p2 = routePoint[i];
                            break;
                        case 3:
                            routePoint[i] = (i != 3 ? p3 : originPoint) + newPosition;
                            p3 = routePoint[i];
                            break;
                        case 0:
                            routePoint[i] = (i != 4 ? p4 : originPoint) + newPosition;
                            p4 = routePoint[i];
                            break;
                    }
                }
            }

            return routePoint.ToList();
        }

        ///<summary>
        ///Perhitungan Formasi Flank Kanan, Flank Kiri, Serial V2 -- Izzan
        ///</summary>
        ///<param name = "originPoint">
        ///Titik Inti Point
        ///</param>
        ///<param name = "numberOfUnits">
        ///Jumlah Unit dalam Formasi
        ///</param>
        ///<param name = "jarakDegLng">
        ///Jarak Degree Yang Sudah Terkonversi ke Latitude
        ///</param>
        public static List<Vector2> Misi2(Vector2 originPoint, int numberOfUnits, List<float> jarakDegLng, List<float> jarakDegLat)
        {
            Vector2 p1 = new();
            Vector2 p2 = new();
            Vector2[] routePoint = new Vector2[numberOfUnits];
            routePoint[0] = originPoint;     

            for(int i = 0; i < routePoint.Length; i++)
            {
                if(i != 0)
                {
                    var j = i - 1;
                    Vector2 newPosition = new()
                    {
                        x = jarakDegLng[j],
                        y = jarakDegLat[j]
                    };
                    Debug.Log(jarakDegLng[j] + ", " + jarakDegLat[j]);

                    switch(i % 2)
                    {
                        case 1:
                            routePoint[i] = (i != 1 ? p1 : originPoint) + newPosition;
                            p1 = routePoint[i];
                            break;
                        case 0:
                            routePoint[i] = (i != 2 ? p2 : originPoint) + newPosition;
                            p2 = routePoint[i];
                            break;
                    }
                }
            }

            return routePoint.ToList();
        }

        ///<summary>
        ///Perhitungan Formasi Sejajar -- Izzan
        ///</summary>
        ///<param name = "originPoint">
        ///Titik Inti Point
        ///</param>
        ///<param name = "numberOfUnits">
        ///Jumlah Unit dalam Formasi
        ///</param>
        ///<param name = "jarakDegLng">
        ///Jarak Degree Yang Sudah Terkonversi ke Latitude
        ///</param>
        public static List<Vector2> Misi3(Vector2 originPoint, int numberOfUnits, List<float> jarakDegLng, List<float> jarakDegLat)
        {
            Vector2 p1 = new();
            Vector2 p2 = new();
            Vector2[] routePoint = new Vector2[numberOfUnits];
            routePoint[0] = originPoint;

            for(int i = 0; i < routePoint.Length; i++)
            {
                if(i != 0)
                {
                    var j = i - 1;
                    Vector2 newPosition = new()
                    {
                        x = jarakDegLng[j],
                        y = jarakDegLat[j]
                    };
                    Debug.Log(jarakDegLng[j] + ", " + jarakDegLat[j]);

                    switch(i % 4)
                    {
                        case 1: case 2:
                            routePoint[i] = (i != 1 ? p1 : originPoint) + newPosition;
                            p1 = routePoint[i];
                            break;
                        case 3: case 0:
                            routePoint[i] = (i != 3 ? p2 : originPoint) + newPosition;
                            p2 = routePoint[i];
                            break;
                    }
                }
            }

            return routePoint.ToList();
        }

        ///<summary>
        ///Perhitungan Formasi Panah -- Izzan
        ///</summary>
        ///<param name = "originPoint">
        ///Titik Inti Point
        ///</param>
        ///<param name = "numberOfUnits">
        ///Jumlah Unit dalam Formasi
        ///</param>
        ///<param name = "jarakDegLng">
        ///Jarak Degree Yang Sudah Terkonversi ke Latitude
        ///</param>
        public static List<Vector2> Misi4(Vector2 originPoint, int numberOfUnits, List<float> jarakDegLng, List<float> jarakDegLat)
        {
            Vector2 p1 = new();
            Vector2 p2 = new();
            Vector2 p3 = new();
            Vector2 p4 = new();
            Vector2[] routePoint = new Vector2[numberOfUnits];
            routePoint[0] = originPoint;

            for (int i = 0; i < routePoint.Length; i++)
            {
                if(i != 0)
                {
                    var j = i - 1;
                    Vector2 newPosition = new()
                    {
                        x = jarakDegLng[j],
                        y = jarakDegLat[j]
                    };

                    switch(i % 5)
                    {
                        case 1:
                            routePoint[i] = (i != 1 ? p1 : originPoint) + newPosition;
                            p1 = routePoint[i];
                            break;
                        case 2:
                            routePoint[i] = (i != 2 ? p2 : originPoint) + newPosition;
                            p2 = routePoint[i];
                            break;
                        case 3: case 0:
                            routePoint[i] = (i != 3 ? p3 : originPoint) + newPosition;
                            p3 = routePoint[i];
                            break;
                        case 4:
                            routePoint[i] = (i != 4 ? p4 : originPoint) + newPosition;
                            p4 = routePoint[i];
                            break;
                    }
                }
            }

            return routePoint.ToList();
        }

        ///<summary>
        ///Perhitungan Formasi Kerucut -- Izzan
        ///</summary>
        ///<param name = "originPoint">
        ///Titik Inti Point
        ///</param>
        ///<param name = "numberOfUnits">
        ///Jumlah Unit dalam Formasi
        ///</param>
        ///<param name = "jarakDegLng">
        ///Jarak Degree Yang Sudah Terkonversi ke Latitude
        ///</param>
        public static List<Vector2> Misi5(Vector2 originPoint, int numberOfUnits, List<float> jarakDegLng, List<float> jarakDegLat)
        {
            var list1 = new List<Vector2>();
            var list2 = new List<Vector2>();
            var listVector1 = new List<Vector2>();
            var listVector2 = new List<Vector2>();
            Vector2 p1 = new();
            Vector2[] routePoint = new Vector2[numberOfUnits];
            routePoint[0] = originPoint;

            for(int i = 0; i < routePoint.Length; i++)
            {
                if(i != 0)
                {
                    var j = i - 1;
                    Vector2 newPosition = new()
                    {
                        x = jarakDegLng[j],
                        y = jarakDegLat[j]
                    };
                    Debug.Log(jarakDegLng[j] + ", " + jarakDegLat[j]);

                    switch(i % 8)
                    {
                        case 1: case 2: case 3: case 4:
                            list1.Add(routePoint[i]);
                            listVector1.Add(newPosition);
                            break;
                        case 5: case 6: case 7: case 0:
                            list2.Add(routePoint[i]);
                            listVector2.Add(newPosition);
                            break;
                    }
                }
            }

            for(int i = 0; i < list1.Count; i++)
            {
                Vector2 newPosition = listVector1[i];
                list1[i] = originPoint + newPosition;
                if(i == 3) p1 = list1[i];
            }
            
            var _list1 = new List<Vector2>(list1);
            for(int i = 0; i < list2.Count; i++)
            {
                var j = i + 1;
                Vector2 newPosition = listVector2[i];
                list2[i] = p1 + newPosition;
                if(j % 4 == 0)
                {
                    if(_list1.Count > 3) _list1.RemoveRange(0, 3);
                    var _p1 = _list1.Count >= 3 ? _list1[3] : new();
                    p1 = _p1 == Vector2.zero ? p1 : _p1;
                }
            }

            for(int i = 0; i < routePoint.Length; i++)
            {
                if(i != 0)
                {
                    switch(i % 8)
                    {
                        case 1: case 2: case 3: case 4:
                            for(int j = 1; j < list1.Count + 1; j++)
                            {
                                var modI = i % 4;
                                var modJ = j % 4;

                                if(modI == modJ)
                                routePoint[i] = list1[j - 1];
                            }
                            break;
                        case 5: case 6: case 7: case 0:
                            for(int j = 1; j < list2.Count + 1; j++)
                            {
                                var modI = i % 4;
                                var modJ = j % 4;

                                if(modI == modJ)
                                routePoint[i] = list2[j - 1];
                            }
                            break;
                    }
                }
            }

            return routePoint.ToList();
        }

        ///<summary>
        ///Perhitungan Formasi Custom -- Izzan
        ///</summary>
        ///<param name = "originPoint">
        ///Titik Inti Point
        ///</param>
        ///<param name = "numberOfUnits">
        ///Jumlah Unit dalam Formasi
        ///</param>
        ///<param name = "jarakDegLng">
        ///Jarak Degree Yang Sudah Terkonversi ke Latitude
        ///</param>
        public static List<Vector2> Misi6(Vector2 originPoint, int numberOfUnits, List<float> jarakDegLng, List<float> jarakDegLat)
        {
            Vector2[] routePoint = new Vector2[numberOfUnits];
            routePoint[0] = originPoint;

            for(int i = 0; i < routePoint.Length; i++)
            {
                if(i != 0)
                {
                    var j = i - 1;
                    Vector2 newPosition = new()
                    {
                        x = jarakDegLng[j],
                        y = jarakDegLat[j]
                    };
                    Debug.Log(jarakDegLng[j] + ", " + jarakDegLat[j]);

                    routePoint[i] = originPoint + newPosition;
                }
            }

            return routePoint.ToList();
        }
    }
}