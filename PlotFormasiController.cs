using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Plot.InputHandler;
using ObjectStat;
using Plot.Pasukan;
using HelperPlotting;
using System.Linq;
using GlobalVariables;

namespace Plot.FormasiPlot
{
    public class PlotFormasiController : MonoBehaviour
    {
        public static PlotFormasiController instance;

        [Header("OnlineMaps")]
        [Space(5)]
        public GameObject map;
        public OnlineMaps onlineMaps;

        [Header("Gambar Formasi")]
        [Space(5)]
        public Sprite belahKetupat;
        public Sprite flankKanan;
        public Sprite flankKiri;
        public Sprite sejajar;
        public Sprite serial;
        public Sprite panah;
        public Sprite kerucut;

        [Header("Formasi Function")]
        [Space(5)]
        public List<float> arrArray;
        List<float> jarak;
        PFormasi.satuanJarak satuanJarak;
        GameObject nowFormasi;

        [Header("Prefab List")]
        [Space(5)]
        public GameObject prefabList;
        public GameObject prefabListTerserah;

        [Header("Canvas")]
        [Space(5)]
        public TMP_InputField namaFormasi;
        public GameObject formasiUI;
        public TMP_Dropdown formasiDropdown;
        public Image formasiImage;
        public TMP_Dropdown jarakDropdown;
        public TMP_InputField arah;
        public ScrollRect scrollView;
        public GameObject content;
        public GameObject contentNormal;
        public GameObject contentTerserah;
        public SidemenuToggler toggle;
        float fBefore;
        float fNow;

        [Header("Edit")]
        [Space(5)]
        public GameObject btnPlot;
        public GameObject btnEdit;
        public GameObject btnMove;
        public GameObject btnDelete;
        public GameObject btnKegiatan;

        [Header ("Kegiatan")]
        [Space (5)]
        List<Vector2> originPoint;
        OnlineMapsDrawingElement poly;
        List<float> jarakKegiatan;

        [Header("MultiSelect")]
        [Space(5)]
        public bool canMultiSelect;
        public List<Transform> selectedUnits = new List<Transform>();
        Vector2 pW;
        Vector2 nW;
        Vector3 mousePos;
        RaycastHit hit;
        Vector2 prevWorldPos;
        Vector2 nowWorldPos;
        bool isDragging;
        bool isDragMove;
        bool isMoving = false;
        int u = 0;
        [HideInInspector] public int indexFormasi;
        bool kegiatanOn;

        void Awake()
        {
            if (instance == null)
                instance = this;
            else
                Destroy(gameObject);
        }

        // Start is called before the first frame update
        void Start()
        {
            fBefore = formasiDropdown.value;
            fNow = formasiDropdown.value;
        }

        // Update is called once per frame
        void Update()
        {
            if(formasiDropdown.value != 8)
            {   
                contentNormal.SetActive(true);
                contentTerserah.SetActive(false);
                content = contentNormal;
                scrollView.content = contentNormal.GetComponent<RectTransform>();
            }
            else
            {
                contentTerserah.SetActive(true);
                contentNormal.SetActive(false);
                content = contentTerserah;
                scrollView.content = contentTerserah.GetComponent<RectTransform>();
            }

            if (formasiDropdown.value == 1)
                formasiImage.sprite = belahKetupat;
            else if (formasiDropdown.value == 2)
                formasiImage.sprite = flankKanan;
            else if (formasiDropdown.value == 3)
                formasiImage.sprite = flankKiri;
            else if (formasiDropdown.value == 4)
                formasiImage.sprite = sejajar;
            else if (formasiDropdown.value == 5)
                formasiImage.sprite = serial;
            else if (formasiDropdown.value == 6)
                formasiImage.sprite = panah;
            else if (formasiDropdown.value == 7)
                formasiImage.sprite = kerucut;

            if (jarakDropdown.value == 0)
                satuanJarak = PFormasi.satuanJarak.Nautical_Miles;
            else if (jarakDropdown.value == 1)
                satuanJarak = PFormasi.satuanJarak.Kilometer;
            else if (jarakDropdown.value == 2)
                satuanJarak = PFormasi.satuanJarak.Feet;
            else if (jarakDropdown.value == 3)
                satuanJarak = PFormasi.satuanJarak.Yard;
            else if (jarakDropdown.value == 4)
                satuanJarak = PFormasi.satuanJarak.Meter;

            if (formasiDropdown.value != 0)
            {
                formasiUI.GetComponent<RectTransform>().sizeDelta = new Vector2(320f, 55f);
                formasiImage.gameObject.SetActive(true);
            }
            else
            {
                formasiUI.GetComponent<RectTransform>().sizeDelta = new Vector2(320f, 25f);
                formasiImage.gameObject.SetActive(false);
            }

            MultiSelect();
            // EditMenu();
            Move();

            if(kegiatanOn)
            DrawRouteKegiatan();
        }

        private void OnGUI()
        {
            if (isDragging)
            {
                Rect rect = DragGUI.GetScreenRect(mousePos, Input.mousePosition);
                DragGUI.DrawScreenRect(rect, new Color(0f, 0f, 0f, 0.25f));
                DragGUI.DrawScreenRectBorder(rect, 3, Color.blue);
            }
        }

        public void MultiSelect()
        {
            fNow = formasiDropdown.value;

            if (canMultiSelect)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    mousePos = Input.mousePosition;
                    double lng, lat;
                    if(OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
                    pW = new Vector2((float)lng, (float)lat);

                    Ray ray = Camera.main.ScreenPointToRay(mousePos);

                    if (Physics.Raycast(ray, out hit))
                    {
                        LayerMask layerMask = hit.transform.gameObject.layer;
                        Transform unitHit = hit.transform;
                        PFormasi fS = null;

                        if (hit.transform.GetComponent<PFormasi>() != null)
                            fS = hit.transform.GetComponent<PFormasi>();

                        switch (layerMask.value)
                        {
                            case 7:
                                if (fS.formasi == PFormasi.formasiType.None)
                                {
                                    if (!selectedUnits.Contains(fS.marker3D.instance.transform))
                                        SelectUnit(fS.marker3D.instance.transform);
                                }
                                else
                                    Debug.Log("Unit Ini Sudah Berada Dalam Formasi!!!");
                                break;
                            default:
                                isDragging = true;
                                break;
                        }
                    }
                }

                if (Input.GetMouseButtonUp(0))
                {
                    foreach (OnlineMapsMarker3D child in OnlineMapsMarker3DManager.instance)
                    {
                        var units = child.instance.transform;
                        if (units.gameObject.GetComponent<PFormasi>() != null)
                        {
                            var pF = units.gameObject.GetComponent<PFormasi>();

                            if (pF.formasi == PFormasi.formasiType.None)
                            if (IsWithinSelectionBounds(units))
                            {
                                if(units.gameObject.activeSelf == true)
                                {
                                    var pFF = units.GetComponent<PFormasi>();

                                    if (!selectedUnits.Contains(pFF.marker3D.instance.transform))
                                    SelectUnit(pFF.marker3D.instance.transform);
                                }
                            }
                        }
                    }

                    isDragging = false;
                }
            }

            if (u < selectedUnits.Count)
            {
                foreach (Transform child in content.transform)
                    GameObject.Destroy(child.gameObject);

                for (int i = 0; i < selectedUnits.Count; i++)
                {
                    Transform editUnit = selectedUnits[i];
                    PFormasi unitStats = selectedUnits[i].GetComponent<PFormasi>();

                    string font = "";
                    string fontType = "TandaTanda";
                    string nama = "";

                    if (unitStats.stat != null)
                    {
                        font = unitStats.stat.font;
                        fontType = unitStats.stat.fontType;
                        nama = unitStats.stat.nama;
                    }
                    else
                    {
                        int indexSym = int.Parse(unitStats.jStatStyle["index"].ToString());
                        font = ((char)indexSym).ToString();
                        fontType = unitStats.jStatStyle["nama"].ToString();
                        nama = unitStats.jStat.nama;
                    }

                    GameObject copy = null;
                    if(formasiDropdown.value != 8)
                    copy = Instantiate(prefabList, content.transform);
                    else
                    copy = Instantiate(prefabListTerserah, content.transform);

                    Metadata meta = copy.GetComponent<Metadata>();
                    meta.FindParameter("No.").parameter.GetComponent<TMP_Text>().text = (i + 1).ToString();
                    meta.FindParameter("Symbol").parameter.GetComponent<Text>().text = font;
                    meta.FindParameter("Symbol").parameter.GetComponent<Text>().font = Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + fontType);
                    meta.FindParameter("Nama").parameter.GetComponent<TMP_Text>().text = nama;

                    if (unitStats.formasi != PFormasi.formasiType.None)
                    {
                        float jarakNow = unitStats.jarak;
                        float degNow = unitStats.sudut;
                        var sJ = unitStats.satuan;

                        if (sJ == PFormasi.satuanJarak.Nautical_Miles)
                            jarakDropdown.value = 0;
                        else if (sJ == PFormasi.satuanJarak.Kilometer)
                            jarakDropdown.value = 1;
                        else if (sJ == PFormasi.satuanJarak.Feet)
                            jarakDropdown.value = 2;
                        else if (sJ == PFormasi.satuanJarak.Yard)
                            jarakDropdown.value = 3;
                        else if (sJ == PFormasi.satuanJarak.Meter)
                            jarakDropdown.value = 4;

                        meta.FindParameter("Jarak Input").parameter.GetComponent<TMP_InputField>().text = Mathf.Round(jarakNow).ToString();

                        if(formasiDropdown.value == 8)
                        meta.FindParameter("Arah Input").parameter.GetComponent<TMP_InputField>().text = Mathf.Round(degNow).ToString();
                    }

                    if (i == 0)
                    {
                        meta.FindParameter("Jarak Input").parameter.SetActive(false);

                        if(formasiDropdown.value == 8)
                        meta.FindParameter("Arah Input").parameter.SetActive(false);
                    }

                    Button delSetting = meta.FindParameter("Delete").parameter.GetComponent<Button>();
                    delSetting.onClick.AddListener(delegate { Remove(editUnit, copy); });

                    copy.transform.localScale = prefabList.transform.localScale;
                    copy.transform.localPosition = Vector3.zero;
                    u++;
                }
            }
            else if (u > selectedUnits.Count)
            {
                u = 0;
            }

            if(fBefore != fNow)
            {
                u = 0;
                fBefore = fNow;
            }

            if (selectedUnits.Count == 0)
            {
                foreach (Transform child in content.transform)
                Destroy(child.gameObject);
            }
        }

        public void SelectUnit(Transform unit)
        {
            if (!canMultiSelect)
                DeselectUnit();

            selectedUnits.Add(unit);
        }

        public void DeselectUnit()
        {
            selectedUnits.Clear();
        }

        private bool HaveSelectedUnit()
        {
            if (selectedUnits.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsWithinSelectionBoundsWorldView(OnlineMapsMarker3D marker)
        {
            if(!isDragging)
            return false;

            double lng, lat;
            if(OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
            nW = new Vector2((float)lng, (float)lat);

            Bounds worldViewBounds = DragGUI.GetWorldViewBounds(pW, nW);
            return worldViewBounds.Contains(marker.position);
        }

        private bool IsWithinSelectionBounds(Transform tf)
        {
            if (!isDragging)
            {
                return false;
            }

            Camera cam = Camera.main;
            Bounds vpBounds = DragGUI.GetVPBounds(cam, mousePos, Input.mousePosition);
            return vpBounds.Contains(cam.WorldToViewportPoint(tf.position));
        }

        public void ResetForm()
        {
            namaFormasi.text = "";
            formasiDropdown.value = 0;
            jarakDropdown.value = 0;
            arah.text = "";
            btnPlot.SetActive(true);
            btnEdit.SetActive(false);
            btnMove.SetActive(false);
            btnDelete.SetActive(false);

            if (isMoving)
                for (int i = 0; i < selectedUnits.Count; i++)
                {
                    OnlineMapsMarker3D marker3D, marker2D;
                    if (selectedUnits[i].GetComponent<Stat>() != null)
                    {
                        Stat oS = selectedUnits[i].GetComponent<Stat>();

                        marker2D = oS.marker2D;
                        marker3D = oS.marker2D;

                        marker2D.OnPress -= OnMarkerPress;
                        marker2D.OnRelease -= OnMarkerRelease;

                        marker3D.OnPress -= OnMarkerPress;
                        marker3D.OnRelease -= OnMarkerRelease;
                    }
                }

            canMultiSelect = false;
            onlineMaps.blockAllInteractions = false;
            isDragging = false;
            isDragMove = false;
            isMoving = false;

            // toggle.toggleOff();
            DeselectUnit();
        }

        public void MultiSelectOn()
        {
            canMultiSelect = !canMultiSelect;
            onlineMaps.blockAllInteractions = !onlineMaps.blockAllInteractions;

            isDragging = false;
        }

        public void Remove(Transform unit, GameObject unitInList)
        {
            selectedUnits.Remove(unit);
            Destroy(unitInList);
        }

        public void Plot()
        {
            jarak = new List<float>();
            List<float> newJarakLng;
            List<float> newJarakLat;

            bool negative = false;
            bool zero = false;
            bool empty = false;
            var arEmpty = false;

            var arahArr = new List<float>();

            if (selectedUnits.Count == 0)
                Debug.Log("Anda Belum Memilih Unit!!");
            else if (selectedUnits.Count == 1)
                Debug.Log("Unit Yang Dipilih Harus Lebih Dari 1!!!");
            else
            {
                foreach (Transform child in content.transform)
                {
                    TMP_InputField inputField = null;

                    if (child != content.transform.GetChild(0))
                        if (child.Find("Jarak Input").gameObject.activeSelf)
                            inputField = child.Find("Jarak Input").GetComponent<TMP_InputField>();

                    if (inputField != null)
                        if (inputField.text != "" && inputField.text != "-")
                        {
                            jarak.Add(int.Parse(inputField.text));
                        }
                        else
                            empty = true;
                }

                if (empty)
                    Debug.Log("Jarak Tidak Boleh Kosong!!!");
                else
                {
                    if (arah.text == "")
                        arah.text = "0";

                    foreach (float _jarak in jarak)
                        Debug.Log("sebelum " + _jarak);

                    arrArray = new List<float>();
                    var jarakList = new List<float>();
                    foreach (float _jarak in jarak)
                        jarakList.Add(_jarak);

                    var newJarak = new PerhitunganFormasi().KonversiJarak(jarakList, jarakDropdown.value);
                    
                    if(formasiDropdown.value != 8)
                    {
                        newJarakLng = new PerhitunganFormasi().Jarak(newJarak, float.Parse(arah.text), formasiDropdown.value, "Longitude");
                        newJarakLat = new PerhitunganFormasi().Jarak(newJarak, float.Parse(arah.text), formasiDropdown.value, "Latitude");
                    }
                    else
                    {
                        foreach(Transform child in content.transform)
                        {
                            TMP_InputField inputField = null;
                            
                            if (child != content.transform.GetChild(0))
                            if (child.Find("Arah Input").gameObject.activeSelf)
                            inputField = child.Find("Arah Input").GetComponent<TMP_InputField>();
                            
                            if (inputField != null)
                            if (inputField.text != "" && inputField.text != "-")
                            {
                                arahArr.Add(int.Parse(inputField.text));
                            }
                            else
                                arEmpty = true;
                        }

                        if(arEmpty)
                        {
                            Debug.LogWarning("Input Arah Tiap Unit Tidak Boleh Kosong!!");
                            return;
                        }

                        newJarakLng = new PerhitunganFormasi().Jarak(newJarak, arahArr, float.Parse(arah.text), "Longitude");
                        newJarakLat = new PerhitunganFormasi().Jarak(newJarak, arahArr, float.Parse(arah.text), "Latitude");
                    }

                    foreach (float m_jarak in jarak)
                    {
                        if (m_jarak < 0)
                            negative = true;

                        if (m_jarak == 0)
                            zero = true;
                    }

                    if (negative)
                        Debug.Log("Jarak Tidak Boleh Ada Yang Negatif!!!");
                    else if (zero)
                        Debug.Log("Jarak Tidak Boleh 0!!!");
                    else if (formasiDropdown.value == 0)
                        Debug.Log("Anda Belum Memilih Formasi!!!");
                    else if (namaFormasi.text == "")
                        Debug.Log("Anda Belum Input Nama Formasi!!!");
                    else
                    {
                        GetFormasi(newJarakLng, newJarakLat);
                        MakeKegiatanForLeader();
                        toggle.toggleOff();

                        foreach (float _jarak in jarak)
                            Debug.Log("sesudah " + _jarak);
                    }
                }
            }
        }

        public void GetFormasi(List<float> jarakSatuanLng, List<float> jarakSatuanLat)
        {
            if (formasiDropdown.value == 1)
                PerhitunganFormasi.Formasi1(selectedUnits, namaFormasi.text, float.Parse(arah.text), PFormasi.formasiType.Belah_Ketupat, satuanJarak, jarak, jarakSatuanLng, jarakSatuanLat);
            else if (formasiDropdown.value == 2)
                PerhitunganFormasi.Formasi2(selectedUnits, namaFormasi.text, float.Parse(arah.text), PFormasi.formasiType.Flank_Kanan, satuanJarak, jarak, jarakSatuanLng, jarakSatuanLat);
            else if (formasiDropdown.value == 3)
                PerhitunganFormasi.Formasi2(selectedUnits, namaFormasi.text, float.Parse(arah.text), PFormasi.formasiType.Flank_Kiri, satuanJarak, jarak, jarakSatuanLng, jarakSatuanLat);
            else if (formasiDropdown.value == 4)
                PerhitunganFormasi.Formasi3(selectedUnits, namaFormasi.text, float.Parse(arah.text), PFormasi.formasiType.Sejajar, satuanJarak, jarak, jarakSatuanLng, jarakSatuanLat);
            else if (formasiDropdown.value == 5)
                PerhitunganFormasi.Formasi4(selectedUnits, namaFormasi.text, float.Parse(arah.text), PFormasi.formasiType.Serial, satuanJarak, jarak, jarakSatuanLng, jarakSatuanLat);
            else if (formasiDropdown.value == 6)
                PerhitunganFormasi.Formasi5(selectedUnits, namaFormasi.text, float.Parse(arah.text), PFormasi.formasiType.Panah, satuanJarak, jarak, jarakSatuanLng, jarakSatuanLat);
            else if (formasiDropdown.value == 7)
                PerhitunganFormasi.Formasi6(selectedUnits, namaFormasi.text, float.Parse(arah.text), PFormasi.formasiType.Kerucut, satuanJarak, jarak, jarakSatuanLng, jarakSatuanLat);
            else if (formasiDropdown.value == 8)
                PerhitunganFormasi.Formasi7(selectedUnits, namaFormasi.text, float.Parse(arah.text), PFormasi.formasiType.Custom, satuanJarak, jarak, jarakSatuanLng, jarakSatuanLat);

            if(GameObject.Find("FormasiObj") == null)
            new GameObject("FormasiObj");
            
            var objFormasi = GameObject.Find("FormasiObj").transform;
            var pF = selectedUnits[0].GetComponent<PFormasi>();
            var infoFor = Info_Formasi.FromJson(pF.info);
            var nF = pF.nama;
            
            if(GameObject.Find(nF) == null)
            nowFormasi = new GameObject(nF);
            else
            nowFormasi = GameObject.Find(nF);

            nowFormasi.tag = "entity-formasi";
            nowFormasi.transform.parent = objFormasi;

            var entity = new EntityFormasi();
            entity.nama = pF.nama;
            entity.id_user = SessionUser.id.ToString();
            entity.info_formasi = pF.info;
            entity.size = 20;
            entity.warna = infoFor.warna;
            
            if(pF.formasi == PFormasi.formasiType.Belah_Ketupat)
            entity.jenis_formasi = "1";
            else if(pF.formasi == PFormasi.formasiType.Flank_Kanan)
            entity.jenis_formasi = "2";
            else if(pF.formasi == PFormasi.formasiType.Flank_Kiri)
            entity.jenis_formasi = "3";
            else if(pF.formasi == PFormasi.formasiType.Sejajar)
            entity.jenis_formasi = "4";
            else if(pF.formasi == PFormasi.formasiType.Serial)
            entity.jenis_formasi = "5";
            else if(pF.formasi == PFormasi.formasiType.Panah)
            entity.jenis_formasi = "6";
            else if(pF.formasi == PFormasi.formasiType.Kerucut)
            entity.jenis_formasi = "7";
            else if(pF.formasi == PFormasi.formasiType.Custom)
            entity.jenis_formasi = "8";

            EntityFormasiObj eF = null;
            if(nowFormasi.GetComponent<EntityFormasiObj>() == null)
            eF = nowFormasi.AddComponent<EntityFormasiObj>();
            else
            eF = nowFormasi.GetComponent<EntityFormasiObj>();

            eF.json = EntityFormasi.ToString(entity);

            indexFormasi++;
        }

        public void MakeKegiatanForLeader()
        {
            var pF = selectedUnits[0].GetComponent<PFormasi>();
            var jM = selectedUnits[0].GetComponent<JSONDataMisiSatuan>();
            
            for(int i = 0; i < jM.listRoute.Count; i++)
            {
                var data = jM.listRoute[i].data;
                for(int j = 0; j < pF.listObjekFormasi.Count; j++)
                {
                    var pFF = pF.listObjekFormasi[j].GetComponent<PFormasi>();
                    pFF.routePoints = new List<Vector2>();
                }

                for(int j = 0; j < data.Count; j++)
                {
                    var satuan = pF.satuan;
                    var formasi = pF.formasi;
                    var d_satuan = 0;
                    var d_formasi = 0;

                    if(satuan == PFormasi.satuanJarak.Nautical_Miles)
                    d_satuan = 0;
                    else if(satuan == PFormasi.satuanJarak.Kilometer)
                    d_satuan = 1;
                    else if(satuan == PFormasi.satuanJarak.Feet)
                    d_satuan = 2;
                    else if(satuan == PFormasi.satuanJarak.Yard)
                    d_satuan = 3;
                    else if(satuan == PFormasi.satuanJarak.Meter)
                    d_satuan = 4;

                    if(formasi == PFormasi.formasiType.Belah_Ketupat)
                    d_formasi = 1;
                    else if(formasi == PFormasi.formasiType.Flank_Kiri)
                    d_formasi = 2;
                    else if(formasi == PFormasi.formasiType.Flank_Kanan)
                    d_formasi = 3;
                    else if(formasi == PFormasi.formasiType.Sejajar)
                    d_formasi = 4;
                    else if(formasi == PFormasi.formasiType.Serial)
                    d_formasi = 5;
                    else if(formasi == PFormasi.formasiType.Panah)
                    d_formasi = 6;
                    else if(formasi == PFormasi.formasiType.Kerucut)
                    d_formasi = 7;
                    else if(formasi == PFormasi.formasiType.Custom)
                    d_formasi = 8;

                    var n1 = new Vector2();
                    var n2 = new Vector2();
                    var routePoints = new List<Vector2>();

                    var newOrigin = new Vector2(data[j].coordinate.x, data[j].coordinate.y);
                    routePoints.Add(newOrigin);

                    if(data.Count == 1)
                    n1 = (Vector2)data[data.Count - 1].coordinate;
                    else
                    n1 = (Vector2)data[data.Count - 2].coordinate;
                    
                    n2 = (Vector2)data[data.Count - 1].coordinate;

                    var pointsVec = new Vector2[pF.listObjekFormasi.Count - 1].ToList();

                    var arahV = pF.listObjekFormasi[0].GetComponent<PFormasi>().sudut;
                    var angle = new DrawRoute().GetAngleOfLineBetweenTwoPoints(n1, n2) - 90;

                    var jarakKegiatan = new List<float>();
                
                    foreach(Transform child in pF.listObjekFormasi)
                    if(child != pF.listObjekFormasi[0])
                    jarakKegiatan.Add(child.GetComponent<PFormasi>().jarak);

                    var newJarak = new PerhitunganFormasi().KonversiJarak(jarakKegiatan, d_satuan);
                    var jarakLng = new List<float>();
                    var jarakLat = new List<float>();

                    if(d_formasi != 8)
                    {
                        jarakLng = new PerhitunganFormasi().Jarak(newJarak, arahV + angle, d_formasi, "Longitude");
                        jarakLat = new PerhitunganFormasi().Jarak(newJarak, arahV + angle, d_formasi, "Latitude");
                    }
                    else
                    {
                        var arahArr = new List<float>();
                        foreach(var item in pF.listObjekFormasi)
                        {
                            var pFF = item.GetComponent<PFormasi>();
                            arahArr.Add(pFF.sudut);
                        }

                        jarakLng = new PerhitunganFormasi().Jarak(newJarak, arahArr, arahV + angle, "Longitude");
                        jarakLat = new PerhitunganFormasi().Jarak(newJarak, arahArr, arahV + angle, "Latitude");
                    }

                    var formasiPoints = new List<Vector2>();
                    if(d_formasi == 1)
                    formasiPoints = new DrawRoute().Formasi1(jarakLng, jarakLat, newOrigin, pointsVec);
                    else if(d_formasi == 2)
                    formasiPoints = new DrawRoute().Formasi2(jarakLng, jarakLat, newOrigin, pointsVec);
                    else if(d_formasi == 3)
                    formasiPoints = new DrawRoute().Formasi2(jarakLng, jarakLat, newOrigin, pointsVec);
                    else if(d_formasi == 4)
                    formasiPoints = new DrawRoute().Formasi3(jarakLng, jarakLat, newOrigin, pointsVec);
                    else if(d_formasi == 5)
                    formasiPoints = new DrawRoute().Formasi4(jarakLng, jarakLat, newOrigin, pointsVec);
                    else if(d_formasi == 6)
                    formasiPoints = new DrawRoute().Formasi5(jarakLng, jarakLat, newOrigin, pointsVec);
                    else if(d_formasi == 7)
                    formasiPoints = new DrawRoute().Formasi6(jarakLng, jarakLat, newOrigin, pointsVec);
                    else if(d_formasi == 8)
                    formasiPoints = new DrawRoute().Formasi7(jarakLng, jarakLat, newOrigin, pointsVec);

                    routePoints.AddRange(formasiPoints);

                    for(int u = 0; u < pF.listObjekFormasi.Count; u++)
                    {
                        var pFF = pF.listObjekFormasi[u].GetComponent<PFormasi>();

                        if(pFF.stat != null)
                        {
                            var pF2D = pFF.marker2D.instance.GetComponent<PFormasi>();
                            for(int o = 0; o < routePoints.Count; o++)
                            {
                                if(o == pFF.urutanFormasi)
                                pF2D.routePoints.Add(routePoints[o]);
                            }
                        }

                        var pF3D = pFF.marker3D.instance.GetComponent<PFormasi>();
                        for(int o = 0; o < routePoints.Count; o++)
                        {
                            if(o == pFF.urutanFormasi)
                            {
                                if(pF3D.routePoints == null)
                                pF3D.routePoints = new List<Vector2>();
                                
                                pF3D.routePoints.Add(routePoints[o]);
                            }
                        }
                    }
                }

                for (int j = 0; j < pF.listObjekFormasi.Count; j++)
                {
                    var pFF = pF.listObjekFormasi[j].GetComponent<PFormasi>();
                    var jmisi = pF.listObjekFormasi[j].GetComponent<JSONDataMisiSatuan>();

                    // BROOW  
                    
                }
            }

            for(int i = selectedUnits.Count - 1; i > -1; i--)
            {
                if(i != 0)
                {
                    var jMM = selectedUnits[i].GetComponent<JSONDataMisiSatuan>();
                    for(int j = 0; j < jMM.listRoute.Count; j++)
                    {
                        var trash = GameObject.Find("Trash");
                        if(trash == null)
                        {
                            trash = new GameObject("Trash");
                            trash.AddComponent<TrashObj>();
                        }

                        var jMObj = jMM.listRoute[j];
                        var tObj = new Trash();
                        tObj.id = jMObj.name;
                        tObj.type = "misi";

                        var tScript = trash.GetComponent<TrashObj>();
                        tScript.trash.Add(tObj);
                        
                        Destroy(jMObj);
                        jMM.listRoute.RemoveAt(j);
                    }
                }
            }
        }

        public void EditMenu(OnlineMapsMarker3D marker, SidemenuToggler toggler)
        {
            // if (Input.GetMouseButtonDown(1))
            // {
            //     mousePos = Input.mousePosition;

            //     Ray ray = Camera.main.ScreenPointToRay(mousePos);

            //     if (Physics.Raycast(ray, out hit))
            //     {
            //         LayerMask layerMask = hit.transform.gameObject.layer;
            //         PFormasi fS = null;

            //         if (hit.transform.GetComponent<PFormasi>() != null)
            //             fS = hit.transform.GetComponent<PFormasi>();

            //         switch (layerMask.value)
            //         {
            //             case 7:
            //                 if (fS != null)
            //                     if (fS.formasi != PFormasi.formasiType.None)
            //                     {
                                    
            //                     }
            //                 break;
            //             default:
            //                 break;
            //         }
            //     }
            // }
            PlotPasukanController.instance.resetForm();
            toggler.toggleOff();
            toggle.toggleOn();
            btnPlot.SetActive(false);
            btnEdit.SetActive(true);
            btnMove.SetActive(true);
            btnDelete.SetActive(true);
            btnKegiatan.SetActive(true);
            
            var fS = marker.instance.GetComponent<PFormasi>();
            var infoFor = Info_Formasi.FromJson(fS.info);
            namaFormasi.text = infoFor.nama_formasi;
            
            if(fS.stat != null)
            arah.text = fS.stat.heading;

            var sF = fS.formasi;
            if(sF == PFormasi.formasiType.Belah_Ketupat)
            formasiDropdown.value = 1;
            else if(sF == PFormasi.formasiType.Flank_Kanan)
            formasiDropdown.value = 2;
            else if(sF == PFormasi.formasiType.Flank_Kiri)
            formasiDropdown.value = 3;
            else if(sF == PFormasi.formasiType.Sejajar)
            formasiDropdown.value = 4;
            else if(sF == PFormasi.formasiType.Serial)
            formasiDropdown.value = 5;
            else if(sF == PFormasi.formasiType.Panah)
            formasiDropdown.value = 6;
            else if(sF == PFormasi.formasiType.Kerucut)
            formasiDropdown.value = 7;
            else if(sF == PFormasi.formasiType.Custom)
            formasiDropdown.value = 8;

            foreach(var _obj in fS.listObjekFormasi)
            {
                if(!selectedUnits.Contains(_obj))
                selectedUnits.Add(_obj);
            }
        }

        public void Kegiatan(bool turnOn)
        {
            // // var jrk = "jarak: ";

            // var origin = new Vector2();
            // origin.x = selectedUnits[0].GetComponent<PFormasi>().marker3D.position.x;
            // origin.y = selectedUnits[0].GetComponent<PFormasi>().marker3D.position.y;

            // originPoint = new List<Vector2>();
            // if(originPoint.Count == 0)
            // originPoint.Add(origin);

            // if(turnOn == true)
            // kegiatanOn = true;
            // else
            // kegiatanOn = false;
        }

        public void DrawRouteKegiatan()
        {
            // var indexDropdown = formasiDropdown.value;
            // var jrkDrop = jarakDropdown.value;

            // if(Input.GetMouseButtonDown(0))
            // {
            //     var n1 = new Vector2();
            //     var n2 = new Vector2();
            //     var routePoints = new List<Vector2>();

            //     var newOrigin = new Vector2();
            //     var target = Resources.Load<Texture2D>("Images/Icons/Unsorted/icon-filled-target");

            //     if(poly != null)
            //     poly.Dispose();

            //     double lng, lat;
            //     if(OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
            //     { newOrigin.x = (float)lng; newOrigin.y = (float)lat; }

            //     var m = OnlineMapsMarkerManager.instance.Create(newOrigin, target);
            //     m.scale = .25f;

            //     originPoint.Add(newOrigin);
            //     routePoints.Add(newOrigin);
                
            //     if(originPoint.Count == 1)
            //     n1 = originPoint[originPoint.Count - 1];
            //     else
            //     n1 = originPoint[originPoint.Count - 2];
                
            //     n2 = originPoint[originPoint.Count - 1];

            //     var line = new OnlineMapsDrawingLine(originPoint, Color.white, .5f);
            //     poly = OnlineMapsDrawingElementManager.AddItem(line);
                
            //     var pointsVec = new Vector2[selectedUnits.Count - 1].ToList();

            //     var arahV = selectedUnits[0].GetComponent<PFormasi>().sudut;
            //     var angle = new DrawRoute().GetAngleOfLineBetweenTwoPoints(n1, n2) + 90;
                
            //     jarakKegiatan = new List<float>();
                
            //     foreach(Transform child in selectedUnits)
            //     if(child != selectedUnits[0])
            //     jarakKegiatan.Add(child.GetComponent<PFormasi>().jarak);
                
            //     var newJarak = new PerhitunganFormasi().KonversiJarak(jarakKegiatan, jrkDrop);
            //     var jarakLng = new PerhitunganFormasi().Jarak(newJarak, arahV + angle, indexDropdown, "Longitude");
            //     var jarakLat = new PerhitunganFormasi().Jarak(newJarak, arahV + angle, indexDropdown, "Latitude");

            //     var formasiPoints = new List<Vector2>();

            //     if(indexDropdown == 1)
            //     formasiPoints = new DrawRoute().Formasi1(jarakLng, jarakLat, newOrigin, pointsVec);
            //     else if(indexDropdown == 2)
            //     formasiPoints = new DrawRoute().Formasi2(jarakLng, jarakLat, newOrigin, pointsVec);
            //     else if(indexDropdown == 3)
            //     formasiPoints = new DrawRoute().Formasi2(jarakLng, jarakLat, newOrigin, pointsVec);
            //     else if(indexDropdown == 4)
            //     formasiPoints = new DrawRoute().Formasi3(jarakLng, jarakLat, newOrigin, pointsVec);
            //     else if(indexDropdown == 5)
            //     formasiPoints = new DrawRoute().Formasi4(jarakLng, jarakLat, newOrigin, pointsVec);
            //     else if(indexDropdown == 6)
            //     formasiPoints = new DrawRoute().Formasi5(jarakLng, jarakLat, newOrigin, pointsVec);

            //     routePoints.AddRange(formasiPoints);

            //     for(int i = 0; i < selectedUnits.Count; i++)
            //     {
            //         var pF = selectedUnits[i].GetComponent<PFormasi>();

            //         if(pF.stat != null)
            //         {
            //             var pF2D = pF.marker2D.instance.GetComponent<PFormasi>();
            //             for(int j = 0; j < routePoints.Count; j++)
            //             {
            //                 if(j == pF.urutanFormasi)
            //                 pF2D.routePoints.Add(routePoints[j]);
            //             }
            //         }

            //         var pF3D = pF.marker3D.instance.GetComponent<PFormasi>();
            //         for(int j = 0; j < routePoints.Count; j++)
            //         {
            //             if(j == pF.urutanFormasi)
            //             pF3D.routePoints.Add(routePoints[j]);
            //         }
            //     }

            //     foreach(Vector2 _point in formasiPoints)
            //     {
            //         var _m = OnlineMapsMarkerManager.instance.Create(_point, target);
            //         _m.scale = .25f;
            //     }
            // }
        }

        public void MoveOn()
        {
            isMoving = !isMoving;

            if (isMoving)
                Debug.Log("Sedang Memindahkan Formasi...");
            else
                Debug.Log("Pemindahan Selesai");

            for (int i = 0; i < selectedUnits.Count; i++)
            {
                OnlineMapsMarker3D marker3D, marker2D;
                PFormasi oS = selectedUnits[i].GetComponent<PFormasi>();

                marker2D = oS.marker2D;
                marker3D = oS.marker3D;

                if (isMoving)
                {
                    if (oS.stat != null)
                    {
                        marker2D.OnPress += OnMarkerPress;
                        marker2D.OnRelease += OnMarkerRelease;
                    }

                    marker3D.OnPress += OnMarkerPress;
                    marker3D.OnRelease += OnMarkerRelease;
                    // Debug.Log(marker3D.label);
                }
                else
                {
                    if (oS.stat != null)
                    {
                        marker2D.OnPress -= OnMarkerPress;
                        marker2D.OnRelease -= OnMarkerRelease;
                    }

                    marker3D.OnPress -= OnMarkerPress;
                    marker3D.OnRelease -= OnMarkerRelease;
                }
            }
        }

        public void Move()
        {
            if (isDragMove && isMoving)
            {
                double lng, lat;
                if (OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
                    nowWorldPos = new Vector2((float)lng, (float)lat);

                Vector2 diffWorldPos = nowWorldPos - prevWorldPos;

                if (nowWorldPos != prevWorldPos)
                    for (int i = 0; i < selectedUnits.Count; i++)
                    {
                        OnlineMapsMarker3D marker3D, marker2D;
                        if (selectedUnits[i].GetComponent<PFormasi>() != null)
                        {
                            PFormasi oS = selectedUnits[i].GetComponent<PFormasi>();

                            marker2D = oS.marker2D;
                            marker3D = oS.marker3D;

                            if (oS.stat != null)
                                marker2D.position += diffWorldPos;

                            marker3D.position += diffWorldPos;
                        }
                    }

                prevWorldPos = nowWorldPos;
            }
        }

        public void OnMarkerPress(OnlineMapsMarkerBase markerBase)
        {
            if (isMoving)
            {
                OnlineMapsMarker3D marker3D = markerBase as OnlineMapsMarker3D;

                double lng, lat;
                if (OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
                    prevWorldPos = new Vector2((float)lng, (float)lat);

                isDragMove = true;
                OnlineMaps.instance.GetComponent<OnlineMapsTileSetControl>().allowUserControl = false;
            }
        }

        public void OnMarkerRelease(OnlineMapsMarkerBase markerBase)
        {
            if (isMoving)
            {
                OnlineMapsMarker3D marker3D = markerBase as OnlineMapsMarker3D;

                isDragMove = false;
                OnlineMaps.instance.GetComponent<OnlineMapsTileSetControl>().allowUserControl = true;
            }
        }

        public void Edit()
        {
            Plot();
        }

        public void Delete()
        {
            var fSS = selectedUnits[0].GetComponent<PFormasi>();

            var trash = GameObject.Find("Trash");
            if(trash == null)
            {
                trash = new GameObject("Trash");
                trash.AddComponent<TrashObj>();
            }

            var tObj = new Trash();
            tObj.id = fSS.nama;
            tObj.type = "formasi";

            var tScript = trash.GetComponent<TrashObj>();
            tScript.trash.Add(tObj);

            EntityEditorController.Instance.plotTrash.Add(
                new JObject()
                {
                    { "id", fSS.nama },
                    { "type", "polygon" }
                }
            );

            for (int i = 0; i < selectedUnits.Count; i++)
            {
                PFormasi fS = selectedUnits[i].GetComponent<PFormasi>();
                var f2D = fS.marker2D.instance.GetComponent<PFormasi>();
                var f3D = fS.marker3D.instance.GetComponent<PFormasi>();
                
                var jM = selectedUnits[i].GetComponent<JSONDataMisiSatuan>();
                for(int j = 0; j < jM.listRoute.Count; j++)
                {
                    var jMObj = jM.listRoute[j].gameObject;
                    var ttObj = new Trash();
                    ttObj.id = jMObj.name;
                    ttObj.type = "misi";
                    tScript.trash.Add(ttObj);
                    
                    Destroy(jMObj);
                    jM.listRoute = new List<PlotDataRoute>();
                }

                if(fS.stat != null)
                {
                    f2D.nama = "";
                    f2D.namaFormasi = "";
                    f2D.jarak = 0;
                    f2D.urutanFormasi = 0;
                    f2D.sudut = 0;
                    f2D.satuan = PFormasi.satuanJarak.Nautical_Miles;
                    f2D.formasi = PFormasi.formasiType.None;
                    f2D.listObjekFormasi.Clear();
                    f2D.routePoints = new List<Vector2>();
                    f2D.info = "";
                    f2D.listObjekFormasi = new List<Transform>();

                    f2D.gameObject.GetComponent<JSONDataMisiSatuan>().listRoute = new List<PlotDataRoute>();
                }

                var forObj = GameObject.Find(fS.nama);
                if(forObj != null)
                Destroy(forObj);

                fS.nama = "";
                fS.namaFormasi = "";
                fS.jarak = 0;
                fS.urutanFormasi = 0;
                fS.sudut = 0;
                fS.satuan = PFormasi.satuanJarak.Nautical_Miles;
                fS.formasi = PFormasi.formasiType.None;
                fS.listObjekFormasi.Clear();
                fS.routePoints = new List<Vector2>();
                fS.info = "";
            }

            ResetForm();
        }
    }
}