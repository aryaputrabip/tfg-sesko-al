using System;
using System.Collections.Generic;
using HelperPlotting;
using ObjectStat;
using Plot.InputHandler;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

//List of Simple Functions
using static ListOfSimpleFunctions.Functions;
using static Plot.FormasiPlot.PerhitunganFormasiV2;
using static HelperPlotting.FormasiType;
using Plot.Pasukan;
using Unity.VisualScripting;
using Newtonsoft.Json.Linq;

namespace Plot.FormasiPlot
{
    public class PlotFormasiControllerV2 : MonoBehaviour
    {
        public static PlotFormasiControllerV2 instance;
        
        [Header("Prefab List")]
        [Space(5)]
        public GameObject prefabList;
        public GameObject prefabListTerserah;

        [Header("PFormasi Enumeration")]
        [Space(5)]
        public formasiType tipeFormasi;
        public satuanJarak satuanJarak;

        [Header("List")]
        [Space(5)]
        public List<Transform> selectedUnits;
        List<GameObject> contentList = new();
        int u;

        [Header("Selection")]
        [Space(5)]
        public OnlineMapsMarker3D selectedMarker;
        public bool isEditing;
        public bool isSelecting;
        bool isDragging;
        Vector3 mousePos;
        RaycastHit hit;
        
        [Header("Others")]
        [Space(5)]
        public OnlineMapsDeclaration MAP;
        public Canvas canvas;
        public GambarFormasi gambarFormasi;
        public bool fromOtherCanvas;

        [Header("Movement")]
        public bool isMoving;
        public Vector2 prevWorldPos;
        public Vector2 nowWorldPos;
        bool isDragMove;

        void Awake()
        {
            if(instance == null)
                instance = this;
            else
                Destroy(gameObject);
        }

        void Update()
        {
            ChangeType();
            MultipleSelect();
            if(isMoving) Move();
        }

        public void SelectionSwitch(bool switches)
        {
            isSelecting = switches;
            MAP.onlineMaps.blockAllInteractions = switches;
        }

        void ChangeType()
        {
            if(canvas.formasiDropdown.value == 8)
            {
                canvas.contentBiasa.SetActive(false);
                canvas.contentCustom.SetActive(true);
                canvas.content = canvas.contentCustom;
            }
            else
            {
                canvas.contentBiasa.SetActive(true);
                canvas.contentCustom.SetActive(false);
                canvas.content = canvas.contentBiasa;
            }
            canvas.scrollView.content = canvas.content.GetComponent<RectTransform>();

            if (canvas.formasiDropdown.value != 0)
            {
                canvas.formasiImage.gameObject.SetActive(true);
            }
            else
            {
                canvas.formasiImage.gameObject.SetActive(false);
            }

            switch(canvas.formasiDropdown.value)
            {
                case 0:
                    canvas.formasiImage.sprite = null;
                    tipeFormasi                = formasiType.None;
                    break;
                case 1:
                    canvas.formasiImage.sprite = gambarFormasi.belahKetupat;
                    tipeFormasi                = formasiType.Belah_Ketupat;
                    break;
                case 2:
                    canvas.formasiImage.sprite = gambarFormasi.flankKanan;
                    tipeFormasi                = formasiType.Flank_Kanan;
                    break;
                case 3:
                    canvas.formasiImage.sprite = gambarFormasi.flankKiri;
                    tipeFormasi                = formasiType.Flank_Kiri;
                    break;
                case 4:
                    canvas.formasiImage.sprite = gambarFormasi.sejajar;
                    tipeFormasi                = formasiType.Sejajar;
                    break;
                case 5:
                    canvas.formasiImage.sprite = gambarFormasi.serial;
                    tipeFormasi                = formasiType.Serial;
                    break;
                case 6:
                    canvas.formasiImage.sprite = gambarFormasi.panah;
                    tipeFormasi                = formasiType.Panah;
                    break;
                case 7:
                    canvas.formasiImage.sprite = gambarFormasi.kerucut;
                    tipeFormasi                = formasiType.Kerucut;
                    break;
                case 8:
                    canvas.formasiImage.sprite = null;
                    tipeFormasi                = formasiType.Custom;
                    break;
            }
            
            switch(canvas.jarakDropdown.value)
            {
                case 0:
                    satuanJarak = satuanJarak.NauticalMiles;
                    break;
                case 1:
                    satuanJarak = satuanJarak.Kilometer;
                    break;
                case 2:
                    satuanJarak = satuanJarak.Feet;
                    break;
                case 3:
                    satuanJarak = satuanJarak.Yard;
                    break;
                case 4:
                    satuanJarak = satuanJarak.Meter;
                    break;
            }
        }

        void MultipleSelect()
        {
            if(isSelecting)
            {
                if(Input.GetMouseButtonDown(0))
                {
                    mousePos = Input.mousePosition;

                    Ray ray = Camera.main.ScreenPointToRay(mousePos);

                    if (Physics.Raycast(ray, out hit))
                    {
                        var unitHit = hit.transform;
                        var layerMask = unitHit.gameObject.layer;
                        var pF = unitHit.GetComponent<PFormasiV2>();
                        
                        switch (layerMask)
                        {
                            case 7:
                                if(pF != null)
                                {
                                    var fS = pF.formasi;
                                    if(fS.type == formasiType.None)
                                    {
                                        if (!selectedUnits.Contains(unitHit))
                                            SelectUnit(unitHit, true);
                                    }
                                }
                                else
                                    Debug.Log("Unit Ini Sudah Berada Dalam Formasi!!!");
                                break;
                            default:
                                isDragging = true;
                                break;
                        }
                    }
                }

                if(Input.GetMouseButtonUp(0))
                {
                    foreach(OnlineMapsMarker3D child in OnlineMapsMarker3DManager.instance)
                    {
                        var units = child.instance.transform;
                        var pF = units.gameObject.GetComponent<PFormasiV2>();
                        if (pF != null)
                        {
                            var fS = pF.formasi;
                            if (fS.type == formasiType.None)
                            if (IsWithinSelectionBounds(units))
                            if (units.gameObject.activeSelf == true)
                                SelectUnit(units, true);
                        }
                    }

                    isDragging = false;
                }
            }

            if(u < selectedUnits.Count)
            {
                for(var i = 0; i < selectedUnits.Count; i++)
                {
                    var unit = selectedUnits[i];
                    var pF = unit.GetComponent<PFormasiV2>();
                    var fS = pF.formasi;

                    var font = "";
                    var fontType = "TandaTanda";
                    var nama = "";

                    if (pF.stat != null)
                    {
                        var idFont = int.Parse(pF.stat.pasukan.style.index);
                        font = ((char)idFont).ToString();

                        fontType = pF.stat.pasukan.style.nama;
                        nama = pF.stat.pasukan.info.nama_satuan;
                    }
                    else
                    {
                        var style  = StylePasukan.FromJson(pF.jStat.style);
                        var info   = InfoPasukan.FromJson(pF.jStat.info);
                        var idFont = int.Parse(style.index);

                        font = ((char)idFont).ToString();
                        fontType = style.nama.ToString();
                        nama = info.nama_satuan;
                    }

                    GameObject copy = null;
                    if(canvas.formasiDropdown.value != 8)
                    copy = Instantiate(prefabList, canvas.content.transform);
                    else
                    copy = Instantiate(prefabListTerserah, canvas.content.transform);

                    Metadata meta = copy.GetComponent<Metadata>();
                    Component FindPara<Component>(string obj) => meta.FindParameter(obj).parameter.GetComponent<Component>();

                    FindPara<TMP_Text>("No.").text = i == 0 ? "x" : (i + 1).ToString();
                    FindPara<Text>("Symbol").text = font;
                    FindPara<Text>("Symbol").font = Resources.Load<Font>("Fonts/Simbol Taktis/" + fontType);
                    FindPara<TMP_Text>("Nama").text = nama;

                    if(fS.type != formasiType.None)
                    {
                        FindPara<TMP_InputField>("Jarak Input").text = fS.jarakAntarSatuan.ToString();
                        
                        if(tipeFormasi == formasiType.Custom)
                        FindPara<TMP_InputField>("Arah Input").text = fS.arah.ToString();
                    }

                    if (i == 0)
                    {
                        meta.FindParameter("Jarak Input").parameter.SetActive(false);

                        if(tipeFormasi == formasiType.Custom)
                        meta.FindParameter("Arah Input").parameter.SetActive(false);
                    }

                    Button delSetting = meta.FindParameter("Delete").parameter.GetComponent<Button>();
                    delSetting.onClick.AddListener(delegate { Remove(unit, copy); });

                    contentList.Add(copy);
                    u++;
                }
            }
            else if (u > selectedUnits.Count)
            {
                DestroyContent();
                u = 0;
            }

            if(canvas.content != canvas.contentBefore)
            {
                DestroyContent();
                u = 0;
                canvas.contentBefore = canvas.content;
            }

            if(selectedUnits.Count == 0)
            DestroyContent();
        }
        
        public void SelectUnit(Transform unit, bool canMultiSelect = false)
        {
            if(!canMultiSelect)
            DeselectUnit();
            
            if(!selectedUnits.Contains(unit)) selectedUnits.Add(unit);
        }

        public void DeselectUnit()
        {
            selectedUnits.Clear();
            DestroyContent();
        }

        public void DestroyContent()
        {
            foreach(var item in contentList)
                Destroy(item);
            contentList.Clear();
        }

        public void Remove(Transform unit, GameObject unitInList)
        {
            selectedUnits.Remove(unit);
            Destroy(unitInList);
        }
        
        private bool IsWithinSelectionBounds(Transform tf)
        {
            if (!isDragging)
            {
                return false;
            }

            Camera cam = Camera.main;
            Bounds vpBounds = DragGUI.GetVPBounds(cam, mousePos, Input.mousePosition);
            return vpBounds.Contains(cam.WorldToViewportPoint(tf.position));
        }
        
        private void OnGUI()
        {
            if (isDragging)
            {
                Rect rect = DragGUI.GetScreenRect(mousePos, Input.mousePosition);
                DragGUI.DrawScreenRect(rect, new Color(0f, 0f, 0f, 0.25f));
                DragGUI.DrawScreenRectBorder(rect, 3, Color.blue);
            }
        }

        public void ResetForm()
        {
            if(isEditing)
            {
                var marker = selectedMarker.instance;
                var pF = marker != null ? marker.GetComponent<PFormasiV2>() : selectedUnits[0].GetComponent<PFormasiV2>();

                if(fromOtherCanvas)
                {
                    if(pF.stat != null) PlotPasukanControllerV2.instance.OnMarkerClick(selectedMarker);
                    else if(pF.jStat != null) PlotSatuanController.Instance.EditObject(selectedMarker);

                    fromOtherCanvas = false;
                }
            }
            
            DeselectUnit();
            canvas.namaFormasi.text = "";
            canvas.arah.text = "";
            canvas.formasiDropdown.value = 0;
            canvas.jarakDropdown.value = 0;
            canvas.btnPlot.SetActive(true);
            canvas.btnEdit.SetActive(false);
            canvas.btnMove.SetActive(false);
            canvas.btnDelete.SetActive(false);
            canvas.btnKegiatan.SetActive(false);
        }

        public void Plot()
        {
            var isCustom  = tipeFormasi == formasiType.Custom; Debug.Log("Custom = " + isCustom);
            var jarakList = new List<float>();
            var arahList  = new List<int>();
            
            if(canvas.arah.text == "" || canvas.arah.text == "-" || canvas.arah.text == ".")
            canvas.arah.text = "0";

            if (selectedUnits.Count == 0)
                Debug.Log("Anda Belum Memilih Unit!!");
            else if (selectedUnits.Count == 1)
                Debug.Log("Unit Yang Dipilih Harus Lebih Dari 1!!!");
            else if (tipeFormasi == formasiType.None)
                Debug.Log("Anda Belum Memilih Formasi Yang Ingin Digunakan!!!");
            else
            {
                for(int i = 0; i < contentList.Count; i++)
                {
                    Component FindPara<Component>(string src) => contentList[i].GetComponent<Metadata>().FindParameter(src).parameter.GetComponent<Component>();
                    if(i != 0)
                    {
                        var jarak = FindPara<TMP_InputField>("Jarak Input").text;
                        if(jarak == "" || jarak == "-" || jarak == ".") { Debug.Log("Harap Input Data Yang Benar!!!"); return; }
                        else jarakList.Add(float.Parse(jarak));

                        if(isCustom)
                        {
                            var arah = FindPara<TMP_InputField>("Arah Input").text;
                            if(arah == "" || arah == "-" || arah == ".") { Debug.Log("Harap Input Data Yang Benar!!!"); return; }
                            else arahList.Add(int.Parse(arah) + int.Parse(canvas.arah.text));
                        }
                    }
                }
                
                GetFormasi(jarakList, float.Parse(canvas.arah.text), isCustom ? arahList : null);
                Debug.Log("Data Benar!!!");
            }
        }

        public void GetFormasi(List<float> jarakList, float arah, List<int> arahList = null)
        {
            var pF = selectedUnits[0].GetComponent<PFormasiV2>();
            var fS = pF.formasi;
            var newJarakList = KonversiJarak(jarakList, satuanJarak);
            var newJarakLat = Jarak(newJarakList, arah, tipeFormasi, "Latitude", arahList ?? null);
            var newJarakLng = Jarak(newJarakList, arah, tipeFormasi, "Longitude", arahList ?? null);
            var namaSatuan = fS.nama == string.Empty ? CreateName("formasi", "entity-formasi") : fS.nama;
            var arrArah = arahList ?? Heading(selectedUnits.Count - 1, tipeFormasi, arah);

            ToFormation(selectedUnits, newJarakLng, newJarakLat, tipeFormasi);
            SetDataFormasi(selectedUnits, tipeFormasi, satuanJarak, namaSatuan, canvas.namaFormasi.text, arah, arrArah, jarakList);

            var objectFormasi = GameObject.Find(namaSatuan) ?? new GameObject(namaSatuan);
            objectFormasi.tag = "entity-formasi";
            objectFormasi.transform.parent = CreateMarkerParent("Data Formasi");
            var data = objectFormasi.GetComponent<DataFormasi>() ?? objectFormasi.AddComponent<DataFormasi>();
            data.entity = CreateEntityFormasi(canvas.namaFormasi.text, namaSatuan, tipeFormasi, arrArah, selectedUnits);
            canvas.toggler.toggleOff();
        }

        public void DeleteFormasi()
        {
            var pF = selectedUnits[0].GetComponent<PFormasiV2>();
            var fS = pF.formasi;
            var trash = GameObject.Find("Trash") ?? new GameObject("Trash");
            var tScript = trash.GetComponent<TrashObj>() ?? trash.AddComponent<TrashObj>();
            var tObj = new Trash()
            {
                id = fS.nama,
                type = "formasi"
            };
            
            EntityEditorController.Instance.plotTrash.Add(
                new JObject()
                {
                    { "id", fS.nama },
                    { "type", "formasi" }
                }
            );
            
            tScript.trash.Add(tObj);
            Destroy(GameObject.Find(fS.nama).gameObject);

            for(int i = 0; i < selectedUnits.Count; i++)
            {
                var pFF = selectedUnits[i].GetComponent<PFormasiV2>();
                pFF.formasi = new();
                
                var jM = selectedUnits[i].GetComponent<JSONDataMisiSatuan>();
                if(jM.listRoute.Count > 0)
                for(int j = 0; j < jM.listRoute.Count; j++)
                {
                    var jMObj = jM.listRoute[j].gameObject;
                    var ttObj = new Trash
                    {
                        id = jMObj.name,
                        type = "misi"
                    };
                    tScript.trash.Add(ttObj);

                    EntityEditorController.Instance.plotTrash.Add(
                        new JObject()
                        {
                            { "id", jMObj.name },
                            { "type", "misi" }
                        }
                    );
                    
                    Destroy(jMObj);
                    jM.listRoute = new();
                }
            }
            
            canvas.toggler.toggleOff();
        }

        public void MoveToggle(bool toggle)
        {
            isMoving = toggle;
            OnlineMapsTileSetControl.instance.allowUserControl = !toggle;
            foreach(var child in selectedUnits)
            {
                var pF = child.GetComponent<PFormasiV2>();
                var marker = pF.markers.marker3D;
                marker.OnPress = toggle ? marker.OnPress += OnMarkerPress : marker.OnPress -= OnMarkerPress;
                marker.OnRelease = toggle ? marker.OnRelease += OnMarkerRelease : marker.OnRelease -= OnMarkerRelease;
            }
        }

        void Move()
        {
            if(!isDragMove) return;
            
            double lng, lat;
            if (OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
            nowWorldPos = new Vector2((float)lng, (float)lat);
            
            if(nowWorldPos != prevWorldPos)
            {
                var diffWorldPos = nowWorldPos - prevWorldPos;

                foreach(var child in selectedUnits)
                {
                    var pF = child.GetComponent<PFormasiV2>();
                    pF.markers.marker3D.position += diffWorldPos;
                }
                prevWorldPos = nowWorldPos;
            }
        }

        void OnMarkerPress(OnlineMapsMarkerBase markerBase)
        {
            if(!isMoving) return;

            double lng, lat;
            if(OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
            prevWorldPos = new((float)lng, (float)lat);
            isDragMove = true;
        }

        void OnMarkerRelease(OnlineMapsMarkerBase markerBase)
        {
            if(!isMoving) return;
            isDragMove = false;

            var pF = selectedUnits[0].GetComponent<PFormasiV2>();
            var fS = pF.formasi;
            var data = GameObject.Find(fS.nama).GetComponent<DataFormasi>();
            data.entity.info_formasi.satuan_formasi = SetDataSatuan(selectedUnits);
        }
    }
    
    [Serializable]
    public class OnlineMapsDeclaration
    {
        public GameObject maps;
        public OnlineMaps onlineMaps;
    }

    [Serializable]
    public class Canvas
    {
        [Header("Input Field")]
        public TMP_InputField namaFormasi;
        public TMP_InputField arah;
        public TMP_Dropdown formasiDropdown;
        public TMP_Dropdown jarakDropdown;
        public GameObject formasiUI;
        public Image formasiImage;

        [Header("Scroll View")]
        public ScrollRect scrollView;
        public GameObject content;
        public GameObject contentBefore;
        public GameObject contentBiasa;
        public GameObject contentCustom;
        
        [Header("Button")]
        public GameObject btnPlot;
        public GameObject btnEdit;
        public GameObject btnMove;
        public GameObject btnDelete;
        public GameObject btnKegiatan;

        [Header("Side Menu Toggler")]
        public SidemenuToggler toggler;
    }

    [Serializable]
    public class GambarFormasi
    {
        public Sprite belahKetupat;
        public Sprite flankKanan;
        public Sprite flankKiri;
        public Sprite sejajar;
        public Sprite serial;
        public Sprite panah;
        public Sprite kerucut;
    }
}