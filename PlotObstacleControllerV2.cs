using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

using static ListOfSimpleFunctions.Functions;
using GlobalVariables;
using System.Linq;
using Plot.Drawings.Stat;
using HelperPlotting;
using Newtonsoft.Json.Linq;

namespace Plot.Obstacle
{
    public class PlotObstacleControllerV2 : MonoBehaviour
    {
        [Header("Instance")]
        [Space(5)]
        public static PlotObstacleControllerV2 instance;

        [Header("Canvas")]
        [Space(5)]
        [SerializeField] CanvasObs canvas = new();
        [SerializeField] string fontObstacle = "TandaTanda";
        
        [Header("Prefab")]
        [Space(5)]
        public GameObject prefab;
        public GameObject prefabList;

        [Header("List")]
        [Space(5)]
        [SerializeField] List<string> obstacles = new();
        bool haveSelected;
        int indexObs;

        [Header("Selected Unit")]
        [Space(5)]
        [HideInInspector] public OnlineMapsMarker3D editMarker;

        [Header("Plotting")]
        [Space(5)]
        [SerializeField] List<Vector2> points;
        OnlineMapsDrawingElement element;

        void Awake()
        {
            if(instance == null) instance = this;
            else Destroy(gameObject);
        }

        void Start()
        {
            ListingObstacle();
        }

        void ListingObstacle()
        {
            if (obstacles.Count <= 0) return;

            for (int i = 0; i < obstacles.Count; i++)
            {
                var objObstacle = Instantiate(prefabList, canvas.contentList);
                var metadata = objObstacle.GetComponent<Metadata>();
                int index = i;

                metadata.FindParameter("obstacle-symbol").parameter.GetComponent<Text>().text = obstacles[i];
                metadata.FindParameter("obstacle-symbol").parameter.GetComponent<Text>().font = Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + fontObstacle);
                
                objObstacle.GetComponent<Button>().onClick.AddListener(delegate { OnObstacleSelected(metadata.FindParameter("obstacle-symbol").parameter.GetComponent<Text>().text, index + 1); });
            }
        }

        public void OnObstacleSelected(string index, int obstacleIndex)
        {
            if(!canvas.selectedObstacleGroup.activeSelf) canvas.selectedObstacleGroup.SetActive(true);
            
            var metadata = canvas.selectedObstacleGroup.GetComponent<Metadata>();
            metadata.FindParameter("obstacle-symbol").parameter.GetComponent<Text>().text = index;
            metadata.FindParameter("obstacle-symbol").parameter.GetComponent<Text>().font = Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + fontObstacle);

            haveSelected = true;
            indexObs = obstacleIndex;
        }
        
        public void ResetForm()
        {
            canvas.input_jumlah.text = "";
            canvas.input_size.text   = "";
            if(!canvas.input_jumlah.gameObject.activeSelf) canvas.input_jumlah.gameObject.SetActive(true);
            canvas.selectedObstacleGroup.SetActive(false);
            element = null;
            editMarker = null;
        }

        public void Plot()
        {
            if(canvas.input_jumlah.text == "" || canvas.input_jumlah.text == "." || canvas.input_jumlah.text == "-") { Debug.Log("Anda Belum Menentukan Jumlah Obstacle Yang Akan Di Plot!!!"); return; }
            else if(canvas.input_jumlah.text == "0") { Debug.Log("Jumlah Obstacle Tidak Boleh 0"); return; }
            else if(canvas.input_jumlah.text.Contains("-")) { Debug.Log("Jumlah Obstacle Tidak Boleh Negatif!!!"); return; }
            
            if(canvas.input_size.text == "" || canvas.input_size.text == "." || canvas.input_size.text == "-") { Debug.Log("Anda Belum Menentukan Ukuran Obstacle Yang Akan Di Plot!!!"); return; }
            else if(canvas.input_size.text == "0") { Debug.Log("Ukuran Obstacle Tidak Boleh 0"); return; }
            else if(canvas.input_size.text.Contains("-")) { Debug.Log("Ukuran Obstacle Tidak Boleh Negatif!!!"); return; }
            
            points = new();
            OnlineMapsControlBase.instance.OnMapClick += AddPolyLine;

            var button = canvas.confirmMenu.GetComponent<Metadata>().FindParameter("confirm").parameter.GetComponent<Button>();
            button.onClick.AddListener(delegate{ConfirmDraw();});
            
            canvas.confirmMenu.GetComponent<Animator>().Play("FadeIn");
            GetComponent<Animator>().Play("SlideOut");
        }

        void AddPolyLine()
        {
            double lng, lat;
            OnlineMapsControlBase.instance.GetCoords(out lng, out lat);
            points.Add(new((float)lng, (float)lat));

            var warna = SessionUser.nama_asisten == "ASOPS" ? Color.blue : Color.red;
            var warnaBack = warna;
            warnaBack.a = .5f;

            if(element != null) element.Dispose();

            element = AddElement(DrawPoly(points, warna, 2f, warnaBack));
            element.DrawOnTileset(OnlineMapsTileSetControl.instance, OnlineMapsDrawingElementManager.instance.Count - 1);

            element.instance.tag = "draw-polygon";
            element.name         = CreateDrawingName("polygon", "draw-polygon");
            
            var poly        = element as OnlineMapsDrawingPoly;
            var coordinates = new List<List<List<float>>>();
            var coordinate  = new List<List<float>>();

            foreach(Vector2 point in poly.points)
            {
                var lngLat      = new List<float>()
                {
                    point.x,
                    point.y
                };

                coordinate.Add(lngLat);
            }
            coordinates.Add(coordinate);

            var dS = element.instance.AddComponent<DrawStatV2>();
            dS.entity = new()
            {
                nama     = element.name,
                id_user  = (int)SessionUser.id,
                dokumen  = CBSendiri.nama_document,
                geometry = DrawingGeometriesPoly.ToString(new()
                {
                    type        = "Polygon",
                    coordinates = coordinates
                }),
                properties = new()
                {
                    stroke    = true,
                    color     = warna == Color.blue ? "blue" : "red",
                    weight    = poly.borderWidth,
                    opacity   = .5f,
                    fill      = true,
                    fillColor = "#" + ColorUtility.ToHtmlStringRGB(poly.backgroundColor),
                    clickable = true,
                    id_point  = element.name
                },
                type = "Polygon"
            };

            OnlineMaps.instance.Redraw();
        }

        void ConfirmDraw()
        {
            if(points.Count <= 3) { Debug.Log("Point Polygon Tidak Boleh Kurang Dari 4!!!"); return; }
            
            var count = int.Parse(canvas.input_jumlah.text);
            OnlineMapsControlBase.instance.OnMapClick -= AddPolyLine;

            var poly   = element as OnlineMapsDrawingPoly;
            var obsObj = new GameObject("obstacle_area_" + element.name);
            obsObj.transform.parent = CreateMarkerParent("Obstacle Obj");

            var obsObjScript = obsObj.AddComponent<ObstacleObj>();
            obsObjScript.obstacleList = new();
            obsObjScript.element      = element;

            for(int i = 0; i < count; i++)
            {
                var point  = GeneratePointInsidePolygon(points);
                var marker = Create3D(point, prefab);
                marker.sizeType = OnlineMapsMarker3D.SizeType.scene;
                marker.scale = float.Parse(canvas.input_size.text) * 2.5f;
                marker.instance.name = "obstacle_" + i + "_" + element.name + "_" + SessionUser.id + "_" + Randomizer(1, 100);

                var metadata = canvas.selectedObstacleGroup.GetComponent<Metadata>();
                var symbol   = metadata.FindParameter("obstacle-symbol").parameter.GetComponent<Text>().text;

                var meta = marker.instance.GetComponent<Metadata>();
                Component FindPara<Component>(string obj) => meta.FindParameter(obj).parameter.GetComponent<Component>();
                FindPara<Text>("symbol").text  = symbol;
                FindPara<Text>("symbol").color = poly.borderColor;
                
                Destroy(marker.instance.GetComponent<ObstacleStat>());
                var oS = marker.instance.AddComponent<ObstacleStatV2>();
                oS.entity = new()
                {
                    id_user = (int)SessionUser.id,
                    nama    = marker.instance.name,
                    dokumen = CBSendiri.nama_document,
                    lat_y   = marker.position.y,
                    lng_x   = marker.position.x,
                    info_obstacle = new()
                    {
                        nama    = marker.instance.name,
                        font    = symbol,
                        warna   = poly.borderColor == Color.blue ? "blue" : "red",
                        size    = int.Parse(canvas.input_size.text),
                        info    = "TandaTanda",
                        index   = i.ToString(),
                        id_user = (int)SessionUser.id
                    },
                    symbol  = new()
                    {
                        className = "my-div-icon",
                        html      = "<div style='margin-top:-15px; width: 30px;color: blue; font-size: 33px; font-family: TandaTanda'>" + symbol + "</div>",
                        id_point  = marker.instance.name
                    }
                };
                oS.polygon = element.name;
                oS.index   = indexObs;

                obsObjScript.obstacleList.Add(oS.entity);

                marker.OnClick += OnMarkerClick;
            }
            canvas.confirmMenu.GetComponent<Animator>().Play("FadeOut");
            ResetForm();
        }

        public void OnMarkerClick(OnlineMapsMarkerBase markerBase)
        {
            editMarker = markerBase as OnlineMapsMarker3D;
            var oS = editMarker.instance.GetComponent<ObstacleStatV2>();
            element = OnlineMapsDrawingElementManager.instance.FirstOrDefault(x => x.name == oS.polygon);
            
            canvas.input_size.text = oS.entity.info_obstacle.size.ToString();
            OnObstacleSelected(oS.entity.info_obstacle.font, oS.index);

            canvas.editOptions.SetActive(true);
            canvas.btnPlot.SetActive(false);

            canvas.canvasForm.GetComponent<Animator>().Play("SlideIn");
        }

        public void Delete()
        {
            var oS = editMarker.instance.GetComponent<ObstacleStatV2>();
            var obsObj = GameObject.Find("obstacle_area_" + oS.polygon);
            var obsObjScript = obsObj.GetComponent<ObstacleObj>();
            
            var trash = GameObject.Find("Trash") ?? new GameObject("Trash");

            var tObj = new Trash
            {
                id = editMarker.instance.name,
                type = "obstacle"
            };

            var tScript = trash.GetComponent<TrashObj>() ?? trash.AddComponent<TrashObj>();
            tScript.trash.Add(tObj);
            
            EntityEditorController.Instance.plotTrash.Add(
                new JObject()
                {
                    { "id", editMarker.instance.name },
                    { "type", "obstacle" }
                }
            );
            obsObjScript.obstacleList.Remove(oS.entity);

            if(obsObjScript.obstacleList.Count <= 0) 
            {        
                var tTObj = new Trash
                {
                    id = obsObjScript.element.name,
                    type = "tool"
                };
                tScript.trash.Add(tTObj);
                
                EntityEditorController.Instance.plotTrash.Add
                (
                    new JObject()
                    {
                        { "id", obsObjScript.element.name },
                        { "type", "tool" }
                    }
                );
                
                element.Dispose(); Destroy(obsObj);
            }
            
            OnlineMapsMarker3DManager.RemoveItem(editMarker);
            canvas.canvasForm.GetComponent<Animator>().Play("SlideOut");
            ResetForm();
        }

        public void DeleteAll()
        {
            var oS = editMarker.instance.GetComponent<ObstacleStatV2>();
            var obsObj = GameObject.Find("obstacle_area_" + oS.polygon);
            var obsObjScript = obsObj.GetComponent<ObstacleObj>();
            
            var trash = GameObject.Find("Trash") ?? new GameObject("Trash");
            var tScript = trash.GetComponent<TrashObj>() ?? trash.AddComponent<TrashObj>();

            foreach(var child in obsObjScript.obstacleList)
            {
                var tObj = new Trash
                {
                    id = child.nama,
                    type = "obstacle"
                };
                tScript.trash.Add(tObj);
                
                EntityEditorController.Instance.plotTrash.Add(
                    new JObject()
                    {
                        { "id", child.nama },
                        { "type", "obstacle" }
                    }
                );

                OnlineMapsMarker3DManager.RemoveItem(OnlineMapsMarker3DManager.instance.FirstOrDefault(x => x.instance.name == child.nama));
            }
            obsObjScript.obstacleList.Clear();

            if(obsObjScript.obstacleList.Count <= 0) 
            {        
                var tTObj = new Trash
                {
                    id = obsObjScript.element.name,
                    type = "tool"
                };
                tScript.trash.Add(tTObj);
                
                EntityEditorController.Instance.plotTrash.Add
                (
                    new JObject()
                    {
                        { "id", obsObjScript.element.name },
                        { "type", "tool" }
                    }
                );
                
                element.Dispose(); Destroy(obsObj);
            }

            canvas.canvasForm.GetComponent<Animator>().Play("SlideOut");
            ResetForm();
        }

        Vector2 GeneratePointInsidePolygon(List<Vector2> polygon)
        {
            Vector2 MinVec = MinPointOnThePolygon(polygon);
            Vector2 MaxVec = MaxPointOnThePolygon(polygon);
            Vector2 GenVector;
            
            float x = Randomizer(MinVec.x, MaxVec.x);
            float y = Randomizer(MinVec.y, MaxVec.y);
            GenVector = new Vector2(x, y);
            
            while(!IsPointInPoly(polygon, GenVector))
            {
                x = Randomizer(MinVec.x, MaxVec.x);
                y = Randomizer(MinVec.y, MaxVec.y);
                GenVector.x = x;
                GenVector.y = y;
            }
            return GenVector;
        }

        Vector2 MinPointOnThePolygon(List<Vector2> polygon)
        {
            float minX = polygon[0].x;
            float minY = polygon[0].y;
            
            for (int i = 1; i < polygon.Count; i++)
            {
                if(minX > polygon[i].x)
                {
                    minX = polygon[i].x;
                }

                if (minY > polygon[i].y)
                {
                    minY = polygon[i].y;
                }
            }
            return new Vector2(minX, minY);
        }

        Vector2 MaxPointOnThePolygon(List<Vector2> polygon)
        {
            float maxX = polygon[0].x;
            float maxY = polygon[0].y;

            for (int i = 1; i < polygon.Count; i++)
            {
                if (maxX < polygon[i].x)
                {
                    maxX = polygon[i].x;
                }

                if (maxY < polygon[i].y)
                {
                    maxY = polygon[i].y;
                }
            }
            return new Vector2(maxX, maxY);
        }
    }
}

[Serializable]
public class CanvasObs
{
    [Header("Canvas Obj")]
    [Space(5)]
    public GameObject canvasForm;
    public TMP_InputField input_jumlah;
    public TMP_InputField input_size;
    public GameObject confirmMenu;

    [Header("References")]
    [Space(5)]
    public Transform contentList;
    public GameObject selectedObstacleGroup;

    [Header("Buttons")]
    [Space(5)]
    public GameObject btnPlot;
    public GameObject editOptions;
}