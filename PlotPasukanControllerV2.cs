using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GlobalVariables;
using HelperPlotting;
using Newtonsoft.Json.Linq;
using ObjectStat;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

//Simple Functions
using static ListOfSimpleFunctions.Functions;
using static HelperPlotting.FormasiType;
using static Plot.FormasiPlot.PerhitunganFormasiV2;
using static Plot.FormasiPlot.PerhitunganMisiFormasi;
using Plot.FormasiPlot;

namespace Plot.Pasukan
{
    public class PlotPasukanControllerV2 : MonoBehaviour
    {
        public static PlotPasukanControllerV2 instance;

        [Header("Prefabs")]
        public Sprite icon;
        public GameObject listPrefab;
        public GameObject pasukanPrefab;

        [Header("Editing")]
        [HideInInspector] public OnlineMapsMarker3D editMarker;

        [Header("Lists")]
        List<GameObject> contentListInstantiated = new();
        public List<ListPasukanV2> listPasukan;
        private ListPasukanV2 pasukan;
        private string matraBefore;

        [Header("Hide List")]
        [SerializeField] string hideList;

        [Header("Others")]
        public OnlineMapsDeclare map = new();
        public CanvasUI canvas = new();
        [HideInInspector] public int banyaknyaSatuan = 0;

        [Header ("Declaration")]
        [SerializeField] private bool isMoving;
        [SerializeField] private bool isDrag;
        [SerializeField] private bool haveSelected;
        private LabelStylePasukan labelStyle = new()
        {
            label_symbol = null,
            margin_bottom = -10,
            margin_right = 0,
            margin_top = 0,
            label_symbol_tab2 = null,
            margin_bottom_tab2 = -10,
            margin_right_tab2 = 0,
            margin_top_tab2 = 0,
            label_symbol_tab3 = null,
            margin_bottom_tab3 = -10,
            margin_right_tab3 = 0,
            margin_top_tab3 = 0,
            label_symbol_tab4 = null,
            margin_bottom_tab4 = -10,
            margin_right_tab4 = 0,
            margin_top_tab4 = 0,
        };

        void Awake()
        {
            if(instance == null)
                instance = this;
            else
                Destroy(gameObject);
        }

        void Start()
        {
            map.onlineMaps.OnChangeZoom += OnChangeZoom;
        }

        void Update()
        {
            IsMoving();
        }

        public async Task GrabList()
        {
            var json = await API.Instance.GetListAlutsista("pasukan");
            listPasukan = ListPasukanV2.FromJson(json);
        }

        private List<ListPasukanV2> SortList(List<ListPasukanV2> list)
        {
            var hapus = hideList.Split(", ").ToList();
            var listSorted = new List<ListPasukanV2>();
            
            listSorted.AddRange(list);
            for (int i = 0; i < listSorted.Count; i++)
                for (int j = 0; j < hapus.Count; j++)
                    if (listSorted[i].name.Contains(hapus[j]))
                        listSorted.Remove(listSorted[i]);
            listSorted = listSorted.OrderBy(go => go.name).ToList();

            return listSorted;
        }

        private void InstantiateList(List<ListPasukanV2> list)
        {
            for(int i = 0; i < list.Count; i++)
            {
                var now = list[i];
                GameObject copy = Instantiate(listPrefab, canvas.content);

                TMP_Text number = copy.transform.Find("No.").GetComponent<TMP_Text>();
                Text symbol = copy.transform.Find("Image/Text (Legacy)").GetComponent<Text>();
                TMP_Text namaObj = copy.transform.Find("Nama").GetComponent<TMP_Text>();
                TMP_Text matraPas = copy.transform.Find("Matra").GetComponent<TMP_Text>();
                Button selBtnCopy = copy.transform.Find("Btn - Select").GetComponent<Button>();

                string fontCopy = now.nama;
                string ketPas = now.keterangan;
                string symID = now.pasukan_sym_id;

                number.text = (i + 1).ToString();
                symbol.text = ((char)int.Parse(now.index)).ToString();
                symbol.font = Resources.Load<Font>("Fonts/Simbol Taktis/" + fontCopy);
                namaObj.text = now.name;
                matraPas.text = now.matra_pasukan;
                selBtnCopy.onClick.AddListener(delegate { OnPasukanSelected(now.id, now); });

                if (!contentListInstantiated.Contains(copy))
                    contentListInstantiated.Add(copy);
            }
        }

        public void CreateList(string matra)
        {
            var list = new List<ListPasukanV2>();
            var listSorted = SortList(listPasukan);
            matraBefore = matra;

            ClearList();
            foreach(var pasukan in listSorted)
                if(pasukan.matra_pasukan == matra)
                    list.Add(pasukan);
            InstantiateList(list);
        }

        public void SearchList(TMP_InputField search)
        {
            var list = new List<ListPasukanV2>();
            var listSorted = SortList(listPasukan);
            
            ClearList();
            foreach(var pasukan in listSorted)
                if(pasukan.name.ToLower().Contains(search.text.ToLower()))
                    list.Add(pasukan);
            InstantiateList(list);
        }

        public void ClearList()
        {
            foreach(var item in contentListInstantiated)
                Destroy(item);
            contentListInstantiated.Clear();
        }

        public void ResetForm()
        {
            if (canvas.selectedPasukanGroup.activeSelf) canvas.selectedPasukanGroup.SetActive(false);
            canvas.inputNama.text = "";
            canvas.inputKeterangan.text = "";
            canvas.inputKecepatan.text = "";
            canvas.inputBahanBakar.text = "40";
            canvas.searchBox.text = "";
            canvas.dropdownKecepatan.value = 0;
            editMarker = null;

            foreach (Transform child in map.map.transform)
            {
                if (child.GetComponentInChildren<BoxCollider>() != null)
                    child.GetComponentInChildren<BoxCollider>().enabled = true;
            }

            canvas.plotBtn.SetActive(true);
            canvas.editBtn.SetActive(false);
            canvas.moveBtn.SetActive(false);
            canvas.deleteBtn.SetActive(false);
            canvas.editOptions.SetActive(false);
            haveSelected = false;
        }

        public void OnPasukanSelected(int index, ListPasukanV2 pas = null)
        {
            if (!canvas.selectedPasukanGroup.activeSelf) canvas.selectedPasukanGroup.SetActive(true);

            if(pas == null)
                foreach(var _pas in listPasukan)
                    if(_pas.id == index)
                        pas = _pas;

            var metadata = canvas.selectedPasukanGroup.GetComponent<Metadata>();
            metadata.FindParameter("pasukan-symbol").parameter.GetComponent<Text>().text = ((char)int.Parse(pas.index)).ToString();
            metadata.FindParameter("pasukan-symbol").parameter.GetComponent<Text>().font = Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + pas.nama);
            metadata.FindParameter("pasukan-name").parameter.GetComponent<TMP_Text>().text = pas.name;
            metadata.FindParameter("pasukan-matra").parameter.GetComponent<TMP_Text>().text = pas.matra_pasukan;
            canvas.inputNama.text = pas.name;
            canvas.inputKecepatan.text = pas.speed_pasukan;
            canvas.inputKeterangan.text = pas.keterangan;
            pasukan = pas;
            haveSelected = true;
        }

        public void Plot()
        {
            if(canvas.inputNama.text == "")
            Debug.Log("Anda Belum Menginput Nama!");
            else if(canvas.inputHeading.text == "-" || canvas.inputHeading.text == ".")
            Debug.Log("Harap Mengisi Heading Dengan Benar!!!");
            else
            {
                canvas.toggle.toggleOff();
                OnlineMapsControlBase.instance.OnMapClick += OnMapClick;
            }
        }

        private void OnChangeZoom()
        {
            OnlineMaps map = OnlineMaps.instance;
            foreach (OnlineMapsMarker3D marker in OnlineMapsMarker3DManager.instance)
            {
                ShowMarkerLabelsByZoomItem item = marker["data"] as ShowMarkerLabelsByZoomItem;
                if (item == null) continue;
                
                // Update marker labels.
                marker.label = item.zoomRange.InRange(map.zoom) ? item.label : "";
            }
        }

        public void OnMapClick()
        {
            double lng, lat;
            var point = new Vector2();
            if(OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
            point = new Vector2((float)lng, (float)lat);

            var marker = Create3D(point, pasukanPrefab);
            marker["markerSource"] = marker;
            marker.label = canvas.inputNama.text;
            marker["data"] = new ShowMarkerLabelsByZoomItem(marker.label, new OnlineMapsRange(1, 19));
            marker.sizeType = OnlineMapsMarker3D.SizeType.meters;
            marker.scale = 1.7f;
            marker.rotationY = canvas.inputHeading.text != "" ? int.Parse(canvas.inputHeading.text) : 0;
            marker.instance.name  = CreateNameSatuan("satuan");
            marker.instance.layer = 7;
            marker.transform.tag  = "entity-satuan";
            marker.transform.parent = CreateMarkerParent("Satuan");
            Destroy(marker.instance.GetComponent<Stat>());
            Destroy(marker.instance.GetComponent<PFormasi>());

            var pas = marker.instance.AddComponent<StatV2>();
            pas.image = icon;
            pas.pasukan = new()
            {
                id_user = (int)SessionUser.id,
                id_symbol = pasukan.id,
                dokumen = SkenarioAktif.ID_DOCUMENT,
                nama = marker.instance.name,
                lng_x = (float)lng,
                lat_y = (float)lat,
                style = new()
                {
                    nama               = pasukan.nama,
                    index              = pasukan.pasukan_sym_id,
                    keterangan         = pasukan.keterangan,
                    grup               = 10,
                    label_style        = new List<LabelStylePasukan>(){labelStyle},
                },
                info = new()
                {
                    nomer_satuan       = "NS" + Randomizer(1, 999),
                    nama_satuan        = marker.label,
                    nomer_atasan       = "AT" + Randomizer(1, 999),
                    tgl_selesai        = "",
                    warna              = SessionUser.nama_asisten == "ASOPS" ? "blue" : "red",
                    size               = 30,
                    weapon             = Weapon.ToString(new()),
                    waypoint           = new(),
                    list_embarkasi     = new(),
                    armor              = int.Parse(pasukan.health_pasukan),
                    id_dislokasi       = false,
                    id_dislokasi_obj   = false,
                    bahan_bakar        = int.Parse(canvas.inputBahanBakar.text),
                    bahan_bakar_load   = 0,
                    kecepatan          = canvas.inputKecepatan.text + "|" + canvas.dropdownKecepatan.options[canvas.dropdownKecepatan.value].text.ToLower(),
                    kecepatan_maks     = "undefined",
                    heading            = marker.rotationY.ToString(),
                    ket_satuan         = canvas.inputKeterangan.text,
                    nama_icon_satuan   = "",
                    width_icon_satuan  = "",
                    height_icon_satuan = "",
                    personil           = 1500,
                    tfg                = true,
                    hidewgs            = true
                },
                symbol = "<div style='text-align:center'><span style='font-weight: bolder;color: blue; font-size: 21px; font-family: TAKTIS_AD_2'>H</span></div>",
                id_kegiatan = null,
                isi_logistik = null
            };
            pas.markers = new()
            {
                marker3D = marker
            };
            pas.json = new()
            {
                String = EntityPasukanV2.ToString(pas.pasukan)
            };
            pas.pasukan.matra     = listPasukan.Find(child => child.id == pas.pasukan.id_symbol).matra_pasukan;
            pas.pasukan.tipe_tni  = pas.pasukan.matra == "Darat" ? "AD" : pas.pasukan.matra == "Laut" ? "AL" : "AU";

            var pFor = marker.instance.AddComponent<PFormasiV2>();

            // Add Functions on Marker
            marker.OnClick += OnMarkerClick;
            marker.OnPress += OnMarkerPress;
            marker.OnRelease += OnMarkerRelease;

            // Add Simbol Taktis Component
            SetHUD(marker, pas.pasukan.info.nama_satuan, pas.pasukan.info.warna, pas.pasukan.style.nama, pas.pasukan.style.index);

            OnlineMapsControlBase.instance.OnMapClick -= OnMapClick;
            ResetForm();
        }

        public void OnMarkerClick(OnlineMapsMarkerBase markerBase)
        {
            if(PlotFormasiControllerV2.instance.isMoving) return;
            
            var marker = markerBase as OnlineMapsMarker3D;
            var entity = marker.instance.GetComponent<StatV2>().pasukan;
            var fS     = marker.instance.GetComponent<PFormasiV2>().formasi;
            var speeds = entity.info.kecepatan.Split("|");
            var speed  = speeds[0];

            OnPasukanSelected(entity.id_symbol);
            canvas.inputNama.text = entity.info.nama_satuan;
            canvas.inputKecepatan.text = speed;
            canvas.inputKeterangan.text = entity.info.ket_satuan;
            canvas.inputHeading.text = entity.info.heading;
            
            canvas.plotBtn.SetActive(false);
            canvas.editBtn.SetActive(true);
            canvas.moveBtn.SetActive(true);
            canvas.deleteBtn.SetActive(true);
            canvas.editOptions.SetActive(true);
            
            var btnFormasi = canvas.editOptions.transform.Find("Btn - Formasi").gameObject;
            if(fS.type != formasiType.None) btnFormasi.SetActive(true);
            else btnFormasi.SetActive(false);

            canvas.toggle.toggleOn();

            editMarker = marker;
        }

        public void OnMarkerPress(OnlineMapsMarkerBase markerBase)
        {
            if(isMoving)
            isDrag = true;
        }

        public void OnMarkerRelease(OnlineMapsMarkerBase markerBase)
        {
            if(isMoving)
            isDrag = false;
        }

        public void Move()
        {
            var pF = editMarker.instance.GetComponent<PFormasiV2>();
            var fS = pF.formasi;
            if(fS.type == formasiType.None)
            {
                isMoving = true;
                OnlineMaps.instance.GetComponent<OnlineMapsTileSetControl>().allowUserControl = false;
                map.map.transform.GetComponentInChildren<BoxCollider>().enabled = false;
                editMarker.instance.GetComponent<BoxCollider>().enabled = true;
                canvas.confirmMove.onClick.AddListener(delegate{ConfirmMove();});
                canvas.modalMove.GetComponent<Animator>().Play("FadeIn");
                GetComponent<Animator>().Play("SlideOut");
            }
            else { Debug.Log("Unit ini sedang berada dalam Formasi!!!"); return; }
        }

        public void ConfirmMove()
        {
            OnlineMaps.instance.GetComponent<OnlineMapsTileSetControl>().allowUserControl = true;
            map.map.transform.GetComponentInChildren<BoxCollider>().enabled = true;
            canvas.modalMove.GetComponent<Animator>().Play("FadeOut");
            isMoving = false;
            GetComponent<Animator>().Play("SlideIn");
        }

        private void IsMoving()
        {
            if(isDrag)
            {
                double lng, lat;
                if(OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
                editMarker.position = new Vector2((float)lng, (float)lat);

                var entity = editMarker.instance.GetComponent<StatV2>().pasukan;
                entity.lng_x = (float)lng;
                entity.lat_y = (float)lat;
            }
        }

        public void Edit()
        {
            if(canvas.inputNama.text == "")
            Debug.Log("Anda Belum Menginput Nama!");
            else if(canvas.inputHeading.text == "-" || canvas.inputHeading.text == ".")
            Debug.Log("Harap Mengisi Heading Dengan Benar!!!");
            else
            {
                var entity = editMarker.instance.GetComponent<StatV2>().pasukan;
                var info   = entity.info;
                var style  = entity.style;

                editMarker.label = canvas.inputNama.text;
                editMarker.rotationY = canvas.inputHeading.text != "" ? int.Parse(canvas.inputHeading.text) : 0;

                info.nama_satuan = editMarker.label;
                info.ket_satuan  = canvas.inputKeterangan.text;
                info.heading     = editMarker.rotationY.ToString();
                info.bahan_bakar = int.Parse(canvas.inputBahanBakar.text);
                info.kecepatan   = canvas.inputKecepatan.text + "|" + canvas.dropdownKecepatan.options[canvas.dropdownKecepatan.value].text.ToLower();

                style.index      = pasukan.pasukan_sym_id;
                style.keterangan = pasukan.keterangan;
                style.nama       = pasukan.nama;
                SetHUD(editMarker, info.nama_satuan, info.warna, style.nama, style.index);

                canvas.toggle.toggleOff();
            }
        }

        public void Delete()
        {
            if(editMarker.instance.GetComponent<PFormasiV2>().formasi.type == formasiType.None)
            {
                var trash = GameObject.Find("Trash") ?? new GameObject("Trash");

                var tObj = new Trash
                {
                    id = editMarker.instance.name,
                    type = "satuan"
                };

                var tScript = trash.GetComponent<TrashObj>() ?? trash.AddComponent<TrashObj>();
                tScript.trash.Add(tObj);
                
                EntityEditorController.Instance.plotTrash.Add(
                    new JObject()
                    {
                        { "id", editMarker.instance.name },
                        { "type", "satuan" }
                    }
                );
                
                var jM = editMarker.instance.GetComponent<JSONDataMisiSatuan>();
                if(jM.listRoute.Count > 0)
                for(int i = 0; i < jM.listRoute.Count; i++)
                {
                    var ttObj = new Trash
                    {
                        id = jM.listRoute[i].gameObject.name,
                        type = "misi"
                    };
                    tScript.trash.Add(ttObj);
                    
                    EntityEditorController.Instance.plotTrash.Add
                    (
                        new JObject()
                        {
                            { "id", jM.listRoute[i].gameObject.name },
                            { "type", "misi" }
                        }
                    );

                    Destroy(jM.listRoute[i].gameObject);
                    jM.listRoute.RemoveAt(i);
                }
                jM.listRoute = new();
                OnlineMapsMarker3DManager.RemoveItem(editMarker);

                ResetForm();
                canvas.toggle.toggleOff();
            }
            else
            Debug.Log("Unit ini sedang berada dalam formasi!!");
        }

        public void Formasi()
        {
            canvas.toggle.toggleOff();

            var pF = editMarker.instance.GetComponent<PFormasiV2>();
            var fS = pF.formasi;
            var fController = PlotFormasiControllerV2.instance;
            fController.selectedMarker = editMarker;
            fController.canvas.toggler.toggleOn();
            fController.selectedUnits.AddRange(fS.unitsInFormasi);
            fController.fromOtherCanvas = true;
            fController.isEditing = true;

            var fCanvas = fController.canvas;
            fCanvas.namaFormasi.text = fS.namaFormasi;
            fCanvas.formasiDropdown.value = JenisFormasi(fS.type);
            fCanvas.jarakDropdown.value = JenisSatuanJarak(fS.satuan, true) - 1;
            fCanvas.arah.text = fS.arahUtama.ToString();
            fCanvas.btnPlot.SetActive(false);
            fCanvas.btnEdit.SetActive(true);
            fCanvas.btnMove.SetActive(true);
            fCanvas.btnDelete.SetActive(true);
            fCanvas.btnKegiatan.SetActive(true);
        }
        
        public void Kegiatan()
        {
            var pF = editMarker.instance.GetComponent<PFormasiV2>();
            var fS = pF.formasi;
            var pFF = fS.unitsInFormasi[0].GetComponent<PFormasiV2>();
            RouteController.Instance.markerInstance = pFF.markers.marker3D;
            RouteController.Instance.jarakList = KonversiJarak(JarakList(fS.unitsInFormasi), fS.satuan);

            if(pFF.stat != null)
            {
                canvas.btnKegiatanSat.SetActive(false);
                canvas.btnKegiatanPas.SetActive(true);
                canvas.btnCloseKegiatanSat.SetActive(false);
                canvas.btnCloseKegiatanPas.SetActive(true);
                canvas.btnDeleteKegiatanSat.SetActive(false);
                canvas.btnDeleteKegiatanPas.SetActive(true);
            }
            else
            {
                canvas.btnKegiatanSat.SetActive(true);
                canvas.btnKegiatanPas.SetActive(false);
                canvas.btnCloseKegiatanSat.SetActive(true);
                canvas.btnCloseKegiatanPas.SetActive(false);
                canvas.btnDeleteKegiatanSat.SetActive(true);
                canvas.btnDeleteKegiatanPas.SetActive(false);
            }
        }
        
        public void ResetFormKegiatan()
        {
            RouteController.Instance.markerInstance = null;
            canvas.btnKegiatanSat.SetActive(true);
            canvas.btnKegiatanPas.SetActive(false);
            canvas.btnCloseKegiatanSat.SetActive(true);
            canvas.btnCloseKegiatanPas.SetActive(false);
            canvas.btnDeleteKegiatanSat.SetActive(true);
            canvas.btnDeleteKegiatanPas.SetActive(false);
        }
    }

    [System.Serializable]
    public class OnlineMapsDeclare
    {
        public GameObject map;
        public OnlineMaps onlineMaps;
        public OnlineMapsMarker3DManager manager;
    }

    [System.Serializable]
    public class CanvasUI
    {
        [Header("Canvas")]
        public GameObject canvas;
        
        [Header("Input Field")]
        public TMP_InputField inputNama;
        public TMP_InputField inputKeterangan;
        public TMP_InputField inputHeading;
        public TMP_InputField inputKecepatan;
        public TMP_Dropdown dropdownKecepatan;
        public TMP_InputField inputBahanBakar;
        public TMP_InputField searchBox;

        [Header("GameObjects")]
        public GameObject modalMove;
        
        [Header("List Object")]
        public GameObject selectedPasukanGroup;
        public Transform content;
        
        [Header("Buttons")]
        public GameObject plotBtn;
        public GameObject editBtn;
        public GameObject moveBtn;
        public GameObject deleteBtn;
        public GameObject editOptions;
        public GameObject btnKegiatanSat;
        public GameObject btnKegiatanPas;
        public GameObject btnCloseKegiatanSat;
        public GameObject btnCloseKegiatanPas;
        public GameObject btnDeleteKegiatanSat;
        public GameObject btnDeleteKegiatanPas;
        public Button confirmMove;
        public SidemenuToggler toggle;
    }
}
