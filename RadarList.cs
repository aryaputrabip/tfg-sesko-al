using UnityEngine;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace HelperPlotting
{
    using System.Linq;
    using Data.Connect;

    [Serializable]
    public class ListRadarSatuan
    {
        [JsonIgnore] [SerializeField] int? _AIRCRAFT_ID;
        [JsonIgnore] [SerializeField] int? _SHIP_ID;
        [JsonIgnore] [SerializeField] int? _VEHICLE_ID;
        [JsonIgnore] [SerializeField] List<Radar> _RADARS;
        public int? AIRCRAFT_ID { get{ return _AIRCRAFT_ID; } set{ _AIRCRAFT_ID = value; } }
        public int? SHIP_ID { get{ return _SHIP_ID; } set{ _SHIP_ID = value; } }
        public int? VEHICLE_ID { get{ return _VEHICLE_ID; } set{ _VEHICLE_ID = value; } }
        public List<Radar> RADARS { get{ return _RADARS; } set{ _RADARS = value; } }

        [JsonConstructor]
        public ListRadarSatuan(string string_agg)
        {
            RADARS = new();
            var strings = string_agg.Split(",").ToList();
            foreach(var child in strings)
            {
                foreach(var _child in DataConnect.instance.radarList.listRadar)
                if(_child.RADAR_ID.ToString() == child)
                RADARS.Add(_child);
            }
        }
        public ListRadarSatuan(){}

        public static List<ListRadarSatuan> FromJson(string json) => JsonConvert.DeserializeObject<List<ListRadarSatuan>>(json);
        public static string ToString(List<ListRadarSatuan> json) => JsonConvert.SerializeObject(json, Formatting.Indented);
    }
    
    [Serializable]
    public class Radar
    {
        [JsonIgnore] [SerializeField] int _RADAR_ID;
        [JsonIgnore] [SerializeField] string _RADAR_NAME;
        [JsonIgnore] [SerializeField] string _RADAR_ELEVATION;
        [JsonIgnore] [SerializeField] float _RADAR_DET_RANGE;
        [JsonIgnore] [SerializeField] string _DESCRIPTION;
        [JsonIgnore] [SerializeField] int? _RADAR_SYM_ID;
        [JsonIgnore] public bool isOn;
        public int RADAR_ID { get{ return _RADAR_ID; } set{ _RADAR_ID = value; } }
        public string RADAR_NAME { get{ return _RADAR_NAME; } set{ _RADAR_NAME = value; } }
        public string RADAR_ELEVATION { get{ return _RADAR_ELEVATION; } set{ _RADAR_ELEVATION = value; } }
        public float RADAR_DET_RANGE { get{ return _RADAR_DET_RANGE; } set{ _RADAR_DET_RANGE = value; } }
        public string DESCRIPTION { get{ return _DESCRIPTION; } set{ _DESCRIPTION = value; } }
        public int? RADAR_SYM_ID { get{ return _RADAR_SYM_ID; } set{ _RADAR_SYM_ID = value; } }

        public static List<Radar> FromJson(string json) => JsonConvert.DeserializeObject<List<Radar>>(json);
        public static string ToString(Radar json) => JsonConvert.SerializeObject(json, Formatting.Indented);
    }
    
    [Serializable]
    public class RadarEntity
    {
        [JsonIgnore] [SerializeField] string _id;
        [JsonIgnore] [SerializeField] string _id_user;
        [JsonIgnore] [SerializeField] string _dokumen;
        [JsonIgnore] [SerializeField] string _nama;
        [JsonIgnore] [SerializeField] float _lat;
        [JsonIgnore] [SerializeField] float _lng;
        [JsonIgnore] [SerializeField] RadarInfo _info_radar;
        [JsonIgnore] [SerializeField] RadarInfoSymbol _symbol;
        [JsonIgnore] [SerializeField] string _info_symbol;
        public string id { get{ return _id; } set{ _id = value; } }
        public string id_user { get{ return _id_user; } set{ _id_user = value; } }
        public string dokumen { get{ return _dokumen; } set{ _dokumen = value; } }
        public string nama { get{ return _nama; } set{ _nama = value; } }
        [JsonProperty("lat_y")] public float lat { get{ return _lat; } set{ _lat = value; } }
        [JsonProperty("lng_x")] public float lng { get{ return _lng; } set{ _lng = value; } }
        
        [JsonConverter(typeof(RadarInfoStringConverter))]
        public RadarInfo info_radar { get{ return _info_radar; } set{ _info_radar = value; } }
        
        [JsonConverter(typeof(RadarInfoSymbolStringConverter))]
        public RadarInfoSymbol symbol { get{ return _symbol; } set{ _symbol = value; } }
        public string info_symbol { get{ return _info_symbol; } set{ _info_symbol = value; } }

        public static string ToString(RadarEntity json) => JsonConvert.SerializeObject(json, Formatting.Indented);
        public static RadarEntity FromJson(string json) => JsonConvert.DeserializeObject<RadarEntity>(json);
    }

    [Serializable]
    public class RadarInfo
    {
        [JsonIgnore] [SerializeField] string _nama;
        [JsonIgnore] [SerializeField] string _judul;
        [JsonIgnore] [SerializeField] int _radius;
        [JsonIgnore] [SerializeField] int _size;
        [JsonIgnore] [SerializeField] string _warna;
        [JsonIgnore] [SerializeField] int _id_user;
        [JsonIgnore] [SerializeField] IdSymbol _id_symbol;
        [JsonIgnore] [SerializeField] string _jenis_radar;
        [JsonIgnore] [SerializeField] string _jml_peluru;
        [JsonIgnore] [SerializeField] string _new_index;
        [JsonIgnore] [SerializeField] int _armor;
        public string nama { get{ return _nama; } set{ _nama = value; } }
        public string judul { get{ return _judul; } set{ _judul = value; } }
        public int radius { get{ return _radius; } set{ _radius = value; } }
        public int size { get{ return _size; } set{ _size = value; } }
        public string warna { get{ return _warna; } set{ _warna = value; } }
        public int id_user { get{ return _id_user; } set{ _id_user = value; } }
        public IdSymbol id_symbol { get{ return _id_symbol; } set{ _id_symbol = value; } }
        public string jenis_radar { get{ return _jenis_radar; } set{ _jenis_radar = value; } }
        public string jml_peluru { get{ return _jml_peluru; } set{ _jml_peluru = value; } }
        public string new_index { get{ return _new_index; } set{ _new_index = value; } }
        public int armor { get{ return _armor; } set{ _armor = value; } }

        public static RadarInfo FromJson(string json) => JsonConvert.DeserializeObject<RadarInfo>(json, HelperConverter.Converter.Settings);
    }

    [Serializable]
    public class IdSymbol
    {
        [JsonIgnore] [SerializeField] string _id;
        [JsonIgnore] [SerializeField] string _nama;
        [JsonIgnore] [SerializeField] string _index;
        [JsonIgnore] [SerializeField] string _keterangan;
        [JsonIgnore] [SerializeField] string _grup;
        [JsonIgnore] [SerializeField] string _entity_name;
        [JsonIgnore] [SerializeField] string _entity_kategori;
        [JsonIgnore] [SerializeField] string _jenis_role;
        [JsonIgnore] [SerializeField] int? _health;
        public string id { get{ return _id; } set{ _id = value; } }
        public string nama { get{ return _nama; } set{ _nama = value; } }
        public string index { get{ return _index; } set{ _index = value; } }
        public string keterangan { get{ return _keterangan; } set{ _keterangan = value; } }
        public string grup { get{ return _grup; } set{ _grup = value; } }
        public string entity_name { get{ return _entity_name; } set{ _entity_name = value; } }
        public string entity_kategori { get{ return _entity_kategori; } set{ _entity_kategori = value; } }
        public string jenis_role { get{ return _jenis_role; } set{ _jenis_role = value; } }
        public int? health { get{ return _health; } set{ _health = value; } }

        public static IdSymbol FromJson(string json) => JsonConvert.DeserializeObject<IdSymbol>(json, HelperConverter.Converter.Settings);
    }

    [Serializable]
    public class RadarInfoSymbol
    {
        [JsonIgnore] [SerializeField] string _className;
        [JsonIgnore] [SerializeField] string _html;
        [JsonIgnore] [SerializeField] List<int> _iconSize;
        [JsonIgnore] [SerializeField] List<int> _iconAnchor;
        [JsonIgnore] [SerializeField] string _id_point;
        [JsonIgnore] [SerializeField] string _id_radar;
        public string className { get{ return _className; } set{ _className = value; } }
        public string html { get{ return _html; } set{ _html = value; } }
        public List<int> iconSize { get{ return _iconSize; } set{ _iconSize = value; } }
        public List<int> iconAnchor { get{ return _iconAnchor; } set{ _iconAnchor = value; } }
        public string id_point { get{ return _id_point; } set{ _id_point = value; } }
        public string id_radar { get{ return _id_radar; } set{ _id_radar = value; } }

        public static RadarInfoSymbol FromJson(string json) => JsonConvert.DeserializeObject<RadarInfoSymbol>(json, HelperConverter.Converter.Settings);
    }
    
    public class RadarInfoStringConverter : JsonConverter<RadarInfo>
    {
        public override void WriteJson(JsonWriter writer, RadarInfo value, JsonSerializer serializer)
        {
            writer.WriteRawValue(JsonConvert.SerializeObject(value, Formatting.Indented));
        }

        public override RadarInfo ReadJson(JsonReader reader, Type objectType, RadarInfo existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            string s = (string)reader.Value;

            return JsonConvert.DeserializeObject<RadarInfo>(s);
        }
    }
    
    public class RadarInfoSymbolStringConverter : JsonConverter<RadarInfoSymbol>
    {
        public override void WriteJson(JsonWriter writer, RadarInfoSymbol value, JsonSerializer serializer)
        {
            writer.WriteRawValue(JsonConvert.SerializeObject(value, Formatting.Indented));
        }

        public override RadarInfoSymbol ReadJson(JsonReader reader, Type objectType, RadarInfoSymbol existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            string s = (string)reader.Value;

            return JsonConvert.DeserializeObject<RadarInfoSymbol>(s);
        }
    }
}
