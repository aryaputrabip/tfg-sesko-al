using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TMPro;
using UnityEngine;

public class SearchLocation : MonoBehaviour
{
    [SerializeField] TMP_InputField searchBar;
    [SerializeField] OnlineMapsKeyManager keyManager;
    [SerializeField] string jsonData;
    [SerializeField] string jsonFile;
    [SerializeField] string jsonFolder;
    private static List<GeoJSONParser.ParsedGeoData> lGeo;
    private static string searchText;
    private float resetTime;
    private AnimationCurve animationCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
    private static bool activeTransition;
    private static Vector2 targetLoc;
    private static float targetZoom;
    //private JArray kocag;

    OnlineMapsBingMapsLocation BingFind(string query, string key, int maxResults = 1, bool includeNeighborhood = true) 
    => OnlineMapsBingMapsLocation.FindByQuery(query, key, maxResults, includeNeighborhood);

    private void Start()
    {
        //kocag = JArray.Parse(jsonData);
        loadJsonFile();

        if (lGeo.Count < 1)
        lGeo = JsonConvert.DeserializeObject<List<GeoJSONParser.ParsedGeoData>>(jsonData); 
    }

    void Update()
    {
        if(searchBar.isFocused)
        OnPressEnter();

        if (activeTransition)
        {
            // Updating the progress of the animation
            resetTime += Time.deltaTime * 2;
            float t = resetTime / 3; // resetTime / duration

            // If the animation is finished
            if (t >= 1)
            {
                // Reset values
                resetTime = 0;
                activeTransition = false;
                t = 1;
            }

            // Update the camera
            var OM = OnlineMaps.instance;
            float f = animationCurve.Evaluate(t);

            OM.SetPosition(Mathf.Lerp(OM.position.x, targetLoc.x, f), Mathf.Lerp(OnlineMaps.instance.position.y, targetLoc.y, f));
            OM.floatZoom = Mathf.Lerp(OnlineMaps.instance.floatZoom, targetZoom, f);
        }
    }

    public void OnPressEnter()
    {
        if(Input.GetKeyDown(KeyCode.Return))
        OnSearch();
    }

    public void OnSearch()
    {
        var search = searchBar.text;
        searchText = search;

        var result = BingFind(search, keyManager.bingMaps);
        result.OnComplete += OnQueryComplete;
    }

    private static void OnQueryComplete(string response)
    {
        Debug.Log(response);

        // Get an array of results.
        OnlineMapsBingMapsLocationResult[] results = OnlineMapsBingMapsLocation.GetResults(response);
        if (results == null)
        {
            Debug.Log("No results from bing, using JSON..");
            var search = searchText;
            if (search.Length > 2)
            {
                var find = lGeo.FirstOrDefault(x => x.namaKota.Contains(search.ToUpper()));
                if (find != null)
                {
                    //OnlineMaps.instance.SetPositionAndZoom(find.centerLng, find.centerLat, find.zoomLevel);
                    targetLoc = new Vector2((float) find.centerLng, (float)find.centerLat);
                    targetZoom = find.zoomLevel;
                    activeTransition = true;
                }
            }
            return;
        }

        // Log results info.
        foreach (OnlineMapsBingMapsLocationResult result in results)
        {
            var map = OnlineMaps.instance;
            //map.position = result.location;

            // Get corners nodes
            var bounds = result.boundingBox;
            var sw = new Vector2(bounds.xMin, bounds.yMin);
            var ne = new Vector2(bounds.xMax, bounds.yMax);

            // Get best zoom
            Vector2 center;
            int zoom;
            OnlineMapsUtils.GetCenterPointAndZoom(new[] { sw, ne }, out center, out zoom);

            // Set map zoom
            map.zoom = zoom - 2;

            targetLoc = new Vector2((float)center.x, (float)center.y);
            targetZoom = zoom;
            activeTransition = true;

            Debug.Log(result.location);
            Debug.Log(result.name);
            Debug.Log(result.formattedAddress);
            foreach (KeyValuePair<string, string> pair in result.address)
            {
                Debug.Log(pair.Key + ": " + pair.Value);
            }
            Debug.Log("------------------------------");
        }
    }

    private void loadJsonFile()
    {
        var jFol = Path.Combine(Directory.GetCurrentDirectory(), jsonFolder);
        var jFil = Path.Combine(jFol, jsonFile);
        Debug.Log("Opening " + jFil.ToString() + "!");
        if (File.Exists(jFil))
        {
            Debug.Log("JSON data found! " + jFil.ToString());
            using (StreamReader r = new StreamReader(jFil))
            {
                string JSON = r.ReadToEnd();
                lGeo = JsonConvert.DeserializeObject<List<GeoJSONParser.ParsedGeoData>>(JSON);
            }
        }
    }
}
