﻿using System.Collections;
using System.Collections.Generic;
using Plot.Pasukan;
using UnityEngine;

namespace ObjectStat
{
    public class Stat : MonoBehaviour
    {
        public enum unitType
        {
            Angkatan_Darat,
            Angkatan_Laut,
            Angkatan_Udara,
            Pasukan,
            Kekuatan
        };

        [Header("Stats")]
        public int userID;
        public Sprite icon;
        public string nama;
        public string namaSatuan;
        public string kecepatan;
        public string satuanKecepatan;
        public string heading;
        public string fuel;
        public string matra;
        public string keteranganPas;
        public string keterangan;
        public string warna;
        public unitType type;
        public string font;
        public string fontType;
        public string fontTypeID;
        public int index;
        public string style;
        public string info;
        public string symbol;
        public ListPasukan listPasukan;
        public List<PlotDataRoute> listRoute = new List<PlotDataRoute>();

        [Header("Marker")]
        [HideInInspector] public OnlineMapsMarker3D marker2D, marker3D;
    }
}
