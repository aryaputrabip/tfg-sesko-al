using System.Collections;
using System.Collections.Generic;
using HelperPlotting;
using UnityEngine;

namespace ObjectStat
{
    public class StatV2 : MonoBehaviour
    {
        [Header("Image")]
        public Sprite image;

        [Header("TFG")]
        [Space(5)]
        public Markers markers;
        public EntityPasukanV2 pasukan;

        [Header("WGS")]
        [Space(5)]
        public bool isEnemy;
        public bool showRadar;
        public bool showRadar2D;
        public bool showRadar3D = true;
        bool _showRadar, _showRadar2D, _showRadar3D = true;
        public int containedAsEnemyInList;
        public List<Radar> radars;
        public List<GameObject> radars3D = new();
        public List<OnlineMapsDrawingElement> radars2D = new();
        [HideInInspector] public List<GameObject> enemiesInView = new();
        public JSON json;

        [Header("RadarMovement")]
        [Space(5)]
        Vector2 prevPos;

        void Start()
        {
            prevPos = markers.marker3D.position;
        }

        void Update()
        {
            WGS();
        }

        void WGS()
        {
            if(radars2D.Count > 0)
            {
                if(prevPos != markers.marker3D.position)
                {
                    foreach(var _radar2D in radars2D)
                    {
                        var poly = _radar2D as OnlineMapsDrawingPoly;
                        poly.points = new ArrowManeuver().DrawCircle(markers.marker3D.position, _radar2D.radiusKM);
                    }
                    prevPos = markers.marker3D.position;
                }
            }
        }
    }
}

namespace HelperPlotting
{
    [System.Serializable]
    public class Markers
    {
        public OnlineMapsMarker3D marker3D;
    }

    [System.Serializable]
    public class JSON
    {
        [TextArea(15,20)] public string String;
    }
}
