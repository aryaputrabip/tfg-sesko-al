﻿using System.Collections.Generic;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif

using UnityEngine;

public class AlutsistaBundleData : MonoBehaviour
{
    public enum TNI { Angkatan_Darat, Angkatan_Laut, Angkatan_Udara }

    [Header("Common")]
    public Sprite image;
    public string name;
    public string id_symbol;
    public TNI tipeTNI;

    [Header("Node")]
    public GameObject _nodePropeller;
    public GameObject _nodeFrontDoor;
    public GameObject _nodeRearDoor;
    //public List<GameObject> nodeLifeboat;
    public List<GameObject> nodeHelipad;
    //public List<GameObject> nodeCannon;
    //public List<GameObject> nodeMachineGun;
    //public List<GameObject> nodeTorpedo;
    //public List<GameObject> nodeVariant;

    [Header("Material")]
    [HideInInspector] public List<BundleNodeRadar> nodeRadar = new List<BundleNodeRadar>();
    [HideInInspector] public List<BundleNodeWeapon> nodeWeapon = new List<BundleNodeWeapon>();
    [HideInInspector] public List<BundleVariant> variant = new List<BundleVariant>();

    public BundleVariant FindVariant(string key)
    {
        if (variant == null) return null;
        if (variant.Count <= 0) return null;

        IEnumerable<string> keys = from meta in variant where meta.name == key select meta.name;
        IEnumerable<Material> mats = from meta in variant where meta.name == key select meta.material;

        foreach (string keyselect in keys)
        {
            foreach (Material paramselect in mats)
            {
                var finded = new BundleVariant();
                finded.name = keyselect;
                finded.material = paramselect;

                return finded;
            }
        }

        return null;
    }

    #region CLASS VARIABLES
    [System.Serializable]
    public class BundleVariant
    {
        public string name;
        public Material material;
    }

    [System.Serializable]
    public class BundleNodeRadar
    {
        public GameObject obj;
        public int animationSpeed;
    }

    [System.Serializable]
    public class BundleNodeWeapon
    {
        public GameObject objBase;
        public GameObject objGun;       // (Optional) Can be Useful If Weapon Like A frontside canon that can be rotate up and down but the weapon base itself stay at 0
        public int traverse;            // Show The Maximum Rotation Weapon Can Rotate
        public Vector2 elevation;       // Show The Minimum & Maximum Elevation Weapon Can Rotate
        public int firingRate;          // Rate of Fire The Weapon Can Fire at (X) Time.
    }
    #endregion
}

#region Inspector (Editor)
#if UNITY_EDITOR
[CustomEditor(typeof(AlutsistaBundleData))]
public class AlutsistaBundleDataEditor : Editor
{
    AlutsistaBundleData bundleData;
    ReorderableList OL_Mat, OL_Radar, OL_Weapon;

    bool _nodeRadarState, _nodeWeaponState;

    string headerName;

    private void OnEnable()
    {
        if (target == null) return;
        bundleData = (AlutsistaBundleData)target;

        OL_Mat = new ReorderableList(serializedObject, serializedObject.FindProperty("variant"), true, true, true, true);
        OL_Radar = new ReorderableList(serializedObject, serializedObject.FindProperty("nodeRadar"), true, true, true, true);
        OL_Weapon = new ReorderableList(serializedObject, serializedObject.FindProperty("nodeWeapon"), true, true, true, true);

        //DrawHeader(OL_Mat, "Parameter");
        OL_Mat.drawHeaderCallback = DrawMaterialHeader;
        OL_Mat.drawElementCallback = DrawListItems_Mat; // Delegate to draw the elements on the list

        //DrawHeader(OL_Radar, "Radar List");
        OL_Radar.drawHeaderCallback = DrawRadarHeader;
        OL_Radar.drawElementCallback = DrawListItems_Radar;

        //DrawHeader(OL_Weapon, "Weapon List");
        OL_Weapon.drawHeaderCallback = DrawWeaponHeader;
        OL_Weapon.drawElementCallback = DrawListItems_Weapon;
    }

    //void DrawHeader(ReorderableList list, string name)
    //{
    //    headerName = name;
    //    list.drawHeaderCallback = DrawHeader;
    //}
    void DrawMaterialHeader(Rect rect)
    {
        EditorGUI.LabelField(rect, "Parametter");
    }
    void DrawRadarHeader(Rect rect)
    {
        EditorGUI.LabelField(rect, "Radar List");
    }
    void DrawWeaponHeader(Rect rect)
    {
        EditorGUI.LabelField(rect, "Weapon List");
    }

    void DrawListItems_Mat(Rect rect, int index, bool isActive, bool isFocused)
    {
        SerializedProperty element = OL_Mat.serializedProperty.GetArrayElementAtIndex(index);

        EditorGUI.PropertyField(
          new Rect(rect.x, rect.y, (rect.width - 32) / 2, EditorGUIUtility.singleLineHeight),
          element.FindPropertyRelative("name"), GUIContent.none
        );

        EditorGUI.PropertyField(
          new Rect(rect.x + ((rect.width - 32) / 2) + 2, rect.y, (rect.width) / 2 + 16, EditorGUIUtility.singleLineHeight),
          element.FindPropertyRelative("material"), GUIContent.none
        );
    }

    void DrawListItems_Radar(Rect rect, int index, bool isActive, bool isFocused)
    {
        SerializedProperty element = OL_Radar.serializedProperty.GetArrayElementAtIndex(index);

        //EditorGUI.LabelField(rect, "Radar List");
        EditorGUI.PropertyField(
          new Rect(rect.x, rect.y, (rect.width - 64), EditorGUIUtility.singleLineHeight),
          element.FindPropertyRelative("obj"), GUIContent.none
        );

        EditorGUI.PropertyField(
          new Rect(rect.x + rect.width - 60, rect.y, rect.width - (rect.width - 60), EditorGUIUtility.singleLineHeight),
          element.FindPropertyRelative("animationSpeed"), GUIContent.none
        );
    }

    void DrawListItems_Weapon(Rect rect, int index, bool isActive, bool isFocused)
    {
        SerializedProperty element = OL_Weapon.serializedProperty.GetArrayElementAtIndex(index);

        EditorGUI.PropertyField(
          new Rect(rect.x, rect.y, (rect.width - 64), EditorGUIUtility.singleLineHeight),
          element.FindPropertyRelative("objBase"), GUIContent.none
        );
    }

    void DrawHeader(Rect rect)
    {
        EditorGUI.LabelField(rect, headerName);
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        EditorGUILayout.Space();
        serializedObject.Update(); // Update the array property's representation in the inspector

        EditorGUILayout.LabelField("Node", EditorStyles.boldLabel);
        _nodeRadarState = EditorGUILayout.ToggleLeft("Radar Node", _nodeRadarState);
        if (_nodeRadarState)
        {
            // SET GUI
            EditorGUILayout.HelpBox("Untuk dapat men-assign animasi object radar pada asset, diperlukan Node Radar. \n\nPARAMETER = [GameObject, Animation Speed]", MessageType.Info);
            EditorGUILayout.Space();
            OL_Radar.DoLayoutList(); // Have the ReorderableList do its work
        }

        _nodeWeaponState = EditorGUILayout.ToggleLeft("Weapon Node", _nodeWeaponState);
        if (_nodeWeaponState)
        {
            // SET GUI
            EditorGUILayout.HelpBox("Untuk mendefinisikan object weapon pada asset yang dapat digunakan.", MessageType.Info);
            EditorGUILayout.Space();
            OL_Weapon.DoLayoutList(); // Have the ReorderableList do its work
        }

        EditorGUILayout.LabelField("Variant", EditorStyles.boldLabel);
        OL_Mat.DoLayoutList(); // Have the ReorderableList do its work

        // We need to call this so that changes on the Inspector are saved by Unity.
        serializedObject.ApplyModifiedProperties();
    }

    protected virtual void OnSceneGUI()
    {
        if (_nodeRadarState)
        {
            // SET GUI
            // SHOW NODE RADAR GAMEOBJECT
            for (int i = 0; i < bundleData.nodeRadar.Count; i++)
            {
                if (bundleData.nodeRadar[i].obj == null) continue;
                Handles.color = Color.red;

                GUIStyle style = new GUIStyle();
                style.normal.textColor = Color.blue;

                Handles.Label(bundleData.nodeRadar[i].obj.transform.position, bundleData.nodeRadar[i].obj.name, style);
            }

        }
    }
}
#endif
#endregion