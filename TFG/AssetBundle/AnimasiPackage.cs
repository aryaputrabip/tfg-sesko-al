using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimasiPackage : MonoBehaviour
{
    public List<GameObject> animExplosion;
    public List<GameObject> animPenerjunan;

    #region INSTANCE
    public static AnimasiPackage Instance;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion
}
