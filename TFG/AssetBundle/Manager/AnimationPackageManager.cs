using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationPackageManager : MonoBehaviour
{
    [Header("Radar")]
    public RuntimeAnimatorController radarAnim;

    public static AnimationPackageManager Instance;
    void Start()
    {
        // Set Animation Package Manager Menjadi 1 Origin
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
