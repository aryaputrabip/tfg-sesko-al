using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;
using TMPro;
using System;
using System.Xml.Serialization;
using SickscoreGames.HUDNavigationSystem;

public class AssetPackageManager : MonoBehaviour
{
    public enum assetPackageLocation { DataPath }

    [Header("Default Data")]
    public GameObject defaultVehicle;
    public GameObject defaultShip;
    public GameObject defaultAircraft;
    public GameObject defaultPasukan;
    public HUDElement defaultHUD = new();

    [Header("Prefab Data")]
    public GameObject prefabRadar;
    public GameObject prefabRadar2D;
    public GameObject prefabBungus;
    public GameObject prefabSituasi;
    public GameObject prefabLogistikBandara;
    public GameObject prefabLogistikPelabuhan;
    public GameObject prefabLogistikBebas;
    public GameObject prefabText;
    public GameObject prefabObstacle;
    public GameObject prefabIconCustom;
    public GameObject prefabAnimasi;
    public GameObject prefabVideo;

    [Header("References")]
    public TextMeshProUGUI LoadingNotif;

    [Header("Config")]
    public assetPackageLocation assetLocation;
    public string fileFormat = "dec3d";
    public string configFormat = "cfg";


    private string _dir;

    public List<string> ASSETS_NAME = new List<string>();
    public List<GameObject> ASSETS = new List<GameObject>();

    public static AssetPackageManager Instance;
    void Start()
    {
        // Set Asset Package Manager Menjadi 1 Origin
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        locateAssetDirectory();
    }

    /// <summary>
    /// -- Cari Lokasi Directory (folder) asset 3D disimpan --
    /// </summary>
    private void locateAssetDirectory()
    {
        switch (assetLocation)
        {
            case assetPackageLocation.DataPath:
                _dir = Directory.GetCurrentDirectory() + "/AssetPackage";
                break;
        }
    }

    public async Task loadPackageAlutsista()
    {
        if (_dir == null) return;
        if (!Directory.Exists(_dir))
        {
            Debug.Log("Directory tidak ditemukan!");
            return;
        }

        string[] bundleFolder = Directory.GetDirectories(_dir);

        if(bundleFolder.Length == 0)
        {
            Debug.Log("No Asset 3D Found");
        }
        else
        {
            for(int i=0; i < bundleFolder.Length; i++)
            {
                string[] bundleFiles = Directory.GetFiles(Path.Combine(bundleFolder[i]), "*." + fileFormat);
                string[] bundleConfig = Directory.GetFiles(Path.Combine(bundleFolder[i]), "*." + configFormat);

                int bundleFinishedLoad = 0;

                if (bundleFiles.Length >= 0)
                {
                    // Load and instantiate seluruh pack asset ke "DontDestroy" scene
                    for (int j = 0; j < bundleFiles.Length; j++)
                    {
                        try
                        {
                            bundleFinishedLoad += await SyncLoad(bundleFiles[j], bundleConfig[j], Path.Combine(bundleFolder[i]));
                        }catch(Exception e)
                        {
                            Debug.Log("Failed To Load Asset " + bundleFiles[j]);
                        }
                    }
                }

                if (bundleFinishedLoad == 0)
                {
                    // Use In-Game Default Bundle (tidak ada pack asset ditemukan)
                }
            }
        }


        ////string[] bundleFiles = Directory.GetFiles(_dir, "*." + fileFormat);
        ////int bundleFinishedLoad = 0;

        //if (bundleFiles.Length > 0)
        //{

        //    // Load and instantiate seluruh pack asset ke "DontDestroy" scene
        //    for (int i = 0; i < bundleFiles.Length; i++)
        //    {
        //        bundleFinishedLoad += await SyncLoad(bundleFiles[i], _dir);
        //    }
        //}
        //else
        //{
        //    Debug.Log("No Asset 3D Found");
        //}

        //if (bundleFinishedLoad == 0)
        //{
        //    // Use In-Game Default Bundle (tidak ada pack asset ditemukan)
            
        //}
    }

    async Task<int> SyncLoad(string file, string config, string bundleUrl)
    {
        var fileStream = new FileStream(Path.Combine(bundleUrl, file), FileMode.Open, FileAccess.Read);
        var BundleLoaded = AssetBundle.LoadFromStreamAsync(fileStream);

        //var configStream = new FileStream(Path.Combine(bundleUrl, config), FileMode.Open, FileAccess.Read);
        List<BundleConfig> _ASSETS = new List<BundleConfig>();

        var _XMLSerialize = new XmlSerializer(typeof(List<BundleConfig>));
        using (var reader = new StreamReader(Path.Combine(bundleUrl, config)))
        {
            _ASSETS = (List<BundleConfig>)_XMLSerialize.Deserialize(reader);
        }

        if (LoadingNotif)
        {
            LoadingNotif.text = "Loading " + Path.Combine(bundleUrl, file);
        }
        else
        {
            var loadingObj = GameObject.Find("Scene Loading Overlay(Clone)");
            if (loadingObj) LoadingNotif = loadingObj.GetComponent<Metadata>().FindParameter("message").parameter.GetComponent<TextMeshProUGUI>();
        }

        while (!BundleLoaded.isDone)
        {
            await Task.Yield();
        }

        if (BundleLoaded.assetBundle == null)
        {
            Debug.Log("Failed to load AssetBundle!");
            return 0;
        }
        else
        {
            var AssetInQueue = BundleLoaded.assetBundle.LoadAllAssetsAsync<GameObject>();

            while (!AssetInQueue.isDone)
            {
                await Task.Yield();
            }

            DontDestroyOnLoad(this);

            for (int i = 0; i < AssetInQueue.allAssets.Length; i++)
            {

                //Debug.Log("Load Asset ke " + i);

                var GO = AssetInQueue.allAssets[i] as GameObject;

                if (!ASSETS_NAME.Contains(GO.GetComponent<AlutsistaBundleData>().name))
                {
                    var metadata = GO.GetComponent<AlutsistaBundleData>();
                    ASSETS_NAME.Add(metadata.name);
                    ASSETS.Add(GO);

                    ConfigRadarNodes(metadata);
                }

                //using (TextReader reader = new StreamReader(configStream))
                //{
                //    string contents = reader.ReadToEnd();
                //    contents = contents.Replace("{", "").Replace("}", "").Replace("\n", "");

                //    //foreach(string nama_satuan in contents.Split(','))
                //    //{
                //    //    if (!ASSETS_NAME.Contains(nama_satuan.Replace("\n", "")))
                //    //    {
                //    //        ASSETS_NAME.Add(nama_satuan);
                //    //        ASSETS.Add(GO);
                //    //    }
                //    //}

                //    // Add List Service Into ListView

                //}

                for (int j = 0; j < _ASSETS.Count; j++)
                {
                    if (!ASSETS_NAME.Contains(_ASSETS[j].name))
                    {
                        ASSETS_NAME.Add(_ASSETS[j].name);
                        ASSETS.Add(GO);
                    }
                }

                //if (!ASSETS_NAME.Contains(GO.GetComponent<AlutsistaBundleData>().assetName))
                //{
                //    var metadata = GO.GetComponent<AlutsistaBundleData>();
                //    ASSETS_NAME.Add(metadata.assetName);
                //    ASSETS.Add(GO);
                //}




                //var GO = Instantiate(AssetInQueue.allAssets[i]) as GameObject;

                //
                //GO.name = metadata.assetPrefab.name;
                //GO.transform.position = new Vector3(i * 50, 0, 0);

                //assetName.text = metadata.assetName;
                //assetTipeTNI.text = metadata.tipe_tni.ToString();
                //assetPrefab.text = metadata.assetPrefab.name;
                //assetImage.sprite = metadata.assetImage;
                //assetNumber++;


                //DontDestroyOnLoad(GO);
            }

            BundleLoaded.assetBundle.Unload(false);
            return 1;
        }
    }

    private void ConfigRadarNodes(AlutsistaBundleData asset)
    {
        if (asset.nodeRadar.Count <= 0) return;
        if (AnimationPackageManager.Instance == null) return;

        for(int i=0; i < asset.nodeRadar.Count; i++)
        {
            if (asset.nodeRadar[i].obj == null) continue;

            var animator = asset.nodeRadar[i].obj.AddComponent<Animator>();
            animator.runtimeAnimatorController = AnimationPackageManager.Instance.radarAnim;
            animator.SetFloat("animSpeed", asset.nodeRadar[i].animationSpeed);
        }
    }
}

[Serializable]
public class BundleConfig
{
    [XmlAttribute("name")]
    public string name { get; set; }
    [XmlElement("variant")]
    public string variant { get; set; }
}


[Serializable]
public class HUDElement
{
    public HUDNavigationElement prefabHUDElement;
    public HUDNavigationElement prefabMarker2DElement;
}
