using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class PackAssetMeta : MonoBehaviour
{
  //[SerializeField] public string[]        KeyName;
  //[SerializeField] public GameObject[]    AssetList;

  public enum PackContent { PACK_DARAT, PACK_LAUT, PACK_UDARA, PACK_ALL }

  [Header("GENERATE PACKAGE")]
  [SerializeField] public string NamaPackage = "Package 1";
  [SerializeField] public PackContent packType;

  [SerializeField] Dictionary<string, string> abbb = new Dictionary<string, string>();

  public List<string> ASSET_NAME = new List<string>();
  public List<GameObject> ASSET_PREFAB = new List<GameObject>();
  public List<Sprite> ASSET_IMAGE = new List<Sprite>();

  //[SerializeField] List<string> ASSET_NAME        = new List<string>();
  //[SerializeField] List<GameObject> ASSET_PREFAB  = new List<GameObject>();
}
