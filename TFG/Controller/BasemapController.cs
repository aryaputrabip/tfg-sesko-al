using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BasemapController : MonoBehaviour
{
    [Header("References")]
    public GameObject prefabBasemap;
    public Sprite defaultImage;
    public Transform content;

    public string url_darat = "http://192.168.27.8:8080/geoserver/Openstreet_data/wms?service=WMS&version=1.1.0&request=GetMap&layers=Openstreet_data%3Aosm_basemap&bbox={lx},{by},{rx},{ty}&width=256&height=256&srs=EPSG%3A4326&styles=&format=image%2Fpng";
    public string url_udara = "http://192.168.27.8:8080/geoserver/Peta_Udara/wms?service=WMS&version=1.1.0&request=GetMap&layers=Peta_Udara%3APeta_Udara&bbox={lx},{by},{rx},{ty}&width=256&height=256&srs=EPSG%3A4326&styles=&format=image%2Fpng";
    public string url_laut = "http://192.168.27.8:8080/geoserver/ENC_2021_KELAUTAN_COKLAT/wms?service=WMS&version=1.1.0&request=GetMap&layers=ENC_2021_KELAUTAN_COKLAT%3AENC_2021_KELAUTAN_COKLAT&bbox={lx},{by},{rx},{ty}&width=256&height=256&srs=EPSG%3A4326&styles=&format=image%2Fpng";
    public string url_satellite = "http://192.168.27.8:8080/geoserver/Landsat_Indonesia/wms?service=WMS&version=1.1.0&request=GetMap&layers=Landsat_Indonesia%3ALandsat_Dunia&bbox={lx},{by},{rx},{ty}&width=256&height=256&srs=EPSG%3A4326&styles=&format=image%2Fpng";

    public string mapFile;
    public string mapFolder;

    private void Start()
    {
        mapFolder = Path.Combine(Directory.GetCurrentDirectory(), mapFolder);
        mapFile = Path.Combine(mapFolder, mapFile);
        LoadMapServiceFromFile();

        //OnlineMapsProvider.Create("mywms").AppendTypes(
        //    new OnlineMapsProvider.MapType("petadarat") { urlWithLabels = url_darat },
        //    new OnlineMapsProvider.MapType("petalaut") { urlWithLabels = url_laut },
        //    new OnlineMapsProvider.MapType("petaudara") { urlWithLabels = url_udara },
        //    new OnlineMapsProvider.MapType("peta_satellite") { urlWithLabels = url_satellite }
        //);


        //OnlineMaps.instance.mapType = "mywms.peta_satellite";
    }

    public void LoadMapServiceFromFile()
    {
        List<MapService> mapService = new List<MapService>();

        if (mapFile == null) {
            Debug.Log("Filename undefined"); return;
        }

        if (File.Exists(mapFile))
        {
            using (StreamReader r = new StreamReader(mapFile))
            {
                string JSON = r.ReadToEnd();
                mapService = JsonConvert.DeserializeObject<List<MapService>>(JSON);

                foreach ((MapService data, int i) in mapService.Select((value, i) => (value, i)))
                {
                    if (data.map_name == "" || data.map_name == null || data.provider == "" || data.provider == null) continue;

                    var listProvider = OnlineMapsProvider.GetProvidersTitle();
                    OnlineMapsProvider[] _provider = OnlineMapsProvider.GetProviders();
                    OnlineMapsProvider _activeProvider;

                    int index = Array.IndexOf(listProvider, data.provider);

                    if (index == -1)
                    {
                        _activeProvider = OnlineMapsProvider.Create("mywms");
                    }
                    else
                    {
                        _activeProvider = _provider[index];
                    }

                    if (data.is_custom)
                    {
                        if(data.variant_with_label != null || data.variant_without_label != null)
                        {
                            if (data.variant_with_label != null)
                            {
                                _activeProvider.AppendTypes(
                                    new OnlineMapsProvider.MapType(data.map_name) { variantWithLabels = data.variant_with_label }
                                );
                            }
                            
                            if(data.variant_without_label != null)
                            {
                                _activeProvider.AppendTypes(
                                    new OnlineMapsProvider.MapType(data.map_name) { variantWithoutLabels = data.variant_without_label }
                                );
                            }
                        }
                        else
                        {
                            _activeProvider.AppendTypes(
                                new OnlineMapsProvider.MapType(data.map_name) { urlWithLabels = data.url }
                            );
                        }
                    }
                    

                    var item = Instantiate(prefabBasemap, content);
                    item.name = "Basemap_" + data.map_name;

                    var map_label = item.GetComponent<Metadata>().FindParameter("label").parameter.GetComponent<TextMeshProUGUI>().text = data.label;
                    var map_image = item.GetComponent<Metadata>().FindParameter("image").parameter.GetComponent<Image>();
                    item.GetComponent<Button>().onClick.AddListener(delegate { setBaseMap(data.provider + "." + data.map_name); });

                    map_image.sprite = getImage(data.images);

                    // Set Default Selected Map
                    if (data.is_default) OnlineMaps.instance.mapType = data.provider + "." + data.map_name;
                }
            }
        }
        else
        {
            if (!Directory.Exists(mapFolder)) Directory.CreateDirectory(mapFolder);

            // Create New Map File if it's not found
            mapService.Add(new MapService()
            {
                provider = "virtualearth",
                label = "Satellite",
                map_name = "Aerial",
                images = "imagery.jpg",
                url = "",
                is_custom = false,
                is_default = true
            });
            mapService.Add(new MapService()
            {
                provider = "arcgis",
                label = "Streets",
                map_name = "worldstreetmap",
                images = "streets.jpg",
                url = "",
                is_custom = false,
                is_default = false
            });
            mapService.Add(new MapService()
            {
                provider = "arcgis",
                label = "Topographic",
                map_name = "worldtopomap",
                images = "topographic.jpg",
                url = "",
                is_custom = false,
                is_default = false
            });
            mapService.Add(new MapService()
            {
                provider = "arcgis",
                label = "National Geographic",
                map_name = "natgeoworldmap",
                images = "natgeo.jpg",
                url = "",
                is_custom = false,
                is_default = false
            });
            mapService.Add(new MapService()
            {
                provider = "arcgis",
                label = "Oceans",
                map_name = "oceanbasemap",
                images = "oceans.jpg",
                url = "",
                is_custom = false,
                is_default = false
            });
            mapService.Add(new MapService()
            {
                provider = "arcgis",
                label = "Light Gray",
                map_name = "worldgraycanvas",
                images = "lightGray.jpg",
                url = "",
                is_custom = false,
                is_default = false
            });

            string JSON = JsonConvert.SerializeObject(mapService);
            File.WriteAllText(mapFile, JSON);

            LoadMapServiceFromFile();
        }
    }

    private Sprite getImage(string image)
    {
        byte[] fileData;

        fileData = File.ReadAllBytes(Path.Combine(mapFolder, image));
        if (fileData == null) return defaultImage;

        Texture2D tex = new Texture2D(2, 2);
        tex.LoadImage(fileData);

        Rect rec = new Rect(0, 0, tex.width, tex.height);
        Sprite spriteToUse = Sprite.Create(tex, rec, new Vector2(0.5f, 0.5f), 100);

        return spriteToUse;

    }

    public void setBaseMap(string name)
    {
        OnlineMaps.instance.mapType = name;

        if (OnlineMaps.instance.mapType.Contains("mywms"))
        {
            OnlineMapsTile.OnReplaceURLToken += OnReplaceUrlToken;
        }
        else
        {
            OnlineMapsTile.OnReplaceURLToken = null;
        }
    }

    private string OnReplaceUrlToken(OnlineMapsTile tile, string token)
    {
        if (token == "lx") return tile.topLeft.x.ToString(OnlineMapsUtils.numberFormat);
        if (token == "by") return tile.bottomRight.y.ToString(OnlineMapsUtils.numberFormat);   
        if (token == "rx") return tile.bottomRight.x.ToString(OnlineMapsUtils.numberFormat);
        if (token == "ty") return tile.topLeft.y.ToString(OnlineMapsUtils.numberFormat);

        // Otherwise, return the token
        return token;
    }
}

public class MapService
{
    public string provider;
    public string label;
    public string map_name;
    public string images;
    public string url;
    public string variant_with_label;
    public string variant_without_label;
    public bool is_custom;
    public bool is_default;
}