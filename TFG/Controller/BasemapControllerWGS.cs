using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class BasemapControllerWGS : MonoBehaviour
{
    [Header("References")]
    public GameObject prefabBasemap;
    public Sprite defaultImage;
    public Transform content;

    public string mapFile;
    public string mapFolder;
    public static BasemapControllerWGS instance;

    private async void Start()
    {
        //mapFolder = Path.Combine(Directory.GetCurrentDirectory(), mapFolder);
        //mapFile = Path.Combine(mapFolder, mapFile);
        //await LoadMapServiceFromFileAsync();

        //OnlineMapsProvider.Create("mywms").AppendTypes(
        //    new OnlineMapsProvider.MapType("petadarat") { urlWithLabels = url_darat },
        //    new OnlineMapsProvider.MapType("petalaut") { urlWithLabels = url_laut },
        //    new OnlineMapsProvider.MapType("petaudara") { urlWithLabels = url_udara },
        //    new OnlineMapsProvider.MapType("peta_satellite") { urlWithLabels = url_satellite }
        //);


        //OnlineMaps.instance.mapType = "mywms.peta_satellite";
        if (instance != null)
        {
            return;
        }
        instance = this;
    }

    IEnumerator getTexture(Image tex, string url)
    {
        Debug.Log("Downloading image from " + url);
        using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(url))
        {
            yield return uwr.SendWebRequest();

            if (uwr.result != UnityWebRequest.Result.Success)
            {
                Debug.Log("Downloading failed! " + uwr.error);
            }
            else
            {
                // Get downloaded asset bundle
                Debug.Log("Downloading done!");
                Texture2D res = DownloadHandlerTexture.GetContent(uwr);
                tex.GetComponent<Image>().sprite = Sprite.Create(res, new Rect(0, 0, res.width, res.height), Vector2.zero);
            }
        }
    }

    public async Task LoadMapServiceFromFileAsync()
    {
        List<MapService> mapService = new List<MapService>();
        //var defaultBB = "95.0%2C-11.0%2C140.0%2C9.0";

        //if(mapFile == null && mapFolder == null)
        //{
            string res = await API.Instance.GetListBasemap();
            if (res.Length < 1) return;
            //Debug.Log("JSON Basemap : " + res);
            JArray li = JArray.Parse(res);

            foreach (JObject i in li)
            {
                if (i.GetValue("jenis").ToString() == "Geoserver")
                {
                    Debug.Log("Basemap : " + i.GetValue("nama").ToString());
                    Debug.Log("URL Basemap : " + i.GetValue("url").ToString() + "?service=WMS&version=1.1.0&request=GetMap&layers=" + i.GetValue("layer_name").ToString() + "&bbox={lx},{by},{rx},{ty}&width=256&height=256&srs=EPSG%3A4326&styles=&format=application%2Fopenlayers");
                    MapService data = new MapService()
                    {
                        provider = "mywms",
                        label = i.GetValue("nama").ToString(),
                        map_name = i.GetValue("layer_name").ToString(),
                        images = null,
                        url = i.GetValue("url").ToString() + "?service=WMS&version=1.1.0&request=GetMap&layers=" + i.GetValue("layer_name").ToString() + "&bbox={lx},{by},{rx},{ty}&width=64&height=64&srs=EPSG%3A4326&styles=&format=application%2Fopenlayers",
                        is_custom = true,
                        is_default = false
                    };
                    
                    if (data.map_name == "" || data.map_name == null || data.provider == "" || data.provider == null) continue;

                    var listProvider = OnlineMapsProvider.GetProvidersTitle();
                    OnlineMapsProvider[] _provider = OnlineMapsProvider.GetProviders();
                    OnlineMapsProvider _activeProvider;

                    int index = Array.IndexOf(listProvider, data.provider);

                    if (index == -1)
                    {
                        _activeProvider = OnlineMapsProvider.Create("mywms");
                    }
                    else
                    {
                        _activeProvider = _provider[index];
                    }

                    if (data.is_custom)
                    {
                        if (data.variant_with_label != null || data.variant_without_label != null)
                        {
                            if (data.variant_with_label != null)
                            {
                                _activeProvider.AppendTypes(
                                    new OnlineMapsProvider.MapType(data.map_name) { variantWithLabels = data.variant_with_label }
                                );
                            }

                            if (data.variant_without_label != null)
                            {
                                _activeProvider.AppendTypes(
                                    new OnlineMapsProvider.MapType(data.map_name) { variantWithoutLabels = data.variant_without_label }
                                );
                            }
                        }
                        else
                        {
                            _activeProvider.AppendTypes(
                                new OnlineMapsProvider.MapType(data.map_name) { urlWithLabels = data.url }
                            );
                        }
                    }


                    var item = Instantiate(prefabBasemap, content);
                    item.name = "Basemap_" + data.map_name;

                    var map_label = item.GetComponent<Metadata>().FindParameter("label").parameter.GetComponent<TextMeshProUGUI>().text = data.label;
                    var map_image = item.GetComponent<Metadata>().FindParameter("image").parameter.GetComponent<Image>();
                    item.GetComponent<Button>().onClick.AddListener(delegate { setBaseMap(data.provider + "." + data.map_name); });

                //map_image.sprite = getImage(data.images);

                StartCoroutine(getTexture(map_image, i.GetValue("url").ToString() + "?service=WMS&version=1.1.0&request=GetMap&layers=" + i.GetValue("layer_name").ToString() + "&bbox=95.0%2C-11.0%2C140.0%2C9.0&width=256&height=256&srs=EPSG%3A4326&styles=&format=image%2Fpng"));

                // Set Default Selected Map
                if (data.is_default) OnlineMaps.instance.mapType = data.provider + "." + data.map_name;
                }
            }

            //foreach ((MapService data, int i) in mapService.Select((value, i) => (value, i)))
            //{
            //    if (data.map_name == "" || data.map_name == null || data.provider == "" || data.provider == null) continue;

            //    var listProvider = OnlineMapsProvider.GetProvidersTitle();
            //    OnlineMapsProvider[] _provider = OnlineMapsProvider.GetProviders();
            //    OnlineMapsProvider _activeProvider;

            //    int index = Array.IndexOf(listProvider, data.provider);

            //    if (index == -1)
            //    {
            //        _activeProvider = OnlineMapsProvider.Create("mywms");
            //    }
            //    else
            //    {
            //        _activeProvider = _provider[index];
            //    }

            //    if (data.is_custom)
            //    {
            //        if (data.variant_with_label != null || data.variant_without_label != null)
            //        {
            //            if (data.variant_with_label != null)
            //            {
            //                _activeProvider.AppendTypes(
            //                    new OnlineMapsProvider.MapType(data.map_name) { variantWithLabels = data.variant_with_label }
            //                );
            //            }

            //            if (data.variant_without_label != null)
            //            {
            //                _activeProvider.AppendTypes(
            //                    new OnlineMapsProvider.MapType(data.map_name) { variantWithoutLabels = data.variant_without_label }
            //                );
            //            }
            //        }
            //        else
            //        {
            //            _activeProvider.AppendTypes(
            //                new OnlineMapsProvider.MapType(data.map_name) { urlWithLabels = data.url }
            //            );
            //        }
            //    }


            //    var item = Instantiate(prefabBasemap, content);
            //    item.name = "Basemap_" + data.map_name;

            //    var map_label = item.GetComponent<Metadata>().FindParameter("label").parameter.GetComponent<TextMeshProUGUI>().text = data.label;
            //    var map_image = item.GetComponent<Metadata>().FindParameter("image").parameter.GetComponent<Image>();
            //    item.GetComponent<Button>().onClick.AddListener(delegate { setBaseMap(data.provider + "." + data.map_name); });

            //    map_image.sprite = getImage(data.images);

            //    // Set Default Selected Map
            //    if (data.is_default) OnlineMaps.instance.mapType = data.provider + "." + data.map_name;
            //}
        //}

        //if (mapFile == null) {
        //    Debug.Log("Filename undefined"); return;
        //}

        //if (File.Exists(mapFile))
        //{
        //    using (StreamReader r = new StreamReader(mapFile))
        //    {
        //        string JSON = r.ReadToEnd();
        //        mapService = JsonConvert.DeserializeObject<List<MapService>>(JSON);

        //        foreach ((MapService data, int i) in mapService.Select((value, i) => (value, i)))
        //        {
        //            if (data.map_name == "" || data.map_name == null || data.provider == "" || data.provider == null) continue;

        //            var listProvider = OnlineMapsProvider.GetProvidersTitle();
        //            OnlineMapsProvider[] _provider = OnlineMapsProvider.GetProviders();
        //            OnlineMapsProvider _activeProvider;

        //            int index = Array.IndexOf(listProvider, data.provider);

        //            if (index == -1)
        //            {
        //                _activeProvider = OnlineMapsProvider.Create("mywms");
        //            }
        //            else
        //            {
        //                _activeProvider = _provider[index];
        //            }

        //            if (data.is_custom)
        //            {
        //                if(data.variant_with_label != null || data.variant_without_label != null)
        //                {
        //                    if (data.variant_with_label != null)
        //                    {
        //                        _activeProvider.AppendTypes(
        //                            new OnlineMapsProvider.MapType(data.map_name) { variantWithLabels = data.variant_with_label }
        //                        );
        //                    }
                            
        //                    if(data.variant_without_label != null)
        //                    {
        //                        _activeProvider.AppendTypes(
        //                            new OnlineMapsProvider.MapType(data.map_name) { variantWithoutLabels = data.variant_without_label }
        //                        );
        //                    }
        //                }
        //                else
        //                {
        //                    _activeProvider.AppendTypes(
        //                        new OnlineMapsProvider.MapType(data.map_name) { urlWithLabels = data.url }
        //                    );
        //                }
        //            }
                    

        //            var item = Instantiate(prefabBasemap, content);
        //            item.name = "Basemap_" + data.map_name;

        //            var map_label = item.GetComponent<Metadata>().FindParameter("label").parameter.GetComponent<TextMeshProUGUI>().text = data.label;
        //            var map_image = item.GetComponent<Metadata>().FindParameter("image").parameter.GetComponent<Image>();
        //            item.GetComponent<Button>().onClick.AddListener(delegate { setBaseMap(data.provider + "." + data.map_name); });

        //            map_image.sprite = getImage(data.images);

        //            // Set Default Selected Map
        //            if (data.is_default) OnlineMaps.instance.mapType = data.provider + "." + data.map_name;
        //        }
        //    }
        //}
        //else
        //{
        //    if (!Directory.Exists(mapFolder)) Directory.CreateDirectory(mapFolder);

        //    string res = await API.Instance.GetListBasemap();
        //    if (res.Length < 1) return;
        //    //Debug.Log("JSON Basemap : " + res);
        //    JArray li = JArray.Parse(res);
        //    //http://192.168.27.8:8080/geoserver/Openstreet_data/wms?service=WMS&version=1.1.0&request=GetMap&layers=Openstreet_data%3Aosm_basemap&bbox={lx},{by},{rx},{ty}&width=256&height=256&srs=EPSG%3A4326&styles=&format=image%2Fpng
        //    foreach (JObject i in li)
        //    {
        //        if (i.GetValue("jenis").ToString() == "Geoserver")
        //        {
        //            Debug.Log("Basemap : " + i.GetValue("nama").ToString());
        //            Debug.Log("URL Basemap : " + i.GetValue("url").ToString() + "?service=WMS&version=1.1.0&request=GetMap&layers=" + i.GetValue("layer_name").ToString() + "&bbox={lx},{by},{rx},{ty}&width=256&height=256&srs=EPSG%3A4326&styles=&format=image%2Fpng");
        //            mapService.Add(new MapService()
        //            {
        //                provider = "mywms",
        //                label = i.GetValue("nama").ToString(),
        //                map_name = i.GetValue("layer_name").ToString(),
        //                images = "imagery.jpg",
        //                url = i.GetValue("url").ToString() + "?service=WMS&version=1.1.0&request=GetMap&layers=" + i.GetValue("layer_name").ToString() + "&bbox={lx},{by},{rx},{ty}&width=256&height=256&srs=EPSG%3A4326&styles=&format=image%2Fpng",
        //                is_custom = true,
        //                is_default = false
        //            });
        //        }
        //    }

        //    string JSON = JsonConvert.SerializeObject(mapService);
        //    File.WriteAllText(mapFile, JSON);

        //    LoadMapServiceFromFileAsync();
        //}
    }

    private Sprite getImage(string image)
    {
        if (image == null) return defaultImage;
        byte[] fileData;

        fileData = File.ReadAllBytes(Path.Combine(mapFolder, image));
        if (fileData == null) return defaultImage;

        Texture2D tex = new Texture2D(2, 2);
        tex.LoadImage(fileData);

        Rect rec = new Rect(0, 0, tex.width, tex.height);
        Sprite spriteToUse = Sprite.Create(tex, rec, new Vector2(0.5f, 0.5f), 100);

        return spriteToUse;
    }

    public void setBaseMap(string name)
    {
        OnlineMaps.instance.mapType = name;

        if (OnlineMaps.instance.mapType.Contains("mywms"))
        {
            OnlineMapsTile.OnReplaceURLToken += OnReplaceUrlToken;
        }
        else
        {
            OnlineMapsTile.OnReplaceURLToken = null;
        }
    }

    private string OnReplaceUrlToken(OnlineMapsTile tile, string token)
    {
        if (token == "lx") return tile.topLeft.x.ToString(OnlineMapsUtils.numberFormat);
        if (token == "by") return tile.bottomRight.y.ToString(OnlineMapsUtils.numberFormat);   
        if (token == "rx") return tile.bottomRight.x.ToString(OnlineMapsUtils.numberFormat);
        if (token == "ty") return tile.topLeft.y.ToString(OnlineMapsUtils.numberFormat);

        // Otherwise, return the token
        return token;
    }
}

public class MapServiceWGS
{
    public string provider;
    public string label;
    public string map_name;
    public string images;
    public string url;
    public string variant_with_label;
    public string variant_without_label;
    public bool is_custom;
    public bool is_default;
}