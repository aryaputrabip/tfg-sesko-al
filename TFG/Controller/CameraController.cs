using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CameraController : MonoBehaviour
{
    #region PUBLIC VARIABLES
    [Header("Default Cam")]
    public Vector2 defaultPos;
    public Vector2 defaultRot;
    public float defaultZoom;

    [Header("Reset Cam")]
    public float resetCamDuration = 3f;
    public AnimationCurve animationCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);

    [Header("3D/RTS Cam")]
    public float toggle3DDuration = 2f;
    public Vector2 default3DRot;
    public float default3DZoom;

    [Header("Screen Edge")]
    public float speed = 1;
    public bool isActive = false;

    #endregion

    #region PRIVATE VARIABLES
    // Ketika Tombol Ditekan (hold)
    private bool isOnHold = false;
    private string actionOnHold = "";

    private bool isReset = false;
    private float resetTime;

    private bool is2DCam = false;
    private bool is3DCam = false;

    private float camTilt;
    private float maxCamTilt;
    private bool zoomChange = false;
    #endregion

    private void Start()
    {
        // Set Default Camera Rot, Pos & Zoom
        double posX, posY;
        OnlineMaps.instance.GetPosition(out posX, out posY);
        OnlineMaps.instance.zoomRange = new OnlineMapsRange(5.2f, 20);

        defaultPos = new Vector2((float)posX, (float)posY);
        defaultRot = OnlineMaps.instance.GetComponent<OnlineMapsCameraOrbit>().rotation;
        defaultZoom = OnlineMaps.instance.floatZoom;

        camTilt = OnlineMaps.instance.GetComponent<OnlineMapsCameraOrbit>().rotation.x;
    }

    public void toggleHold(string action)
    {
        isOnHold = isOnHold ? false : true;
        actionOnHold = action;
    }

    public void resetCam()
    {
        if(!isReset) isReset = true;
    }

    public void toggle2DCam()
    {
        resetTime = 0;
        is3DCam = false;
        if (!is2DCam) is2DCam = true;
    }

    public void toggle3DCam()
    {
        resetTime = 0;
        is2DCam = false;
        if (!is3DCam) is3DCam = true;
    }

    public void toggleDrag()
    {
        OnlineMaps.instance.GetComponent<OnlineMapsTileSetControl>().allowUserControl = OnlineMaps.instance.GetComponent<OnlineMapsTileSetControl>().allowUserControl ? false : true;
    }

    public void toggleTilt()
    {
        speed = OnlineMaps.instance.zoom;

        //OnlineMaps.instance.GetComponent<OnlineMapsCameraOrbit>().lockTilt = OnlineMaps.instance.GetComponent<OnlineMapsCameraOrbit>().lockTilt ? false : true;
        OnlineMaps.instance.GetComponent<OnlineMapsCameraOrbit>().lockPan = OnlineMaps.instance.GetComponent<OnlineMapsCameraOrbit>().lockPan ? false : true;
    }

    public void geserLeft()
    {
        speed = OnlineMaps.instance.zoom + (20- OnlineMaps.instance.zoom);
        OnlineMaps.instance.position = new Vector2(OnlineMaps.instance.position.x - (4f / (OnlineMaps.instance.floatZoom * OnlineMaps.instance.floatZoom  * OnlineMaps.instance.floatZoom ) * Time.deltaTime * speed), OnlineMaps.instance.position.y);
    }

    public void geserRight()
    {
        speed = OnlineMaps.instance.zoom + (20 - OnlineMaps.instance.zoom);
        OnlineMaps.instance.position = new Vector2(OnlineMaps.instance.position.x + (4f / (OnlineMaps.instance.floatZoom * OnlineMaps.instance.floatZoom  * OnlineMaps.instance.floatZoom ) * Time.deltaTime * speed), OnlineMaps.instance.position.y);
    }

    public void geserUp()
    {
        speed =  OnlineMaps.instance.zoom + (20 - OnlineMaps.instance.zoom);
        OnlineMaps.instance.position = new Vector2(OnlineMaps.instance.position.x, OnlineMaps.instance.position.y - (4f / (OnlineMaps.instance.floatZoom * OnlineMaps.instance.floatZoom  * OnlineMaps.instance.floatZoom ) * Time.deltaTime * speed));
    }

    public void geserDown()
    {
        speed = OnlineMaps.instance.zoom + (20 - OnlineMaps.instance.zoom);
        OnlineMaps.instance.position = new Vector2(OnlineMaps.instance.position.x, OnlineMaps.instance.position.y + (4f / (OnlineMaps.instance.floatZoom * OnlineMaps.instance.floatZoom  * OnlineMaps.instance.floatZoom ) * Time.deltaTime * speed));
    }

    public void setActiveEdge(bool e)
    {
        isActive = e;
    }

    private void Update()
    {
        if (isOnHold)
        {
            if (actionOnHold == "zoom-in")
            {
                OnlineMaps.instance.floatZoom += 1 * Time.deltaTime * speed;
            }else if(actionOnHold == "zoom-out")
            {
                OnlineMaps.instance.floatZoom -= 1 * Time.deltaTime * speed;
            }
        }

        // Reset Cam Object
        #region RESET_CAM
        if (isReset)
        {
            // Updating the progress of the animation
            resetTime += Time.deltaTime * speed;
            float t = resetTime / resetCamDuration;

            // If the animation is finished
            if (t >= 1)
            {
                // Reset values
                resetTime = 0;
                isReset = false;
                t = 1;
            }

            // Update the camera
            var OM = OnlineMaps.instance;
            float f = animationCurve.Evaluate(t);

            OM.SetPosition(Mathf.Lerp(OM.position.x, defaultPos.x, f), Mathf.Lerp(OnlineMaps.instance.position.y, defaultPos.y, f));
            OM.GetComponent<OnlineMapsCameraOrbit>().rotation.x = Mathf.LerpAngle(OM.GetComponent<OnlineMapsCameraOrbit>().rotation.x, 0, f*3);
            OM.GetComponent<OnlineMapsCameraOrbit>().rotation.y = Mathf.Lerp(OM.GetComponent<OnlineMapsCameraOrbit>().rotation.y, 0, f*3);
            OM.floatZoom = Mathf.Lerp(OnlineMaps.instance.floatZoom, defaultZoom, f);
        }
        #endregion

        // Toggle 3D/RTS Cam
        #region RTS_CAM
        if (is3DCam)
        {
            // Updating the progress of the animation
            resetTime += Time.deltaTime * speed;
            float t = resetTime / toggle3DDuration;

            // If the animation is finished
            if (t >= 1)
            {
                // Reset values
                resetTime = 0;
                is3DCam = false;
                t = 1;
            }

            // Update the camera
            var OM = OnlineMaps.instance;
            float f = animationCurve.Evaluate(t);

            OM.GetComponent<OnlineMapsCameraOrbit>().rotation.x = Mathf.LerpAngle(OM.GetComponent<OnlineMapsCameraOrbit>().rotation.x, default3DRot.x, f * 3);
            OM.GetComponent<OnlineMapsCameraOrbit>().rotation.y = Mathf.Lerp(OM.GetComponent<OnlineMapsCameraOrbit>().rotation.y, default3DRot.y, f * 3);
            OM.floatZoom = Mathf.Lerp(OnlineMaps.instance.floatZoom, default3DZoom, f);
        }


        // Back To 2D Cam From 3D
        if (is2DCam)
        {
            // Updating the progress of the animation
            resetTime += Time.deltaTime * speed;
            float t = resetTime / toggle3DDuration;

            // If the animation is finished
            if (t >= 1)
            {
                // Reset values
                resetTime = 0;
                is2DCam = false;
                t = 1;
            }

            // Update the camera
            var OM = OnlineMaps.instance;
            float f = animationCurve.Evaluate(t);

            OM.GetComponent<OnlineMapsCameraOrbit>().rotation.x = Mathf.LerpAngle(OM.GetComponent<OnlineMapsCameraOrbit>().rotation.x, 0, f * 3);
            OM.GetComponent<OnlineMapsCameraOrbit>().rotation.y = Mathf.Lerp(OM.GetComponent<OnlineMapsCameraOrbit>().rotation.y, 0, f * 3);
            OM.floatZoom = Mathf.Lerp(OnlineMaps.instance.floatZoom, 14, f);
        }
        #endregion

        #region 3D_CAM_ADJUSTER
        if (zoomChange)
        {
            var CAM_ORBIT = OnlineMaps.instance.GetComponent<OnlineMapsCameraOrbit>();
            CAM_ORBIT.rotation = Vector2.Lerp(CAM_ORBIT.rotation, new Vector2(camTilt, CAM_ORBIT.rotation.y), Time.deltaTime * speed * 5);

            if (OnlineMapsCameraOrbit.instance.rotation.x != camTilt)
            {
                OnlineMapsCameraOrbit.instance.maxRotationX = maxCamTilt;
                zoomChange = false;
            }
        }
        #endregion

        // Screen Edge
        if (isActive)
        {
            if (Input.mousePosition.x <= Screen.width * 0.0001)
            {
                geserLeft();
            }
            if (Input.mousePosition.x >= Screen.width * 0.999)
            {
                geserRight();
            }
            if (Input.mousePosition.y <= Screen.height * 0.0001)
            {
                geserUp();
            }
            if (Input.mousePosition.y >= Screen.height * 0.999)
            {
                geserDown();
            }
        }
    }

    #region 3D_CAM_ADJUSTER
    // Adjust Camera Maximum Rotation Based On Zoom
    private void OnMapsZoom()
    {
        var currentZoom = OnlineMaps.instance.floatZoom;
        //Debug.Log(OnlineMaps.instance.floatZoom);
        if(currentZoom > 3 && currentZoom <= 6)
        {
            setCamTilt(10, 10);
        }else if(currentZoom > 6 && currentZoom <= 7)
        {
            setCamTilt(15, 15);
        }
        else if(currentZoom > 7 && currentZoom <= 8)
        {
            setCamTilt(20, 20);
        }else if(currentZoom > 8 && currentZoom <= 9)
        {
            setCamTilt(25, 25);
        }else if(currentZoom > 9 && currentZoom <= 10)
        {
            setCamTilt(30, 30);
        }else if(currentZoom > 10 && currentZoom <= 11)
        {
            setCamTilt(35, 35);
        }else if(currentZoom > 11 && currentZoom <= 12)
        {
            setCamTilt(45, 45);
        }else if(currentZoom > 12)
        {
            setCamTilt(60, 80);
        }
        else
        {
            setCamTilt(0, 0);
        }

        zoomChange = true;
    }

    private void setCamTilt(float tilt, float max_tilt)
    {
        camTilt = tilt;
        maxCamTilt = max_tilt;
    }
    #endregion
}
