using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;

using Colyseus;
using Colyseus.Schema;
using GlobalVariables;
using System.Linq;

using HelperPlotting;
using System.Globalization;

public class ColyseusController : MonoBehaviour
{
    #region Variables
    [Header("Config")]
    public string roomName = "tfg";

    [Header("Properties")]
    public List<players> players = new List<players>();

    private GameController _controller;
    private TimetableController _timetable;
    private ColyseusClient _client;
    private ColyseusRoom<StateTFG> _room;

    private NetworkRequest network;
    #endregion

    public static ColyseusController instance;
    void Awake()
    {
        // Set Asset Package Manager Menjadi 1 Origin
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        _controller = GetComponent<GameController>();
        _timetable = FindObjectOfType<TimetableController>();
        if (_controller == null) _controller = FindObjectOfType<GameController>();
    }

    public async Task ClientPrepare()
    {
        _client = new ColyseusClient(Config.SERVER_COLYSEUS);
        network = new NetworkRequest();

        var roomConfig = setRoomConfig();
        try
        {
            if (SessionUser.id == null)
            {
                throw new ArgumentNullException(paramName: nameof(SessionUser.id), message: "User is not found!");
            }
            else
            {
                if (SessionUser.id == 1)
                {
                    // Login Sebagai : WASDAL
                    _room = await _client.JoinOrCreate<StateTFG>(roomName, roomConfig);
                }
                else
                {
                    // Login Sebagai : KOGAS
                    _room = await _client.Join<StateTFG>(roomName, roomConfig);
                }

                RegisterRoomHandlers();
                RegisterRoomMessage();
            }
        } catch (Exception e)
        {
            Debug.Log(e.Message);
            _controller.CloseStartOverlayMenu(true);
        }
    }

    private void RegisterRoomHandlers()
    {
        //_room.OnJoin += OnJoin;
        //_room.OnLeave += OnLeave;
        _room.OnError += (code, message) => Debug.LogError("ERROR, code =>" + code + ", message => " + message);

        if(_controller.GameMode == GameController.gameplayMode.MP_SPECTATOR)
        {
            _room.State.entities.OnAdd += OnAddEntities;
            _room.State.entities.OnRemove += OnRemoveEntities;
        }

        _room.State.waktu.OnAdd += OnSyncWaktu;
        _room.State.percepatan.OnAdd += OnSyncPercepatan;
        _room.State.players.OnAdd += OnPlayerJoin;
        _room.State.players.OnRemove += OnPlayerLeave;
    }

    void RegisterRoomMessage()
    {
        var roomMessageManager = new RoomMessage();
        roomMessageManager.registerRoom(_room, PlaybackController.Instance);
    }

    /// <summary>
    /// Jalankan Sesuatu Jika Terdapat User JOIN
    /// </summary>
    /// <param name="key">ID User</param>
    /// <param name="playerTFG">Data User</param>
    private async void OnPlayerJoin(string key, players playerTFG)
    {
        Debug.Log("On Player Join");
        players.Add(playerTFG);
        
        if(SessionUser.id == 1)
        {
            WASDALJoinRoom(playerTFG);

            // Hanya Wasdal yang dapat menerima notifikasi user JOIN ke room
            Debug.Log(playerTFG.nama + "(" + key + ")" + " join the room");
            if (key != "" && key != "1" && key != "server")
            {
                // Jika ada user join, hanya KOGAS yang akan di load data CB-nya
                // (dalam kondisi jika login sebagai WASDAL)
                await API.Instance.GetCBTerbaik(playerTFG.bagian);
                await _controller._entity.LoadEntityFromCB(false, key);
                UserActiveController.Instance.addToList(playerTFG.nama);
                //await _controller._entity.LoadEntityFromCB((playerTFG.asisten == "ASOPS") ? false : true, key);
            }
        }
    }

    /// <summary>
    /// Jalankan Sesuatu Jika Terdapat User LEAVE
    /// </summary>
    /// <param name="key">ID User</param>
    /// <param name="playerTFG">Data User</param>
    private async void OnPlayerLeave(string key, players playerTFG)
    {
        if (SessionUser.id == 1)
        {
            // Hanya Wasdal yang dapat menerima notifikasi user LEAVE dari room
            Debug.Log(playerTFG.nama + " leave the room");

            if(key != "1" && key != "server")
            {
                await _controller._entity.RemoveEntityFromCB(key);
                UserActiveController.Instance.removeFromList(playerTFG.nama);
            }
        }
    }

    /// <summary>
    /// Ketika WASDAL Join Ke Room
    /// - Set Default Camera Sart
    /// - Set Mode Control Server
    /// </summary>
    public async void WASDALJoinRoom(players playerWasdal)
    {
        // Tidak Dijalankan jika sudah ada user WASDAL di server
        Debug.Log(playerWasdal);

        // Init Default Camera Start
        await _room.Send("init_cam", new Dictionary<string, object>
        {
            ["lat"]     = -1f,
            ["lng"]     = 118f,
            ["zoom"]    = 5f,
        });

        // Init Mode Control Server
        await _room.Send("setModeControl", "sync");
    }

    Dictionary<string, object> entityCam = new Dictionary<string, object>
    {
        ["lat"]     = 0,
        ["lng"]     = 0,
        ["zoom"]    = 0
    };

    public async void SyncCameraTo2D(double lat, double lng, float zoom)
    {
        if (_room == null) return;

        entityCam["lat"]    = (float)lat;
        entityCam["lng"]    = (float)lng;
        entityCam["zoom"]   = zoom;

        await _room.Send("move_cam", entityCam);
    }

    /// <summary>
    /// Jalankan Sesuatu Jika Terdapat Entity Baru Masuk
    /// </summary>
    /// <param name="key">ID KEY</param>
    /// <param name="entity">EntityTFG (data)</param>
    private void OnAddEntities(string key, EntityTFG entity)
    {
        if (key != _room.SessionId)
        {
            switch (entity.type)
            {
                case "satuan":
                    // -- Terdapat Entity Satuan Baru Masuk --
                    // Spawn Entity Satuan Baru ke Dalam Peta
                    //var isSuccess = await _controller._entity.LoadEntityFromColyseus(entity);
                    //if(isSuccess) entity.OnChange += (List<Colyseus.Schema.DataChange> changes) => OnEntityChange(changes, key);
                    break;
            }
        }
    }

    private void OnRemoveEntities(string key, EntityTFG entity)
    {
        if (key != _room.SessionId)
        {
            //Lookup for entity, if found, continue.
            if (SessionUser.id == 1 || entity.id_entity == SessionUser.id.ToString())
            {
                Entities metadata = _controller._entity.OBJ_ENTITY.FirstOrDefault(x => x.id_entity == key);
                if (metadata == null) return;

                Destroy(metadata.gameObject);
                //_controller._entity.ListAlutsista.removeAlutsistaFromList(entity.id_entity);
                _controller._entity.OBJ_ENTITY.Remove(metadata);
            }
        }
    }

    private void OnEntityChange(List<Colyseus.Schema.DataChange> changes, string key)
    {
        Entities metadata = _controller._entity.OBJ_ENTITY.FirstOrDefault(x => x.id_entity == key);
        if (metadata == null) return;

        EntityWalker walker = metadata.gameObject.GetComponent<EntityWalker>();
        int index = _controller._entity.OBJ_ENTITY.FindIndex(a => a == metadata);

        float lat = walker.coordinate.y;
        float lng = walker.coordinate.x;
        float heading = metadata.heading;

        bool isMoving = false;
        bool isActive = true;

        for (int i = 0; i < changes.Count; i++)
        {
            switch (changes[i].Field)
            {
                case "lat":
                    lat = (float)changes[i].Value;
                    isMoving = true;
                    break;
                case "lng":
                    lng = (float)changes[i].Value;
                    isMoving = true;
                    break;
                case "heading":
                    heading = (float)changes[i].Value;
                    metadata.heading = heading;

                    walker.marker.rotation = Quaternion.Euler(0, metadata.heading + 180, 0);
                    break;
                case "opacity":
                    if (changes[i].Value.ToString() == "remove")
                    {
                        isActive = false;
                    }
                    break;
                default: break;
            }
        }

        if (isMoving)
        {
            walker.coordinate = new Vector2(lng, lat);
            walker.marker.SetPosition(lng, lat);
            _controller._entity.OBJ_ENTITY[index] = metadata;
        }

        if (_controller._entity.OBJ_ENTITY[index].gameObject.activeSelf && !isActive)
        {
            _controller._entity.OBJ_ENTITY[index].opacity = Entities.opacityType.REMOVE;
            _controller._entity.OBJ_ENTITY[index].gameObject.SetActive(false);
        }
        else if (!_controller._entity.OBJ_ENTITY[index].gameObject.activeSelf && isActive)
        {
            _controller._entity.OBJ_ENTITY[index].opacity = Entities.opacityType.ADD;
            _controller._entity.OBJ_ENTITY[index].gameObject.SetActive(true);
        }
    }

    private void OnSyncWaktu(string key, WaktuTFG waktu)
    {
        waktu.OnChange += (List<Colyseus.Schema.DataChange> changes) => OnTimeChange(changes, key);
    }

    private void OnTimeChange(List<DataChange> changes, string key)
    {
        for (int i = 0; i < changes.Count; i++)
        {
            switch (changes[i].Field)
            {
                case "harih":
                    //int index = -1;
                    //if (changes[i].Value.ToString().Contains("H-"))
                    //{
                    //    index = _timetable.hariH.IndexOf(int.Parse(changes[i].Value.ToString().Split("H")[1]));
                    //}
                    //else if (changes[i].Value.ToString().Contains("H+"))
                    //{
                    //    index = _timetable.hariH.IndexOf(int.Parse(changes[i].Value.ToString().Split("H+")[1]));
                    //}
                    //else
                    //{
                    //    index = _timetable.hariH.IndexOf(0);
                    //}

                    //if (index != -1)
                    //{
                    //    if (!PlaybackController.Instance.sliderDragged)
                    //    {
                    //        PlaybackController.Instance._TIME = _timetable.hariHDate[index];
                    //        PlaybackController.Instance.refreshTimeUI();
                    //    }
                    //}
                    break;
                case "waktu":
                    //string new_waktu = PlaybackController.Instance._TIME.Date.ToShortDateString() + " " + changes[i].Value.ToString();

                    //if (PlaybackController.Instance.sliderDragged)
                    //{
                    //    PlaybackController.Instance._TIME = DateTime.Parse(new_waktu);
                    //    PlaybackController.Instance.playbackSlider.value = PlaybackController.Instance._TIME.Ticks;
                    //    PlaybackController.Instance.refreshTimeUI();
                    //}
                    break;
                case "timeString":
                    var datechange = DateTimeOffset.FromUnixTimeMilliseconds(Convert.ToInt64(Math.Floor(Convert.ToDouble(changes[i].Value.ToString())))).LocalDateTime;

                    PlaybackController.Instance._TIME = datechange;
                    PlaybackController.Instance.playbackSlider.value = PlaybackController.Instance._TIME.Ticks;
                    //PlaybackController.Instance.timeOfDaySlider.value = (PlaybackController.Instance.isTimeCycle) ? (float)PlaybackController.Instance._TIME.TimeOfDay.TotalSeconds : 43200;
                    if (PlaybackController.Instance.isTimeCycle) EnviroSkyMgr.instance.SetTime(PlaybackController.Instance._TIME);
                    PlaybackController.Instance.refreshTimeUI();
                    break;
                case "media":
                    switch (changes[i].Value.ToString())
                    {
                        case "start":
                            PlaybackController.Instance.state = PlaybackController.playbackState.PLAYING;
                            PlaybackController.Instance.refreshPlaybackUI();
                            break;
                        case "pause":
                            PlaybackController.Instance.state = PlaybackController.playbackState.PAUSED;
                            PlaybackController.Instance.refreshPlaybackUI();
                            break;
                        case "stop":
                            PlaybackController.Instance.changeState("stop");
                            break;
                    }
                    break;
            }
        }
    }

    public async Task AddSatuanState(EntitySatuan entity, EntitySatuanInfo info, EntitySatuanStyle style)
    {
        await _room.Send("tambah_entity", new Dictionary<string, object>
        {
            ["id_object"] = entity.nama,
            ["type"] = "satuan",
            ["lat"] = entity.lat,
            ["lng"] = entity.lng,
            ["heading"] = info.heading,
            ["opacity"] = "add",
            ["path_object_3d"] = entity.path_object_3d,
            ["jenis"] = entity.jenis,
            ["tipe_tni"] = entity.tipe_tni,
            ["kode_ascii"] = style.index,
            ["name_family"] = style.nama,
            ["namaAlutsista"] = info.nama_satuan,
            ["object3D"] = entity.object3D,
            ["defaultData"] = EntitySatuan.ToString(entity),
            ["alutsista"] = entity.alutsista
        });
    }

    Dictionary<string, object> entityMovement = new Dictionary<string, object>
    {
        ["id_object"] = "",
        ["lat"] = 0,
        ["lng"] = 0,
        ["heading"] = 0,
        ["opacity"] = "add"
    };

    public async Task SyncEntityState(EntityWalker meta)
    {
        double currentLat, currentLng;
        meta.marker.GetPosition(out currentLng, out currentLat);

        entityMovement["id_object"] = meta.gameObject.name;
        entityMovement["lat"] = currentLat;
        entityMovement["lng"] = currentLng;
        entityMovement["heading"] = meta.marker.rotationY - 180;
        entityMovement["opacity"] = "add";

        await _room.Send("movement", entityMovement);
    }

    public async Task SyncPlaybackState(string state)
    {
        Dictionary<string, object> syncWaktu = new Dictionary<string, object>
        {
            ["media"] = state
        };

        await _room.Send("sync_waktu", syncWaktu);
    }

    public async Task SyncPlaybackTime(string hariH, string waktu, long time)
    {
        Dictionary<string, object> syncPlaybackTime = new Dictionary<string, object>
        {
            ["harih"] = hariH,
            ["waktu"] = waktu,
            ["time"] = time,
            ["timeString"] = time
        };

        await _room.Send("sync_waktu", syncPlaybackTime);
    }

    private void OnSyncPercepatan(string key, percepatan percepatan)
    {
        percepatan.OnChange += (List<Colyseus.Schema.DataChange> changes) => OnChangePercepatan(changes, key);
    }

    private void OnChangePercepatan(List<DataChange> changes, string key)
    {
        for (int i = 0; i < changes.Count; i++)
        {
            switch (changes[i].Field)
            {
                case "kecepatan":
                    PlaybackController.Instance.changePercepatan(int.Parse(changes[i].Value.ToString()));
                    break;
                case "changeDay":
                    /* */
                    break;
            }
        }
    }

    public async Task SyncPlaybackPercepatan(int percepatan)
    {
        await _room.Send("setPercepatan", percepatan.ToString());
    }

    public async Task JUMPDATETEST(string date)
    {
        await _room.Send("jumpDate", date);
    }

    /// <summary>
    /// -- Set Konfigurasi User Pada Room Colyseus --
    /// Sebagai "WASDAL" atau "KOGAS", parameter yang dikirimkan berbeda
    /// </summary>
    /// <returns></returns>
    private Dictionary<string, object> setRoomConfig()
    {
        if(SessionUser.id == 1 || SessionUser.name == "WASDAL")
        {
            // Return Room Config Untuk WASDAL
            return new Dictionary<string, object>
            {
                ["nama"]        = SessionUser.name.ToString(),
                ["id_user"]     = SessionUser.id.ToString(),
                ["jenisUser"]   = "wasdal",
            };
        }

        // Return Room Config Untuk KOGAS
        return new Dictionary<string, object>
        {
            ["nama"]        = (_controller.GameMode == GameController.gameplayMode.MP_SPECTATOR) ? SessionUser.name + " (3D)" : SessionUser.name,
            ["asisten"]     = SessionUser.nama_asisten,
            ["bagian"]      = SessionUser.id_bagian.ToString(),
            ["id_user"]     = SessionUser.id.ToString(),
            ["jenisUser"]   = "asisten",
        };
    }
}

#region RoomMessage
public class RoomMessage
{
    protected ColyseusRoom<StateTFG> room;
    protected PlaybackController playback;

    public void registerRoom(ColyseusRoom<StateTFG> registered, PlaybackController playbackController)
    {
        room = registered;
        playback = playbackController;
        initMessage();
    }

    void initMessage()
    {
        room.OnMessage<string>("setPercepatan", (speed) =>
        {
            Debug.Log("Set Percepata To : " + speed);
            playback.changePercepatan(int.Parse(speed));
        }); 

        room.OnMessage<JumpDateTime>("jumpDate", (date) =>
        {
            var datechange = DateTimeOffset.FromUnixTimeMilliseconds(date.changeTime).LocalDateTime;

            PlaybackController.Instance._TIME = datechange;
            PlaybackController.Instance.playbackSlider.value = PlaybackController.Instance._TIME.Ticks;
            //PlaybackController.Instance.timeOfDaySlider.value = (PlaybackController.Instance.isTimeCycle) ? (float)PlaybackController.Instance._TIME.TimeOfDay.TotalSeconds : 43200;
            if (PlaybackController.Instance.isTimeCycle) EnviroSkyMgr.instance.SetTime(PlaybackController.Instance._TIME);
            PlaybackController.Instance.refreshTimeUI();
        });
        room.OnMessage<string>("media", (media) =>
        {
            Debug.Log(media);
        });
        room.OnMessage<atkProp>("attack", (prop) =>
        {
            Debug.Log("Obj Attack" + prop.ToString());
        });

        room.OnMessage<string>("detek_radar", (radar) =>
        {
            Debug.Log("Radar On: " + radar.ToString());
        });
    }

    private class JumpDateTime
    {
        public long changeTime { get; set; }
    }

    public class atkProp
    {
        public string id_object { get; set; }
        public string latlng_object { get; set; }
        public string id_musuh { get; set; }
        public string latlng_musuh { get; set; }
        public string senjata { get; set; }
    }
}


#endregion