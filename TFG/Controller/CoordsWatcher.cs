using System;
using UnityEngine;

using TMPro;
using CoordinateSharp;

public class CoordsWatcher : MonoBehaviour
{
    [Header("References")]
    public GameObject default_group;
    public GameObject mgrs_group;
    public GameObject utm_group;

    public enum coordinateMode { DEFAULT, MGRS, UTM }
    public coordinateMode mode;

    private OnlineMaps OM;
    private EagerLoad el;

    // Start is called before the first frame update
    void Start()
    {
        OM = OnlineMaps.instance;
        OM.OnChangeZoom += OnMapsZoom;
        //OM.OnChangePosition += OnMapsMove;

        el = new EagerLoad(false);
    }

    public void ChangeCoordinateMode()
    {
        mode = (mode == coordinateMode.DEFAULT) ? coordinateMode.MGRS : (mode == coordinateMode.MGRS) ? coordinateMode.UTM : coordinateMode.DEFAULT;
        if(mode == coordinateMode.DEFAULT)
        {
            default_group.SetActive(true);
            mgrs_group.SetActive(false);
            utm_group.SetActive(false);
        }else if(mode == coordinateMode.MGRS)
        {
            default_group.SetActive(false);
            mgrs_group.SetActive(true);
            utm_group.SetActive(false);
        }
        else
        {
            default_group.SetActive(false);
            mgrs_group.SetActive(false);
            utm_group.SetActive(true);
        }

        OnMapsMove();
    }

    private void OnMapsMove()
    {
        // Sync Maps Lattitude & Longtitude
        //Coordinate coords = new Coordinate(OM.position.y, OM.position.x, DateTime.Now, el);
        double lng, lat;
        OnlineMapsControlBase.instance.GetCoords(out lng, out lat);

        Coordinate coords = new Coordinate(lat, lng, DateTime.Now, el);
        coords.LoadUTM_MGRS_Info();

        //if (mode == coordinateMode.DEFAULT)
        //{
        //    // On Default Mode
        //    foreach (var label in GameObject.FindGameObjectsWithTag("label-lattitude"))
        //    {
        //        label.GetComponent<TextMeshProUGUI>().text = coords.Latitude.ToString();
        //    }

        //    foreach (var label in GameObject.FindGameObjectsWithTag("label-longtitude"))
        //    {
        //        label.GetComponent<TextMeshProUGUI>().text = coords.Longitude.ToString();
        //    }
        //}else if(mode == coordinateMode.MGRS)
        //{
        //    // On MGRS Mode
        //    foreach (var label in GameObject.FindGameObjectsWithTag("label-mgrs"))
        //    {
        //        label.GetComponent<TextMeshProUGUI>().text = coords.MGRS.ToString();
        //    }
        //}
        //else if(mode == coordinateMode.UTM)
        //{
        //    // On UTM Mode
        //    foreach (var label in GameObject.FindGameObjectsWithTag("label-utm"))
        //    {
        //        label.GetComponent<TextMeshProUGUI>().text = coords.UTM.ToString();
        //    }
        //}

        //// Sync Camera to Colyseus (MULTIPLAYER and SPECTATOR ONLY)
        //if (GameController.instance.GameMode == GameController.gameplayMode.MULTIPLAYER) {
        //    // When on Game Mode "MULTIPALYER", Unity Send Camera Coordinate to 2D
        //    ColyseusController.instance.SyncCameraTo2D(lat, lng, OnlineMaps.instance.floatZoom);
        //}else if(GameController.instance.GameMode == GameController.gameplayMode.MP_SPECTATOR)
        //{
        //    // When on Game Mode "SPECTATOR", 2D Send Camera Coordinate to Unity
        //    // * */
        //}

        foreach (var label in GameObject.FindGameObjectsWithTag("label-lattitude"))
        {
            label.GetComponent<TextMeshProUGUI>().text = coords.Latitude.ToString();
        }

        foreach (var label in GameObject.FindGameObjectsWithTag("label-longtitude"))
        {
            label.GetComponent<TextMeshProUGUI>().text = coords.Longitude.ToString();
        }

        // On MGRS Mode
        foreach (var label in GameObject.FindGameObjectsWithTag("label-mgrs"))
        {
            label.GetComponent<TextMeshProUGUI>().text = coords.MGRS.ToString();
        }

        // On UTM Mode
        foreach (var label in GameObject.FindGameObjectsWithTag("label-utm"))
        {
            label.GetComponent<TextMeshProUGUI>().text = coords.UTM.ToString();
        }

    }

    private void OnMapsZoom()
    {
        foreach (var label in GameObject.FindGameObjectsWithTag("label-zoom"))
        {
            label.GetComponent<TextMeshProUGUI>().text = OM.floatZoom.ToString();
        }
    }

    private void LateUpdate()
    {
        OnMapsMove();
    }
}
