using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using TMPro;
using CoordinateSharp;
using System;

public class DetailSatuanController : MonoBehaviour
{
    [Header("Marker")]
    public OnlineMapsMarker3D marker;

    [Header("References")]
    public TextMeshProUGUI lat;
    public TextMeshProUGUI lng;
    public TextMeshProUGUI utm;
    public TextMeshProUGUI mgrs;

    public TextMeshProUGUI kecepatan;
    public TextMeshProUGUI heading;
    public TextMeshProUGUI task;

    public SidemenuToggler btnFollow;

    public Sprite defaultIcon;

    private EagerLoad el;

    private OnlineMapsMarker3D markerOnFollow;
    private int markerIndex = 0;
    private bool isFollowing = false;

    #region INSTANCE
    public static DetailSatuanController Instance;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    void Start()
    {
        el = new EagerLoad(false);
    }

    public void ShowDetailSatuan(OnlineMapsMarker3D marker, string id = "", string nama_satuan = "", Sprite icon = null, string index = "", Font fontTaktis = null, string tipe_tni = "", string tipe = "", string jenis = "", string warna = "", float ukuran = 1)
    {
        this.marker = marker;

        var metadata = GetComponent<Metadata>();
        //metadata.FindParameter("header").parameter.GetComponent<TextMeshProUGUI>().text = "INFO - " + nama_satuan;
        metadata.FindParameter("icon").parameter.GetComponent<Image>().sprite = icon != null ? icon : defaultIcon;
        metadata.FindParameter("nama_satuan").parameter.GetComponent<TextMeshProUGUI>().text = nama_satuan;
        metadata.FindParameter("id_satuan").parameter.GetComponent<TextMeshProUGUI>().text = id;
        metadata.FindParameter("font_taktis").parameter.GetComponent<Text>().text = index;
        metadata.FindParameter("font_taktis").parameter.GetComponent<Text>().font = fontTaktis;
        metadata.FindParameter("tipe_tni").parameter.GetComponent<TextMeshProUGUI>().text = tipe_tni;
        metadata.FindParameter("tipe").parameter.GetComponent<TextMeshProUGUI>().text = tipe;
        metadata.FindParameter("jenis").parameter.GetComponent<TextMeshProUGUI>().text = jenis;
        metadata.FindParameter("warna").parameter.GetComponent<TextMeshProUGUI>().text = warna;
        metadata.FindParameter("ukuran").parameter.GetComponent<TextMeshProUGUI>().text = (ukuran * 100).ToString() + " %";

        if (marker.instance.GetComponent<EntityWalker>().followMarker)
        {
            var btnIcon = btnFollow.transform.Find(btnFollow.iconName).GetComponent<Image>();
            btnIcon.color = btnFollow.activeMenu;
        }
        else
        {
            var btnIcon = btnFollow.transform.Find(btnFollow.iconName).GetComponent<Image>();
            btnIcon.color = btnFollow.disableMenu;
        }
    }

    public void Toggle3DView()
    {

    }

    public void ToggleIconView()
    {

    }

    public void LocateSatuan()
    {
        if (marker == null) return;
        ListEntityController.Instance.LocateEntity(marker);
    }

    public void ToggleFollowSatuan(bool state)
    {
        if (isFollowing)
        {
            // Set Unfollow Satuan Jika Object Sedang di-Follow
            isFollowing = false;
            markerOnFollow = null;

            OnlineMapsCameraOrbit.instance.adjustTo = OnlineMapsCameraAdjust.averageCenter;
            OnlineMapsCameraOrbit.instance.adjustToGameObject = null;

            if (marker != null)
            {
                markerOnFollow = marker;
                markerOnFollow.instance.GetComponent<EntityWalker>().followMarker = false;
                isFollowing = true;

                if (marker.instance.GetComponent<DetailSatuan>().tipeTNI == DetailSatuan.TNI.AU || marker.instance.GetComponent<DetailSatuan>().tipeKendaraan == DetailSatuan.type.Udara)
                {
                    OnlineMapsCameraOrbit.instance.adjustTo = OnlineMapsCameraAdjust.gameObject;
                    OnlineMapsCameraOrbit.instance.adjustToGameObject = marker.instance;
                }
                else
                {
                    OnlineMapsCameraOrbit.instance.adjustTo = OnlineMapsCameraAdjust.averageCenter;
                    OnlineMapsCameraOrbit.instance.adjustToGameObject = null;
                }
            };
        }
        else
        {
            if (marker == null) return;

            // Set Fllow Satuan Jika Object Sedang tidak di-Follow
            OnlineMapsCameraOrbit.instance.adjustToGameObject = null;
            ListEntityController.Instance.LocateEntity(marker);
            markerOnFollow = marker;
            isFollowing = true;
            markerOnFollow.instance.GetComponent<EntityWalker>().followMarker = true;

            if (marker.instance.GetComponent<DetailSatuan>().tipeTNI == DetailSatuan.TNI.AU || marker.instance.GetComponent<DetailSatuan>().tipeKendaraan == DetailSatuan.type.Udara)
            {
                OnlineMapsCameraOrbit.instance.adjustTo = OnlineMapsCameraAdjust.gameObject;
                OnlineMapsCameraOrbit.instance.adjustToGameObject = marker.instance;
            }
            else
            {
                OnlineMapsCameraOrbit.instance.adjustTo = OnlineMapsCameraAdjust.averageCenter;
                OnlineMapsCameraOrbit.instance.adjustToGameObject = null;
            }
        }

        //if(state)
        //{
        //    OnlineMapsCameraOrbit.instance.adjustTo = OnlineMapsCameraAdjust.gameObject;
        //    OnlineMapsCameraOrbit.instance.adjustToGameObject = marker.instance;
        //}
        //else
        //{
        //    OnlineMapsCameraOrbit.instance.adjustTo = OnlineMapsCameraAdjust.averageCenter;
        //    OnlineMapsCameraOrbit.instance.adjustToGameObject = null;
        //}
        
        marker.instance.GetComponent<EntityWalker>().followMarker = isFollowing;
    }

    /// <summary>
    /// Switch Follow Object to Next Satuan Available
    /// </summary>
    public void SwitchFollowBetween(int add)
    {
        markerIndex += add;
        if (markerIndex > ListEntityController.Instance.GetComponent<GListSatuanScroller>()._data.Count || markerIndex < 0)
        {
            markerIndex = 0;
        }

        Debug.Log("Switch To " + ListEntityController.Instance.GetComponent<GListSatuanScroller>()._data[markerIndex].nama_satuan);

        marker = ListEntityController.Instance.GetComponent<GListSatuanScroller>()._data[markerIndex].marker;
        ToggleFollowSatuan(true);
    }

    public void unselect()
    {
        marker = null;
    }

    private void Update()
    {
        if (marker == null) return;
        if (!PlaybackController.Instance.sliderDragged && PlaybackController.Instance.state != PlaybackController.playbackState.PLAYING) return;

        double marker_lng;
        double marker_lat;
        marker.GetPosition(out marker_lng, out marker_lat);

        var walker = marker.instance.GetComponent<EntityWalker>();
        kecepatan.text = walker.speed.ToString();
        heading.text = marker.rotationY.ToString();

        Coordinate coords = new Coordinate(marker_lat, marker_lng, DateTime.Now, el);
        coords.LoadUTM_MGRS_Info();

        lat.text = coords.Latitude.ToString();
        lng.text = coords.Longitude.ToString();
        mgrs.text = coords.MGRS.ToString();
        utm.text = coords.UTM.ToString();
    }
}
