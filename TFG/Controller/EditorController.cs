using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;
using DigitalRuby.WeatherMaker;
using GlobalVariables;
using HelperUser;
using HelperEditor;
using System.Threading.Tasks;
using Plot.Pasukan;
using Data.Connect;

public class EditorController : MonoBehaviour
{
    [Header("Controller References")]
    public TimetableController _timetable;

    [Header("Start Overlay References")]
    public GameObject UI_StartOverlay;

    [Header("Canvas UI")]
    public Animator UI_Canvas;
    
    [Header("Weather Configuration")]
    public WeatherMakerConfigurationScript weatherConfig;

    [Header("Docs User References")]
    public TextMeshProUGUI UI_Docs;

    [Header("Scenes")]
    [SerializeField] private string serverMultiplayer;
    [SerializeField] private string login;
    [SerializeField] private string assetBrowser;

    #region -- DEBUG MODE --
    /// <summary>
    /// Debug Untuk Login Sebagai User Tanpa Harus Membuka Halaman Login Terlebih Dahulu
    /// </summary>
    [Header("Debug")]
    [SerializeField] private bool debugUser;
    [SerializeField] private string userJSON;
    [SerializeField] private string serviceLogin;
    [SerializeField] private string serviceCB;
    [SerializeField] private string serverColyseus;
    [SerializeField] private int aircraftAlt;
    [SerializeField] private int vehicleScale;
    [SerializeField] private int shipScale;
    [SerializeField] private int aircraftScale;
    #endregion


    #region -- Controller References --
    [SerializeField] private PlotSatuanController _plotSatuan;
    [SerializeField] private PlotRadarController _plotRadar;
    private PlotPasukanController _plotPasukan;
    private EntityEditorController _entityEditor;
    #endregion

    private async void Start()
    {
        // _plotSatuan = PlotSatuanController.Instance;
        _plotPasukan = PlotPasukanController.instance;
        _entityEditor = EntityEditorController.Instance;

        initDefaultWeather();

        if (debugUser)
        {
            await TestLoggedUser();
        }
        else
        {
            await LoadListPlotting();
            await prepareEditor();
        }
    }

    private async Task TestLoggedUser()
    {
        // -- KOGASRATGAB TEST --
        // [{"username":"asops_kogasratgab","id":124,"name":"ASOPS KOGASRATGAB","jenis_user":{"ID":1,"jenis_user":"user"},"bagian":{"ID":8,"nama_bagian":"KOGASRATGAB"},"jabatan":{"ID":2,"nama_jabatan":"asisten"},"asisten":{"ID":6,"nama_asisten":"ASOPS"},"atasan":null,"status_login":1,"be":0}]
        User[] user = User.FromJson(userJSON);

        //await AssetPackageManager.Instance.loadPackageAlutsista();

        new SessionUser(user[0]);

        Config.SERVICE_LOGIN = (serviceLogin != null) ? serviceLogin : "http://localhost/eppkm/public";
        Config.SERVICE_CB = (serviceCB != null) ? serviceCB : "http://localhost/eppkm_simulasi";
        Config.SERVER_COLYSEUS = (serverColyseus != null) ? serverColyseus : "ws://localhost:2567";
        Config.aircraft_alt = aircraftAlt;
        Config.VEHICLE_SCALE = vehicleScale;
        Config.SHIP_SCALE = shipScale;
        Config.AIRCRAFT_SCALE = aircraftScale;

        await API.Instance.GetSkenarioAktif();
        await API.Instance.GetCBTerbaik(SessionUser.id_bagian.ToString());
        //await API.Instance.GetDocument();

        //int index = AssetPackageManager.Instance.ASSETS_NAME.IndexOf("KRI KAPITAN PATTIMURA (ex-Prenziau)");
        //Instantiate(AssetPackageManager.Instance.ASSETS[index]);

        Debug.Log("Prepare Asset");
        SyncGameplayLoading("Preparing Asset");

        await AssetPackageManager.Instance.loadPackageAlutsista();
        //await BasemapLayerController.instance.fetchLayerList();
        //await BasemapLayerController.instance.fetchBasemapList();
        //await BasemapLayerController.instance.getDefaultBasemap();
        // await BasemapControllerWGS.instance.LoadMapServiceFromFileAsync();

        await LoadListPlotting();
        await prepareEditor();
    }

    /// <summary>
    /// Set Default Weather When Scene Started
    /// </summary>
    private void initDefaultWeather()
    {
        //weatherConfig.CloudDropdown.value = 4;

        EnviroSkyMgr.instance.ChangeWeather(2);
    }

    public async Task LoadListPlotting()
    {
        var list_AD = await API.Instance.GetListAlutsista("vehicle");
        var list_AL = await API.Instance.GetListAlutsista("ship");
        var list_AU = await API.Instance.GetListAlutsista("aircraft");

        // Load List Alutsista ke Dalam Plot Satuan
        if(list_AD != null) await _plotSatuan.AddToList(ListSatuan.FromJson(list_AD), StyleListSatuan.FromJson(list_AD), ListSatuanData.typeObject.Darat);
        if(list_AL != null) await _plotSatuan.AddToList(ListSatuan.FromJson(list_AL), StyleListSatuan.FromJson(list_AL), ListSatuanData.typeObject.Laut);
        if(list_AU != null) await _plotSatuan.AddToList(ListSatuan.FromJson(list_AU), StyleListSatuan.FromJson(list_AU), ListSatuanData.typeObject.Udara);

        // Load List Pasukan ke Dalam Plot Pasukan
        var list_pasukan = await API.Instance.GetListAlutsista("pasukan");
        //Debug.Log("list_pasukan : " + list_pasukan);

        // PlotPasukanController.instance.GrabListViaEditor(list_pasukan);
        await PlotPasukanControllerV2.instance.GrabList();

        // Load List Radar Ke Dalam Plot Radar
        var list_radar = await API.Instance.GetListAlutsista("radar");
        // await _plotRadar.AddToList(GetListRadar.FromJson(list_radar));
    }

    private async Task prepareEditor()
    {
        Debug.Log("Prepare Editor");
        SyncGameplayLoading("Preparing Editor");
        await Task.Delay(1000);

        SyncGameplayLoading("Setting Up Environment");
        initDefaultWeather();
        await Task.Delay(1000);

        // Persiapkan Data Timetable Kegiatan dari Kogas yang login
        SyncGameplayLoading("Load Timetable Kegiatan");
        var kegiatan = await _timetable.InitTimetable();

        SyncGameplayLoading("Load CB");
        await _entityEditor.LoadEntityFromCB();

        UI_Docs.text = "[ " + CBSendiri.nama_document + " ] - " + SessionUser.name;

        await Task.Delay(1000);
        UI_StartOverlay.GetComponent<Animator>().Play("ServiceSlideOut");
    }

    /// <summary>
    /// Change Loading Text After Each Prepare Progress Done
    /// </summary>
    /// <param name="message"></param>
    private void SyncGameplayLoading(string message)
    {
        var loadingText = UI_StartOverlay.GetComponent<Metadata>().FindParameter("loading-text").parameter;
        if (loadingText == null) return;

        if (loadingText != null) loadingText.GetComponent<TextMeshProUGUI>().text = message + "...";
    }

    public async void PlayCB()
    {
        UI_StartOverlay.GetComponent<Animator>().Play("ServiceSlideIn");
        SyncGameplayLoading("Playing Skenario...");

        await Task.Delay(2000);

        SceneLoad.returnTo = "Scenario_Editor";
        LevelManager.Instance.LoadScene(serverMultiplayer, null, "FadeIn", "FadeOut");
        await Task.Delay(250);
        UI_Canvas.Play("SlideOut");
    }

    public async void refreshMarker()
    {
        foreach(OnlineMapsMarker3D marker in OnlineMapsMarker3DManager.instance)
        {
            if (marker.prefab == null) OnlineMapsMarker3DManager.RemoveItem(marker);
        }

        await Task.Delay(100);

        foreach (OnlineMapsMarker marker2D in OnlineMapsMarkerManager.instance)
        {
            if (marker2D.texture == null || marker2D.texture == OnlineMapsMarkerManager.instance.defaultTexture) OnlineMapsMarkerManager.RemoveItem(marker2D);
        }
    }

    /// <summary>
    /// Logout Back to Login Menu
    /// </summary>
    public async void LogoutToMenu()
    {
        UI_Canvas.Play("SlideOut");
        LevelManager.Instance.LoadScene("TFG_Login", null, "FadeIn", "FadeOut");
    }
}