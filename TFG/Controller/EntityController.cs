using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using HelperDataAlutsista;
//using HelperKecepatan;
using UnityEngine;
using Newtonsoft.Json.Linq;
using SickscoreGames.HUDNavigationSystem;

using HelperPlotting;
using GlobalVariables;
using UnityEngine.UI;
using Newtonsoft.Json;
using System.Linq;
using System.Text;
using System.Globalization;
using ListOfSimpleFunctions;

using static Walker.Path;
using static ListOfSimpleFunctions.Functions;

public class EntityController : MonoBehaviour
{
    [Header("Default 3D Assets")]
    //public GameObject defaultVehicle;
    //public GameObject defaultShip;
    //public GameObject defaultAircraft;

    public HUDNavigationElement prefabHUDElement;
    public HUDNavigationElement prefabMarker2DElement;
    public HUDNavigationElement prefabMarkerRawElement;

    public List<Entities> OBJ_ENTITY = new List<Entities>();

    [Header("Data Entity")]
    public List<DetailSatuan> OBJ_SATUAN = new List<DetailSatuan>();
    public List<DetailSituasi> OBJ_SITUASI = new List<DetailSituasi>();

    private GameController _controller;
    private EntityRouteController _route;

    private float kecepatan_induk_formasi = 0;
    private string induk_formasi;

    [Header("Data Marker 2D")]
    public GameObject prefabIcon;
    public Sprite iconSituasi;

    [Header("Data Marker 3D")]
    public GameObject prefabRadar;

    [SerializeField] string parseEx;
    int except = 0;

    private void Start()
    {
        _route = FindObjectOfType<EntityRouteController>();
        _controller = FindObjectOfType<GameController>();
    }
    
    /// <summary>
    /// Load & Spawn Entity CB Musuh dari Database
    /// </summary>
    public async Task LoadEntityFromCB(bool loadMusuh = false, string id_user = null, bool loadAsStatic = false)
    {
        try
        {
            // Load Data dari Database
            var plotting = await API.Instance.LoadDataCB(loadMusuh, id_user, CBMusuh.id_kogas, CBMusuh.nama_document);
            parseEx = JArray.Parse(plotting[0].ToString()).ToString();
            
            // Spawn Entity Satuan
            await SpawnEntitySatuan(setJArrayResult(plotting, 0), null, loadAsStatic);
        }
        catch
        {
            if(except == 0)
            {
                // Load Data dari Database
                var plotting = await API.Instance.LoadDataCB(false, id_user, CBSendiri.id_kogas, CBSendiri.nama_document);
                parseEx = JArray.Parse(plotting[0].ToString()).ToString();
                
                // Spawn Entity Satuan
                await SpawnEntitySatuan(setJArrayResult(plotting, 0), null, loadAsStatic);
                except++;
            }
            else
            Debug.Log("Tidak Memiliki CB Musuh");
        }
    }

    /// <summary>
    /// Load & Spawn Entity CB Terpilih dari Database
    /// </summary>
    public async Task LoadEntityFromCB(string id_user = null, bool loadAsStatic = false)
    {
        // Load Data dari Database
        var plotting = await API.Instance.LoadDataCB(false, id_user, CBSendiri.id_kogas, CBSendiri.nama_document);

        // Spawn Entity Satuan
        await SpawnEntitySatuan(setJArrayResult(plotting, 0), null, loadAsStatic);

        // Spawn Entity Formasi (Di Load Sebelum Generate Load Untuk Generate Kecepatan Formasi yang Sesuai)
        //if (!loadAsStatic) await SpawnEntityFormasi(setJArrayResult(plotting, 11));

        // Spawn Rute Satuan
        await SpawnRouteMisi(setJArrayResult(plotting, 13), id_user);

        // Spawn Entity Situasi
        await SpawnEntitySituasi(setJArrayResult(plotting, 2));

        // Spawn Entity Radar
        await SpawnEntityRadar(setJArrayResult(plotting, 7));

        // Spawn Entity Tools
        await SpawnEntityTools(setJArrayResult(plotting, 10));

        // Spawn Entity Obstacle
        await SpawnEntityObstacle(setJArrayResult(plotting, 3));

        // Spawn Entity Kekuatan

        // Spawn Entity SRoute

        // Spawn Entity Bungus
        await SpawnEntityBungus(setJArrayResult(plotting, 5));

        // Spawn Entity Passen

        // Spawn Entity Logis

        // Spawn Entity Text

        // Spawn Entity Icon Custom
        await SpawnEntityIconCustom(setJArrayResult(plotting, 12));

        // Spawn Entity Mission

        // Spawn Entity Animasi
        //await SpawnEntityAnimasi(setJArrayResult(plotting, 14));

        // Spawn Entity Video
        //await SpawnEntityVideo(setJArrayResult(plotting, 15));

        // Spawn Entity Radar Simulasi

        // Spawn Entity Faskes
    }

    /// <summary>
    /// Remove Entity CB User Tidak Terpakai
    /// </summary>
    /// <param name="id_user">ID User</param>
    /// <returns></returns>
    public async Task RemoveEntityFromCB(string id_user)
    {
        for(int i=0; i < OBJ_ENTITY.Count; i++)
        {
            if(OBJ_ENTITY[i].id_user == id_user)
            {
                var walker = OBJ_ENTITY[i].gameObject.GetComponent<EntityWalker>();

                if(walker != null)
                {
                    Destroy(walker.marker.instance);
                    Destroy(walker.gameObject);
                }
            }
        }
        
        var objRouteMisi = _route.misiRouteContainer.Find(id_user);
        if (objRouteMisi != null)
        {
            Destroy(objRouteMisi.gameObject);
        }

        var objDrawRoutes = _route.drawRouteContainer.Find(id_user);
        if (objDrawRoutes != null)
        {
            foreach (var item in objDrawRoutes.GetComponent<MetadataDrawRoute>().dataDrawRoute)
            {
                item.Dispose();
            }
            
            Destroy(objDrawRoutes.gameObject);
        }
    }

    /// <summary>
    /// Load & Spawn Entity CB Dari Colyseus
    /// </summary>
    public async Task<bool> LoadEntityFromColyseus(EntityTFG entity)
    {
        // Spawn Entity Satuan
        return await SpawnEntitySatuan(null, entity);
    }

    /// <summary>
    /// Spawn Entity Satuan
    /// </summary>
    /// <param name="arraySatuan">list satuan (JArray)</param>
    /// <returns></returns>
    //public async Task<bool> SpawnEntitySatuan(JArray arraySatuan, EntityTFG entity = null, bool loadAsStatic = false)
    //{
    //    if(arraySatuan != null) if (arraySatuan.Count == 0) return false;
    //    int indexLoop = (arraySatuan != null) ? arraySatuan.Count : 2;

    //    for (int i = 0; i < indexLoop; i++)
    //    {
    //        Debug.Log("Spawn Satuan " + i);
    //        var satuan = EntitySatuan.FromJson((arraySatuan != null) ? arraySatuan[i].ToString() : entity.defaultData);
    //        var style_satuan = EntitySatuanStyle.FromJson(satuan.style);
    //        var info_satuan = EntitySatuanInfo.FromJson(satuan.info);

    //        if(SessionUser.id != 1)
    //        {
    //            if (satuan.id_user != SessionUser.id.ToString()) return false;
    //        }

    //        // Get & Set Detail Satuan
    //        await SetDetailSatuan(satuan, style_satuan.grup, entity);

    //        if(style_satuan.grup == "10")
    //        {
    //            satuan.jenis = "pasukan";
    //        }

    //        var objPosition = new Vector2(satuan.lng, satuan.lat);

    //        // Create Marker 3D Berdasarkan Object 3D-nya
    //        var marker = OnlineMapsMarker3DManager.CreateItem(objPosition, getAssetSatuan(info_satuan.nama_satuan, satuan.object3D, (arraySatuan != null) ? satuan.jenis : entity.jenis));
    //        marker["markerSource"] = marker;

    //        // Add & Set WALKER Properties
    //        marker.instance.gameObject.AddComponent<DetailSatuan>();

    //        if (!loadAsStatic)
    //        {
    //            marker.instance.gameObject.AddComponent<EntityWalker>();
    //            marker.instance.gameObject.AddComponent<Entities>();

    //            var walker = marker.instance.GetComponent<EntityWalker>();
    //            var entitiesData = marker.instance.GetComponent<Entities>();
    //            walker.marker = marker;

    //            walker.coordinate = objPosition;
    //            walker.name = satuan.nama;
    //            walker.tag = getTagSatuan((arraySatuan != null) ? satuan.jenis : entity.jenis);
    //            walker.entities = entitiesData;

    //            // Set Detail Satuan
    //            if (!OBJ_ENTITY.Contains(entitiesData)) OBJ_ENTITY.Add(entitiesData);
    //            entitiesData.id_entity = satuan.nama;
    //            entitiesData.nama_entity = info_satuan.nama_satuan;
    //            entitiesData.id_user = satuan.id_user;
    //            entitiesData.type = Entities.entityType.Satuan;
    //            entitiesData.heading = info_satuan.heading;
    //            entitiesData.opacity = (arraySatuan != null) ? Entities.opacityType.ADD :
    //            entity.opacity == "add" ? Entities.opacityType.ADD : Entities.opacityType.REMOVE;

    //            // Add Route Component
    //            walker.gameObject.AddComponent<MetadataRoute>();
    //        }
    //        else
    //        {
    //            marker.instance.name = satuan.nama;
    //            marker.instance.tag = getTagSatuan((arraySatuan != null) ? satuan.jenis : entity.jenis);
    //            marker.label = satuan.nama;
    //        }


    //        //if (!OBJ_SATUAN.Contains(marker.instance.gameObject.GetComponent<DetailSatuan>())) OBJ_SATUAN.Add(marker.instance.gameObject.GetComponent<DetailSatuan>());

    //        // Set Detail Satuan
    //        SetDataSatuan(marker.instance, satuan, style_satuan, info_satuan);

    //        // Set Ukuran Satuan
    //        marker.sizeType = OnlineMapsMarker3D.SizeType.meters;
    //        marker.scale = 1;

    //        if (satuan.jenis == "aircraft") marker.altitude = Config.aircraft_alt != null ? (float)Config.aircraft_alt : 1000;
    //        marker.altitudeType = OnlineMapsAltitudeType.absolute;

    //        if (style_satuan.grup == "10")
    //        {
    //            marker.scale = 10;
    //        }
    //        else
    //        {
    //            marker.scale = setScaleSatuan(satuan.jenis);
    //        }

    //        // Set Heading Satuan
    //        marker.rotation = Quaternion.Euler(0, info_satuan.heading + 180, 0);

    //        // Add Simbol Taktis Component
    //        var parentHUD = new GameObject("HUD_ELEMENT");
    //        parentHUD.transform.parent = marker.transform;
    //        parentHUD.transform.localPosition = Vector3.zero;

    //        parentHUD.tag = "entity-simboltaktis";

    //        var HUDElement = Instantiate(prefabHUDElement.gameObject, parentHUD.transform).GetComponent<HUDNavigationElement>();
    //        //HUDElement.tag = "entity-simboltaktis";

    //        HUDElement.Prefabs.IndicatorPrefab = Instantiate(HUDElement.Prefabs.IndicatorPrefab, marker.transform);
    //        HUDElement.Prefabs.IndicatorPrefab.name = "-- simbol taktis --";
    //        //HUDElement.Prefabs.IndicatorPrefab.tag = "entity-simboltaktis";

    //        string fontIndex = (arraySatuan != null) ? ((char)style_satuan.index).ToString() : ((char)entity.kode_ascii).ToString();
    //        string fontFamily = (arraySatuan != null) ? style_satuan.nama : entity.name_family;

    //        //if (arraySatuan != null)
    //        //{
    //        //    HUDElement.Prefabs.IndicatorPrefab.ChangeFontTaktis(((char)style_satuan.index).ToString(), style_satuan.nama);
    //        //}
    //        //else
    //        //{
    //        //    HUDElement.Prefabs.IndicatorPrefab.ChangeFontTaktis(((char)entity.kode_ascii).ToString(), entity.name_family);
    //        //}

    //        HUDElement.Prefabs.IndicatorPrefab.ChangeFontTaktis(fontIndex, fontFamily);
    //        HUDElement.Prefabs.IndicatorPrefab.ChangeFontTaktisColor(info_satuan.warna == "blue" ? Color.blue : Color.red);
    //        if (!loadAsStatic)
    //        {
    //            var walker = marker.instance.GetComponent<EntityWalker>();
    //            var entitiesData = marker.instance.GetComponent<Entities>();

    //            // Add HUD Info
    //            //HUDElement.Prefabs.IndicatorPrefab.ChangeEntityInfo(info_satuan.nama_satuan + "\n" + "Speed : " + SpeedConvertion.Instance.getSpeedName(walker.speedType, walker.speed.ToString()) + "\n" + "Heading : " + satuan.heading);
    //            HUDElement.Prefabs.IndicatorPrefab.ChangeEntityInfo(info_satuan.nama_satuan);
    //            entitiesData.HUDElement = HUDElement;

    //            // Add Satuan Ke Dalam List Browser
    //            await ListEntityController.Instance.AddToList(marker, entitiesData, fontIndex, fontFamily);
    //            marker.OnClick += ListEntityController.Instance.OnEntitySelected;
    //        }
    //        else
    //        {
    //            // Add HUD Info
    //            HUDElement.Prefabs.IndicatorPrefab.ChangeEntityInfo(info_satuan.nama_satuan);
    //        }

    //        // -- Komponen Sync Pergerakan To Colyseus Hanya Digunakan Jika Data Berasal dari Database, Bukan Colyseus --
    //        if (_controller && arraySatuan != null)
    //        {
    //            if (_controller.GameMode == GameController.gameplayMode.MULTIPLAYER)
    //            {
    //                await _controller._colyseus.AddSatuanState(satuan, info_satuan, style_satuan);
    //            }
    //        }
    //    }

    //    return true;
    //}

    public async Task<bool> SpawnEntitySatuan(JArray arraySatuan, EntityTFG entity = null, bool loadAsStatic = false)
    {
        if(arraySatuan != null) if (arraySatuan.Count == 0) return false;
        int indexLoop = (arraySatuan != null) ? arraySatuan.Count : 2;
        Debug.Log(indexLoop);

        for (int i = 0; i < indexLoop; i++)
        {
            Debug.Log("Spawn Satuan " + i);
            Debug.Log(arraySatuan[i].ToString());
            var satuan = EntitySatuan.FromJson((arraySatuan != null) ? arraySatuan[i].ToString() : entity.defaultData);
            var style_satuan = EntitySatuanStyle.FromJson(satuan.style);
            var info_satuan = EntitySatuanInfo.FromJson(satuan.info);

            // if(SessionUser.id != 1)
            // {
            //     if (satuan.id_user != SessionUser.id.ToString()) return false;
            // }
            
            // Get & Set Detail Satuan
            await SetDetailSatuan(satuan, style_satuan.grup, entity);

            if(style_satuan.grup == "10")
            {
                satuan.jenis = "pasukan";
            }

            var objPosition = new Vector2(satuan.lng, satuan.lat);
            var _satuanSendiri = CreateMarkerParent("Satuan Sendiri");
            var _satuanMusuh = CreateMarkerParent("Satuan Musuh");

            // Create Marker 3D Berdasarkan Object 3D-nya(arraySatuan != null) ? style_satuan.nama : entity.name_family
            var marker = Create3D(objPosition, getAssetSatuan(info_satuan.nama_satuan, satuan.object3D, (arraySatuan != null) ? satuan.jenis : entity.jenis));
            marker["markerSource"] = marker;
            marker.instance.transform.parent = satuan.id_user == SessionUser.id.ToString() ? _satuanSendiri.transform : _satuanMusuh.transform;

            // Add & Set WALKER Properties
            marker.instance.gameObject.AddComponent<DetailSatuan>();

            if (!loadAsStatic)
            {
                marker.instance.gameObject.AddComponent<EntityWalker>();
                marker.instance.gameObject.AddComponent<Entities>();

                var walker = marker.instance.GetComponent<EntityWalker>();
                var entitiesData = marker.instance.GetComponent<Entities>();
                walker.marker = marker;

                walker.coordinate = objPosition;
                walker.name = satuan.nama;
                walker.tag = getTagSatuan((arraySatuan != null) ? satuan.jenis : entity.jenis);
                walker.entities = entitiesData;

                // Set Detail Satuan
                if (!OBJ_ENTITY.Contains(entitiesData)) OBJ_ENTITY.Add(entitiesData);
                entitiesData.id_entity = satuan.nama;
                entitiesData.nama_entity = info_satuan.nama_satuan;
                entitiesData.id_user = satuan.id_user;
                entitiesData.type = Entities.entityType.Satuan;
                entitiesData.heading = info_satuan.heading;
                entitiesData.opacity = (arraySatuan != null) ? Entities.opacityType.ADD :
                entity.opacity == "add" ? Entities.opacityType.ADD : Entities.opacityType.REMOVE;

                // Add Route Component
                walker.gameObject.AddComponent<MetadataRoute>();
            }
            else
            {
                marker.instance.name = satuan.nama;
                marker.instance.tag = getTagSatuan((arraySatuan != null) ? satuan.jenis : entity.jenis);
                marker.label = satuan.nama;
            }

            
            if (!OBJ_SATUAN.Contains(marker.instance.gameObject.GetComponent<DetailSatuan>())) OBJ_SATUAN.Add(marker.instance.gameObject.GetComponent<DetailSatuan>());

            // Set Detail Satuan
            if(style_satuan.grup != "10")
            SetDataSatuan(marker.instance, satuan, style_satuan, info_satuan);

            // Set Ukuran Satuan
            marker.sizeType = OnlineMapsMarker3D.SizeType.meters;
            marker.scale = 1;
            marker.OnClick += ListEntityController.Instance.OnEntitySelected;

            if (style_satuan.grup == "10")
            {
                marker.scale = 10;
            }
            else
            {
                //Debug.Log("Scale = " + setScaleSatuan(satuan.jenis));
                marker.scale = setScaleSatuan(satuan.jenis);
            }

            if (satuan.jenis == "aircraft")
            {
                marker.altitude = Config.aircraft_alt != null ? (float)Config.aircraft_alt : 100;
                marker.altitudeType = OnlineMapsAltitudeType.absolute;
            }

            // Set Heading Satuan
            marker.rotation = Quaternion.Euler(0, info_satuan.heading + 180, 0);

            // Add Simbol Taktis Component
            var parentHUD = new GameObject("HUD_ELEMENT");
            parentHUD.transform.parent = marker.transform;
            //parentHUD.transform.localPosition = new Vector3(0, marker.scale * 25, 0);
            parentHUD.transform.localPosition = Vector3.zero;

            parentHUD.tag = "entity-simboltaktis";
            
            var HUDElement = Instantiate(prefabHUDElement.gameObject, parentHUD.transform).GetComponent<HUDNavigationElement>();
            //HUDElement.tag = "entity-simboltaktis";
                
            HUDElement.Prefabs.IndicatorPrefab = Instantiate(HUDElement.Prefabs.IndicatorPrefab, marker.transform);
            HUDElement.Prefabs.IndicatorPrefab.name = "-- simbol taktis --";
            //HUDElement.Prefabs.IndicatorPrefab.tag = "entity-simboltaktis";

            string fontIndex = (arraySatuan != null) ? ((char)style_satuan.index).ToString() : ((char)entity.kode_ascii).ToString();
            string fontFamily = (arraySatuan != null) ? style_satuan.nama : entity.name_family;

            //if (arraySatuan != null)
            //{
            //    HUDElement.Prefabs.IndicatorPrefab.ChangeFontTaktis(((char)style_satuan.index).ToString(), style_satuan.nama);
            //}
            //else
            //{
            //    HUDElement.Prefabs.IndicatorPrefab.ChangeFontTaktis(((char)entity.kode_ascii).ToString(), entity.name_family);
            //}

            HUDElement.Prefabs.IndicatorPrefab.ChangeFontTaktis(fontIndex, fontFamily);
            HUDElement.Prefabs.IndicatorPrefab.ChangeFontTaktisColor(info_satuan.warna == "blue" ? Color.blue : Color.red);
            if (!loadAsStatic)
            {
                var walker = marker.instance.GetComponent<EntityWalker>();
                var entitiesData = marker.instance.GetComponent<Entities>();

                // Add HUD Info
                //HUDElement.Prefabs.IndicatorPrefab.ChangeEntityInfo(info_satuan.nama_satuan + "\n" + "Speed : " + SpeedConvertion.Instance.getSpeedName(walker.speedType, walker.speed.ToString()) + "\n" + "Heading : " + satuan.heading);
                HUDElement.Prefabs.IndicatorPrefab.ChangeEntityInfo(info_satuan.nama_satuan);
                entitiesData.HUDElement = HUDElement;

                // Add Satuan Ke Dalam List Browser
                await ListEntityController.Instance.AddToList(marker, entitiesData, fontIndex, fontFamily);
            }
            else
            {
                // Add HUD Info
                HUDElement.Prefabs.IndicatorPrefab.ChangeEntityInfo(info_satuan.nama_satuan);
            }

            // -- Komponen Sync Pergerakan To Colyseus Hanya Digunakan Jika Data Berasal dari Database, Bukan Colyseus --
            if (_controller && arraySatuan != null)
            {
                if (_controller.GameMode == GameController.gameplayMode.MULTIPLAYER)
                {
                    await _controller._colyseus.AddSatuanState(satuan, info_satuan, style_satuan);
                }
            }
        }

        return true;
    }

    public async Task SpawnEntityFormasi(JArray arrayFormasi, string id_user = null)
    {
        if (arrayFormasi == null) return;
        if (arrayFormasi.Count == 0) return;

        string id_satuan_pembanding = null;

        // Get Induk Formasi
        for(int i = 0; i < arrayFormasi.Count; i++)
        {
            var formasi = EntityFormasi.FromJson(arrayFormasi[i].ToString());

            // Get Info Formasi
            var info_formasi = EntityFormasiInfo.FromJson(formasi.info_formasi);
            // Get Formasi Satuan
            var satuan_formasi = EntityFormasiSatuan.FromJson(info_formasi.satuan_formasi);

            // Jika Object Satuan Sebagai Induk Formasi, Set Sebagai Pembanding
            if(id_satuan_pembanding == null)
            {
                if (satuan_formasi.inti != 1) continue;
                id_satuan_pembanding = satuan_formasi.id_point;
            }
            else
            {
                foreach(DetailSatuan satuan in OBJ_SATUAN)
                {
                    if(satuan.id == id_satuan_pembanding)
                    {
                        kecepatan_induk_formasi = satuan.kecepatan;
                        induk_formasi = satuan.id;
                    }
                }
            }
        }
    }

    public async Task SpawnRouteMisi(JArray arrayMisi, string id_user = null)
    {
        if (arrayMisi.Count == 0) return;

        double jarak_jalur_induk_formasi = 0;
        if(kecepatan_induk_formasi != 0 && induk_formasi != null)
        {
            for (int i = 0; i < arrayMisi.Count; i++)
            {
                var misi = EntityMisi.FromJson(arrayMisi[i].ToString());
                if (misi.properties == null) continue;

                if (misi.id_object == induk_formasi)
                {
                    var misi_properties = misi.properties;

                    List<Vector2> paths = new List<Vector2>();
                    for (int j = 0; j < misi_properties.jalur.Count; j++)
                    {
                        if(j > 0)
                        {
                            jarak_jalur_induk_formasi += OnlineMapsUtils.DistanceBetweenPointsD(new Vector2(misi_properties.jalur[j - 1].lng, misi_properties.jalur[j - 1].lat), new Vector2(misi_properties.jalur[j].lng, misi_properties.jalur[j].lat));
                        }
                    }
                }
            }
        }
        
        // Add Rute Per-Misi
        for (int i = 0; i < arrayMisi.Count; i++)
        {
            var misi = EntityMisi.FromJson(arrayMisi[i].ToString());
            
            // Set Property Misi (Jump to Next Index if It's Null)
            if (misi.properties == null) continue;
            var misi_properties = misi.properties;

            if (misi_properties.jalur == null) continue;

            // Create Route Path Points
            List<Vector3> paths = new List<Vector3>();
            double jarak_jalur = 0;

            for (int j = 0; j < misi_properties.jalur.Count; j++)
            {
                paths.Add(new Vector3(misi_properties.jalur[j].lng, misi_properties.jalur[j].lat, (float)misi_properties.jalur[j].alt));
                if(j > 0)
                {
                    jarak_jalur += OnlineMapsUtils.DistanceBetweenPointsD(new Vector2(misi_properties.jalur[j - 1].lng, misi_properties.jalur[j - 1].lat), new Vector2(misi_properties.jalur[j].lng, misi_properties.jalur[j].lat));
                }
            }
            Debug.Log(misi.id_object + " misi: " + misi.id_mission);
            var distance1 = GetDistance(paths);
            var menit = RouteController.estimateTimeFMin((float)distance1, misi_properties.kecepatan, misi_properties.type);
            Debug.Log("Distance: " + distance1 + " Kecepatan: " + RouteController.GetKecepatanByType(misi_properties.kecepatan, misi_properties.type) + "|Kilometer");

            paths = SmoothPath(paths);
            var distance2 = GetDistance(paths);
            var speedAfter = distance2 / menit * 60;
            Debug.Log("New Distance: " + distance2 + " New Kecepatan: " + speedAfter + "|Kilometer");

            // Set Rute Misi Ke Satuannya
            var newRoute = await _route.AddNewRoute(paths, misi, misi_properties, id_user, speedAfter);

            // Buat atau Gunakan Container Drawing Route Untuk Menyimpan Data Drawing Berdasarkan ID Usernya
            // (Disimpan per-user agar ketika user tersebut logout seluruh data drawing user tersebut dapat terhapus)
            MetadataDrawRoute newDrawRoute = null;
            var objDrawRoutes = _route.drawRouteContainer.Find((id_user) == null ? SessionUser.id.ToString() : id_user);
            if (objDrawRoutes == null)
            {
                newDrawRoute = Instantiate(_route.prefabDrawRoute, _route.drawRouteContainer);
                newDrawRoute.name = (id_user == null) ? SessionUser.id.ToString() : id_user;
            }
            else
            {
                newDrawRoute = objDrawRoutes.GetComponent<MetadataDrawRoute>();
            }

            for (int j = 0; j < OBJ_ENTITY.Count; j++)
            {
                if (misi.id_object == OBJ_ENTITY[j].gameObject.name)
                {
                    var entityWalker = OBJ_ENTITY[j].GetComponent<EntityWalker>();
                    //var speedConverter = new speedConversion();
                    
                    entityWalker.speedType = SpeedConvertion.Instance.getSpeedType(misi_properties.type);
                    entityWalker.speed = float.Parse(misi_properties.kecepatan);

                    if(kecepatan_induk_formasi != 0 && induk_formasi != null)
                    {
                        entityWalker.speed = kecepatan_induk_formasi * ((float)jarak_jalur / (float)jarak_jalur_induk_formasi);
                    }
                    
                    entityWalker.rute.Add(newRoute.Item1);
                    entityWalker.rutePath.Add(newRoute.Item2);
                    entityWalker.InitRuteWalker();
                }
                
                newDrawRoute.dataDrawRoute.Add(newRoute.Item2);
            }
        }
    }

    public async Task SpawnEntitySituasi(JArray arraySituasi)
    {
        if (arraySituasi == null) return;

        for (int i = 0; i < arraySituasi.Count; i++)
        {
            Debug.Log("Spawn Situasi " + i);
            var situasi = EntitySituasi.FromJson(arraySituasi[i].ToString());
            var info_situasi = EntitySituasiInfo.FromJson(situasi.info_situasi);

            var objPosition = new Vector2(situasi.lng, situasi.lat);

            // Create Marker 3D
            var marker = OnlineMapsMarker3DManager.CreateItem(objPosition, AssetPackageManager.Instance.prefabSituasi);
            marker.instance.name = situasi.nama;
            marker.instance.tag = "entity-situasi";

            // Add & Set Detail Properties
            var detail = marker.instance.AddComponent<DetailSituasi>();
            detail.marker = marker;

            detail.nama = situasi.nama;
            detail.desc = info_situasi.isi_situasi;
            detail.warna = (info_situasi.warna == "blue") ? Color.blue : Color.red;
            detail.size = info_situasi.size;

            if(!DateTime.TryParseExact(info_situasi.tgl_situasi + " " + info_situasi.waktu_situasi.ToString(), "MMddyyyy HHmm", CultureInfo.InvariantCulture, DateTimeStyles.None, out detail.date))
            {
                DateTime.TryParseExact(info_situasi.tgl_situasi + " " + info_situasi.waktu_situasi.ToString(), "ddMMyyyy HHmm", CultureInfo.InvariantCulture, DateTimeStyles.None, out detail.date);
            }
                
            OBJ_SITUASI.Add(detail);

            // Add & Set Timetrial Properties
            var timetrial = marker.instance.AddComponent<EntityTimetrial>();
            timetrial.startTime = detail.date;
            timetrial.endTime = SkenarioAktif.WaktuAkhir;

            // Add Simbol Taktis Component
            var parentHUD = new GameObject("HUD_ELEMENT");
            parentHUD.transform.parent = marker.instance.transform.GetChild(0);
            parentHUD.transform.localPosition = Vector3.zero;

            var HUDElement = Instantiate(prefabMarker2DElement.gameObject, parentHUD.transform).GetComponent<HUDNavigationElement>();
            HUDElement.tag = "entity-simboltaktis";

            HUDElement.Prefabs.IndicatorPrefab.ChangeIcon(AssetPackageManager.Instance.prefabSituasi.GetComponent<MetadataSprite>().FindParameter("icon").parameter);
            HUDElement.Prefabs.IndicatorPrefab.ChangeEntityInfo("");

            //HUDElement.Prefabs.IndicatorPrefab = Instantiate(HUDElement.Prefabs.IndicatorPrefab, marker.transform);
            //HUDElement.Prefabs.IndicatorPrefab.name = "-- simbol taktis --";
            //HUDElement.Prefabs.IndicatorPrefab.ChangeIcon(iconSituasi);
            //HUDElement.Prefabs.IndicatorPrefab.ChangeIconColor(detail.warna);
        }
    }

    public async Task SpawnEntityRadar(JArray arrayRadar)
    {
        var assetPackageManager = AssetPackageManager.Instance;

        if (arrayRadar == null) return;
        for (int i = 0; i < arrayRadar.Count; i++)
        {
            Debug.Log("Spawn Radar " + i);
            var radar = RadarEntity.FromJson(arrayRadar[i].ToString());
            var info_radar = radar.info_radar;
            var objPosition = new Vector2(radar.lng, radar.lat);

            // Create Marker 3D
            var marker3D = Create3D(objPosition, assetPackageManager.prefabRadar);
            marker3D.instance.name = radar.nama;
            marker3D.instance.tag = "entity-radar";
            marker3D.instance.transform.parent = CreateMarkerParent("Radars");

            // Add & Set Radar Properties
            var detail = marker3D.instance.gameObject.AddComponent<DetailRadar>();
            detail.radar = radar;
            detail.json = RadarEntity.ToString(radar);

            var color = SessionUser.nama_asisten == "ASOPS" ? Color.blue : Color.red;
            color.a = .5f;

            // Draw 2D Radar Circle
            var points  = new ArrowManeuver().DrawCircle(objPosition, info_radar.radius / 1000);
            var element = AddElement(DrawPoly(points, Color.blue, 1f));
            element.DrawOnTileset(OnlineMapsTileSetControl.instance, OnlineMapsDrawingElementManager.instance.Count - 1);
            element.instance.transform.parent = CreateMarkerParent("Radars");
            element.instance.transform.position = Vector3.up;
            element.name = "draw_" + radar.nama;
            element.visible = false;
            detail.showRadar = true;
            detail.radar2D = element;

            // Set Ukuran Radar
            marker3D.sizeType = OnlineMapsMarker3D.SizeType.meters;
            marker3D.scale = info_radar.radius * 2;
            
            //Set HUD untuk marker
            SetHUD(marker3D, info_radar.judul, info_radar.warna, info_radar.id_symbol.nama, info_radar.id_symbol.index);
        }
    }

    public async Task<bool> SpawnEntityTools(JArray arrayTools)
    {
        if (isJArrayNull(arrayTools)) return false;

        int indexLoop = (arrayTools != null) ? arrayTools.Count : 2;

        for (int i = 0; i < indexLoop; i++)
        {
            Debug.Log("Spawn Tools " + i);

            var tools = EntityTools.FromJson(arrayTools[i].ToString());
            var properties_tools = EntityToolsProperties.FromJson(tools.properties);

            switch (tools.type)
            {
                case "LineString":
                    var geometriLine = EntityToolsGeometryLine.FromJson(tools.geometry);
                    var colorPolyline = properties_tools.color == "blue" ? Color.blue : Color.red;
                    colorPolyline.a = properties_tools.opacity;

                    List<Vector2> coords_line = new List<Vector2>();
                    for (int j = 0; j < geometriLine.coordinates.Count; j++)
                    {
                        coords_line.Add(new Vector2((float)geometriLine.coordinates[j][0], (float)geometriLine.coordinates[j][1]));
                    }

                    var line = OnlineMapsDrawingElementManager.AddItem(new OnlineMapsDrawingLine(coords_line, colorPolyline, properties_tools.weight * 0.05f));

                    break;
                case "Polygon":
                    var geometriPolygon = EntityToolsGeometryPolygon.FromJson(tools.geometry);

                    List<Vector2> coords_polygon = new List<Vector2>();
                    for (int j = 0; j < geometriPolygon.coordinates[0].Count; j++)
                    {
                        coords_polygon.Add(new Vector2((float)geometriPolygon.coordinates[0][j][0], (float)geometriPolygon.coordinates[0][j][1]));
                    }

                    var colorPolygon = properties_tools.color == "blue" ? Color.blue : Color.red;
                    colorPolygon.a = (properties_tools.opacity > 0) ? properties_tools.opacity : 1;

                    var colorPolygonInside = properties_tools.fillColor == "blue" ? Color.blue : Color.red;
                    colorPolygonInside.a = (properties_tools.fillOpacity > 0) ? properties_tools.fillOpacity : 0.5f;

                    var poly = OnlineMapsDrawingElementManager.AddItem(new OnlineMapsDrawingPoly(coords_polygon, colorPolygon, properties_tools.weight * 0.05f, colorPolygonInside));

                    break;
                case "circle":
                    var geometriCircle = EntityToolsGeometryCircle.FromJson(tools.geometry);

                    var colorCircle = (properties_tools.color == "blue") ? Color.blue : Color.red;
                    colorCircle.a = (properties_tools.opacity > 0) ? properties_tools.opacity : 1;

                    var colorCircleInside = (properties_tools.fillColor == "blue") ? Color.blue : Color.red;
                    colorCircleInside.a = (properties_tools.fillOpacity > 0) ? properties_tools.fillOpacity : 1;

                    DrawEntityToolCircle(geometriCircle.coordinates[0], geometriCircle.coordinates[1], properties_tools.radius / 1000, colorCircle, colorCircleInside);
                    break;
            }
        }

        return false;
    }

    private void DrawEntityToolCircle(double lng, double lat, float radius, Color lineColor, Color fillColor, int segments = 32)
    {
        // Get the coordinate at the desired distance
        double nlng, nlat;
        OnlineMapsUtils.GetCoordinateInDistance(lng, lat, radius, 90, out nlng, out nlat);

        double tx1, ty1, tx2, ty2;
        // Convert the coordinate under cursor to tile position
        OnlineMaps.instance.projection.CoordinatesToTile(lng, lat, 20, out tx1, out ty1);

        // Convert remote coordinate to tile position
        OnlineMaps.instance.projection.CoordinatesToTile(nlng, nlat, 20, out tx2, out ty2);

        // Calculate radius in tiles
        double r = tx2 - tx1;

        // Create a new array for points
        OnlineMapsVector2d[] points = new OnlineMapsVector2d[segments];

        // Calculate a step
        double step = 360d / segments;

        // Calculate each point of circle
        for (int i = 0; i < segments; i++)
        {
            double px = tx1 + Math.Cos(step * i * OnlineMapsUtils.Deg2Rad) * r;
            double py = ty1 + Math.Sin(step * i * OnlineMapsUtils.Deg2Rad) * r;
            OnlineMaps.instance.projection.TileToCoordinates(px, py, 20, out lng, out lat);
            points[i] = new OnlineMapsVector2d(lng, lat);
        }

        // Create a new polygon to draw a circle
        var circle = OnlineMapsDrawingElementManager.AddItem(new OnlineMapsDrawingPoly(points, lineColor, 0.25f, fillColor));
    }

    public async Task SpawnEntityObstacle(JArray arrayObstacle)
    {
        if (isJArrayNull(arrayObstacle)) return;

        for(int i=0; i < arrayObstacle.Count; i++)
        {
            Debug.Log("Spawn Obstacle " + i);

            var obstacle = EntityObstacle.FromJson(arrayObstacle[i].ToString());
            var info_obstacle = Info_Obstacle.FromJson(obstacle.info_obstacle);

            // Get Posisi Obstacle
            var objPosition = new Vector2(obstacle.lng_x, obstacle.lat_y);

            // Create Marker 3D
            var marker = OnlineMapsMarker3DManager.CreateItem(objPosition, AssetPackageManager.Instance.prefabObstacle);
            marker.instance.name = obstacle.nama;
            marker.instance.tag = "entity-obstacle";

            // Set Ukuran Radar
            marker.sizeType = OnlineMapsMarker3D.SizeType.meters;
            marker.scale =  100;

            // Add & Set Obstacle Properties
            var detail = marker.instance.gameObject.AddComponent<DetailObstacle>();
            detail.marker = marker;

            detail.nama = obstacle.nama;
            detail.warna = (info_obstacle.warna == "blue") ? Color.blue : Color.red;
            detail.size = info_obstacle.size;

            detail.fontTaktis = info_obstacle.font;
            detail.fontFamily = Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + info_obstacle.info);

            // Add Simbol Taktis Component
            var parentHUD = new GameObject("HUD_ELEMENT");
            parentHUD.transform.parent = marker.transform;
            parentHUD.transform.localPosition = Vector3.zero;

            var HUDElement = Instantiate(prefabHUDElement.gameObject, parentHUD.transform).GetComponent<HUDNavigationElement>();

            HUDElement.Prefabs.IndicatorPrefab = Instantiate(HUDElement.Prefabs.IndicatorPrefab, marker.transform);
            HUDElement.Prefabs.IndicatorPrefab.name = "-- simbol taktis --";

            HUDElement.Prefabs.IndicatorPrefab.ChangeFontTaktis(detail.fontTaktis, info_obstacle.info);
            HUDElement.Prefabs.IndicatorPrefab.ChangeFontTaktisColor(detail.warna);
            HUDElement.Prefabs.IndicatorPrefab.ChangeEntityInfo("");
        }
    }

    public async Task SpawnEntityBungus(JArray arrayBungus)
    {
        if (isJArrayNull(arrayBungus)) return;

        for (int i = 0; i < arrayBungus.Count; i++)
        {
            Debug.Log("Spawn Bungus " + i);

            var bungus = EntityBungus.FromJson(arrayBungus[i].ToString());
            var info_bungus = EntityBungusInfo.FromJson(bungus.info_bungus);

            // Get Posisi Obstacle
            var objPosition = new Vector2(bungus.lng, bungus.lat);

            // Create Marker 3D
            var marker = OnlineMapsMarker3DManager.CreateItem(objPosition, AssetPackageManager.Instance.prefabBungus);
            marker.instance.name = bungus.nama;
            marker.instance.tag = "entity-bungus";

            // Add & Set Bungus Properties
            var detail = marker.instance.gameObject.AddComponent<DetailBungus>();
            detail.marker = marker;

            detail.nama = bungus.nama;
            detail.date = DateTime.ParseExact(info_bungus.date[0] + " " + info_bungus.date[1], "MM-dd-yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None);

            // Add & Set Timetrial Properties
            var timetrial = marker.instance.AddComponent<EntityTimetrial>();
            timetrial.startTime = detail.date;
            timetrial.endTime = SkenarioAktif.WaktuAkhir;

            // Add Simbol Taktis Component
            var parentHUD = new GameObject("HUD_ELEMENT");
            parentHUD.transform.parent = marker.instance.transform.GetChild(0);
            parentHUD.transform.localPosition = Vector3.zero;

            var HUDElement = Instantiate(prefabMarker2DElement.gameObject, parentHUD.transform).GetComponent<HUDNavigationElement>();
            HUDElement.tag = "entity-simboltaktis";

            HUDElement.Prefabs.IndicatorPrefab.ChangeIcon(AssetPackageManager.Instance.prefabBungus.GetComponent<MetadataSprite>().FindParameter("icon").parameter);
            HUDElement.Prefabs.IndicatorPrefab.ChangeEntityInfo("");
        }
    }

    public async Task SpawnEntityVideo(JArray arrayVideo)
    {
        if (isJArrayNull(arrayVideo)) return;

        var video_container = VideoKegiatanController.Instance;

        for (int i = 0; i < arrayVideo.Count; i++)
        {
            Debug.Log("Spawn Video " + i);

            var video = EntityVideo.FromJson(arrayVideo[i].ToString());
            var info_video = EntityVideoInfo.FromJson(video.info);

            //Debug.Log(video_container.folders.Any(x => x.Contains(info_video.judul_image)));

            if (!video_container.folders.Contains(info_video.judul_image.ToLower())) continue;

            var objVideo = Instantiate(AssetPackageManager.Instance.prefabVideo, video_container.objContainer);
            objVideo.name = video.nama;

            var detailVideo = objVideo.AddComponent<DetailVideo>();
            detailVideo.nama = video.nama;
            detailVideo.judul = info_video.judul_image.ToLower();
            detailVideo.startTime = DateTime.ParseExact(info_video.waktuMulai, "MM-dd-yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None);
            detailVideo.endTime = DateTime.ParseExact(info_video.waktuAkhir, "MM-dd-yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None);

            // Add & Set Timetrial Properties
            var timetrial = objVideo.AddComponent<EntityTimetrial>();
            timetrial.startTime = detailVideo.startTime;
            timetrial.endTime = detailVideo.endTime;

            timetrial.OnEnterTime = new UnityEngine.Events.UnityEvent();
            timetrial.OnExitTime = new UnityEngine.Events.UnityEvent();

            timetrial.OnEnterTime.AddListener(delegate { video_container.playVideo(detailVideo.judul); });
            timetrial.OnExitTime.AddListener(delegate { video_container.stopVideo(); });
        }
    }

    public async Task SpawnEntityAnimasi(JArray arrayAnimasi)
    {
        if (isJArrayNull(arrayAnimasi)) return;

        for (int i = 0; i < arrayAnimasi.Count; i++)
        {
            Debug.Log("Spawn Animasi " + i);

            var animasi = EntityAnimasi.FromJson(arrayAnimasi[i].ToString());
            var info_animasi = EntityAnimasiInfo.FromJson(animasi.info);

            // Get Posisi Animasi
            var objPosition = new Vector2(animasi.lng, animasi.lat);

            // Create Marker 3D
            var marker = OnlineMapsMarker3DManager.CreateItem(objPosition, AssetPackageManager.Instance.prefabAnimasi);
            marker.instance.name = animasi.nama;
            marker.instance.tag = "entity-animasi";

            // Set Ukuran Satuan
            //marker.sizeType = OnlineMapsMarker3D.SizeType.meters;

            // Add & Set Animasi Properties
            var detail = marker.instance.gameObject.AddComponent<DetailAnimasi>();
            detail.marker = marker;

            detail.nama = animasi.nama;
            detail.urlImage = info_animasi.urlImage;
            detail.kategori = info_animasi.kategori;
            detail.startTime = DateTime.ParseExact(info_animasi.waktuMulai, "MM-dd-yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None);
            detail.endTime = DateTime.ParseExact(info_animasi.waktuMulai, "MM-dd-yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None);

            // Add & Set Timetrial Properties
            var timetrial = marker.instance.AddComponent<EntityTimetrial>();
            timetrial.startTime = detail.startTime;
            timetrial.endTime = detail.endTime;

            if (info_animasi.kategori != null)
            {
                System.Random rnd = new System.Random();
                int n = -99;
                GameObject VFX;

                switch (info_animasi.kategori)
                {
                    case "explosion":
                        n = rnd.Next(0, AnimasiPackage.Instance.animExplosion.Count - 1);
                        VFX = Instantiate(AnimasiPackage.Instance.animExplosion[n], marker.instance.transform.GetChild(0).transform);

                        break;
                    case "penerjunan":
                        n = rnd.Next(0, AnimasiPackage.Instance.animPenerjunan.Count - 1);
                        VFX = Instantiate(AnimasiPackage.Instance.animPenerjunan[n], marker.instance.transform.GetChild(0).transform);
                        break;
                    default:
                        n = rnd.Next(0, AnimasiPackage.Instance.animExplosion.Count - 1);
                        VFX = Instantiate(AnimasiPackage.Instance.animExplosion[n], marker.instance.transform.GetChild(0).transform);
                        break;
                }

                VFX.transform.localScale = new Vector3(25, 25, 25);
                VFX.transform.localPosition = Vector3.zero;
            }
            else
            {
                System.Random rnd = new System.Random();
                int n = -99;

                n = rnd.Next(0, AnimasiPackage.Instance.animExplosion.Count - 1);
                var VFX = Instantiate(AnimasiPackage.Instance.animExplosion[n], marker.instance.transform.GetChild(0).transform);
                VFX.transform.localScale = new Vector3(25, 25, 25);
                VFX.transform.localPosition = Vector3.zero;
            }

            // Add Simbol Taktis Component
            var parentHUD = new GameObject("HUD_ELEMENT");
            parentHUD.transform.parent = marker.instance.transform.GetChild(0);
            parentHUD.transform.localPosition = Vector3.zero;

            var HUDElement = Instantiate(prefabMarker2DElement.gameObject, parentHUD.transform).GetComponent<HUDNavigationElement>();
            HUDElement.tag = "entity-simboltaktis";

            HUDElement.Prefabs.IndicatorPrefab.ChangeIcon(AssetPackageManager.Instance.prefabAnimasi.GetComponent<MetadataSprite>().FindParameter("icon").parameter);
            HUDElement.Prefabs.IndicatorPrefab.ChangeEntityInfo("");
        }
    }

    public async Task SpawnEntityIconCustom(JArray arrayIcon)
    {
        if (isJArrayNull(arrayIcon)) return;

        for (int i = 0; i < arrayIcon.Count; i++)
        {
            Debug.Log("Spawn Icon Custom " + i);

            var icon = EntityIconCustom.FromJson(arrayIcon[i].ToString());
            //var icon_info = EntityIconCustomInfo.FromJson(icon.info_icon_custom);
            var icon_symbol = EntityIconCustomSymbol.FromJson(icon.symbol);

            // Get Posisi Icon
            var objPosition = new Vector2(icon.lng, icon.lat);

            var marker = OnlineMapsMarker3DManager.CreateItem(objPosition, AssetPackageManager.Instance.prefabIconCustom);
            marker.instance.name = icon.nama;
            marker.instance.tag = "entity-iconCustom";

            var detailIcon = marker.instance.AddComponent<DetailIconCustom>();
            detailIcon.nama = icon.nama;
            detailIcon.size = icon.size;
            detailIcon.url = icon_symbol.urlimage;

            var listInfo = icon.info_icon_custom.Replace("[", "").Replace("]", "").Split(",");
            if(listInfo.Length > 0 && icon.info_icon_custom != null)
            {
                for (int j = 0; j < listInfo.Length; j++)
                {
                    detailIcon.info.Add(listInfo[j]);
                }
            }

            // Add Simbol Taktis Component
            var parentHUD = new GameObject("HUD_ELEMENT");
            parentHUD.transform.parent = marker.instance.transform.GetChild(0);
            parentHUD.transform.localPosition = Vector3.zero;

            var HUDElement = Instantiate(prefabMarkerRawElement.gameObject, parentHUD.transform).GetComponent<HUDNavigationElement>();
            HUDElement.tag = "entity-simboltaktis";

            if (icon_symbol.urlimage == null || icon_symbol.urlimage == "")
            {
                HUDElement.Prefabs.IndicatorPrefab.ChangeRawIcon(AssetPackageManager.Instance.prefabIconCustom.GetComponent<MetadataTexture>().FindParameter("default").parameter);
            }
            else
            {
                var textureURL = await API.Instance.GetRemoteTexture(icon_symbol.urlimage);
                if(textureURL == null)
                {
                    HUDElement.Prefabs.IndicatorPrefab.ChangeRawIcon(AssetPackageManager.Instance.prefabIconCustom.GetComponent<MetadataTexture>().FindParameter("default").parameter);
                }
                else
                {
                    HUDElement.Prefabs.IndicatorPrefab.ChangeRawIcon(textureURL);
                }
            }
            HUDElement.Prefabs.IndicatorPrefab.ChangeEntityInfo("");
        }
    }

    public async Task SetDetailSatuan(EntitySatuan satuan, string jenis, EntityTFG entity = null)
    {
        // Request Detail Satuan Dilakukan Jika Data Entity
        string data = "";
        if (entity == null)
        {
            data = await API.Instance.GetDetailSatuan(satuan.id_symbol, jenis);
            if (data == null) return;

            switch (jenis)
            {
                case "1":
                    // Jika Jenis Satuan = Angkatan Darat
                    var detailSatuanDarat = DetailSatuanDarat.FromJson(data);

                    satuan.tipe_tni = detailSatuanDarat.tipe_tni;
                    satuan.jenis = "vehicle";
                    satuan.path_object_3d = detailSatuanDarat.path_object_3d;
                    satuan.object3D = GetObj3DSatuan(detailSatuanDarat.path_object_3d);

                    satuan.alutsista = ObjectDarat.ToString(detailSatuanDarat.OBJ);
                    break;
                case "2":
                    // Jika Jenis Satuan = Angkatan Laut
                    var detailSatuanLaut = DetailSatuanLaut.FromJson(data);

                    satuan.tipe_tni = detailSatuanLaut.tipe_tni;
                    satuan.jenis = "ship";
                    satuan.path_object_3d = detailSatuanLaut.path_object_3d;
                    satuan.object3D = GetObj3DSatuan(detailSatuanLaut.path_object_3d);

                    satuan.alutsista = ObjectLaut.ToString(detailSatuanLaut.OBJ);
                    break;
                case "3":
                    // Jika Jenis Satuan = Angkatan Udara
                    var detailSatuanUdara = DetailSatuanUdara.FromJson(data);

                    satuan.tipe_tni = detailSatuanUdara.tipe_tni;
                    satuan.jenis = "aircraft";
                    satuan.path_object_3d = detailSatuanUdara.path_object_3d;
                    satuan.object3D = GetObj3DSatuan(detailSatuanUdara.path_object_3d);

                    satuan.alutsista = ObjectUdara.ToString(detailSatuanUdara.OBJ);
                    break;
            }
        }
        else
        {
            // Jika Data Berasal dari Colyseus, Maka Langsung dimasukkan tanpa perlu meminta request detail alutsista
            satuan.tipe_tni = entity.tipe_tni;
            satuan.jenis = entity.jenis;
            satuan.path_object_3d = entity.path_object_3d;
            satuan.object3D = entity.object3D;

            satuan.alutsista = entity.alutsista;
        }
    }

    private string GetObj3DSatuan(string path)
    {
        if (string.IsNullOrEmpty(path)) return "";

        var splitDirectory = path.Split("/");
        return splitDirectory[splitDirectory.Length - 1].Split(".FBX")[0];
    }

    private GameObject getAssetSatuan(string nama_satuan, string nama_obj3D, string jenis_satuan)
    {
        Debug.Log(nama_obj3D + " | " + jenis_satuan);
        // Dapatkan Asset 3D Satuan dari Asset Package Berdasarkan Nama Satuan atau Nama Object 3D Satuannya
        int index = AssetPackageManager.Instance.ASSETS_NAME.IndexOf(nama_satuan);
        if(index == -1) index = AssetPackageManager.Instance.ASSETS_NAME.IndexOf(nama_obj3D);

        // Jika Tidak Terdapat Asset 3D dalam Asset Package, Gunakan Asset Default Berdasarkan Jenis Satuannya
        if (index == -1) return jenis_satuan == "vehicle" ? AssetPackageManager.Instance.defaultVehicle :
                                jenis_satuan == "ship" ? AssetPackageManager.Instance.defaultShip :
                                jenis_satuan == "aircraft" ? AssetPackageManager.Instance.defaultAircraft :
                                jenis_satuan == "pasukan" ? AssetPackageManager.Instance.defaultPasukan :
                                AssetPackageManager.Instance.defaultVehicle;

        return AssetPackageManager.Instance.ASSETS[index];
    }

    private string getTagSatuan(string jenis_satuan)
    {
        return jenis_satuan == "vehicle" ? "entity-vehicle" :
               jenis_satuan == "ship" ? "entity-ship" :
               jenis_satuan == "aircraft" ? "entity-aircraft" : "entity-vehicle";
    }

    private int setScaleSatuan(string jenis_satuan)
    {
        switch (jenis_satuan)
        {
            case "vehicle":
                return (Config.VEHICLE_SCALE != null) ? Config.VEHICLE_SCALE.Value : 1;
            case "ship":
                return (Config.SHIP_SCALE != null) ? Config.SHIP_SCALE.Value : 1;
            case "aircraft":
                return (Config.AIRCRAFT_SCALE != null) ? Config.AIRCRAFT_SCALE.Value : 1;
            default:
                return (Config.VEHICLE_SCALE != null) ? Config.VEHICLE_SCALE.Value : 1;
        }
    }

    private void SetDataSatuan(GameObject marker, EntitySatuan satuan, EntitySatuanStyle style, EntitySatuanInfo info)
    {
        var obj = marker.GetComponent<DetailSatuan>();

        obj.OBJ = obj.gameObject;
        //obj.icon = "";
        // obj.icon = marker.GetComponent<AlutsistaBundleData>().image;
        obj.fontTaktis = ((char)style.index).ToString();
        obj.fontFamily = Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + style.nama);

        obj.id = satuan.nama;
        obj.nama = info.nama_satuan;
        obj.tipeTNI = (satuan.tipe_tni == "darat") ? DetailSatuan.TNI.AD : (satuan.tipe_tni == "laut") ? DetailSatuan.TNI.AL : (satuan.tipe_tni == "udara") ? DetailSatuan.TNI.AU : DetailSatuan.TNI.AD;
        obj.tipeKendaraan = (satuan.tipe_tni == "darat") ? DetailSatuan.type.Darat : (satuan.tipe_tni == "laut") ? DetailSatuan.type.Laut : (satuan.tipe_tni == "udara") ? DetailSatuan.type.Udara : DetailSatuan.type.Darat;
        obj.jenisKendaraan = DetailSatuan.jenis.Surface;
        obj.warna = info.warna == "blue" ? Color.blue : info.warna == "red" ? Color.red : Color.black;
        obj.path_obj_3d = satuan.path_object_3d;
        obj.obj3D = satuan.object3D;
        obj.skala = 1;

        obj.coordinate = new Vector2(satuan.lng, satuan.lat);
        //obj.kecepatan = 0;
        obj.heading = info.heading;

        if (marker.GetComponent<EntityWalker>() == null) return;
        if (marker.GetComponent<EntityWalker>().rute.Count <= 0) return;

        obj.misiBerjalan = marker.GetComponent<EntityWalker>().rute[0];
    }

    public void toggleSimbolTaktis(bool toggle)
    {
        foreach (var parent in GameObject.FindGameObjectsWithTag("entity-simboltaktis"))
        {
            foreach (Transform childElement in parent.transform)
            {
                childElement.gameObject.SetActive(toggle);
            }
        }
    }

    public void toggleSimbolTaktisLabel(bool toggle)
    {
        toggleSimbolTaktis(false);

        foreach (var parent in GameObject.FindGameObjectsWithTag("entity-simboltaktis"))
        {
            parent.transform.parent.Find("-- simbol taktis --").GetComponent<HNSIndicatorPrefab>().OnScreenEntityInfo.gameObject.SetActive(toggle);
        }

        toggleSimbolTaktis(true);
    }

    public void toggleEntity(string tag)
    {
        foreach (var parent in GameObject.FindGameObjectsWithTag(tag))
        {
            foreach (Transform childElement in parent.transform)
            {
                childElement.gameObject.SetActive(childElement.gameObject.activeSelf ? false : true);
            }
        }
    }

    public JArray setJArrayResult(JArray arrayResult, int index)
    {
        if (arrayResult[index].ToString() != "False")
        {
            return JArray.Parse(arrayResult[index].ToString());
        }
        else
        {
            return null;
        }
    }

    public bool isJArrayNull(JArray array)
    {
        if (array == null)
        {
            return true;
        }
        else
        {
            if (array.Count == 0) return true;
        }

        return false;
    }
}
