using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

using Newtonsoft.Json.Linq;
using SickscoreGames.HUDNavigationSystem;

using HelperDataAlutsista;
using HelperPlotting;
using GlobalVariables;
using HelperEditor;
using TMPro;
using UnityEngine.UI;
using System;
using ObjectStat;
using Plot.Pasukan;
using Newtonsoft.Json;
using Plot.FormasiPlot;
using System.Globalization;
using System.Linq;

//Functions
using static ListOfSimpleFunctions.Functions;
using static Plot.FormasiPlot.PerhitunganFormasiV2;
using Plot.Obstacle;
using Plot.Drawings.Stat;

public class EntityEditorController : MonoBehaviour
{
    [Header("HUD Navigation")]
    public HUDNavigationElement prefabHUDElement;
    public HUDNavigationElement prefabMarker2DElement;

    [Header("Data Entity")]
    public List<JSONDataSatuan> PLOT_SATUAN = new List<JSONDataSatuan>();

    private GameController _controller;
    private EntityRouteController _route;

    [Header("Delete Plot")]
    public List<JObject> plotTrash;

    #region INSTANCE
    public static EntityEditorController Instance;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    private void Start()
    {
        _route = FindObjectOfType<EntityRouteController>();
        _controller = FindObjectOfType<GameController>();
        plotTrash = new List<JObject>();
    }

    public async Task LoadEntityFromCB(string id_user = null)
    {
        // Load Data dari Database
        var plotting = await API.Instance.LoadDataCB(false, id_user, CBSendiri.id_kogas, CBSendiri.nama_document); 

        // Spawn Entity Satuan
        await SpawnEntitySatuan(setJArrayResult(plotting, 0));
        // await SpawnEntityPasukan(setJArrayResult(plotting, 0));
        await SpawnEntityPasukanV2(setJArrayResult(plotting, 0));

        // Spawn Entity Kekuatan
        await SpawnEntityKekuatan(setJArrayResult(plotting, 1));

        // Spawn Entity Situasi
        await SpawnEntitySituasi(setJArrayResult(plotting, 2));

        // Spawn Entity SRoute

        // Spawn Entity Bungus
        await SpawnEntityBungus(setJArrayResult(plotting, 5));

        // Spawn Entity Passen

        // Spawn Entity Radar
        // await SpawnEntityRadar(setJArrayResult(plotting, 7));

        // Spawn Entity Logis
        await SpawnEntityLogistik(setJArrayResult(plotting, 8));

        // Spawn Entity Formasi
        // await SpawnEntityFormasi(setJArrayResult(plotting, 9));
        await SpawnEntityFormasiV2(setJArrayResult(plotting, 9));

        // Spawn Entity Tool
        await SpawnEntityTools(setJArrayResult(plotting, 10));

        // Spawn Entity Obstacle
        // await SpawnEntityObstacle(setJArrayResult(plotting, 3));
        await SpawnEntityObstacleV2(setJArrayResult(plotting, 3));

        // Spawn Entity Text
        await SpawnEntityText(setJArrayResult(plotting, 11));

        // Spawn Entity Icon Custom

        // Spawn Entity Mission
        await SpawnMisiSatuan(setJArrayResult(plotting, 13), id_user);

        // Spawn Entity Animasi

        // Spawn Entity Video

        // Spawn Entity Radar Simulasi

        // Spawn Entity Faskes
    }

    public async Task SpawnEntityLogistik(float lng, float lat, GameObject prefabLogistik, InfoLogistik info_logistik, List<IsiLogistik> isi_logistik, string jenis, string warna, string symbol = null, string nama = null)
    {
        // Get Posisi Marker
        var objPosition = new Vector2(lng, lat);

        // Create Marker 3D
        var marker = OnlineMapsMarker3DManager.CreateItem(objPosition, prefabLogistik);
        marker.instance.tag = "entity-logistik";
        marker.instance.name = nama;
        marker["markerSource"] = marker;
        marker.OnClick += PlotLogistikController.Instance.OnMarkerClick;

        // Set Data Marker
        var marker_data = marker.instance.AddComponent<JSONDataLogistik>();
        marker_data.id_user = (int)SessionUser.id;
        marker_data.dokumen = SkenarioAktif.ID_DOCUMENT;
        marker_data.nama = nama;
        marker_data.lat_y = lat;
        marker_data.lng_x = lng;
        marker_data.info_logistik = InfoLogistik.ToString(info_logistik);
        marker_data.isi_logistik = IsiLogistik.ToString(isi_logistik);
        marker_data.jenis = jenis;
        marker_data.warna = warna;

        // Create Simbol Taktis
        var HUDElement = CreateHUDElement(marker, prefabMarker2DElement);

        HUDElement.Prefabs.IndicatorPrefab.ChangeIcon(prefabLogistik.GetComponent<MetadataSprite>().FindParameter("icon").parameter);
        HUDElement.Prefabs.IndicatorPrefab.ChangeEntityInfo("");
    }

    public async Task<bool> SpawnEntityLogistik(JArray arrayLogistik)
    {
        if (isJArrayNull(arrayLogistik)) return false;

        foreach (var l in arrayLogistik)
        {
            ListLogistik logistik = ListLogistik.FromJson(l.ToString());
            InfoLogistik info = InfoLogistik.FromJson(logistik.info_logistik);
            List<IsiLogistik> isi = IsiLogistik.FromJson(logistik.isi_logistik);
            await SpawnEntityLogistik(logistik.lng_x, logistik.lat_y, PlotLogistikController.Instance.prefabLogistik[0], info, isi, logistik.jenis, logistik.warna, logistik.symbol, logistik.nama);
        }

        return false;
    }

    //public async Task SpawnEntityText(float lng, float lat, GameObject prefabText, SymbolText symbol, string info_text = null)
    //{
    //    // Get Posisi Marker
    //    var objPosition = new Vector2(lng, lat);

    //    // Create Marker 3D
    //    var marker = OnlineMapsMarker3DManager.CreateItem(objPosition, prefabText);
    //    marker.instance.name = "TEXT_NEW";
    //    marker["markerSource"] = marker;

    //    // Set Data Marker
    //    var marker_data = marker.instance.AddComponent<JSONDataText>();
    //    marker_data.id_user = (int)SessionUser.id;
    //    marker_data.dokumen = SkenarioAktif.ID_DOCUMENT;
    //    marker_data.lat_y = lat;
    //    marker_data.lng_x = lng;
    //    marker_data.info_text = info_text;
    //    marker_data.symbol = SymbolText.ToString(symbol);
    //    marker.instance.transform.Rotate(0, float.Parse(symbol.angel) - 180, 0, Space.Self);

    //    var objText = marker.instance.GetComponent<Metadata>().FindParameter("text").parameter.GetComponent<TextMeshProUGUI>();
    //    objText.gameObject.GetComponent<Button>().onClick.AddListener(delegate { PlotTextController.Instance.OnMarkerClick(marker); });
    //    objText.text = symbol.text;
    //    objText.fontSize = float.Parse(symbol.size);

    //    Color objColor;
    //    ColorUtility.TryParseHtmlString(symbol.warna, out objColor);
    //    objText.color = objColor;
    //}

    public async Task<bool> SpawnEntityTools(JArray arrayTools)
    {
        if (isJArrayNull(arrayTools)) return false;

        int indexLoop = (arrayTools != null) ? arrayTools.Count : 2;

        for (int i = 0; i < indexLoop; i++)
        {
            Debug.Log("Spawn Tools " + i);

            var tools = EntityTools.FromJson(arrayTools[i].ToString());
            var properties_tools = EntityToolsProperties.FromJson(tools.properties);

            switch (tools.type)
            {
                case "LineString":
                    var geometriLine = EntityToolsGeometryLine.FromJson(tools.geometry);
                    var colorPolyline = properties_tools.color == "blue" ? Color.blue : Color.red;
                    colorPolyline.a = properties_tools.opacity;

                    List<Vector2> coords_line = new List<Vector2>();
                    for (int j = 0; j < geometriLine.coordinates.Count; j++)
                    {
                        coords_line.Add(new Vector2((float)geometriLine.coordinates[j][0], (float)geometriLine.coordinates[j][1]));
                    }

                    var line = OnlineMapsDrawingElementManager.AddItem(new OnlineMapsDrawingLine(coords_line, colorPolyline, (properties_tools.weight >= 1) ? properties_tools.weight * 0.05f : properties_tools.weight));
                    line.DrawOnTileset(OnlineMapsTileSetControl.instance, OnlineMapsDrawingElementManager.instance.Count - 1);
                    line.instance.transform.localPosition = new Vector3(0f, 1f, 0f);

                    var dsline = line.instance.AddComponent<DrawStat>();
                    dsline.userID = tools.id_user;
                    dsline.polyName = tools.nama;
                    dsline.type = DrawStat.polyType.LineString;
                    dsline.color = properties_tools.color;
                    dsline.points = coords_line;
                    dsline.geometry = tools.geometry;
                    dsline.properties = tools.properties;

                    line.OnClick += DrawPolyLine.Instance.onLineClick;
                    line.instance.tag = "draw-polyline";
                    line.name = tools.nama;
                    DrawingManager.Instance.lines.Add((OnlineMapsDrawingLine)line);

                    break;
                case "Polygon":
                    var geometriPolygon = EntityToolsGeometryPolygon.FromJson(tools.geometry);
                    var entityPolyV2 = EntityToolV2.FromJson(arrayTools[i].ToString());

                    List<Vector2> coords_polygon = new List<Vector2>();
                    for (int j = 0; j < geometriPolygon.coordinates[0].Count; j++)
                    {
                        if (tools.nama.Contains("arrow_") || tools.nama.Contains("Arrow_") || tools.nama.Contains("Curve_") || tools.nama.Contains("arrow_"))
                        {
                            if (properties_tools.jenisArrow == "arrow")
                            {
                                if (geometriPolygon.coordinates[0].Count == 7)
                                    coords_polygon.Add(new Vector2((float)geometriPolygon.coordinates[0][j][0], (float)geometriPolygon.coordinates[0][j][1]));
                                else
                                    {
                                        if (j > 0)
                                        coords_polygon.Add(new Vector2((float)geometriPolygon.coordinates[0][j][0], (float)geometriPolygon.coordinates[0][j][1]));
                                    }
                            }
                            else if (geometriPolygon.coordinates[0].Count == 343)
                                coords_polygon.Add(new Vector2((float)geometriPolygon.coordinates[0][j][0], (float)geometriPolygon.coordinates[0][j][1]));
                            else
                                {
                                    if (j > 0)
                                    coords_polygon.Add(new Vector2((float)geometriPolygon.coordinates[0][j][0], (float)geometriPolygon.coordinates[0][j][1]));
                                }
                        }
                        else
                            coords_polygon.Add(new Vector2((float)geometriPolygon.coordinates[0][j][0], (float)geometriPolygon.coordinates[0][j][1]));
                    }
                    

                    var colorPolygon = properties_tools.color == "blue" ? Color.blue : Color.red;
                    var colorPolygonInside = properties_tools.fillColor == "blue" ? Color.blue : Color.red;

                    if (properties_tools.color != "blue" && properties_tools.color != "red")
                        ColorUtility.TryParseHtmlString(properties_tools.color, out colorPolygonInside);

                    if (properties_tools.fillColor != "blue" && properties_tools.fillColor != "red")
                        ColorUtility.TryParseHtmlString(properties_tools.fillColor, out colorPolygonInside);

                    colorPolygon.a = (properties_tools.opacity > 0) ? properties_tools.opacity : 1;
                    colorPolygonInside.a = (properties_tools.fillOpacity > 0) ? properties_tools.fillOpacity : 0.5f;

                    var polygon = new OnlineMapsDrawingPoly(coords_polygon, colorPolygon, (properties_tools.weight >= 1) ? properties_tools.weight * 0.05f : properties_tools.weight, colorPolygonInside);

                    if(tools.nama.Contains("arrow_"))
                    if(properties_tools.jenisArrow == "arrowManuverBawah" || properties_tools.jenisArrow == "arrowManuverAtas")
                    polygon = new OnlineMapsDrawingPoly(coords_polygon, colorPolygon, (properties_tools.weight >= 1) ? properties_tools.weight * 0.05f : properties_tools.weight);

                    if (tools.nama.Contains("rectangle_")) polygon.polyType = DrawStat.polyType.Square;
                    else if (tools.nama.Contains("arrow_"))
                    {
                        if (properties_tools.jenisArrow == "arrowManuverBawah") polygon.polyType = DrawStat.polyType.Arrow_Maneuver_Bottom;
                        else if (properties_tools.jenisArrow == "arrowLengkung") polygon.polyType = DrawStat.polyType.Arrow_Curve_Bottom;
                        else if (properties_tools.jenisArrow == "arrowLengkungAtas") polygon.polyType = DrawStat.polyType.Arrow_Curve_Top;
                        else if (properties_tools.jenisArrow == "arrowManuverAtas") polygon.polyType = DrawStat.polyType.Arrow_Maneuver_Top;
                        else polygon.polyType = DrawStat.polyType.Arrow;
                    }
                    else polygon.polyType = DrawStat.polyType.Polygon;

                    var poly = OnlineMapsDrawingElementManager.AddItem(polygon);
                    poly.DrawOnTileset(OnlineMapsTileSetControl.instance, OnlineMapsDrawingElementManager.instance.Count - 1);

                    poly.instance.tag = "draw-polygon";
                    poly.instance.transform.localPosition = new Vector3(0f, 1f, 0f);
                    poly.name = tools.nama;
                    var dspoly = poly.instance.AddComponent<DrawStat>();
                    dspoly.userID = tools.id_user;
                    dspoly.polyName = tools.nama;
                    dspoly.type = polygon.polyType;
                    dspoly.color = properties_tools.color;
                    dspoly.points = coords_polygon;
                    dspoly.geometry = tools.geometry;
                    dspoly.properties = tools.properties;
                    dspoly.drawType = geometriPolygon.type;

                    var dSPolyV2 = poly.instance.AddComponent<DrawStatV2>();
                    dSPolyV2.entity = entityPolyV2;

                    if (!tools.nama.Contains("polygon_"))
                    {
                        poly.OnClick += DrawArrow.instance.OnElementClick;
                        poly.OnPress += DrawArrow.instance.OnElementPress;
                        poly.OnRelease += DrawArrow.instance.OnElementRelease;
                        DrawArrow.instance.drawingList.Add(polygon);
                    }
                    else
                    {
                        poly.OnClick += DrawPolygon.Instance.onPolyClick;
                        DrawingManager.Instance.polygons.Add(polygon);

                        // PlotObstacleController.instance.m++;
                    }

                    Debug.Log(tools.properties);
                    break;
                case "circle":

                    var geometri_circle = EntityToolsGeometryCircle.FromJson(tools.geometry);
                    var p1 = new OnlineMapsVector2d(geometri_circle.coordinates[0], geometri_circle.coordinates[1]);
                    var radiusKM = properties_tools.radius / 1000;

                    // Get the coordinates
                    double lng, lat;
                    lng = p1.x;
                    lat = p1.y;

                    var points = new ArrowManeuver().DrawCircle(lng, lat, radiusKM);

                    var colorCircle = properties_tools.color == "blue" ? Color.blue : Color.red;
                    var colorCircleInside = properties_tools.fillColor == "blue" ? Color.blue : Color.red;

                    if (properties_tools.color != "blue" && properties_tools.color != "red")
                        ColorUtility.TryParseHtmlString(properties_tools.color, out colorCircle);

                    if (properties_tools.fillColor != "blue" && properties_tools.fillColor != "red")
                        ColorUtility.TryParseHtmlString(properties_tools.fillColor, out colorCircleInside);

                    colorCircle.a = (properties_tools.opacity > 0) ? properties_tools.opacity : 1;
                    colorCircleInside.a = (properties_tools.fillOpacity > 0) ? properties_tools.fillOpacity : 0.5f;

                    var circlePoly = new OnlineMapsDrawingPoly(points, colorCircle, (properties_tools.weight >= 1) ? properties_tools.weight * 0.05f : properties_tools.weight, colorCircleInside);
                    circlePoly.polyType = DrawStat.polyType.Circle;
                    circlePoly.radius = properties_tools.radius;

                    var circle = OnlineMapsDrawingElementManager.AddItem(circlePoly);
                    circle.DrawOnTileset(OnlineMapsTileSetControl.instance, OnlineMapsDrawingElementManager.instance.Count - 1);
                    circle.name = tools.nama;
                    circle.instance.tag = "draw-circle";
                    circle.OnClick += DrawArrow.instance.OnElementClick;
                    circle.OnPress += DrawArrow.instance.OnElementPress;
                    circle.OnRelease += DrawArrow.instance.OnElementRelease;
                    circle.instance.transform.localPosition = new Vector3(0f, 1f, 0f);

                    DrawingManager.Instance.polygons.Add((OnlineMapsDrawingPoly)circle);

                    DrawArrow.instance.drawingList.Add(circle);
                    break;
            }
        }
        OnlineMaps.instance.Redraw();

        return false;
    }

    private void DrawEntityToolCircle(double lng, double lat, float radius, Color lineColor, Color fillColor, int segments = 32)
    {
        // Get the coordinate at the desired distance
        double nlng, nlat;
        OnlineMapsUtils.GetCoordinateInDistance(lng, lat, radius, 90, out nlng, out nlat);

        double tx1, ty1, tx2, ty2;
        // Convert the coordinate under cursor to tile position
        OnlineMaps.instance.projection.CoordinatesToTile(lng, lat, 20, out tx1, out ty1);

        // Convert remote coordinate to tile position
        OnlineMaps.instance.projection.CoordinatesToTile(nlng, nlat, 20, out tx2, out ty2);

        // Calculate radius in tiles
        double r = tx2 - tx1;

        // Create a new array for points
        OnlineMapsVector2d[] points = new OnlineMapsVector2d[segments];

        // Calculate a step
        double step = 360d / segments;

        // Calculate each point of circle
        for (int i = 0; i < segments; i++)
        {
            double px = tx1 + Math.Cos(step * i * OnlineMapsUtils.Deg2Rad) * r;
            double py = ty1 + Math.Sin(step * i * OnlineMapsUtils.Deg2Rad) * r;
            OnlineMaps.instance.projection.TileToCoordinates(px, py, 20, out lng, out lat);
            points[i] = new OnlineMapsVector2d(lng, lat);
        }

        // Create a new polygon to draw a circle
        var circle = OnlineMapsDrawingElementManager.AddItem(new OnlineMapsDrawingPoly(points, lineColor, 0.25f, fillColor));
        DrawingManager.Instance.polygons.Add((OnlineMapsDrawingPoly)circle);
    }

    public async Task<bool> SpawnEntityText(JArray arrayText)
    {
        if (isJArrayNull(arrayText)) return false;

        int indexLoop = (arrayText != null) ? arrayText.Count : 2;

        for (int i = 0; i < indexLoop; i++)
        {
            var text = EntityText.FromJson(arrayText[i].ToString());
            var symbol_text = SymbolText.FromJson(text.symbol);

            // Get Posisi Satuan
            var objPosition = new Vector2(text.lng, text.lat);

            // Create Marker 3D Berdasarkan Object 3D-nya
            var marker = OnlineMapsMarker3DManager.CreateItem(objPosition, AssetPackageManager.Instance.prefabText);
            marker.scale = 1f;
            marker.ifText = true;
            marker.instance.name = text.nama;

            // Add & Set JSON Properties
            await SetJSONTextAndHUD(marker, symbol_text, text.info_text);
            
            // Scale Marker Based On Zoom
            if(symbol_text.zoom != null)
            {
                var diffZoom = (float)(OnlineMaps.instance.zoom - symbol_text.zoom);
                marker.scale *= MathF.Pow(2, diffZoom);
            }
        }

        return false;
    }

    public async Task SetJSONTextAndHUD(OnlineMapsMarker3D marker, SymbolText symbol_text, string info_text)
    {
        double lat, lng;
        marker.GetPosition(out lng, out lat);

        marker["markerSource"] = marker;
        marker.OnClick += PlotTextController.Instance.OnMarkerClick;

        // Set Data Marker
        var marker_data = marker.instance.AddComponent<JSONDataText>();
        marker_data.nama = marker.instance.name;
        marker_data.id_user = (int)SessionUser.id;
        marker_data.dokumen = SkenarioAktif.ID_DOCUMENT;
        marker_data.lat_y = lat;
        marker_data.lng_x = lng;
        marker_data.info_text = info_text;
        marker_data.symbol = SymbolText.ToString(symbol_text);
        marker_data.originZoom = symbol_text.zoom;
        marker_data.marker = marker;
        marker.instance.transform.Rotate(0, float.Parse(symbol_text.angel) - 180, 0, Space.Self);
        
        var _text = GameObject.Find("TextMarker");
        if(_text == null)
        {
            _text = new GameObject("TextMarker");
            _text.transform.parent = OnlineMaps.instance.transform;
            _text.transform.localPosition = Vector3.zero;
            _text.transform.localRotation = Quaternion.identity;
            _text.transform.localScale = Vector3.one;
        }
        marker.instance.transform.parent = _text.transform;
        // marker.transform.position = Vector3.zero;

        var objText = marker.instance.GetComponent<Metadata>().FindParameter("text").parameter.GetComponent<TextMeshProUGUI>();
        objText.gameObject.GetComponent<Button>().onClick.AddListener(delegate { PlotTextController.Instance.OnMarkerClick(marker); });
        objText.text = symbol_text.text;
        objText.fontSize = float.Parse(symbol_text.size);

        Color objColor;
        ColorUtility.TryParseHtmlString(symbol_text.warna, out objColor);
        objText.color = objColor;
    }

    //public async Task SpawnEntitySituasi(float lng, float lat, GameObject prefabSituasi, InfoSituasi info_situasi, string symbol = null)
    //{
    //    // Get Posisi Marker
    //    var objPosition = new Vector2(lng, lat);

    //    // Create Marker 3D
    //    var marker = OnlineMapsMarker3DManager.CreateItem(objPosition, prefabSituasi);
    //    marker.instance.name = "SITUASI_NEW";
    //    marker["markerSource"] = marker;
    //    marker.OnClick += PlotSituasiController.Instance.OnMarkerClick;

    //    // Set Data Marker
    //    var marker_data = marker.instance.AddComponent<JSONDataSituasi>();
    //    marker_data.id_user = (int)SessionUser.id;
    //    marker_data.dokumen = SkenarioAktif.ID_DOCUMENT;
    //    marker_data.lat_y = lat;
    //    marker_data.lng_x = lng;
    //    marker_data.info_situasi = InfoSituasi.ToString(info_situasi);
    //    marker_data.symbol_situasi = symbol;

    //    // Create Simbol Taktis
    //    var HUDElement = CreateHUDElement(marker, prefabMarker2DElement);

    //    HUDElement.Prefabs.IndicatorPrefab.ChangeIcon(prefabSituasi.GetComponent<MetadataSprite>().FindParameter("icon").parameter);
    //    HUDElement.Prefabs.IndicatorPrefab.ChangeEntityInfo("");
    //}

    //public async Task SpawnEntityBungus(float lng, float lat, string nama, GameObject prefabBungus, InfoBungus info, string symbol = null)
    //{
    //    // Get Posisi Marker
    //    var objPosition = new Vector2(lng, lat);

    //    // Create Marker 3D
    //    var marker = OnlineMapsMarker3DManager.CreateItem(objPosition, prefabBungus);
    //    marker.instance.name = nama;
    //    marker["markerSource"] = marker;
    //    marker.OnClick += PlotBungusController.Instance.OnMarkerClick;

    //    // Set Data Marker
    //    var marker_data = marker.instance.AddComponent<JSONDataBungus>();
    //    marker_data.id_user = (int)SessionUser.id;
    //    marker_data.dokumen = SkenarioAktif.ID_DOCUMENT;
    //    marker_data.lat_y = lat;
    //    marker_data.lng_x = lng;

    //    marker_data.info_bungus = InfoBungus.ToString(info);
    //    marker_data.symbol = symbol;

    //    // Create Simbol Taktis
    //    var HUDElement = CreateHUDElement(marker, prefabMarker2DElement, true);

    //    HUDElement.Prefabs.IndicatorPrefab.ChangeIcon(prefabBungus.GetComponent<MetadataSprite>().FindParameter("icon").parameter);
    //    HUDElement.Prefabs.IndicatorPrefab.ChangeEntityInfo("");
    //}

    public async Task<bool> SpawnEntityBungus(JArray arrayBungus)
    {
        if (isJArrayNull(arrayBungus)) return false;

        int indexLoop = (arrayBungus != null) ? arrayBungus.Count : 2;

        for (int i = 0; i < indexLoop; i++)
        {
            var bungus = EntityBungus.FromJson(arrayBungus[i].ToString());
            var info_bungus = InfoBungus.FromJson(bungus.info_bungus);

            // Get Posisi Satuan
            var objPosition = new Vector2(bungus.lng, bungus.lat);

            // Create Marker 3D Berdasarkan Object 3D-nya
            var marker = OnlineMapsMarker3DManager.CreateItem(objPosition, AssetPackageManager.Instance.prefabSituasi);
            marker.instance.name = bungus.nama;

            // Add & Set JSON Properties
            await SetJSONBungusAndHUD(marker, bungus.nama, info_bungus, bungus.symbol);
        }

        return false;
    }

    public async Task SetJSONBungusAndHUD(OnlineMapsMarker3D marker, string nama, InfoBungus info, string symbol)
    {
        double lat, lng;
        marker.GetPosition(out lng, out lat);

        marker["markerSource"] = marker;
        marker.instance.tag = "entity-bungus";
        marker.OnClick += PlotBungusController.Instance.OnMarkerClick;

        // Set Data Marker
        var marker_data = marker.instance.AddComponent<JSONDataBungus>();
        marker_data.id_user = (int)SessionUser.id;
        marker_data.dokumen = SkenarioAktif.ID_DOCUMENT;
        marker_data.nama = nama;
        marker_data.lat_y = lat;
        marker_data.lng_x = lng;

        marker_data.info_bungus = InfoBungus.ToString(info);
        marker_data.symbol = symbol;

        // Create Simbol Taktis
        var HUDElement = CreateHUDElement(marker, prefabMarker2DElement, true);

        HUDElement.Prefabs.IndicatorPrefab.ChangeIcon(AssetPackageManager.Instance.prefabBungus.GetComponent<MetadataSprite>().FindParameter("icon").parameter);
        HUDElement.Prefabs.IndicatorPrefab.ChangeEntityInfo("");
    }

    //public async Task SpawnEntityRadar(float lng, float lat, GameObject prefabRadar, ListRadarData listRadar = null, string symbol = null)
    //{
    //    // Get Posisi Marker
    //    var objPosition = new Vector2(lng, lat);

    //    // Create Marker 3D
    //    var marker = OnlineMapsMarker3DManager.CreateItem(objPosition, prefabRadar);
    //    marker.instance.name = "RADAR_NEW";
    //    marker["markerSource"] = marker;
    //    marker.OnClick += PlotRadarController.Instance.OnMarkerClick;

    //    // Set Ukuran Marker 3D
    //    marker.sizeType = OnlineMapsMarker3D.SizeType.meters;
    //    marker.scale = ((float)listRadar.info_radar.radius * 185.2f) * 2f;

    //    // Set Data Marker
    //    var marker_data = marker.instance.GetComponent<JSONDataRadar>();
    //    marker_data.id_user = (int)SessionUser.id;
    //    marker_data.dokumen = SkenarioAktif.ID_DOCUMENT;
    //    marker_data.lat_y = lat;
    //    marker_data.lng_x = lng;

    //    marker_data.info_radar = ListInfoRadar.ToString(listRadar.info_radar);
    //    marker_data.symbol = symbol;

    //    // Create Simbol Taktis
    //    var HUDElement = CreateHUDElement(marker);

    //    HUDElement.Prefabs.IndicatorPrefab.ChangeFontTaktis(listRadar.index, listRadar.fontFamily.name);
    //    HUDElement.Prefabs.IndicatorPrefab.ChangeFontTaktisColor((listRadar.info_radar.warna == "blue" ? Color.blue : Color.red));
    //    HUDElement.Prefabs.IndicatorPrefab.ChangeEntityInfo("");
    //}

    public async Task<bool> SpawnEntityRadar(JArray arrayRadar)
    {
        if (isJArrayNull(arrayRadar)) return false;

        int indexLoop = (arrayRadar != null) ? arrayRadar.Count : 2;

        var ListRadarData = new ListRadarData();

        for (int i = 0; i < indexLoop; i++)
        {
            var radar = EntityRadar.FromJson(arrayRadar[i].ToString());
            var info_radar = ListInfoRadar.FromJson(radar.info_radar);
            info_radar.radius = info_radar.radius;

            // Get Posisi Satuan
            var objPosition = new Vector2(radar.lng, radar.lat);

            // Create Marker 3D Berdasarkan Object 3D-nya
            var marker = OnlineMapsMarker3DManager.CreateItem(objPosition, AssetPackageManager.Instance.prefabRadar);
            marker.instance.name = radar.nama;

            ListRadarData.nama_radar = radar.nama;
            ListRadarData.desc_radar = info_radar.judul;
            ListRadarData.index = info_radar.id_symbol.index;
            ListRadarData.fontFamily = Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + info_radar.id_symbol.nama);
            ListRadarData.info_radar = info_radar;

            // Add & Set JSON Properties
            await SetJSONRadarAndHUD(marker, ListRadarData, radar.symbol, radar.info_symbol);
        }

        return false;
    }

    public async Task SetJSONRadarAndHUD(OnlineMapsMarker3D marker, ListRadarData listRadar, string symbol, string info_symbol)
    {
        double lat, lng;
        marker.GetPosition(out lng, out lat);

        marker["markerSource"] = marker;
        marker.instance.tag = "entity-radar";
        marker.OnClick += PlotRadarController.Instance.OnMarkerClick;

        // Set Ukuran Marker 3D
        marker.sizeType = OnlineMapsMarker3D.SizeType.meters;
        //marker.scale = (float)listRadar.info_radar.radius * 2f;
        //marker.instance.transform.GetChild(0).localScale = new Vector3((float)listRadar.info_radar.radius * 2f, (float)listRadar.info_radar.radius * 2f, (float)listRadar.info_radar.radius * 2f);
        if ((float)listRadar.info_radar.radius <= 0) listRadar.info_radar.radius = 1;
        marker.scale = (float)listRadar.info_radar.radius * 2f;

        // Set Data Marker
        var marker_data = marker.instance.GetComponent<JSONDataRadar>();
        marker_data.id_user = (int)SessionUser.id;
        marker_data.dokumen = SkenarioAktif.ID_DOCUMENT;
        marker_data.nama = listRadar.info_radar.nama;
        marker_data.lat_y = lat;
        marker_data.lng_x = lng;
        marker_data.info_radar = ListInfoRadar.ToString(listRadar.info_radar);
        marker_data.symbol = symbol;
        marker_data.info_symbol = info_symbol;

        // Create Simbol Taktis
        var HUDElement = CreateHUDElement(marker);

        HUDElement.Prefabs.IndicatorPrefab.ChangeFontTaktis(((char)listRadar.index).ToString(), listRadar.fontFamily.name);
        HUDElement.Prefabs.IndicatorPrefab.ChangeFontTaktisColor((listRadar.info_radar.warna == "blue" ? Color.blue : Color.red));
        HUDElement.Prefabs.IndicatorPrefab.ChangeEntityInfo("");
    }

    public async Task<bool> SpawnEntitySituasi(JArray arraySituasi)
    {
        if (isJArrayNull(arraySituasi)) return false;   

        int indexLoop = (arraySituasi != null) ? arraySituasi.Count : 2;

        for (int i = 0; i < indexLoop; i++)
        {
            var situasi = EntitySituasi.FromJson(arraySituasi[i].ToString());
            var info_situasi = InfoSituasi.FromJson(situasi.info_situasi);

            // Get Posisi Satuan
            var objPosition = new Vector2(situasi.lng, situasi.lat);

            // Create Marker 3D Berdasarkan Object 3D-nya
            var marker = OnlineMapsMarker3DManager.CreateItem(objPosition, AssetPackageManager.Instance.prefabSituasi);
            marker.instance.name = situasi.nama;

            // Add & Set JSON Properties
            await SetJSONSituasiAndHUD(marker, situasi.nama, info_situasi, situasi.symbol_situasi);
        }

        return false;
    }

    public async Task SetJSONSituasiAndHUD(OnlineMapsMarker3D marker,string nama, InfoSituasi info_situasi, string symbol)
    {
        double lat, lng;
        marker.GetPosition(out lng, out lat);

        marker["markerSource"] = marker;
        marker.instance.tag = "entity-situasi";
        marker.OnClick += PlotSituasiController.Instance.OnMarkerClick;

        // Set Data Marker
        var marker_data = marker.instance.AddComponent<JSONDataSituasi>();
        marker_data.id_user = (int)SessionUser.id;
        marker_data.dokumen = SkenarioAktif.ID_DOCUMENT;
        marker_data.nama = nama;
        marker_data.lat_y = lat;
        marker_data.lng_x = lng;
        marker_data.info_situasi = InfoSituasi.ToString(info_situasi);
        marker_data.symbol_situasi = symbol;

        // Create Simbol Taktis
        var HUDElement = CreateHUDElement(marker, prefabMarker2DElement, true);

        HUDElement.Prefabs.IndicatorPrefab.ChangeIcon(AssetPackageManager.Instance.prefabSituasi.GetComponent<MetadataSprite>().FindParameter("icon").parameter);
        HUDElement.Prefabs.IndicatorPrefab.ChangeEntityInfo("");
    }

    public async Task<bool> SpawnEntitySatuan(JArray arraySatuan)
    {
        if (isJArrayNull(arraySatuan)) return false;
        if (arraySatuan != null) if (arraySatuan.Count == 0) return false;

        int indexLoop = (arraySatuan != null) ? arraySatuan.Count : 2;
        var es = EntitySatuan.FromJson(arraySatuan[0].ToString());
        if (SkenarioAktif.ID_DOCUMENT.Length < 1) SkenarioAktif.ID_DOCUMENT = es.dokumen;

        for (int i = 0; i < indexLoop; i++)
        {
            var satuan = EntitySatuan.FromJson(arraySatuan[i].ToString());
            var style_satuan = EntitySatuanStyle.FromJson(satuan.style);
            var info_satuan = EntitySatuanInfo.FromJson(satuan.info);

            if(style_satuan.grup != "10")
            {    // Get & Set Detail Satuan
            Debug.Log(style_satuan.grup);
            Debug.Log(satuan.nama);
                await SetDetailSatuan(satuan, style_satuan.grup);

                // Get Posisi Satuan
                var objPosition = new Vector2(satuan.lng, satuan.lat);

                // Create Marker 3D Berdasarkan Object 3D-nya
                var marker = OnlineMapsMarker3DManager.CreateItem(objPosition, getAssetSatuan(info_satuan.nama_satuan, satuan.object3D, satuan.jenis));
                marker["markerSource"] = marker;
                marker.instance.name = satuan.nama;
                marker.transform.parent = CreateMarkerParent("Satuan");
                marker.instance.tag = "entity-satuan";

                marker.OnClick += PlotSatuanController.Instance.EditObject;
                marker.instance.layer = 7;

                // Set Ukuran Satuan
                marker.sizeType = OnlineMapsMarker3D.SizeType.meters;
                marker.scale = 1;

                if (satuan.jenis == "aircraft") marker.altitude = Config.aircraft_alt != null ? (float)Config.aircraft_alt : 250;
                marker.altitudeType = OnlineMapsAltitudeType.absolute;

                // Add & Set JSON Properties
                await SetJSONSatuanAndHUD(marker, satuan, style_satuan, info_satuan);
            }
        }

        return false;
    }

    public async Task SetJSONSatuanAndHUD(OnlineMapsMarker3D marker, EntitySatuan satuan = null, EntitySatuanStyle style_satuan = null, EntitySatuanInfo info_satuan = null, JSONDataSatuan plotData = null, bool replaceIndex = false)
    {
        var marker_data = (plotData == null) ? marker.instance.AddComponent<JSONDataSatuan>() : plotData;
        marker_data.marker = marker;

        if(plotData == null)
        {
            marker_data.id = satuan.id_entity;
            marker_data.id_user = satuan.id_user;
            marker_data.id_symbol = satuan.id_symbol;
            marker_data.dokumen = satuan.dokumen;
            marker_data.nama = satuan.nama;
            marker_data.lat_y = satuan.lat;
            marker_data.lng_x = satuan.lng;
            marker_data.id_kegiatan = satuan.id_kegiatan;
            marker_data.symbol = satuan.symbol;

            marker_data.style = satuan.style;
            marker_data.info = satuan.info;
            marker_data.isi_logistik = satuan.isi_logistik;
        }
        marker.instance.AddComponent<JSONDataMisiSatuan>();
        marker.instance.AddComponent<PFormasiV2>();

        // Set Heading Satuan
        marker.rotation = Quaternion.Euler(0, info_satuan.heading + 180, 0);

        // Add Simbol Taktis Component
        var parentHUD = new GameObject("HUD_ELEMENT");
        parentHUD.transform.parent = marker.transform;
        parentHUD.transform.localPosition = new Vector3(0, marker.scale * 25, 0);

        parentHUD.tag = "entity-simboltaktis";

        var HUDElement = Instantiate(prefabHUDElement.gameObject, parentHUD.transform).GetComponent<HUDNavigationElement>();

        HUDElement.Prefabs.IndicatorPrefab = Instantiate(HUDElement.Prefabs.IndicatorPrefab, marker.transform);
        HUDElement.Prefabs.IndicatorPrefab.name = "-- simbol taktis --";

        string fontIndex = ((char)style_satuan.index).ToString();
        string fontFamily = style_satuan.nama;

        //HUDElement.Indicator.ChangeFontTaktis(fontIndex, fontFamily);
        //HUDElement.Indicator.ChangeFontTaktisColor(info_satuan.warna == "blue" ? Color.blue : info_satuan.warna == "red" ? Color.red : Color.yellow);
        //HUDElement.Indicator.ChangeEntityInfo(info_satuan.nama_satuan);

        HUDElement.Prefabs.IndicatorPrefab.ChangeFontTaktis(fontIndex, fontFamily);
        HUDElement.Prefabs.IndicatorPrefab.ChangeFontTaktisColor(info_satuan.warna == "blue" ? Color.blue : Color.red);
        HUDElement.Prefabs.IndicatorPrefab.ChangeEntityInfo(info_satuan.nama_satuan);

        PLOT_SATUAN.Add(marker.instance.GetComponent<JSONDataSatuan>());
    }

    public async Task SetDetailSatuan(EntitySatuan satuan, string jenis)
    {
        // Request Detail Satuan Dilakukan Jika Data Entity
        string data = await API.Instance.GetDetailSatuan(satuan.id_symbol, jenis);
        if (data == null) return;

        switch (jenis)
        {
            case "1":
                // Jika Jenis Satuan = Angkatan Darat
                var detailSatuanDarat = DetailSatuanDarat.FromJson(data);

                satuan.tipe_tni = detailSatuanDarat.tipe_tni;
                satuan.jenis = "vehicle";
                satuan.path_object_3d = detailSatuanDarat.path_object_3d;
                satuan.id_symbol = detailSatuanDarat.OBJ.symbolID.ToString();
                satuan.object3D = GetObj3DSatuan(detailSatuanDarat.path_object_3d);

                satuan.alutsista = ObjectDarat.ToString(detailSatuanDarat.OBJ);
                break;
            case "2":
                // Jika Jenis Satuan = Angkatan Laut
                var detailSatuanLaut = DetailSatuanLaut.FromJson(data);

                satuan.tipe_tni = detailSatuanLaut.tipe_tni;
                satuan.jenis = "ship";
                satuan.path_object_3d = detailSatuanLaut.path_object_3d;
                satuan.id_symbol = detailSatuanLaut.OBJ.symbolID.ToString();
                satuan.object3D = GetObj3DSatuan(detailSatuanLaut.path_object_3d);

                satuan.alutsista = ObjectLaut.ToString(detailSatuanLaut.OBJ);
                break;
            case "3":
                // Jika Jenis Satuan = Angkatan Udara
                var detailSatuanUdara = DetailSatuanUdara.FromJson(data);

                satuan.tipe_tni = detailSatuanUdara.tipe_tni;
                satuan.jenis = "aircraft";
                satuan.path_object_3d = detailSatuanUdara.path_object_3d;
                satuan.id_symbol = detailSatuanUdara.OBJ.symbolID.ToString();
                satuan.object3D = GetObj3DSatuan(detailSatuanUdara.path_object_3d);

                satuan.alutsista = ObjectUdara.ToString(detailSatuanUdara.OBJ);
                break;
        }
    }

    public async Task<bool> SpawnMisiSatuan(JArray arrayMisi, string id_user = null)
    {
        if (isJArrayNull(arrayMisi)) return false;
        if (arrayMisi != null) if (arrayMisi.Count == 0) return false;

        // ADD MISI
        for (int i = 0; i < arrayMisi.Count; i++)
        {
            Debug.Log("Misi " + (i+1) + " : " + arrayMisi[i].ToString());
            var misi = EntityMisi.FromJson(arrayMisi[i].ToString());

            // Set Property Misi (Jump to Next Index if It's Null)
            if (misi.properties == null) continue;
            var misi_properties = misi.properties;

            if (misi_properties.jalur == null) continue;

            misi_properties.idPrimary = misi.id;
            // Create Route Path Points
            //List<Vector3> paths = new List<Vector3>();
            double jarak_jalur = 0;

            //GameObject go = new GameObject("GO_ROUTE_" + i);
            GameObject go = new GameObject(misi.id_mission);
            go.transform.parent = RouteController.Instance.routeData;
            go.tag = "entity-route";
            PlotDataRoute mRoute = go.AddComponent<PlotDataRoute>();
            mRoute.ID = Int32.Parse(misi.id);
            RouteController.Instance.nPoly = i;

            for (int j = 0; j < misi_properties.jalur.Count; j++)
            {
                //paths.Add(new Vector3(misi_properties.jalur[j].lng, misi_properties.jalur[j].lat, (float)misi_properties.jalur[j].alt));
                if (j > 0)
                {
                    jarak_jalur += OnlineMapsUtils.DistanceBetweenPointsD(new Vector2(misi_properties.jalur[j - 1].lng, misi_properties.jalur[j - 1].lat), new Vector2(misi_properties.jalur[j].lng, misi_properties.jalur[j].lat));
                }

                mRoute.AddNewPos("", new Vector3(misi_properties.jalur[j].lng, misi_properties.jalur[j].lat, (float)misi_properties.jalur[j].alt), j);
            }

            foreach (OnlineMapsMarker3D _m in OnlineMapsMarker3DManager.instance)
            {
                if (misi.id_object == _m.instance.name)
                {
                    var dataMisiSatuan = _m.instance.GetComponent<JSONDataMisiSatuan>();
                    Debug.Log(misi.tgl_mulai);
                    var date = DateTime.ParseExact(misi.tgl_mulai, "MM-dd-yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None);

                    mRoute.id_satuan = _m.instance.name;
                    mRoute.heading_satuan = _m.rotationY;

                    mRoute.routeID = "Route " + i;
                    //mRoute.lines = aLine;
                    mRoute.routeName = misi_properties.nama_misi;
                    mRoute.routeType = misi_properties.typeMisi;
                    mRoute.routeDesc = "-";
                    mRoute.routeSpeed = float.Parse(misi_properties.kecepatan);
                    mRoute.routeDistance = (float) misi_properties.distance;
                    mRoute.routeDistanceUnit = misi_properties.type;
                    mRoute.routeTime = RouteController.Instance.estimateTimeFMin((float)misi_properties.distance, misi_properties.kecepatan, misi_properties.type);
                    mRoute.routeStart = date;
                    mRoute.routeEnd = date.AddMinutes(mRoute.routeTime);
                    mRoute.kegiatanID = misi_properties.id_kegiatan;
                    mRoute.generateProperties();
                    mRoute.id_mission = go.name;

                    dataMisiSatuan.listRoute.Add(mRoute);

                    var pF = _m.instance.GetComponent<PFormasiV2>();
                    for(int j = 0; j < mRoute.data.Count; j++)
                    {
                        var coord = mRoute.data[j].coordinate;
                        pF.routePoints.Add(coord);
                    }
                }
            }
        }

        return false;
    }

    #region IZZAN

    public async Task<bool> SpawnEntityObstacle(JArray arrayObstacle)
    {
        if (isJArrayNull(arrayObstacle)) return false;
        if (arrayObstacle != null) if (arrayObstacle.Count == 0) return false;

        int indexLoop = (arrayObstacle != null) ? arrayObstacle.Count : 2;

        for(int i = 0; i < indexLoop; i++)
        {
            var obstacle = EntityObstacle.FromJson(arrayObstacle[i].ToString());
            var info = Info_Obstacle.FromJson(obstacle.info_obstacle);
            var symbol = SymbolObstacle.FromJson(obstacle.symbol);

            var marker3D = OnlineMapsMarker3DManager.CreateItem(obstacle.lng_x, obstacle.lat_y, PlotObstacleController.instance.prefab);            
            marker3D.scale = info.size / 5;
            marker3D.OnClick += PlotObstacleController.instance.Marker3DOnClick;

            await SetDataObstacle(marker3D, obstacle, info, symbol);
        }

        return false;
    }

    public async Task<bool> SpawnEntityObstacleV2(JArray arrayObs)
    {
        if (isJArrayNull(arrayObs)) return false;
        if (arrayObs != null) if (arrayObs.Count == 0) return false;

        int indexLoop = (arrayObs != null) ? arrayObs.Count : 2;

        var polyBefore = "";
        for(int i = 0; i < indexLoop; i++)
        {
            Debug.Log(arrayObs[i].ToString());
            var entity = EntityObstacleV2.FromJson(arrayObs[i].ToString());
            Debug.Log(entity.nama);

            var split = entity.nama.Split("_");
            var polyName = split[2] + "_" + split[3] + "_" + split[4] + "_" + split[5];
            Debug.Log(polyName);

            var marker = Create3D(entity.lng_x, entity.lat_y, PlotObstacleControllerV2.instance.prefab);
            marker.scale         = entity.info_obstacle.size * 2.5f;
            marker.sizeType      = OnlineMapsMarker3D.SizeType.scene;
            marker.instance.name = entity.nama;
            marker.OnClick       += PlotObstacleControllerV2.instance.OnMarkerClick;

            Component FindPara<Component>(string obj) => marker.instance.GetComponent<Metadata>().FindParameter(obj).parameter.GetComponent<Component>();
            FindPara<Text>("symbol").text  = entity.info_obstacle.font;
            FindPara<Text>("symbol").color = entity.info_obstacle.warna == "blue" ? Color.blue : Color.red;

            Destroy(marker.instance.GetComponent<ObstacleStat>());
            var oS = marker.instance.AddComponent<ObstacleStatV2>();
            oS.entity  = entity;
            oS.polygon = polyName;
            
            var element = OnlineMapsDrawingElementManager.instance.FirstOrDefault(x => x.name == polyName);
            element.instance.transform.parent = CreateMarkerParent("Obstacle Area");

            if(polyBefore != polyName)
            {
                var obsObj = new GameObject("obstacle_area_" + polyName);
                obsObj.transform.parent = CreateMarkerParent("Obstacle Obj");
                
                var obsObjScript = obsObj.AddComponent<ObstacleObj>();
                obsObjScript.obstacleList = new();
                obsObjScript.element      = element;
                
                polyBefore = polyName;
            }
            var obObj = GameObject.Find("obstacle_area_" + polyName);
            var obObjScript = obObj.GetComponent<ObstacleObj>();
            obObjScript.obstacleList.Add(entity);
        }

        return true;
    }

    public async Task SetDataObstacle(OnlineMapsMarker3D marker, EntityObstacle obstacle, Info_Obstacle info, SymbolObstacle symbol)
    {
        var stat = marker.instance.GetComponent<ObstacleStat>();
        stat.nama = obstacle.nama;
        stat.lngLat = new Vector2(obstacle.lng_x, obstacle.lat_y);
        stat.font = info.font;
        stat.warnaInString = info.warna;
        stat.size = info.size;
        stat.info = info.info;
        stat.index = info.index;
        stat.info_obstacle = JsonConvert.SerializeObject(info);
        stat.symbol = JsonConvert.SerializeObject(symbol);

        string[] pisah = stat.nama.Split("_");
        stat.polygonName = pisah[2] + "_" + pisah[3] + "_" + pisah[4] + "_" + pisah [5];

        foreach(OnlineMapsDrawingElement element in OnlineMapsDrawingElementManager.instance)
        if(element.name == stat.polygonName)
        {
            element.OnClick -= DrawPolygon.Instance.onPolyClick;
            element.OnPress += PlotObstacleController.instance.OnPolyPress;
            element.OnRelease += PlotObstacleController.instance.OnPolyRelease;
            element.isObstacleArea = true;
            OnlineMapsDrawingPoly poly = element as OnlineMapsDrawingPoly;

            element.instance.transform.parent = GameObject.Find("Obstacle Area").transform;
            marker.instance.transform.parent = element.instance.transform;
            stat.polygonPoints = poly.points as Vector2[];
        }

        if(stat.warnaInString == "blue")
        {
            stat.warna = ObstacleStat.warnaType.Biru;
            marker.instance.transform.Find("Canvas").GetChild(0).GetComponent<Text>().color = Color.blue;
        }
        else if(stat.warnaInString == "red")
        {
            stat.warna = ObstacleStat.warnaType.Merah;
            marker.instance.transform.Find("Canvas").GetChild(0).GetComponent<Text>().color = Color.red;
        }
    }

    public async Task<bool> SpawnEntityFormasi(JArray arrayFormasi)
    {
        if (isJArrayNull(arrayFormasi)) return false;

        if (arrayFormasi != null) if (arrayFormasi.Count == 0) return false;
        int indexLoop = (arrayFormasi != null) ? arrayFormasi.Count : 2;

        for(int i = 0; i < indexLoop; i++)
        {
            var formasi = EntityFormasi.FromJson(arrayFormasi[i].ToString());
            var info = Info_Formasi.FromJson(formasi.info_formasi);
            var satuan = Satuan_Formasi.FromJson(Satuan_Formasi.ToString(info.satuan_formasi));

            // DITAMBAHKAN IF ELSE KARENA TERKADANG DATA "arrArah" di return list atau false
            if (info.RAWarrArah.Equals(typeof(bool)))
            {
                info.arrArah = new List<float>();
                info.arrArah.Add(0);
            }
            else
            {
                info.arrArah = info.RAWarrArah as List<float>;
            }
            // ------
            
            if(GameObject.Find("FormasiObj") == null)
            new GameObject("FormasiObj");
            var obj = GameObject.Find("FormasiObj").transform;
            var _obj = new GameObject(formasi.nama);
            _obj.transform.parent = obj;
            
            var eF = _obj.AddComponent<EntityFormasiObj>();
            eF.json = EntityFormasi.ToString(formasi);

            PlotFormasiController.instance.indexFormasi++;

            await SetDetailFormasi(formasi, info, satuan);
            foreach(OnlineMapsMarker3D child in OnlineMapsMarker3DManager.instance)
            {
                if(child.instance.GetComponent<PFormasi>() != null)
                {
                    var pFormasi = child.instance.GetComponent<PFormasi>();
                    
                    if(pFormasi.nama == formasi.nama)
                    {
                        pFormasi.listObjekFormasi = new List<Transform>();
                        
                        foreach(var sat in satuan)
                        {
                            var objOnList = GameObject.Find(sat.id_point).GetComponent<PFormasi>();

                            if(objOnList == null) Debug.Log(sat.id_point);
                            pFormasi.listObjekFormasi.Add(objOnList.marker3D.instance.transform);
                        }

                        pFormasi.listObjekFormasi = pFormasi.listObjekFormasi.GroupBy(x => x.name).Select(x => x.First()).ToList();
                        pFormasi.listObjekFormasi.Sort(SortByUrutan);

                        static int SortByUrutan(Transform p1, Transform p2)
                        {
                            return p1.GetComponent<PFormasi>().urutanFormasi.CompareTo(p2.GetComponent<PFormasi>().urutanFormasi);
                        }
                    }
                }
            }
        }        
        PlotFormasiController.instance.selectedUnits = new List<Transform>();

        return false;
    }

    public async Task SetDetailFormasi(EntityFormasi formasi, Info_Formasi info, List<Satuan_Formasi> satuan)
    {
        PlotFormasiController.instance.selectedUnits = new List<Transform>();
        foreach(OnlineMapsMarker3D marker in OnlineMapsMarker3DManager.instance)
        {
            var pFormasi = marker.instance.GetComponent<PFormasi>();

            if(pFormasi != null)
            if(pFormasi.stat != null)
            {
                await SetDataPerSatuanFormasi(pFormasi, formasi, info, satuan, marker, pFormasi.stat.namaSatuan);
            }
            else if(pFormasi.jStat != null)
            {
                await SetDataPerSatuanFormasi(pFormasi, formasi, info, satuan, marker, pFormasi.jStat.nama);
            }
        }
    }

    public async Task SetDataPerSatuanFormasi(PFormasi pFormasi, EntityFormasi formasi, Info_Formasi info, List<Satuan_Formasi> satuan, OnlineMapsMarker3D marker, string namaSatuan)
    {
        for(int i = 0; i < satuan.Count; i++)
        {
            if(namaSatuan == satuan[i].id_point)
            {
                pFormasi.nama = formasi.nama;
                pFormasi.namaFormasi = info.nama_formasi;
                pFormasi.urutanFormasi = i;
                pFormasi.jarak = satuan[i].jarak;
                pFormasi.info = formasi.info_formasi;
                pFormasi.sudut = satuan[i].sudut;

                if(satuan[i].satuan == "nm")
                pFormasi.satuan = PFormasi.satuanJarak.Nautical_Miles;
                else if(satuan[i].satuan == "km")
                pFormasi.satuan = PFormasi.satuanJarak.Kilometer;
                else if(satuan[i].satuan == "foot")
                pFormasi.satuan = PFormasi.satuanJarak.Feet;
                else if(satuan[i].satuan == "yard")
                pFormasi.satuan = PFormasi.satuanJarak.Yard;
                else if(satuan[i].satuan == "m")
                pFormasi.satuan = PFormasi.satuanJarak.Meter;

                if(formasi.jenis_formasi == "1")
                pFormasi.formasi = PFormasi.formasiType.Belah_Ketupat;
                else if(formasi.jenis_formasi == "2")
                pFormasi.formasi = PFormasi.formasiType.Flank_Kanan;
                else if(formasi.jenis_formasi == "3")
                pFormasi.formasi = PFormasi.formasiType.Flank_Kiri;
                else if(formasi.jenis_formasi == "4")
                pFormasi.formasi = PFormasi.formasiType.Sejajar;
                else if(formasi.jenis_formasi == "5")
                pFormasi.formasi = PFormasi.formasiType.Serial;
                else if(formasi.jenis_formasi == "6")
                pFormasi.formasi = PFormasi.formasiType.Panah;
                else if(formasi.jenis_formasi == "7")
                pFormasi.formasi = PFormasi.formasiType.Kerucut;
                else if(formasi.jenis_formasi == "8")
                pFormasi.formasi = PFormasi.formasiType.Custom;

                if(pFormasi.stat != null)
                {
                    var pF2D = pFormasi.marker2D.instance.GetComponent<PFormasi>();
                    pF2D.namaFormasi = pFormasi.namaFormasi;
                    pF2D.nama = pFormasi.nama;
                    pF2D.urutanFormasi = i;
                    pF2D.jarak = pFormasi.jarak;
                    pF2D.info = pFormasi.info;
                    pF2D.satuan = pFormasi.satuan;
                    pF2D.formasi = pFormasi.formasi;
                    pF2D.sudut = pFormasi.sudut;
                }

                var pF3D = pFormasi.marker3D.instance.GetComponent<PFormasi>();
                pF3D.namaFormasi = pFormasi.namaFormasi;
                pF3D.nama = pFormasi.nama;
                pF3D.urutanFormasi = i;
                pF3D.jarak = pFormasi.jarak;
                pF3D.info = pFormasi.info;
                pF3D.satuan = pFormasi.satuan;
                pF3D.formasi = pFormasi.formasi;
                pF3D.sudut = pFormasi.sudut;
            }
        }
        // PlotFormasiController.instance.selectedUnits = new List<Transform>();
    }

    public async Task<bool> SpawnEntityFormasiV2(JArray arrayFormasi)
    {
        if (isJArrayNull(arrayFormasi)) return false;
        if (arrayFormasi != null) if (arrayFormasi.Count == 0) return false;
        
        for(int i = 0; i < arrayFormasi.Count; i++)
        {
            var formasi = EntityFormasiV2.FromJson(arrayFormasi[0].ToString());
            var gameObject = new GameObject(formasi.nama)
            {
                tag = "entity-formasi"
            };
            gameObject.transform.parent = CreateMarkerParent("Data Formasi");

            var dataFormasi = gameObject.AddComponent<DataFormasi>();
            dataFormasi.entity = formasi;

            await SetDataFormasiV2(dataFormasi);
        }

        // Debug.Log(arrayFormasi[0].ToString());
        return true;
    }

    public async Task SetDataFormasiV2(DataFormasi data)
    {
        var entity = data.entity;
        var info = entity.info_formasi;
        var satuanList = info.satuan_formasi;
        var list = new List<Transform>();

        for(int i = 0; i < satuanList.Count; i++)
        {
            var satuan = satuanList[i];
            var marker = OnlineMapsMarker3DManager.instance.FirstOrDefault(x => x.instance.name == satuan.id_point);
            var obj = marker.transform;
            var pF = obj.GetComponent<PFormasiV2>();
            var fS = pF.formasi;

            fS.id = i;
            fS.nama = entity.nama;
            fS.namaFormasi = info.nama_formasi;
            fS.arah = i == 0 ? info.arah : satuan.sudut;
            fS.arahUtama = info.arah;
            fS.type = (FormasiType.formasiType)int.Parse(entity.jenis_formasi);
            fS.satuan = JenisSatuanJarakString(satuan.satuan);
            fS.jarak = float.Parse(satuan.jarak);
            fS.jarakAntarSatuan = fS.jarak;
            list.Add(obj);
            
            foreach(OnlineMapsMarker3D child in OnlineMapsMarker3DManager.instance)
            if(child.instance.name == satuan.id_point)
            child.rotationY += fS.arahUtama;
        }

        for(int i = 0; i < satuanList.Count; i++)
        {
            var satuan = satuanList[i];
            var marker = OnlineMapsMarker3DManager.instance.FirstOrDefault(x => x.instance.name == satuan.id_point);
            var pF = marker.instance.GetComponent<PFormasiV2>();
            var fS = pF.formasi;
            fS.unitsInFormasi = new();
            fS.unitsInFormasi.AddRange(list);
            fS.unitsInFormasi.OrderBy(x => x.GetComponent<PFormasiV2>().formasi.id);
        }
    }

    public async Task<bool> SpawnEntityPasukanV2(JArray arraySatuan)
    {
        
        if (isJArrayNull(arraySatuan)) return false;
        if (arraySatuan != null) if (arraySatuan.Count == 0) return false;

        int indexLoop = (arraySatuan != null) ? arraySatuan.Count : 2;
        
        for(int i = 0; i < indexLoop; i++)
        {
            var entity = EntityPasukanV2.FromJson(arraySatuan[i].ToString());
            var info   = entity.info;
            var style  = entity.style;

            if(style.grup == 10)
            {
                entity.matra     = PlotPasukanControllerV2.instance.listPasukan.Find(child => child.id == entity.id_symbol).matra_pasukan;
                entity.tipe_tni  = entity.matra == "Darat" ? "AD" : entity.matra == "Laut" ? "AL" : "AU";
                
                Debug.Log(arraySatuan[i].ToString());
                Debug.Log(entity.nama);
                var marker              = Create3D(entity.lng_x, entity.lat_y, PlotPasukanControllerV2.instance.pasukanPrefab);
                marker["markerSource"]  = marker;
                marker.label            = info.nama_satuan;
                marker["data"]          = new ShowMarkerLabelsByZoomItem(marker.label, new OnlineMapsRange(1,19));
                marker.sizeType         = OnlineMapsMarker3D.SizeType.meters;
                marker.rotationY        = int.Parse(info.heading);
                marker.instance.name    = entity.nama;
                marker.instance.layer   = 7;
                marker.transform.tag    = "Plot_Pasukan";
                marker.transform.parent = CreateMarkerParent("Pasukan");
                Destroy(marker.instance.GetComponent<Stat>());
                Destroy(marker.instance.GetComponent<PFormasi>());
                
                var pas                 = marker.instance.AddComponent<StatV2>();
                pas.image               = PlotPasukanControllerV2.instance.icon;
                pas.pasukan             = entity;
                pas.pasukan.info        = info;
                pas.pasukan.style       = style;
                pas.json = new()
                {
                    String = EntityPasukanV2.ToString(pas.pasukan)
                };
                pas.markers             = new()
                {
                    marker3D = marker
                };
                
                var removeChar  = (char)92;
                pas.json.String = pas.json.String.Replace(removeChar.ToString(), "");

                removeChar      = (char)34;
                pas.json.String = pas.json.String.Replace(removeChar.ToString() + "[", "[");
                pas.json.String = pas.json.String.Replace("]" + removeChar.ToString(), "]");
                
                //Set HUD untuk marker
                SetHUD(marker, info.nama_satuan, info.warna, style.nama, style.index);
                marker.instance.AddComponent<PFormasiV2>();

                //Add functions to marker
                marker.OnClick      += PlotPasukanControllerV2.instance.OnMarkerClick;
                marker.OnPress      += PlotPasukanControllerV2.instance.OnMarkerPress;
                marker.OnRelease    += PlotPasukanControllerV2.instance.OnMarkerRelease;
            }
        }
        return false;
    }

    public async Task<bool> SpawnEntityPasukan(JArray arraySatuan)
    {

        if (isJArrayNull(arraySatuan)) return false;
        if (arraySatuan != null) if (arraySatuan.Count == 0) return false;

        int indexLoop = (arraySatuan != null) ? arraySatuan.Count : 2;

        List<string> namaPasukan = PlotPasukanController.instance.listNamaPasukan;
        List<string> matraPasukan = PlotPasukanController.instance.listMatraPasukan;
        List<string> keteranganPasList = PlotPasukanController.instance.listKeteranganPasukan;

        for (int i = 0; i < indexLoop; i++)
        {
            PlotPasukanController.instance.indexSatuan++;
            var satuan = EntityPasukan.FromJson(arraySatuan[i].ToString());
            var style_satuan = Style_Pasukan.FromJson(satuan.style);
            var label_style = Label_Style_Pasukan.FromJson(style_satuan.label_style);
            var info_satuan = Info_Pasukan.FromJson(satuan.info);
            
            string fontIndex = ((char)int.Parse(style_satuan.index)).ToString();
            string fontFamily = style_satuan.nama;
            // string lStyle = JsonConvert.SerializeObject(label_style);
            // style_satuan.label_style = lStyle;

            // Get & Set Detail Satuan
            // await SetDetailPasukan(satuan, style_satuan.grup.ToString());

            // Get Posisi Satuan
            var objPosition = new Vector2(satuan.lng_x, satuan.lat_y);

            if(style_satuan.grup == 10)
            {
                var marker = OnlineMapsMarker3DManager.CreateItem(objPosition, PlotPasukanController.instance.soldier2D);
                marker["markerSource"] = marker;
                marker.label = info_satuan.nama_satuan;
                marker["data"] = new PlotPasukanController.ShowMarkerLabelsByZoomItem(marker.label, new OnlineMapsRange(0f, 16f));
                marker.range = new OnlineMapsRange(0f, 16f);
                marker.instance.transform.parent = GameObject.Find("Marker 2D").transform;
                marker.instance.name = satuan.nama;

                var oS = marker.instance.GetComponent<Stat>();
                var pF = marker.instance.GetComponent<PFormasi>();
                await SetDetailPasukan(oS, satuan, style_satuan, info_satuan);

                Transform pasMarker = marker.instance.transform;
                Image pasImage = pasMarker.Find("Canvas/Simbol").GetChild(0).GetComponent<Image>();
                Text pasText = pasImage.transform.GetChild(0).GetComponent<Text>();
                Image pNamaImage = marker.instance.transform.Find("Canvas/Nama").GetChild(0).GetComponent<Image>();
                
                var m_meta = marker.instance.GetComponent<Metadata>();

                m_meta.FindParameter("Simbol").parameter.GetComponent<Text>().text = fontIndex;
                m_meta.FindParameter("Simbol").parameter.GetComponent<Text>().font = Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + fontFamily);
                
                m_meta.FindParameter("Nama").parameter.GetComponent<TMP_Text>().text = info_satuan.nama_satuan;

                if(info_satuan.warna == "blue")
                {
                    pasImage.color = Color.blue;
                    pNamaImage.color = Color.blue;
                }
                else if(info_satuan.warna == "red")
                {
                    pasImage.color = Color.red;
                    pNamaImage.color = Color.red;
                }
                
                var marker3D = OnlineMapsMarker3DManager.CreateItem(objPosition, PlotPasukanController.instance.soldier);
                marker3D["markerSource"] = marker3D;
                marker3D.label = marker.label;
                marker3D.sizeType = OnlineMapsMarker3D.SizeType.meters;
                marker3D.scale = 5f;
                marker3D.range = new OnlineMapsRange(16.5f, 20f);
                marker3D.instance.name = marker.instance.name;
                
                var oSS = marker3D.instance.GetComponent<Stat>();
                if(oSS == null)
                oSS = marker3D.instance.AddComponent<Stat>();

                var pFF = marker3D.instance.GetComponent<PFormasi>();
                if(pFF == null)
                pFF = marker3D.instance.AddComponent<PFormasi>();
                
                var jM = marker3D.instance.GetComponent<JSONDataMisiSatuan>();
                if(jM == null)
                jM = marker3D.instance.AddComponent<JSONDataMisiSatuan>();

                await SetDetailPasukan(oSS, satuan, style_satuan, info_satuan);
                
                marker["markerSource"] = marker;
                marker.OnClick += PlotPasukanController.instance.OnMarkerClick;
                marker.OnPress += PlotPasukanController.instance.OnMarkerPress;
                marker.OnRelease += PlotPasukanController.instance.OnMarkerRelease;
                PlotPasukanController.instance.listMarker2DOnMap.Add(marker);
                
                marker3D["markerSource"] = marker3D;
                marker3D.OnClick += PlotPasukanController.instance.OnMarkerClick;
                marker3D.OnPress += PlotPasukanController.instance.OnMarkerPress;
                marker3D.OnRelease += PlotPasukanController.instance.OnMarkerRelease;

                marker.instance.tag = "Plot_Pasukan";
                marker.instance.layer = LayerMask.NameToLayer("PlayerUnit");
                marker3D.instance.tag = "Plot_Pasukan";
                marker3D.instance.layer = LayerMask.NameToLayer("PlayerUnit");
                marker3D.rotation = Quaternion.Euler(Vector3.up * float.Parse(oS.heading));

                oS.marker2D = marker;
                oS.marker3D = marker3D;

                oSS.marker2D = marker;
                oSS.marker3D = marker3D;

                pF.marker2D = marker;
                pF.marker3D = marker3D;
                
                pFF.stat = oSS;
                pFF.marker2D = marker;
                pFF.marker3D = marker3D;

                // Set Ukuran Satuan
                // marker3D.sizeType = OnlineMapsMarker3D.SizeType.meters;
                marker3D.scale = 1;
            }
            // Add & Set JSON Properties
            // await SetJSONSatuanAndHUD(marker, satuan, style_satuan, info_satuan);
        }

        return false;
    }

    public async Task SetDetailPasukan(Stat stat, EntityPasukan pasukan, Style_Pasukan style, Info_Pasukan info)
    {
        List<string> namaPasukan = PlotPasukanController.instance.listNamaPasukan;
        List<string> matraPasukan = PlotPasukanController.instance.listMatraPasukan;
        
        var list_pasukan = await API.Instance.GetListAlutsista("pasukan");
        var pList = Plot.Pasukan.ListPasukan.FromJson(list_pasukan);
        
        for(int i = 0; i < pList.Count; i++)
        {
            if(info.nama_satuan == pList[i].name)
            {
                stat.matra = pList[i].matra_pasukan;
                stat.listPasukan = pList[i];
            }
        }

        var infoKecepatan = info.kecepatan.Split("|").ToList();

        // Debug.Log(pasukan.id_user);
        stat.userID = int.Parse(pasukan.id_user);
        stat.nama = info.nama_satuan;
        stat.namaSatuan = pasukan.nama;
        stat.kecepatan = infoKecepatan[0];
        stat.satuanKecepatan = infoKecepatan[1];
        stat.fuel = info.bahan_bakar.ToString();
        stat.heading = info.heading;

        stat.keteranganPas = style.keterangan;
        stat.keterangan = info.ket_satuan;
        stat.warna = info.warna;
        stat.type = Stat.unitType.Pasukan;
        stat.font = ((char)int.Parse(style.index)).ToString();
        stat.fontType = style.nama;
        stat.fontTypeID = pasukan.id_symbol;
        stat.style = Style_Pasukan.ToString(style);
        stat.info = Info_Pasukan.ToString(info);
        stat.symbol = pasukan.symbol;

        for(int i = 0; i < matraPasukan.Count; i++)
        {
            if(stat.nama == namaPasukan[i])
            {
                stat.matra = matraPasukan[i];
                stat.index = i;
            }
        }
    }

    public async Task<bool> SpawnEntityKekuatan(JArray arrayKekuatan)
    {
        if (isJArrayNull(arrayKekuatan)) return false;
        if (arrayKekuatan != null) if (arrayKekuatan.Count == 0) return false;

        int indexLoop = (arrayKekuatan != null) ? arrayKekuatan.Count : 2;

        for (int i = 0; i < indexLoop; i++)
        {
            var kekuatan = EntityKekuatan.FromJson(arrayKekuatan[i].ToString());
            var info_kekuatan = Info_Kekuatan.FromJson(kekuatan.info_kekuatan);
            var rincian_kekuatan = Rincian_Kekuatan.FromJson(kekuatan.rincian);
            var property_kekuatan = Property_Kekuatan.FromJson(rincian_kekuatan.property);

            var position = new Vector2(kekuatan.lng_x, kekuatan.lat_y);

            var marker3D = OnlineMapsMarker3DManager.CreateItem(position, PlotKekuatanController.Instance.prefab);
            marker3D["markerSource"] = marker3D;
            marker3D.label = kekuatan.nama;
            marker3D.instance.name = kekuatan.nama;
            marker3D.OnClick += PlotKekuatanController.Instance.OnMarkerClick;
            marker3D.OnPress += PlotKekuatanController.Instance.OnMarkerPress;
            marker3D.OnRelease += PlotKekuatanController.Instance.OnMarkerRelease;
            
            var _kekuatan = GameObject.Find("KekuatanMarker");
            if(_kekuatan == null)
            {
                _kekuatan = new GameObject("KekuatanMarker");
                _kekuatan.transform.parent = OnlineMaps.instance.transform;
                _kekuatan.transform.localPosition = Vector3.zero;
                _kekuatan.transform.localRotation = Quaternion.identity;
                _kekuatan.transform.localScale = Vector3.one;
            }
            marker3D.instance.transform.parent = _kekuatan.transform;

            await SetDetailKekuatan(kekuatan, marker3D.instance, marker3D, info_kekuatan, rincian_kekuatan);
        }

        return false;
    }

    public async Task SetDetailKekuatan(EntityKekuatan kekuatan, GameObject instance, OnlineMapsMarker3D marker3D, Info_Kekuatan[] info, Rincian_Kekuatan rincian)
    {
        Color kColor = new Color(0, 0, 0, 0);

        if (rincian.warna == "blue")
            kColor = Color.blue;
        else if (rincian.warna == "red")
            kColor = Color.red;

        KekuatanStat kS = instance.GetComponent<KekuatanStat>();
        kS.namaSatuan = kekuatan.nama;
        kS.nama = rincian.nama_kekuatan;
        kS.warna = rincian.warna;
        kS.keterangan = rincian.ket;
        kS.pemilikKekuatan = rincian.milik;
        kS.infoKekuatan = kekuatan.info_kekuatan;
        kS.rincian = kekuatan.rincian;
        kS.lat_y = kekuatan.lat_y;
        kS.lng_x = kekuatan.lng_x;
        kS.marker3D = marker3D;

        var metadata = instance.GetComponent<Metadata>();
        var mImage = metadata.FindParameter("marker").parameter.GetComponent<Image>();
        var mGaris = metadata.FindParameter("garis").parameter.GetComponent<Image>();
        var mBackground = metadata.FindParameter("background").parameter.GetComponent<Image>();
        var nKekuatan = metadata.FindParameter("nama").parameter.GetComponent<TMP_Text>();
        var kKekuatan = metadata.FindParameter("keterangan").parameter.GetComponent<TMP_Text>();
        var content = metadata.FindParameter("content").parameter.transform;

        for (int i = 0; i < info.Length; i++)
        {
            var font_kekuatan = Font_Kekuatan.FromJson(Font_Kekuatan.ToString(info[i].font));
            string matra = "";

            if (info[i].jenis == "kekuatanAD")
                matra = "Darat";
            else if (info[i].jenis == "kekuatanAL")
                matra = "Laut";
            else if (info[i].jenis == "kekuatanAU")
                matra = "Udara";

            var copy = Instantiate(PlotKekuatanController.Instance.listKekuatan, content);
            var meta = copy.GetComponent<Metadata>();

            meta.FindParameter("No.").parameter.GetComponent<TMP_Text>().text = (i + 1).ToString();
            meta.FindParameter("No.").parameter.GetComponent<TMP_Text>().color = kColor;
            // copy.transform.Find("Icon").GetComponent<Image>().sprite = iSatuan.sprite;
            meta.FindParameter("Nama").parameter.GetComponent<TMP_Text>().text = info[i].nama;
            meta.FindParameter("Nama").parameter.GetComponent<TMP_Text>().color = kColor;
            meta.FindParameter("Jumlah Num").parameter.GetComponent<TMP_Text>().text = info[i].jumlah;
            meta.FindParameter("Jumlah Num").parameter.GetComponent<TMP_Text>().color = kColor;
            meta.FindParameter("Simbol").parameter.GetComponent<Text>().text = ((char)int.Parse(font_kekuatan.index)).ToString();
            meta.FindParameter("Simbol").parameter.GetComponent<Text>().font = Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + font_kekuatan.nama);
            meta.FindParameter("Simbol").parameter.GetComponent<Text>().color = kColor;

            kS.satuanList.Add(copy);
            kS.matraSatuan.Add(matra);
        }

        mGaris.color = kColor;
        mBackground.color = kColor;

        nKekuatan.color = kColor;
        nKekuatan.text = rincian.nama_kekuatan;

        kKekuatan.color = kColor;
        kKekuatan.text = rincian.ket;

        switch (rincian.milik)
        {
            case "0":
                mImage.sprite = PlotKekuatanController.Instance.defaultImage;
                mImage.color = kColor;
                break;
            case "AD":
                mImage.sprite = PlotKekuatanController.Instance.angkatanDarat;
                break;
            case "AL":
                mImage.sprite = PlotKekuatanController.Instance.angkatanLaut;
                break;
            case "AU":
                mImage.sprite = PlotKekuatanController.Instance.angkatanUdara;
                break;
        }
    }

    #endregion

    private string GetObj3DSatuan(string path)
    {
        if (string.IsNullOrEmpty(path)) return "";

        var splitDirectory = path.Split("/");
        return splitDirectory[splitDirectory.Length - 1].Split(".FBX")[0];
    }

    private GameObject getAssetSatuan(string nama_satuan, string nama_obj3D, string jenis_satuan)
    {
        // Dapatkan Asset 3D Satuan dari Asset Package Berdasarkan Nama Satuan atau Nama Object 3D Satuannya
        int index = AssetPackageManager.Instance.ASSETS_NAME.IndexOf(nama_satuan);
        if (index == -1) index = AssetPackageManager.Instance.ASSETS_NAME.IndexOf(nama_obj3D);

        // Jika Tidak Terdapat Asset 3D dalam Asset Package, Gunakan Asset Default Berdasarkan Jenis Satuannya
        if (index == -1) return jenis_satuan == "vehicle" ? AssetPackageManager.Instance.defaultVehicle :
                                jenis_satuan == "ship" ? AssetPackageManager.Instance.defaultShip :
                                jenis_satuan == "aircraft" ? AssetPackageManager.Instance.defaultAircraft :
                                AssetPackageManager.Instance.defaultVehicle;

        return AssetPackageManager.Instance.ASSETS[index];
    }

    public HUDNavigationElement CreateHUDElement(OnlineMapsMarker3D marker, HUDNavigationElement customElement = null, bool isInsideOBJ = false)
    {
        // Add Simbol Taktis Component
        var parentHUD = new GameObject("HUD_ELEMENT");
        if (isInsideOBJ)
        {
            parentHUD.transform.parent = marker.instance.transform.GetChild(0);
        }
        else
        {
            parentHUD.transform.parent = marker.transform;
        }
        parentHUD.transform.localPosition = Vector3.zero;

        parentHUD.tag = "entity-simboltaktis";
        var HUDElement = Instantiate((customElement == null) ? prefabHUDElement.gameObject : customElement.gameObject, parentHUD.transform).GetComponent<HUDNavigationElement>();


        HUDElement.Prefabs.IndicatorPrefab = Instantiate(HUDElement.Prefabs.IndicatorPrefab, marker.transform);
        HUDElement.Prefabs.IndicatorPrefab.name = "-- simbol taktis --";

        return HUDElement;
    }

    public JArray setJArrayResult(JArray arrayResult, int index)
    {
        Debug.Log(arrayResult[index].ToString());
        if (arrayResult[index].ToString() != "False")
        {
            return JArray.Parse(arrayResult[index].ToString());
        }
        else
        {
            return null;
        }
    }

    public bool isJArrayNull(JArray array)
    {
        if (array == null)
        {
            return true;
        }
        else
        {
            if (array.Count == 0) return true;
        }

        return false;
    }
}
