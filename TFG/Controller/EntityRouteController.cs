using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using GlobalVariables;
using UnityEngine;

using HelperPlotting;

public class EntityRouteController : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private MetadataRoute metadata;
    public Transform misiRouteContainer;
    public Transform drawRouteContainer;
    public MetadataDrawRoute prefabDrawRoute;
    public MetadataParentRoute prefabMisiRoute;

    [Header("Data")]
    public List<MetadataRoute> listRoute = new List<MetadataRoute>();
    public List<OnlineMapsDrawingLine> listRouteDrawing = new List<OnlineMapsDrawingLine>();

    public async Task<(MetadataRoute, OnlineMapsDrawingLine)> AddNewRoute(List<Vector3> path, EntityMisi dataMisi, EntityMisiProperties propertyMisi, string id_user = null, double? speed = null)
    {
        GameObject newRouteParent = null;
        
        var misiRouteParentUser = misiRouteContainer.Find((id_user == null) ? SessionUser.id.ToString() : id_user);
        if (misiRouteParentUser == null)
        {
            newRouteParent = Instantiate(prefabMisiRoute.gameObject, misiRouteContainer);
            newRouteParent.name = (id_user == null) ? SessionUser.id.ToString() : id_user;
        }
        else
        {
            newRouteParent = misiRouteParentUser.gameObject;
        }
        
        var new_route = Instantiate(metadata.gameObject, newRouteParent.transform).GetComponent<MetadataRoute>();
        new_route.routeID = dataMisi.id_mission;
        new_route.routeType = getJenisRoute(dataMisi.jenis);
        new_route.name = dataMisi.id_mission;
        
        if(speed == null) speed = GetKecepatanByType(propertyMisi.kecepatan, propertyMisi.type);
        DateTime startDate = DateTime.Parse(dataMisi.tgl_mulai);

        var drawRouteLine = new List<Vector2>();

        for (int i = 0; i < path.Count; i++)
        {
            var new_coordinate =
                new Vector3(
                    path[i].x,
                    path[i].y,
                    path[i].z
                );
            
            drawRouteLine.Add(
                new Vector2(path[i].x, path[i].y));

            if (i != 0)
            {
                double dx, dy;
                OnlineMapsUtils.DistanceBetweenPoints(path[i - 1].x, path[i - 1].y, path[i].x, path[i].y, out dx, out dy);
                double stepDistance = Math.Sqrt(dx * dx + dy * dy);
                
                // Total step time
                double totalTime = stepDistance / (double)speed * 3600;
                startDate = startDate.AddSeconds(totalTime);
            }
            
            new_route.AddNewPos(startDate.ToString(), new_coordinate, i);
        }

        var drawing = OnlineMapsDrawingElementManager.AddItem(new OnlineMapsDrawingLine(drawRouteLine, Color.gray, 0.35f));
        drawing.DrawOnTileset(OnlineMapsTileSetControl.instance, OnlineMapsDrawingElementManager.instance.Count - 1);
        drawing.instance.transform.position = Vector3.up;
        drawing.name = "path_" + new_route.name;
        listRouteDrawing.Add((OnlineMapsDrawingLine)drawing);
        
        listRoute.Add(new_route);
        newRouteParent.GetComponent<MetadataParentRoute>().dataRouteMisi.Add(new_route);

        return (new_route, (OnlineMapsDrawingLine)drawing);
    }

    public void ToggleDrawMisi(bool toggle)
    {
        foreach (Transform metadata in drawRouteContainer)
        {
            Debug.Log(metadata.gameObject.name);
            foreach (var drawing in metadata.GetComponent<MetadataDrawRoute>().dataDrawRoute)
            {
                drawing.visible = toggle;
            }
        }
    }
    
    private double GetKecepatanByType(string speed, string speed_type)
    {
        switch (speed_type)
        {
            case "km":
                return double.Parse(speed);
            case "knot":
                return double.Parse(speed) * 1.852;
            case "mach":
                return double.Parse(speed) * 1192.68f;
            default:
                return double.Parse(speed);
        }
    }

    private MetadataRoute.type getJenisRoute(string jenis)
    {
        switch (jenis)
        {
            case "pergerakan":
                return MetadataRoute.type.Pergerakan;
            case "embar":
                return MetadataRoute.type.Embarkasi;
            case "debar":
                return MetadataRoute.type.Debarkasi;
            default: return MetadataRoute.type.Pergerakan;
        }
    }
}
