using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

using TMPro;
using DigitalRuby.WeatherMaker;

using GlobalVariables;
using HelperUser;

public class GameController : MonoBehaviour
{
    public enum gameplayMode { SINGLEPLAYER, MULTIPLAYER, MP_SPECTATOR, MP_WARGAMING }

    [Header("Controller References")]
    public TimetableController _timetable;
    public EntityController _entity;

    [Header("Start Overlay References")]
    public GameObject UI_StartOverlay;

    [Header("Canvas UI")]
    public Animator UI_Canvas;

    [Header("Docs User References")]
    public TextMeshProUGUI UI_Docs;

    [Header("Config")]
    public gameplayMode GameMode;

    #region -- DEBUG MODE --
    /// <summary>
    /// Debug Untuk Login Sebagai User Tanpa Harus Membuka Halaman Login Terlebih Dahulu
    /// </summary>
    [Header("Debug")]
    [SerializeField] private bool debugUser;
    [SerializeField] private string userJSON;
    [SerializeField] private string serviceLogin;
    [SerializeField] private string serviceCB;
    [SerializeField] private string serverColyseus;
    [SerializeField] private int aircraftAlt;
    [SerializeField] private int vehicleScale;
    [SerializeField] private int shipScale;
    [SerializeField] private int aircraftScale;
    #endregion

    public Vector2 indukPosition;
    public int indukHeading;
    public int jumlahUnitFormasi = 3;
    public int jarak = 1;

    public WeatherMakerConfigurationScript weatherConfig;
    public ColyseusController _colyseus;

    public static GameController instance;
    void Awake()
    {
        // Set Asset Package Manager Menjadi 1 Origin
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    private async void Start()
    {
        if (debugUser)
        {
            await TestLoggedUser();
        }
        else
        {
            //if (SessionUser.id == 1)
            //{
            //    joinAsPlayerMP();
            //}
        }
    }

    private async Task TestLoggedUser()
    {
        // -- KOGASRATGAB TEST --
        // [{"username":"asops_kogasratgab","id":124,"name":"ASOPS KOGASRATGAB","jenis_user":{"ID":1,"jenis_user":"user"},"bagian":{"ID":8,"nama_bagian":"KOGASRATGAB"},"jabatan":{"ID":2,"nama_jabatan":"asisten"},"asisten":{"ID":6,"nama_asisten":"ASOPS"},"atasan":null,"status_login":1,"be":0}]
        User[] user = User.FromJson(userJSON);

        new SessionUser(user[0]);

        Config.SERVICE_LOGIN = (serviceLogin != null) ? serviceLogin : "http://localhost/eppkm/public";
        Config.SERVICE_CB = (serviceCB != null) ? serviceCB : "http://localhost/eppkm_simulasi";
        Config.SERVER_COLYSEUS = (serverColyseus != null) ? serverColyseus : "ws://localhost:2567";
        Config.aircraft_alt = aircraftAlt;
        Config.VEHICLE_SCALE = vehicleScale;
        Config.SHIP_SCALE = shipScale;
        Config.AIRCRAFT_SCALE = aircraftScale;

        await AssetPackageManager.Instance.loadPackageAlutsista();

        await API.Instance.GetSkenarioAktif();
        await API.Instance.GetCBTerbaik(SessionUser.id_bagian.ToString());

        if(GameMode == gameplayMode.SINGLEPLAYER)
        {
            joinAsPlayerSP();
        }
        else if(GameMode == gameplayMode.MULTIPLAYER)
        {
            joinAsPlayerMP();
        }
        else if(GameMode == gameplayMode.MP_WARGAMING)
        {
            joinWargaming();
        }
        else
        {
            joinAsSpectator();
        }

        //if(SessionUser.id == 1)
        //{
        //    joinAsPlayerMP();
        //}
        //else
        //{
        //    joinAsPlayerSP();
        //}
    }

    private void initDefaultWeather()
    {
        WeatherController.instance.ChangeCloud(1);
    }

    /// <summary>
    /// Masuk ke ROOM Sebagai "Spectator"
    /// Spectator : Apliaksi akan men-simulasikan pergerakan berdasarkan pergerakan yang terjadi di TFG 2D (App hanya men-sinkronisasi pergerakan object dari colyseus)
    /// </summary>
    public async void joinAsSpectator()
    {
        GameMode = gameplayMode.MP_SPECTATOR;
        prepareGameplay();
    }
    
    /// <summary>
    /// Masuk ke ROOM "Wargaming"
    /// Wargaming : Aplikasi akan men-simulasikan peperangan seperti halnya pada 2D yang nanti akan mengirimkan hasil tersebut ke dalam colyseus yang akan di sinkronkan ke dalam WGS 2D
    /// </summary>
    public async void joinWargaming()
    {
        GameMode = gameplayMode.MP_WARGAMING;
        prepareGameplay();
    }

    /// <summary>
    /// -- MULTIPLAYER MODE --
    /// Masuk ke ROOM Sebagai "Player"
    /// Player : Aplikasi akan men-simulasikan pergerakannya sendiri (App akan mengirimkan hasil pergerakan ke dalam colyseus untuk di-sinkronisasi ke dalam TFG 2D)
    /// </summary>
    public async void joinAsPlayerMP()
    {
        GameMode = gameplayMode.MULTIPLAYER;
        prepareGameplay();
    }

    /// <summary>
    /// -- SINGLEPLAYER MODE --
    /// Masuk ke ROOM Sebagai "Player"
    /// Player : Aplikasi akan men-simulasikan pergerakannya sendiri (App bergerak sendiri dalam mode singleplayer tanpa menggunakan server / room)
    /// </summary>
    public async void joinAsPlayerSP()
    {
        GameMode = gameplayMode.SINGLEPLAYER;
        prepareGameplay();
    }

    private async void prepareGameplay()
    {
        // Loading Progress
        CloseStartOverlayMenu(false);

        SyncGameplayLoading("Preparing Gameplay");
        await Task.Delay(1000);

        SyncGameplayLoading("Setting Up Environment");
        initDefaultWeather();
        await Task.Delay(1000);

        // Persiapkan Data Timetable Kegiatan dari Kogas yang login
        SyncGameplayLoading("Load Timetable Kegiatan");
        var kegiatan = await _timetable.InitTimetable();

        if(GameMode != gameplayMode.MP_SPECTATOR)
        {
            // -- SPECTATOR TIDAK PERLU CONTROL PLAYBACK --
            
        }
        
        // Persiapkan waktu awal dan akhir playback berdasarkan waktu pada timetable kegiatan
        SyncGameplayLoading("Prepare Playback Timing");
        await PlaybackController.Instance.initPlayback(kegiatan);

        UserActiveController.Instance.addToList(SessionUser.name);

        if (GameMode != gameplayMode.SINGLEPLAYER)
        {
            // -- JOIN COLYSEUS KHUSUS UNTUK GAMEPLAY MULTIPLAYER
            //Join / Create ROOM Colyseus dan masuk sebagai KOGAS(X)

            _colyseus = GetComponent<ColyseusController>();
            PlaybackController.Instance._colyseus = GetComponent<ColyseusController>();

            if (_colyseus == null) _colyseus = FindObjectOfType<ColyseusController>();
            if (_colyseus != null)
            {
                SyncGameplayLoading("Joining Room");
                await _colyseus.ClientPrepare();

                SyncGameplayLoading("Creating Track");
                PlaybackController.Instance.SyncTimeToColyseus();
                ColyseusController.instance.SyncPlaybackPercepatan(1);
            }
            else
            {
                //Component Colyseus Gagal Ditemukan, Catch ERROR
                CloseStartOverlayMenu(true);
            }
        }

        //if(GameMode == gameplayMode.MULTIPLAYER)
        //{
        //    SyncGameplayLoading("Load CB");
        //    await _entity.LoadEntityFromCB();
        //}

        if(SessionUser.id != 1)
        {
            // Load CB Otomatis Hanya Berlaku Jika User Login Sebagai Kogas (WASDAL tidak memiliki dokumen apapun)
            SyncGameplayLoading("Load CB");
            if(SessionUser.id_kogas != 0) await _entity.LoadEntityFromCB(SessionUser.id.ToString());
            // else await _entity.LoadEntityFromCB(true, "116");

            if(GameMode == gameplayMode.MP_WARGAMING)
            await _entity.LoadEntityFromCB(true, "116");
            
            //await _entity.LoadEntityFromCB(true);
        }

        UI_Docs.text = "[ " + CBSendiri.nama_document + " ] - " + SessionUser.name;

        if(SessionUser.id != 1)
        {
            foreach (var text in GameObject.FindGameObjectsWithTag("label-asisten"))
            {
                text.GetComponent<TextMeshProUGUI>().text = SessionUser.nama_asisten;
            }
        }

        foreach (var text in GameObject.FindGameObjectsWithTag("label-user"))
        {
            text.GetComponent<TextMeshProUGUI>().text = SessionUser.bagian;
        }

        await Task.Delay(1000);
        UI_StartOverlay.GetComponent<Animator>().Play("ServiceSlideOut");
        //await BasemapLayerController.instance.fetchLayerList();
        //await BasemapLayerController.instance.fetchBasemapList();
        //await BasemapLayerController.instance.getDefaultBasemap();
        await BasemapControllerWGS.instance.LoadMapServiceFromFileAsync();
    }

    /// <summary>
    /// Choose Gameplay Mode And Start Loading Gameplay Components (PrepareGameplay)
    /// </summary>
    /// <param name="isInversingAction">Show Back Start Overlay Menu (IF Prepare Gameplay Failed)</param>
    public void CloseStartOverlayMenu(bool isInversingAction)
    {
        var menuGroup = UI_StartOverlay.GetComponent<Metadata>().FindParameter("menu-group").parameter;
        var loadingGroup = UI_StartOverlay.GetComponent<Metadata>().FindParameter("loading-group").parameter;

        if (menuGroup == null || loadingGroup == null) return;

        if (!isInversingAction)
        {
            menuGroup.GetComponent<Animator>().Play("SlideOut");
            loadingGroup.GetComponent<Animator>().Play("SlideIn");
        }
        else
        {
            loadingGroup.GetComponent<Animator>().Play("SlideOut");
            menuGroup.GetComponent<Animator>().Play("SlideIn");
        }
    }

    /// <summary>
    /// Change Loading Text After Each Prepare Progress Done
    /// </summary>
    /// <param name="message"></param>
    private async void SyncGameplayLoading(string message)
    {
        Debug.Log(message);
        var loadingText = UI_StartOverlay.GetComponent<Metadata>().FindParameter("loading-text").parameter;
        if (loadingText == null) return;

        if (loadingText != null) loadingText.GetComponent<TextMeshProUGUI>().text = message + "...";
    }

    /// <summary>
    /// Logout Back to Login Menu
    /// </summary>
    public async void LogoutToMenu()
    {
        UI_Canvas.Play("SlideOut");

        if (SceneLoad.returnTo != null)
        {
            LevelManager.Instance.LoadScene(SceneLoad.returnTo, null, "FadeIn", "FadeOut");
        }
        else
        {
            LevelManager.Instance.LoadScene("TFG_Login", null, "FadeIn", "FadeOut");
        }
    }

    //private void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.LeftBracket))
    //    {
    //        DetailSatuanController.Instance.SwitchFollowBetween(-2);
    //    }

    //    if (Input.GetKeyDown(KeyCode.RightBracket))
    //    {
    //        DetailSatuanController.Instance.SwitchFollowBetween(2);
    //    }
    //}
}
