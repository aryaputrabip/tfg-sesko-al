using System.Threading.Tasks;
using System.Linq;

using UnityEngine;

using TMPro;
using EnhancedUI.EnhancedScroller;

public class ListEntityController : MonoBehaviour
{
    [Header("References")]
    public EnhancedScroller listSatuan;
    public DetailSatuanController _detailSatuan;

    #region INSTANCE
    public static ListEntityController Instance;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    // Add Satuan Baru Ke Dalam List
    public async Task AddToList(OnlineMapsMarker3D marker, Entities entity, string index, string fontFamily)
    {
        if (marker == null) return;

        var scrollerSatuan = GetComponent<GListSatuanScroller>();
        scrollerSatuan.AddData(
            marker, entity.nama_entity,
            "",
            marker.instance.GetComponent<AlutsistaBundleData>().image,
            index,
            Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + fontFamily)
        );  

        scrollerSatuan.scroller.ReloadData();
    }

    public void OnEntitySelected(OnlineMapsMarker3D marker)
    {
        var data = marker.instance.GetComponent<DetailSatuan>();
        _detailSatuan.gameObject.SetActive(true);
        _detailSatuan.GetComponent<Animator>().Play("FadeIn");

        _detailSatuan.ShowDetailSatuan(
            marker, data.id,
            data.nama,
            data.icon != null ? data.icon : null,
            data.fontTaktis,
            data.fontFamily,
            data.tipeTNI.ToString(),
            data.tipeKendaraan.ToString(),
            data.jenisKendaraan.ToString(),
            data.warna == Color.blue ? "Blue" : "Red"
        );
    }

    public void LocateEntity(OnlineMapsMarker3D marker)
    {
        MapController.Instance.GoToLocation(marker.position, 19.5f);
    }

    public void OnEntitySelected(OnlineMapsMarkerBase markerBase)
    {
        Debug.Log("Locate Entity");

        OnlineMapsMarker3D marker = markerBase["markerSource"] as OnlineMapsMarker3D;
        OnEntitySelected(marker);
    }
}
