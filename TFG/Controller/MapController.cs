using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour
{
    [Header("Movement Properties")]
    /// <summary>
    /// Move duration (sec)
    /// </summary>
    public float time = 3;
    public AnimationCurve animationCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
    public AnimationCurve animationCurveZoom = AnimationCurve.EaseInOut(0, 1, 1, 0);

    /// <summary>
    /// Relative position (0-1) between from and to
    /// </summary>
    [SerializeField] private float angle;
    /// <summary>
    /// Movement trigger
    /// </summary>
    [SerializeField] private bool isMovement;

    private Vector2 toPosition;
    private float toZoom;
    private float movementTime;

    #region INSTANCE
    public static MapController Instance;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    public void GoToLocation(Vector2 location, float zoom)
    {
        toPosition = location;
        toZoom = zoom;

        isMovement = true;
    }

    private void Update()
    {
        if (!isMovement) return;

        // Updating the progress of the animation
        movementTime += Time.deltaTime;
        float t = movementTime / time;

        // If the animation is finished
        if (t >= 1)
        {
            // Reset values
            movementTime = 0;
            isMovement = false;
            t = 1;
        }

        // Update the camera
        var OM = OnlineMaps.instance;
        float f = animationCurve.Evaluate(t);
        float fz = animationCurveZoom.Evaluate(t);

        OM.SetPositionAndZoom(Mathf.Lerp(OM.position.x, toPosition.x, f), Mathf.Lerp(OnlineMaps.instance.position.y, toPosition.y, f), Mathf.Lerp(OnlineMaps.instance.floatZoom, toZoom, fz));
    }
}
