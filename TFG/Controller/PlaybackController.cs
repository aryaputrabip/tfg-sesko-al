using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

using GlobalVariables;
using HelperKegiatan;
using TMPro;

public class PlaybackController : MonoBehaviour
{
    public enum playbackState { STOPPED, PLAYING, PAUSED }
    
    [Header("Properties")]
    public int percepatan;

    public playbackState state;
    public DateTime _TIME;

    [Header("Playback References")]
    public Image playPauseIcon;
    public Slider playbackSlider;
    public Sprite play_icon;
    public Sprite pause_icon;
    public Sprite stop_icon;

    [Header("Notification References")]
    public Animator notifPercepatan;
    public Animator notifState;

    [Header("Percepatan References")]
    public TMP_Dropdown dropdownPercepatan;
    public TMP_InputField inputPercepatan;

    public bool sliderDragged = false;

    [Header("Time Of Day")]
    public Slider timeOfDaySlider;

    public ColyseusController _colyseus;

    [Header("Time Zone References")]
    public Animator timeRegion;
    public Toggle toggleAdditionalTime;
    public TMP_Dropdown dropdownTimeZone;
    public TMP_Text timeWIB, timeWITA, timeWIT;
    public TextMeshProUGUI labelUTC;

    private double timeDiff;
    public bool isTimeCycle = false;
    
    #region SET INSTANCE
    // INSTANCE VARIABLE
    public static PlaybackController Instance;
    
    /// <summary>
    /// Set Instance On Awake
    /// </summary>
    private void Awake()
    {
        // Set Playback Controller Menjadi 1 Origin
        if (Instance == null)
        {
            Instance = this;
            playbackSlider = this.GetComponent<Slider>();
            sliderDragged = false;

            //DateTime.SpecifyKind(_TIME, DateTimeKind.Utc);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    /// <summary>
    /// Prepare Waktu Mulai dan Akhir Playback Berdasarkan Kegiatan Pada Timetable
    /// </summary>
    /// <param name="kegiatan"></param>
    public async Task initPlayback(TimetableKegiatan[] kegiatan)
    {
        Debug.Log("Init Playback");
        List<long> list_waktu = new List<long>();

        // Dapatkan Waktu Mulai Dan Berakhir Kegiatan
        for (int i = 0; i < kegiatan.Length; i++)
        {
            if (kegiatan[i].kogas != "false" && kegiatan[i].waktu != "")
            {
                if (!list_waktu.Contains(kegiatan[i].waktuDT.Ticks)) list_waktu.Add(kegiatan[i].waktuDT.Ticks);
            }
        }

        timeDiff = 0;

        // Sort Waktu Kegiatan Dari Terkecil ke Terbesar
        list_waktu.Sort();

        Debug.Log("Count List Waktu : " + list_waktu.Count);

        if(list_waktu.Count >= 2)  
        {
            SkenarioAktif.WaktuMulai = new DateTime(list_waktu[0]);
            SkenarioAktif.WaktuAkhir = new DateTime(list_waktu[list_waktu.Count - 1]);

            Debug.Log(new DateTime(list_waktu[0]).ToLongDateString() + " | " + SkenarioAktif.WaktuMulai.ToLongDateString());

            // Set Scroller Start Value & End Value Berdasarkan Waktu Mulai dan Akhir Skenario Aktif
            playbackSlider.minValue = SkenarioAktif.WaktuMulai.Ticks;
            playbackSlider.maxValue = SkenarioAktif.WaktuAkhir.Ticks;

            _TIME = new DateTime((long)playbackSlider.minValue);
            if (isTimeCycle) EnviroSkyMgr.instance.SetTime(_TIME);
            //timeOfDaySlider.value = (isTimeCycle) ? (float)_TIME.TimeOfDay.TotalSeconds : 43200;

            refreshTimeUI();
        }
    }

    public void changeState(string action)
    {
        if (action == "play/pause")
        {
            // Ketika Men-click Tombol Play/Pause
            // - Jika Tombol Berada di posisi stop / pause, ubah state menjadi play
            // - Jika Tombol Berada di posisi play, ubah state menjadi pause
            state = (state == playbackState.STOPPED || state == playbackState.PAUSED)
                ? playbackState.PLAYING
                : playbackState.PAUSED;

            refreshTimeUI();
            refreshPlaybackUI();
        }else if (action == "stop")
        {
            // Ketika Men-click Tombol Stop (kembali ke waktu paling awal)
            state = playbackState.STOPPED;

            playbackSlider.value = playbackSlider.minValue;
            _TIME = new DateTime((long)playbackSlider.value);
            if (isTimeCycle) EnviroSkyMgr.instance.SetTime(_TIME);
            //timeOfDaySlider.value = (isTimeCycle) ? (float)_TIME.TimeOfDay.TotalSeconds : 43200;

            refreshTimeUI();
            refreshPlaybackUI();
        }
    }

    /// <summary>
    /// Set Percepatan Playback
    /// </summary>
    /// <param name="speed"></param>
    public void changePercepatan(int? speed = null)
    {
        // Conditional : Jika Percepatan NULL / < 0 = TIDAK BERUBAH
        if (speed == null)
        {
            if (inputPercepatan.text == null) return;
            speed = int.Parse(inputPercepatan.text);
        }

        if (speed < 0) return;
        percepatan = speed.Value;
        
        refreshPercepatanUI();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="day"></param>
    public async void changeDay(int day = 0)
    {
        _TIME = _TIME.AddDays(day);
        playbackSlider.value = _TIME.Ticks;
        if (isTimeCycle) EnviroSkyMgr.instance.SetTime(_TIME);
        //timeOfDaySlider.value = (isTimeCycle) ? (float)_TIME.TimeOfDay.TotalSeconds : 43200;

        refreshWalkerPOS();

        refreshTimeUI();
        SyncTimeToColyseus();
    }

    #region -- ON SLIDER DRAG EVENT --
    public void OnSliderBeginDrag()
    {
        sliderDragged = true;
    }

    public void OnSliderEndDrag()
    {
        sliderDragged = false;
    }
    
    public async void OnSliderDragged()
    {
        if (!sliderDragged) return;
        
        _TIME = new DateTime((long)playbackSlider.value);
        if (isTimeCycle) EnviroSkyMgr.instance.SetTime(_TIME);
        //timeOfDaySlider.value = (isTimeCycle) ? (float)_TIME.TimeOfDay.TotalSeconds : 43200;
        refreshTimeUI();

        SyncTimeToColyseus();
    }
    #endregion

    #region  EVENT PERCEPATAN --

    public void OnPercepatanDropdownChanged()
    {
        inputPercepatan.transform.parent.gameObject.SetActive(dropdownPercepatan.value == 0 ? true : false);
        
        if (dropdownPercepatan.value != 0)
        {
            percepatan = int.Parse(dropdownPercepatan.options[dropdownPercepatan.value].text.Split("x")[0]);
            
            refreshPercepatanUI();
        }
        else
        {
            inputPercepatan.text = percepatan.ToString();
        }
    }

    public void OnCustomPercepatanFilled()
    {
        // Percepatan Tidak Boleh Negatif
        if (int.Parse(inputPercepatan.text) < 0) return;
        
        percepatan = int.Parse(inputPercepatan.text);
        refreshPercepatanUI();
    }
    #endregion

    /// <summary>
    /// Update UI Yang Menunjukkan Tanggal dan Waktu Playback
    /// </summary>
    public void refreshTimeUI()
    {
        if (timeDiff == 0)
        {
            foreach (var objTime in UITaggedManager.Instance.labelTimes)
            {
                if (objTime) objTime.text = _TIME.ToString("HH:mm:ss");
            }

            foreach (var objDate in UITaggedManager.Instance.labelDates)
            {
                if (objDate) objDate.text = _TIME.ToString("D", new CultureInfo("id-ID"));
            }

            foreach (var objHariH in UITaggedManager.Instance.labelHariH)
            {
                if (objHariH)
                {
                    var difference = (int)(_TIME - SkenarioAktif.HARI_H).TotalDays;
                    objHariH.text = (difference > 0) ? "H+" + difference : "H" + ((difference == 0) ? "" : difference);
                }
            }
        } else
        {
            foreach (var objTime in UITaggedManager.Instance.labelTimes)
            {
                if (objTime) objTime.text = _TIME.AddMinutes(timeDiff).ToString("HH:mm:ss");
            }

            foreach (var objDate in UITaggedManager.Instance.labelDates)
            {
                if (objDate) objDate.text = _TIME.AddMinutes(timeDiff).ToString("D", new CultureInfo("id-ID"));
            }

            foreach (var objHariH in UITaggedManager.Instance.labelHariH)
            {
                if (objHariH)
                {
                    var difference = (int)(_TIME.AddMinutes(timeDiff) - SkenarioAktif.HARI_H.AddMinutes(timeDiff)).TotalDays;
                    objHariH.text = (difference > 0) ? "H+" + difference : "H" + ((difference == 0) ? "" : difference);
                }
            }
        }

        if (toggleAdditionalTime.isOn)
        {
            timeWIB.text = _TIME.ToString("HH:mm:ss");
            timeWITA.text = _TIME.AddMinutes(60).ToString("HH:mm:ss");
            timeWIT.text = _TIME.AddMinutes(120).ToString("HH:mm:ss");
        }
    }

    /// <summary>
    /// Update UI Playback Controller
    /// </summary>
    public void refreshPlaybackUI()
    {
        switch (state)
        {
            case playbackState.PAUSED:
                playPauseIcon.sprite = play_icon;

                notifState.GetComponent<Image>().sprite = pause_icon;
                notifState.Play("CountFadeOut");
                break;
            case playbackState.PLAYING:
                playPauseIcon.sprite = pause_icon;

                notifState.GetComponent<Image>().sprite = play_icon;
                notifState.Play("CountFadeOut");
                break;
            case playbackState.STOPPED:
                playPauseIcon.sprite = play_icon;

                notifState.GetComponent<Image>().sprite = stop_icon;
                notifState.Play("CountFadeOut");
                break;
        }
    }
    
    /// <summary>
    /// Update UI Yang Menunjukkan Percepatan Playback
    /// </summary>
    public void refreshPercepatanUI()
    {
        foreach (var text in GameObject.FindGameObjectsWithTag("label-percepatan"))
        {
            var objPercepatan = text.GetComponent<TextMeshProUGUI>();
            if (objPercepatan != null) objPercepatan.text = percepatan.ToString();
        }

        notifPercepatan.Play("CountFadeOut");
    }

    public async void refreshWalkerPOS()
    {
        sliderDragged = true;
        await Task.Delay(100);
        sliderDragged = false;
    }

    public async void SyncStateToColyseus()
    {
        if(!_colyseus) return;
        await _colyseus.SyncPlaybackState((state == playbackState.PLAYING) ? "start" : (state == playbackState.PAUSED) ? "pause" : "stop");
    }

    public async void SendPercepatanToColyseus()
    {
        if (!_colyseus) return;
        
        Debug.Log("Send Percepatan To Colyseus");
        await _colyseus.SyncPlaybackPercepatan(percepatan);
    }

    public async void SyncTimeToColyseus()
    {
        if (_colyseus)
        {
            var difference = (int)(_TIME - SkenarioAktif.HARI_H).TotalDays;
            var hariHAktif = (difference > 0) ? "H+" + difference : "H" + ((difference == 0) ? "" : difference);

            var waktuAktif = _TIME.ToString("HH:mm:ss tt");

            var msAktif = new DateTimeOffset(_TIME).ToUnixTimeMilliseconds();

            await _colyseus.SyncPlaybackTime(hariHAktif, waktuAktif, msAktif);
        }
    }

    public async void JUMPDATETEST()
    {
        await _colyseus.JUMPDATETEST("02/10/2021 12:00:00");
    }

    private float _updateTime;
    private void Update()
    {
        // Unity Dalam Mode Spectator Hanya Men-sinkronisasi Waktu dari 2D (tidak meng-update)
        if (_colyseus && GameController.instance.GameMode == GameController.gameplayMode.MP_SPECTATOR) return;

        if (state == playbackState.PLAYING && !sliderDragged)
        {
            _TIME = _TIME.AddSeconds(Time.deltaTime * percepatan);
            playbackSlider.value = _TIME.Ticks;

            if (isTimeCycle) EnviroSkyMgr.instance.SetTime(_TIME);
            //timeOfDaySlider.value = (isTimeCycle) ? (float)_TIME.TimeOfDay.TotalSeconds : 43200;

            _updateTime += Time.deltaTime;
            if(_updateTime > (1 / percepatan))
            {
                _updateTime = _updateTime - (1 / percepatan);

                SyncTimeToColyseus();
                refreshTimeUI();
            }
        }
    }

    public void toggleTimeCycle(Toggle state)
    {
        isTimeCycle = state.isOn;
        if (!isTimeCycle)
        {
            EnviroSkyMgr.instance.SetTimeOfDay(12);
        }
        else
        {
            EnviroSkyMgr.instance.SetTime(_TIME);
        }

        if (isTimeCycle) EnviroSkyMgr.instance.SetTime(_TIME);
        //timeOfDaySlider.value = (isTimeCycle) ? (float)_TIME.TimeOfDay.TotalSeconds : 43200;
    }

    //public void SetTimeDiff(int time) scrapped
    public void SetTimeDiff()
    {
        // Time difference in minute(s) from WIB (UTC+7:00)
        if (dropdownTimeZone.value == 0) return;
        //timeDiff = (double)time;
        //Debug.Log("Changed time to " + playbackTime.AddMinutes(timeDiff).ToString("dddd, dd MMMM yyyy HH:mm:ss tt"));

        timeDiff = (-20 + dropdownTimeZone.value) * 60;

        //Debug.Log("Time offset from UTC+7 : " + timeDiff + " minutes");
        labelUTC.text = dropdownTimeZone.options[dropdownTimeZone.value].text.Split("UTC")[1];
        
        refreshTimeUI();
    }

    public void toggleAdditionalTimeRegion()
    {
        timeRegion.Play((!toggleAdditionalTime.isOn) ? "SlideOut" : "SlideIn");
    }
}
