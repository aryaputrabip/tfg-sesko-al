using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

//using HelperEntity;
using HelperPlotting;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Plot.Pasukan;
using GlobalVariables;
using ObjectStat;
using System.Linq;
using Plot.FormasiPlot;

using static ListOfSimpleFunctions.Functions;
using static HelperPlotting.FormasiType;
using static Plot.FormasiPlot.PerhitunganFormasiV2;
using static Plot.FormasiPlot.PerhitunganMisiFormasi;

public class RouteController : MonoBehaviour
{
    [Header("View List Rute")]
    [SerializeField] private GameObject listContainer;
    public Transform routeData;
    [SerializeField] private GameObject prefabList;

    [Header("Input UI")]
    public GameObject modalRute;
    public GameObject modalDraw;
    public GameObject modalDelete;
    public GameObject routeManager;
    //public TMP_InputField inputNamaSatuan;
    public TMP_InputField inputNamaRute;
    public TMP_InputField inputNamaKegiatan;
    //public TMP_InputField inputTanggal;
    public TMP_InputField inputKecepatan;
    public TMP_Dropdown dropdownKecepatan;
    public TMP_Dropdown dropdownJenis;
    //public TMP_InputField inputRute;
    //public TMP_InputField inputKoordinat;
    public TMP_InputField inputWaktuSampai;
    public GameObject btnToggler;
    public GameObject editGroup;
    private DateTime date;
    [SerializeField]  private string IDKegiatan;
    [SerializeField]  private string descKegiatan;

    [Header("Draw Rute")]
    [SerializeField] private Texture2D routePoint;
    private List<Vector2> paths;
    private bool isDrawing, isEditing, isDeleting;
    //private OnlineMapsDrawingLine aLine;
    private OnlineMapsDrawingLine aLine;
    private double lng, lat;
    private Vector3 pos;
    private int n;
    public int nPoly;
    private float dis;
    private String labelRoute;
    private List<OnlineMapsMarker> markers;
    [HideInInspector] public List<float> jarakList;
    OnlineMapsDrawingElement _poly;

    [Header("Metadata")]
    public PlotDataRoute editedRoute;
    public PlotDataRoute deletedRoute;

    [Header("Metadata")]
    public List<PlotDataRoute> listRoute;
    private List<int> listDeletedRoute = new List<int>();
    [SerializeField] private PlotDataRoute metadata;

    public ListSatuanRoute selectedListMisi;

    #region INSTANCE
    public static RouteController Instance;
    public OnlineMapsMarker3D markerInstance;
    OnlineMapsDrawingElement poly;
    List<Vector2> originPoint;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        setDate();
    }

    public void setDate()
    {
        date = modalRute.GetComponent<DateInputHelper>().date;
    }

    public double GetKecepatanByType(string speed, string speed_type)
    {
        switch (speed_type)
        {
            case "miles":
                return double.Parse(speed) * 0.621371;
                break;
            case "knot":
                return double.Parse(speed) * 1.852;
                break;
            case "mach":
                return double.Parse(speed) * 1192.68f;
                break;
            default:
                return double.Parse(speed);
                break;
        }
    }
    
    public static double GetKecepatanByType(string speed, string speed_type, bool isStatic = true)
    {
        switch (speed_type)
        {
            case "miles":
                return double.Parse(speed) * 0.621371;
                break;
            case "knot":
                return double.Parse(speed) * 1.852;
                break;
            case "mach":
                return double.Parse(speed) * 1192.68f;
                break;
            default:
                return double.Parse(speed);
                break;
        }
    }

    public void toggleLineOn()
    {
        foreach (Transform go in listContainer.transform)
        {
            var tr = go.transform.Find("Btn - Toggle");
            if (tr == null) continue;

            tr.GetComponent<SidemenuToggler>().toggleOn();
        }
    }

    public void toggleLineOff()
    {
        foreach(Transform go in listContainer.transform)
        {
            var tr = go.transform.Find("Btn - Toggle");
            if (tr == null) continue;

            tr.GetComponent<SidemenuToggler>().toggleOff();
        }
    }

    private void clearDrawings()
    {
        if (n > 0)
        {
            foreach (OnlineMapsMarker m in markers)
            {
                OnlineMapsMarkerManagerBase<OnlineMapsMarkerManager, OnlineMapsMarker>.RemoveItem(m, true);
                m.Dispose();
            }
            if(poly != null)
            poly.Dispose();
            aLine.Dispose();
            paths.Clear();
            markers.Clear();
            n = 0;
            OnlineMaps.instance.Redraw();

            var pF = markerInstance.instance.GetComponent<PFormasiV2>();
            var fS = pF.formasi;
            if(fS.type != formasiType.None)
            for(int i = 0; i < fS.unitsInFormasi.Count; i++)
            {
                var pFF = fS.unitsInFormasi[i].GetComponent<PFormasiV2>();
                pFF.routePoints.Clear();
            }

            // var pF = markerInstance.instance.GetComponent<PFormasi>();

            // foreach(var _obj in pF.listObjekFormasi)
            // {
            //     var pFF = _obj.GetComponent<PFormasi>();
            //     if(pFF.stat != null)
            //     {
            //         var pF2D = pFF.marker2D.instance.GetComponent<PFormasi>();
            //         pF2D.routePoints = new List<Vector2>();
            //     }
            //     var pF3D = pFF.marker3D.instance.GetComponent<PFormasi>();
            //     pF3D.routePoints = new List<Vector2>();
            // }
        }
    }

    public void performDraw()
    {
        clearDrawings();

        paths = new();
        // paths = new Vector2[]{markerInstance.position}.ToList();

        OnlineMapsControlBase.instance.OnMapClick += addRouteLine;
        isDrawing = true;
        //DrawingManager.Instance.isDeleting = false;
        modalDraw.GetComponent<Animator>().Play("FadeIn");
        modalRute.GetComponent<Animator>().Play("FadeOut");
        markers = new List<OnlineMapsMarker>();
        n = 0;
        dis = 0;
        labelRoute = "";
    }

    public void cancelDraw()
    {
        isDrawing = false;
        isEditing = false;
        nPoly = 0;
        clearDrawings();
        modalDraw.GetComponent<Animator>().Play("FadeOut");
    }

    private void addRouteLine()
    {
        var pF = markerInstance.instance.GetComponent<PFormasiV2>();
        var fS = pF.formasi;
        
        if(fS.type == formasiType.None)
        {
            double lng, lat;
            OnlineMapsControlBase.instance.GetCoords(out lng, out lat);
            paths.Add(new Vector2((float)lng, (float)lat));
            if (n < 1)
            {
                var man = OnlineMapsDrawingElementManager.AddItem(new OnlineMapsDrawingLine(paths, Color.white, 1));
                aLine = (OnlineMapsDrawingLine)man;
                aLine.width = 0.25f;
                pos = new Vector2((float)lng, (float)lat);
                //labelRoute += "lat : " + lat + " | " + "long : " + lng;
                labelRoute += lat + "," + lng;
            }
            else
            {
                //dis += OnlineMapsUtils.DistanceBetweenPoints(pos, new Vector2((float)lng, (float)lat)).magnitude;
                pos = new Vector2((float)lng, (float)lat);
                labelRoute += Environment.NewLine + lat + "," + lng;
            }
            OnlineMapsMarker mm = OnlineMapsMarkerManager.CreateItem(lng, lat, routePoint, "Titik Rute");
            mm.scale = 0.5f;

            markers.Add(mm);
            mm["line"] = aLine;
            mm["index"] = n;
            mm.OnPress += MarkerLongPress;
            mm.OnPositionChanged += changePosition;
            mm.OnRelease += finishChangePostion;

            n++;
            Debug.Log((float)lng + " | " + (float)lat);
        }
        else
        PerformDrawFormasiV2(pF);
    }

    void PerformDrawFormasiV2(PFormasiV2 pF)
    {
        var fS = pF.formasi;
        var n1 = new Vector2();
        var n2 = new Vector2();
        var newOrigin = new Vector2();
        var target = Resources.Load<Texture2D>("Images/Icons/Unsorted/icon-filled-target");

        double lng, lat;
        OnlineMapsControlBase.instance.GetCoords(out lng, out lat);
        { newOrigin.x = (float)lng; newOrigin.y = (float)lat; }
        labelRoute += lat + "," + lng + Environment.NewLine;

        var m = OnlineMapsMarkerManager.instance.Create(newOrigin, target);
        m.scale = .25f;
        m["line"] = aLine;
        m["index"] = n;
        m.OnPress += MarkerLongPress;
        m.OnPositionChanged += changePosition;
        m.OnRelease += finishChangePostion;
        markers.Add(m);

        paths.Add(newOrigin);
        
        if(paths.Count == 1)
        n1 = paths[^1];
        else
        n1 = paths[^2];
        
        n2 = paths[^1];
        
        var angleBetweenPoints = GetAngle(n1, n2) + 90;
        Debug.Log(angleBetweenPoints);
        var angle = paths.Count == 1 ? fS.arahUtama : angleBetweenPoints + fS.arahUtama;
        var jarakLng = Jarak(jarakList, angle, fS.type, "Longitude");
        var jarakLat = Jarak(jarakList, angle, fS.type, "Latitude");

        var routePoint = ToMission(newOrigin, fS.unitsInFormasi.Count, jarakLng, jarakLat, fS.type);

        // foreach(var child in routePoint)
        // Debug.Log("x: " + child.x + ", y: " + child.y);
        
        if(_poly != null) _poly.Dispose();
        for(int i = 0; i < fS.unitsInFormasi.Count; i++)
        {
            var pFF = fS.unitsInFormasi[i].GetComponent<PFormasiV2>();
            pFF.routePoints.Add(routePoint[i]);
            
            // _poly = AddElement(DrawLine(pFF.routePoints, Color.red, 1f));
        }

        if(poly != null) poly.Dispose();

        aLine = new(paths, Color.white, .5f);
        poly = AddElement(aLine);
    }

    // void PerformDrawFormasi(PFormasi pF)
    // {
    //     var satuan = pF.satuan;
    //     var formasi = pF.formasi;
    //     var d_satuan = 0;
    //     var d_formasi = 0;

    //     if(satuan == PFormasi.satuanJarak.Nautical_Miles)
    //     d_satuan = 0;
    //     else if(satuan == PFormasi.satuanJarak.Kilometer)
    //     d_satuan = 1;
    //     else if(satuan == PFormasi.satuanJarak.Feet)
    //     d_satuan = 2;
    //     else if(satuan == PFormasi.satuanJarak.Yard)
    //     d_satuan = 3;
    //     else if(satuan == PFormasi.satuanJarak.Meter)
    //     d_satuan = 4;

    //     if(formasi == PFormasi.formasiType.Belah_Ketupat)
    //     d_formasi = 1;
    //     else if(formasi == PFormasi.formasiType.Flank_Kiri)
    //     d_formasi = 2;
    //     else if(formasi == PFormasi.formasiType.Flank_Kanan)
    //     d_formasi = 3;
    //     else if(formasi == PFormasi.formasiType.Sejajar)
    //     d_formasi = 4;
    //     else if(formasi == PFormasi.formasiType.Serial)
    //     d_formasi = 5;
    //     else if(formasi == PFormasi.formasiType.Panah)
    //     d_formasi = 6;
    //     else if(formasi == PFormasi.formasiType.Kerucut)
    //     d_formasi = 7;
    //     else if(formasi == PFormasi.formasiType.Custom)
    //     d_formasi = 8;
        
    //     var n1 = new Vector2();
    //     var n2 = new Vector2();
    //     var routePoints = new List<Vector2>();

    //     var newOrigin = new Vector2();
    //     var target = Resources.Load<Texture2D>("Images/Icons/Unsorted/icon-filled-target");

    //     if(poly != null)
    //     poly.Dispose();

    //     double lng, lat;
    //     if(OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
    //     { newOrigin.x = (float)lng; newOrigin.y = (float)lat; }
    //     labelRoute += lat + "," + lng + Environment.NewLine;

    //     var m = OnlineMapsMarkerManager.instance.Create(newOrigin, target);
    //     m.scale = .25f;
    //     m["line"] = aLine;
    //     m["index"] = n;
    //     m.OnPress += MarkerLongPress;
    //     m.OnPositionChanged += changePosition;
    //     m.OnRelease += finishChangePostion;
    //     markers.Add(m);

    //     paths.Add(newOrigin);
    //     routePoints.Add(newOrigin);
        
    //     if(paths.Count == 1)
    //     n1 = paths[paths.Count - 1];
    //     else
    //     n1 = paths[paths.Count - 2];
        
    //     n2 = paths[paths.Count - 1];

    //     aLine = new OnlineMapsDrawingLine(paths, Color.white, .5f);
    //     poly = OnlineMapsDrawingElementManager.AddItem(aLine);
        
    //     var pointsVec = new Vector2[pF.listObjekFormasi.Count - 1].ToList();

    //     var arahV = pF.listObjekFormasi[0].GetComponent<PFormasi>().sudut;
    //     var angle = new DrawRoute().GetAngleOfLineBetweenTwoPoints(n1, n2) - 90;

    //     // if(d_formasi == 3 || d_formasi == 4)
    //     // angle -= 90;

    //     var jarakKegiatan = new List<float>();
                
    //     foreach(Transform child in pF.listObjekFormasi)
    //     if(child != pF.listObjekFormasi[0])
    //     jarakKegiatan.Add(child.GetComponent<PFormasi>().jarak);
        
    //     var newJarak = new PerhitunganFormasi().KonversiJarak(jarakKegiatan, d_satuan);
        
    //     var jarakLng = new List<float>();
    //     var jarakLat = new List<float>();

    //     if(d_formasi != 8)
    //     {
    //         jarakLng = new PerhitunganFormasi().Jarak(newJarak, arahV + angle, d_formasi, "Longitude");
    //         jarakLat = new PerhitunganFormasi().Jarak(newJarak, arahV + angle, d_formasi, "Latitude");
    //     }
    //     else
    //     {
    //         var arahArr = new List<float>();
    //         foreach(var item in pF.listObjekFormasi)
    //         {
    //             var pFF = item.GetComponent<PFormasi>();
    //             arahArr.Add(pFF.sudut);
    //         }

    //         jarakLng = new PerhitunganFormasi().Jarak(newJarak, arahArr, arahV + angle, "Longitude");
    //         jarakLat = new PerhitunganFormasi().Jarak(newJarak, arahArr, arahV + angle, "Latitude");
    //     }

    //     var formasiPoints = new List<Vector2>();
    //     if(d_formasi == 1)
    //     formasiPoints = new DrawRoute().Formasi1(jarakLng, jarakLat, newOrigin, pointsVec);
    //     else if(d_formasi == 2)
    //     formasiPoints = new DrawRoute().Formasi2(jarakLng, jarakLat, newOrigin, pointsVec);
    //     else if(d_formasi == 3)
    //     formasiPoints = new DrawRoute().Formasi2(jarakLng, jarakLat, newOrigin, pointsVec);
    //     else if(d_formasi == 4)
    //     formasiPoints = new DrawRoute().Formasi3(jarakLng, jarakLat, newOrigin, pointsVec);
    //     else if(d_formasi == 5)
    //     formasiPoints = new DrawRoute().Formasi4(jarakLng, jarakLat, newOrigin, pointsVec);
    //     else if(d_formasi == 6)
    //     formasiPoints = new DrawRoute().Formasi5(jarakLng, jarakLat, newOrigin, pointsVec);
    //     else if(d_formasi == 7)
    //     formasiPoints = new DrawRoute().Formasi6(jarakLng, jarakLat, newOrigin, pointsVec);
    //     else if(d_formasi == 8)
    //     formasiPoints = new DrawRoute().Formasi7(jarakLng, jarakLat, newOrigin, pointsVec);

    //     routePoints.AddRange(formasiPoints);

    //     for(int i = 0; i < pF.listObjekFormasi.Count; i++)
    //     {
    //         var pFF = pF.listObjekFormasi[i].GetComponent<PFormasi>();

    //         if(pFF.stat != null)
    //         {
    //             var pF2D = pFF.marker2D.instance.GetComponent<PFormasi>();
    //             for(int j = 0; j < routePoints.Count; j++)
    //             {
    //                 if(j == pFF.urutanFormasi)
    //                 pF2D.routePoints.Add(routePoints[j]);
    //             }
    //         }

    //         var pF3D = pFF.marker3D.instance.GetComponent<PFormasi>();
    //         for(int j = 0; j < routePoints.Count; j++)
    //         {
    //             if(j == pFF.urutanFormasi)
    //             {
    //                 if(pF3D.routePoints == null)
    //                 pF3D.routePoints = new List<Vector2>();
                    
    //                 pF3D.routePoints.Add(routePoints[j]);
    //             }
    //         }
    //     }

    //     n++;
    //     // foreach(Vector2 _point in formasiPoints)
    //     // {
    //     //     var _m = OnlineMapsMarkerManager.instance.Create(_point, target);
    //     //     _m.scale = .25f;
    //     // }
    // }

    private void MarkerLongPress(OnlineMapsMarkerBase marker)
    {
        // Starts moving the marker.
        OnlineMapsControlBase.instance.dragMarker = marker;
        OnlineMapsControlBase.instance.isMapDrag = false;
    }

    private void changePosition(OnlineMapsMarkerBase marker)
    {
        var line = (OnlineMapsDrawingLine)marker["line"];
        var ls = (List<Vector2>)line.points;
        ls[(int)marker["index"]] = marker.position;
    }

    public void confirmDraw()
    {
        if (!isDrawing && !isEditing) return;

        var lines = (aLine != null) ? aLine : editedRoute.lines;
        List<Vector2> pts = (List<Vector2>) lines.points;
        Vector2 pos = pts[0];
        
        for(var i = 1; i < pts.Count; i++)
        {
            dis += OnlineMapsUtils.DistanceBetweenPoints(pos, pts[i]).magnitude;
            pos = pts[i];
        }

        OnlineMapsControlBase.instance.OnMapClick = null;
        isDrawing = false;
        modalDraw.GetComponent<Animator>().Play("FadeOut");
        setEstimasi();
        Debug.Log("Jarak (KM) : " + dis);
        Debug.Log("Kecepatan : " + inputKecepatan.text);
        Debug.Log("Waktu ditempuh : " + inputWaktuSampai.text);

        foreach (OnlineMapsMarker m in markers)
        {
            OnlineMapsMarkerManagerBase<OnlineMapsMarkerManager, OnlineMapsMarker>.RemoveItem(m, true);
            m.Dispose();
        }

        markers.Clear();
        //inputKoordinat.text = labelRoute;
        modalRute.SetActive(true);
        modalRute.GetComponent<Animator>().Play("FadeIn");
        OnlineMaps.instance.Redraw();
    }

    public void confirmPlotRoute()
    {
        if (isEditing && editedRoute != null) return;
        // Saat Selesai Plotting
        var pF = markerInstance.instance.GetComponent<PFormasi>();
        if(pF.formasi != PFormasi.formasiType.None)
        {
            if(pF.listObjekFormasi.Count > 0 && pF.routePoints.Count > 0)
            {
                var namaRute = inputNamaRute.text;
                var descRute = inputNamaKegiatan.text;
                var speedRoute = inputKecepatan.text;
                var d_speedRoute = dropdownKecepatan.options[dropdownKecepatan.value].text;

                for(int i = 0; i< pF.listObjekFormasi.Count; i++)
                {
                    var lo = pF.listObjekFormasi[i];
                    var peef = lo.GetComponent<PFormasi>();
                    if(peef == peef.routePoints.Count > 0)
                    {
                        nPoly++;
                        // aLine.name = "Route " + nPoly;
                        //GameObject.Find("Route " + nPoly).tag = "Route Lines";
                        var man = OnlineMapsDrawingElementManager.AddItem(new OnlineMapsDrawingLine(peef.routePoints, Color.white, 0));
                        aLine = (OnlineMapsDrawingLine)man;
                        aLine.name = "Route " + nPoly;

                        GameObject go = new GameObject(generateIDMisiPasukan(lo.name));
                        go.tag = "entity-route";
                        go.transform.parent = routeData;
                        PlotDataRoute mRoute = go.AddComponent<PlotDataRoute>();

                        mRoute.id_satuan = lo.name;
                        // mRoute.heading_satuan = PlotSatuanController.Instance.selectedEditMarker.rotationY;
                        
                        mRoute.id_mission = go.name;
                        // int count = GameObject.FindGameObjectsWithTag("entity-route").Length;
                        // mRoute.routeID = count.ToString();
                        mRoute.routeID = aLine.name;
                        mRoute.lines = aLine;
                        mRoute.routeName = namaRute;
                        mRoute.routeDesc = descRute;
                        Debug.Log("Kecepatan " + go.name + " : " + speedRoute);
                        mRoute.routeSpeed = float.Parse(speedRoute);
                        mRoute.routeDistance = dis;
                        mRoute.routeDistanceUnit = d_speedRoute;
                        mRoute.routeTime = estimateTimeFMin(dis, speedRoute, d_speedRoute);
                        mRoute.routeStart = date;
                        mRoute.routeEnd = date.AddMinutes(mRoute.routeTime);
                        mRoute.kegiatanID = IDKegiatan;

                        for(int j = 0; j < peef.routePoints.Count; j++)
                        {
                            var r = peef.routePoints[j];
                            // Debug.Log("Parameter ke-" + (i + 1) + " : " + paths[i].x + "," + paths[i].y);
                            mRoute.AddNewPos("", new Vector3(r.x, r.y), j);
                        }

                        mRoute.lines["PlotDataRoute"] = mRoute;
                        mRoute.lines.width = 0;
                        mRoute.generateProperties();

                        listRoute.Add(mRoute);
                        lo.GetComponent<JSONDataMisiSatuan>().listRoute.Add(mRoute);

                        if(peef.stat != null)
                        peef.marker2D.instance.GetComponent<JSONDataMisiSatuan>().listRoute.Add(mRoute);
                        peef.marker3D.instance.GetComponent<JSONDataMisiSatuan>().listRoute.Add(mRoute);

                        if(i == 0)
                        setListRoute(false, mRoute);
                    }
                }
                n = 0;
                labelRoute = "";
                dis = 0;

                clearDrawings();
                //btnToggler.GetComponent<SidemenuToggler>().toggleOff();
                clear();
            }
        }
        else
        {
            if (n > 0)
            {
                nPoly++;
                aLine.name = "Route " + nPoly;
                //GameObject.Find("Route " + nPoly)e.tag = "Route Lines";

                GameObject go = new GameObject(generateIDMisi());
                go.tag = "entity-route";
                go.transform.parent = routeData;
                PlotDataRoute mRoute = go.AddComponent<PlotDataRoute>();

                mRoute.id_satuan = PlotSatuanController.Instance.selectedEditMarker.instance.GetComponent<JSONDataSatuan>().nama;
                mRoute.heading_satuan = PlotSatuanController.Instance.selectedEditMarker.rotationY;

                mRoute.id_mission = go.name;
                int count = GameObject.FindGameObjectsWithTag("entity-route").Length;
                mRoute.routeID = count.ToString();
                mRoute.lines = aLine;
                mRoute.routeName = inputNamaRute.text;
                mRoute.routeType = getRuteType(dropdownJenis.value);
                mRoute.routeDesc = inputNamaKegiatan.text;
                mRoute.routeSpeed = float.Parse(inputKecepatan.text);
                mRoute.routeDistance = dis;
                mRoute.routeDistanceUnit = dropdownKecepatan.options[dropdownKecepatan.value].text;
                mRoute.routeTime = estimateTimeFMin(dis, inputKecepatan.text, dropdownKecepatan.options[dropdownKecepatan.value].text);
                mRoute.routeStart = date;
                mRoute.routeEnd = date.AddMinutes(mRoute.routeTime);
                mRoute.kegiatanID = IDKegiatan;

                //var pos = (paths != null) ? paths : (List<Vector2>) mRoute.lines.points;
                List<Vector2> pos = (List<Vector2>)mRoute.lines.points;
                for (int x = 0; x < pos.Count; x++)
                { 
                    //Debug.Log("Parameter ke-" + (x + 1) + " : " + paths[x].x + "," + paths[x].y);
                    mRoute.AddNewPos("", new Vector3(pos[x].x, pos[x].y), x);
                }

                mRoute.lines["PlotDataRoute"] = mRoute;
                mRoute.lines.width = 0;
                mRoute.generateProperties();

                listRoute.Add(mRoute);
                PlotSatuanController.Instance.selectedEditMarker.instance.GetComponent<JSONDataMisiSatuan>().listRoute.Add(mRoute);

                setListRoute(false, mRoute);

                n = 0;
                labelRoute = "";
                dis = 0;

                clearDrawings();
                //btnToggler.GetComponent<SidemenuToggler>().toggleOff();
                clear();
            }
        }
    }

    #region IZZAN
    public void confirmPlotRoutePasukan()
    {
        if (isEditing && editedRoute != null) return;
        // Saat Selesai Plotting
        var pF = markerInstance.instance.GetComponent<PFormasi>();
        if(pF.formasi != PFormasi.formasiType.None)
        {
            if(pF.listObjekFormasi.Count > 0 && pF.routePoints.Count > 0)
            {
                Debug.Log("Plot executed!");
                var namaRute = inputNamaRute.text;
                var descRute = inputNamaKegiatan.text;
                var speedRoute = inputKecepatan.text;
                var d_speedRoute = dropdownKecepatan.options[dropdownKecepatan.value].text;

                for(int i = 0; i < pF.listObjekFormasi.Count; i++)
                {
                    var lo = pF.listObjekFormasi[i];
                    var peef = lo.GetComponent<PFormasi>();
                    if(peef == peef.routePoints.Count > 0)
                    {
                        nPoly++;
                        // aLine.name = "Route " + nPoly;
                        //GameObject.Find("Route " + nPoly).tag = "Route Lines";
                        var man = OnlineMapsDrawingElementManager.AddItem(new OnlineMapsDrawingLine(peef.routePoints, Color.white, 0));
                        aLine = (OnlineMapsDrawingLine)man;
                        aLine.name = "Route " + nPoly;

                        GameObject go = new GameObject(generateIDMisi(lo.name));
                        go.tag = "entity-route";
                        go.transform.parent = routeData;
                        PlotDataRoute mRoute = go.AddComponent<PlotDataRoute>();

                        mRoute.id_satuan = lo.name;
                        // mRoute.heading_satuan = PlotSatuanController.Instance.selectedEditMarker.rotationY;
                        
                        mRoute.id_mission = go.name;
                        // int count = GameObject.FindGameObjectsWithTag("entity-route").Length;
                        // mRoute.routeID = count.ToString();
                        mRoute.routeID = aLine.name;
                        mRoute.lines = aLine;
                        mRoute.routeName = namaRute;
                        mRoute.routeDesc = descRute;
                        Debug.Log("Kecepatan " + go.name + " : " + speedRoute);
                        mRoute.routeSpeed = float.Parse(speedRoute);
                        mRoute.routeDistance = dis;
                        mRoute.routeDistanceUnit = d_speedRoute;
                        mRoute.routeTime = estimateTimeFMin(dis, speedRoute, d_speedRoute);
                        mRoute.routeStart = date;
                        mRoute.routeEnd = date.AddMinutes(mRoute.routeTime);
                        mRoute.kegiatanID = IDKegiatan;

                        for(int j = 0; j < peef.routePoints.Count; j++)
                        {
                            var r = peef.routePoints[j];
                            // Debug.Log("Parameter ke-" + (i + 1) + " : " + paths[i].x + "," + paths[i].y);
                            mRoute.AddNewPos("", new Vector3(r.x, r.y), j);
                        }

                        mRoute.lines["PlotDataRoute"] = mRoute;
                        mRoute.lines.width = 0;
                        mRoute.generateProperties();

                        listRoute.Add(mRoute);
                        lo.GetComponent<JSONDataMisiSatuan>().listRoute.Add(mRoute);
                        
                        if(peef.stat != null)
                        peef.marker2D.instance.GetComponent<JSONDataMisiSatuan>().listRoute.Add(mRoute);
                        peef.marker3D.instance.GetComponent<JSONDataMisiSatuan>().listRoute.Add(mRoute);

                        if(i == 0)
                        setListRoute(false, mRoute);
                    }
                }
                n = 0;
                labelRoute = "";
                dis = 0;

                clearDrawings();
                //btnToggler.GetComponent<SidemenuToggler>().toggleOff();
                clear();
            }
        }
        else 
        {
            if (n > 0)
            {

                nPoly++;
                aLine.name = "Route " + nPoly;
                //GameObject.Find("Route " + nPoly).tag = "Route Lines";

                GameObject go = new GameObject(generateIDMisiPasukan());
                go.tag = "entity-route";
                go.transform.parent = routeData;
                PlotDataRoute mRoute = go.AddComponent<PlotDataRoute>();
                mRoute.routeID = aLine.name;
                mRoute.lines = aLine;
                mRoute.routeName = inputNamaRute.text;
                mRoute.routeDesc = inputNamaKegiatan.text;
                mRoute.routeSpeed = float.Parse(inputKecepatan.text);
                mRoute.routeDistance = dis;
                mRoute.routeDistanceUnit = dropdownKecepatan.options[dropdownKecepatan.value].text;
                mRoute.routeTime = estimateTimeFMin(dis, inputKecepatan.text, dropdownKecepatan.options[dropdownKecepatan.value].text);
                mRoute.routeStart = date;
                mRoute.routeEnd = date.AddMinutes(mRoute.routeTime);

                List<Vector2> pos = (List<Vector2>) mRoute.lines.points;
                for (int i = 0; i < pos.Count; i++)
                {
                    Debug.Log("Parameter ke-" + (i + 1) + " : " + pos[i].x + "," + pos[i].y);
                    mRoute.AddNewPos("", new Vector3(pos[i].x, pos[i].y), i);
                }

                mRoute.lines["PlotDataRoute"] = mRoute;
                mRoute.lines.width = 0;
                mRoute.generateProperties();

                listRoute.Add(mRoute);
                PlotPasukanController.instance.edit2D.instance.GetComponent<JSONDataMisiSatuan>().listRoute.Add(mRoute);
                PlotPasukanController.instance.edit3D.instance.GetComponent<JSONDataMisiSatuan>().listRoute.Add(mRoute);

                setListRoute(false, mRoute);

                n = 0;
                labelRoute = "";
                dis = 0;

                clearDrawings();
                //btnToggler.GetComponent<SidemenuToggler>().toggleOff();
                clear();
            }
        }
    }
    
    public void confirmEditRoutePasukan()
    {
        if (!isEditing && editedRoute == null) return;
        // Saat Selesai Plotting
        if (n > 0)
        {
            editedRoute.data.Clear();
            editedRoute.lines.Dispose();

            aLine.name = editedRoute.routeID;
            editedRoute.lines = aLine;
            editedRoute.routeName = inputNamaRute.text;
            editedRoute.routeSpeed = float.Parse(inputKecepatan.text);
            editedRoute.routeDistance = dis;
            editedRoute.routeDistanceUnit = dropdownKecepatan.options[dropdownKecepatan.value].text;
            editedRoute.routeTime = estimateTimeFMin(dis, inputKecepatan.text, dropdownKecepatan.options[dropdownKecepatan.value].text);
            editedRoute.routeStart = date;
            editedRoute.routeEnd = date.AddMinutes(editedRoute.routeTime);

            List<Vector2> pos = (List<Vector2>) editedRoute.lines.points;
            for (int x = 0; x < pos.Count; x++)
            {
                editedRoute.AddNewPos("", new Vector3(pos[x].x, pos[x].y), x);
            }

            editedRoute.lines["PlotDataRoute"] = editedRoute;
            editedRoute.lines.width = 0;
        }
        else
        {
            editedRoute.routeName = inputNamaRute.text;
            editedRoute.routeDistanceUnit = dropdownKecepatan.options[dropdownKecepatan.value].text;
            editedRoute.routeTime = estimateTimeFMin(editedRoute.routeDistance, editedRoute.routeSpeed.ToString(), dropdownKecepatan.options[dropdownKecepatan.value].text);
            editedRoute.routeStart = date;
            editedRoute.routeEnd = date.AddMinutes(editedRoute.routeTime);
        }

        n = 0;
        labelRoute = "";
        dis = 0;
        clearDrawings();
        setListRoute(true);
        //btnToggler.GetComponent<SidemenuToggler>().toggleOff();
        clear();
    }
    
    public void ConfirmDeleteRoutePasukan(Boolean delete)
    {
        if (delete)
        {
            listRoute.Remove(deletedRoute);
            PlotPasukanController.instance.edit2D.instance.GetComponent<JSONDataMisiSatuan>().listRoute.Remove(deletedRoute);
            PlotPasukanController.instance.edit3D.instance.GetComponent<JSONDataMisiSatuan>().listRoute.Remove(deletedRoute);
            Destroy(deletedRoute.gameObject);
            Destroy(deletedRoute);
            deletedRoute = null;
            setListRoute(true);
        }
        modalDelete.GetComponent<Animator>().Play("FadeOut");
    }
    #endregion

    public void confirmEditRoute()
    {
        if (!isEditing && editedRoute == null) return;
        // Saat Selesai Plotting
        if (n > 0)
        {
            editedRoute.data.Clear();
            if(aLine != null)
            {
                editedRoute.lines.Dispose();
                editedRoute.lines = aLine;
            }

            editedRoute.id_satuan = PlotSatuanController.Instance.selectedEditMarker.instance.GetComponent<JSONDataSatuan>().nama;
            editedRoute.heading_satuan = PlotSatuanController.Instance.selectedEditMarker.rotationY;

            aLine.name = editedRoute.routeID;
            editedRoute.routeName = inputNamaRute.text;
            editedRoute.routeType = getRuteType(dropdownJenis.value);
            editedRoute.routeSpeed = float.Parse(inputKecepatan.text);
            editedRoute.routeDistance = dis;
            editedRoute.routeDistanceUnit = dropdownKecepatan.options[dropdownKecepatan.value].text;
            editedRoute.routeTime = estimateTimeFMin(dis, inputKecepatan.text, dropdownKecepatan.options[dropdownKecepatan.value].text);
            editedRoute.routeStart = date;
            editedRoute.routeEnd = date.AddMinutes(editedRoute.routeTime);
            editedRoute.kegiatanID = IDKegiatan;

            List<Vector2> pos = (List<Vector2>)editedRoute.lines.points;
            for (int x = 0; x < pos.Count; x++)
            {
                //Debug.Log("Parameter ke-" + (x + 1) + " : " + paths[x].x + "," + paths[x].y);
                editedRoute.AddNewPos("", new Vector3(pos[x].x, pos[x].y), x);
            }

            editedRoute.generateProperties();
            editedRoute.lines["PlotDataRoute"] = editedRoute;
            editedRoute.lines.width = 0;
        }
        else
        {
            editedRoute.routeName = inputNamaRute.text;
            editedRoute.routeDistanceUnit = dropdownKecepatan.options[dropdownKecepatan.value].text;
            editedRoute.routeTime = estimateTimeFMin(editedRoute.routeDistance, editedRoute.routeSpeed.ToString(), dropdownKecepatan.options[dropdownKecepatan.value].text);
            editedRoute.routeStart = date;
            editedRoute.routeEnd = date.AddMinutes(editedRoute.routeTime);
        }

        n = 0;
        labelRoute = "";
        dis = 0;
        clearDrawings();
        setListRoute(true);
        //btnToggler.GetComponent<SidemenuToggler>().toggleOff();
        clear();
    }

    public void clear()
    {
        //inputNamaSatuan.text = "";
        inputNamaRute.text = "";
        inputNamaKegiatan.text = "";
        //inputTanggal.text = "";
        inputKecepatan.text = "";
        dropdownKecepatan.value = 0;
        dropdownJenis.value = 0;
        //inputRute.text = "";
        //inputKoordinat.text = "";
        inputWaktuSampai.text = "";
        IDKegiatan = null;
        descKegiatan = null;
        selectedListMisi = null;


        modalRute.GetComponent<DateInputHelper>().resetTime();

        if (editedRoute != null) editedRoute = null;
        if (isEditing) isEditing = false;
    }

    public string estimateTime(float distance, string speed, string speed_type)
    {
        //Distance defined in Kilometers
        //Speed measured per hour;
        double kecepatan = GetKecepatanByType(speed, speed_type);
        float jam = distance / (float)kecepatan;
        float fr = (jam - (int)jam) * 60;
        int min = (int)fr;

        return (int)jam + " jam " + (int)min + " menit ";
    }

    public float estimateTimeFMin(float distance, string speed, string speed_type)
    {
        //Distance defined in Kilometers
        //Speed measured per hour;
        double kecepatan = GetKecepatanByType(speed, speed_type);
        float menit = distance / (float)kecepatan * 60;

        return menit;
    }

    public static float estimateTimeFMin(float distance, string speed, string speed_type, bool isStatic = true)
    {
        //Distance defined in Kilometers
        //Speed measured per hour;
        double kecepatan = GetKecepatanByType(speed, speed_type);
        float menit = distance / (float)kecepatan * 60;

        return menit;
    }    

    public void setListRoute(bool update, PlotDataRoute addedRoute = null)
    {
        if (update)
        {
            if (listContainer.transform.childCount > 0)
            {
                foreach (Transform child in listContainer.transform)
                {
                    Destroy(child.gameObject);
                }
            }

            if (listRoute.Count > 0)
            {
                foreach (var l in listRoute)
                {
                    appedListRoute(l);
                }
            }
        }
        else
        {
            appedListRoute(addedRoute);
        }

        btnToggler.GetComponent<SidemenuToggler>().toggleOff();
        //listContainer.transform.parent.parent.parent.parent.gameObject.SetActive(true);
        //listContainer.transform.parent.parent.parent.parent.GetComponent<Animator>().Play("FadeIn");
    }

    private void appedListRoute(PlotDataRoute m)
    {
        GameObject inst = Instantiate(prefabList, listContainer.transform);
        ListSatuanRoute ls = inst.GetComponent<ListSatuanRoute>();
        inst.name = "LIST_" + m.routeID;
        ls.cRoute = this;
        ls.mRoute = m;
        ls.namaRoute.text = m.routeName;
        ls.descRoute.text = m.routeDesc;
        ls.IDKegiatan = IDKegiatan;
        //ls.descRoute.text = m.routeStart.ToString() + ", " + m.data[0].coordinate.ToString();
        ls.distanceRoute.text = 
            "(WA) " + m.routeStart.ToString("dd-MM-yyyy hh:mm") + "\n" +
            "(WS) " + m.routeEnd.ToString("dd-MM-yyyy hh:mm") + "\n" +
            "(S) " + m.routeDistance.ToString() + " " + m.routeDistanceUnit;

        //ls.distanceRoute.text = m.routeDistance.ToString() + " " + m.routeDistanceUnit;
        //inst.transform.localScale = new Vector3(1, 1, 1);

        inst.GetComponent<Button>().onClick.AddListener(delegate { SelectMisi(ls); });
    }

    public void AddRoute()
    {
        clear();
        clearDrawings();
        isEditing = false;
        isDrawing = false;
        //modalRute.GetComponent<Animator>().Play("FadeIn");
        //listContainer.transform.parent.parent.parent.parent.GetComponent<Animator>().Play("FadeOut");
    }

    public void EditRoute(PlotDataRoute mr)
    {
        if (mr != null)
        {
            isEditing = true;
            isDrawing = false;
            editedRoute = mr;
            Debug.Log(editedRoute);
            modalRute.GetComponent<Animator>().Play("FadeIn");
            inputNamaRute.text = editedRoute.routeName;
            dropdownJenis.value = getIndexRuteType(editedRoute.routeType);
            inputKecepatan.text = editedRoute.routeSpeed.ToString();
            dropdownKecepatan.value = dropdownKecepatan.options.FindIndex(option => option.text == editedRoute.routeDistanceUnit);
            IDKegiatan = editedRoute.kegiatanID;
            descKegiatan = editedRoute.routeDesc;
            setEstimasi();
            for (var i = 0; i < editedRoute.data.Count; i++)
            {
                labelRoute += Environment.NewLine + editedRoute.data[i].coordinate.x + "," + editedRoute.data[i].coordinate.y;
            }

            modalRute.GetComponent<DateInputHelper>().date = mr.routeStart;
            modalRute.GetComponent<DateInputHelper>().refreshUI();

            //inputKoordinat.text = labelRoute;
            modalRute.GetComponent<DateInputHelper>().date = mr.routeStart;
            labelRoute = null;
        }
        btnToggler.GetComponent<SidemenuToggler>().toggleOn();
    }

    public void DeleteRoute(PlotDataRoute mr)
    {
        if (mr != null)
        {
            if(mr.ID != null)
            {
                listDeletedRoute.Add(mr.ID);
            }

            deletedRoute = mr;
            modalDelete.GetComponent<Animator>().Play("FadeIn");
        }
    }

    public void ConfirmDeleteRoute(Boolean delete)
    {
        if (delete)
        {
            listRoute.Remove(deletedRoute);
            PlotSatuanController.Instance.selectedEditMarker.instance.GetComponent<JSONDataMisiSatuan>().listRoute.Remove(deletedRoute);
            Destroy(deletedRoute.gameObject);
            Destroy(deletedRoute);
            deletedRoute = null;
            setListRoute(true);
        }
        modalDelete.GetComponent<Animator>().Play("FadeOut");
    }

    public void setEstimasi()
    {
        int spd;
        int.TryParse(inputKecepatan.text, out spd);

        if (!isEditing)
        {
            if (dis < 1 || spd < 1)
            {
                inputWaktuSampai.text = "";
            }
            else
            {
                inputWaktuSampai.text = estimateTime(dis, inputKecepatan.text, dropdownKecepatan.options[dropdownKecepatan.value].text);
            }
        }
        else
        {
            if (n > 0)
            {
                inputWaktuSampai.text = estimateTime(dis, inputKecepatan.text, dropdownKecepatan.options[dropdownKecepatan.value].text);
            }
            else
            {
                inputWaktuSampai.text = estimateTime(editedRoute.routeDistance, inputKecepatan.text, dropdownKecepatan.options[dropdownKecepatan.value].text);
            }
        }

    }

    public void openModalMisi()
    {
        if (listContainer.transform.childCount > 0)
        {
            foreach (Transform child in listContainer.transform)
            {   
                Destroy(child.gameObject);
            }
        }

        var misiSatuanSelected = PlotSatuanController.Instance.selectedEditMarker.instance.GetComponent<JSONDataMisiSatuan>().listRoute;
        for (int i=0; i < misiSatuanSelected.Count; i++)
        {
            appedListRoute(misiSatuanSelected[i]);
        }
    }

    public void SelectKegiatan(TimetableData data)
    {
        IDKegiatan = data.kegiatanID;
        descKegiatan = data.kegiatanDesc;

        inputNamaKegiatan.text = data.kegiatanDesc.Replace("\n", " ").Replace("\r", " ");
        modalRute.GetComponent<DateInputHelper>().date = data.timeKegiatan;
        modalRute.GetComponent<DateInputHelper>().refreshUI();
    }

    public void SelectMisi(ListSatuanRoute data)
    {
        if(selectedListMisi != null)
        {

        }

        editGroup.SetActive(true);
        selectedListMisi = data;
    }

    public void editSelectedMisi()
    {
        if (selectedListMisi == null) return;

        selectedListMisi.StartEditRoute();
        editGroup.SetActive(false);

        selectedListMisi = null;
    }

    public void deleteSelectedMisi()
    {
        if (selectedListMisi == null) return;

        selectedListMisi.StartDeleteRoute();
        editGroup.SetActive(false);

        selectedListMisi = null;

    }

    public string getRuteType(int value)
    {
        return (value == 0) ? "pergerakan" : (value == 1) ? "embar" : (value == 2) ? "debar" : "pergerakan";
    }

    public int getIndexRuteType(string value)
    {
        return (value == "pergerakan") ? 0 : (value == "embar") ? 1 : (value == "debar") ? 2 : 0;
    }

    // Edit Rute
    public void startEditRouteLine()
    {
        if (editedRoute == null && aLine == null) return;
        dis = 0;
        var line = (aLine != null) ? aLine : editedRoute.lines;
        line.width = 0.25f;
        spawnMarkers(line);
        isDrawing = true;
        modalDraw.GetComponent<Animator>().Play("FadeIn");
        modalRute.GetComponent<Animator>().Play("FadeOut");
    }

    private void spawnMarkers(OnlineMapsDrawingLine ln)
    {
        int i = 0;
        foreach (Vector2 pts in ln.points as List<Vector2>)
        {
            addMarker(new Vector2(pts.x, pts.y), ln, i);
            i++;
            n++;
        }
    }

    public void addMarker(Vector2 pos, OnlineMapsDrawingLine ln, int index)
    {
        OnlineMapsMarker mm = OnlineMapsMarkerManager.CreateItem(pos.x, pos.y, routePoint, "Titik Rute ke-" + index);
        markers.Add(mm);
        mm["line"] = ln;
        mm["index"] = index;
        mm["index"] = index;
        mm.OnPress += MarkerLongPress;
        mm.OnPositionChanged += changePositionEdit;
        mm.OnRelease += finishChangePostion;
    }

    private void changePositionEdit(OnlineMapsMarkerBase marker)
    {
        var line = (OnlineMapsDrawingLine)marker["line"];
        var ls = (List<Vector2>)line.points;
        ls[(int)marker["index"]] = marker.position;

        //if (line.instance.GetComponent<DrawStat>() != null)
        //{
        //    DrawStat ds = line.instance.GetComponent<DrawStat>();
        //    ds.points[(int)marker["index"]] = marker.position;
        //    ds.geometry = "{\"type\": \"Polygon\", \"coordinates\": [[" + ListVectorToText(ds.points) + "]]}";
        //}
    }

    private void finishChangePostion(OnlineMapsMarkerBase marker)
    {
        var line = (OnlineMapsDrawingLine)marker["line"];
        var ls = (List<Vector2>)line.points;
        ls[(int)marker["index"]] = marker.position;
    }

    // Format id_mission : mission_<angka>_<id_satuan/id_object>_<epoch_timestamp> 
    private string generateIDMisi(string nama = null)
    {
        var index_misi = GameObject.FindGameObjectsWithTag("entity-route").Length;
        bool isExist = true;

        do
        {
            isExist = checkMisiIndex(index_misi);
            index_misi++;
        } while (isExist);

        TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0);
        int epoch_time = (int)t.TotalSeconds;

        if(nama == null)
        return "mission_" + index_misi + "_" + PlotSatuanController.Instance.selectedEditMarker.instance.GetComponent<JSONDataSatuan>().nama + "_" + DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
        else
        return "mission_" + index_misi + "_" + nama + "_" + DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
    }

    private string generateIDMisiPasukan(string nama = null)
    {
        var index_misi = GameObject.FindGameObjectsWithTag("entity-route").Length;
        bool isExist = true;

        do
        {
            isExist = checkMisiIndex(index_misi);
            index_misi++;
        } while (isExist);

        TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0);
        int epoch_time = (int)t.TotalSeconds;

        if(nama == null)
        return "mission_" + index_misi + "_" + PlotPasukanController.instance.edit3D.instance.GetComponent<Stat>().namaSatuan + "_" + DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
        else
        return "mission_" + index_misi + "_" + nama + "_" + DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
    }

    private bool checkMisiIndex(int index)
    {
        bool isExist = false;

        foreach (GameObject misi in GameObject.FindGameObjectsWithTag("entity-route"))
        {
            if (misi.name.Contains("mission" + index)) isExist = true;
        }

        return isExist;
    }
}