using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SlotSatuanController : MonoBehaviour
{
    private bool onDrag = false;
    private GameObject TEMP_LIST;

    [SerializeField] private Canvas canvas;
    private RectTransform rectTransform;

    #region INSTANCE
    public static SlotSatuanController Instance;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            canvas = GetComponentInParent<Canvas>();
            rectTransform = GetComponent<RectTransform>();
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    public void BeginDragSatuan(GameObject list)
    {
        if (list.GetComponent<PlotSatuanCellView>().objPrefab == null) return;

        Debug.Log("Begin Drag");
        TEMP_LIST = Instantiate(list, canvas.transform);

        if (list.GetComponent<PlotSatuanCellView>().isSlot)
        {
            TEMP_LIST.GetComponent<RectTransform>().sizeDelta = new Vector2 (70, 70);
        }

        onDrag = true;

        TEMP_LIST.GetComponent<CanvasGroup>().alpha = 0.6f;
        TEMP_LIST.GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (TEMP_LIST == null) return;

        rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
    }

    public void EndDragSatuan(PointerEventData eventData)
    {
        if (TEMP_LIST == null) return;

        List<GameObject> hoveredList = eventData.hovered;
        foreach (var GO in hoveredList)
        {
            if (GO.GetComponent<PlotSatuanCellView>() != null)
            {
                if (!GO.GetComponent<PlotSatuanCellView>().isSlot) continue;

                Debug.Log("Dropped To " + GO.name);
                var slotSelected = GO.GetComponent<PlotSatuanCellView>();
                var tempData = TEMP_LIST.GetComponent<PlotSatuanCellView>();

                slotSelected.cellIdentifier         = tempData.cellIdentifier;
                slotSelected.satuanImage.sprite     = tempData.satuanImage.sprite;
                slotSelected.satuanImage.color      = Color.white;
                slotSelected.objPrefab              = tempData.objPrefab;
                slotSelected.obj3D                  = tempData.obj3D;
                slotSelected.StyleSatuan            = tempData.StyleSatuan;
                slotSelected.InfoSatuan             = tempData.InfoSatuan;
                slotSelected.SymbolSatuan           = tempData.SymbolSatuan;
            }
        }

        Destroy(TEMP_LIST);
    }

    public void EndDragSlotSatuan(PointerEventData eventData, PlotSatuanCellView data)
    {
        if (TEMP_LIST == null) return;

        List<GameObject> hoveredList = eventData.hovered;
        foreach (var GO in hoveredList)
        {
            if (GO.GetComponent<PlotSatuanCellView>() != null)
            {
                Debug.Log("Dropped To " + GO.name);
                var slotSelected = GO.GetComponent<PlotSatuanCellView>();
                var tempData = TEMP_LIST.GetComponent<PlotSatuanCellView>();

                slotSelected.cellIdentifier = tempData.cellIdentifier;
                slotSelected.satuanImage.sprite = tempData.satuanImage.sprite;
                slotSelected.satuanImage.color = Color.white;
                slotSelected.objPrefab = tempData.objPrefab;
                slotSelected.obj3D = tempData.obj3D;
                slotSelected.StyleSatuan = tempData.StyleSatuan;
                slotSelected.InfoSatuan = tempData.InfoSatuan;
                slotSelected.SymbolSatuan = tempData.SymbolSatuan;

                var temp_image = data.satuanImage;

                data.reset();
            }
        }

        Destroy(TEMP_LIST);
    }

    public void OnDrop(PointerEventData eventData)
    {
        
    }

    private void Update()
    {
        if (!onDrag) return;
        if (TEMP_LIST == null) return;

        Vector2 movePos;

        RectTransformUtility.ScreenPointToLocalPointInRectangle(
                canvas.transform as RectTransform,
                Input.mousePosition, canvas.worldCamera,
            out movePos);

        TEMP_LIST.transform.position = canvas.transform.TransformPoint(movePos);
    }
}
