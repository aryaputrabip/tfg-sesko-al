using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

using TMPro;

public class TerrainController : MonoBehaviour
{
    [Header("References")]
    public Toggle toggler;
    public TMP_Dropdown terrainService;

    public async void ToggleTerrain()
    {
        refreshComponent();
        if (toggler.isOn)
        {
            await Task.Delay(500);
            switch (terrainService.value)
            {
                case 0:
                    OnlineMaps.instance.gameObject.AddComponent<OnlineMapsMapboxElevationManager>();

                    break;
                case 1:
                    OnlineMaps.instance.gameObject.AddComponent<OnlineMapsBingMapsElevationManager>();
                    break;
                default: break;
            }
        }
    }

    private void refreshComponent()
    {
        Destroy(OnlineMaps.instance.GetComponent<OnlineMapsMapboxElevationManager>());
        Destroy(OnlineMaps.instance.GetComponent<OnlineMapsBingMapsElevationManager>());
    }
}
