using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.UI;
using TMPro;

using GlobalVariables;
using HelperKegiatan;
using System.Globalization;

public class TimetableController : MonoBehaviour
{
    [Header("Scroll Rect References")]
    [SerializeField] private ScrollRect timetable;
    [SerializeField] private ScrollRect hari_h;

    [Header("References")]
    [SerializeField] private GameObject prefabHariH;
    [SerializeField] private GameObject prefabList;
    [SerializeField] private GameObject prefabKegiatan;
    [SerializeField] private TextMeshProUGUI labelDate;
    [SerializeField] private Button btnPlay;

    private int selected_hari_h;
    [SerializeField] private GameObject selectedKegiatan;

    public List<int> hariH;
    public List<DateTime> hariHDate;

    API api;

    private void Start()
    {
        foreach (Transform item in hari_h.content.transform)
        {
            Destroy(item.gameObject);
        }

        foreach (Transform item in timetable.content.transform)
        {
            Destroy(item.gameObject);
        }
    }

    public async Task<TimetableKegiatan[]> InitTimetable()
    {
        api = new API();
        hariH = new List<int>();
        hariHDate = new List<DateTime>();

        //Config.SERVICE_LOGIN = "http://localhost/eppkm/public";
        //Config.SERVICE_CB = "http://localhost/eppkm_simulasi";

        await api.GetHariH();
        await api.GetSkenarioAktif();

        var kegiatan = TimetableKegiatan.FromJson(await api.GetKegiatan(SkenarioAktif.ID_SKENARIO.ToString()));
        for (int i = 0; i < kegiatan.Length; i++)
        {
            DateTime waktuDT;
            if (DateTime.TryParseExact(kegiatan[i].waktu, "MM-dd-yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out waktuDT))
            {
                kegiatan[i].waktuDT = waktuDT;
            }
            else
            {
                kegiatan[i].waktu = "";
            }
        }

        // Get Hari-H Kegiatan
        await AddHariHBaru(kegiatan);

        // Get Waktu Kegiatan
        await AddWaktuBaru(kegiatan);

        // Get Data Kegiatan
        await AddKegiatanBaru(kegiatan);

        // Set Default Aktifkan Hari-H Tanggal Terlama
        ChangeHariH(selected_hari_h);

        // Set Default Selected Kegiatan Null
        SelectKegiatan(null);

        return kegiatan;
    }

    private async Task AddHariHBaru(TimetableKegiatan[] kegiatan)
    {
        // Dapatkan Hari-H Kegiatan Dari Perbandingagn Waktu Kegiatan dengan Hari-H Skenario
        for (int i = 0; i < kegiatan.Length; i++)
        {
            if (SessionUser.id != 1)
            {
                if (kegiatan[i].kogas != SessionUser.bagian) continue;
            }

            if (kegiatan[i].kogas != "false" && kegiatan[i].waktu != "")
            {
                //Debug.Log(kegiatan[i].waktu.ToUniversalTime().Date.ToLongDateString() + " | " + SkenarioAktif.HARI_H.ToLongDateString());
                var difference = (int)(kegiatan[i].waktuDT.ToUniversalTime().Date - SkenarioAktif.HARI_H).TotalDays;
                
                if (!hariH.Contains(difference)) hariH.Add(difference);
                if (!hariHDate.Contains(kegiatan[i].waktuDT.ToUniversalTime().Date)) hariHDate.Add(kegiatan[i].waktuDT.ToUniversalTime().Date);
            }
        }

        // Sort Hari-H Dari Terkecil ke Terbesar
        hariH.Sort();
        
        // Instantiate Hari-H Kegiatan
        for(int i=0; i < hariH.Count; i++)
        {
            var obj = Instantiate(prefabHariH, hari_h.content.transform);
            if (i == 0)
            {
                // Set First Hari-H As Selected
                Color selectedColor;
                ColorUtility.TryParseHtmlString("#EEB24AFF", out selectedColor);

                obj.transform.GetChild(0).GetComponent<TextMeshProUGUI>().color = selectedColor;
                selected_hari_h = hariH[i];
            }

            obj.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = (hariH[i]) > 0 ? "H+" + hariH[i] : "H" + ((hariH[i] == 0) ? "" : hariH[i]);
            int hariH_active = obj.GetComponent<TimetableData>().hariH = hariH[i];

            // Add Event Register Untuk Mengnganti Hari-H Ketika Di-Klik
            obj.GetComponent<Button>().onClick.AddListener(() => ChangeHariH(hariH_active, obj));
        }
    }

    public async Task AddWaktuBaru(TimetableKegiatan[] kegiatan)
    {
        List<string> waktu = new List<string>();

        // Dapatkan List Waktu dari Seluruh Kegiatan
        for (int i = 0; i < kegiatan.Length; i++)
        {
            if (SessionUser.id != 1)
            {
                if (kegiatan[i].kogas != SessionUser.bagian) continue;
            }

            if (kegiatan[i].kogas != "false" && kegiatan[i].waktu != "")
            {
                var waktu_kegiatan = kegiatan[i].waktuDT.ToUniversalTime().ToString("HH:mm:ss");
                if ( !waktu.Contains(waktu_kegiatan) ) waktu.Add(waktu_kegiatan);
            }
        }

        // Sort List Waktu Dari Terkecil ke Terbesar
        waktu.Sort();

        // Masukkan List Waktu ke dalam Timetable
        for (int i = 0; i < waktu.Count; i++)
        {
            var obj = Instantiate(prefabList, timetable.content.transform);
            obj.GetComponent<Metadata>().FindParameter("label-waktu").parameter.GetComponent<TextMeshProUGUI>().text = waktu[i];
            obj.name = waktu[i];
        }

        // Masukkan Data Di Hari-H Apa Waktu Akan Tampil
        for (int i = 0; i < kegiatan.Length; i++)
        {
            if (SessionUser.id != 1)
            {
                if (kegiatan[i].kogas != SessionUser.bagian) continue;
            }

            if (kegiatan[i].kogas != "false" && kegiatan[i].waktu != "")
            {
                var objWaktu = timetable.content.transform.Find(kegiatan[i].waktuDT.ToUniversalTime().ToString("HH:mm:ss"));
                if(objWaktu != null)
                {
                    var difference = (int)(kegiatan[i].waktuDT.ToUniversalTime().Date - SkenarioAktif.HARI_H).TotalDays;

                    if (!objWaktu.GetComponent<TimetableData>().activeOn.Contains(difference))
                    {
                        objWaktu.GetComponent<TimetableData>().activeOn.Add(difference);
                    }
                }
            }
        }
    }

    public async Task AddKegiatanBaru(TimetableKegiatan[] kegiatan)
    {
        // Sorting Kegiatan Berdasarkan Waktu & Hari-H
        for (int i = 0; i < kegiatan.Length; i++)
        {
            if (kegiatan[i].waktu == "") continue;

            if (SessionUser.id != 1)
            {
                if (kegiatan[i].kogas != SessionUser.bagian) continue;
            }

            if (kegiatan[i].kogas != "false" && kegiatan[i].waktu != "")
            {
                // Masukkan Kegiatan Berdasarkan Waktu
                var parent = timetable.content.transform.Find(kegiatan[i].waktuDT.ToUniversalTime().ToString("HH:mm:ss")).GetComponent<Metadata>().FindParameter("content").parameter.transform;
                if(parent != null)
                {
                    // Configure Hari-H Kegiatan
                    var objKegiatan = Instantiate(prefabKegiatan, parent);
                    objKegiatan.name = kegiatan[i].id_kegiatan;

                    objKegiatan.GetComponent<Metadata>().FindParameter("label-kogas").parameter.GetComponent<TextMeshProUGUI>().text = kegiatan[i].kogas;
                    objKegiatan.GetComponent<Metadata>().FindParameter("label-keterangan").parameter.GetComponent<TextMeshProUGUI>().text = kegiatan[i].keterangan;

                    objKegiatan.GetComponent<TimetableData>().kegiatanID = kegiatan[i].id_kegiatan;
                    objKegiatan.GetComponent<TimetableData>().kegiatanDesc = kegiatan[i].kegiatan;
                    objKegiatan.GetComponent<TimetableData>().hariH = (int)(kegiatan[i].waktuDT.ToUniversalTime().Date - SkenarioAktif.HARI_H).TotalDays;
                    objKegiatan.GetComponent<TimetableData>().timeKegiatan = kegiatan[i].waktuDT.AddHours(-7);
                    objKegiatan.GetComponent<TimetableData>().percepatan = int.Parse(kegiatan[i].percepatan);

                    objKegiatan.GetComponent<Button>().onClick.AddListener(() => SelectKegiatan(objKegiatan));

                    if (kegiatan[i].keterangan.Length < 122)
                    {
                        objKegiatan.GetComponent<ContentSizeFitter>().horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
                    }
                    else
                    {
                        objKegiatan.GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x - 340,  objKegiatan.GetComponent<RectTransform>().sizeDelta.y);
                    }
                }
            }
        }
    }

    public void ChangeHariH(int hariH, GameObject objHariH = null)
    {
        // Set Selected Hari-H
        selected_hari_h = hariH;

        // Set Timetable Content Berdasarkan Hari-H Dipilih
        for (int i = 0; i < timetable.content.transform.childCount; i++)
        {
            var obj = timetable.content.transform.GetChild(i);
            obj.gameObject.SetActive(obj.GetComponent<TimetableData>().activeOn.Contains(hariH) ? true : false);

            if (obj.gameObject.activeSelf)
            {
                var content_kegiatan = obj.GetComponent<Metadata>().FindParameter("content").parameter.transform;
                for (int j = 0; j < content_kegiatan.childCount; j++)
                {
                    content_kegiatan.GetChild(j).gameObject.SetActive( (content_kegiatan.GetChild(j).GetComponent<TimetableData>().hariH == hariH) ? true : false );
                }
            }
        }

        // Set Date Label
        var namaHari = System.Threading.Thread.CurrentThread.CurrentUICulture.DateTimeFormat.GetDayName(SkenarioAktif.HARI_H.AddDays(selected_hari_h).DayOfWeek);
        labelDate.text = namaHari + ", " + SkenarioAktif.HARI_H.AddDays(selected_hari_h).ToString("dd MMMM yyyy") + " (H" + ((selected_hari_h > 0) ? "+" + selected_hari_h : (selected_hari_h < 0) ? selected_hari_h : "") + ")";

        if (objHariH != null)
        {
            // Set Hari-H Color To Default
            Color labelColor;
            for (int i = 0; i < hari_h.content.transform.childCount; i++)
            {
                ColorUtility.TryParseHtmlString("#C3C3C3FF", out labelColor);

                var obj = hari_h.content.transform.GetChild(i);
                obj.transform.GetChild(0).GetComponent<TextMeshProUGUI>().color = labelColor;
            }

            // Set Selected Hari-H Color
            ColorUtility.TryParseHtmlString("#EEB24AFF", out labelColor);

            objHariH.transform.GetChild(0).GetComponent<TextMeshProUGUI>().color = labelColor;
        }
    }

    public void SelectKegiatan(GameObject objHariH = null)
    {
        if(objHariH != null)
        {
            Color labelColor;
            if (selectedKegiatan != null)
            {
                ColorUtility.TryParseHtmlString("#71787EFF", out labelColor);
                selectedKegiatan.GetComponent<Image>().color = labelColor;
            }

            selectedKegiatan = objHariH;
            btnPlay.interactable = true;
            
            ColorUtility.TryParseHtmlString("#4D5154FF", out labelColor);
            objHariH.GetComponent<Image>().color = labelColor;
        }
        else
        {
            btnPlay.interactable = false;
        }
    }

    public async void PlayKegiatan()
    {
        if(selectedKegiatan == null) return;

        PlaybackController.Instance._TIME = selectedKegiatan.GetComponent<TimetableData>().timeKegiatan;
        PlaybackController.Instance.playbackSlider.value = selectedKegiatan.GetComponent<TimetableData>().timeKegiatan.Ticks;
        PlaybackController.Instance.percepatan = selectedKegiatan.GetComponent<TimetableData>().percepatan;

        PlaybackController.Instance.refreshTimeUI();
        PlaybackController.Instance.refreshPlaybackUI();

        PlaybackController.Instance.SyncStateToColyseus();
        PlaybackController.Instance.SyncTimeToColyseus();
        PlaybackController.Instance.state = PlaybackController.playbackState.PAUSED;

        PlaybackController.Instance.refreshWalkerPOS();
    }

    public async void UseKegiatan()
    {
        if (selectedKegiatan == null) return;

        RouteController.Instance.SelectKegiatan(selectedKegiatan.GetComponent<TimetableData>());

        GetComponent<Animator>().Play("FadeOut");
    }

    public void OpenTimetable()
    {
        if (selectedKegiatan != null)
        {
            Color labelColor;
            ColorUtility.TryParseHtmlString("#71787EFF", out labelColor);
            selectedKegiatan.GetComponent<Image>().color = labelColor;
        }

        selectedKegiatan = null;
        btnPlay.interactable = false;
    }
}
