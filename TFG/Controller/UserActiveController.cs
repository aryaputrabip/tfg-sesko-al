using UnityEngine;

using TMPro;

public class UserActiveController : MonoBehaviour
{
    [Header("References")]
    public Transform container;
    public GameObject prefabList;

    #region INSTANCE
    public static UserActiveController Instance;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    public void addToList(string nama)
    {
        bool isFound = false;
        foreach (Transform item in container)
        {
            if (item.name == "USER ACTIVE " + nama) isFound = true;
        }

        if (isFound == true) return;

        var list = Instantiate(prefabList, container);
        list.name = "USER ACTIVE " + nama;

        list.GetComponent<Metadata>().FindParameter("label-nama").parameter.GetComponent<TextMeshProUGUI>().text = nama;
    }

    public void removeFromList(string nama)
    {
        foreach(Transform item in container)
        {
            if (item.name == "USER ACTIVE " + nama) Destroy(item.gameObject);
        }
    }
}
