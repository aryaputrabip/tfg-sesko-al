using GlobalVariables;
using HelperPlotting;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class EntityControllerV2 : MonoBehaviour
{
    [Header("Controller")]
    private GameControllerV2 _controller;
    private EntityRouteController _route;

    [Header("Data")]
    public List<string> DATA_MISI = new List<string>();

    private void Start()
    {
        _route = FindObjectOfType<EntityRouteController>();
        _controller = FindObjectOfType<GameControllerV2>();
    }

    public async Task LoadEntityFromCB(string id_user = null, bool loadEnemy = false)
    {
        // Load Data dari Database
        var plotting = await API.Instance.LoadDataCB(loadEnemy, id_user);

        // Spawn Rute Satuan
        await SpawnRouteMisi(setJArrayResult(plotting, 13), id_user);

        // Spawn Entity Satuan
        await SpawnEntitySatuan(setJArrayResult(plotting, 0));

    }

    public async Task SpawnEntitySatuan(JArray arraySatuan, EntityTFG entity = null)
    {
        if (isJArrayNull(arraySatuan)) return;

        for(int i=0; i < arraySatuan.Count; i++)
        {
            Debug.Log("Spawn Satuan " + i);
            var satuan = EntitySatuan.FromJson((arraySatuan != null) ? arraySatuan[i].ToString() : entity.defaultData);
            var style_satuan = EntitySatuanStyle.FromJson(satuan.style);
            var info_satuan = EntitySatuanInfo.FromJson(satuan.info);

            if (SessionUser.id != 1) if (satuan.id_user != SessionUser.id.ToString()) return;

            // Set Marker Position
            var POS = new Vector2(satuan.lng, satuan.lat);

            // Create Marker 3D Berdasarkan Object 3D-nya
            var marker = OnlineMapsMarker3DManager.CreateItem(POS, AssetPackageManager.Instance.defaultShip);
            marker.instance.name = satuan.nama;
            marker["markerSource"] = marker;
            marker.rotationY = satuan.heading;

            // Set Ukuran Satuan
            marker.sizeType = OnlineMapsMarker3D.SizeType.scene;
            switch (satuan.jenis)
            {
                case "vehicle":
                    marker.scale = (float) Config.VEHICLE_SCALE;
                    break;
                case "ship":
                    marker.scale = (float) Config.SHIP_SCALE;
                    break;
                case "aircraft":
                    marker.scale = (float) Config.AIRCRAFT_SCALE;
                    break;
                default: break;
            }
        }
    }

    public async Task SpawnRouteMisi(JArray arrayMisi, string id_user = null)
    {
        if (isJArrayNull(arrayMisi)) return;

        // Add Rute Per-Misi
        for (int i = 0; i < arrayMisi.Count; i++)
        {
            // Get Data Misi (Jump to Next Index if It's Null)
            var misi = EntityMisi.FromJson(arrayMisi[i].ToString());
            if (misi.properties == null) continue;

            // Get Data Properties Misi (Jump to Next Index if It's Null)
            var misi_properties = misi.properties;
            if (misi_properties.jalur == null) continue;

            if (!DATA_MISI.Contains(misi_properties.nama_misi))
            {
                DATA_MISI.Add(misi_properties.nama_misi);

                // Create Route Path Points
                List<Vector3> paths = new List<Vector3>();
                double jarak_jalur = 0;


                for (int j = 0; j < misi_properties.jalur.Count; j++)
                {
                    paths.Add(new Vector3(misi_properties.jalur[j].lng, misi_properties.jalur[j].lat, (float)misi_properties.jalur[j].alt));
                    if (j > 0)
                    {
                        jarak_jalur += OnlineMapsUtils.DistanceBetweenPointsD(new Vector2(misi_properties.jalur[j - 1].lng, misi_properties.jalur[j - 1].lat), new Vector2(misi_properties.jalur[j].lng, misi_properties.jalur[j].lat));
                    }
                }

                // Set Rute Misi Ke Satuannya
                var newRoute = await _route.AddNewRoute(paths, misi, misi_properties, id_user);

                // Buat atau Gunakan Container Drawing Route Untuk Menyimpan Data Drawing Berdasarkan ID Usernya
                // (Disimpan per-user agar ketika user tersebut logout seluruh data drawing user tersebut dapat terhapus)
                MetadataDrawRoute newDrawRoute = null;
                var objDrawRoutes = _route.drawRouteContainer.Find((id_user) == null ? SessionUser.id.ToString() : id_user);
                if (objDrawRoutes == null)
                {
                    newDrawRoute = Instantiate(_route.prefabDrawRoute, _route.drawRouteContainer);
                    newDrawRoute.name = (id_user == null) ? SessionUser.id.ToString() : id_user;
                }
                else
                {
                    newDrawRoute = objDrawRoutes.GetComponent<MetadataDrawRoute>();
                }
            }
            else
            {
                //Destroy(GameObject.Find(misi.id_object));
            }
        }
    }

    #region HELPER
    public JArray setJArrayResult(JArray arrayResult, int index)
    {
        if (arrayResult[index].ToString() != "False")
        {
            return JArray.Parse(arrayResult[index].ToString());
        }
        else
        {
            return null;
        }
    }

    public bool isJArrayNull(JArray array)
    {
        if (array == null)
        {
            return true;
        }
        else
        {
            if (array.Count == 0) return true;
        }

        return false;
    }
    #endregion
}
