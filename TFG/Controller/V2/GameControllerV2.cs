using GlobalVariables;
using HelperUser;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class GameControllerV2 : MonoBehaviour
{
    public enum gameplayMode { SINGLEPLAYER, MULTIPLAYER, MP_SPECTATOR }

    [Header("Controller")]
    public EntityControllerV2 _entity;

    [Header("Config")]
    public gameplayMode GameMode;


    #region -- DEBUG MODE --
    /// <summary>
    /// Debug Untuk Login Sebagai User Tanpa Harus Membuka Halaman Login Terlebih Dahulu
    /// </summary>
    [Header("Debug User")]
    [SerializeField] private bool debugUser;
    [SerializeField] private string userJSON;
    [SerializeField] private string serviceLogin;
    [SerializeField] private string serviceCB;
    [SerializeField] private string serverColyseus;

    [Header("Debug Config")]
    [SerializeField] private int aircraftAlt;
    [SerializeField] private int vehicleScale;
    [SerializeField] private int shipScale;
    [SerializeField] private int aircraftScale;

    #endregion
    // Start is called before the first frame update
    private async void Start()
    {
        if (debugUser)
        {
            await TestLoggedUser();
        }
    }

    private async Task TestLoggedUser()
    {
        // -- KOGASRATGAB TEST --
        // [{"username":"asops_kogasratgab","id":124,"name":"ASOPS KOGASRATGAB","jenis_user":{"ID":1,"jenis_user":"user"},"bagian":{"ID":8,"nama_bagian":"KOGASRATGAB"},"jabatan":{"ID":2,"nama_jabatan":"asisten"},"asisten":{"ID":6,"nama_asisten":"ASOPS"},"atasan":null,"status_login":1,"be":0}]
        User[] user = User.FromJson(userJSON);

        new SessionUser(user[0]);

        Config.SERVICE_LOGIN = (serviceLogin != null) ? serviceLogin : "http://localhost/eppkm/public";
        Config.SERVICE_CB = (serviceCB != null) ? serviceCB : "http://localhost/eppkm_simulasi";
        Config.SERVER_COLYSEUS = (serverColyseus != null) ? serverColyseus : "ws://localhost:2567";
        Config.aircraft_alt = aircraftAlt;
        Config.VEHICLE_SCALE = vehicleScale;
        Config.SHIP_SCALE = shipScale;
        Config.AIRCRAFT_SCALE = aircraftScale;

        await API.Instance.GetSkenarioAktif();
        await API.Instance.GetCBTerbaik(SessionUser.id_bagian.ToString());

        joinAsPlayerSP();
    }

    /// <summary>
    /// -- SINGLEPLAYER MODE --
    /// Masuk ke ROOM Sebagai "Player"
    /// Player : Aplikasi akan men-simulasikan pergerakannya sendiri (App bergerak sendiri dalam mode singleplayer tanpa menggunakan server / room)
    /// </summary>
    public async void joinAsPlayerSP()
    {
        GameMode = gameplayMode.SINGLEPLAYER;
        prepareGameplay();
    }

    private async void prepareGameplay()
    {
        Debug.Log("Loading CB Terbaik...");
        await _entity.LoadEntityFromCB();
    }
}
