using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Threading.Tasks;
using UnityEngine.Video;

public class VideoKegiatanController : MonoBehaviour
{
    //public static VideoKegiatanController Instance;

    public VideoClip[] videos;
    public VideoPlayer videoPlayer;
    public GameObject videoContainer;
    public Transform objContainer;

    private int lc;

    [SerializeField] private string _dir;
    public List<string> folders;
    public List<List<string>> lv;

    #region INSTANCE
    public static VideoKegiatanController Instance;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        _dir = Directory.GetCurrentDirectory() + "/" + _dir;
        folders = new List<string>();
        lv = new List<List<string>>();
        getDirectories();
        //loadVideos();
        //playVideo("Debarkasi");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void playVideo()
    {
        GetComponent<Animator>().Play("FadeIn");

        //lc = 0;
        //videoContainer.SetActive(true);
        System.Random rnd = new System.Random();
        var n = rnd.Next(0,30);
        videoPlayer.clip = videos[n];
        // Kalo play lewat url
        //videoPlayer.url = "";
        videoPlayer.Play();
        //videoPlayer.loopPointReached += stopVideo;

    }

    public void playVideo(string cat)
    {
        GetComponent<Animator>().Play("FadeIn");

        Debug.Log("Now Playing : " + _dir + "/" + cat);
        //videoContainer.SetActive(true);
        System.Random rnd = new System.Random();
        string[] bundleVideos = Directory.GetFiles(_dir + "/" + cat, "*.mp4");
        var n = rnd.Next(0, bundleVideos.Length - 1);
        videoPlayer.url = bundleVideos[n];
        videoPlayer.Play();
        //videoPlayer.loopPointReached += stopVideo;
    }

    public void playVideoUrl(string url)
    {
        GetComponent<Animator>().Play("FadeIn");

        //videoContainer.SetActive(true);
        videoPlayer.url = url;
        videoPlayer.Play();
        //videoPlayer.loopPointReached += stopVideo;
    }

    public void stopVideo()
    {
        //if(lc >= 3)
        //{
            videoPlayer.Stop();
            //videoContainer.SetActive(false);

        GetComponent<Animator>().Play("FadeOut");
        //}
        //lc++;
        //Debug.Log("Loop ke-"+lc);
    }

    public void loadVideos()
    {
        if (_dir == null) return;
        if (!Directory.Exists(_dir))
        {
            Debug.Log("Directory tidak ditemukan!");
            return;
        }

        string[] isi = Directory.GetDirectories(_dir);

        for (int i = 0; i < isi.Length; i++)
        {
            string[] bundleVideos = Directory.GetFiles(isi[i], "*.mp4");
            if (bundleVideos.Length > 0)
            {
                folders.Insert(i, isi[i]);
             
                List<string> vids = new List<string>();
                for (int j = 0; j < bundleVideos.Length; j++)
                {
                    vids.Add(bundleVideos[j]);
                }
                lv.Insert(i, vids);
                vids.Clear();
            }
            else
            {
                Debug.Log("No Videos Found Inside "+isi[i]);
            }
        }
    }

    private void getDirectories()
    {
        if (_dir == null) return;
        if (!Directory.Exists(_dir))
        {
            Debug.Log("Directory tidak ditemukan!");
            return;
        }

        string[] isi = Directory.GetDirectories(_dir);
        for (int i = 0; i < isi.Length; i++)
        {
            FileInfo f = new FileInfo(isi[i]);
            folders.Add(f.Name);
        }
    }
}
