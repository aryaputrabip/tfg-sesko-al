using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using TMPro;
using DigitalRuby.WeatherMaker;

public class WeatherController : MonoBehaviour
{
    [SerializeField] private float effectTransition = 0.5f;

    [Header("References")]
    public TMP_Dropdown dropdownWeather;
    public TMP_Dropdown dropdownCloud;

    public static WeatherController instance;
    void Awake()
    {
        // Set Asset Package Manager Menjadi 1 Origin
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void ChangeWeather(int weather_index)
    {
        if (weather_index != -99)
        {
            EnviroSkyMgr.instance.ChangeWeather((weather_index == 0) ? weather_index : weather_index + 4);
        }
        else
        {
            EnviroSkyMgr.instance.ChangeWeather((dropdownWeather.value == 0) ? dropdownWeather.value : dropdownWeather.value + 4);
        }
    }

    public void ChangeCloud(int cloud_index)
    {
        Debug.Log(cloud_index);
        EnviroSkyMgr.instance.ChangeWeather(cloud_index != -99 ? cloud_index : dropdownCloud.value);
    }
}
