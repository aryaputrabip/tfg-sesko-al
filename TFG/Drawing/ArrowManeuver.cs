﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;

public class ArrowManeuver : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Calculate Degree
    /// </summary>
    float calcDegree(Vector2 pt1, Vector2 pt2)
    {
        float ret = 0.0f;

        float ax = pt2.x - pt1.x;
        float ay = pt2.y - pt1.y;

        ret = (Mathf.Atan2(ay, ax) * 180)/ Mathf.PI;
        
        return ret;
    }

    /// <summary>
    /// Calculate Distance
    /// </summary>
    float calcDistance(Vector2 pt1, Vector2 pt2){
        float a = ((pt2.x - pt1.x)*(pt2.x - pt1.x))+((pt2.y-pt1.y)*(pt2.y-pt1.y));
        float dist = Mathf.Sqrt(a);
        return dist;
    }

    /// <summary>
    /// Calculate Mid Point
    /// </summary>
    Vector2 getMidPoint(Vector2 p1, Vector2 p2)
    {
        Vector2 res = Vector2.zero;

        float ax = (p1.x + p2.x) /2;
        float ay = (p1.y + p2.y) /2;

        res = new Vector2(ax, ay);

        return res;
    }

    /// <summary>
    /// Drawing a Rectangle
    /// </summary>
    public List<Vector2> DrawRect(Vector2 origin, Vector2 scale)
    {
        var points = new List<Vector2>();
        var p1 = origin;
        var p2 = new Vector2(scale.x, origin.y);
        var p3 = scale;
        var p4 = new Vector2(origin.x, scale.y);
        var pList = new Vector2[]{p1, p2, p3, p4};
        
        for(int i = 0; i < 4; i++)
        {
            points.Add(pList[i]);
        }

        return points;
    }


    /// <summary>
    /// Drawing a Scale
    /// </summary>
    public List<Vector2> CreateScale(OnlineMapsDrawingPoly polyNow, List<Vector2> vertices2D)
    {
        Vector2 titikTengahPanjang;
        Vector2 titikTengahLebar;
        // Debug.Log(polyNow.center.x + ", " + polyNow.center.y);
        // Debug.Log(DrawArrow.instance.ListVectorToText(vertices2D));

        double panjangSet = 0;
        double lebarSet = 0;
        double panjang10pers = 0;
        double lebar10pers = 0;

        float beforeX = 0;
        float beforeY = 0;
        float highestX = 0;
        float lowestY = 0;

        foreach(Vector2 child in vertices2D)
        {
            if(beforeX == 0 && beforeY == 0)
            {
                highestX = child.x;
                lowestY = child.y;
                beforeX = child.x;
                beforeY = child.y;
            }

            if(child.x > beforeX)
            {
                highestX = child.x;
                beforeX = child.x;
            }

            if(child.y < beforeY)
            {
                lowestY = child.y;
                beforeY = child.y;
            }
        }
        
        panjangSet = OnlineMapsUtils.DistanceBetweenPointsD(polyNow.center, new Vector2(highestX, (float)polyNow.center.y));
        lebarSet = OnlineMapsUtils.DistanceBetweenPointsD(polyNow.center, new Vector2((float)polyNow.center.x, lowestY));
        panjang10pers = OnlineMapsUtils.DistanceBetweenPointsD(OnlineMapsControlBase.instance.GetCoords(new Vector2(0f, 0f)), OnlineMapsControlBase.instance.GetCoords(new Vector2(10f, 10f))) / 1.852f / 60f;
        lebar10pers = (lebarSet * 0.1) / 1.852f / 60f; 

        titikTengahPanjang =  new Vector2(highestX, (float)polyNow.center.y);
        titikTengahLebar = new Vector2((float)polyNow.center.x, lowestY);

        List<Vector2> scalePointOrigin = new List<Vector2>(){Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero};

        if(polyNow.polyType == DrawStat.polyType.Square || polyNow.polyType == DrawStat.polyType.Circle)
        {
            scalePointOrigin[0] = new Vector2(highestX, lowestY) + new Vector2((float)panjang10pers, 0 - (float)panjang10pers);
            scalePointOrigin[1] = new Vector2(scalePointOrigin[0].x + (float)panjang10pers, scalePointOrigin[0].y);
            scalePointOrigin[2] = new Vector2(scalePointOrigin[1].x, scalePointOrigin[1].y - (float)panjang10pers);
            scalePointOrigin[3] = new Vector2(scalePointOrigin[0].x, scalePointOrigin[2].y);
            Debug.Log(DrawArrow.instance.ListVectorToText(scalePointOrigin));
        }
        else if(polyNow.polyType == DrawStat.polyType.Arrow)
        {
            scalePointOrigin[0] = vertices2D[4];

            if(vertices2D[4].x > polyNow.center.x)
            {
                scalePointOrigin[1] = new Vector2(scalePointOrigin[0].x + (float)panjang10pers, scalePointOrigin[0].y);
            }
            else
            {
                scalePointOrigin[1] = new Vector2(scalePointOrigin[0].x - (float)panjang10pers, scalePointOrigin[0].y);
            }

            if(vertices2D[4].y > polyNow.center.y)
            {
                scalePointOrigin[2] = new Vector2(scalePointOrigin[1].x, scalePointOrigin[1].y + (float)panjang10pers);
            }
            else
            {
                scalePointOrigin[2] = new Vector2(scalePointOrigin[1].x, scalePointOrigin[1].y - (float)panjang10pers);
            }

            scalePointOrigin[3] = new Vector2(scalePointOrigin[0].x, scalePointOrigin[2].y);
        }
        else
        {
            scalePointOrigin[0] = vertices2D[171];

            if(vertices2D[171].x > polyNow.center.x)
            {
                scalePointOrigin[1] = new Vector2(scalePointOrigin[0].x + (float)panjang10pers, scalePointOrigin[0].y);
            }
            else
            {
                scalePointOrigin[1] = new Vector2(scalePointOrigin[0].x - (float)panjang10pers, scalePointOrigin[0].y);
            }

            if(vertices2D[171].y > polyNow.center.y)
            {
                scalePointOrigin[2] = new Vector2(scalePointOrigin[1].x, scalePointOrigin[1].y + (float)panjang10pers);
            }
            else
            {
                scalePointOrigin[2] = new Vector2(scalePointOrigin[1].x, scalePointOrigin[1].y - (float)panjang10pers);
            }

            scalePointOrigin[3] = new Vector2(scalePointOrigin[0].x, scalePointOrigin[2].y);
        }

        return scalePointOrigin;
    }
    
    /// <summary>
    /// Drawing a circle with lng lat double
    /// </summary>
    public List<Vector2> DrawCircle(double lng, double lat, float radiusKM)
    {
        OnlineMaps map = OnlineMaps.instance;

        // Get the coordinate at the desired distance
        double nlng, nlat;
        OnlineMapsUtils.GetCoordinateInDistance(lng, lat, radiusKM, 90, out nlng, out nlat);

        double tx1, ty1, tx2, ty2;

        // Convert the coordinate under cursor to tile position
        map.projection.CoordinatesToTile(lng, lat, 20, out tx1, out ty1);

        // Convert remote coordinate to tile position
        map.projection.CoordinatesToTile(nlng, nlat, 20, out tx2, out ty2);

        // Calculate radius in tiles
        double r = tx2 - tx1;

        // Create a new array for points
        List<Vector2> points = new List<Vector2>();

        // Calculate a step
        var segments = 64;
        double step = 360d / segments;

        // Calculate each point of circle
        for (int i = 0; i < segments; i++)
        {
            double px = tx1 + Math.Cos(step * i * OnlineMapsUtils.Deg2Rad) * r;
            double py = ty1 + Math.Sin(step * i * OnlineMapsUtils.Deg2Rad) * r;
            map.projection.TileToCoordinates(px, py, 20, out lng, out lat);
            points.Add(new Vector2((float)lng, (float)lat));
        }

        return points;
    }
    
    /// <summary>
    /// Drawing a circle with lng lat Vector
    /// </summary>
    public List<Vector2> DrawCircle(OnlineMapsVector2d lngLat, float radiusKM)
    {
        var lng = lngLat.x; 
        var lat = lngLat.y;
        var points = DrawCircle(lng, lat, radiusKM);

        return points;
    }

    public static List<Vector2> CircleNormal(double lng, double lat, float radiusKM)
    {
        var segments = 64;
        var points = new Vector2[segments];
        var step = 360d / segments;

        for(int i = 0; i < segments; i++)
        {
            var _lng = (float)(lng + Math.Sin(step * i * OnlineMapsUtils.Deg2Rad)) * radiusKM;
            var _lat = (float)(lat + Math.Sin(step * i * OnlineMapsUtils.Deg2Rad)) * radiusKM;

            points[i] = new Vector2(_lng, _lat);
        }

        return points.ToList();
    }

    /// <summary>
    /// Drawing a Elipse
    /// </summary>
    public List<Vector2> DrawElipse(double lng, double lat, float radiusKM, float angle = 45)
    {
        OnlineMaps map = OnlineMaps.instance;

        // Get the coordinate at the desired distance
        double nlng, nlat;
        OnlineMapsUtils.GetCoordinateInDistance(lng, lat, radiusKM, 90, out nlng, out nlat);

        double tx1, ty1, tx2, ty2;

        // Convert the coordinate under cursor to tile position
        map.projection.CoordinatesToTile(lng, lat, 20, out tx1, out ty1);

        // Convert remote coordinate to tile position
        map.projection.CoordinatesToTile(nlng, nlat, 20, out tx2, out ty2);

        // Calculate radius in tiles
        double r = tx2 - tx1;

        // Create a new array for points
        var points = new List<Vector2>();

        // Calculate a step
        var segments = 64;
        double step = 360d / segments;

        for(int i = 0; i < segments; i++)
        {
            var heading = (step * i) * OnlineMapsUtils.Deg2Rad;
            var px = (r / 2 * Math.Cos(heading)) + tx1;
            var py = (r * Math.Sin(heading)) + ty1;
            
            map.projection.TileToCoordinates(px, py, 20, out lng, out lat);
            points.Add(new Vector2((float)lng, (float)lat));
        }

        return points;
    }

    /// <summary>
    /// Drawing a Arrow
    /// </summary>
    public List<Vector2> createSAP(Vector2 pt1, Vector2 pt2, float size)
    {
        List<Vector2> res = new List<Vector2>();

        float ang = calcDegree(pt1, pt2);

        float tmpx = size * Mathf.Cos((ang+90) * Mathf.PI/180);
        float tmpy = size * Mathf.Sin((ang+90) * Mathf.PI/180);
        float tmpx1 = size * 2 * Mathf.Cos((ang+90) * Mathf.PI/180);
        float tmpy1 = size * 2 * Mathf.Sin((ang+90) * Mathf.PI/180);

        //create 1st Point
        float _x = pt1.x + tmpx;
        float _y = pt1.y + tmpy;
        Vector2 tmpPt = new Vector2(_x,_y);
        res.Add(tmpPt);

        //create 2nd Point
        _x = pt1.x - tmpx;
        _y = pt1.y - tmpy;
        tmpPt = new Vector2(_x,_y);
        res.Add(tmpPt);

        //create 3rd Point
        _x = (getMidPoint(pt1,pt2).x + pt2.x) / 2 - tmpx;
        _y = (getMidPoint(pt1,pt2).y + pt2.y) / 2 - tmpy;
        tmpPt = new Vector2(_x, _y);
        res.Add(tmpPt);

        //create 4th Point
        _x = (getMidPoint(pt1,pt2).x + pt2.x) / 2 - tmpx1;
        _y = (getMidPoint(pt1,pt2).y + pt2.y) / 2 - tmpy1;
        tmpPt = new Vector2(_x, _y);
        res.Add(tmpPt);

        //create 5th Point
        res.Add(pt2);

        //create 6th Point
        _x = (getMidPoint(pt1,pt2).x + pt2.x) / 2 + tmpx1;
        _y = (getMidPoint(pt1,pt2).y + pt2.y) / 2 + tmpy1;
        tmpPt = new Vector2(_x, _y);
        res.Add(tmpPt);

        //create 7th Point
        _x = (getMidPoint(pt1,pt2).x + pt2.x) / 2 + tmpx;
        _y = (getMidPoint(pt1,pt2).y + pt2.y) / 2 + tmpy;
        tmpPt = new Vector2(_x, _y);
        res.Add(tmpPt);

        return res.ToList();
    }

    /// <summary>
    /// Drawing a Curve Bottom Arrow
    /// </summary>
    public List<Vector2> createCAP(Vector2 pt1, Vector2 pt2, float size, float kurang)
    {
        float ang1 = calcDegree(pt1,pt2);
        float ang2 = calcDegree(pt2,pt1);
        float aDist = calcDistance(pt1,pt2) / 2;
        List<Vector2> arrPoint = new List<Vector2>();
        
        for(int i = 0; i < 343; i++)
        arrPoint.Add(Vector2.zero);

        if (ang1 < ang2){
            //curved to rigth
            
            var n = 0;
            for(var i= Mathf.Round(ang2);i <= Mathf.Round(360 + ang1);i++){            
            //for(var i= Mathf.Round(ang2);i <= Mathf.Round(ang1);i++){            

                //draw line
                if (i == (Mathf.Round(360+ang1) - 10)){
                    var tmpx = (getMidPoint(pt1,pt2).x+((aDist-size) * Mathf.Cos(i * Mathf.PI/ 180)));
                    var tmpy = (getMidPoint(pt1,pt2).y+((aDist-size) * Mathf.Sin(i * Mathf.PI/ 180)));
                    arrPoint[n] = new Vector2(tmpx, tmpy);
                    n++;
                    break;
                }else if(i < (Mathf.Round(360+ang1) - 10)) {
                    var tmpx = (getMidPoint(pt1,pt2).x+((aDist-(size-kurang)) * Mathf.Cos(i * Mathf.PI/ 180)));
                    var tmpy = (getMidPoint(pt1,pt2).y+((aDist-(size-kurang)) * Mathf.Sin(i * Mathf.PI/ 180)));
                    arrPoint[n] = new Vector2(tmpx, tmpy);
                    n++;
                }
            }

            //create outer side polygon
            arrPoint[n] = pt2;
            n = 342;
            for (var i = Mathf.Round(ang2);i<= Mathf.Round(360 + ang1);i++){ 
            //for(var i= Mathf.Round(ang2);i <= Mathf.Round(ang1);i++){ 
                
                if (i == (Mathf.Round(360+ang1) - 10) ){
                    var tmpx = getMidPoint(pt1,pt2).x+((aDist+size) * Mathf.Cos(i * Mathf.PI/ 180));
                    var tmpy = getMidPoint(pt1,pt2).y+((aDist+size) * Mathf.Sin(i * Mathf.PI/ 180));
                    arrPoint[n] = new Vector2(tmpx, tmpy);
                    n--;
                    break;
                }
                else if (i < (Mathf.Round(360+ang1) - 10)){
                  var tmpx = getMidPoint(pt1,pt2).x+((aDist+(size-kurang)) * Mathf.Cos(i * Mathf.PI/ 180));
                  var tmpy = getMidPoint(pt1,pt2).y+((aDist+(size-kurang)) * Mathf.Sin(i * Mathf.PI/ 180));
                  arrPoint[n] = new Vector2(tmpx, tmpy);
                  n--;
                }

            }
        }else{
            //reversed
            ang2 = 360 - Mathf.Abs(calcDegree(pt2,pt1));

            var n = 0;
            for(var i= Mathf.Round(ang2);i <= Mathf.Round(360 + ang1);i++){            
            //for(var i= Mathf.Round(ang2);i <= Mathf.Round(ang1);i++){ 
                
                //draw line
                if (i == (Mathf.Round(360+ang1) - 10)){
                    var tmpx = (getMidPoint(pt1,pt2).x+((aDist-size) * Mathf.Cos(i * Mathf.PI/ 180)));
                    var tmpy = (getMidPoint(pt1,pt2).y+((aDist-size) * Mathf.Sin(i * Mathf.PI/ 180)));
                    arrPoint[n] = new Vector2(tmpx, tmpy);
                    n++;
                    break;
                }else if(i < (Mathf.Round(360+ang1) - 10)) {
                    var tmpx = (getMidPoint(pt1,pt2).x+((aDist-(size-kurang)) * Mathf.Cos(i * Mathf.PI/ 180)));
                    var tmpy = (getMidPoint(pt1,pt2).y+((aDist-(size-kurang)) * Mathf.Sin(i * Mathf.PI/ 180)));
                    arrPoint[n] = new Vector2(tmpx, tmpy);
                    n++;
                }
            }

            //create outer side polygon
            arrPoint[n] = pt2;
            n = 342;
            for (var i = Mathf.Round(ang2);i<= Mathf.Round(360 + ang1);i++){ 
            //for(var i= Mathf.Round(ang2);i <= Mathf.Round(ang1);i++){ 
                
                if (i == (Mathf.Round(360+ang1) - 10) ){
                    var tmpx = getMidPoint(pt1,pt2).x+((aDist+size) * Mathf.Cos(i * Mathf.PI/ 180));
                    var tmpy = getMidPoint(pt1,pt2).y+((aDist+size) * Mathf.Sin(i * Mathf.PI/ 180));
                    arrPoint[n] = new Vector2(tmpx, tmpy);
                    n--;
                    break;
                }
                else if (i < (Mathf.Round(360+ang1) - 10)){
                  var tmpx = getMidPoint(pt1,pt2).x+((aDist+(size-kurang)) * Mathf.Cos(i * Mathf.PI/ 180));
                  var tmpy = getMidPoint(pt1,pt2).y+((aDist+(size-kurang)) * Mathf.Sin(i * Mathf.PI/ 180));
                  arrPoint[n] = new Vector2(tmpx, tmpy);
                  n--;
                }

            }
        }
        
        return arrPoint;
    }

    /// <summary>
    /// Drawing a Curve Top Arrow
    /// </summary>
    public List<Vector2> createCAPRev(Vector2 pt1, Vector2 pt2, float size, float kurang)
    {
        float ang1 = calcDegree(pt1,pt2);
        float ang2 = calcDegree(pt2,pt1);
        float aDist = calcDistance(pt1,pt2) / 2;
        List<Vector2> arrPoint = new List<Vector2>();
        
        for(int i = 0; i < 343; i++)
        arrPoint.Add(Vector2.zero);

        if (ang1 < ang2){
            //curved to rigth
            
            var n = 0;
            for(var i= Mathf.Round(ang2);i >= Mathf.Round(ang1);i--){            
            //for(var i= Mathf.Round(ang2);i <= Mathf.Round(ang1);i++){            

                //draw line
                if (i == (Mathf.Round(ang1) + 10)){
                    var tmpx = (getMidPoint(pt1,pt2).x+((aDist-size) * Mathf.Cos(i * Mathf.PI/ 180)));
                    var tmpy = (getMidPoint(pt1,pt2).y+((aDist-size) * Mathf.Sin(i * Mathf.PI/ 180)));
                    arrPoint[n] = new Vector2(tmpx, tmpy);
                    n++;
                    break;
                }else if(i > (Mathf.Round(ang1) + 10)) {
                    var tmpx = (getMidPoint(pt1,pt2).x+((aDist-(size-kurang)) * Mathf.Cos(i * Mathf.PI/ 180)));
                    var tmpy = (getMidPoint(pt1,pt2).y+((aDist-(size-kurang)) * Mathf.Sin(i * Mathf.PI/ 180)));
                    arrPoint[n] = new Vector2(tmpx, tmpy);
                    n++;
                }
            }

            //create outer side polygon
            arrPoint[n] = pt2;
            n = 342;
            for (var i = Mathf.Round(ang2);i>= Mathf.Round(ang1);i--){ 
            //for(var i= Mathf.Round(ang2);i <= Mathf.Round(ang1);i++){ 
                
                if (i == (Mathf.Round(ang1) + 10) ){
                    var tmpx = getMidPoint(pt1,pt2).x+((aDist+size) * Mathf.Cos(i * Mathf.PI/ 180));
                    var tmpy = getMidPoint(pt1,pt2).y+((aDist+size) * Mathf.Sin(i * Mathf.PI/ 180));
                    arrPoint[n] = new Vector2(tmpx, tmpy);
                    n--;
                    break;
                }
                else if (i > (Mathf.Round(ang1) + 10)){
                  var tmpx = getMidPoint(pt1,pt2).x+((aDist+(size-kurang)) * Mathf.Cos(i * Mathf.PI/ 180));
                  var tmpy = getMidPoint(pt1,pt2).y+((aDist+(size-kurang)) * Mathf.Sin(i * Mathf.PI/ 180));
                  arrPoint[n] = new Vector2(tmpx, tmpy);
                  n--;
                }

            }
        }else{
            //reversed
            ang2 = 360 - Mathf.Abs(calcDegree(pt2,pt1));

            var n = 0;
            for(var i= Mathf.Round(ang2);i >= Mathf.Round(ang1);i--){            
            //for(var i= Mathf.Round(ang2);i <= Mathf.Round(ang1);i++){ 
                
                //draw line
                if (i == (Mathf.Round(ang1) + 10)){
                    var tmpx = (getMidPoint(pt1,pt2).x+((aDist-size) * Mathf.Cos(i * Mathf.PI/ 180)));
                    var tmpy = (getMidPoint(pt1,pt2).y+((aDist-size) * Mathf.Sin(i * Mathf.PI/ 180)));
                    arrPoint[n] = new Vector2(tmpx, tmpy);
                    n++;
                    break;
                }else if(i > (Mathf.Round(ang1) + 10)) {
                    var tmpx = (getMidPoint(pt1,pt2).x+((aDist-(size-kurang)) * Mathf.Cos(i * Mathf.PI/ 180)));
                    var tmpy = (getMidPoint(pt1,pt2).y+((aDist-(size-kurang)) * Mathf.Sin(i * Mathf.PI/ 180)));
                    arrPoint[n] = new Vector2(tmpx, tmpy);
                    n++;
                }
            }

            //create outer side polygon
            arrPoint[n] = pt2;
            n = 342;
            for (var i = Mathf.Round(ang2);i>= Mathf.Round(ang1);i--){ 
            //for(var i= Mathf.Round(ang2);i <= Mathf.Round(ang1);i++){ 
                
                if (i == (Mathf.Round(ang1) + 10) ){
                    var tmpx = getMidPoint(pt1,pt2).x+((aDist+size) * Mathf.Cos(i * Mathf.PI/ 180));
                    var tmpy = getMidPoint(pt1,pt2).y+((aDist+size) * Mathf.Sin(i * Mathf.PI/ 180));
                    arrPoint[n] = new Vector2(tmpx, tmpy);
                    n--;
                    break;
                }
                else if (i > (Mathf.Round(ang1) + 10)){
                  var tmpx = getMidPoint(pt1,pt2).x+((aDist+(size-kurang)) * Mathf.Cos(i * Mathf.PI/ 180));
                  var tmpy = getMidPoint(pt1,pt2).y+((aDist+(size-kurang)) * Mathf.Sin(i * Mathf.PI/ 180));
                  arrPoint[n] = new Vector2(tmpx, tmpy);
                  n--;
                }
            }
        }
        
        return arrPoint;
    }

    /// <summary>
    /// Drawing a Maneuver Top Arrow
    /// </summary>
    public List<Vector2> createCAM(Vector2 pt1, Vector2 pt2, float size, float kurang)
    {
        float ang1 = calcDegree(pt1,pt2);
        float ang2 = calcDegree(pt2,pt1);
        float aDist = calcDistance(pt1,pt2) / 2;
        List<Vector2> arrPoint = new List<Vector2>();
        
        for(int i = 0; i < 343; i++)
        arrPoint.Add(Vector2.zero);

        Vector2 aPt1 = new Vector2();
        Vector2 aPt2 = new Vector2();
        
        aPt1.x = getMidPoint(pt1, pt2).x - (2*size * Mathf.Cos((ang1)* Mathf.PI/ 180));
		aPt1.y = getMidPoint(pt1, pt2).y - (2*size * Mathf.Sin((ang1)* Mathf.PI/ 180));
		aPt2.x = getMidPoint(pt1, pt2).x + (size * Mathf.Cos((ang1)* Mathf.PI/ 180));
		aPt2.y = getMidPoint(pt1, pt2).y + (size * Mathf.Sin((ang1)* Mathf.PI/ 180));

        if (ang1 < ang2){
            //curved to rigth
            
            var n = 0;
            //for(var i= Mathf.Round(ang2);i <= Mathf.Round(360 + ang1);i++){              
			for(var i= Mathf.Round(ang2);i >= Mathf.Round(ang1);i--){ 			

                //draw line
                //if (i === (Mathf.Round(360+ang1) - 10)){
				if (i == (Mathf.Round(ang1) + 10)){
                    var tmpx = (aPt1.x+((aDist-size) * Mathf.Cos(i * Mathf.PI/ 180)));
                    var tmpy = (aPt1.y+((aDist-size) * Mathf.Sin(i * Mathf.PI/ 180)));
                    arrPoint[n] = new Vector2(tmpx, tmpy);
                    n++;
                    break;
                //}else if(i < (Mathf.Round(360+ang1) - 10)) {
				}else if(i > (Mathf.Round(ang1) + 10)) {
                    var tmpx = (aPt1.x+((aDist-(size-kurang)) * Mathf.Cos(i * Mathf.PI/ 180)));
                    var tmpy = (aPt1.y+((aDist-(size-kurang)) * Mathf.Sin(i * Mathf.PI/ 180)));
                    arrPoint[n] = new Vector2(tmpx, tmpy);
                    n++;
                }
            }

            //create outer side polygon
            arrPoint[n] = pt2;
            n = 342;
            //for (var i = Mathf.Round(ang2);i<= Mathf.Round(360 + ang1);i++){ 
            //for(var i= Mathf.Round(ang2);i <= Mathf.Round(ang1);i++){ 
			for (var i = Mathf.Round(ang2);i>= Mathf.Round(ang1);i--){
                
                //if (i === (Mathf.Round(360+ang1) - 10) ){
				if (i == (Mathf.Round(ang1) + 10) ){
                    var tmpx = aPt2.x+((aDist+size) * Mathf.Cos(i * Mathf.PI/ 180));
                    var tmpy = aPt2.y+((aDist+size) * Mathf.Sin(i * Mathf.PI/ 180));
                    arrPoint[n] = new Vector2(tmpx, tmpy);
                    n--;
                    break;
                }
                //else if (i < (Mathf.Round(360+ang1) - 10)){
				else if (i > (Mathf.Round(ang1) + 10)){
                  var tmpx = aPt2.x+((aDist+(size-kurang)) * Mathf.Cos(i * Mathf.PI/ 180));
                  var tmpy = aPt2.y+((aDist+(size-kurang)) * Mathf.Sin(i * Mathf.PI/ 180));
                  arrPoint[n] = new Vector2(tmpx, tmpy);
                  n--;
                }

            }
        }else{
            //reversed
            ang2 = 360 - Mathf.Abs(calcDegree(pt2,pt1));

            var n = 0;
            for(var i= Mathf.Round(ang2);i >= Mathf.Round(ang1);i--){ 
                
                //draw line
                if (i == (Mathf.Round(ang1) + 10)){
                    var tmpx = (aPt1.x+((aDist-size) * Mathf.Cos(i * Mathf.PI/ 180)));
                    var tmpy = (aPt1.y+((aDist-size) * Mathf.Sin(i * Mathf.PI/ 180)));
                    arrPoint[n] = new Vector2(tmpx, tmpy);
                    n++;
                    break;
                }else if(i > (Mathf.Round(ang1) + 10)) {
                    var tmpx = (aPt1.x+((aDist-(size-kurang)) * Mathf.Cos(i * Mathf.PI/ 180)));
                    var tmpy = (aPt1.y+((aDist-(size-kurang)) * Mathf.Sin(i * Mathf.PI/ 180)));
                    arrPoint[n] = new Vector2(tmpx, tmpy);
                    n++;
                }
            }

            //create outer side polygon
            arrPoint[n] = pt2;
            n = 342;
            for (var i = Mathf.Round(ang2);i>= Mathf.Round(ang1);i--){ 
            //for(var i= Mathf.Round(ang2);i <= Mathf.Round(ang1);i++){ 
                
                if (i == (Mathf.Round(ang1) + 10) ){
                    var tmpx = aPt2.x+((aDist+size) * Mathf.Cos(i * Mathf.PI/ 180));
                    var tmpy = aPt2.y+((aDist+size) * Mathf.Sin(i * Mathf.PI/ 180));
                    arrPoint[n] = new Vector2(tmpx, tmpy);
                    n--;
                    break;
                }
                else if (i > (Mathf.Round(ang1) + 10)){
                  var tmpx = aPt2.x+((aDist+(size-kurang)) * Mathf.Cos(i * Mathf.PI/ 180));
                  var tmpy = aPt2.y+((aDist+(size-kurang)) * Mathf.Sin(i * Mathf.PI/ 180));
                  arrPoint[n] = new Vector2(tmpx, tmpy);
                  n--;
                }

            }
        }
        
        return arrPoint;
    }

    /// <summary>
    /// Drawing a Maneuver Bottom Arrow
    /// </summary>
    public List<Vector2> createCBM(Vector2 pt1, Vector2 pt2, float size, float kurang)
    {
        float ang1 = calcDegree(pt1,pt2);
        float ang2 = calcDegree(pt2,pt1);
        float aDist = calcDistance(pt1,pt2) / 2;
        List<Vector2> arrPoint = new List<Vector2>();
        
        for(int i = 0; i < 343; i++)
        arrPoint.Add(Vector2.zero);

        Vector2 aPt1 = new Vector2();
        Vector2 aPt2 = new Vector2();
        
        aPt1.x = getMidPoint(pt1, pt2).x - (2*size * Mathf.Cos((ang1)* Mathf.PI/ 180));
		aPt1.y = getMidPoint(pt1, pt2).y - (2*size * Mathf.Sin((ang1)* Mathf.PI/ 180));
		aPt2.x = getMidPoint(pt1, pt2).x + (size * Mathf.Cos((ang1)* Mathf.PI/ 180));
		aPt2.y = getMidPoint(pt1, pt2).y + (size * Mathf.Sin((ang1)* Mathf.PI/ 180));

        if (ang1 < ang2){
            //curved to rigth
            
            var n = 0;
            for(var i= Mathf.Round(ang2);i <= Mathf.Round(360 + ang1);i++){            
            //for(var i= Mathf.Round(ang2);i <= Mathf.Round(ang1);i++){            

                //draw line
                if (i == (Mathf.Round(360+ang1) - 10)){
                    var tmpx = (aPt1.x+((aDist-size) * Mathf.Cos(i * Mathf.PI/ 180)));
                    var tmpy = (aPt1.y+((aDist-size) * Mathf.Sin(i * Mathf.PI/ 180)));
                    arrPoint[n] = new Vector2(tmpx, tmpy);
                    n++;
                    break;
                }else if(i < (Mathf.Round(360+ang1) - 10)) {
                    var tmpx = (aPt1.x+((aDist-(size-kurang)) * Mathf.Cos(i * Mathf.PI/ 180)));
                    var tmpy = (aPt1.y+((aDist-(size-kurang)) * Mathf.Sin(i * Mathf.PI/ 180)));
                    arrPoint[n] = new Vector2(tmpx, tmpy);
                    n++;
                }
            }

            //create outer side polygon
            arrPoint[n] = pt2;
            n = 342;
            for (var i = Mathf.Round(ang2);i<= Mathf.Round(360 + ang1);i++){ 
            //for(var i= Mathf.Round(ang2);i <= Mathf.Round(ang1);i++){ 
                
                if (i == (Mathf.Round(360+ang1) - 10) ){
                    var tmpx = aPt2.x+((aDist+size) * Mathf.Cos(i * Mathf.PI/ 180));
                    var tmpy = aPt2.y+((aDist+size) * Mathf.Sin(i * Mathf.PI/ 180));
                    arrPoint[n] = new Vector2(tmpx, tmpy);
                    n--;
                    break;
                }
                else if (i < (Mathf.Round(360+ang1) - 10)){
                  var tmpx = aPt2.x+((aDist+(size-kurang)) * Mathf.Cos(i * Mathf.PI/ 180));
                  var tmpy = aPt2.y+((aDist+(size-kurang)) * Mathf.Sin(i * Mathf.PI/ 180));
                  arrPoint[n] = new Vector2(tmpx, tmpy);
                  n--;
                }

            }
        }else{
            //reversed
            ang2 = 360 - Mathf.Abs(calcDegree(pt2,pt1));

            var n = 0;
            for(var i= Mathf.Round(ang2);i <= Mathf.Round(360 + ang1);i++){            
            //for(var i= Mathf.Round(ang2);i <= Mathf.Round(ang1);i++){ 
                
                //draw line
                if (i == (Mathf.Round(360+ang1) - 10)){
                    var tmpx = (aPt1.x+((aDist-size) * Mathf.Cos(i * Mathf.PI/ 180)));
                    var tmpy = (aPt1.y+((aDist-size) * Mathf.Sin(i * Mathf.PI/ 180)));
                    arrPoint[n] = new Vector2(tmpx, tmpy);
                    n++;
                    break;
                }else if(i < (Mathf.Round(360+ang1) - 10)) {
                    var tmpx = (aPt1.x+((aDist-(size-kurang)) * Mathf.Cos(i * Mathf.PI/ 180)));
                    var tmpy = (aPt1.y+((aDist-(size-kurang)) * Mathf.Sin(i * Mathf.PI/ 180)));
                    arrPoint[n] = new Vector2(tmpx, tmpy);
                    n++;
                }
            }

            //create outer side polygon
            arrPoint[n] = pt2;
            n = 342;
            for (var i = Mathf.Round(ang2);i<= Mathf.Round(360 + ang1);i++){ 
            //for(var i= Mathf.Round(ang2);i <= Mathf.Round(ang1);i++){ 
                
                if (i == (Mathf.Round(360+ang1) - 10) ){
                    var tmpx = aPt2.x+((aDist+size) * Mathf.Cos(i * Mathf.PI/ 180));
                    var tmpy = aPt2.y+((aDist+size) * Mathf.Sin(i * Mathf.PI/ 180));
                    arrPoint[n] = new Vector2(tmpx, tmpy);
                    n--;
                    break;
                }
                else if (i < (Mathf.Round(360+ang1) - 10)){
                  var tmpx = aPt2.x+((aDist+(size-kurang)) * Mathf.Cos(i * Mathf.PI/ 180));
                  var tmpy = aPt2.y+((aDist+(size-kurang)) * Mathf.Sin(i * Mathf.PI/ 180));
                  arrPoint[n] = new Vector2(tmpx, tmpy);
                  n--;
                }

            }
        }
        
        return arrPoint;
    }
}
