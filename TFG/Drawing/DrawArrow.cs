﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using GlobalVariables;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using HelperPlotting;
using Plot.FormasiPlot;

public class DrawArrow : MonoBehaviour
{
    public static DrawArrow instance;

    [Header("Online Map")]
    [Space(5)]
    public OnlineMaps onlineMaps;
    public Shader shader;

    [Header("JArray")]
    [Space(5)]
    JArray array_polygon = new JArray();

    [Header("Color Picker")]
    [Space(5)]
    [SerializeField] FlexibleColorPicker colorPicker;
    public Image outColor;

    [Header("Plot Draw")]
    [Space(5)]
    public bool isDrawingSquare;
    public bool isDrawingCircle;
    public bool isDrawingElipse;
    public bool isDrawingArrow;
    public bool isDrawingCurveTop;
    public bool isDrawingCurveBottom;
    public bool isDrawingManeuverTop;
    public bool isDrawingManeuverBottom;
    public bool isEdit = false;
    public bool isDelete = false;
    public OnlineMapsDrawingPoly polygon;
    public List<Vector2> point = new List<Vector2>();
    Vector3 mousePos;
    int countPoint = 0;

    [Header("Rect")]
    [Space(5)]
    // public Rect rectGUI;
    [HideInInspector] public Vector2 worldPos;
    [HideInInspector] public Vector2 nowWorldPos;
    OnlineMapsVector2d posDouble;
    OnlineMapsVector2d nowPosDouble;
    public OnlineMapsDrawingRect rectMap;
    private static Texture2D _whiteTexture;
    OnlineMapsDrawingElement poly;
    [HideInInspector] public GameObject polyObj;
    [HideInInspector] public List<Vector2> pointRect = new List<Vector2>();
    [HideInInspector] public List<GameObject> polyList = new List<GameObject>();
    bool isDragging = false;
    int n = 0;

    [Header("Circle")]
    [Space(5)]
    public float radiusKM = 0f;
    public int segments = 64;

    [Header("Edit")]
    [Space(5)]
    public GameObject editObj;
    public Animator editCanva;
    public OnlineMapsDrawingPoly polyNow;
    [SerializeField] FlexibleColorPicker colorPickerEdit;
    [SerializeField] Image outBorderImage;
    [SerializeField] Image outFillImage;
    public List<Vector2> editPoints;
    [HideInInspector] public List<Vector2> editPointsNow;
    OnlineMapsDrawingPoly polyBefore;
    OnlineMapsDrawingElement scale;
    OnlineMapsDrawingElement scaleBefore;
    List<Vector2> scalePoints;
    List<Vector2> scaleBeforePoints;
    bool objectDrag = false;
    bool objectScale = false;
    bool isBorder = false;
    bool isFill = false;

    [Header("Poly List")]
    [Space(5)]
    public List<OnlineMapsDrawingElement> drawingList = new List<OnlineMapsDrawingElement>();
    float beforeX = 0;
    float beforeY = 0;
    float highestX = 0;
    float lowestX = 0;
    float highestY = 0;
    float lowestY = 0;

    [Header("Poly Destroy")]
    [Space(5)]
    public GameObject polyDestroy;
    private RaycastHit hit;

    [Header("Toggler")]
    [Space(5)]
    [SerializeField] SidemenuToggler lineTog;
    [SerializeField] SidemenuToggler polyTog;
    [SerializeField] SidemenuToggler rectTog;
    [SerializeField] SidemenuToggler cirTog;
    [SerializeField] SidemenuToggler arrowTog;
    [SerializeField] SidemenuToggler curveDown;
    [SerializeField] SidemenuToggler curveUp;
    [SerializeField] SidemenuToggler maneuverDown;
    [SerializeField] SidemenuToggler maneuverUp;

    public void BorderToggle(bool toggle)
    {
        colorPickerEdit.color = outBorderImage.color;
        isBorder = toggle;
    }

    public void FillToggle(bool toggle)
    {
        colorPickerEdit.color = outFillImage.color;
        isFill = toggle;
    }

    public void OnEditColorToggle()
    {
        if (isBorder)
            colorPickerEdit.gameObject.GetComponent<Animator>().Play("FadeIn");
        else
            colorPickerEdit.gameObject.GetComponent<Animator>().Play("FadeOut");

        if (isFill)
            colorPickerEdit.gameObject.GetComponent<Animator>().Play("FadeIn");
        else
            colorPickerEdit.gameObject.GetComponent<Animator>().Play("FadeOut");
    }

    void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        outColor.color = colorPicker.color;

        if (isBorder)
            outBorderImage.color = colorPickerEdit.color;

        if (isFill)
            outFillImage.color = colorPickerEdit.color;

        if (drawingList.Count > 0)
        {
            foreach (OnlineMapsDrawingElement element in drawingList)
            {
                if (element.instance.GetComponent<DrawStat>() == null)
                {
                    OnlineMapsDrawingPoly polygonOnList = element as OnlineMapsDrawingPoly;
                    DrawStat dS = element.instance.AddComponent<DrawStat>();

                    if (SessionUser.id != null)
                        dS.userID = SessionUser.id.ToString();
                    dS.polyName = polygonOnList.name;
                    dS.type = polygonOnList.polyType;
                    dS.points = polygonOnList.points as List<Vector2>;
                    dS.gameObject.tag = "Plot_Drawings";

                    if (polygonOnList.borderColor == Color.blue)
                        dS.color = "blue";
                    else if (polygonOnList.borderColor == Color.red)
                        dS.color = "red";
                    else
                        dS.color = "#" + ColorUtility.ToHtmlStringRGB(polygonOnList.borderColor);

                    if (polygonOnList.backgroundColor == Color.blue)
                        dS.fillColor = "blue";
                    else if (polygonOnList.backgroundColor == Color.red)
                        dS.fillColor = "red";
                    else
                        dS.fillColor = "#" + ColorUtility.ToHtmlStringRGB(polygonOnList.backgroundColor);

                    if (dS.type == DrawStat.polyType.Square)
                        dS.drawType = "Rect";
                    else if (dS.type == DrawStat.polyType.Circle)
                        dS.drawType = "Circle";
                    else if (dS.type == DrawStat.polyType.Arrow)
                        dS.drawType = "arrow";
                    else if (dS.type == DrawStat.polyType.Arrow_Curve_Bottom)
                        dS.drawType = "arrowLengkung";
                    else if (dS.type == DrawStat.polyType.Arrow_Curve_Top)
                        dS.drawType = "arrowLengkungAtas";
                    else if (dS.type == DrawStat.polyType.Arrow_Maneuver_Bottom)
                        dS.drawType = "arrowManuverBawah";
                    else if (dS.type == DrawStat.polyType.Arrow_Maneuver_Top)
                        dS.drawType = "arrowManuverAtas";
                    else if (dS.type == DrawStat.polyType.Polygon)
                        dS.drawType = "Polygon";
                    else if (dS.type == DrawStat.polyType.LineString)
                        dS.drawType = "LineString";

                    if (dS.type == DrawStat.polyType.Circle)
                        dS.gameObject.tag = "draw-circle";
                    else if (dS.type != DrawStat.polyType.LineString)
                        dS.gameObject.tag = "draw-polygon";

                    // JObject new_polygon = new JObject
                    // {
                    //     { "type", dS.drawType },
                    //     { "coordinates", "[[" + ListVectorToText(dS.points) + "]]" }
                    // };
                    // array_polygon.Add(new_polygon);

                    if (dS.type != DrawStat.polyType.Circle)
                    {
                        var coordinates = new List<List<double>>();

                        foreach (Vector2 _child in dS.points)
                        {
                            var coordinate = new List<double>() { _child.x, _child.y };
                            coordinates.Add(coordinate);
                        }

                        var finalGeo = new List<List<List<double>>>() { coordinates };

                        var geoPoly = new EntityToolsGeometryPolygon();
                        geoPoly.type = "Polygon";
                        geoPoly.coordinates = finalGeo;
                        dS.geometry = EntityToolsGeometryPolygon.ToString(geoPoly);
                    }
                    else if (dS.type != DrawStat.polyType.LineString)
                    {
                        var geoCircle = new EntityToolsGeometryCircle();
                        geoCircle.type = "circle";
                        geoCircle.coordinates = new List<double>() { dS.points[0].x, dS.points[0].y };
                        dS.geometry = EntityToolsGeometryCircle.ToString(geoCircle);
                    }

                    var p = new EntityToolsProperties();
                    if (dS.type == DrawStat.polyType.Polygon || dS.type == DrawStat.polyType.Square || dS.type == DrawStat.polyType.LineString)
                    {
                        p.stroke = true;
                        p.color = dS.color;
                        p.fillColor = dS.fillColor;
                        p.weight = polygonOnList.borderWidth / 0.05f;
                        p.opacity = polygonOnList.borderColor.a;
                        p.fillOpacity = polygonOnList.backgroundColor.a;
                        p.clickable = true;
                        p.dashArray = "0, 0";
                        p.transform = true;
                        p.draggable = true;
                        p.interactive = true;
                        p.className = "leaflet-path-draggable";
                    }
                    else if (dS.type == DrawStat.polyType.Circle)
                    {
                        p.stroke = true;
                        p.color = dS.color;
                        p.fillColor = dS.fillColor;
                        p.weight = polygonOnList.borderWidth / 0.05f;
                        p.opacity = polygonOnList.borderColor.a;
                        p.fillOpacity = polygonOnList.backgroundColor.a;
                        p.clickable = true;
                        p.dashArray = "0, 0";
                        p.transform = true;
                        p.draggable = true;
                        p.radius = polygonOnList.radius;
                        p.interactive = true;
                        p.className = "leaflet-path-draggable";
                    }
                    else
                    {
                        p.stroke = true;
                        p.color = dS.color;
                        p.fillColor = dS.fillColor;
                        p.weight = polygonOnList.borderWidth / 0.05f;
                        p.opacity = polygonOnList.borderColor.a;
                        p.fillOpacity = polygonOnList.backgroundColor.a;
                        p.clickable = true;
                        p.dashArray = "0, 0";
                        p.transform = true;
                        p.draggable = true;
                        p.jenisArrow = dS.drawType;
                        p.interactive = true;
                        p.className = "leaflet-path-draggable";
                    }

                    dS.properties = EntityToolsProperties.ToString(p);

                    dS.transform.position = new Vector3(0, 1, 0);
                }
                else
                {
                    OnlineMapsDrawingPoly polygonOnList = element as OnlineMapsDrawingPoly;
                    DrawStat dS = element.instance.GetComponent<DrawStat>();
                    dS.points = polygonOnList.points as List<Vector2>;
                    
                    if (dS.type == DrawStat.polyType.Square)
                        dS.drawType = "Rect";
                    else if (dS.type == DrawStat.polyType.Circle)
                        dS.drawType = "Circle";
                    else if (dS.type == DrawStat.polyType.Arrow)
                        dS.drawType = "arrow";
                    else if (dS.type == DrawStat.polyType.Arrow_Curve_Bottom)
                        dS.drawType = "arrowLengkung";
                    else if (dS.type == DrawStat.polyType.Arrow_Curve_Top)
                        dS.drawType = "arrowLengkungAtas";
                    else if (dS.type == DrawStat.polyType.Arrow_Maneuver_Bottom)
                        dS.drawType = "arrowManuverBawah";
                    else if (dS.type == DrawStat.polyType.Arrow_Maneuver_Top)
                        dS.drawType = "arrowManuverAtas";
                    else if (dS.type == DrawStat.polyType.Polygon)
                        dS.drawType = "Polygon";
                    else if (dS.type == DrawStat.polyType.LineString)
                        dS.drawType = "LineString";

                    if (polygonOnList.borderColor == Color.blue)
                        dS.color = "blue";
                    else if (polygonOnList.borderColor == Color.red)
                        dS.color = "red";
                    else
                        dS.color = "#" + ColorUtility.ToHtmlStringRGB(polygonOnList.borderColor);

                    if (polygonOnList.backgroundColor == Color.blue)
                        dS.fillColor = "blue";
                    else if (polygonOnList.backgroundColor == Color.red)
                        dS.fillColor = "red";
                    else
                        dS.fillColor = "#" + ColorUtility.ToHtmlStringRGB(polygonOnList.backgroundColor);

                    if (dS.type != DrawStat.polyType.Circle)
                    {
                        var coordinates = new List<List<double>>();

                        foreach (Vector2 _child in dS.points)
                        {
                            var coordinate = new List<double>() { _child.x, _child.y };
                            coordinates.Add(coordinate);
                        }

                        var finalGeo = new List<List<List<double>>>() { coordinates };

                        var geoPoly = new EntityToolsGeometryPolygon();
                        geoPoly.type = "Polygon";
                        geoPoly.coordinates = finalGeo;
                        dS.geometry = EntityToolsGeometryPolygon.ToString(geoPoly);
                    }
                    else if (dS.type != DrawStat.polyType.LineString)
                    {
                        var geoCircle = new EntityToolsGeometryCircle();
                        geoCircle.type = "circle";
                        geoCircle.coordinates = new List<double>() { polygonOnList.center.x, polygonOnList.center.y };
                        dS.geometry = EntityToolsGeometryCircle.ToString(geoCircle);
                    }

                    var p = new EntityToolsProperties();
                    if (dS.type == DrawStat.polyType.Polygon || dS.type == DrawStat.polyType.Square || dS.type == DrawStat.polyType.LineString)
                    {
                        p.stroke = true;
                        p.color = dS.color;
                        p.fillColor = dS.fillColor;
                        p.weight = polygonOnList.borderWidth / 0.05f;
                        p.opacity = polygonOnList.borderColor.a;
                        p.fillOpacity = polygonOnList.backgroundColor.a;
                        p.clickable = true;
                        p.dashArray = "0, 0";
                        p.transform = true;
                        p.draggable = true;
                        p.interactive = true;
                        p.className = "leaflet-path-draggable";
                    }
                    else if (dS.type == DrawStat.polyType.Circle)
                    {
                        p.stroke = true;
                        p.color = dS.color;
                        p.fillColor = dS.fillColor;
                        p.weight = polygonOnList.borderWidth / 0.05f;
                        p.opacity = polygonOnList.borderColor.a;
                        p.fillOpacity = polygonOnList.backgroundColor.a;
                        p.clickable = true;
                        p.dashArray = "0, 0";
                        p.transform = true;
                        p.draggable = true;
                        p.radius = polygonOnList.radius;
                        p.interactive = true;
                        p.className = "leaflet-path-draggable";
                    }
                    else
                    {
                        p.stroke = true;
                        p.color = dS.color;
                        p.fillColor = dS.fillColor;
                        p.weight = polygonOnList.borderWidth / 0.05f;
                        p.opacity = polygonOnList.borderColor.a;
                        p.fillOpacity = polygonOnList.backgroundColor.a;
                        p.clickable = true;
                        p.dashArray = "0, 0";
                        p.transform = true;
                        p.draggable = true;
                        p.jenisArrow = dS.drawType;
                        p.interactive = true;
                        p.className = "leaflet-path-draggable";
                    }

                    dS.properties = EntityToolsProperties.ToString(p);
                }
            }
        }

        IsDrawing();

        if (GameObject.Find("Drawings") != null)
            foreach (Transform child in GameObject.Find("Drawings").transform)
            {
                // if(GameObject.Find("Drawings").transform.childCount > 0)
                if (child.GetComponent<BoxCollider>() == null)
                {
                    child.gameObject.AddComponent<BoxCollider>();
                    child.gameObject.layer = LayerMask.NameToLayer("Drawings");
                }
            }

        if (polyNow != null)
        {
            editPoints = polyNow.points as List<Vector2>;
        }
    }

    public void DrawingSquare(bool turnOn)
    {
        if (turnOn)
        {
            cancelDraw();
            isDrawingSquare = true;
            isDrawingCircle = false;
            isDrawingManeuverBottom = false;
            isDrawingManeuverTop = false;
            isDrawingArrow = false;
            isDrawingCurveBottom = false;
            isDrawingCurveTop = false;
            DrawPolygon.Instance.cancelDraw();
            DrawPolyLine.Instance.cancelDraw();
            isDelete = false;
        }
        else
        {
            isDrawingSquare = false;
        }
    }

    public void DrawingCircle(bool turnOn)
    {
        if (turnOn)
        {
            cancelDraw();
            isDrawingSquare = false;
            isDrawingManeuverBottom = false;
            isDrawingManeuverTop = false;
            isDrawingArrow = false;
            isDrawingCurveBottom = false;
            isDrawingCurveTop = false;
            isDelete = false;
            isDrawingCircle = true;
            DrawPolygon.Instance.cancelDraw();
            DrawPolyLine.Instance.cancelDraw();
        }
        else
        {
            isDrawingCircle = false;
        }
    }

    public void DrawingManeuverBottom(bool turnOn)
    {
        if (turnOn)
        {
            cancelDraw();
            isDrawingSquare = false;
            isDrawingCircle = false;
            isDrawingManeuverTop = false;
            isDrawingArrow = false;
            isDrawingCurveBottom = false;
            isDrawingCurveTop = false;
            isDelete = false;
            isDrawingManeuverBottom = true;
            DrawPolygon.Instance.cancelDraw();
            DrawPolyLine.Instance.cancelDraw();
        }
        else
        {
            isDrawingManeuverBottom = false;
        }
    }

    public void DrawingManeuverTop(bool turnOn)
    {
        if (turnOn)
        {
            cancelDraw();
            isDrawingSquare = false;
            isDrawingCircle = false;
            isDrawingManeuverBottom = false;
            isDrawingArrow = false;
            isDrawingCurveBottom = false;
            isDrawingCurveTop = false;
            isDelete = false;
            isDrawingManeuverTop = true;
            DrawPolygon.Instance.cancelDraw();
            DrawPolyLine.Instance.cancelDraw();
        }
        else
        {
            isDrawingManeuverTop = false;
        }
    }

    public void DrawingArrow(bool turnOn)
    {
        if (turnOn)
        {
            cancelDraw();
            isDrawingSquare = false;
            isDrawingCircle = false;
            isDrawingManeuverBottom = false;
            isDrawingManeuverTop = false;
            isDrawingCurveBottom = false;
            isDrawingCurveTop = false;
            isDelete = false;
            isDrawingArrow = true;
            DrawPolygon.Instance.cancelDraw();
            DrawPolyLine.Instance.cancelDraw();
        }
        else
        {
            isDrawingArrow = false;
        }
    }

    public void DrawingCurveBottom(bool turnOn)
    {
        if (turnOn)
        {
            cancelDraw();
            isDrawingSquare = false;
            isDrawingCircle = false;
            isDrawingManeuverBottom = false;
            isDrawingManeuverTop = false;
            isDrawingArrow = false;
            isDrawingCurveTop = false;
            isDelete = false;
            isDrawingCurveBottom = true;
            DrawPolygon.Instance.cancelDraw();
            DrawPolyLine.Instance.cancelDraw();
        }
        else
        {
            isDrawingCurveBottom = false;
        }
    }

    public void DrawingCurveTop(bool turnOn)
    {
        if (turnOn)
        {
            cancelDraw();
            isDrawingSquare = false;
            isDrawingCircle = false;
            isDrawingManeuverBottom = false;
            isDrawingManeuverTop = false;
            isDrawingArrow = false;
            isDrawingCurveBottom = false;
            isDelete = false;
            isDrawingCurveTop = true;
            DrawPolygon.Instance.cancelDraw();
            DrawPolyLine.Instance.cancelDraw();
        }
        else
        {
            isDrawingCurveTop = false;
        }
    }

    public void EditOn(bool nowEdit)
    {
        if (nowEdit)
        {
            cancelDraw();
            isDrawingSquare = false;
            isDrawingCircle = false;
            isDrawingManeuverBottom = false;
            isDrawingManeuverTop = false;
            isDrawingArrow = false;
            isDrawingCurveBottom = false;
            isDrawingCurveTop = false;
            isDelete = false;
            isEdit = true;
            DrawPolygon.Instance.cancelDraw();
            DrawPolyLine.Instance.cancelDraw();
        }
        else
        {
            if (scale != null)
                scale.Dispose();
            editCanva.Play("FadeOut");
            colorPickerEdit.gameObject.GetComponent<Animator>().Play("FadeOut");
            polyNow = null;
            polyBefore = null;
            isEdit = false;
        }
    }

    public void DeleteOn(bool nowDelete)
    {
        if (nowDelete)
        {
            cancelDraw();
            isDrawingSquare = false;
            isDrawingCircle = false;
            isDrawingManeuverBottom = false;
            isDrawingManeuverTop = false;
            isDrawingArrow = false;
            isDrawingCurveBottom = false;
            isDrawingCurveTop = false;
            isEdit = false;
            isDelete = true;
            DrawPolygon.Instance.cancelDraw();
            DrawPolyLine.Instance.cancelDraw();

            if (scale != null)
                scale.Dispose();
        }
        else
            isDelete = false;
    }

    public void Editing()
    {
        if (isEdit == false)
        {
            cancelDraw();
            isDrawingSquare = false;
            isDrawingCircle = false;
            isDrawingManeuverBottom = false;
            isDrawingManeuverTop = false;
            isDrawingArrow = false;
            isDrawingCurveBottom = false;
            isDrawingCurveTop = false;
            isDelete = false;
            isEdit = true;
            DrawPolygon.Instance.cancelDraw();
            DrawPolyLine.Instance.cancelDraw();
        }
        else
        {
            if (scale != null)
                scale.Dispose();
            editCanva.Play("FadeOut");
            colorPickerEdit.gameObject.GetComponent<Animator>().Play("FadeOut");
            polyNow = null;
            polyBefore = null;
            isEdit = false;
        }
    }

    public void Deleting()
    {
        if (isDelete == false)
        {
            cancelDraw();
            isDrawingSquare = false;
            isDrawingCircle = false;
            isDrawingManeuverBottom = false;
            isDrawingManeuverTop = false;
            isDrawingArrow = false;
            isDrawingCurveBottom = false;
            isDrawingCurveTop = false;
            isEdit = false;
            isDelete = true;
//            DrawPolygon.Instance.cancelDraw();
//            DrawPolyLine.Instance.cancelDraw();

            if (scale != null)
                scale.Dispose();
        }
        else
        {
            isDelete = false;
        }
    }

    public void cancelDraw()
    {
        lineTog.toggleOff();
        polyTog.toggleOff();
        rectTog.toggleOff();
        cirTog.toggleOff();
        arrowTog.toggleOff();
        curveDown.toggleOff();
        curveUp.toggleOff();
        maneuverDown.toggleOff();
        maneuverUp.toggleOff();
    }

    public void IsDrawing()
    {
        if (isDrawingSquare == true)
        {
            DrawKotak();
        }

        if (isDrawingCircle == true)
        {
            DrawCircle();
        }

        if (isDrawingElipse == true)
        {
            DrawElipse();
        }

        if (isDrawingManeuverBottom == true)
        {
            DrawManeuverBawah();
        }

        if (isDrawingManeuverTop == true)
        {
            DrawManeuverAtas();
        }

        if (isDrawingArrow == true)
        {
            DrawPanah();
        }

        if (isDrawingCurveBottom == true)
        {
            DrawCurveBawah();
        }

        if (isDrawingCurveTop == true)
        {
            DrawCurveAtas();
        }

        if (isEdit == true)
        {
            Move();
            Scale();
        }
    }

    public void DrawKotak()
    {
        onlineMaps.blockAllInteractions = true;
        if (Input.GetMouseButtonDown(0))
        {
            polyList.Clear();
            double lng, lat;

            if (OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
                worldPos = new Vector2((float)lng, (float)lat);
        }

        if (Input.GetMouseButton(0))
        {
            double lng, lat;

            if (OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
                nowWorldPos = new Vector2((float)lng, (float)lat);

            if (nowWorldPos != worldPos)
            {
                if (poly != null)
                    poly.Dispose();

                var points = new ArrowManeuver().DrawRect(worldPos, nowWorldPos);

                polygon = new OnlineMapsDrawingPoly(points, outColor.color, .2f, new Color(0, 0, 0, 0.5f));

                polygon.polyType = DrawStat.polyType.Square;
                poly = OnlineMapsDrawingElementManager.AddItem(polygon);

                poly.OnClick += OnElementClick;
                poly.OnPress += OnElementPress;
                poly.OnRelease += OnElementRelease;
            }

            if (poly != null)
                OnlineMaps.instance.Redraw();
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (poly != null && polygon.polyType == DrawStat.polyType.Square)
            {
                worldPos = nowWorldPos;

                polygon.polyID = UnityEngine.Random.Range(1, 1000);
                poly.name = "Rect_" + SessionUser.id + "_" + polygon.polyID;
                polygon.name = poly.name;

                OnlineMaps.instance.Redraw();

                OnlineMapsDrawingElement polyForList = poly;
                drawingList.Add(polyForList);

                poly = null;
                onlineMaps.blockAllInteractions = false;
                isDrawingSquare = false;
                n = 0;
            }
        }
    }

    public void DrawCircle()
    {
        onlineMaps.blockAllInteractions = true;
        double lng, lat;
        if (Input.GetMouseButtonDown(0))
        {
            if (OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
            {
                worldPos = new Vector2((float)lng, (float)lat);
                point.Add(new Vector2((float)lng, (float)lat));
            }
        }

        double nlng, nlat;
        if (Input.GetMouseButton(0))
        {
            Vector2 p1 = point[0];
            if (OnlineMapsControlBase.instance.GetCoords(out nlng, out nlat))
                nowWorldPos = new Vector2((float)nlng, (float)nlat);

            // Create a new polygon to draw a circle
            if (nowWorldPos != worldPos)
            {
                if (poly != null)
                    poly.Dispose();

                radiusKM = (float)OnlineMapsUtils.DistanceBetweenPointsD(nowWorldPos, p1);
                var points = new ArrowManeuver().DrawCircle(p1.x, p1.y, radiusKM);

                polygon = new OnlineMapsDrawingPoly(points, outColor.color, .2f, new Color(0, 0, 0, 0.5f));
                polygon.radius = radiusKM * 1000;

                poly = OnlineMapsDrawingElementManager.AddItem(polygon);
                polygon.polyType = DrawStat.polyType.Circle;

                poly.OnClick += OnElementClick;
                poly.OnPress += OnElementPress;
                poly.OnRelease += OnElementRelease;
                // n++;
            }

            worldPos = nowWorldPos;

            if (poly != null)
                OnlineMaps.instance.Redraw();
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (poly != null && polygon.polyType == DrawStat.polyType.Circle)
            {
                polygon.polyID = UnityEngine.Random.Range(1, 1000);
                poly.name = "circle_" + SessionUser.id + "_" + polygon.polyID;
                polygon.name = poly.name;

                OnlineMaps.instance.Redraw();

                OnlineMapsDrawingElement polyForList = poly;
                drawingList.Add(polyForList);

                poly = null;
                radiusKM = 0;
                point.Clear();
                onlineMaps.blockAllInteractions = false;
                isDrawingCircle = false;
            }
        }
    }

    public void DrawElipse()
    {
        onlineMaps.blockAllInteractions = true;

        if(Input.GetMouseButtonDown(0))
        {
            double lng, lat;
            if(OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
            {
                worldPos = new Vector2((float)lng, (float)lat);
                point.Add(worldPos);
            }
        }

        if(Input.GetMouseButton(0))
        {
            Vector2 p1 = point[0];
            
            double lng, lat;
            if(OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
            nowWorldPos = new Vector2((float)lng, (float)lat);

            if(nowWorldPos != worldPos)
            {
                if(poly != null)
                poly.Dispose();

                radiusKM = (float)OnlineMapsUtils.DistanceBetweenPointsD(nowWorldPos, p1);
                if(radiusKM < 1) radiusKM = 1;
                var angle = (float)new DrawRoute().GetAngleOfLineBetweenTwoPoints(worldPos, nowWorldPos) - 90;
                var points = new ArrowManeuver().DrawElipse(p1.x, p1.y, radiusKM);

                polygon = new OnlineMapsDrawingPoly(points, outColor.color, .2f, new Color(0, 0, 0, 0.5f));
                polygon.radius = radiusKM * 1000;
                polygon.polyType = DrawStat.polyType.Elipse;

                poly = OnlineMapsDrawingElementManager.AddItem(polygon);
                poly.DrawOnTileset(OnlineMapsTileSetControl.instance, OnlineMapsDrawingElementManager.instance.Count - 1);
                poly.instance.transform.localPosition = Vector3.up;
            }
            
            worldPos = nowWorldPos;
            if (poly != null)
                OnlineMaps.instance.Redraw();
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (poly != null && polygon.polyType == DrawStat.polyType.Elipse)
            {
                polygon.polyID = UnityEngine.Random.Range(1, 1000);
                poly.name = "elipse_" + SessionUser.id + "_" + polygon.polyID;
                polygon.name = poly.name;

                OnlineMaps.instance.Redraw();

                OnlineMapsDrawingElement polyForList = poly;
                drawingList.Add(polyForList);

                poly = null;
                radiusKM = 0;
                point.Clear();
                onlineMaps.blockAllInteractions = false;
                isDrawingElipse = false;
            }
        }
    }

    public void DrawPanah()
    {
        if (Input.GetMouseButtonDown(0))
        {
            double lng, lat;
            if (OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
            {
                Vector2 po = new Vector2((float)lng, (float)lat);
                point.Add(po);
                countPoint++;
            }
        }

        if (countPoint == 2)
        {
            Vector2 p1 = point[0];
            Vector2 p2 = point[1];
            float size = getSize(p1, p2);
            float kurang = getKurang(size);
            List<Vector2> vertices2D = new ArrowManeuver().createSAP(p1, p2, size);

            polygon = new OnlineMapsDrawingPoly(vertices2D, outColor.color, .2f, new Color(0, 0, 0, 0.5f));

            poly = OnlineMapsDrawingElementManager.AddItem(polygon);
            polygon.polyID = UnityEngine.Random.Range(1, 1000);
            polygon.polyType = DrawStat.polyType.Arrow;
            poly.name = "arrow_" + SessionUser.id + "_" + polygon.polyID;
            polygon.name = poly.name;

            poly.OnClick += OnElementClick;
            poly.OnPress += OnElementPress;
            poly.OnRelease += OnElementRelease;

            point.Clear();
            countPoint = 0;
            OnlineMaps.instance.Redraw();
            OnlineMapsDrawingElement polyForList = poly;
            drawingList.Add(polyForList);
            poly = null;
            isDrawingArrow = false;
        }
    }

    public void DrawCurveAtas()
    {
        if (Input.GetMouseButtonDown(0))
        {
            double lng, lat;
            if (OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
            {
                Vector2 po = new Vector2((float)lng, (float)lat);
                point.Add(po);
                countPoint++;
            }
        }

        if (countPoint == 2)
        {
            Vector2 p1 = point[0];
            Vector2 p2 = point[1];
            float size = getSize(p1, p2);
            float kurang = getKurang(size);
            List<Vector2> vertices2D = new ArrowManeuver().createCAPRev(p1, p2, size, kurang);

            polygon = new OnlineMapsDrawingPoly(vertices2D, outColor.color, .2f, new Color(0, 0, 0, 0.5f));

            poly = OnlineMapsDrawingElementManager.AddItem(polygon);
            polygon.polyID = UnityEngine.Random.Range(1, 1000);
            polygon.polyType = DrawStat.polyType.Arrow_Curve_Top;
            poly.name = "arrow_" + SessionUser.id + "_" + polygon.polyID;
            polygon.name = poly.name;

            poly.OnClick += OnElementClick;
            poly.OnPress += OnElementPress;
            poly.OnRelease += OnElementRelease;

            point.Clear();
            countPoint = 0;
            OnlineMaps.instance.Redraw();
            OnlineMapsDrawingElement polyForList = poly;
            drawingList.Add(polyForList);
            poly = null;
            isDrawingCurveTop = false;
        }
    }

    public void DrawCurveBawah()
    {
        if (Input.GetMouseButtonDown(0))
        {
            double lng, lat;
            if (OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
            {
                Vector2 po = new Vector2((float)lng, (float)lat);
                point.Add(po);
                countPoint++;
            }
        }

        if (countPoint == 2)
        {
            Vector2 p1 = point[0];
            Vector2 p2 = point[1];
            float size = getSize(p1, p2);
            float kurang = getKurang(size);
            List<Vector2> vertices2D = new ArrowManeuver().createCAP(p1, p2, size, kurang);

            polygon = new OnlineMapsDrawingPoly(vertices2D, outColor.color, .2f, new Color(0, 0, 0, 0.5f));

            poly = OnlineMapsDrawingElementManager.AddItem(polygon);
            polygon.polyID = UnityEngine.Random.Range(1, 1000);
            polygon.polyType = DrawStat.polyType.Arrow_Curve_Bottom;
            poly.name = "arrow_" + SessionUser.id + "_" + polygon.polyID;
            polygon.name = poly.name;

            poly.OnClick += OnElementClick;
            poly.OnPress += OnElementPress;
            poly.OnRelease += OnElementRelease;

            point.Clear();
            countPoint = 0;
            OnlineMaps.instance.Redraw();
            OnlineMapsDrawingElement polyForList = poly;
            drawingList.Add(polyForList);
            poly = null;
            isDrawingCurveBottom = false;
        }
    }

    public void DrawManeuverAtas()
    {
        if (Input.GetMouseButtonDown(0))
        {
            double lng, lat;
            if (OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
            {
                Vector2 po = new Vector2((float)lng, (float)lat);
                point.Add(po);
                countPoint++;
            }
        }

        if (countPoint == 2)
        {
            Vector2 p1 = point[0];
            Vector2 p2 = point[1];
            float size = getSize(p1, p2);
            float kurang = getKurang(size);
            List<Vector2> vertices2D = new ArrowManeuver().createCAM(p1, p2, size, kurang);

            polygon = new OnlineMapsDrawingPoly(vertices2D, outColor.color, .2f);

            poly = OnlineMapsDrawingElementManager.AddItem(polygon);
            polygon.polyID = UnityEngine.Random.Range(1, 1000);
            polygon.polyType = DrawStat.polyType.Arrow_Maneuver_Top;
            poly.name = "arrow_" + SessionUser.id + "_" + polygon.polyID;
            polygon.name = poly.name;

            poly.OnClick += OnElementClick;
            poly.OnPress += OnElementPress;
            poly.OnRelease += OnElementRelease;

            point.Clear();
            countPoint = 0;
            OnlineMaps.instance.Redraw();
            OnlineMapsDrawingElement polyForList = poly;
            drawingList.Add(polyForList);
            poly = null;
            isDrawingManeuverTop = false;
        }
    }

    public void DrawManeuverBawah()
    {
        if (Input.GetMouseButtonDown(0))
        {
            double lng, lat;
            if (OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
            {
                Vector2 po = new Vector2((float)lng, (float)lat);
                point.Add(po);
                countPoint++;
            }
        }

        if (countPoint == 2)
        {
            Vector2 p1 = point[0];
            Vector2 p2 = point[1];
            float size = getSize(p1, p2);
            float kurang = getKurang(size);
            List<Vector2> vertices2D = new ArrowManeuver().createCBM(p1, p2, size, kurang);

            polygon = new OnlineMapsDrawingPoly(vertices2D, outColor.color, .2f);

            poly = OnlineMapsDrawingElementManager.AddItem(polygon);
            polygon.polyID = UnityEngine.Random.Range(1, 1000);
            polygon.polyType = DrawStat.polyType.Arrow_Maneuver_Bottom;
            poly.name = "arrow_" + SessionUser.id + "_" + polygon.polyID;
            polygon.name = poly.name;

            poly.OnClick += OnElementClick;
            poly.OnPress += OnElementPress;
            poly.OnRelease += OnElementRelease;

            point.Clear();
            countPoint = 0;
            OnlineMaps.instance.Redraw();
            OnlineMapsDrawingElement polyForList = poly;
            drawingList.Add(polyForList);
            poly = null;
            isDrawingManeuverBottom = false;
        }
    }

    public string ListVectorToText(List<Vector2> list)
    {
        string result = "";
        foreach (var listMember in list)
        {
            if (listMember != list[list.Count - 1])
                result += "[" + listMember.x.ToString() + "," + listMember.y.ToString() + "]" + ", ";
            else
                result += "[" + listMember.ToString() + "]";
        }
        return result;
    }

    float getSize(Vector2 p1, Vector2 p2)
    {
        float distance = Vector2.Distance(p1, p2);

        float ret = distance / 10;
        Debug.Log(ret);

        if (ret > 2)
        {
            return 2;
        }

        // if(ret < 1)
        // {
        //     return 1;
        // }

        return ret;
    }

    float getKurang(float size)
    {
        float ret = size / 3.333f;

        return ret;
    }

    public void OnEdit()
    {
        Transform polyTransform = editObj.transform;
        Transform scaleTransform = scale.instance.transform;
        TMP_InputField height = editCanva.transform.Find("Content/Height").GetChild(1).GetComponent<TMP_InputField>();
        TMP_InputField thickness = editCanva.transform.Find("Content/Thickness").GetChild(1).GetComponent<TMP_InputField>();
        TMP_InputField opacity = editCanva.transform.Find("Content/Opacity").GetChild(1).GetComponent<TMP_InputField>();

        if (thickness.text == "" || thickness.text == "." || thickness.text == "-" || float.Parse(thickness.text) == 0)
            Debug.Log("Ketebalan Tidak Boleh Kosong!!!");
        else if (opacity.text == "" || opacity.text == "." || opacity.text == "-" || float.Parse(opacity.text) == 0)
            Debug.Log("Ketransparanan Tidak Boleh Kosong!!!");
        else if (height.text == "" || height.text == "." || height.text == "-" || float.Parse(height.text) == 0)
            Debug.Log("Ketinggian Tidak Boleh Kosong!!!");
        else if (float.Parse(thickness.text) < 0)
            Debug.Log("Ketebalan Tidak Boleh Negatif!!!");
        else if (float.Parse(opacity.text) < 0)
            Debug.Log("Ketebalan Tidak Boleh Negatif!!!");
        else if (float.Parse(height.text) < 0)
            Debug.Log("Ketinggian Tidak Boleh Negatif!!!");
        else
        {
            Color borderColor = outBorderImage.color;
            Color fillColor = outFillImage.color;
            fillColor.a = float.Parse(opacity.text);

            polyTransform.position = new Vector3(0, float.Parse(height.text), 0);
            scaleTransform.position = new Vector3(0, float.Parse(height.text), 0);
            polyNow.borderWidth = float.Parse(thickness.text);
            polyNow.borderColor = borderColor;
            polyNow.backgroundColor = fillColor;
        }
    }

    public void OnDelete()
    {
        var trash = GameObject.Find("Trash");
        if(trash == null)
        {
            trash = new GameObject("Trash");
            trash.AddComponent<TrashObj>();
        }

        var tObj = new Trash();
        tObj.id = polyNow.name;
        tObj.type = "polygon";

        var tScript = trash.GetComponent<TrashObj>();
        tScript.trash.Add(tObj);
        
        drawingList.Remove(polyNow);
        polyNow.Dispose();
        scale.Dispose();
        editCanva.Play("FadeOut");
        colorPickerEdit.gameObject.GetComponent<Animator>().Play("FadeOut");
    }

    public void OnElementClick(OnlineMapsDrawingElement element)
    {
        polyNow = element as OnlineMapsDrawingPoly;
        editObj = polyNow.instance;
        
        if (isEdit)
        {
            editCanva.Play("FadeIn");
            outBorderImage.color = polyNow.borderColor;
            outFillImage.color = polyNow.backgroundColor;

            TMP_InputField height = editCanva.transform.Find("Content/Height").GetChild(1).GetComponent<TMP_InputField>();
            TMP_InputField thickness = editCanva.transform.Find("Content/Thickness").GetChild(1).GetComponent<TMP_InputField>();
            TMP_InputField opacity = editCanva.transform.Find("Content/Opacity").GetChild(1).GetComponent<TMP_InputField>();

            height.text = editObj.transform.position.y.ToString();
            thickness.text = polyNow.borderWidth.ToString();
            opacity.text = polyNow.backgroundColor.a.ToString();

            List<Vector2> vertices2D = polyNow.points as List<Vector2>;
            var scalePointOrigin = new ArrowManeuver().CreateScale(polyNow, vertices2D);

            if (polyBefore != polyNow || scaleBefore == null)
            {
                scale = OnlineMapsDrawingElementManager.AddItem(new OnlineMapsDrawingPoly(scalePointOrigin, Color.white, 1, Color.white));
                scale.OnPress += OnScalePress;
                scale.OnRelease += OnScaleRelease;
                scale.DrawOnTileset(OnlineMapsTileSetControl.instance, OnlineMapsDrawingElementManager.instance.Count - 1);
                scale.instance.transform.position = polyNow.instance.transform.position;

                if (scaleBefore != null)
                    scaleBefore.Dispose();
                scaleBefore = scale;

                Debug.Log(ListVectorToText(scalePointOrigin));
            }
            polyBefore = polyNow;
            editPointsNow = new List<Vector2>();

            for (int i = 0; i < vertices2D.Count; i++)
            {
                if (polyNow.polyType == DrawStat.polyType.Square)
                {
                    if (editPointsNow.Count < 4)
                    {
                        editPointsNow.Add(vertices2D[i]);
                    }
                }
                else
                {
                    editPointsNow.Add(vertices2D[i]);
                }
            }
        }

        if (isDelete)
        {
            var trash = GameObject.Find("Trash");
            if(trash == null)
            {
                trash = new GameObject("Trash");
                trash.AddComponent<TrashObj>();
            }

            var tObj = new Trash();
            tObj.id = editObj.name;
            tObj.type = "polygon";

            var tScript = trash.GetComponent<TrashObj>();
            tScript.trash.Add(tObj);

            drawingList.Remove(element);
            element.Dispose();
        }
    }

    public void OnElementPress(OnlineMapsDrawingElement element)
    {
        if (isEdit)
        {
            double lng, lat;

            if (OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
            {
                worldPos = new Vector2((float)lng, (float)lat);
                posDouble = new OnlineMapsVector2d(lng, lat);
            }

            objectDrag = true;
        }
    }

    public void OnElementRelease(OnlineMapsDrawingElement element)
    {
        if (isEdit)
        {
            objectDrag = false;
            OnlineMaps.instance.Redraw();
            OnlineMaps.instance.GetComponent<OnlineMapsTileSetControl>().allowUserControl = true;
        }
    }

    public void OnScalePress(OnlineMapsDrawingElement element)
    {
        List<Vector2> vertices2D = polyNow.points as List<Vector2>;
        GetLowAndHighestPoint(vertices2D);
        Debug.Log(lowestX + ", " + highestY);

        if (polyNow.polyType == DrawStat.polyType.Square)
        {
            vertices2D[0] = new Vector2(lowestX, highestY);
            vertices2D[1] = new Vector2(lowestX, lowestY);
            vertices2D[2] = new Vector2(highestX, lowestY);
            vertices2D[3] = new Vector2(highestX, highestY);
        }

        element.visible = false;
        objectScale = true;
    }

    public void OnScaleRelease(OnlineMapsDrawingElement element)
    {
        OnlineMaps.instance.GetComponent<OnlineMapsTileSetControl>().allowUserControl = true;
        objectScale = false;
        element.visible = true;
        OnlineMaps.instance.Redraw();
    }

    public void GetLowAndHighestPoint(List<Vector2> vertices2D)
    {
        foreach (Vector2 child in vertices2D)
        {
            if (beforeX == 0 && beforeY == 0)
            {
                highestX = child.x;
                lowestX = child.x;
                highestY = child.y;
                lowestY = child.y;
                beforeX = child.x;
                beforeY = child.y;
            }

            if (highestX > beforeX)
            {
                highestX = child.x;
                beforeX = child.x;
            }
            else if (lowestX < beforeX)
            {
                lowestX = child.x;
                beforeX = child.x;
            }

            if (lowestY < beforeY)
            {
                lowestY = child.y;
                beforeY = child.y;
            }
            else if (highestY > beforeY)
            {
                highestY = child.y;
                beforeY = child.y;
            }
        }
    }

    private void Move()
    {
        if (objectDrag)
        {
            OnlineMaps.instance.GetComponent<OnlineMapsTileSetControl>().allowUserControl = false;
            double lng, lat;
            if (OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
            {
                nowWorldPos = new Vector2((float)lng, (float)lat);

                List<Vector2> vertices2D = new List<Vector2>();

                if (polyNow != null)
                    vertices2D = polyNow.points as List<Vector2>;

                Vector2 diffPoints = new Vector2(nowWorldPos.x - worldPos.x, nowWorldPos.y - worldPos.y);

                if (nowWorldPos != worldPos)
                {
                    for (int i = 0; i < vertices2D.Count; i++)
                        vertices2D[i] += diffPoints;

                    OnlineMaps.instance.Redraw();
                }

                if (scale != null)
                {
                    OnlineMapsDrawingPoly scalePoly = scale as OnlineMapsDrawingPoly;
                    List<Vector2> vertices2DScale = scalePoly.points as List<Vector2>;

                    if (!objectScale)
                        if (nowWorldPos != worldPos)
                        {
                            for (int i = 0; i < vertices2DScale.Count; i++)
                                vertices2DScale[i] += diffPoints;

                            OnlineMaps.instance.Redraw();
                        }
                }

                worldPos = nowWorldPos;
            }
        }
    }

    private void Scale()
    {
        if (objectScale)
        {
            OnlineMaps.instance.GetComponent<OnlineMapsTileSetControl>().allowUserControl = false;
            double lng, lat;
            if (OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
            {
                nowWorldPos = new Vector2((float)lng, (float)lat);
                nowPosDouble = new OnlineMapsVector2d(lng, lat);
                var diffDouble = nowPosDouble - posDouble;
                var diffPoints = new Vector2(nowWorldPos.x - worldPos.x, nowWorldPos.y - worldPos.y);
                var scalePoly = scale as OnlineMapsDrawingPoly;
                var vertices2D = polyNow.points as List<Vector2>;
                var vertices2DScale = scalePoly.points as List<Vector2>;

                // List<Vector2> verticesRect = new List<Vector2>(){vertices2D[0], vertices2D[1], vertices2D[2], vertices2D[3]};
                // verticesRect[0] = new Vector2(lowestX, highestY);
                // verticesRect[1] = new Vector2(highestX, highestY);
                // verticesRect[2] = new Vector2(highestX, lowestY);
                // verticesRect[3] = new Vector2(lowestX, lowestY);

                if (nowWorldPos != worldPos)
                {
                    if (polyNow.polyType == DrawStat.polyType.Square)
                    {
                        // vertices2D[0] = verticesRect[0];
                        vertices2D[1] = new Vector2(vertices2D[0].x, nowWorldPos.y);
                        vertices2D[2] = nowWorldPos;
                        vertices2D[3] = new Vector2(nowWorldPos.x, vertices2D[0].y);

                        OnlineMaps.instance.Redraw();
                    }
                    else if (polyNow.polyType == DrawStat.polyType.Circle)
                    {
                        Vector2 p1 = polyNow.center;

                        // Create a new polygon to draw a circle
                        if (nowWorldPos != worldPos)
                        {
                            radiusKM = (float)OnlineMapsUtils.DistanceBetweenPointsD(nowWorldPos, p1);
                            var points = new ArrowManeuver().DrawCircle(p1.x, p1.y, radiusKM);
                            polyNow.radius = radiusKM * 1000;

                            for (int i = 0; i < vertices2D.Count; i++)
                                vertices2D[i] = points[i];
                        }

                        OnlineMaps.instance.Redraw();
                    }
                    else
                    {
                        Vector2 p2 = nowWorldPos;
                        Vector2 p1;

                        if (polyNow.polyType == DrawStat.polyType.Arrow)
                            p1 = editPointsNow[0];
                        else
                            p1 = editPointsNow[342];

                        List<Vector2> vertices2DArrow = new List<Vector2> { };

                        float size = getSize(p1, p2);
                        float kurang = getKurang(size);

                        if (polyNow.polyType == DrawStat.polyType.Arrow)
                            vertices2DArrow = new ArrowManeuver().createSAP(p1, p2, size);
                        else if (polyNow.polyType == DrawStat.polyType.Arrow_Curve_Top)
                            vertices2DArrow = new ArrowManeuver().createCAPRev(p1, p2, size, kurang);
                        else if (polyNow.polyType == DrawStat.polyType.Arrow_Curve_Bottom)
                            vertices2DArrow = new ArrowManeuver().createCAP(p1, p2, size, kurang);
                        else if (polyNow.polyType == DrawStat.polyType.Arrow_Maneuver_Top)
                            vertices2DArrow = new ArrowManeuver().createCAM(p1, p2, size, kurang);
                        else if (polyNow.polyType == DrawStat.polyType.Arrow_Maneuver_Bottom)
                            vertices2DArrow = new ArrowManeuver().createCBM(p1, p2, size, kurang);

                        for (int i = 0; i < vertices2D.Count; i++)
                            vertices2D[i] = vertices2DArrow[i];

                        OnlineMaps.instance.Redraw();
                    }

                    // var panjang10pers = OnlineMapsUtils.DistanceBetweenPoints(OnlineMapsControlBase.instance.GetCoords(new Vector2(0f, 0f)), OnlineMapsControlBase.instance.GetCoords(new Vector2(10f, 10f))) / 1.852f / 60f;

                    for (int i = 0; i < vertices2DScale.Count; i++)
                        vertices2DScale[i] += diffPoints;

                    worldPos = nowWorldPos;
                    // vertices2DScale = new ArrowManeuver().CreateScale(polyNow, vertices2D);
                    // OnlineMaps.instance.Redraw();
                    // Debug.Log(ListVectorToText(vertices2DScale));
                }

                OnlineMaps.instance.Redraw();
            }
        }
    }

    public void DeleteAll()
    {
        drawingList.Clear();

        foreach (OnlineMapsDrawingElement element in OnlineMapsDrawingElementManager.instance)
            if (element.isObstacleArea)
                element.Dispose();
    }
}
