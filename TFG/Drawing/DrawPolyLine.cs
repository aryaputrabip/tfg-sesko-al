using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using GlobalVariables;
using Newtonsoft.Json.Linq;
using UnityEngine.UI;

public class DrawPolyLine : MonoBehaviour
{
    public static DrawPolyLine Instance { get; private set; }

    private List<Vector2> lines;
    private bool isDrawing;
    public bool isEditing, isDeleting;
    //private OnlineMapsDrawingLine aLine;
    private OnlineMapsDrawingLine aLine;
    private double lng, lat;
    private Vector2 pos;
    private DrawStat ds;
    private int n;
    protected int nPoly;
    private float dis;
    public List<OnlineMapsMarker> markers;
    Image clr;

    void Awake()
    {
        if (Instance == null)
        {
            isDrawing = false;
            isEditing = false;
            n = 0;
            nPoly = 0;
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        clr = DrawArrow.instance.outColor;
    }

    // Update is called once per frame
    void Update()
    {
        if (isDrawing)
        {
            if (Input.GetKey(KeyCode.Return))
            {
                confirmDraw();
            }
        }
    }

    public void cancelDraw()
    {
        isDrawing = false;
        isEditing = false;
        n = 0;
        nPoly = 0;
        OnlineMapsControlBase.instance.OnMapClick = null;
        isDrawing = true;
        DrawingManager.Instance.isDeleting = false;
        if (aLine != null) aLine.Dispose();
        if (lines != null) lines.Clear();
        GameObject.Find("Modal - Confirm Draw").GetComponent<Animator>().Play("FadeOut");
        clearMarkers();
    }

    public void performDraw()
    {
        lines = new List<Vector2>();
        OnlineMapsControlBase.instance.OnMapClick += addPolyLine;
        isDrawing = true;
        DrawingManager.Instance.isDeleting = false;
        GameObject.Find("Modal - Confirm Draw").GetComponent<Animator>().Play("FadeIn");
    }

    private void addPolyLine()
    {
        double lng, lat;
        OnlineMapsControlBase.instance.GetCoords(out lng, out lat);
        lines.Add(new Vector2((float)lng, (float)lat));
        if (n < 1)
        {
            var man = OnlineMapsDrawingElementManager.AddItem(new OnlineMapsDrawingLine(lines, clr.color, 0.25f));
            aLine = (OnlineMapsDrawingLine)man;
            pos = new Vector2((float)lng, (float)lat);
        }
        else
        {
            dis += OnlineMapsUtils.DistanceBetweenPoints(pos, new Vector2((float)lng, (float)lat)).magnitude;
            pos = new Vector2((float)lng, (float)lat);

        }
        addMarker(pos, aLine, n);
        n++;
    }

    public void addMarker(Vector2 pos, OnlineMapsDrawingLine ln, int index)
    {
        OnlineMapsMarker mm = OnlineMapsMarkerManager.CreateItem(pos.x, pos.y, OnlineMapsMarkerManager.instance.defaultTexture, "Titik Polyline ke-" + index);
        markers.Add(mm);
        mm["line"] = ln;
        mm["index"] = index;
        mm.OnPress += MarkerLongPress;
        mm.OnPositionChanged += changePosition;
    }

    public void confirmDraw()
    {
        if (isDrawing)
        {
            OnlineMaps.instance.Redraw();
            OnlineMapsControlBase.instance.OnMapClick = null;
            isDrawing = false;
            //Debug.Log("Long : " + lines[0].x + " | Lat :" + lines[0].y);
            //OnlineMaps.instance.position = new Vector2((float) lines[0].x, (float) lines[0].y);
            GameObject.Find("Modal - Confirm Draw").GetComponent<Animator>().Play("FadeOut");
            if (n > 0)
            {
                nPoly++;
                aLine.name = generateIDpolyline();
                GameObject.Find(aLine.name).tag = "draw-polyline";
                aLine.polyID = nPoly;
                DrawingManager.Instance.lines.Add(aLine);

                // Test Save Plotting
                List<float[]> c1 = new List<float[]>();
                //var x = (List<Vector2>)aPoly.points;
                //x.Count;
                foreach (var l in (List<Vector2>)aLine.points)
                {
                    float[] arr = new float[] { (float)l.x, (float)l.y };
                    c1.Add(arr);
                    //Debug.Log((float)l.x + " | " + (float)l.y);
                }

                //List<float[]> c2 = new List<float[]> { c1 };
                JsonPolyline d1 = new JsonPolyline
                {
                    type = "LineString",
                    coordinates = c1
                };

                string json = JsonConvert.SerializeObject(d1, Formatting.Indented);
                Debug.Log(json);

                aLine.OnClick += onLineClick;
                aLine.DrawOnTileset(OnlineMapsTileSetControl.instance, OnlineMapsDrawingElementManager.instance.Count - 1);
                ds = aLine.instance.AddComponent<DrawStat>();
                ds.color = (SessionUser.nama_asisten == "ASOPS") ? "blue" : "red";
                ds.polyName = aLine.name;
                ds.type = DrawStat.polyType.LineString;
                //ds.markers = markers;
                //markers.Clear();
                //ds.clearMarkers();
                ds.points = lines;
                ds.geometry = "{\"type\": \"LineString\", \"coordinates\": [" + ListVectorToText(ds.points.ToArray()) + "]}";
                ds.properties = JsonConvert.SerializeObject(new JObject{
                        {"color", ColorUtility.ToHtmlStringRGB(aLine.color)},
                        {"weight", aLine.width},
                        {"opacity", 1},
                        {"id_point", aLine.name},
                        {"dashArray", "0,0"} });
            }
        }
        else if (isEditing)
        {
            isEditing = false;
            GameObject.Find("Modal - Confirm Draw").GetComponent<Animator>().Play("FadeOut");
        }
        clearMarkers();
        n = 0;
        Debug.Log("Distance : " + dis);
        dis = 0;
        ds = null;
    }

    private string generateIDpolyline()
    {
        var index_polyline = GameObject.FindGameObjectsWithTag("draw-polyline").Length;
        bool isExist = true;

        do
        {
            isExist = checkPolylineIndex(index_polyline);
            index_polyline++;
        } while (isExist);

        return "polyline_" + index_polyline + "_" + SessionUser.id + "_" + Random.Range(0, 999);
    }

    private bool checkPolylineIndex(int index)
    {
        bool isExist = false;

        foreach (GameObject polyline in GameObject.FindGameObjectsWithTag("draw-polyline"))
        {
            if (polyline.name.Contains("polyline_" + index)) isExist = true;
        }

        return isExist;
    }

    public string ListVectorToText(Vector2[] list)
    {
        string result = "";
        foreach (var listMember in list)
        {
            if (listMember != list[list.Length - 1])
                result += "[" + listMember.x + "," + listMember.y + "]" + ", ";
            else
                result += "[" + listMember.x + "," + listMember.y + "]";
        }
        return result;
    }

    private void MarkerLongPress(OnlineMapsMarkerBase marker)
    {
        // Starts moving the marker.
        OnlineMapsControlBase.instance.dragMarker = marker;
        OnlineMapsControlBase.instance.isMapDrag = false;
    }

    private void changePosition(OnlineMapsMarkerBase marker)
    {
        var line = (OnlineMapsDrawingLine)marker["line"];
        var ls = (List<Vector2>)line.points;
        ls[(int)marker["index"]] = marker.position;

        if (line.instance.GetComponent<DrawStat>() != null)
        {
            DrawStat ds = line.instance.GetComponent<DrawStat>();
            ds.points[(int)marker["index"]] = marker.position;
            ds.geometry = "{\"type\": \"LineString\", \"coordinates\": [" + ListVectorToText(ds.points.ToArray()) + "]}";
        }
    }

    public void onLineClick(OnlineMapsDrawingElement element)
    {
        if (isDeleting)
        {
            EntityEditorController.Instance.plotTrash.Add(
                new JObject()
                {
                    { "id", element.name },
                    { "type", "polygon" }
                }
            );
            element.Dispose();
        }
        else if (isEditing)
        {
            spawnMarkers((OnlineMapsDrawingLine)element);
            GameObject.Find("Modal - Confirm Draw").GetComponent<Animator>().Play("FadeIn");
        }
    }

    public void clearMarkers()
    {
        foreach (OnlineMapsMarker m in markers)
        {
            OnlineMapsMarkerManagerBase<OnlineMapsMarkerManager, OnlineMapsMarker>.RemoveItem(m, true);
            m.Dispose();
        }
        markers.Clear();
    }

    private void spawnMarkers(OnlineMapsDrawingLine ln)
    {
        int i = 0;
        foreach (Vector2 pts in ln.points as List<Vector2>)
        {
            addMarker(new Vector2(pts.x, pts.y), ln, i);
            i++;
            n++;
        }
    }

    public void setEditing(bool y)
    {
        isEditing = y;
    }

    public void setDeleting(bool y)
    {
        isDeleting = y;
    }
    //private void deletePolyLine()
    //{
    //  OnlineMapsControlBase.instance.GetCoords(out lng, out lat);
    //  pos = new Vector2((float)lng, (float)lat);

    //  if (polyLines != null)
    //  {
    //    foreach (var lns in polyLines)
    //    {
    //      if (lns.HitTest(pos, OnlineMaps.instance.zoom))
    //      {
    //        lns.Dispose();
    //      }
    //    }
    //  }
    //}

    //public void startDeleteDraw()
    //{
    //  isDeleting = true;
    //  GameObject.Find("Modal - Confirm Delete").GetComponent<Animator>().Play("FadeIn");
    //  //GameObject.Find("Modal - Confirm").GetComponent<CanvasGroup>().alpha = 1f;
    //} 

    //public void stopDeleteDraw()
    //{
    //  isDeleting = false;
    //  GameObject.Find("Modal - Confirm Delete").GetComponent<Animator>().Play("FadeOut");
    //  //GameObject.Find("Modal - Confirm").GetComponent<CanvasGroup>().alpha = 0f;
    //}
}
