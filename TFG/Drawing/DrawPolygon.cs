using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using GlobalVariables;
using Newtonsoft.Json.Linq;
using UnityEngine.UI;

public class DrawPolygon : MonoBehaviour
{
    public static DrawPolygon Instance { get; private set; }

    private List<Vector2> lines;
    private bool isDrawing;
    public bool isEditing, isDeleting;
    //private OnlineMapsDrawingPoly aPoly;
    private OnlineMapsDrawingPoly aPoly;
    private double lng, lat;
    private Vector2 pos;
    private DrawStat ds;
    private int n;
    protected int nPoly;
    private float dis;
    public List<OnlineMapsMarker> markers;
    public Texture2D centerLogo;
    public Image clr;

    void Awake()
    {
        if (Instance == null)
        {
            isDrawing = false;
            isEditing = false;
            n = 0;
            nPoly = 0;
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        clr = DrawArrow.instance.outColor;
    }

    // Update is called once per frame
    void Update()
    {
        if (isDrawing)
        {
            if (Input.GetKey(KeyCode.Return))
            {
                confirmDraw();
            }
        }
    }

    public void cancelDraw()
    {
        isDrawing = false;
        isEditing = false;
        n = 0;
        nPoly = 0;
        OnlineMapsControlBase.instance.OnMapClick = null;
        isDrawing = true;
        DrawingManager.Instance.isDeleting = false;
        if (aPoly != null) aPoly.Dispose();
        if (lines != null) lines.Clear();
        GameObject.Find("Modal - Confirm Draw").GetComponent<Animator>().Play("FadeOut");
        clearMarkers();
    }
    public void performDraw()
    {
        lines = new List<Vector2>();
        OnlineMapsControlBase.instance.OnMapClick += addPolygon;
        isDrawing = true;
        DrawingManager.Instance.isDeleting = false;
        GameObject.Find("Modal - Confirm Draw").GetComponent<Animator>().Play("FadeIn");
    }

    private void addPolygon()
    {
        double lng, lat;
        OnlineMapsControlBase.instance.GetCoords(out lng, out lat);
        lines.Add(new Vector2((float)lng, (float)lat));
        if (n < 1)
        {
            var tcolor = (SessionUser.nama_asisten == "ASOPS") ? Color.blue : Color.red;
            //tcolor.a = 0.5f;
            var man = OnlineMapsDrawingElementManager.AddItem(new OnlineMapsDrawingPoly(lines, tcolor, 0.25f, tcolor));
            aPoly = (OnlineMapsDrawingPoly)man;
            pos = new Vector2((float)lng, (float)lat);
        }
        else
        {
            dis += OnlineMapsUtils.DistanceBetweenPoints(pos, new Vector2((float)lng, (float)lat)).magnitude;
            pos = new Vector2((float)lng, (float)lat);

        }
        addMarker(pos, aPoly, n);
        n++;
    }

    public void addMarker(Vector2 pos, OnlineMapsDrawingPoly ln, int index)
    {
        OnlineMapsMarker mm = OnlineMapsMarkerManager.CreateItem(pos.x, pos.y, OnlineMapsMarkerManager.instance.defaultTexture, "Titik Polyline ke-" + index);
        markers.Add(mm);
        mm["line"] = ln;
        mm["index"] = index;
        mm["range"] = (Vector2)ln.center - pos;
        mm.OnPress += MarkerLongPress;
        mm.OnPositionChanged += changePosition;
        mm.OnRelease += finishChangePostion;
    }

    public void confirmDraw()
    {
        if (isDrawing)
        {
            OnlineMaps.instance.Redraw();
            OnlineMapsControlBase.instance.OnMapClick = null;
            isDrawing = false;
            //Debug.Log("Long : " + lines[0].x + " | Lat :" + lines[0].y);
            //OnlineMaps.instance.position = new Vector2((float) lines[0].x, (float) lines[0].y);
            GameObject.Find("Modal - Confirm Draw").GetComponent<Animator>().Play("FadeOut");
            if (n > 0)
            {
                nPoly++;
                aPoly.name = generateIDpolygon();
                GameObject.Find(aPoly.name).tag = "draw-polygon";
                aPoly.polyID = nPoly;
                DrawingManager.Instance.polygons.Add(aPoly);

                aPoly.OnClick += onPolyClick;
                aPoly.DrawOnTileset(OnlineMapsTileSetControl.instance, OnlineMapsDrawingElementManager.instance.Count - 1);
                ds = aPoly.instance.AddComponent<DrawStat>();
                ds.color = (SessionUser.nama_asisten == "ASOPS") ? "blue" : "red";
                ds.polyName = aPoly.name;
                ds.type = DrawStat.polyType.Polygon;
                //ds.markers = markers;
                //markers.Clear();
                //ds.clearMarkers();
                ds.points = lines;
                ds.geometry = "{\"type\": \"Polygon\", \"coordinates\": [[" + ListVectorToText(ds.points) + "]]}";
                ds.properties = JsonConvert.SerializeObject(new JObject{
                    //{"color", ds.color},
                    //{"weight", aPoly.borderWidth},
                    //{"opacity", 1},
                    //{"id_point", aPoly.name},
                    //{"dashArray", "0,0"} });
                    { "weight", 5 },
                    { "dashArray", "0,0" },
                    { "color", ColorUtility.ToHtmlStringRGB(aPoly.borderColor) },
                    { "fillColor", ColorUtility.ToHtmlStringRGB(aPoly.backgroundColor) },
                    { "fillOpacity", aPoly.backgroundColor.a },
                    { "opacity", aPoly.borderColor.a },
                    { "id_point", aPoly.name },
                    { "transform", true },
                    { "draggable", true },
                    { "interactive", true },
                    { "className", "leaflet-path-draggable" } });
            }
        }
        else if (isEditing)
        {
            aPoly.instance.GetComponent<DrawStat>().properties = JsonConvert.SerializeObject(new JObject{
                    //{"color", ds.color},
                    //{"weight", aPoly.borderWidth},
                    //{"opacity", 1},
                    //{"id_point", aPoly.name},
                    //{"dashArray", "0,0"} });
                    { "weight", 5 },
                    { "dashArray", "0,0" },
                    { "color", ColorUtility.ToHtmlStringRGB(aPoly.borderColor) },
                    { "fillColor", ColorUtility.ToHtmlStringRGB(aPoly.backgroundColor) },
                    { "fillOpacity", aPoly.backgroundColor.a },
                    { "opacity", aPoly.borderColor.a },
                    { "id_point", aPoly.name },
                    { "transform", true },
                    { "draggable", true },
                    { "interactive", true },
                    { "className", "leaflet-path-draggable" } });
            isEditing = false;
            GameObject.Find("Modal - Confirm Draw").GetComponent<Animator>().Play("FadeOut");
        }
        clearMarkers();
        n = 0;
        Debug.Log("Distance : " + dis);
        dis = 0;
        ds = null;
        aPoly = null;
        // PlotObstacleController.instance.m++;
    }

    private string generateIDpolygon()
    {
        var index_polygon = PlotObstacleController.instance.m;

        return "polygon_" + index_polygon + "_" + SessionUser.id + "_" + Random.Range(0, 999);
    }

    private bool checkPolylineIndex(int index)
    {
        bool isExist = false;

        foreach (GameObject polygon in GameObject.FindGameObjectsWithTag("draw-polygon"))
        {
            if (polygon.name.Contains("polygon_" + index)) isExist = true;
        }

        return isExist;
    }

    public string ListVectorToText(List<Vector2> list)
    {
        string result = "";
        foreach (var listMember in list)
        {
            if (listMember != list[list.Count - 1])
                result += "[" + listMember.x + "," + listMember.y + "]" + ", ";
            else
                result += "[" + listMember.x + "," + listMember.y + "]";
        }
        return result;
    }

    private void MarkerLongPress(OnlineMapsMarkerBase marker)
    {
        // Starts moving the marker.
        OnlineMapsControlBase.instance.dragMarker = marker;
        OnlineMapsControlBase.instance.isMapDrag = false;
    }

    private void changePosition(OnlineMapsMarkerBase marker)
    {
        var line = (OnlineMapsDrawingPoly)marker["line"];
        var ls = (List<Vector2>)line.points;
        ls[(int)marker["index"]] = marker.position;

        if (line.instance.GetComponent<DrawStat>() != null)
        {
            DrawStat ds = line.instance.GetComponent<DrawStat>();
            ds.points[(int)marker["index"]] = marker.position;
            ds.geometry = "{\"type\": \"Polygon\", \"coordinates\": [[" + ListVectorToText(ds.points) + "]]}";
        }
    }

    private void finishChangePostion(OnlineMapsMarkerBase marker)
    {
        var line = (OnlineMapsDrawingPoly)marker["line"];
        var ls = (List<Vector2>)line.points;
        ls[(int)marker["index"]] = marker.position;
        marker["range"] = (Vector2)line.center - ls[(int)marker["index"]];
        markers[ls.Count].OnPositionChanged = null;
        markers[ls.Count].position = line.center;
        markers[ls.Count].OnPositionChanged += changePositionCenter;
    }

    private void changePositionCenter(OnlineMapsMarkerBase marker)
    {
        var line = (OnlineMapsDrawingPoly)marker["line"];
        var ls = (List<Vector2>)line.points;
        DrawStat ds = line.instance.GetComponent<DrawStat>();

        for (int i = 0; i < ls.Count; i++)
        {
            ls[i] = marker.position - (Vector2)markers[i]["range"];
            ds.points[i] = ls[i];
            markers[i].position = ls[i];
        }

        ds.geometry = "{\"type\": \"Polygon\", \"coordinates\": [[" + ListVectorToText(ds.points) + "]]}";
    }

    public void onPolyClick(OnlineMapsDrawingElement element)
    {
        if (isDeleting)
        {
            EntityEditorController.Instance.plotTrash.Add(
                new JObject()
                {
                    { "id", element.name },
                    { "type", "polygon" }
                }
            );
            element.Dispose();
        }

        else if (isEditing)
        {
            if (markers.Count < 1)
            {
                spawnMarkers((OnlineMapsDrawingPoly)element);
                aPoly = (OnlineMapsDrawingPoly)element;
                GameObject.Find("Modal - Confirm Draw").GetComponent<Animator>().Play("FadeIn");
            }
        }
    }

    public void clearMarkers()
    {
        foreach (OnlineMapsMarker m in markers)
        {
            OnlineMapsMarkerManagerBase<OnlineMapsMarkerManager, OnlineMapsMarker>.RemoveItem(m, true);
            m.Dispose();
        }
        markers.Clear();
    }

    private void spawnMarkers(OnlineMapsDrawingPoly ln)
    {
        int i = 0;
        foreach (Vector2 pts in ln.points as List<Vector2>)
        {
            addMarker(new Vector2(pts.x, pts.y), ln, i);
            i++;
            n++;
        }

        OnlineMapsMarker mm = OnlineMapsMarkerManager.CreateItem(ln.center.x, ln.center.y, centerLogo, "Drag to move");
        mm.scale = 0.333f;
        markers.Add(mm);
        mm["line"] = ln;
        mm.OnPress += MarkerLongPress;
        mm.OnPositionChanged += changePositionCenter;
    }

    public void setEditing(bool y)
    {
        isEditing = y;
    }

    public void setDeleting(bool y)
    {
        isDeleting = y;
    }

    public void changeEditedColor(FlexibleColorPicker cl)
    {
        if (aPoly != null)
        {
            aPoly.backgroundColor = cl.color;
            aPoly.borderColor = cl.color;
        }
    }
}
