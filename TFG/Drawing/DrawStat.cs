using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawStat : MonoBehaviour
{
    public enum polyType
    {
        None,
        LineString,
        Polygon,
        Square,
        Circle,
        Elipse,
        Arrow,
        Arrow_Curve_Top,
        Arrow_Curve_Bottom,
        Arrow_Maneuver_Top,
        Arrow_Maneuver_Bottom
    }
    public string userID;
    public string polyName;
    public polyType type;
    public string drawType;
    public string color;
    public string fillColor;
    public List<Vector2> points;
    public string geometry;
    public string properties;
}