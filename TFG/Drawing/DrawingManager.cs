using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawingManager : MonoBehaviour
{
    public static DrawingManager Instance { get; private set; }

    public List<OnlineMapsDrawingLine> lines;
    public List<OnlineMapsDrawingPoly> polygons;
    public bool isDeleting;
    private double lng, lat;
    private Vector2 pos;

    void Awake()
    {
        if (Instance != null && Instance != this) 
        { 
            Destroy(this); 
        } 
        else 
        { 
            Instance = this;
        }
    }

    private void Start()
    {
      this.lines = new List<OnlineMapsDrawingLine>();
      this.polygons = new List<OnlineMapsDrawingPoly>();
      this.isDeleting = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isDeleting)
        { 
          if (Input.GetMouseButton(0))
          {
            deleteDrawingsOnClick();
          }
        }
    }

    private void deleteDrawingsOnClick()
    {
      OnlineMapsControlBase.instance.GetCoords(out lng, out lat);
      pos = new Vector2((float)lng, (float)lat);

      deletePolyLines(pos);
      deletePolygons(pos);
    }

    // Deleting PolyLines
    private void deletePolyLines(Vector2 pos)
    {
      if (lines != null)
      {
        for (int i = 0; i < lines.Count; i++)
        {
          if (lines[i].HitTest(pos, OnlineMaps.instance.zoom))
          {
            lines[i].Dispose();
            //lines.Remove(lines[i]);
            lines.RemoveAt(i);
            i--;
          }
        }
      }
    }
    
    // Deleting Polygons
    private void deletePolygons(Vector2 pos)
    {
      if (polygons != null)
      {
        for (int i = 0; i < polygons.Count; i++)
        {
          if (polygons[i].HitTest(pos, OnlineMaps.instance.zoom))
          {
            polygons[i].Dispose();
            polygons.RemoveAt(i);
            i--;
          }
        }
      }
    }
    public void startDeleteDraw()
    {
      isDeleting = true;
      GameObject.Find("Modal - Confirm Delete").GetComponent<Animator>().Play("FadeIn");
      //GameObject.Find("Modal - Confirm").GetComponent<CanvasGroup>().alpha = 1f;
    } 

    public void stopDeleteDraw()
    {
      isDeleting = false;
      GameObject.Find("Modal - Confirm Delete").GetComponent<Animator>().Play("FadeOut");
      //GameObject.Find("Modal - Confirm").GetComponent<CanvasGroup>().alpha = 0f;
    }

    public void discardDrawings()
    {
      // Delete All Polylines
      if (lines != null)
      {
        for (int i = 0; i < lines.Count; i++)
        {
            lines[i].Dispose();
        }
        lines.Clear();
      }

      // Delete All Polygons
      if (polygons != null)
      {
        for (int i = 0; i < polygons.Count; i++)
        {
            polygons[i].Dispose();
        }
        polygons.Clear();
      }
    }
}
