using System;
using UnityEngine;

public class DetailAnimasi : MonoBehaviour
{
    [Header("References")]
    public OnlineMapsMarker3D marker;

    [Header("Info Video")]
    public string nama;
    public string urlImage;
    public string kategori;

    [Header("Info Timing")]
    public DateTime startTime;
    public DateTime endTime;
}
