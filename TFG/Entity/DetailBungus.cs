using System;
using UnityEngine;

public class DetailBungus : MonoBehaviour
{
    [Header("References")]
    public OnlineMapsMarker3D marker;

    [Header("Info Bungus")]
    public string nama;
    public DateTime date;
}
