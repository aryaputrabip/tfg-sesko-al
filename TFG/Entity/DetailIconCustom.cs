using System.Collections.Generic;
using UnityEngine;

public class DetailIconCustom : MonoBehaviour
{
    [Header("References")]
    public OnlineMapsMarker3D marker;

    [Header("Info Icon Custom")]
    public string nama;
    public string url;
    public int size;
    public List<string> info = new List<string>();
}
