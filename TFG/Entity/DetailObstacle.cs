using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetailObstacle : MonoBehaviour
{
    [Header("References")]
    public OnlineMapsMarker3D marker;

    [Header("Info Obstacle")]
    public string nama;
    public Color warna;
    public int size;

    [Header("Info Simbol Taktis")]
    public string fontTaktis;
    public Font fontFamily;
}
