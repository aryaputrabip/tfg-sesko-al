using System.Collections;
using System.Collections.Generic;
using HelperPlotting;
using UnityEngine;

public class DetailRadar : MonoBehaviour
{
    [Header("Instances")]
    [Space(5)]
    public GameObject instance;
    public GameObject radar3D;
    public OnlineMapsDrawingElement radar2D;
    
    [Header("TFG")]
    [Space(5)]
    public RadarEntity radar;
    public string json;

    [Header("WGS")]
    [Space(5)]
    public bool showRadar;
    public bool showRadar2D;
    public bool showRadar3D = true;
    bool _showRadar, _showRadar2D, _showRadar3D = true;
    public List<GameObject> enemiesInView = new();

    void Start()
    {
        if(instance == null) instance = gameObject;
        else Destroy(gameObject);

        radar3D = transform.Find("Sphere").gameObject;
    }

    void Update()
    {
        if(radar3D != null && radar2D != null)
        if(_showRadar == showRadar && (!_showRadar || !showRadar))
        {
            radar3D.SetActive(false);
            radar2D.visible = false;

            _showRadar = !showRadar;
        }
        else if(_showRadar == showRadar && (_showRadar || showRadar))
        {
            if(showRadar3D && !showRadar2D)radar3D.SetActive(true);
            else radar2D.visible = true;
            
            _showRadar = !showRadar;
        }
        
        if(radar3D != null && radar2D != null)
        {
            if(_showRadar2D == showRadar2D && (!_showRadar2D || !showRadar2D))
            {
                if(showRadar) radar2D.visible = false;
                _showRadar2D = !showRadar2D;
            }
            else if(_showRadar2D == showRadar2D && (_showRadar2D || showRadar2D))
            {
                if(showRadar) radar2D.visible = true;
                _showRadar2D = !showRadar2D;
            }
            
            if(_showRadar3D == showRadar3D && (!_showRadar3D || !showRadar3D))
            {
                if(showRadar) radar3D.SetActive(false);
                _showRadar3D = !showRadar3D;
            }
            else if(_showRadar3D == showRadar3D && (_showRadar3D || showRadar3D))
            {
                if(showRadar) radar3D.SetActive(true);
                _showRadar3D = !showRadar3D;
            }
        }
    }
}