using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetailSatuan : MonoBehaviour
{
    public enum TNI { AD, AL, AU }
    public enum jenis { Surface, Sub }
    public enum type { Darat, Laut, Udara }

    [Header("References")]
    public OnlineMapsMarker3D marker;

    [Header("Info Gameobject")]
    public GameObject OBJ;
    public Sprite icon;
    public string fontTaktis;
    public Font fontFamily;
    public string path_obj_3d;
    public string obj3D;

    [Header("Info Satuan")]
    public string id;
    public string nama;
    public TNI tipeTNI;
    public type tipeKendaraan;
    public jenis jenisKendaraan;
    public Color warna;
    public float skala;

    [Header("Info Posisi")]
    public Vector2 coordinate;
    public float kecepatan;
    public float heading;

    [Header("Info Misi")]
    public MetadataRoute misiBerjalan;
}
