using System;
using UnityEngine;

public class DetailSituasi : MonoBehaviour
{
    [Header("References")]
    public OnlineMapsMarker3D marker;

    [Header("Info Situasi")]
    public string nama;
    public Color warna;
    public string desc;
    public DateTime date;
    public int size;
}
