using System;
using UnityEngine;

public class DetailVideo : MonoBehaviour
{
    [Header("Info Video")]
    public string nama;
    public string judul;

    public DateTime startTime;
    public DateTime endTime;
}
