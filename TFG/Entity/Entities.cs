using System;
using SickscoreGames.HUDNavigationSystem;
using UnityEngine;

public class Entities : MonoBehaviour
{
    public enum opacityType { ADD, REMOVE }
    public enum entityType { Satuan, Radar }

    /// <summary>
    /// ID Entity
    /// </summary>
    public string id_entity;
    public string nama_entity;
    public string id_user;
    public entityType type;

    /// <summary>
    /// Heading / Rotation Entity
    /// </summary>
    public float heading;

    /// <summary>
    /// Opacity Entity
    /// </summary>
    public opacityType opacity;

    /// <summary>
    /// HUD / Simbol Taktis Entity
    /// </summary>
    public HUDNavigationElement HUDElement;

    private void Update()
    {
        if (PlaybackController.Instance.state == PlaybackController.playbackState.PLAYING ||
            PlaybackController.Instance.sliderDragged)
        {
            HUDElement.Prefabs.IndicatorPrefab.ChangeEntityInfo(nama_entity);
            //HUDElement.Prefabs.IndicatorPrefab.ChangeEntityInfo(nama_entity + "\n" + "Speed : " + SpeedConvertion.Instance.getSpeedName(SpeedConvertion.speedType.kmph, id_entity) + "\n" + "Heading : " + heading);
        }
    }
}
