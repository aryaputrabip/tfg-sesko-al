using System;
using UnityEngine;
using UnityEngine.Events;

public class EntityTimetrial : MonoBehaviour
{
    public GameObject obj;

    public DateTime startTime;
    public DateTime endTime;
    private PlaybackController _playback;

    public UnityEvent OnEnterTime;
    public UnityEvent OnExitTime;

    public string animation;

    private void Start()
    {
        _playback = FindObjectOfType<PlaybackController>();

        obj = gameObject.transform.GetChild(0).gameObject;
        //obj.SetActive(false);
    }

    void Update()
    {
        if (!_playback) Destroy(this);
        if (_playback.state != PlaybackController.playbackState.PLAYING && !_playback.sliderDragged) return;

        if (_playback._TIME > startTime && _playback._TIME < endTime && !obj.activeSelf)
        {
            OnEnterTime.Invoke();

            for(int i=0; i < obj.transform.parent.childCount; i++)
            {
                obj.transform.parent.GetChild(i).gameObject.SetActive(true);
            }

            //if(animation != null) obj.GetComponent<Animator>().Play(animation);
        }
        else if ((_playback._TIME < startTime || _playback._TIME > endTime) && obj.activeSelf)
        {
            OnExitTime.Invoke();

            for (int i = 0; i < obj.transform.parent.childCount; i++)
            {
                obj.transform.parent.GetChild(i).gameObject.SetActive(false);
            }

            //obj.SetActive(false);
        }
    }
}
