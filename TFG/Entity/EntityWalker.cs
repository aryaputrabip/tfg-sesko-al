using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//using HelperKecepatan;

public class EntityWalker : MonoBehaviour
{
    //public enum opacityType { ADD, REMOVE }

    [Header("Marker")]
    public OnlineMapsMarker3D marker;
    public GameObject obj;

    [Header("General Properties")]
    public Vector2 coordinate;
    //public float heading;
    //public opacityType opacity;

    [Header("Simulation Properties")]
    public float speed;
    public SpeedConvertion.speedType speedType;

    [Header("References")]
    public Entities entities;
    /// <summary>
    /// Skala Marker
    /// </summary>
    //public float scale;
    /// <summary>
    /// Geser viewport peta ke posisi marker
    /// </summary>
    public bool focusToMarker = false;
    /// <summary>
    /// Rotasikan marker di waypoint berikutnya
    /// </summary>
    public bool syncRotation = true;
    /// <summary>
    /// Camera Mengikuti Marker
    /// </summary>
    public bool followMarker = false;
    private float speedKM;

    [Header("Rute Misi")]
    public List<MetadataRoute> rute = new List<MetadataRoute>();
    public List<OnlineMapsDrawingLine> rutePath = new List<OnlineMapsDrawingLine>();

    private double distance;
    [SerializeField] private double progress;
    public int pointIndex = -1;
    public int ruteIndex = -1;
    private DateTime entityTiming;


    private Vector2 lastTargetPos;
    private Quaternion lastTargetRot;

    private PlaybackController _playback;
    public ColyseusController _colyseus;

    private void Start()
    {
        _playback = FindObjectOfType<PlaybackController>();
        _colyseus = FindObjectOfType<ColyseusController>();

        obj = marker.instance.transform.GetChild(0).gameObject;
    }

    public void InitRuteWalker()
    {
        _playback = FindObjectOfType<PlaybackController>();
        _colyseus = FindObjectOfType<ColyseusController>();

        if (!_playback) Destroy(this);
        if (rute == null) Destroy(this);
        if (rute.Count <= 0) Destroy(this);

        // Harus Memiliki Setidaknya 1 Rute, Jika Tidak maka Script WALKER (Pergerakan Obj) Akan Dihapus
        // - Object Tanpa Script WALKER akan berperan sebagai object statis
        if (rute[0].data.Count > 1)
        {
            entityTiming = _playback._TIME;

            pointIndex = 0;
            ruteIndex = 0;
            distance = OnlineMapsUtils.DistanceBetweenPointsD(rute[0].data[pointIndex].coordinate, rute[0].data[pointIndex + 1].coordinate);

            // Set Property SpeedInKM (Kecepatan yang akan digunakan untuk menentukan pergerakan)
            speedKM = SpeedConvertion.Instance.speedInKM(speed, speedType);
            /*var convert = new speedConversion();
            speedKM = convert.speedInKM(speed, speedType);*/
        }
    }

    #region MOVEMENT CONTROL

    private void Update()
    {
        // ========================================= READ ME =========================================
        // Created By : Aryaputra BIP
        // - This update section are used to simulate movement in Online Maps Based on playback time
        // - IF Anything goes wrong, you know where to ask
        // - Only edit this update function only if you know what you're doing
        // ===========================================================================================
        
        if (!_playback) Destroy(this);
        if(rute == null) Destroy(this);
        if (_playback.state != PlaybackController.playbackState.PLAYING && !_playback.sliderDragged) return;

        for (int i = rute.Count - 1; i >= 0; i--)
        {
            if (_playback._TIME >= DateTime.Parse(rute[i].data[0].time))
            {
                ruteIndex = i;
                i = 0;
            }
            else
            {
                ruteIndex = 0;
            }
        }
        
        bool isFound = false;
        if (ruteIndex == -1) return;

        for (int i = 0; i < rute[ruteIndex].data.Count - 1; i++)
        {
            DateTime ruteSelected = DateTime.Parse(rute[ruteIndex].data[i].time);
            DateTime ruteNext = DateTime.Parse(rute[ruteIndex].data[i + 1].time);

            if (_playback._TIME == ruteSelected)
            {
                pointIndex = i;
                i = rute[ruteIndex].data.Count;
                isFound = true;
            }else if (_playback._TIME > ruteSelected && _playback._TIME < ruteNext)
            {
                pointIndex = i;
                i = rute[ruteIndex].data.Count;
                isFound = true;
            }
        }

        if (!isFound)
        {
            if (_playback._TIME < DateTime.Parse(rute[ruteIndex].data[0].time))
            {
                pointIndex = 0;
                progress = 0;
            }
            else
            {
                pointIndex = rute[ruteIndex].data.Count - 2;
                progress = 1;
            }
        }
        
        var startDate = DateTime.Parse(rute[ruteIndex].data[pointIndex].time);
        var endDate = DateTime.Parse(rute[ruteIndex].data[pointIndex + 1].time);
        
        var total = (endDate - startDate).TotalSeconds;
        var percentage = (_playback._TIME - startDate).TotalSeconds * 1 / total;
        progress = percentage;
        
        Vector2 p1 = rute[ruteIndex].data[pointIndex].coordinate;
        Vector2 p2 = rute[ruteIndex].data[pointIndex + 1].coordinate;
        
        double dx, dy;
        OnlineMapsUtils.DistanceBetweenPoints(p1.x, p1.y, p2.x, p2.y, out dx, out dy);
        
        OnlineMapsVector2d position;
        position = OnlineMapsVector2d.Lerp(p1, p2, progress);
        marker.SetPosition(position.x, position.y);

        if (followMarker)
        {
            lastTargetPos = marker.position;
            OnlineMaps.instance.position = lastTargetPos;
        };

        if (rute[ruteIndex].routeType == MetadataRoute.type.Pergerakan && !obj.activeSelf)
        {
            // Eksekusi Pergerakan Biasa (IF OBJECT HIDE)
            for (int i=0; i < obj.transform.parent.childCount; i++)
            {
                obj.transform.parent.GetChild(i).gameObject.SetActive(true);
            }
        }else if(rute[ruteIndex].routeType == MetadataRoute.type.Embarkasi && obj.activeSelf && progress >= 1)
        {
            // Eksekusi Embarkasi (HIDE OBJECT ON END)
            for (int i = 0; i < obj.transform.parent.childCount; i++)
            {
                obj.transform.parent.GetChild(i).gameObject.SetActive(false);
            }
        }else if(rute[ruteIndex].routeType == MetadataRoute.type.Debarkasi && !obj.activeSelf && progress <= 0)
        {
            // Eksekusi Debarkasi (SHOW OBJECT AT START)
            for (int i = 0; i < obj.transform.parent.childCount; i++)
            {
                obj.transform.parent.GetChild(i).gameObject.SetActive(true);
            }
        }

        if (followMarker)
        {
            //if (transform.position != lastTargetPos)
            //{
            //    lastTargetPos = transform.position;

            //    // Size of map in scene
            //    Vector2 size = OnlineMapsTileSetControl.instance.sizeInScene;

            //    // Calculate offset (in tile position)
            //    Vector3 offset = OnlineMaps.instance.transform.rotation * (lastTargetPos - OnlineMaps.instance.transform.position - OnlineMapsTileSetControl.instance.center);
            //    offset.x = offset.x / OnlineMapsUtils.tileSize / size.x * OnlineMaps.instance.width * OnlineMaps.instance.zoomCoof;
            //    offset.z = offset.z / OnlineMapsUtils.tileSize / size.y * OnlineMaps.instance.height * OnlineMaps.instance.zoomCoof;

            //    // Calculate current tile position of the center of the map
            //    lastTileX -= offset.x;
            //    lastTileY += offset.z;

            //    // Set position of the map center
            //    if (Mathf.Abs(offset.x) > float.Epsilon || Mathf.Abs(offset.z) > float.Epsilon) OnlineMaps.instance.SetTilePosition(lastTileX, lastTileY);

            //    // Update map GameObject position
            //    OnlineMaps.instance.transform.position = lastTargetPos - OnlineMapsTileSetControl.instance.center;
            //}

            //OnlineMaps.instance.position = marker.position;
        }

        // Orient marker
        syncAngle(p1, p2);

        // Sinkronisasi Colyseus
        if (_colyseus)
        {
            _colyseus.SyncEntityState(this);
        }
    }

    private void syncAngle(Vector2 p1, Vector2 p2)
    {
        if (syncRotation)
        {
            lastTargetRot = Quaternion.Euler(Vector3.up * GetAngleOfLineBetweenTwoPoints(p1, p2));
            marker.rotation = Quaternion.Slerp(marker.rotation, lastTargetRot, 1.5f * Time.deltaTime);
            entities.heading = marker.rotationY;
        }
    }

    public float GetAngleOfLineBetweenTwoPoints(Vector2 p1, Vector2 p2)
    {
        // var data = Math.Atan2((p2.x - p1.x), (p2.y - p1.y)) * (180 / Math.PI);

        // Calculate the distance in km between locations.
        float distance = OnlineMapsUtils.DistanceBetweenPoints(p1, p2).magnitude;

        int zoom = 15;
        int maxX = 1 << (zoom - 1);

        // Calculate the tile position of locations.
        double userTileX, userTileY, markerTileX, markerTileY;
        OnlineMaps.instance.projection.CoordinatesToTile(p1.x, p1.y, zoom, out userTileX, out userTileY);
        OnlineMaps.instance.projection.CoordinatesToTile(p2.x, p2.y, zoom, out markerTileX, out markerTileY);

        // Calculate the angle between locations.
        double angle = OnlineMapsUtils.Angle2D(userTileX, userTileY, markerTileX, markerTileY) + 270;

        return (float)angle;
    }

    #endregion
}
