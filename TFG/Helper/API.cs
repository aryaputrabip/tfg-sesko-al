using System;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using GlobalVariables;
using HelperDataAlutsista;
using HelperSkenario;
using HelperUser;
using SkenarioAktif = GlobalVariables.SkenarioAktif;
using HelperEditor;

public class API : MonoBehaviour
{
    NetworkRequest network = new NetworkRequest();
    bool enableDebug = true;

    public static API Instance;
    
    void Awake()
    {
        // Set API Menjadi 1 Origin
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Login Aplikasi
    /// </summary>
    /// <param name="form">WWWForm</param>
    /// <returns></returns>
    public async Task<string> RequestLogin(WWWForm form, string server_file = null, int server = -99)
    {
        var result = await network.PostRequest(Config.SERVICE_LOGIN + "/api/login", form, enableDebug);

        if (result.data != null)
        {
            try
            {
                User[] user = User.FromJson(result.data);
                User userLogin = user[0];

                new SessionUser(userLogin);
                return "done";

            }
            catch (Exception e)
            {
                return "retry";
            }
        }

        return checkRequestResult(result.request);
    }

    /// <summary>
    /// Logout Aplikasi
    /// </summary>
    /// <param name="form">New Empty Form ( karena metode post, perlu form )</param>
    /// <returns></returns>
    public async Task RequestLogout()
    {
        var result = await network.PostRequest(Config.SERVICE_LOGIN + "/api/logout", null);
    }

    /// <summary>
    /// Dapatkan Skenario Aktif
    /// </summary>
    /// <returns></returns>
    public async Task<string> GetSkenarioAktif()
    {
        var result = await network.PostRequest(Config.SERVICE_LOGIN + "/api/getSkenarioAktif", null);

        Debug.Log(result.data);
        if (result.data != null)
        {
            try
            {
                var skenario = HelperSkenario.SkenarioAktif.FromJson(result.data);
                new SkenarioAktif(skenario);

                return "done";

            }
            catch (Exception e)
            {
                return "retry";
            }
        }

        return checkRequestResult(result.request);
    }
    
    public async Task<string> GetCBTerbaik(string id_bagian)
    {
        WWWForm form = new WWWForm();
        form.AddField("bagian", id_bagian);
        Debug.Log(id_bagian);

        //form.AddField("ID_KOGAS", id_bagian);
        Debug.Log("get CB Terbaik");

        var result = await network.PostRequest(Config.SERVICE_LOGIN + "/api/get/cb/getCB_Terbaik_all", form, true);

        if (result.data != null)
        {
            try
            {
                var cbTerbaik = HelperSkenario.CBTerbaik.FromJson(result.data);
                Debug.Log("GetCBTerbaik : " + cbTerbaik.ToString());
                
                new CBSendiri(cbTerbaik[0]);
                SkenarioAktif.ID_DOCUMENT = cbTerbaik[0].id_document.ToString();
                
                var cbtNow = cbTerbaik[0];
                SessionUser.id_kogas = cbtNow.id_kogas;

                if(cbTerbaik.Length > 1)
                foreach(CBTerbaik item in cbTerbaik)
                if(item.tipe_cb == "CB Musuh")
                {
                    new CBMusuh(item);
                    // Debug.Log(CBMusuh.id_kogas);
                    // SkenarioAktif.ID_DOCUMENT = cbTerbaik[1].id_document.ToString();
                }

                // if (cbTerbaik.Length > 1){
                //     new CBMusuh(cbTerbaik[1]);
                //     SkenarioAktif.ID_DOCUMENT = cbTerbaik[1].id_document.ToString();
                // }

                return "done";

            }
            catch (Exception e)
            {
                return "cb_terbaik_belum_dipilih";
            }
        }

        return checkRequestResult(result.request);
    }

    public async Task<string> GetDocument(bool loadMusuh = false, string id_user = null)
    {
        WWWForm form = new WWWForm();
        form.AddField("status", "ambil_dokumen");
        form.AddField("id_user", (id_user != null) ? id_user : SessionUser.id.ToString());
        form.AddField("dokumen", loadMusuh != true ? CBSendiri.nama_document : CBMusuh.nama_document);
        form.AddField("id_skenario", SkenarioAktif.ID_SKENARIO.ToString());
        form.AddField("type", SessionUser.id_kogas == 0 ? "menu" : "menu_cb");

        var result = await network.PostRequest(Config.SERVICE_CB + "/docs/source/source_get.php", form, enableDebug);
        if (result.data != null)
        {
            Debug.Log(result.data);
            var docs = HelperSkenario.DocumentActive.FromJson(result.data.Replace("[", "").Replace("]", ""));
            SkenarioAktif.ID_DOCUMENT = docs.id;
        }

        return null;
    }

    /// <summary>
    /// Dapatkan Hari-H Skenario Aktif
    /// </summary>
    /// <returns></returns>
    public async Task<string> GetHariH()
    {
        var result = await network.PostRequest(Config.SERVICE_LOGIN + "/api/getHariHSkenario", null);

        if (result.data != null)
        {
            Debug.Log(result.data);
            try
            {
                SkenarioAktif.HARI_H = DateTime.Parse(result.data.Replace("\"", ""));
                //DateTime.SpecifyKind(SkenarioAktif.HARI_H, DateTimeKind.Utc);
                return "done";

            }
            catch (Exception e)
            {
                Debug.Log(e.Message.ToString());
                return "retry";
            }
        }

        return checkRequestResult(result.request);
    }

    /// <summary>
    /// Dapatkan Data Kegiatan / Timetable
    /// </summary>
    /// <param name="skenario">ID Skenario Aktif</param>
    /// <returns></returns>
    public async Task<string> GetKegiatan(string skenario = null)
    {
        WWWForm form = new WWWForm();

        form.AddField("status", "ambil_kegiatan_tfg");
        form.AddField("id_scenario", (skenario != null) ? skenario : SkenarioAktif.ID_SKENARIO.ToString());
        Debug.Log("Get Data Kegiatan");

        var result = await network.PostRequest(Config.SERVICE_CB + "/docs/source/source_get.php", form, enableDebug);

        if (result.data != null) return result.data;

        return checkRequestResult(result.request);
    }

    /// <summary>
    /// Load Data Plotting CB Terbaik KOGAS Aktif
    /// </summary>
    /// <param name="loadMusuh">Load CB Sendiri atau CB Musuh ?</param>
    /// <returns></returns>
    public async Task<JArray> LoadDataCB(bool loadMusuh = false, string id_user = null, long? id_kogas = null, string? nama_document = null)
    {
        WWWForm form = new WWWForm();
        form.AddField("status", "ambil_load_dokumen");
        form.AddField("id_user", (id_user != null) ? id_user : SessionUser.id.ToString());
        form.AddField("nama_dokumen", nama_document);
        form.AddField("skenario", SkenarioAktif.ID_SKENARIO.ToString());
        form.AddField("type", id_kogas == 0 ? "menu" : "menu_cb");

        Debug.Log(form.ToString());
        Debug.Log(id_user);
        Debug.Log(nama_document);
        Debug.Log(SkenarioAktif.ID_SKENARIO.ToString());
        Debug.Log(id_kogas);
        Debug.Log(id_kogas == 0 ? "menu" : "menu_cb");

        var result = await network.PostRequest(Config.SERVICE_CB + "/docs/source/source_get.php", form, enableDebug);
        if (result.data == null) return null;

        return JArray.Parse(result.data);
    }

    public async Task<string> GetListAlutsista(string tipe_obj)
    {
        WWWForm form = new WWWForm();
        form.AddField("pasukan_check", "f");
        form.AddField("status", "ambil_object_" + tipe_obj);
        form.AddField("dokumen", SkenarioAktif.ID_SKENARIO.ToString());

        var result = await network.PostRequest(Config.SERVICE_CB + "/docs/source/source_get.php", form, true);
        if (result.data == null) return null;

        //var data = ListSatuan.FromJson(result.data);
        /*for(int i=0; i < data.Length; i++)
        {
            var detail = await GetDetailSatuan(data[i].ID, (tipe_obj == "darat") ? "1" : (tipe_obj == "laut") ? "2" : (tipe_obj == "udara") ? "3" : "1");
            
            if(tipe_obj == "darat")
            {
                var detailDarat = DetailSatuanDarat.FromJson(detail);
                data[i].path_object_3d = detailDarat.path_object_3d;
            }
            else if(tipe_obj == "laut")
            {
                var detailLaut = DetailSatuanDarat.FromJson(detail);
                data[i].path_object_3d = detailLaut.path_object_3d;
            }
            else if(tipe_obj == "udara")
            {
                var detailUdara = DetailSatuanDarat.FromJson(detail);
                data[i].path_object_3d = detailUdara.path_object_3d;
            }
        }*/

        return result.data;
    }

    //public async Task<ListPasukan[]> GetListPasukan(string tipe_obj)
    //{
    //    WWWForm form = new WWWForm();   
    //    form.AddField("pasukan_check", "f");
    //    form.AddField("status", "ambil_object_" + tipe_obj);
    //    form.AddField("dokumen", SkenarioAktif.ID_SKENARIO.ToString());

    //    var result = await network.PostRequest(Config.SERVICE_CB + "/docs/source/source_get.php", form, enableDebug);
    //    if (result.data == null) return null;

    //    return ListPasukan.FromJson(result.data);
    //}

    public async Task<JArray> GetListObstacle()
    {
        return null;
    }

    public async Task<string> GetDetailSatuan(string ID, string jenis)
    {
        var result = await network.GetRequest(Config.SERVICE_LOGIN + "/api/" + DetailSatuanGroup(jenis) + "?id=" + ID);

        if (result.data == null) return null;


        return result.data;
    }

    public async Task<Texture> GetRemoteTexture(string url)
    {
        var result = await network.GetRemoteTexture(Config.SERVICE_CB + "/docs/source/" + url);

        if (result == null) return null;


        return result;
    }

    private string DetailSatuanGroup(string jenis)
    {
        switch (jenis)
        {
            case "1":
                return "getDetailVehicle";
                break;
            case "2":
                return "getDetailShip";
                break;
            case "3":
                return "getDetailAircraft";
                break;
            default: return null;
        }
    }

    private string checkRequestResult(UnityWebRequest.Result result)
    {
        if (result == UnityEngine.Networking.UnityWebRequest.Result.ConnectionError)
        {
            return "connection_failed";
        }
        else if (result == UnityEngine.Networking.UnityWebRequest.Result.ProtocolError)
        {
            return "user_failed";
        }
        else
        {
            return "processing_failed";
        }
    }

    public async Task<string> simpanDokumen(string jsonObstacle = null, string jsonSatuan = null, string jsonBungus = null, string jsonLogistik = null,
        string jsonSituasi = null, string jsonRadar = null, string jsonTool = null, string jsonText = null, string jsonMisi = null, string jsonFormasi = null,
        string jsonKek = null, string jsonTrash = null)
    {
        WWWForm form = new WWWForm();
        form.AddField("status", "simpan");
        form.AddField("document[]", (int) SessionUser.id);
        form.AddField("document[]", CBSendiri.nama_document);
        form.AddField("document[]", "menu_cb");
        form.AddField("document[]", (int) SkenarioAktif.ID_SKENARIO);
        if (jsonObstacle != null) form.AddField("obstacle", jsonObstacle);
        if (jsonSatuan != null) form.AddField("satuan", jsonSatuan);
        if (jsonBungus != null) form.AddField("bungus", jsonBungus);
        if (jsonLogistik != null) form.AddField("logistik", jsonLogistik);
        if (jsonSituasi != null) form.AddField("situasi", jsonSituasi);
        if (jsonRadar != null) form.AddField("radar", jsonRadar);
        if (jsonTool != null) form.AddField("tool", jsonTool);
        if (jsonText != null) form.AddField("text", jsonText);
        if (jsonMisi != null) form.AddField("mission", jsonMisi);
        if (jsonFormasi != null) form.AddField("formasi", jsonFormasi);
        if (jsonKek != null) form.AddField("kekuatan", jsonKek);
        if (jsonTrash != null) form.AddField("trash", jsonTrash);

        var result = await network.PostRequest("http://localhost/eppkm_simulasi/docs/source/source_post_unity.php", form, enableDebug);
        if (result.data == null) return null;

        return result.data;
    }

    public async Task<string> GetListLayers()
    {
        WWWForm form = new WWWForm();
        form.AddField("status", "ambil_layers");

        var result = await network.PostRequest(Config.SERVICE_CB + "/docs/source/source_get.php", form, true);
        if (result.data == null) return null;

        return result.data;
    }

    public async Task<string> GetListBasemap()
    {
        WWWForm form = new WWWForm();
        form.AddField("status", "ambil_basemap");

        var result = await network.PostRequest(Config.SERVICE_CB + "/docs/source/source_get.php", form, true);
        if (result.data == null) return null;

        return result.data;
    }

    public async Task<string> GetDefaultBasemap()
    {
        WWWForm form = new WWWForm();
        form.AddField("status", "basemap_default");

        var result = await network.PostRequest(Config.SERVICE_CB + "/docs/source/source_get.php", form, true);
        if (result.data == null) return null;

        return result.data;
    }
}
