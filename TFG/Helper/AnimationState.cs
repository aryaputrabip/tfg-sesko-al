using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimationState : MonoBehaviour
{
    public UnityEvent onAnimationStart;
    public UnityEvent onAnimationFinish;
    
    public void RunWhenAnimationEnd(){
        onAnimationFinish.Invoke();
    }
    
    public void RunWhenAnimationStart(){
        onAnimationStart.Invoke();
    }
}
