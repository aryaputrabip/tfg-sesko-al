using System;
using UnityEngine;
using UnityEngine.Events;

using TMPro;

public class DateInputHelper : MonoBehaviour
{
    public DateTime date;

    [Header("Input Data")]
    public TMP_InputField inputDay;
    public TMP_InputField inputMonth;
    public TMP_InputField inputYear;
    public TMP_InputField inputHour;
    public TMP_InputField inputMinute;
    public TMP_InputField inputSecond;

    [Header("Label Time")]
    public TextMeshProUGUI labeldate;
    public TextMeshProUGUI labelTime;

    [Header("Events")]
    public UnityEvent onTimeChange;

    private void Start()
    {
        date = DateTime.Now;
        refreshUI();
    }

    public void increaseTime(string type)
    {
        switch (type)
        {
            case "day":
                date = date.AddDays(1);
                break;
            case "month":
                date = date.AddMonths(1);
                break;
            case "year":
                date = date.AddYears(1);
                break;
            case "hour":
                date = date.AddHours(1);
                break;
            case "minute":
                date = date.AddMinutes(1);
                break;
            case "second":
                date = date.AddSeconds(1);
                break;
            default: break;
        }

        refreshUI();
    }

    public void decreaseTime(string type)
    {
        switch (type)
        {
            case "day":
                date = date.AddDays(-1);
                break;
            case "month":
                date = date.AddMonths(-1);
                break;
            case "year":
                date = date.AddYears(-1);
                break;
            case "hour":
                date = date.AddHours(-1);
                break;
            case "minute":
                date = date.AddMinutes(-1);
                break;
            case "second":
                date = date.AddSeconds(-1);
                break;
            default: break;
        }

        refreshUI();
    }

    public void resetTime()
    {
        date = DateTime.Now;

        refreshUI();
    }

    public void refreshUI()
    {
        inputDay.text = date.ToString("dd");
        inputMonth.text = date.ToString("MM");
        inputYear.text = date.ToString("yyyy");
        inputHour.text = date.ToString("HH");
        inputMinute.text = date.ToString("mm");
        inputSecond.text = date.ToString("ss");

        labeldate.text = date.ToString("dddd, dd M yyyy");
        labelTime.text = date.ToString("(HH:mm:ss)");

        onTimeChange.Invoke();
    }
}
