using UnityEngine;

using System;
using System.Collections.Generic;
using System.Globalization;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace HelperConfig
{
    public partial class Config
    {
        public int? active_index { get; set; }
        public string server_name { get; set; }
        public string service_login { get; set; }
        public string service_cb { get; set; }
        public string server_colyseus { get; set; }
        public int? aircraft_alt { get; set; }
        public int? ship_scale { get; set; }
        public int? vehicle_scale { get; set; }
        public int? aircraft_scale { get; set; }
    }
}

namespace HelperUser
{
    public partial class User
    {
        public string username { get; set; }
        public long? id { get; set; }
        public string name { get; set; }
        public JenisUser jenis_user { get; set; }
        public Bagian bagian { get; set; }
        public Jabatan jabatan { get; set; }
        public Asisten asisten { get; set; }
        public long? atasan { get; set; }
        public long? status_login { get; set; }

        public static User[] FromJson(string json) => JsonConvert.DeserializeObject<User[]>(json, HelperConverter.Converter.Settings);
    }

    public partial class JenisUser
    {
        public long ID { get; set; }
        public string jenis_user { get; set; }
    }

    public partial class Bagian
    {
        public long ID { get; set; }
        public string nama_bagian { get; set; }
    }

    public partial class Jabatan
    {
        public long ID { get; set; }
        public string nama_jabatan { get; set; }
    }

    public partial class Asisten
    {
        public long ID { get; set; }
        public string nama_asisten { get; set; }
    }
}

namespace HelperSkenario
{
    public partial class SkenarioAktif
    {
        public long? ID { get; set; }
        public string nama_skenario { get; set; }

        public static SkenarioAktif FromJson(string json) => JsonConvert.DeserializeObject<SkenarioAktif>(json, HelperConverter.Converter.Settings);
    }

    public partial class CBTerbaik
    {
        public long? id_cb { get; set; }
        public long? id_cb_eppkm { get; set; }
        public long? id_kogas { get; set; }
        public long? id_user { get; set; }
        public string nama_kogas { get; set; }
        public string tipe_cb { get; set; }
        public int? id_document { get; set; }
        public string nama_document { get; set; }
        public string hari { get; set; }

        public static CBTerbaik[] FromJson(string json) => JsonConvert.DeserializeObject<CBTerbaik[]>(json, HelperConverter.Converter.Settings);
    }

    public partial class DocumentActive
    {
        public string id { get; set; }
        public static DocumentActive FromJson(string json) => JsonConvert.DeserializeObject<DocumentActive>(json, HelperConverter.Converter.Settings);
    }
}

namespace HelperKegiatan
{
    public partial class TimetableKegiatan
    {
        public string id_kegiatan { get; set; }
        public string id_satuan { get; set; }
        public string id_user { get; set; }
        public string kegiatan { get; set; }
        public string keterangan { get; set; }
        //public DateTime waktu { get; set; }
        public string waktu { get; set; }
        public DateTime waktuDT { get; set; }
        //public DateTime waktu { get; set; }   
        public string x { get; set; }
        public string y { get; set; }
        public string kogas { get; set; }
        public string waypoint { get; set; }
        public string detail { get; set; }
        public string nama_object { get; set; }
        public string type_kegiatan { get; set; }
        public string url_video { get; set; }
        public string percepatan { get; set; }

        public static TimetableKegiatan[] FromJson(string json) => JsonConvert.DeserializeObject<TimetableKegiatan[]>(json, HelperConverter.Converter.Settings);
    }
}

namespace HelperPlotting
{
    #region TRASH

    [Serializable]
    public class Trash
    {
        [JsonIgnore] [SerializeField] string _id;
        [JsonIgnore] [SerializeField] string _type;
        public string id { get{ return _id; } set{ _id = value; } }
        public string type { get{ return _type; } set{ _type = value; } }

        public static string ToString(List<Trash> json) => JsonConvert.SerializeObject(json, Formatting.Indented);
        public static List<Trash> FromJson(string json) => JsonConvert.DeserializeObject<List<Trash>>(json, HelperConverter.Converter.Settings);
    }

    #endregion

    #region  ENTITY_SATUAN
    public partial class EntitySatuan
    {
        [JsonProperty("id")]
        public string id_entity { get; set; }
        public string id_user { get; set; }
        public string id_symbol { get; set; }
        public string dokumen { get; set; }
        public string nama { get; set; }
        [JsonProperty("lat_y")]
        public float lat { get; set; }
        [JsonProperty("lng_x")]
        public float lng { get; set; }
        public string id_kegiatan { get; set; }
        public string isi_logistik { get; set; }
        public string symbol { get; set; }

        public string tipe_tni { get; set; }
        public string path_object_3d { get; set; }
        public string object3D { get; set; }
        public string jenis { get; set; }

        public string style { get; set; }
        public string info { get; set; }
        public string alutsista { get; set; }

        // Section Khusus EntityTFG
        /// <summary>
        /// Type (hasil parse data dari EntityTFG)
        /// </summary>
        public string type { get; set; }
        private float lat_entityTFG { set { lat = value; } }
        [JsonProperty("lng")]
        private float lng_entityTFG { set { lng = value; } }
        /// <summary>
        /// Heading (hasil parse data dari EntityTFG)
        /// </summary>
        public float heading { get; set; }
        /// <summary>
        /// Opacity (hasil parse data dari EntityTFG)
        /// </summary>
        public float opacity  { get; set; }
        /// <summary>
        /// Kode ASCII / Font Taktis (hasil parse data dari EntityTFG)
        /// </summary>
        public float kode_ascii { get; set; }
        /// <summary>
        /// Name Family / File Font Taktis Digunakan (hasil parse data dari EntityTFG)
        /// </summary>
        public float name_family { get; set; }
        [JsonProperty("namaAlutsista")]
        private string nama_entityTFG { set { nama = value; } }
        /// <summary>
        /// Default Data (hasil parse data dari EntityTFG)
        /// </summary>
        public float defaultData { get; set; }


        public static string ToString(EntitySatuan json) => JsonConvert.SerializeObject(json);
        public static EntitySatuan FromJson(string json) => JsonConvert.DeserializeObject<EntitySatuan>(json, HelperConverter.Converter.Settings);
    }

    public partial class EntitySatuanStyle
    {
        public string nama { get; set; }
        public int index { get; set; }
        public string grup { get; set; }
        public string label_style { get; set; }
        public string keterangan { get; set; }

        public static EntitySatuanStyle FromJson(string json) => JsonConvert.DeserializeObject<EntitySatuanStyle>(json, HelperConverter.Converter.Settings);
    }

    public partial class EntitySatuanInfo
    {
        public string kecepatan { get; set; }
        public string nomer_satuan { get; set; }
        public string nama_satuan { get; set; }
        public string nomer_atasan { get; set; }
        public string warna { get; set; }
        public string bahan_bakar { get; set; }
        public string kecepatan_maks { get; set; }
        public float heading { get; set; }
        public string ket_satuan { get; set; }
        public bool tfg { get; set; }
        public bool hidewgs { get; set; }
        public string kedalaman { get; set; }

        public static EntitySatuanInfo FromJson(string json) => JsonConvert.DeserializeObject<EntitySatuanInfo>(json, HelperConverter.Converter.Settings);
    }
    #endregion

    #region MISI_ENTITY_SATUAN

    [Serializable]
    public class EntityMisi
    {
        [JsonIgnore] [SerializeField] string _id;
        [JsonIgnore] [SerializeField] string _id_mission;
        [JsonIgnore] [SerializeField] string _id_object;
        [JsonIgnore] [SerializeField] string _tgl_mulai;
        [JsonIgnore] [SerializeField] string _jenis;
        [JsonIgnore] [SerializeField] EntityMisiProperties _properties;
        public string id { get{ return _id; } set{ _id = value; } }
        public string id_mission { get{ return _id_mission; } set{ _id_mission = value; } }
        public string id_object { get{ return _id_object; } set{ _id_object = value; } }
        public string tgl_mulai { get{ return _tgl_mulai; } set{ _tgl_mulai = value; } }
        public string jenis { get{ return _jenis; } set{ _jenis = value; } }
        public EntityMisiProperties properties { get{ return _properties; } set{ _properties = value; } }

        [JsonConstructor]
        public EntityMisi(string properties)
        {
            this.properties = EntityMisiProperties.FromJson(properties);
        }

        public static EntityMisi FromJson(string json) => JsonConvert.DeserializeObject<EntityMisi>(json, HelperConverter.Converter.Settings);
    }

    [Serializable]
    public class EntityMisiProperties
    {
        [JsonIgnore] [SerializeField] string _nama_misi;
        [JsonIgnore] [SerializeField] List<JalurMisi> _jalur;
        [JsonIgnore] [SerializeField] string _kecepatan;
        [JsonIgnore] [SerializeField] string _type;
        [JsonIgnore] [SerializeField] double _heading;
        [JsonIgnore] [SerializeField] double _distance;
        [JsonIgnore] [SerializeField] string _typeMisi;
        [JsonIgnore] [SerializeField] string _id_kegiatan;
        [JsonIgnore] [SerializeField] string _idPrimary;
        public string nama_misi { get{ return _nama_misi; } set{ _nama_misi = value; } }
        public List<JalurMisi> jalur { get{ return _jalur; } set{ _jalur = value; } }
        public string kecepatan { get{ return _kecepatan; } set{ _kecepatan = value; } }
        public string type { get{ return _type; } set{ _type = value; } }
        public double heading { get{ return _heading; } set{ _heading = value; } }
        public double distance { get{ return _distance; } set{ _distance = value; } }
        public string typeMisi { get{ return _typeMisi; } set{ _typeMisi = value; } }
        public string id_kegiatan { get{ return _id_kegiatan; } set{ _id_kegiatan = value; } }
        public string idPrimary { get{ return _idPrimary; } set{ _idPrimary = value; } }

        public static EntityMisiProperties FromJson(string json) => JsonConvert.DeserializeObject<EntityMisiProperties>(json, HelperConverter.Converter.Settings);
    }

    [Serializable]
    public class JalurMisi
    {
        [JsonIgnore] [SerializeField] float _lat;
        [JsonIgnore] [SerializeField] float _lng;
        [JsonIgnore] [SerializeField] int _alt;
        public float lat { get { return _lat; } set{ _lat = value; } }
        public float lng { get { return _lng; } set{ _lng = value; } }
        public int? alt { get { return _alt; } set{ _alt = value ?? 0; } }
    }
    #endregion

    #region ENTITY_FORMASI
    public partial class EntityFormasi
    {
        public int? id { get; set; }
        public string id_user { get; set; }
        public string dokumen { get; set; }
        public string nama { get; set; }
        public string info_formasi { get; set; }
        public string tgl_formasi { get; set; }
        public float size { get; set; }
        public string jenis_formasi { get; set; }
        public string warna { get; set; }
        
        public static string ToString(EntityFormasi json) => JsonConvert.SerializeObject(json);
        public static EntityFormasi FromJson(string json) => JsonConvert.DeserializeObject<EntityFormasi>(json, HelperConverter.Converter.Settings);
    }

    #region IZZAN_EDIT_FORMASI
    public class Icon_Formasi
    {
        public string nama { get; set; }
        public string index { get; set; }
        public string keterangan { get; set; }
        public int grup { get; set; }
        public string label_style { get; set; }
        public static Icon_Formasi FromJson(string json) => JsonConvert.DeserializeObject<Icon_Formasi>(json, HelperConverter.Converter.Settings);
    }

    public class Info_Formasi
    {
        public string nama_formasi { get; set; }
        public string warna { get; set; }
        public int size { get; set; }
        public string id_point { get; set; }
        public float arah { get; set; }
        [JsonProperty("arrArah")]
        public object RAWarrArah { get; set; }
        [JsonProperty("arrArah2")]
        public List<float> arrArah { get; set; }
        //public List<float> arrArah { get; set; }
        public List<Satuan_Formasi> satuan_formasi { get; set; }

        public static string ToString(Info_Formasi json) => JsonConvert.SerializeObject(json);
        public static Info_Formasi FromJson(string json) => JsonConvert.DeserializeObject<Info_Formasi>(json, HelperConverter.Converter.Settings);
    }

    public class Satuan_Formasi
    {
        public string id_point { get; set; }
        public string nama { get; set; }
        public Icon_Formasi icon { get; set; }
        public string color { get; set; }
        public double lat { get; set; }
        public double lng { get; set; }
        public int inti { get; set; }
        public int id { get; set; }
        public float jarak { get; set; }
        public float sudut { get; set; }
        public string satuan { get; set; }
        public double? new_jarak { get; set; }

        public static string ToString(List<Satuan_Formasi> json) => JsonConvert.SerializeObject(json);
        public static List<Satuan_Formasi> FromJson(string json) => JsonConvert.DeserializeObject<List<Satuan_Formasi>>(json, HelperConverter.Converter.Settings);
    }
    #endregion

    public partial class EntityFormasiInfo
    {
        public string id_point { get; set; }
        public string nama_formasi { get; set; }
        public string warna { get; set; }
        public string arah { get; set; }
        public string satuan_formasi { get; set; }

        public static EntityFormasiInfo FromJson(string json) => JsonConvert.DeserializeObject<EntityFormasiInfo>(json, HelperConverter.Converter.Settings);
    }

    public partial class EntityFormasiSatuan
    {
        public string id_point { get; set; }
        public string nama { get; set; }
        public string color { get; set; }
        public float? lat { get; set; }
        public float? lng { get; set; }
        public int? inti { get; set; }

        public static EntityFormasiSatuan FromJson(string json) => JsonConvert.DeserializeObject<EntityFormasiSatuan>(json, HelperConverter.Converter.Settings);
    }
    #endregion

    #region ENTITY_SITUASI
    public partial class EntitySituasi
    {
        public string id_situasi { get; set; }
        public string id_user { get; set; }
        public string dokumen { get; set; }
        public string nama { get; set; }
        [JsonProperty("lat_y")]
        public float lat { get; set; }
        [JsonProperty("lng_x")]
        public float lng { get; set; }
        public string info_situasi { get; set; }
        public string symbol_situasi { get; set; }

        public static string ToString(EntitySituasi json) => JsonConvert.SerializeObject(json);
        public static EntitySituasi FromJson(string json) => JsonConvert.DeserializeObject<EntitySituasi>(json, HelperConverter.Converter.Settings);
    }

    public partial class EntitySituasiInfo
    {
        public string isi_situasi { get; set; }
        public string tgl_situasi { get; set; }
        public string waktu_situasi { get; set; }
        public int size { get; set; }
        public string warna { get; set; }
        public string property { get; set; }

        public static EntitySituasiInfo FromJson(string json) => JsonConvert.DeserializeObject<EntitySituasiInfo>(json, HelperConverter.Converter.Settings);
    }
    #endregion

    #region ENTITY_RADAR
    public partial class EntityRadar
    {
        public string id { get; set; }
        public string id_user { get; set; }
        public string dokumen { get; set; }
        public string nama { get; set; }
        [JsonProperty("lat_y")] public float lat { get; set; }
        [JsonProperty("lng_x")] public float lng { get; set; }
        public string info_radar { get; set; }
        public string symbol { get; set; }
        public string info_symbol { get; set; }

        public static string ToString(EntityRadar json) => JsonConvert.SerializeObject(json);
        public static EntityRadar FromJson(string json) => JsonConvert.DeserializeObject<EntityRadar>(json, HelperConverter.Converter.Settings);
    }

    public partial class EntityRadarInfo
    {
        public string nama { get; set; }
        public string judul { get; set; }
        public float radius { get; set; }
        public int size { get; set; }
        public string warna { get; set; }
        public EntityRadarInfoSymbol id_symbol { get; set; }
        public string jenis_radar { get; set; }

        public static EntityRadarInfo FromJson(string json) => JsonConvert.DeserializeObject<EntityRadarInfo>(json, HelperConverter.Converter.Settings);
    }

    public partial class EntityRadarInfoSymbol
    {
        public string id { get; set; }
        public string nama { get; set; }
        public string index { get; set; }
        public string keterangan { get; set; }
        public string grup { get; set; }
    }
    #endregion

    #region ENTITY_TOOLS
    public partial class EntityTools
    {
        public string id { get; set; }
        public string id_user { get; set; }
        public string dokumen { get; set; }
        public string nama { get; set; }
        public string geometry { get; set; }
        public string properties { get; set; }
        public string type { get; set; }

        public static string ToString(EntityTools json) => JsonConvert.SerializeObject(json);
        public static EntityTools FromJson(string json) => JsonConvert.DeserializeObject<EntityTools>(json, HelperConverter.Converter.Settings);
    }

    public partial class EntityToolsGeometryLine
    {
        public string type { get; set; }
        public List<List<double>> coordinates { get; set; }

        public static EntityToolsGeometryLine FromJson(string json) => JsonConvert.DeserializeObject<EntityToolsGeometryLine>(json, HelperConverter.Converter.Settings);
    }

    public partial class EntityToolsGeometryPolygon
    {
        public string type { get; set; }
        public List<List<List<double>>> coordinates { get; set; }

        public static string ToString(EntityToolsGeometryPolygon json) => JsonConvert.SerializeObject(json);
        public static EntityToolsGeometryPolygon FromJson(string json) => JsonConvert.DeserializeObject<EntityToolsGeometryPolygon>(json, HelperConverter.Converter.Settings);
    }

    public partial class EntityToolsGeometryPolygon2
    {
        public string type { get; set; }
        public List<List<double>> coordinates { get; set; }

        public static EntityToolsGeometryPolygon2 FromJson(string json) => JsonConvert.DeserializeObject<EntityToolsGeometryPolygon2>(json, HelperConverter.Converter.Settings);
    }

    public partial class EntityToolsGeometryCircle
    {
        public string type { get; set; }
        public List<double> coordinates { get; set; }

        public static string ToString(EntityToolsGeometryCircle json) => JsonConvert.SerializeObject(json);

        public static EntityToolsGeometryCircle FromJson(string json) => JsonConvert.DeserializeObject<EntityToolsGeometryCircle>(json, HelperConverter.Converter.Settings);
    }

    public partial class EntityToolsProperties
    {
        public bool stroke { get; set; }
        public string color { get; set; }
        public float weight { get; set; }
        public float opacity { get; set; }
        public bool fill { get; set; }
        public string fillColor { get; set; }
        public float fillOpacity { get; set; }
        public bool clickable { get; set; }
        public string dashArray { get; set; }
        public bool transform { get; set; }
        public bool draggable { get; set; }
        public float radius { get; set; }
        public string jenisArrow { get; set; }
        public bool interactive { get; set; }
        public string className { get; set; }

        public static string ToString(EntityToolsProperties json) => JsonConvert.SerializeObject(json);
        public static EntityToolsProperties FromJson(string json) => JsonConvert.DeserializeObject<EntityToolsProperties>(json, HelperConverter.Converter.Settings);
    }
    #endregion

    #region ENTITY_TEXT
    public partial class EntityText
    {
        public string id { get; set; }
        public string id_user { get; set; }
        public string dokumen { get; set; }
        public string nama { get; set; }
        [JsonProperty("lat_y")]
        public float lat { get; set; }
        [JsonProperty("lng_x")]
        public float lng { get; set; }
        public string info_text { get; set; }
        public string symbol { get; set; }

        public static EntityText FromJson(string json) => JsonConvert.DeserializeObject<EntityText>(json, HelperConverter.Converter.Settings);
    }
    #endregion

    #region ENTITY_OBSTACLE
    public partial class EntityObstacle
    {
        public int id { get; set; }
        public string id_user { get; set; }
        public string dokumen { get; set; }
        public string nama { get; set; }
        public float lat_y { get; set; }
        public float lng_x { get; set; }
        public string info_obstacle { get; set; }
        public string symbol { get; set; }
        public static EntityObstacle FromJson(string json) => JsonConvert.DeserializeObject<EntityObstacle>(json, HelperConverter.Converter.Settings);
    }

    public class Info_Obstacle
    {
        public string nama { get; set; }
        public string font { get; set; }
        public string warna { get; set; }
        public int size { get; set; }
        public string info { get; set; }
        public int index { get; set; }
        public int id_user { get; set; }

        public static string ToString(Info_Obstacle json) => JsonConvert.SerializeObject(json);
        public static Info_Obstacle FromJson(string json) => JsonConvert.DeserializeObject<Info_Obstacle>(json, HelperConverter.Converter.Settings);
    }

    public class SymbolObstacle
    {
        public string className { get; set; }
        public string html { get; set; }
        public object iconSize { get; set; }
        public string id_point { get; set; }
        public static SymbolObstacle FromJson(string json) => JsonConvert.DeserializeObject<SymbolObstacle>(json, HelperConverter.Converter.Settings);
    }
    #endregion

    #region ENTITY_BUNGUS
    public partial class EntityBungus
    {
        public string id { get; set; }
        public string id_user { get; set; }
        public string dokumen { get; set; }
        public string nama { get; set; }
        [JsonProperty("lat_y")]
        public float lat { get; set; }
        [JsonProperty("lng_x")]
        public float lng { get; set; }
        public string info_bungus { get; set; }
        public string symbol { get; set; }

        public static EntityBungus FromJson(string json) => JsonConvert.DeserializeObject<EntityBungus>(json, HelperConverter.Converter.Settings);
    }

    public partial class EntityBungusInfo
    {   
        public List<string> date { get; set; }
        public float? ukuran { get; set; }

        public static string ToString(EntityBungusInfo json) => JsonConvert.SerializeObject(json);
        public static EntityBungusInfo FromJson(string json) => JsonConvert.DeserializeObject<EntityBungusInfo>(json, HelperConverter.Converter.Settings);
    }
    #endregion

    #region ENTITY_VIDEO
    public partial class EntityVideo
    {
        public string id { get; set; }
        public string id_user { get; set; }
        public string dokumen { get; set; }
        public string nama { get; set; }
        [JsonProperty("lat_y")]
        public float? lat { get; set; }
        [JsonProperty("lng_x")]
        public float? lng { get; set; }
        public string info { get; set; }

        public static EntityVideo FromJson(string json) => JsonConvert.DeserializeObject<EntityVideo>(json, HelperConverter.Converter.Settings);    
    }

    public partial class EntityVideoInfo
    {
        public string nama_image { get; set; }
        public string judul_image { get; set; }
        public string waktuMulai { get; set; }
        public string waktuAkhir { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public string url { get; set; }

        public static string ToString(EntityVideoInfo json) => JsonConvert.SerializeObject(json);
        public static EntityVideoInfo FromJson(string json) => JsonConvert.DeserializeObject<EntityVideoInfo>(json, HelperConverter.Converter.Settings);
    }
    #endregion

    #region ENTITY_ANIMASI
    public partial class EntityAnimasi
    {
        public string id { get; set; }
        public string id_user { get; set; }
        public string dokumen { get; set; }
        public string nama { get; set; }
        [JsonProperty("lat_y")]
        public float lat { get; set; }
        [JsonProperty("lng_x")]
        public float lng { get; set; }
        public string info { get; set; }

        public static EntityAnimasi FromJson(string json) => JsonConvert.DeserializeObject<EntityAnimasi>(json, HelperConverter.Converter.Settings);
    }

    public partial class EntityAnimasiInfo
    {
        public string nama_image { get; set; }
        public string waktuMulai { get; set; }
        public string waktuAkhir { get; set; }
        public int size { get; set; }
        public string urlImage { get; set; }
        public string kategori { get; set; }

        public static string ToString(EntityAnimasiInfo json) => JsonConvert.SerializeObject(json);
        public static EntityAnimasiInfo FromJson(string json) => JsonConvert.DeserializeObject<EntityAnimasiInfo>(json, HelperConverter.Converter.Settings);
    }
    #endregion

    #region ENTITY_ICON_CUSTOM
    public partial class EntityIconCustom
    {
        public string id { get; set; }
        public string id_user { get; set; }
        public string dokumen { get; set; }
        public string nama { get; set; }
        [JsonProperty("lat_y")]
        public float lat { get; set; }
        [JsonProperty("lng_x")]
        public float lng { get; set; }
        public string info_icon_custom { get; set; }
        public string symbol { get; set; }
        public int size { get; set; }

        public static EntityIconCustom FromJson(string json) => JsonConvert.DeserializeObject<EntityIconCustom>(json, HelperConverter.Converter.Settings);
    }

    public partial class EntityIconCustomSymbol
    {
        public string namaimage { get; set; }
        public string urlimage { get; set; }
        public string id_point { get; set; }

        public static string ToString(EntityIconCustomSymbol json) => JsonConvert.SerializeObject(json);
        public static EntityIconCustomSymbol FromJson(string json) => JsonConvert.DeserializeObject<EntityIconCustomSymbol>(json, HelperConverter.Converter.Settings);
    }
    #endregion
}

namespace HelperDataAlutsista
{
    #region Detail Satuan Darat
    public class DetailSatuanDarat
    {
        [JsonProperty("object")]
        public ObjectDarat OBJ { get; set; }
        public ImageDarat images { get; set; }
        public string path_object_3d { get; set; }
        public string tipe_tni { get; set; }

        public static DetailSatuanDarat FromJson(string json) => JsonConvert.DeserializeObject<DetailSatuanDarat>(json, HelperConverter.Converter.Settings);
    }

    public class ObjectDarat
    {
        [JsonProperty("VEHICLE_ID")]
        public int? id { get; set; }
        [JsonProperty("VEHICLE_SYM_ID")]
        public int? symbolID { get; set; }
        [JsonProperty("VEHICLE_NAME")]
        public string name { get; set; }
        [JsonProperty("VEHICLE_MAX_SPEED")]
        public string maxSpeed { get; set; }
        [JsonProperty("vehicle_matra")]
        public string matra { get; set; }
        [JsonProperty("VEHICLE_FUEL")]
        public string fuelLoad { get; set; }
        [JsonProperty("VEHICLE_FUEL_CAPACITY")]
        public string fuelCapacity { get; set; }
        [JsonProperty("VEHICLE_CONSUME_FUEL")]
        public string fuelConsume { get; set; }
        [JsonProperty("VEHICLE_HEIGHT")]
        public string height { get; set; }
        [JsonProperty("VEHICLE_WIDTH")]
        public string width { get; set; }
        [JsonProperty("VEHICLE_WEIGHT")]
        public string weight { get; set; }
        [JsonProperty("VEHICLE_HEALTH")]
        public string health { get; set; }
        [JsonProperty("VEHICLE_LENGTH")]
        public string length { get; set; }
        [JsonProperty("image")]
        public ImageDarat image { get; set; }
        [JsonProperty("negara_pembuat")]
        public string negaraPembuat { get; set; }
        [JsonProperty("perusahaan_pembuat")]
        public string perusahaanPembuat { get; set; }
        [JsonProperty("tahun_pembuat")]
        public string tahunPembuat { get; set; }
        [JsonProperty("titan")]
        public string titan { get; set; }
        [JsonProperty("VEHICLE_IS_AMFIBI")]
        public int? isAmfibi { get; set; }
        [JsonProperty("VEHICLE_TYPE_ID")]
        public int? typeID { get; set; }
        [JsonProperty("VEHICLE_TYPE_DESC")]
        public string typeDesc { get; set; }

        public string jenis { get; set; }
        public string style_symbol { get; set; }

        public static string ToString(ObjectDarat json) => JsonConvert.SerializeObject(json);
    }

    public class ImageDarat
    {
        public int? VEHICLE_ID { get; set; }
        public string VEHICLE_IMAGE_FNAME { get; set; }
        public string VEHICLE_IMAGE_DESC { get; set; }
    }
    #endregion

    #region Detail Satuan Laut
    public class DetailSatuanLaut
    {
        [JsonProperty("object")]
        public ObjectLaut OBJ { get; set; }
        //public ImageLaut? images { get; set; }
        public string path_object_3d { get; set; }
        public string tipe_tni { get; set; }
        
        public static DetailSatuanLaut FromJson(string json) => JsonConvert.DeserializeObject<DetailSatuanLaut>(json, HelperConverter.Converter.Settings);
    }

    public class ObjectLaut
    {
        [JsonProperty("SHIP_ID")]
        public int? id { get; set; }
        [JsonProperty("SHIP_SYM_ID")]
        public int? symbolID { get; set; }
        [JsonProperty("SHIP_NAME")]
        public string name { get; set; }
        [JsonProperty("SHIP_MAX_SPEED")]
        public string maxSpeed { get; set; }
        [JsonProperty("ship_matra")]
        public string matra { get; set; }
        [JsonProperty("SHIP_FUEL_LOAD")]
        public float? fuelLoad { get; set; }
        [JsonProperty("SHIP_FUEL_MAX")]
        public float? fuelMax { get; set; }
        [JsonProperty("SHIP_FUEL_CAPACITY")]
        public float? fuelCapacity { get; set; }
        [JsonProperty("SHIP_CONSUME_FUEL")]
        public float? fuelConsume { get; set; }
        [JsonProperty("SHIP_DIM_LENGTH")]
        public float? length { get; set; }
        [JsonProperty("SHIP_DIM_HEIGHT")]
        public float? height { get; set; }
        [JsonProperty("SHIP_DIM_WIDTH")]
        public float? width { get; set; }
        [JsonProperty("image")]
        public ImageLaut image { get; set; }
        [JsonProperty("SHIP_CATEGORY_ID")]
        public string categoryID { get; set; }
        [JsonProperty("SHIP_CATEGORY_NAME")]
        public string categoryName { get; set; }
        [JsonProperty("SHIP_CATEGORY_DESC")]
        public string categoryDesc { get; set; }
        [JsonProperty("SHIP_CLASS_ID")]
        public int? classID { get; set; }
        [JsonProperty("SHIP_CLASS_NAME")]
        public string className { get; set; }
        [JsonProperty("SHIP_HEALTH")]
        public float? health { get; set; }
        [JsonProperty("SHIP_MAX_LOAD_QTY")]
        public string maxLoadQty { get; set; }
        [JsonProperty("negara_pembuat")]
        public string negaraPembuat { get; set; }
        [JsonProperty("perusahaan_pembuat")]
        public string perusahaanPembuat { get; set; }
        [JsonProperty("tahun_pembuat")]
        public string tahunPembuat { get; set; }
        [JsonProperty("JUMLAH_PERWIRA")]
        public string jumlah_perwira { get; set; }
        [JsonProperty("JUMLAH_TAMTAMA")]
        public string jumlah_tamtama { get; set; }
        [JsonProperty("JUMLAH_BINTARA")]
        public string jumlah_bintara { get; set; }
        [JsonProperty("titan")]
        public string titan { get; set; }

        public string jenis { get; set; }
        public string style_symbol { get; set; }

        public static string ToString(ObjectLaut json) => JsonConvert.SerializeObject(json);
    }

    public class ImageLaut
    {
        public int? SHIP_ID { get; set; }
        public string SHIP_IMAGE_FNAME { get; set; }
        public string SHIP_IMAGE_DESC { get; set; }
    }
    #endregion
    
    #region Detail Satuan Udara
    public class DetailSatuanUdara
    {
        [JsonProperty("object")]
        public ObjectUdara OBJ { get; set; }
        public ImageUdara images { get; set; }
        public string path_object_3d { get; set; }
        public string tipe_tni { get; set; }
        
        public static DetailSatuanUdara FromJson(string json) => JsonConvert.DeserializeObject<DetailSatuanUdara>(json, HelperConverter.Converter.Settings);
    }

    public class ObjectUdara
    {
        [JsonProperty("AIRCRAFT_ID")]
        public int? id { get; set; }
        [JsonProperty("AIRCRAFT_SYM_ID")]
        public int? symbolID { get; set; }
        [JsonProperty("AIRCRAFT_NAME")]
        public string name { get; set; }
        [JsonProperty("AIRCRAFT_MAX_SPEED")]
        public float? maxSpeed { get; set; }
        [JsonProperty("aircraft_matra")]
        public string matra { get; set; }
        [JsonProperty("AIRCRAFT_FUEL_CAPACITY")]
        public float? fuelCapacity { get; set; }
        [JsonProperty("AIRCRAFT_CONSUME_FUEL")]
        public float? fuelConsume { get; set; }
        [JsonProperty("AIRCRAFT_HEIGHT")]
        public float? height { get; set; }
        [JsonProperty("AIRCRAFT_WEIGHT")]
        public float? weight { get; set; }
        [JsonProperty("AIRCRAFT_HEALTH")]
        public float? health { get; set; }
        [JsonProperty("AIRCRAFT_TYPE")]
        public int? type { get; set; }
        [JsonProperty("AIRCRAFT_TYPE_DESC")]
        public string typeDesc { get; set; }
        [JsonProperty("image")]
        public ImageUdara image { get; set; }
        [JsonProperty("negara_pembuat")]
        public string negaraPembuat { get; set; }
        [JsonProperty("perusahaan_pembuat")]
        public string perusahaanPembuat { get; set; }
        [JsonProperty("tahun_pembuat")]
        public string tahunPembuat { get; set; }
        [JsonProperty("titan")]
        public string titan { get; set; }

        public string jenis { get; set; }
        public string style_symbol { get; set; }

        public static string ToString(ObjectUdara json) => JsonConvert.SerializeObject(json);
    }

    public class ImageUdara
    {
        public int? AIRCRAFT_ID { get; set; }
        public string AIRCRAFT_IMAGE_FNAME { get; set; }
        public string AIRCRAFT_IMAGE_DESC { get; set; }
    }
    #endregion
}

namespace HelperEditor
{
    #region -- ARYA PLOTTING REGION --
    public partial class ListWeapon
    {
        public int? id { get; set; }
        public string jenis { get; set; }
    }

    public partial class ListSatuan
    {
        [JsonProperty("VEHICLE_ID")]
        public string ID { get; set; }
        public string SHIP_ID { set { ID = value; } }
        public string AIRCRAFT_ID { set { ID = value; } }

        [JsonProperty("VEHICLE_NAME")]
        public string NAME { get; set; }
        public string SHIP_NAME { set { NAME = value; } }
        public string AIRCRAFT_NAME { set { NAME = value; } }

        [JsonProperty("VEHICLE_NO")]
        public string NO { get; set; }
        public string SHIP_NO { set { NO = value; } }
        public string AIRCRAFT_NO { set { NO = value; } }

        //[JsonProperty("VEHICLE_MAX_SPEED")]
        //public string MAX_SPEED { get; set; }
        //private string SHIP_MAX_SPEED { set { MAX_SPEED = value; } }
        //public int? id_tipe_object { get; set; }
        //public int? id_font_taktis { get; set; }
        //public int? id_kategori_object { get; set; }
        //public int? id_jenis_object { get; set; }
        public string nama_object { get; set; }
        public string path_object_3d { get; set; }

        public static ListSatuan[] FromJson(string json) => JsonConvert.DeserializeObject<ListSatuan[]>(json, HelperConverter.Converter.Settings);
    }

    public partial class StyleListSatuan
    {
        public string nama { get; set; }
        public int? index { get; set; }
        public string keterangan { get; set; }
        public string grup { get; set; }
        public string entity_name { get; set; }
        public string entity_kategori { get; set; }
        public string jenis_role { get; set; }
        public string label_style { get; set; }
        public string style_symbol { set { label_style = value; } }

        public static StyleListSatuan[] FromJson(string json) => JsonConvert.DeserializeObject<StyleListSatuan[]>(json, HelperConverter.Converter.Settings);
        public static string ToString(StyleListSatuan json) => JsonConvert.SerializeObject(json);
    }

    public partial class InfoListSatuan
    {
        public string kecepatan { get; set; }
        public string nomer_satuan { get; set; }
        public string nama_satuan { get; set; }
        public string nomer_atasan { get; set; }
        public string tgl_mulai { get; set; }
        public string tgl_selesai { get; set; }
        public string warna { get; set; }
        public string size { get; set; }
        public string weapon { get; set; }
        public List<string> waypoint { get; set; }
        public List<string> list_embarkasi { get; set; }
        public int? armor { get; set; }
        public bool id_dislokasi { get; set; }
        public bool id_dislokasi_obj { get; set; }
        public string bahan_bakar { get; set; }
        public string kecepatan_maks { get; set; }
        public string heading { get; set; }
        public string ket_satuan { get; set; }
        public string nama_icon_satuan { get; set; }
        public string width_icon_satuan { get; set; }
        public string height_icon_satuan { get; set; }
        public int? personil { get; set; }
        public bool tfg { get; set; }
        public bool hidewgs { get; set; }
        public string kedalaman { get; set; }

        public static InfoListSatuan FromJson(string json) => JsonConvert.DeserializeObject<InfoListSatuan>(json, HelperConverter.Converter.Settings);
        public static string ToString(InfoListSatuan json) => JsonConvert.SerializeObject(json);
    }

    public partial class ListRadar
    {
        public int? id { get; set; }
        public int? id_user { get; set; }
        public string dokumen { get; set; }
        public string nama { get; set; }
        public string lat_y { get; set; }
        public string lng_x { get; set; }
        public string info_radar { get; set; }
        public string symbol { get; set; }
        public string info_symbol { get; set; }

        public static ListRadar FromJson(string json) => JsonConvert.DeserializeObject<ListRadar>(json, HelperConverter.Converter.Settings);
    }

    public partial class ListInfoRadar
    {
        public string nama { get; set; }
        public string judul { get; set; }
        public int? radius { get; set; }
        public string size { get; set; }
        public string warna { get; set; }
        public int? id_user { get; set; }
        public ListIDSymbolInfoRadar id_symbol { get; set; }
        public string jenis_radar { get; set; }
        public string jml_peluru { get; set; }
        public string new_index { get; set; }
        public ListWeapon weapon { get; set; }
        public int? armor { get; set; }

        public static string ToString(ListInfoRadar json) => JsonConvert.SerializeObject(json);
        public static ListInfoRadar FromJson(string json) => JsonConvert.DeserializeObject<ListInfoRadar>(json, HelperConverter.Converter.Settings);
    }

    public partial class ListIDSymbolInfoRadar
    {
        public string id { get; set; }
        public string nama { get; set; }
        public int index { get; set; }
        public string keterangan { get; set; }
        public string grup { get; set; }
        public string entity_name { get; set; }
        public string entity_kategori { get; set; }
        public string jenis_role { get; set; }
        public int? health { get; set; }
    }

    public partial class GetListRadar
    {
        // DIAMBIL DARI SERVICE GET LIST RADAR
        // 1. Required Untuk ListInfoRadar
        public string RADAR_NAME { get; set; }
        public int? RADAR_DET_RANGE { get; set; }
        public string id { get; set; }
        public string nama { get; set; }
        public int index { get; set; }
        public string keterangan { get; set; }
        public string grup { get; set; }
        public string entity_name { get; set; }
        public string entity_kategori { get; set; }
        public string jenis_role { get; set; }
        public int? health { get; set; }
        // --

        public static GetListRadar[] FromJson(string json) => JsonConvert.DeserializeObject<GetListRadar[]>(json, HelperConverter.Converter.Settings);
    }

    public partial class InfoSituasi
    {
        public string isi_situasi { get; set; }
        public string tgl_situasi { get; set; }
        public string waktu_situasi { get; set; }
        public int size { get; set; }
        public string warna { get; set; }
        public string property { get; set; }

        public static string ToString(InfoSituasi json) => JsonConvert.SerializeObject(json);
        public static InfoSituasi FromJson(string json) => JsonConvert.DeserializeObject<InfoSituasi>(json, HelperConverter.Converter.Settings);
    }

    public partial class SymbolText
    {
        public string text { get; set; }
        public string angel { get; set; }
        public string size { get; set; }
        public string warna { get; set; }
        public string weight { get; set; }

        public float? zoom { get; set; }

        public static string ToString(SymbolText json) => JsonConvert.SerializeObject(json);
        public static SymbolText FromJson(string json) => JsonConvert.DeserializeObject<SymbolText>(json, HelperConverter.Converter.Settings);
    }


    public partial class ListLogistik
    {
        public long? id_user { get; set; }
        public string dokumen { get; set; }
        public string nama { get; set; }
        public float lat_y { get; set; }
        public float lng_x { get; set; }
        public string info_logistik { get; set; }
        public string isi_logistik { get; set; }
        public string jenis { get; set; }
        public string warna { get; set; }
        public string symbol { get; set; }
        public float size { get; set; }

        public static string ToString(ListLogistik json) => JsonConvert.SerializeObject(json);
        public static ListLogistik FromJson(string json) => JsonConvert.DeserializeObject<ListLogistik>(json, HelperConverter.Converter.Settings);
    }

    public partial class InfoLogistik
    {
        public string id { get; set; }
        public string nama { get; set; }
        public int index { get; set; }
        public string keteragan { get; set; }
        public string grup { get; set; }
        public string entity_name { get; set; }
        public string entity_kategori { get; set; }
        public string jenis_role { get; set; }
        public string health { get; set; }

        public static string ToString(InfoLogistik json) => JsonConvert.SerializeObject(json);
        public static InfoLogistik FromJson(string json) => JsonConvert.DeserializeObject<InfoLogistik>(json, HelperConverter.Converter.Settings);
    }

    public partial class IsiLogistik
    {
        public string judul { get; set; }
        public string isi { get; set; }

        public static string ToString(List<IsiLogistik> json) => JsonConvert.SerializeObject(json);
        public static string ToString(IsiLogistik json) => JsonConvert.SerializeObject(json);
        public static List<IsiLogistik> FromJson(string json) => JsonConvert.DeserializeObject<List<IsiLogistik>> (json, HelperConverter.Converter.Settings);
    }

    public partial class InfoBungus
    {
        public List<string> date { get; set; }
        public int ukuran { get; set; }

        public static string ToString(InfoBungus json) => JsonConvert.SerializeObject(json);
        public static InfoBungus FromJson(string json) => JsonConvert.DeserializeObject<InfoBungus>(json, HelperConverter.Converter.Settings);
    }
    #endregion

    #region -- IZZAN PLOTTING REGION --
    public partial class ListPasukan
    {
        public int id { get; set; }
        public string name { get; set; }
        public string matra_pasukan { get; set; }
        public string pasukan_sym_id { get; set; }
        public string style_symbol { get; set; }
        public string nama { get; set; }
        public string index { get; set; }
        public string keterangan { get; set; }

        public static List<ListPasukan> FromJson(string json) => JsonConvert.DeserializeObject<List<ListPasukan>>(json, HelperConverter.Converter.Settings);
    }

    #region -- DETAIL SATUAN KEKUATAN --

    public partial class EntityKekuatan
    {
        public int id { get; set; }
        public string id_user { get; set; }
        public string dokumen { get; set; }
        public string nama { get; set; }
        public float lat_y { get; set; }
        public float lng_x { get; set; }
        public string info_kekuatan { get; set; }
        public string rincian { get; set; }
        public string size { get; set; }

        public static EntityKekuatan FromJson(string json) => JsonConvert.DeserializeObject<EntityKekuatan>(json, HelperConverter.Converter.Settings);
    }

    public class Font_Kekuatan
    {
        public string entity_kategori { get; set; }
        public string entity_name { get; set; }
        public string grup { get; set; }
        public object health { get; set; }
        public string id { get; set; }
        public string index { get; set; }
        public string jenis_role { get; set; }
        public string keterangan { get; set; }
        public string nama { get; set; }

        public static string ToString(Font_Kekuatan json) => JsonConvert.SerializeObject(json);
        public static Font_Kekuatan FromJson(string json) => JsonConvert.DeserializeObject<Font_Kekuatan>(json, HelperConverter.Converter.Settings);
    }

    public class Info_Kekuatan
    {
        public Font_Kekuatan font { get; set; }
        public string jumlah { get; set; }
        public string jenis { get; set; }
        public string id_symbol { get; set; }
        public string nama { get; set; }

        public static string ToString(Info_Kekuatan json) => JsonConvert.SerializeObject(json);
        public static Info_Kekuatan[] FromJson(string json) => JsonConvert.DeserializeObject<Info_Kekuatan[]>(json, HelperConverter.Converter.Settings);
    }

    public class Property_Kekuatan
    {
        public int distance_ { get; set; }
        public int degree_ { get; set; }
        public int width_ { get; set; }
        public int height_ { get; set; }

        // public static string ToString(Property_Kekuatan json) => JsonConvert.SerializeObject(json);
        public static Property_Kekuatan FromJson(string json) => JsonConvert.DeserializeObject<Property_Kekuatan>(json, HelperConverter.Converter.Settings);
    }

    public class Rincian_Kekuatan
    {
        public string nama_kekuatan { get; set; }
        public string warna { get; set; }
        public string ket { get; set; }
        public string property { get; set; }
        public string milik { get; set; }

        // public static string ToString(Rincian_Kekuatan json) => JsonConvert.SerializeObject(json);
        public static Rincian_Kekuatan FromJson(string json) => JsonConvert.DeserializeObject<Rincian_Kekuatan>(json, HelperConverter.Converter.Settings);
    }
    #endregion
    #endregion
}

/*namespace HelperKecepatan
{
    public class speedConversion
    {
        public enum speedType { kmph, mph, knot, mach }
        public float speedInKM(float speed, speedType type)
        {
            switch (type)
            {
                case speedType.kmph:
                    return speed;
                    break;
                case speedType.mph :
                    return speed * 1.60934f;
                    break;
                case speedType.knot:
                    return speed * 1.852f;
                    break;
                case speedType.mach:
                    return speed * 1192.68f;
                    break;
                default: return speed;
                    break;
            }
        }
        
        public speedType getSpeedType(string type)
        {
            switch (type)
            {
                case "km":
                    return speedType.kmph;
                    break;
                case "m":
                    return speedType.mph;
                    break;
                case "knot":
                    return speedType.knot;
                    break;
                case "mach":
                    return speedType.mach;
                    break;
                default:
                    return speedType.kmph;
                    break;
            }
        }

        public string getSpeedName(speedType type, string speed = "")
        {
            switch (type)
            {
                case speedType.kmph:
                    return speed + " km/h";
                    break;
                case speedType.mph:
                    return speed + " m/h";
                case speedType.knot:
                    return speed + " knot";
                case speedType.mach:
                    return "mach" + speed;
                default:
                    return speed + "km/h";
                    break;
            }
        }
    }
}*/

// --- CONVERTER HELPER (Untuk Seluruh Helper) ---
public partial class HelperConverter
{
    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };

    }
}
// -----------------------------------------------