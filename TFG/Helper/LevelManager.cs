using System.Collections.Generic;
using System.Threading.Tasks;


using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    [Header("SCENE DATA")]
    public string LoginScene;
    public string GameplayScene;
    public string EditorScene;
    public string AssetScene;

    [Header("Konfigurasi")]
    public List<GameObject> _loaderUI;

    public static LevelManager Instance;

    void Awake()
    {
        // Set Scene Manager Menjadi 1 Origin
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public async void LoadScene(string sceneName, string loaderName = null, string loadingAnimationStartAnim = null, string loadingAnimationEndAnim = null)
    {
        var old_scene = SceneManager.GetActiveScene();

        int selectedIndex = loaderName == null ? 0 : _loaderUI.FindIndex(gameObject => string.Equals(loaderName, gameObject.name));

        GameObject LoadingPrefab = Instantiate(_loaderUI[selectedIndex].gameObject, gameObject.transform);                                                  // Spawn Loading UI Into Level Manager (this)


        LoadingPrefab.GetComponent<Animator>().Play(loadingAnimationStartAnim == null ? "LoadingScreenStart" : loadingAnimationStartAnim);                  // Play Start Animation

        await Task.Delay(2000);
        if(sceneName == GameplayScene || sceneName == EditorScene)
        {
            await AssetPackageManager.Instance.loadPackageAlutsista();
        }

        var scene = SceneManager.LoadSceneAsync(sceneName);                                                                                                 // Load The Scene
        scene.allowSceneActivation = false;                                                                                                                 // Disable Scene Activation When It's Not Loaded Yet

        // Progress Bar Respond
        do
        {
            await Task.Delay(100);
            //_loaderUI[selectedIndex].transform.FindChild("ProgressBar");
        } while (scene.progress < 0.9f);


        await Task.Delay(1000);
        scene.allowSceneActivation = true;

        LoadingPrefab.GetComponent<Animator>().Play(loadingAnimationEndAnim == null ? "LoadingScreenEnd" : loadingAnimationEndAnim);                        // Play End Animation

        await Task.Delay(2000);
        Destroy(LoadingPrefab);                                                                                                                             // Destroy UI Loading Setelah Selesai (wait for 2 seconds first)


    }
}
