using UnityEngine;
using TMPro;

public class PlotNotificationHelper : MonoBehaviour
{
    public Animator slotSatuan;

    #region INSTANCE
    public static PlotNotificationHelper Instance;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    public void TogglePlotNotification(bool state, string message = null)
    {
        GetComponent<Metadata>().FindParameter("message").parameter.GetComponent<TextMeshProUGUI>().text = (message == null) ? "Click on the Map to place the object..." : message;
        GetComponent<Animator>().Play((state) ? "FadeIn" : "FadeOut");

        // Hide Slot Satuan Ketika Notifikasi Plot Tampil
        slotSatuan.Play((state) ? "FadeOut" : "FadeIn");
    }
}
