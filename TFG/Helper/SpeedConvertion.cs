using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedConvertion : MonoBehaviour
{
    #region SET INSTANCE
    // INSTANCE VARIABLE
    public static SpeedConvertion Instance;
    
    /// <summary>
    /// Set Instance On Awake
    /// </summary>
    private void Awake()
    {
        // Set Speed Convertion Helper Menjadi 1 Origin
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion
    
    public enum speedType { kmph, mph, knot, mach }
    
    public float speedInKM(float speed, speedType type)
    {
        switch (type)
        {
            case speedType.kmph:
                return speed;
                break;
            case speedType.mph :
                return speed * 1.60934f;
                break;
            case speedType.knot:
                return speed * 1.852f;
                break;
            case speedType.mach:
                return speed * 1192.68f;
                break;
            default: return speed;
                break;
        }
    }
            
    public speedType getSpeedType(string type)
    {
        switch (type)
        {
            case "km":
                return speedType.kmph;
                break;
            case "m":
                return speedType.mph;
                break;
            case "knot":
                return speedType.knot;
                break;
            case "mach":
                return speedType.mach;
                break;
            default:
                return speedType.kmph;
                break;
        }
    }
    
    public string getSpeedName(speedType type, string speed = "")
    {
        switch (type)
        {
            case speedType.kmph:
                return speed + " km/h";
                break;
            case speedType.mph:
                return speed + " m/h";
            case speedType.knot:
                return speed + " knot";
            case speedType.mach:
                return "mach" + speed;
            default:
                return speed + "km/h";
                break;
        }
    }
}
