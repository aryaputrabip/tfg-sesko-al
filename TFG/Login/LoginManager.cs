using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

using TMPro;
using GlobalVariables;

public class LoginManager : MonoBehaviour
{
    [Header("References")]
    public Animator UI_Canvas;

    [Header("FORM DATA")]
    public TMP_Dropdown server;
    public TMP_InputField username;
    public TMP_InputField password;

    [Header("EVENTS")]
    public UnityEvent OnLoginSuccess;
    public UnityEvent OnLogoutSuccess;

    private API api;
    private int selectedInput;
    private bool canUseKeyboardOnLogin = true;

    #region INSTANCE
    public static LoginManager instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;

            api = new API();
            selectedInput = 0;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    private void Update()
    {
        if (canUseKeyboardOnLogin)
        {
            #region TAB BETWEEN FORM INPUT
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                // Set Selected Form Input
                switch (selectedInput)
                {
                    case 0:
                        username.Select();
                        break;
                    case 1:
                        password.Select();
                        break;
                    default: break;
                }
                
                // Set to Next Form Input Index
                selectedInput++;
                if (selectedInput >= 2) selectedInput = 0;
            }

            if (Input.GetKeyDown(KeyCode.Return))
            {
                CheckForm();
            }
        }
        #endregion
    }

    public async void CheckForm()
    {
        bool isFormCompleted = true;
        // Check if Server is Choosed
        if(server.value == null)
        {
            isFormCompleted = false;
        }

        // Check If All Required Input is Filled
        if(username.text == null || username.text == "") isFormCompleted = OnInputError(username.gameObject);
        if(password.text == null || password.text == "") isFormCompleted = OnInputError(password.gameObject);

        // If Form Checking Passed, Login...
        if (isFormCompleted) await Login();
    }

    private async Task Login()
    {
        canUseKeyboardOnLogin = false;

        // Set Delay to allow loading animation start before executing login request
        LoadingManager.instance.ShowLoading("loading-blur", "Logged In...");
        await Task.Delay(500);

        // Requesting Login
        WWWForm form = new WWWForm();
        form.AddField("username", username.text);
        form.AddField("password", password.text);

        var result = await api.RequestLogin(form);

        if(result == "done")
        {
            /* 
             * ==> If Login Request Success <==
             * - Request Skenario Aktif to get ID Skenario
             * - Request CB Terbaik to get CB Terbaik of current user
             * - Show Main Menu Section
             * - Clear Login Form
            */
            await API.Instance.GetSkenarioAktif();
            await API.Instance.GetCBTerbaik(SessionUser.id_bagian.ToString());

            LoadingManager.instance.HideLoading();

            ResetForm();

            OnLoginSuccess.Invoke();
        }
        else if(result == "retry")
        {
            /* ==> If Login Request Need to Retry <==
             * - Request Logout and Set Delay to allow request refreshing first
             * - Retry Checking Form again to re-executing request login
             */
            await api.RequestLogout();
            await Task.Delay(500);

            CheckForm();
        }
        else
        {
            /* ==> If Login Request Failed <==
             * - Check What Caused Request Failed
             * - Show Message Based On the Failed
             */
            if (result == "user_failed")
            {
                OnInputError(username.gameObject, "Username / Password Salah!");
            }
            else if (result == "connection_failed")
            {
                OnInputError(server.gameObject, "Koneksi Gagal!");
            }

            LoadingManager.instance.HideLoading();
            canUseKeyboardOnLogin = true;
        }
    }

    public async void Logout()
    {
        LoadingManager.instance.ShowLoading("loading-blur", "Logging Out...");

        await Task.Delay(500);
        await api.RequestLogout();

        LoadingManager.instance.HideLoading();
        OnLogoutSuccess.Invoke();
    }

    /// <summary>
    /// Go To Gameplay Scene
    /// </summary>
    public async void LoadToGameplay()
    {
        LoadingManager.instance.ShowLoading("loading-blur", null);
        await Task.Delay(2000);

        SceneLoad.returnTo = LevelManager.Instance.LoginScene;
        LevelManager.Instance.LoadScene(LevelManager.Instance.GameplayScene, null, "FadeIn", "FadeOut");

        await Task.Delay(250);
        LoadingManager.instance.HideLoading();

        UI_Canvas.Play("SlideOut");
    }

    /// <summary>
    /// Go To Scenario Editor Scene
    /// </summary>
    public async void LoadToScenarioEditor()
    {
        LoadingManager.instance.ShowLoading("loading-blur", null);
        await Task.Delay(2000);

        SceneLoad.returnTo = LevelManager.Instance.LoginScene;
        LevelManager.Instance.LoadScene(LevelManager.Instance.EditorScene, null, "FadeIn", "FadeOut");

        await Task.Delay(250);
        LoadingManager.instance.HideLoading();

        UI_Canvas.Play("SlideOut");
    }

    /// <summary>
    /// Go To Asset Editor Scene
    /// </summary>
    public async void LoadToAssetEditor()
    {
        LoadingManager.instance.ShowLoading("loading-blur", null);
        await Task.Delay(2000);

        SceneLoad.returnTo = LevelManager.Instance.LoginScene;
        LevelManager.Instance.LoadScene(LevelManager.Instance.AssetScene, null, "FadeIn", "FadeOut");

        await Task.Delay(250);
        LoadingManager.instance.HideLoading();

        UI_Canvas.Play("SlideOut");
    }

    /// <summary>
    /// Reset Input Form
    /// </summary>
    public void ResetForm()
    {
        username.text = "";
        password.text = "";

        HideAnyInputMessage();
    }

    /// <summary>
    /// Toggle Set Action Where User Can Use Tab and Enter on Login Form
    /// </summary>
    public void ToggleUseKeyboardOnLogin(bool state)
    {
        canUseKeyboardOnLogin = state;
    }

    /// <summary>
    /// On Form Required Input Not Filled
    /// </summary>
    /// <param name="input">GameObject Input</param>
    /// <param name="message">Custom Message (optional)</param>
    public bool OnInputError(GameObject input, string message = null)
    {
        var data = input.GetComponent<Metadata>().FindParameter("validator").parameter;

        data.SetActive(true);
        data.GetComponent<TextMeshProUGUI>().text = (message != null) ? message : (input.name + " belum diisi!");

        return false;
    }

    /// <summary>
    /// Hide or Reset All Form Input Error Message to Hidden
    /// </summary>
    private void HideAnyInputMessage()
    {
        server.GetComponent<Metadata>().FindParameter("validator").parameter.SetActive(false);
        username.GetComponent<Metadata>().FindParameter("validator").parameter.SetActive(false);
        password.GetComponent<Metadata>().FindParameter("validator").parameter.SetActive(false);
    }

    public void ExitApp()
    {
        Application.Quit();
    }
}
