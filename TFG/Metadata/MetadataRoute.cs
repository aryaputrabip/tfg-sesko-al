using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using System;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif

/// <summary>
/// Fungsi Metadata Sebagai Preferensi Rute Misi
/// Editor By : Aryaputra BIP
/// </summary>
public class MetadataRoute : MonoBehaviour
{
    public enum type { Pergerakan, Embarkasi, Debarkasi }

  public string routeID;
  public type routeType;

  public List<MetadataRouteParam> data = new List<MetadataRouteParam>();

  public void AddNewPos(string time, Vector3 coordinate, int index)
  {
    var new_metadata = new MetadataRouteParam();
    new_metadata.time = time;
    new_metadata.coordinate = coordinate;
    new_metadata.pointIndex = index;

    data.Add(new_metadata);
  }

  /// <summary>
  /// Mendapatkan Parameter Dari Data Berdasarkan Key Yang Di-Select
  /// </summary>
  /// <returns>Key</returns>
  /// <param name="key">Key Yang Dipilih</param>
  public MetadataRouteParam RunOnTime(string time)
  {
    var timeSearch = DateTime.Parse(time);
    var sel_pointIndex = -1;
    var sel_time = "";
    var sel_coords = new Vector2(0, 0);

    for(int i=0; i < data.Count; i++)
    {
      if(timeSearch == DateTime.Parse(data[i].time))
      {
        RunOnTimeFound(data[i], i, out sel_pointIndex, out sel_time, out sel_coords);
        i = data.Count;
      }
      else if(timeSearch > DateTime.Parse(data[i].time) && timeSearch < DateTime.Parse(data[i + 3].time))
      {
        RunOnTimeFound(data[i], i, out sel_pointIndex, out sel_time, out sel_coords);
        i = data.Count;
      }
    }

    if(sel_pointIndex != -1)
    {
      var finded = new MetadataRouteParam();

      finded.pointIndex = sel_pointIndex;
      finded.time = sel_time;
      finded.coordinate = sel_coords;

      return finded;
    }

    //IEnumerable<string> times = from meta in data where meta.time == time select meta.time;
    //IEnumerable<Vector3> coordinate = from meta in data where meta.time == time select meta.coordinate;

    //foreach (string time_select in times)
    //{
    //  foreach (Vector3 coord_select in coordinate)
    //  {
    //    var finded = new MetadataRouteParam();
    //    finded.time = time_select;
    //    finded.coordinate = coord_select;

    //    return finded;
    //  }
    //}

    return null;
  }

  private void RunOnTimeFound(MetadataRouteParam data, int index, out int pointIndex, out string time, out Vector2 coordinate)
  {
    pointIndex = index;
    time = data.time;
    coordinate = data.coordinate;

  }
}

[System.Serializable]
public class MetadataRouteParam
{
  public string time;
  public Vector3 coordinate;
  public int pointIndex;
}

#region Inspector (Editor)
#if UNITY_EDITOR
[CustomEditor(typeof(MetadataRoute))]
public class MetadataRouteEditor : Editor
{
  MetadataRoute meta;
  ReorderableList orderList;

  private void OnEnable()
  {
    if (target == null) return;
    meta = (MetadataRoute)target;

    orderList = new ReorderableList(serializedObject, serializedObject.FindProperty("data"),
                                    true, true, true, true);

    orderList.drawElementCallback = DrawListItems; // Delegate to draw the elements on the list
    orderList.drawHeaderCallback = DrawHeader;
  }

  void DrawListItems(Rect rect, int index, bool isActive, bool isFocused)
  {
    SerializedProperty element = orderList.serializedProperty.GetArrayElementAtIndex(index);

    EditorGUI.PropertyField(
      new Rect(rect.x, rect.y, 90, EditorGUIUtility.singleLineHeight),
      element.FindPropertyRelative("time"), GUIContent.none
    );

    EditorGUI.PropertyField(
      new Rect(rect.x + 92, rect.y, rect.width - 92, EditorGUIUtility.singleLineHeight),
      element.FindPropertyRelative("coordinate"), GUIContent.none
    );
  }

  void DrawHeader(Rect rect)
  {
    string name = "Parameter";
    EditorGUI.LabelField(rect, name);
  }

  public override void OnInspectorGUI()
  {
    DrawDefaultInspector();
    serializedObject.Update(); // Update the array property's representation in the inspector

    orderList.DoLayoutList(); // Have the ReorderableList do its work

    // We need to call this so that changes on the Inspector are saved by Unity.
    serializedObject.ApplyModifiedProperties();
  }
}
#endif
#endregion