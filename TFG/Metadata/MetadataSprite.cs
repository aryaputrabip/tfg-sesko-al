using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif

/// <summary>
/// Fungsi Metadata Sebagai Preferensi Data Dari Sprite UI
/// Editor By : Aryaputra BIP
/// </summary>

public class MetadataSprite : MonoBehaviour
{
  public List<MetadataSpriteParam> data = new List<MetadataSpriteParam>();

  /// <summary>
  /// Mendapatkan Parameter Dari Data Berdasarkan Key Yang Di-Select
  /// </summary>
  /// <returns>Key</returns>
  /// <param name="key">Key Yang Dipilih</param>
  public MetadataSpriteParam FindParameter(string key)
  {
    IEnumerable<string> keys = from meta in data where meta.key == key select meta.key;
    IEnumerable<Sprite> parameters = from meta in data where meta.key == key select meta.parameter;

    foreach (string keyselect in keys)
    {
      foreach (Sprite paramselect in parameters)
      {
        var finded = new MetadataSpriteParam();
        finded.key = keyselect;
        finded.parameter = paramselect;

        return finded;
      }
    }

    return null;
  }
}


[System.Serializable]
public class MetadataSpriteParam
{
  public string key;
  public Sprite parameter;
}

#if UNITY_EDITOR
#region Inspector (Editor)
#if UNITY_EDITOR
[CustomEditor(typeof(MetadataSprite))]
public class MetadataSpriteEditor : Editor
{
  MetadataSprite meta;
  ReorderableList orderList;

  private void OnEnable()
  {
    if (target == null) return;
    meta = (MetadataSprite)target;

    orderList = new ReorderableList(serializedObject, serializedObject.FindProperty("data"),
                                    true, true, true, true);

    orderList.drawElementCallback = DrawListItems; // Delegate to draw the elements on the list
    orderList.drawHeaderCallback = DrawHeader;
  }

  void DrawListItems(Rect rect, int index, bool isActive, bool isFocused)
  {
    SerializedProperty element = orderList.serializedProperty.GetArrayElementAtIndex(index);

    EditorGUI.PropertyField(
      new Rect(rect.x, rect.y, 90, EditorGUIUtility.singleLineHeight),
      element.FindPropertyRelative("key"), GUIContent.none
    );

    EditorGUI.PropertyField(
      new Rect(rect.x + 90, rect.y, rect.width - 90, EditorGUIUtility.singleLineHeight),
      element.FindPropertyRelative("parameter"), GUIContent.none
    );
  }

  void DrawHeader(Rect rect)
  {
    string name = "Parameter";
    EditorGUI.LabelField(rect, name);
  }

  public override void OnInspectorGUI()
  {
    DrawDefaultInspector();
    serializedObject.Update(); // Update the array property's representation in the inspector

    orderList.DoLayoutList(); // Have the ReorderableList do its work

    // We need to call this so that changes on the Inspector are saved by Unity.
    serializedObject.ApplyModifiedProperties();
  }
}
#endif
#endregion
#endif
