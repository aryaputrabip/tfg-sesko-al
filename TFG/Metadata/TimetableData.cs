using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimetableData : MonoBehaviour
{
    public DateTime timeKegiatan;

    public string kegiatanID;
    public string kegiatanDesc;
    public int hariH;
    public int percepatan;
    public List<int> activeOn = new List<int>();
}
