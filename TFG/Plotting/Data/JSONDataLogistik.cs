using UnityEngine;

/// <summary>
/// JSON Data Satuan Hanya Digunakan Untuk Menampung Data Yang Diperlukan DB Ketika Plotting Text (Untuk Mode Editor)
/// </summary>
public class JSONDataLogistik : MonoBehaviour
{
    [Header("Main")]
    public int id;
    public int id_user;
    public string dokumen;
    public string nama;
    public double lat_y;
    public double lng_x;
    public string info_logistik;
    public string isi_logistik;
    public string jenis;
    public string warna;
    public string symbol;
}
