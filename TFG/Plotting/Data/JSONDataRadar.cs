using UnityEngine;

/// <summary>
/// JSON Data Satuan Hanya Digunakan Untuk Menampung Data Yang Diperlukan DB Ketika Plotting Radar (Untuk Mode Editor)
/// </summary>
public class JSONDataRadar : MonoBehaviour
{
    [Header("Main")]
    public int? id;
    public int? id_user;
    public string dokumen;
    public string nama;
    public double lat_y;
    public double lng_x;
    public string info_radar;
    public string symbol;
    public string info_symbol;
}
