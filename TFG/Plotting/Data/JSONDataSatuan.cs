using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Globalization;

using UnityEngine;

/// <summary>
/// JSON Data Satuan Hanya Digunakan Untuk Menampung Data Yang Diperlukan DB Ketika Plotting Satuan (Untuk Mode Editor)
/// </summary>
public class JSONDataSatuan : MonoBehaviour
{
    [Header("Marker")]
    public OnlineMapsMarker3D marker;

    [Header("References")]
    public Sprite icon;

    [Header("Main")]
    public string id;
    public string id_user;
    public string id_symbol;
    public string dokumen;
    public string nama;
    public double lat_y;
    public double lng_x;
    public string id_kegiatan;
    public string symbol;

    [Header("Data Properties")]
    public string style;
    public string info;
    public string isi_logistik;

    public string deskripsi;
}
