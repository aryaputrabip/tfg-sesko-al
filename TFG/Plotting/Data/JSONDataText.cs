using UnityEngine;

/// <summary>
/// JSON Data Satuan Hanya Digunakan Untuk Menampung Data Yang Diperlukan DB Ketika Plotting Text (Untuk Mode Editor)
/// </summary>
public class JSONDataText : MonoBehaviour
{
    [Header("Main")]
    public int id;
    public int id_user;
    public string dokumen;
    public string nama;
    public double lat_y;
    public double lng_x;
    public string info_text;
    public string symbol;
    public float? originZoom;
    public OnlineMapsMarker3D marker;
}
