using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListPasukanData : MonoBehaviour
{
    public enum typeObject { Darat = 1, Laut = 2, Udara = 3 }
    
    [Header("Properties")]
    public string id;
    public string name;

    public string fontFamily;
    public string index;

    public typeObject tipe_obj;
}
