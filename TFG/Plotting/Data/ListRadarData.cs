using UnityEngine;
using HelperEditor;

public class ListRadarData
{
    // Data Untuk Label List
    public string nama_radar;
    public string desc_radar;
    public int index;
    public Font fontFamily;

    // Data Dari SERVICE GET LIST RADAR DISIMPAN DISINI
    public ListInfoRadar info_radar;
}
