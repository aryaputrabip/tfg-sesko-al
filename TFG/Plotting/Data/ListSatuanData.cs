using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using HelperEditor;

public class ListSatuanData
{
    public enum typeObject { Darat = 1, Laut = 2, Udara = 3 }
    public enum jenisObject { Static = 1, Wheeled = 2, Tracked = 3, Surface = 4, Sub = 5, Amphibi = 6, Rotary = 7, Fixed = 8, Space = 9}

    [Header("Properties")]
    public AlutsistaBundleData objPrefab;
    public string obj3D;

    public string id;
    public string name;

    public string nomor;
    public string atasan;

    public string fontFamily;
    public string index;
    public string id_symbol;

    public typeObject tipe_obj;
    public jenisObject jenis_obj;

    public StyleListSatuan styleSatuan;
    public InfoListSatuan infoSatuan;
    public string symbolSatuan;
}
