using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using TMPro;

public class ListSatuanRoute : MonoBehaviour
{
    [Header("Meta")]
    public string IDKegiatan;
    public RouteController cRoute;
    public PlotDataRoute mRoute;
    [SerializeField] private Vector2 startingPostion;

    [Header("UI")]
    public TMP_Text namaRoute;
    public TMP_Text descRoute;
    public TMP_Text distanceRoute;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void toggleLineOn()
    {
        if(mRoute.lines != null)
        {
            if(mRoute.tmpLine != null)
            {
                mRoute.tmpLine.Dispose();
            }

            mRoute.lines.width = 0.25f;
        }
        else
        {
            if (mRoute.tmpLine != null)
            {
                mRoute.tmpLine.Dispose();
            }

            List<Vector2> kck = new List<Vector2>();
            foreach(PlotDataRouteParam pr in mRoute.data)
            {
                kck.Add(new Vector2(pr.coordinate.x, pr.coordinate.y));
            }
            var man = OnlineMapsDrawingElementManager.AddItem(new OnlineMapsDrawingLine(kck, Color.white, 0));
            mRoute.tmpLine = (OnlineMapsDrawingLine)man;
            mRoute.tmpLine.width = 0.25f;
        }
    }

    public void toggleLineOff()
    {
        if (mRoute.lines != null)
        {
            mRoute.lines.width = 0;
        }
        else
        {
            mRoute.tmpLine.Dispose();
        }
    }

    public void navigateToPosition()
    {
        OnlineMaps.instance.position = (Vector2) mRoute.data[0].coordinate;
    }

    public void StartEditRoute()
    {
        cRoute.EditRoute(mRoute);
    }

    public void StartDeleteRoute()
    {
        cRoute.DeleteRoute(mRoute);
    }
}
