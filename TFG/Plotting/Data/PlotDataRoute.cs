using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using UnityEditor;
using System;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
#if UNITY_EDITOR
using UnityEditorInternal;
#endif

/// <summary>
/// Fungsi Metadata Sebagai Preferensi Rute Misi
/// Editor By : Aryaputra BIP
/// </summary>
public class PlotDataRoute : MonoBehaviour
{
    public int ID;

    public string id_satuan;
    public float heading_satuan;

    public string routeID;
    public string routeName;
    public string routeType;
    public string routeDesc;
    public float routeDistance;
    public float routeSpeed;
    public string routeDistanceUnit;
    public float routeTime;
    public string kegiatanID;
    public DateTime routeStart;
    public DateTime routeEnd;
    public List<PlotDataRouteParam> data = new List<PlotDataRouteParam>();
    public OnlineMapsDrawingLine lines;
    public OnlineMapsDrawingLine tmpLine;

    // Buat Save Plot
    public string properties;
    public string id_mission;

    public void AddNewPos(string time, Vector3 coordinate, int index)
    {
        var new_metadata = new PlotDataRouteParam();
        new_metadata.time = time;
        new_metadata.coordinate = coordinate;
        new_metadata.pointIndex = index;

        data.Add(new_metadata);
    }

    /// <summary>
    /// Mendapatkan Parameter Dari Data Berdasarkan Key Yang Di-Select
    /// </summary>
    /// <returns>Key</returns>
    /// <param name="key">Key Yang Dipilih</param>
    public PlotDataRouteParam RunOnTime(string time)
    {
        var timeSearch = DateTime.Parse(time);
        var sel_pointIndex = -1;
        var sel_time = "";
        var sel_coords = new Vector2(0, 0);

        for (int i = 0; i < data.Count; i++)
        {
            if (timeSearch == DateTime.Parse(data[i].time))
            {
                RunOnTimeFound(data[i], i, out sel_pointIndex, out sel_time, out sel_coords);
                i = data.Count;
            }
            else if (timeSearch > DateTime.Parse(data[i].time) && timeSearch < DateTime.Parse(data[i + 3].time))
            {
                RunOnTimeFound(data[i], i, out sel_pointIndex, out sel_time, out sel_coords);
                i = data.Count;
            }
        }

        if (sel_pointIndex != -1)
        {
            var finded = new PlotDataRouteParam();

            finded.pointIndex = sel_pointIndex;
            finded.time = sel_time;
            finded.coordinate = sel_coords;

            return finded;
        }

        //IEnumerable<string> times = from meta in data where meta.time == time select meta.time;
        //IEnumerable<Vector3> coordinate = from meta in data where meta.time == time select meta.coordinate;

        //foreach (string time_select in times)
        //{
        //  foreach (Vector3 coord_select in coordinate)
        //  {
        //    var finded = new PlotDataRouteParam();
        //    finded.time = time_select;
        //    finded.coordinate = coord_select;

        //    return finded;
        //  }
        //}

        return null;
    }

    private void RunOnTimeFound(PlotDataRouteParam data, int index, out int pointIndex, out string time, out Vector2 coordinate)
    {
        pointIndex = index;
        time = data.time;
        coordinate = data.coordinate;

    }

    private void OnDestroy()
    {
        if(data != null) data.Clear();
        if(lines != null) lines.Dispose();
    }

    public void generateProperties()
    {
        List<JObject> jalur = new List<JObject>();
        foreach (PlotDataRouteParam p in data)
        {
            JObject jalurPerRoute = new JObject{
                { "lng", p.coordinate.x },
                { "lat", p.coordinate.y }
            };
            jalur.Add(jalurPerRoute);
        }

        JObject property = new JObject
        {
            { "nama_misi", routeName },
            { "jalur", JToken.FromObject(jalur) },
            { "id_kegiatan", kegiatanID},
            { "kecepatan", routeSpeed},
            { "keterangan", "-"},
            { "type", routeDistanceUnit},
            { "distance", routeDistance},
            { "heading", heading_satuan},
            { "typeMisi", routeType},
            { "idPrimary", ID.ToString()},
            { "jenis", "biasa"}
        };

        properties = JsonConvert.SerializeObject(property);
    }
}

[System.Serializable]
public class PlotDataRouteParam
{
    public string time;
    public Vector3 coordinate;
    public int pointIndex;
}

#region Inspector (Editor)
#if UNITY_EDITOR
[CustomEditor(typeof(PlotDataRoute))]
public class PlotDataRouteEditor : Editor
{
    PlotDataRoute meta;
    ReorderableList orderList;

    private void OnEnable()
    {
        if (target == null) return;
        meta = (PlotDataRoute)target;

        orderList = new ReorderableList(serializedObject, serializedObject.FindProperty("data"),
                                        true, true, true, true);

        orderList.drawElementCallback = DrawListItems; // Delegate to draw the elements on the list
        orderList.drawHeaderCallback = DrawHeader;
    }

    void DrawListItems(Rect rect, int index, bool isActive, bool isFocused)
    {
        SerializedProperty element = orderList.serializedProperty.GetArrayElementAtIndex(index);

        EditorGUI.PropertyField(
          new Rect(rect.x, rect.y, 90, EditorGUIUtility.singleLineHeight),
          element.FindPropertyRelative("time"), GUIContent.none
        );

        EditorGUI.PropertyField(
          new Rect(rect.x + 92, rect.y, rect.width - 92, EditorGUIUtility.singleLineHeight),
          element.FindPropertyRelative("coordinate"), GUIContent.none
        );
    }

    void DrawHeader(Rect rect)
    {
        string name = "Parameter";
        EditorGUI.LabelField(rect, name);
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        serializedObject.Update(); // Update the array property's representation in the inspector

        orderList.DoLayoutList(); // Have the ReorderableList do its work

        // We need to call this so that changes on the Inspector are saved by Unity.
        serializedObject.ApplyModifiedProperties();
    }
}
#endif
#endregion