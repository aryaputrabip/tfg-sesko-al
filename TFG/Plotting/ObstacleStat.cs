using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleStat : MonoBehaviour
{
    public enum warnaType
    {
        Biru,
        Merah
    }
    public string userID;
    public string nama;
    public Vector2 lngLat;
    public string font;
    public warnaType warna;
    public string warnaInString;
    public int size;
    public string info = "TandaTanda";
    public int index;
    public string polygonName;
    public string info_obstacle;
    public string symbol;
    public Vector2[] polygonPoints;
}