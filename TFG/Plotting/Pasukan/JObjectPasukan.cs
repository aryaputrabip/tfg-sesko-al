using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using HelperDataAlutsista;

namespace Plot.Pasukan
{
    [System.Serializable]
    public partial class EntityPasukan
    {
        public int id { get; set; }
        public string id_user { get; set; }
        public string id_symbol { get; set; }
        public string dokumen { get; set; }
        public string nama { get; set; }
        public float lat_y { get; set; }
        public float lng_x { get; set; }
        public string style { get; set; }
        public string info { get; set; }
        public string symbol { get; set; }
        public string id_kegiatan { get; set; }
        public string isi_logistik { get; set; } 
        public static EntityPasukan FromJson(string json) => JsonConvert.DeserializeObject<EntityPasukan>(json, HelperConverter.Converter.Settings);
    }

    [System.Serializable]
    public class Label_Style_Pasukan
    {
        public string label_symbol { get; set; }
        public string margin_bottom { get; set; }
        public string margin_right { get; set; }
        public string margin_top { get; set; }
        public string label_symbol_tab2 { get; set; }
        public string margin_bottom_tab2 { get; set; }
        public string margin_right_tab2 { get; set; }
        public string margin_top_tab2 { get; set; }
        public string label_symbol_tab3 { get; set; }
        public string margin_bottom_tab3 { get; set; }
        public string margin_right_tab3 { get; set; }
        public string margin_top_tab3 { get; set; }
        public string label_symbol_tab4 { get; set; }
        public string margin_bottom_tab4 { get; set; }
        public string margin_right_tab4 { get; set; }
        public string margin_top_tab4 { get; set; }
        public static Label_Style_Pasukan[] FromJson(string json) => JsonConvert.DeserializeObject<Label_Style_Pasukan[]>(json, HelperConverter.Converter.Settings);
    }
    
    [System.Serializable]
    public class Style_Pasukan
    {
        public string nama { get; set; }
        public string index { get; set; }
        public string keterangan { get; set; }
        public int grup { get; set; }
        public string label_style { get; set; }

        public static string ToString(Style_Pasukan json) => JsonConvert.SerializeObject(json);
        public static Style_Pasukan FromJson(string json) => JsonConvert.DeserializeObject<Style_Pasukan>(json, HelperConverter.Converter.Settings);
    }

    [System.Serializable]
    public class Info_Pasukan
    {
        public string kecepatan { get; set; }
        public string nomer_satuan { get; set; }
        public string nama_satuan { get; set; }
        public string nomer_atasan { get; set; }
        public string tgl_selesai { get; set; }
        public string warna { get; set; }
        public int size { get; set; }
        public string weapon { get; set; }
        public List<string> waypoint { get; set; }
        public List<string> list_embarkasi { get; set; }
        public int armor { get; set; }
        public bool id_dislokasi { get; set; }
        public bool id_dislokasi_obj { get; set; }
        public int bahan_bakar { get; set; }
        public int bahan_bakar_load { get; set; }
        public string kecepatan_maks { get; set; }
        public string heading { get; set; }
        public string ket_satuan { get; set; }
        public string nama_icon_satuan { get; set; }
        public string width_icon_satuan { get; set; }
        public string height_icon_satuan { get; set; }
        public int personil { get; set; }
        public bool tfg { get; set; }
        public bool hidewgs { get; set; }

        public static string ToString(Info_Pasukan json) => JsonConvert.SerializeObject(json);
        public static Info_Pasukan FromJson(string json) => JsonConvert.DeserializeObject<Info_Pasukan>(json, HelperConverter.Converter.Settings);
    }
}
