using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Data;
using Npgsql;
using Newtonsoft.Json;

namespace Plot.Pasukan
{
    class PasukanList
    {
        void Start()
        {
            // GrabList();
        }

        public static void GrabList()
        {
            using(NpgsqlConnection conn = new NpgsqlConnection())
            {
                conn.ConnectionString = PlotPasukanController.instance.dataConnection;
                List<string> listName = new List<string>();
                List<string> listMatra = new List<string>();
                List<string> listKeterangan = new List<string>();
                List<string> listFont = new List<string>();
                List<string> listSymbol = new List<string>();
                List<string> listSymbolID = new List<string>();
                
                try
                {
                    NpgsqlCommand cmd = new NpgsqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT a.*, b.* FROM m_pasukan a inner join font_taktis b on a.pasukan_sym_id = b.id order by a.name asc";
                    cmd.Connection = conn;

                    conn.Open();

                    // Debug.Log("Connection Open!");
                    // Console.WriteLine("Connection Open!!");

                    NpgsqlDataReader sdr = cmd.ExecuteReader();

                    while(sdr.Read())
                    {
                        // int id = (int)sdr["id"];
                        string name = sdr["name"].ToString();
                        string namaFont = sdr["nama"].ToString();
                        string keteranganSatuan = sdr["keterangan"].ToString();
                        int index = int.Parse(sdr["index"].ToString());
                        string matra_pasukan = sdr["matra_pasukan"].ToString();
                        int pasukan_sym_id = (int)sdr["pasukan_sym_id"];
                        // float speed_pasukan = (float)sdr["speed_pasukan"];
                        // float health_pasukan = (float)sdr["health_pasukan"];
                        // string icon_wgs = sdr["icon_wgs"].ToString();

                        // Debug.Log(((char)index).ToString());
                        listName.Add(name);
                        listMatra.Add(matra_pasukan);
                        listKeterangan.Add(keteranganSatuan);
                        listFont.Add(namaFont);
                        listSymbol.Add(((char) index).ToString());
                        listSymbolID.Add(pasukan_sym_id.ToString());
                        GetList(listName, listMatra, listKeterangan, listFont, listSymbol, listSymbolID);
                    }
                    
                    conn.Close();
                }
                catch(Exception ex)
                {
                    Debug.Log("Cannot Open Connection!!");
                }
            }
        }

        public static void GetList(List<string> listName, List<string> listMatra, List<string> listKeterangan, List<string> listFont, List<string> listSymbol, List<string> listSymbolID)
        {
            PlotPasukanController pasControl = PlotPasukanController.instance;

            List<string> namaList = pasControl.listNamaPasukan;
            namaList = new List<string>();
            namaList.AddRange(listName);
            pasControl.listNamaPasukan = namaList;
            
            List<string> matraList = pasControl.listMatraPasukan;
            matraList = new List<string>();
            matraList.AddRange(listMatra);
            pasControl.listMatraPasukan = matraList;
            
            List<string> keteranganList = pasControl.listKeteranganPasukan;
            keteranganList = new List<string>();
            keteranganList.AddRange(listKeterangan);
            pasControl.listKeteranganPasukan = keteranganList;

            List<string> fontList = pasControl.listFont;
            fontList = new List<string>();
            fontList.AddRange(listFont);
            pasControl.listFont = fontList;
            
            List<string> symbolList = pasControl.listSymbol;
            symbolList = new List<string>();
            symbolList.AddRange(listSymbol);
            pasControl.listSymbol = symbolList;
            
            List<string> symbolIDList = pasControl.listSymbolID;
            symbolIDList = new List<string>();
            symbolIDList.AddRange(listSymbolID);
            pasControl.listSymbolID = symbolIDList;
        }
    }

    public partial class ListPasukan
    {
        public int id { get; set; }
        public string nomer_satuan { get; set; }
        public string nomer_atasan { get; set; }
        public string name { get; set; }
        public string matra_pasukan { get; set; }
        public string pasukan_sym_id { get; set; }
        public string speed_pasukan { get; set; }
        public string health_pasukan { get; set; }
        public string jumlah_bintara { get; set; }
        public string jumlah_tamtama { get; set; }
        public string jumlah_perwira { get; set; }
        public string icon_wgs { get; set; }
        public int? id_wgs { get; set; } 
        public string style_symbol { get; set; }
        public string deskripsi { get; set; }
        public string titan { get; set; }
        public string nama { get; set; }
        public string index { get; set; }
        public string keterangan { get; set; }        
        
        public static List<ListPasukan> FromJson(string json) => JsonConvert.DeserializeObject<List<ListPasukan>>(json, HelperConverter.Converter.Settings);
    }
}
