using System;
using System.Globalization;
using UnityEngine;

using TMPro;
using HelperEditor;
using System.Collections.Generic;
using GlobalVariables;
using Newtonsoft.Json.Linq;

public class PlotBungusController : MonoBehaviour
{
    public enum plotMode { CREATE, EDIT, ONPLOT, ONMOVE }
    [Header("Plot Mode")]
    public plotMode mode;

    [Header("UI References")]
    public TextMeshProUGUI label_header;
    public TextMeshProUGUI label_btnPlot;
    public GameObject label_notification;
    public SidemenuToggler btnToggler;
    public GameObject edit_tools;
    public DateTime date;

    [Header("Prefab Ref")]
    public GameObject prefabBungus;

    [Header("Form Data")]
    public DateInputHelper dateInput;

    // TEMP DATA STORAGE
    private InfoBungus SELECTED_DATA;
    private OnlineMapsMarker3D SELECTED_MARKER;

    #region INSTANCE
    public static PlotBungusController Instance;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    private void Start()
    {
        OnlineMapsControlBase.instance.OnMapClick += OnMapClick;
        date = DateTime.Now;
    }

    public void SetTime(DateInputHelper dateInput)
    {
        date = dateInput.date;
    }

    #region PLOTTING
    /// <summary>
    /// Reset form ke posisi default
    /// </summary>
    public void ResetForm()
    {
        dateInput.resetTime();

        // Reset Form Label
        label_header.text = "PLOT BUNGUS";
        label_btnPlot.text = "PLOT";
        edit_tools.SetActive(false);

        mode = plotMode.CREATE;

        PlotNotificationHelper.Instance.TogglePlotNotification(false);
    }

    /// <summary>
    /// Ketika Button Plot pada Form Di Klik
    /// (Bertindak sebagai plot object baru jika masuk ke dalam mode create dan save perubahan data object dipilih ketika masuk dalam mode edit)
    /// </summary>
    public void OnButtonPlotClick()
    {
        if (dateInput.inputDay.text == "" || dateInput.inputMonth.text == "" || dateInput.inputYear.text == "" ||
            dateInput.inputHour.text == "" || dateInput.inputMinute.text == "" || dateInput.inputSecond.text == "")
        {
            // Notifikasi Data Form Belum Lengkap
            return;
        }

        // Jika Data Form Sudah Lengkap, Eksekusi Aksi Create / Save Object Aktif
        if (mode == plotMode.EDIT)
        {
            SaveData();
            return;
        }

        GetComponent<Animator>().Play("SlideOutNoTrigger");
        mode = plotMode.ONPLOT;
        PlotNotificationHelper.Instance.TogglePlotNotification(true);
    }

    /// <summary>
    /// Ketika Marker Di Klik
    /// (Bertindak untuk menampilkan form dengan data dari marker tersebut & juga mengubah mode ke dalam mode edit
    /// </summary>
    public void OnMarkerClick(OnlineMapsMarkerBase markerBase)
    {
        // Marker Click Tidak Akan Berpengaruh Jika Plotting Sedang Dalam Mode ONPLOT / ONMOVE
        if (mode == plotMode.ONPLOT || mode == plotMode.ONMOVE) return;
        btnToggler.toggleOn();

        mode = plotMode.EDIT;
        label_header.text = "EDIT BUNGUS";
        label_btnPlot.text = "SAVE";
        edit_tools.SetActive(true);

        OnlineMapsMarker3D marker = markerBase["markerSource"] as OnlineMapsMarker3D;
        var marker_data = marker.instance.GetComponent<JSONDataBungus>();

        SELECTED_DATA = new InfoBungus();
        SELECTED_DATA = InfoBungus.FromJson(marker_data.info_bungus);
        SELECTED_MARKER = marker;

        dateInput.date = DateTime.ParseExact(SELECTED_DATA.date[0] + " " + SELECTED_DATA.date[1], "MM-dd-yyyy HH:mm", CultureInfo.InvariantCulture);
        date = dateInput.date;

        dateInput.inputDay.text = date.ToString("dd");
        dateInput.inputMonth.text = date.ToString("MM");
        dateInput.inputYear.text = date.ToString("yyyy");
        dateInput.inputHour.text = date.ToString("HH");
        dateInput.inputMinute.text = date.ToString("mm");
        dateInput.inputSecond.text = date.ToString("ss");

        dateInput.refreshUI();
    }

    /// <summary>
    /// Event Ketika Click pada Peta
    /// (Bertindak untuk Plot Object)
    /// </summary>
    public async void OnMapClick()
    {
        if (mode != plotMode.ONPLOT) return;

        // Get the coordinates under the cursor.
        double lng, lat;
        OnlineMapsControlBase.instance.GetCoords(out lng, out lat);

        Debug.Log("Plot New Bungus At (" + lng + ", " + lat + ")");

        // Set Form Data Into Data Bungus
        var info_bungus = new InfoBungus();
        var listDate = new List<string>
        {
            date.ToString("MM-dd-yyyy"),
            date.ToString("HH:mm")
        };

        info_bungus.date = listDate;
        info_bungus.ukuran = 30; // Unknown Data, Set To Default

        var warna = (SessionUser.nama_asisten == "ASOPS") ? "blue" : "red";
        var id_bungus = generateIDBungus();

        var symbol = "{\"iconUrl\":\"image/plotting/bungus_red.png\",\"iconSize\":[30,30],\"id_point\":\"" + id_bungus + "\",\"waktu\":[\"" + date.ToString("MM-dd-yyyy") + "\",\"" + date.ToString("HH:mm") + "\"]}";

        var marker = OnlineMapsMarker3DManager.CreateItem(new Vector2((float)lng, (float)lat), AssetPackageManager.Instance.prefabBungus);
        marker.instance.name = id_bungus;
        await EntityEditorController.Instance.SetJSONBungusAndHUD(marker, id_bungus, info_bungus, symbol);

        //await EntityEditorController.Instance.SpawnEntityBungus((float)lng, (float)lat, generateIDBungus(), prefabBungus, info_bungus, symbol);

        ResetForm();
        btnToggler.toggleOff(); 

        PlotNotificationHelper.Instance.TogglePlotNotification(false);
    }

    /// <summary>
    /// Ketika Marker di Klik Dalam Mode OnMove
    /// </summary>
    /// <param name="markerBase"></param>
    private void ToggleDragMarker(OnlineMapsMarkerBase markerBase)
    {
        // Starts moving the marker.
        OnlineMapsControlBase.instance.dragMarker = markerBase;
        OnlineMapsControlBase.instance.isMapDrag = false;
    }

    /// <summary>
    /// Ketika Marker di Drag Dalam Mode OnMove
    /// </summary>
    /// <param name="markerBase"></param>
    private void OnMarkerDrag(OnlineMapsMarkerBase markerBase)
    {
        double lng, lat;
        markerBase.GetPosition(out lng, out lat);

        OnlineMapsMarker3D marker = markerBase["markerSource"] as OnlineMapsMarker3D;
        marker.label = lng + ", " + lat;
        marker.instance.GetComponent<JSONDataBungus>().lng_x = lng;
        marker.instance.GetComponent<JSONDataBungus>().lat_y = lat;
    }

    /// <summary>
    /// Save perubahan data pada object terpilih
    /// </summary>
    public async void SaveData()
    {
        var dataBungus = SELECTED_MARKER.instance.GetComponent<JSONDataBungus>();

        // Set Perubahan Data Bungus Dari Form Data
        SELECTED_DATA.date = new List<string>
        {
            date.ToString("MM-dd-yyyy"),
            date.ToString("HH:mm")
        };

        var warna = (SessionUser.nama_asisten == "ASOPS") ? "blue" : "red";
        var symbol = "{\"iconUrl\":\"image/plotting/bungus_red.png\",\"iconSize\":[30,30],\"id_point\":\"" + dataBungus.nama + "\",\"waktu\":[\"" + date.ToString("MM-dd-yyyy") + "\",\"" + date.ToString("HH:mm") + "\"]}";
        //await EntityEditorController.Instance.SpawnEntityBungusr((float)dataBungus.lng_x, (float)dataBungus.lat_y, dataBungus.nama, prefabBungus, SELECTED_DATA, dataBungus.symbol);

        // Respawn Saved Object
        var marker = OnlineMapsMarker3DManager.CreateItem(new Vector2((float)dataBungus.lng_x, (float)dataBungus.lat_y), AssetPackageManager.Instance.prefabBungus);
        marker.instance.name = dataBungus.nama;
        await EntityEditorController.Instance.SetJSONBungusAndHUD(marker, dataBungus.nama, SELECTED_DATA, dataBungus.symbol);

        // Delete Old Marker
        OnlineMapsMarker3DManager.RemoveItem(SELECTED_MARKER);

        btnToggler.toggleOff();
        ResetForm();
    }

    /// <summary>
    /// Ubah Lokasi Object Terpilih
    /// </summary>
    public void MoveObject()
    {
        if (SELECTED_MARKER == null) return;
        PlotNotificationHelper.Instance.TogglePlotNotification(true, "Drag the object to change position");

        double lng, lat;
        SELECTED_MARKER.GetPosition(out lng, out lat);
        SELECTED_MARKER.label = lng + ", " + lat;

        SELECTED_MARKER.OnPress = ToggleDragMarker;
        SELECTED_MARKER.OnDrag = OnMarkerDrag;
        mode = plotMode.ONMOVE;

        GetComponent<Animator>().Play("SlideOutNoTrigger");
    }

    /// <summary>
    /// Hapus Object Terpilih
    /// </summary>
    public void DeleteObject()
    {
        if (SELECTED_MARKER == null) return;

        EntityEditorController.Instance.plotTrash.Add(
            new JObject()
            {
                { "id", SELECTED_MARKER.instance.name },
                { "type", "bungus" }
            }
        );

        OnlineMapsMarker3DManager.RemoveItem(SELECTED_MARKER);

        Debug.Log("Data Bungus Berhasil Dihapus!");
        btnToggler.toggleOff();
    }
    #endregion

    #region HELPER
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (mode == plotMode.ONPLOT)
            {
                mode = plotMode.CREATE;

                PlotNotificationHelper.Instance.TogglePlotNotification(false);
                GetComponent<Animator>().Play("SlideIn");
            }
            else if (mode == plotMode.ONMOVE)
            {
                mode = plotMode.EDIT;
                SELECTED_MARKER.OnPress = null;
                SELECTED_MARKER.OnDrag = null;
                SELECTED_MARKER.label = "";

                PlotNotificationHelper.Instance.TogglePlotNotification(false);
                GetComponent<Animator>().Play("SlideIn");
            }
        }
    }

    private string generateIDBungus()
    {
        var index_bungus = GameObject.FindGameObjectsWithTag("entity-bungus").Length;
        bool isExist = true;

        do
        {
            isExist = checkBungusIndex(index_bungus);
            index_bungus++;
        } while (isExist);

        return "bungus_" + index_bungus + "_" + SessionUser.id + "_" + UnityEngine.Random.Range(0, 999);
    }

    private bool checkBungusIndex(int index)
    {
        bool isExist = false;

        foreach (GameObject bungus in GameObject.FindGameObjectsWithTag("entity-bungus"))
        {
            if (bungus.name.Contains("bungus_" + index)) isExist = true;
        }

        return isExist;
    }
    #endregion
}
