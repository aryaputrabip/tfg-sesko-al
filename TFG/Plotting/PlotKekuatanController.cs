using UnityEngine;
using UnityEngine.UI;

using TMPro;
using System.Collections.Generic;
using GlobalVariables;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Security.Cryptography;
using HelperPlotting;

public class PlotKekuatanController : MonoBehaviour
{
    [Header("UI References")]
    public GameObject prefabKekuatan;
    public Transform contentData;

    [Header("Data Kekuatan")]
    [SerializeField] private List<string> id_satuan = new List<string>();
    [SerializeField] private List<string> listNamaSatuan = new List<string>();
    [SerializeField] private List<GameObject> listSatuanAktif = new List<GameObject>();
    [SerializeField] private List<string> matraSatuan = new List<string>();
    [SerializeField] private List<string> keteranganSatuan = new List<string>();

    [Header("Image For Marker")]
    public Sprite defaultImage;
    public Sprite angkatanDarat;
    public Sprite angkatanLaut;
    public Sprite angkatanUdara;
    [SerializeField] string pK;

    [Header("Prefab")]
    public GameObject prefab;
    public GameObject listKekuatan;
    [SerializeField] Color color;
    [SerializeField] string warna;
    [HideInInspector] public int index_kekuatan = 0;

    [Header("Canvas")]
    public TMP_InputField nama;
    public TMP_Dropdown pemilikKekuatan;
    public RectTransform pemilikKekuatanUI;
    public Image pemilikKekuatanImage;
    public TMP_InputField keterangan;
    public SidemenuToggler toggler;

    [Header("Edit")]
    public GameObject btnPlot;
    public GameObject btnEdit;
    public GameObject btnMove;
    public GameObject btnDelete;
    [HideInInspector] public OnlineMapsMarker3D editMarker;
    bool isDragging = false;
    bool isMoving = false;
    Vector2 prevWorldPos;
    Vector2 nowWorldPos;

    #region INSTANCE
    public static PlotKekuatanController Instance;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    void Update()
    {
        if (pemilikKekuatan.value == 0)
        {
            pemilikKekuatanImage.sprite = defaultImage;
            pK = "0";
        }
        else if (pemilikKekuatan.value == 1)
        {
            pemilikKekuatanImage.sprite = angkatanDarat;
            pK = "AD";
        }
        else if (pemilikKekuatan.value == 2)
        {
            pemilikKekuatanImage.sprite = angkatanLaut;
            pK = "AL";
        }
        else if (pemilikKekuatan.value == 3)
        {
            pemilikKekuatanImage.sprite = angkatanUdara;
            pK = "AU";
        }

        if (pemilikKekuatan.value == 0)
        {
            pemilikKekuatanUI.sizeDelta = new Vector2(320f, 25f);
            pemilikKekuatanImage.enabled = false;
        }
        else
        {
            pemilikKekuatanUI.sizeDelta = new Vector2(320f, 55f);
            pemilikKekuatanImage.enabled = true;
        }

        if (SessionUser.nama_asisten == "ASOPS")
        {
            color = Color.blue;
            warna = "blue";
        }
        else if (SessionUser.nama_asisten == "ASINTEL")
        {
            color = Color.red;
            warna = "red";
        }
        else
        {
            color = Color.blue;
            warna = "";
        }

        Move();
    }

    public void OnSatuanSelected(PlotSatuanScrollerData objData, PlotSatuanCellView cellData)
    {
        // Data tidak bisa duplicate, Tambah value input jika sudah ada
        if (listNamaSatuan.Contains(objData.nama_satuan))
        {
            var index = listNamaSatuan.IndexOf(objData.nama_satuan);
            var input = listSatuanAktif[index].GetComponent<Metadata>().FindParameter("input-jumlah").parameter.GetComponent<TMP_InputField>();
            input.text = (int.Parse(input.text) + 1).ToString();

            return;
        }

        var newList = Instantiate(prefabKekuatan, contentData);
        var metadata = newList.GetComponent<Metadata>();

        metadata.FindParameter("nama-satuan").parameter.GetComponent<TextMeshProUGUI>().text = objData.nama_satuan;
        metadata.FindParameter("icon-satuan").parameter.GetComponent<Image>().sprite = objData.image_satuan;
        metadata.FindParameter("simbol-satuan").parameter.GetComponent<Text>().text = objData.index;
        metadata.FindParameter("simbol-satuan").parameter.GetComponent<Text>().font = objData.fontFamily;

        id_satuan.Add(objData.id_satuan);
        listNamaSatuan.Add(objData.nama_satuan);
        listSatuanAktif.Add(newList);
        matraSatuan.Add(objData.matra_satuan);
        keteranganSatuan.Add(objData.desc_satuan);

        metadata.FindParameter("btn-remove").parameter.GetComponent<Button>().onClick.AddListener(delegate { RemoveSatuan(newList, listSatuanAktif.Count - 1); });
    }

    public void RemoveSatuan(GameObject list, int index)
    {
        id_satuan.RemoveAt(index);
        listNamaSatuan.RemoveAt(index);
        listSatuanAktif.RemoveAt(index);
        matraSatuan.RemoveAt(index);
        keteranganSatuan.RemoveAt(index);

        Destroy(list);
    }

    public void ResetForm()
    {
        nama.text = "";
        pemilikKekuatan.value = 0;
        keterangan.text = "";

        // for(int i = 0; i < listSatuanAktif.Count; i++)
        // RemoveSatuan(listSatuanAktif[i], listSatuanAktif.Count - 1);

        foreach (GameObject child in listSatuanAktif)
            Destroy(child);

        id_satuan.Clear();
        listNamaSatuan.Clear();
        listSatuanAktif.Clear();
        matraSatuan.Clear();
        keteranganSatuan.Clear();

        btnPlot.SetActive(true);
        btnEdit.SetActive(false);
        btnMove.SetActive(false);
        btnDelete.SetActive(false);

        isMoving = false;

        if (contentData.childCount > 0)
            for (int i = 0; i < contentData.childCount; i++)
                Destroy(contentData.GetChild(i).gameObject);

        listNamaSatuan.Clear();
        listSatuanAktif.Clear();
        matraSatuan.Clear();
    }

    public void Plot()
    {
        if (nama.text == "")
            Debug.Log("Nama Tidak Boleh Kosong!!!");
        else if (keterangan.text == "")
            Debug.Log("Keterangan Tidak Boleh Kosong!!!");
        else if (contentData.childCount == 0)
            Debug.Log("Anda Belum Memilih Unit!!!");
        else
        {
            OnlineMapsControlBase.instance.OnMapClick += OnMapClick;
            toggler.toggleOff();
        }
    }

    public void OnMapClick()
    {
        List<JObject> jStringList = new List<JObject>();
        double lng, lat;
        if (OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
        {
            OnlineMapsMarker3D marker3D = OnlineMapsMarker3DManager.instance.Create(lng, lat, prefab);
            marker3D["markerSource"] = marker3D;
            marker3D.label = generateIDkekuatan();
            marker3D.OnClick += OnMarkerClick;
            marker3D.OnPress += OnMarkerPress;
            marker3D.OnRelease += OnMarkerRelease;
            marker3D.instance.name = marker3D.label;
            
            var _kekuatan = GameObject.Find("KekuatanMarker");
            if(_kekuatan == null)
            {
                _kekuatan = new GameObject("KekuatanMarker");
                _kekuatan.transform.parent = OnlineMaps.instance.transform;
                _kekuatan.transform.localPosition = Vector3.zero;
                _kekuatan.transform.localRotation = Quaternion.identity;
                _kekuatan.transform.localScale = Vector3.one;
            }
            Debug.Log(_kekuatan);
            marker3D.instance.transform.parent = _kekuatan.transform;

            GameObject objInstance = marker3D.instance;
            Metadata metadata = objInstance.GetComponent<Metadata>();
            metadata.FindParameter("marker").parameter.GetComponent<Image>().sprite = pemilikKekuatanImage.sprite;
            metadata.FindParameter("marker").parameter.GetComponent<Image>().color = Color.white;
            metadata.FindParameter("garis").parameter.GetComponent<Image>().color = color;
            metadata.FindParameter("background").parameter.GetComponent<Image>().color = color;
            metadata.FindParameter("nama").parameter.GetComponent<TMP_Text>().color = color;
            metadata.FindParameter("nama").parameter.GetComponent<TMP_Text>().text = nama.text;
            metadata.FindParameter("keterangan").parameter.GetComponent<TMP_Text>().color = color;
            metadata.FindParameter("keterangan").parameter.GetComponent<TMP_Text>().text = keterangan.text;

            if (pemilikKekuatan.value == 0)
            {
                metadata.FindParameter("marker").parameter.GetComponent<Image>().color = color;
            }

            KekuatanStat kS = objInstance.GetComponent<KekuatanStat>();
            kS.nama = nama.text;
            kS.namaSatuan = marker3D.label;
            kS.warna = warna;
            kS.pemilikKekuatan = pK;
            kS.keterangan = metadata.FindParameter("keterangan").parameter.GetComponent<TMP_Text>().text;
            kS.lat_y = lat;
            kS.lng_x = lng;

            foreach (OnlineMapsMarker3D child in OnlineMapsMarker3DManager.instance)
                if (child == marker3D)
                    kS.marker3D = child;

            Transform contentList = metadata.FindParameter("content").parameter.transform;

            for (int i = 0; i < listSatuanAktif.Count; i++)
            {
                Metadata meta = listSatuanAktif[i].GetComponent<Metadata>();
                TextMeshProUGUI nSatuan = meta.FindParameter("nama-satuan").parameter.GetComponent<TextMeshProUGUI>();
                Image iSatuan = meta.FindParameter("icon-satuan").parameter.GetComponent<Image>();
                Text tSatuan = meta.FindParameter("simbol-satuan").parameter.GetComponent<Text>();
                TMP_InputField inputSatuan = meta.FindParameter("input-jumlah").parameter.GetComponent<TMP_InputField>();

                GameObject copy = Instantiate(listKekuatan, contentList);  
                var _meta = copy.GetComponent<Metadata>();              
                _meta.FindParameter("No.").parameter.GetComponent<TMP_Text>().text = (i + 1).ToString();
                _meta.FindParameter("No.").parameter.GetComponent<TMP_Text>().color = color;
                _meta.FindParameter("Icon").parameter.GetComponent<Image>().sprite = iSatuan.sprite;
                _meta.FindParameter("Nama").parameter.GetComponent<TMP_Text>().text = nSatuan.text;
                _meta.FindParameter("Nama").parameter.GetComponent<TMP_Text>().color = color;
                _meta.FindParameter("Jumlah Num").parameter.GetComponent<TMP_Text>().text = inputSatuan.text;
                _meta.FindParameter("Jumlah Num").parameter.GetComponent<TMP_Text>().color = color;
                _meta.FindParameter("Simbol").parameter.GetComponent<Text>().text = tSatuan.text;
                _meta.FindParameter("Simbol").parameter.GetComponent<Text>().font = Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + tSatuan.font);
                _meta.FindParameter("Simbol").parameter.GetComponent<Text>().color = color;

                if (i > 2)
                {
                    RectTransform bkgrnd = metadata.FindParameter("background").parameter.GetComponent<RectTransform>();
                    bkgrnd.sizeDelta = new Vector2(bkgrnd.sizeDelta.x, bkgrnd.sizeDelta.y + 170f);
                    bkgrnd.localPosition = Vector3.zero;

                    RectTransform ktrng = metadata.FindParameter("keterangan-parent").parameter.GetComponent<RectTransform>();
                    ktrng.sizeDelta = new Vector2(ktrng.sizeDelta.x, ktrng.sizeDelta.y + 170f);

                    RectTransform cntnt = metadata.FindParameter("content").parameter.GetComponent<RectTransform>();
                    cntnt.sizeDelta = new Vector2(cntnt.sizeDelta.x, cntnt.sizeDelta.y + 170f);
                }

                int m_matra = 0;
                string jenisSatuanKekuatan = "";

                if (matraSatuan[i] == "Darat")
                {
                    m_matra = 1;
                    jenisSatuanKekuatan = "kekuatan_AD";
                }
                else if (matraSatuan[i] == "Laut")
                {
                    m_matra = 2;
                    jenisSatuanKekuatan = "kekuatan_AL";
                }
                else if (matraSatuan[i] == "Udara")
                {
                    m_matra = 3;
                    jenisSatuanKekuatan = "kekuatan_AU";
                }

                char simbolIndex = char.Parse(tSatuan.text);
                int sIndex = (int)simbolIndex;

                JObject fontJSON = new JObject
                {
                    { "entity_kategori", "" },
                    { "entity_name", "" },
                    { "grup", m_matra },
                    { "health", null },
                    { "id", SessionUser.id },
                    { "index", sIndex },
                    { "jenis_role", "" },
                    { "keterangan", keteranganSatuan[i] },
                    { "nama", tSatuan.font.name }
                };


                JObject infoKekuatan = new JObject
                {
                    { "font", fontJSON },
                    { "jumlah", inputSatuan.text },
                    { "jenis", jenisSatuanKekuatan },
                    { "id_symbol", id_satuan[i] },
                    { "nama", nSatuan.text }
                };

                //string jString = JsonConvert.SerializeObject(infoKekuatan);

                jStringList.Add(infoKekuatan);
                kS.satuanList.Add(copy);
                kS.matraSatuan.Add(matraSatuan[i]);
                // Debug.Log(tSatuan.font.ToString());
            }

            JObject property = new JObject
            {
                { "distance", "20" },
                { "degree", "45" },
                { "width", "0" },
                { "heigth", "100" }
            };

            JObject rincian = new JObject
            {
                { "nama_kekuatan", kS.nama },
                { "warna", kS.warna },
                { "ket", kS.keterangan },
                { "property", JsonConvert.SerializeObject(property) },
                { "milik", kS.pemilikKekuatan }
            };

            kS.rincian = JsonConvert.SerializeObject(rincian);
            kS.infoKekuatan = JsonConvert.SerializeObject(JToken.FromObject(jStringList));

            if (pemilikKekuatan.value == 0)
                metadata.FindParameter("marker").parameter.GetComponent<Image>().color = color;

            objInstance.tag = "Plot_Kekuatan";

            index_kekuatan++;
            ResetForm();
            OnlineMapsControlBase.instance.OnMapClick -= OnMapClick;
        }
    }

    public string ListToText(List<string> list)
    {
        string result = "";
        foreach (var listMember in list)
        {
            if (listMember != list[list.Count - 1])
                result += listMember + ", ";
            else
                result += listMember;
        }
        return result;
    }

    public void OnMarkerClick(OnlineMapsMarkerBase markerBase)
    {
        toggler.toggleOn();

        OnlineMapsMarker3D marker3D = markerBase as OnlineMapsMarker3D;
        KekuatanStat kS = marker3D.instance.GetComponent<KekuatanStat>();
        editMarker = marker3D;

        nama.text = kS.nama;
        keterangan.text = kS.keterangan;

        if (kS.pemilikKekuatan == "AD")
            pemilikKekuatan.value = 1;
        else if (kS.pemilikKekuatan == "AL")
            pemilikKekuatan.value = 2;
        else if (kS.pemilikKekuatan == "AU")
            pemilikKekuatan.value = 3;
        else
            pemilikKekuatan.value = 0;

        for (int i = 0; i < kS.satuanList.Count; i++)
        {   
            var newList = Instantiate(prefabKekuatan, contentData);
            var _meta = kS.satuanList[i].GetComponent<Metadata>();
            string namaS = _meta.FindParameter("Nama").parameter.GetComponent<TMP_Text>().text;
            Sprite iconS = _meta.FindParameter("Icon").parameter.GetComponent<Image>().sprite;
            string jumlahS = _meta.FindParameter("Jumlah Num").parameter.GetComponent<TMP_Text>().text;
            Text simbolS = _meta.FindParameter("Simbol").parameter.GetComponent<Text>();

            var metadata = newList.GetComponent<Metadata>();

            metadata.FindParameter("nama-satuan").parameter.GetComponent<TextMeshProUGUI>().text = namaS;
            metadata.FindParameter("icon-satuan").parameter.GetComponent<Image>().sprite = iconS;
            metadata.FindParameter("input-jumlah").parameter.GetComponent<TMP_InputField>().text = jumlahS;
            metadata.FindParameter("simbol-satuan").parameter.GetComponent<Text>().text = simbolS.text;
            metadata.FindParameter("simbol-satuan").parameter.GetComponent<Text>().font = simbolS.font;

            listNamaSatuan.Add(namaS);
            listSatuanAktif.Add(newList);
            matraSatuan.Add(kS.matraSatuan[i]);

            metadata.FindParameter("btn-remove").parameter.GetComponent<Button>().onClick.AddListener(delegate { RemoveSatuan(newList, listSatuanAktif.Count - 1); });
        }

        btnPlot.SetActive(false);
        btnEdit.SetActive(true);
        btnMove.SetActive(true);
        btnDelete.SetActive(true);
    }

    public void OnMarkerPress(OnlineMapsMarkerBase markerBase)
    {
        if (isMoving)
        {
            isDragging = true;
            OnlineMaps.instance.GetComponent<OnlineMapsTileSetControl>().allowUserControl = false;

            double lng, lat;
            if (OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
                prevWorldPos = new Vector2((float)lng, (float)lat);
        }
    }

    public void OnMarkerRelease(OnlineMapsMarkerBase markerBase)
    {
        if (isMoving)
        {
            isDragging = false;
            OnlineMaps.instance.GetComponent<OnlineMapsTileSetControl>().allowUserControl = true;
        }
    }

    public void Edit()
    {
        GameObject objEdit = editMarker.instance;

        Metadata metadata = objEdit.GetComponent<Metadata>();
        metadata.FindParameter("marker").parameter.GetComponent<Image>().sprite = pemilikKekuatanImage.sprite;
        metadata.FindParameter("garis").parameter.GetComponent<Image>().color = color;
        metadata.FindParameter("background").parameter.GetComponent<Image>().color = color;
        metadata.FindParameter("nama").parameter.GetComponent<TMP_Text>().color = color;
        metadata.FindParameter("nama").parameter.GetComponent<TMP_Text>().text = nama.text;
        metadata.FindParameter("keterangan").parameter.GetComponent<TMP_Text>().color = color;
        metadata.FindParameter("keterangan").parameter.GetComponent<TMP_Text>().text = keterangan.text;

        if(pemilikKekuatan.value != 0)
            metadata.FindParameter("marker").parameter.GetComponent<Image>().color = Color.white;
        else
            metadata.FindParameter("marker").parameter.GetComponent<Image>().color = color;

        KekuatanStat kS = objEdit.GetComponent<KekuatanStat>();

        kS.nama = nama.text;
        kS.pemilikKekuatan = pK;
        kS.keterangan = keterangan.text;

        foreach (GameObject child in kS.satuanList)
            Destroy(child);

        kS.satuanList.Clear();

        Transform contentList = metadata.FindParameter("content").parameter.transform;

        for (int i = 0; i < listSatuanAktif.Count; i++)
        {
            Metadata meta = listSatuanAktif[i].GetComponent<Metadata>();
            TextMeshProUGUI nSatuan = meta.FindParameter("nama-satuan").parameter.GetComponent<TextMeshProUGUI>();
            Image iSatuan = meta.FindParameter("icon-satuan").parameter.GetComponent<Image>();
            Text tSatuan = meta.FindParameter("simbol-satuan").parameter.GetComponent<Text>();
            TMP_InputField inputSatuan = meta.FindParameter("input-jumlah").parameter.GetComponent<TMP_InputField>();

            GameObject copy = Instantiate(listKekuatan, contentList);
            copy.transform.Find("No.").GetComponent<TMP_Text>().text = (i + 1).ToString();
            copy.transform.Find("Icon").GetComponent<Image>().sprite = iSatuan.sprite;
            copy.transform.Find("Nama").GetComponent<TMP_Text>().text = nSatuan.text;
            copy.transform.Find("Jumlah Num").GetComponent<TMP_Text>().text = inputSatuan.text;
            copy.transform.Find("Simbol").GetComponent<Text>().text = tSatuan.text;
            copy.transform.Find("Simbol").GetComponent<Text>().font = tSatuan.font;

            if (i > 2)
            {
                RectTransform bkgrnd = metadata.FindParameter("background").parameter.GetComponent<RectTransform>();
                bkgrnd.sizeDelta = new Vector2(bkgrnd.sizeDelta.x, bkgrnd.sizeDelta.y + 170f);
                bkgrnd.localPosition = Vector3.zero;

                RectTransform ktrng = metadata.FindParameter("keterangan-parent").parameter.GetComponent<RectTransform>();
                ktrng.sizeDelta = new Vector2(ktrng.sizeDelta.x, ktrng.sizeDelta.y + 170f);

                RectTransform cntnt = metadata.FindParameter("content").parameter.GetComponent<RectTransform>();
                cntnt.sizeDelta = new Vector2(cntnt.sizeDelta.x, cntnt.sizeDelta.y + 170f);
            }
            else
            {
                RectTransform bkgrnd = metadata.FindParameter("background").parameter.GetComponent<RectTransform>();
                bkgrnd.sizeDelta = new Vector2(bkgrnd.sizeDelta.x, 710f);

                RectTransform ktrng = metadata.FindParameter("keterangan-parent").parameter.GetComponent<RectTransform>();
                ktrng.sizeDelta = new Vector2(ktrng.sizeDelta.x, 530f);

                RectTransform cntnt = metadata.FindParameter("content").parameter.GetComponent<RectTransform>();
                cntnt.sizeDelta = new Vector2(cntnt.sizeDelta.x, 530f);
            }

            kS.satuanList.Add(copy);
            kS.matraSatuan.Add(matraSatuan[i]);
        }

        if (pemilikKekuatan.value == 0)
            metadata.FindParameter("marker").parameter.GetComponent<Image>().color = color;

        ResetForm();
        toggler.toggleOff();
    }

    public void MoveOn()
    {
        isMoving = !isMoving;
    }

    public void Move()
    {
        if (isDragging && isMoving)
        {
            double lng, lat;
            if (OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
                nowWorldPos = new Vector2((float)lng, (float)lat);

            Vector2 diffPos = nowWorldPos - prevWorldPos;

            editMarker.position += diffPos;
            
            var kS = editMarker.instance.GetComponent<KekuatanStat>();
            kS.lat_y = lat;
            kS.lng_x = lng; 

            prevWorldPos = nowWorldPos;
        }
    }

    public void Delete()
    {
        var trash = GameObject.Find("Trash");
        if(trash == null)
        {
            trash = new GameObject("Trash");
            trash.AddComponent<TrashObj>();
        }

        var tObj = new Trash();
        tObj.id = editMarker.instance.name;
        tObj.type = "kekuatan";

        var tScript = trash.GetComponent<TrashObj>();
        tScript.trash.Add(tObj);

        EntityEditorController.Instance.plotTrash.Add(
            new JObject()
            {
                { "id", editMarker.instance.name },
                { "type", "kekuatan" }
            }
        );

        OnlineMapsMarker3DManager.RemoveItem(editMarker);

        ResetForm();
        toggler.toggleOff();
    }

    private string generateIDkekuatan()
    {
        return "kekuatan_" + index_kekuatan + "_" + SessionUser.id + "_" + Random.Range(0, 999);
    }

    private bool checkKekuatanIndex(int index)
    {
        bool isExist = false;

        foreach (GameObject kekuatan in GameObject.FindGameObjectsWithTag("Plot_Kekuatan"))
        {
            if (kekuatan.name.Contains("kekuatan_" + index)) isExist = true;
        }

        return isExist;
    }
}