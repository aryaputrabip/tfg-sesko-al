using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;
using HelperEditor;
using GlobalVariables;
using UnityEngine.UI;
using System.Linq;

public class PlotLogistikController : MonoBehaviour
{
    public enum plotMode { CREATE, EDIT, ONPLOT, ONMOVE }
    [Header("Plot Mode")]
    public plotMode mode;

    [Header("UI References")]
    public TextMeshProUGUI label_header;
    public TextMeshProUGUI label_btnPlot;
    public GameObject label_notification;
    public SidemenuToggler btnToggler;
    public GameObject edit_tools;
    public Transform container_logistik;
    public List<ListLogistikData> LogData;

    [Header("Prefab Ref")]
    public GameObject[] prefabLogistik;
    public GameObject prefabList;

    [Header("Form Data")]
    public TMP_InputField inputNama;
    public TMP_InputField inputLogNama;
    public TMP_InputField inputLogJumlah;
    public TMP_Dropdown DDJenis;

    // TEMP DATA STORAGE
    private List<IsiLogistik> SELECTED_DATA;
    private InfoLogistik SELECTED_DATA_INFO;
    private OnlineMapsMarker3D SELECTED_MARKER;

    #region INSTANCE
    public static PlotLogistikController Instance;
    void Awake()
    {
        if (Instance == null)
        {
            LogData = new List<ListLogistikData>();
            OnlineMapsControlBase.instance.OnMapClick += OnMapClick;
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    #region PLOTTING
    /// <summary>
    /// Reset form ke posisi default
    /// </summary>
    public void ResetForm()
    {
        // Reset Form Data
        inputNama.text = "";
        inputLogNama.text = "";
        inputLogJumlah.text = "";
        DDJenis.value = 0;

        for (int i = 0; i < LogData.Count; i++)
        {
            Destroy(LogData[i].gameObject);
        }
        LogData = new List<ListLogistikData>();

        // Reset Form Label
        label_header.text = "PLOT LOGISTIK";
        label_btnPlot.text = "PLOT";
        edit_tools.SetActive(false);

        mode = plotMode.CREATE;
        PlotNotificationHelper.Instance.TogglePlotNotification(false);
    }

    public void AddItemLogistik()
    {
        if (inputLogNama.text == "" || inputLogJumlah.text == "") return;

        var new_item = Instantiate(prefabList, container_logistik).GetComponent<ListLogistikData>();
        new_item.judul = inputLogNama.name;
        new_item.isi = inputLogJumlah.text;

        var isi_logistik = new_item.isiLogistik = new IsiLogistik();
        isi_logistik.judul = inputLogNama.text;
        isi_logistik.isi = inputLogJumlah.text;

        new_item.gameObject.GetComponent<Metadata>().FindParameter("label-judul").parameter.GetComponent<TextMeshProUGUI>().text = inputLogNama.text;
        new_item.gameObject.GetComponent<Metadata>().FindParameter("label-isi").parameter.GetComponent<TextMeshProUGUI>().text = inputLogJumlah.text;
        new_item.gameObject.GetComponent<Metadata>().FindParameter("btn-delete").parameter.GetComponent<Button>()
            .onClick.AddListener(delegate { RemoveItemLogistik(new_item); });

        LogData.Add(new_item);

        inputLogNama.text = "";
        inputLogJumlah.text = "";
    }

    public void RemoveItemLogistik(ListLogistikData item)
    {
        LogData.Remove(item);
        Destroy(item.gameObject);
    }

    /// <summary>
    /// Ketika Button Plot pada Form Di Klik
    /// (Bertindak sebagai plot object baru jika masuk ke dalam mode create dan save perubahan data object dipilih ketika masuk dalam mode edit)
    /// </summary>
    public void OnButtonPlotClick()
    {
        if (inputNama.text == "" || LogData.Count <= 0)
        {
            // Notifikasi Data Form Belum Lengkap
            return;
        }

        // Jika Data Form Sudah Lengkap, Eksekusi Aksi Create / Save Object Aktif
        if (mode == plotMode.EDIT)
        {
            SaveData();
            return;
        }

        GetComponent<Animator>().Play("SlideOutNoTrigger");
        mode = plotMode.ONPLOT;
        PlotNotificationHelper.Instance.TogglePlotNotification(true);
    }

    /// <summary>
    /// Ketika Marker Di Klik
    /// (Bertindak untuk menampilkan form dengan data dari marker tersebut & juga mengubah mode ke dalam mode edit
    /// </summary>
    public void OnMarkerClick(OnlineMapsMarkerBase markerBase)
    {
        // Marker Click Tidak Akan Berpengaruh Jika Plotting Sedang Dalam Mode ONPLOT / ONMOVE
        if (mode == plotMode.ONPLOT || mode == plotMode.ONMOVE) return;
        btnToggler.toggleOn();

        mode = plotMode.EDIT;
        label_header.text = "EDIT LOGISTIK";
        label_btnPlot.text = "SAVE";
        edit_tools.SetActive(true);

        OnlineMapsMarker3D marker = markerBase["markerSource"] as OnlineMapsMarker3D;
        var marker_data = marker.instance.GetComponent<JSONDataLogistik>();

        SELECTED_DATA = new List<IsiLogistik>();
        SELECTED_DATA = IsiLogistik.FromJson(marker_data.isi_logistik);
        SELECTED_MARKER = marker;

        var info_logistik = InfoLogistik.FromJson(marker_data.info_logistik);
        SELECTED_DATA_INFO = info_logistik;

        inputNama.text = info_logistik.nama;

        var DDJenisStrings = DDJenis.options.Select(option => option.text).ToList();
        DDJenis.value = DDJenisStrings.IndexOf(marker_data.warna);

        for (int i = 0; i < SELECTED_DATA.Count; i++)
        {
            inputLogNama.text = SELECTED_DATA[i].judul;
            inputLogJumlah.text = SELECTED_DATA[i].isi;

            AddItemLogistik();
        }

        SELECTED_MARKER = marker;
    }

    /// <summary>
    /// Event Ketika Click pada Peta
    /// (Bertindak untuk Plot Object)
    /// </summary>
    public async void OnMapClick()
    {
        if (mode != plotMode.ONPLOT) return;

        // Get the coordinates under the cursor.
        double lng, lat;
        OnlineMapsControlBase.instance.GetCoords(out lng, out lat);

        Debug.Log("Plot New Logistik At (" + lng + ", " + lat + ")");

        // Set Form Data Into Data Logistik
        var info_logistik = new InfoLogistik();
        info_logistik.nama = inputNama.text;

        var isi = new List<IsiLogistik>();
        for (int i = 0; i < LogData.Count; i++)
        {
            isi.Add(LogData[i].isiLogistik);
        }

        var jenis = DDJenis.options[DDJenis.value].text;
        var warna = (SessionUser.nama_asisten == "ASOPS") ? "blue" : "red";
        var symbol = "";
        var prefabLog = prefabLogistik[DDJenis.value];
        var nama = generateIDLogistik();

        await EntityEditorController.Instance.SpawnEntityLogistik((float)lng, (float)lat, prefabLog, info_logistik, isi, jenis, warna, symbol, nama);

        ResetForm();
        btnToggler.toggleOff();

        PlotNotificationHelper.Instance.TogglePlotNotification(false);
    }

    /// <summary>
    /// Ketika Marker di Klik Dalam Mode OnMove
    /// </summary>
    /// <param name="markerBase"></param>
    private void ToggleDragMarker(OnlineMapsMarkerBase markerBase)
    {
        // Starts moving the marker.
        OnlineMapsControlBase.instance.dragMarker = markerBase;
        OnlineMapsControlBase.instance.isMapDrag = false;
    }

    /// <summary>
    /// Ketika Marker di Drag Dalam Mode OnMove
    /// </summary>
    /// <param name="markerBase"></param>
    private void OnMarkerDrag(OnlineMapsMarkerBase markerBase)
    {
        double lng, lat;
        markerBase.GetPosition(out lng, out lat);

        OnlineMapsMarker3D marker = markerBase["markerSource"] as OnlineMapsMarker3D;
        marker.label = lng + ", " + lat;
        marker.instance.GetComponent<JSONDataLogistik>().lng_x = lng;
        marker.instance.GetComponent<JSONDataLogistik>().lat_y = lat;
    }

    /// <summary>
    /// Save perubahan data pada object terpilih
    /// </summary>
    public async void SaveData()
    {
        var dataLogistik = SELECTED_MARKER.instance.GetComponent<JSONDataLogistik>();

        // Set Perubahan Data Logistik Dari Form Data
        SELECTED_DATA_INFO.nama = inputNama.text;

        var jenis = DDJenis.options[DDJenis.value].text;
        var prefabLog = prefabLogistik[DDJenis.value];

        var isi = new List<IsiLogistik>();
        for (int i = 0; i < LogData.Count; i++)
        {
            isi.Add(LogData[i].isiLogistik);
        }

        await EntityEditorController.Instance.SpawnEntityLogistik((float)dataLogistik.lng_x, (float)dataLogistik.lat_y, prefabLog, SELECTED_DATA_INFO, isi, jenis, dataLogistik.warna, dataLogistik.symbol);

        // Delete Old Marker
        OnlineMapsMarker3DManager.RemoveItem(SELECTED_MARKER);

        btnToggler.toggleOff();
        ResetForm();
    }

    /// <summary>
    /// Ubah Lokasi Object Terpilih
    /// </summary>
    public void MoveObject()
    {
        if (SELECTED_MARKER == null) return;
        PlotNotificationHelper.Instance.TogglePlotNotification(true, "Drag the object to change position");

        double lng, lat;
        SELECTED_MARKER.GetPosition(out lng, out lat);
        SELECTED_MARKER.label = lng + ", " + lat;

        SELECTED_MARKER.OnPress = ToggleDragMarker;
        SELECTED_MARKER.OnDrag = OnMarkerDrag;
        mode = plotMode.ONMOVE;

        GetComponent<Animator>().Play("SlideOutNoTrigger");
    }

    /// <summary>
    /// Hapus Object Terpilih
    /// </summary>
    public void DeleteObject()
    {
        if (SELECTED_MARKER == null) return;
        OnlineMapsMarker3DManager.RemoveItem(SELECTED_MARKER);

        Debug.Log("Data Logistik Berhasil Dihapus!");
        btnToggler.toggleOff();
    }
    #endregion

    #region HELPER
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (mode == plotMode.ONPLOT)
            {
                mode = plotMode.CREATE;

                PlotNotificationHelper.Instance.TogglePlotNotification(false);
                GetComponent<Animator>().Play("SlideIn");
            }
            else if (mode == plotMode.ONMOVE)
            {
                mode = plotMode.EDIT;
                SELECTED_MARKER.OnPress = null;
                SELECTED_MARKER.OnDrag = null;
                SELECTED_MARKER.label = "";

                PlotNotificationHelper.Instance.TogglePlotNotification(false);
                GetComponent<Animator>().Play("SlideIn");
            }
        }
    }

    private string generateIDLogistik()
    {
        var index_logistik = GameObject.FindGameObjectsWithTag("entity-logistik").Length;
        bool isExist = true;
        do
        {
            isExist = checkLogistikIndex(index_logistik);
            index_logistik++;
        } while (isExist);
        return "logistik_" + index_logistik + "_" + SessionUser.id + "_" + Random.Range(0, 999);
    }

    private bool checkLogistikIndex(int index)
    {
        bool isExist = false;
        foreach (GameObject satuan in GameObject.FindGameObjectsWithTag("entity-logistik"))
        {
            if (satuan.name.Contains("logistik_" + index)) isExist = true;
        }
        return isExist;
    }
    #endregion
}
