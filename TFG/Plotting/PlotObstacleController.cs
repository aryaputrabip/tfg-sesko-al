using System.Collections.Generic;
using UnityEngine;

using TMPro;
using UnityEngine.UI;
using GlobalVariables;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using HelperPlotting;

public class PlotObstacleController : MonoBehaviour
{
    [Header("Online Maps")]
    public OnlineMaps map;
    public OnlineMapsMarker3DManager manager3D;
    List<OnlineMapsMarker3D> markers;
    List<OnlineMapsMarker3D> markerList;

    [Header("References")]
    public GameObject prefabList;
    public Transform listObstacle;
    public GameObject selectedObstacleGroup;

    [Header("Obstacle Data")]
    public GameObject prefab;
    public string fontObstacle;
    public List<string> obstacles = new List<string>();
    int totalObs;

    [Header("Form Data")]
    public GameObject canvasForm;
    public TMP_InputField input_jumlah;
    public TMP_InputField input_size;
    public TMP_Dropdown dropdownWarna;
    public GameObject confirmMenu;
    Color warna;
    int size = 5;

    [Header("Form Edit")]
    [HideInInspector] public OnlineMapsMarker3D editMarker;
    OnlineMapsDrawingPoly polyEdit;
    public GameObject inputJumlah;
    public GameObject plotBtn;
    public GameObject scrollView;
    public GameObject edit;
    public GameObject editAll;
    public GameObject delete;
    public GameObject deleteAll;
    public GameObject move;
    public GameObject moveAll;
    public bool isMoving;
    public bool isMovingAll;
    public bool isDragging;
    public List<Vector2> polyPoints;
    Vector2 worldPos;
    Vector2 nowWorldPos;
    RaycastHit hit;

    [Header("Lain - Lain")]
    public bool haveSelected = false;
    public List<Vector2> markerPositions;
    public int indexObs;
    int n = 0;
    [HideInInspector] public int m = 0;
    [HideInInspector] public int c = 0;

    [Header("Drawings")]
    OnlineMapsDrawingElement poly;
    OnlineMapsDrawingPoly polygon;
    Vector2 polygonCenter;
    
    #region INSTANCE
    public static PlotObstacleController instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion
    
    // Start is called before the first frame update
    void Start()
    {
        ListingObstacle();
    }

    void Update()
    {
        Move();
        MoveAll();
    }

    private void ListingObstacle()
    {
        if (obstacles.Count <= 0) return;

        for (int i = 0; i < obstacles.Count; i++)
        {
            var objObstacle = Instantiate(prefabList, listObstacle);
            var metadata = objObstacle.GetComponent<Metadata>();
            int index = i;

            metadata.FindParameter("obstacle-symbol").parameter.GetComponent<Text>().text = obstacles[i];
            metadata.FindParameter("obstacle-symbol").parameter.GetComponent<Text>().font = Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + fontObstacle);
            
            objObstacle.GetComponent<Button>().onClick.AddListener(delegate {OnObstacleSelected(metadata.FindParameter("obstacle-symbol").parameter.GetComponent<Text>().text, index + 1); });
        }
    }

    public void OnObstacleSelected(string index, int obstacleIndex)
    {
        if(!selectedObstacleGroup.activeSelf) selectedObstacleGroup.SetActive(true);
        
        var metadata = selectedObstacleGroup.GetComponent<Metadata>();
        metadata.FindParameter("obstacle-symbol").parameter.GetComponent<Text>().text = index;
        metadata.FindParameter("obstacle-symbol").parameter.GetComponent<Text>().font = Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + fontObstacle);

        haveSelected = true;
        indexObs = obstacleIndex;
    }
    
    public void resetForm()
    {
        if(selectedObstacleGroup.activeSelf) selectedObstacleGroup.SetActive(false);
        input_jumlah.text = "";
        input_size.text = "";
        dropdownWarna.value = 0;

        if(editMarker.instance != null)
        {       
            editMarker.instance.GetComponent<CanvasGroup>().blocksRaycasts = false;
            
            ObstacleStat oS = editMarker.instance.GetComponent<ObstacleStat>();
            List<OnlineMapsMarker3D> markersInPoly = new List<OnlineMapsMarker3D>();

            foreach(OnlineMapsMarker3D child in OnlineMapsMarker3DManager.instance)
            if(child.instance.GetComponent<ObstacleStat>() != null)
            if(child.instance.GetComponent<ObstacleStat>().polygonName == oS.polygonName)
            markersInPoly.Add(child);

            foreach(OnlineMapsMarker3D child in markersInPoly)
            child.instance.GetComponent<BoxCollider>().enabled = true;
        }
        
        haveSelected = false;
        plotBtn.SetActive(true);
        inputJumlah.SetActive(true);
        edit.SetActive(false);
        editAll.SetActive(false);
        delete.SetActive(false);
        deleteAll.SetActive(false);
        move.SetActive(false);
        moveAll.SetActive(false);
        isMoving = false;
        isMovingAll = false;
    }

    public void Plot()
    {
        if(haveSelected)
        {
            if(input_jumlah.text != "" && input_size.text != "")
            {
                if(int.Parse(input_size.text) > 0 && int.Parse(input_size.text) > 0)
                {
                    GetComponent<Animator>().Play("SlideOut");
                    markerPositions = new List<Vector2>();
                    OnlineMapsControlBase.instance.OnMapClick += AddPolyLine;
                    n = 0;
                    
                    if(SessionUser.nama_asisten == "ASOPS")
                    warna = Color.blue;
                    else if(SessionUser.nama_asisten == "ASINTEL")
                    warna = Color.red;
                    else
                    {warna = Color.blue; Debug.Log("Belum Login, Asisten Kosong");}

                    size = int.Parse(input_size.text);

                    prefab.transform.Find("Canvas/Text").GetComponent<Text>().color = warna;
                    prefab.transform.Find("Canvas/Text").GetComponent<Text>().text = selectedObstacleGroup.transform.Find("Selected/Text").GetComponent<Text>().text;
                    Debug.Log(SessionUser.nama_asisten);
                    
                    totalObs = int.Parse(input_jumlah.text);
                    confirmMenu.GetComponent<Animator>().Play("FadeIn");
                    confirmMenu.transform.localPosition = new Vector3(0, 156, 0);
                    confirmMenu.transform.Find("Content/Button").GetComponent<Button>().onClick.RemoveListener(delegate{ConfirmDraw();});
                    confirmMenu.transform.Find("Content/Button").GetComponent<Button>().onClick.AddListener(delegate{ConfirmDraw();});

                    resetForm();
                }
                else if(float.Parse(input_size.text) <= 0)
                {
                    Debug.LogError("Ukuran Tidak Boleh Negatif Maupun 0!!!");
                }
                else if(int.Parse(input_jumlah.text) <= 0)
                {
                    Debug.LogError("Jumlah Tidak Boleh Negatif Maupun 0!!!");
                }
            }
            else if(input_size.text == "")
            {
                Debug.LogError("Anda Belum Input Ukuran!!!");
            }
            else
            {
                Debug.LogError("Anda Belum Input Jumlah!!!");
            }
        }
        else
        {
            Debug.LogError("Anda Belum Memilih Obstacle!!!");
        }
    }

    public void AddPolyLine()
    {
        double lng, lat;
        OnlineMapsControlBase.instance.GetCoords(out lng, out lat);
        markerPositions.Add(new Vector2((float)lng, (float)lat));
        OnlineMapsMarker3D marker3D = manager3D.Create(lng, lat, prefab);
        marker3D.instance.name = "Points";
        marker3D.scale = 5f;
        markers = new List<OnlineMapsMarker3D>();

        foreach(OnlineMapsMarker3D child in OnlineMapsMarker3DManager.instance)
        if(child.instance.name == "Points")
        markers.Add(child);

        if(n == 0)
        {
            polygon = new OnlineMapsDrawingPoly(markerPositions, warna, .5f, new Color(0, 0, 0, 0.5f));
            poly = OnlineMapsDrawingElementManager.AddItem(polygon);
        }
        n++;

        map.Redraw();
        polygonCenter = new Vector2((float)polygon.center.x, (float)polygon.center.y);
    }

    public void ConfirmDraw()
    {
        if(markerPositions.Count < 4)
        Debug.LogError("Titik Polygon Tidak Boleh Kurang Dari 4!!!");
        else
        {
            OnlineMapsControlBase.instance.OnMapClick -= AddPolyLine;

            poly.Dispose();

            List<Vector2> vertices2D = markerPositions;

            polygon = new OnlineMapsDrawingPoly(vertices2D, warna, .5f, new Color(0, 0, 0, 0.5f));
            polygon.n = m;
            polygon.polyType = DrawStat.polyType.Polygon;
            poly = OnlineMapsDrawingElementManager.AddItem(polygon);
            poly.name = "polygon_" + polygon.n + "_" + SessionUser.id + "_" + Random.Range(10, 1000);
            poly.OnPress += OnPolyPress;
            poly.OnRelease += OnPolyRelease;
            poly.isObstacleArea = true;

            poly.DrawOnTileset(OnlineMapsTileSetControl.instance, OnlineMapsDrawingElementManager.instance.Count - 1);
            poly.instance.transform.localPosition = new Vector3(0f, 1f, 0f);

            map.Redraw();

            float x = 0;
            float y = 0;

            poly.instance.transform.parent = GameObject.Find("Map").transform.Find("Obstacle Area");

            int halfPolygon = markerPositions.Count / 2;

            JArray array_obstacle = new JArray();
            
            for (int i = 0; i < totalObs; i++)
            {
                if(i < totalObs / 3)
                {  
                    x = UnityEngine.Random.Range(polygonCenter.x, markerPositions[0].x);
                    y = UnityEngine.Random.Range(polygonCenter.y, markerPositions[0].y);
                }
                else if(i > totalObs / 3 && i < (totalObs / 3) * 2)
                {
                    x = UnityEngine.Random.Range(polygonCenter.x, markerPositions[halfPolygon].x);
                    y = UnityEngine.Random.Range(polygonCenter.y, markerPositions[halfPolygon].y);
                }
                else
                {
                    x = UnityEngine.Random.Range(polygonCenter.x, markerPositions[markerPositions.Count - 1].x);
                    y = UnityEngine.Random.Range(polygonCenter.y, markerPositions[markerPositions.Count - 1].y);
                }

                if(!OnlineMapsUtils.IsPointInPolygon(markerPositions, x, y))
                i--;
                else
                {                
                    int randomNum = Random.Range(10, 1000);
                    OnlineMapsMarker3D marker3D = manager3D.Create(x, y, prefab);
                    marker3D.n = c;
                    marker3D.label = "obstacle_" + marker3D.n + "_" + poly.name + "_" + SessionUser.id + "_" + randomNum;
                    marker3D.scale = size;
                    marker3D.isObstacle = true;
                    marker3D.poly = poly;
                    marker3D.OnClick += Marker3DOnClick;
                    marker3D.instance.tag = "Plot_Obstacle";
                    marker3D.instance.transform.parent = GameObject.Find(poly.name).transform;
                    marker3D.instance.name = marker3D.label;

                    ObstacleStat oS = marker3D.instance.GetComponent<ObstacleStat>();
                    if(SessionUser.id != null)
                    oS.userID = SessionUser.id.ToString();
                    oS.nama = marker3D.label;
                    oS.lngLat = marker3D.position;
                    oS.font = marker3D.instance.transform.Find("Canvas/Text").GetComponent<Text>().text;
                    
                    if(SessionUser.nama_asisten == "ASOPS")
                    oS.warna = ObstacleStat.warnaType.Biru;
                    else if(SessionUser.nama_asisten == "ASINTEL")
                    oS.warna = ObstacleStat.warnaType.Merah;
                    else
                    {oS.warna = ObstacleStat.warnaType.Biru; Debug.Log("Belum Login, Asisten Kosong");}

                    if(oS.warna == ObstacleStat.warnaType.Biru)
                    oS.warnaInString = "blue";
                    else
                    oS.warnaInString = "red";

                    oS.size = size * 5;
                    oS.index = indexObs;
                    oS.polygonName = poly.name;
                    oS.polygonPoints = polygon.points as Vector2[];
                    oS.info = "TandaTanda";

                    markerList = new List<OnlineMapsMarker3D>();

                    var eo = new Info_Obstacle();
                    eo.nama = oS.nama;
                    eo.font = oS.font;
                    eo.warna = oS.warnaInString;
                    eo.size = oS.size;
                    eo.info = oS.info;
                    eo.index = oS.index;
                    eo.id_user = int.Parse(oS.userID);

                    oS.info_obstacle = JsonConvert.SerializeObject(eo);

                    JObject new_symbol = new JObject
                    {
                        { "className", "my-div-icon" },
                        { "html", "<div style='margin-top:-15px; width: 30px;color: blue; font-size: 15px; font-family: TandaTanda'>B</div>" },
                        { "iconSize", "null" },
                        { "id_point", oS.nama }
                    };

                    oS.symbol = JsonConvert.SerializeObject(new_symbol);
                    
                    // array_obstacle.Add(new_obstacle);
                    c++;
                }
            }
            List<Vector2> polyPointsNow = polygon.points as List<Vector2>;

            poly.instance.tag = "draw-polygon";
            
            DrawStat dS = poly.instance.AddComponent<DrawStat>();
            if(SessionUser.id != null)
            dS.userID = SessionUser.id.ToString();
            dS.polyName = poly.name;
            dS.type = polygon.polyType;
            dS.drawType = "polygon";
            
            if(polygon.backgroundColor == Color.blue)
            dS.fillColor = "blue";
            else if(polygon.backgroundColor == Color.red)
            dS.fillColor = "red";
            else
            dS.fillColor = "#" + ColorUtility.ToHtmlStringRGB(polygon.backgroundColor);

            if(polygon.borderColor == Color.blue)
            dS.color = "blue";
            else if(polygon.borderColor == Color.red)
            dS.color = "red";
            else 
            dS.color = "#" + ColorUtility.ToHtmlStringRGB(polygon.borderColor);
            
            dS.points = polygon.points as List<Vector2>;
            
            var coordinates = new List<List<double>>();

            foreach (Vector2 _child in dS.points)
            {
                var coordinate = new List<double>() { _child.x, _child.y };
                coordinates.Add(coordinate);
            }

            var finalGeo = new List<List<List<double>>>() { coordinates };

            var geoPoly = new EntityToolsGeometryPolygon();
            geoPoly.coordinates = finalGeo;
            dS.geometry = EntityToolsGeometryPolygon.ToString(geoPoly);

            JObject new_properties = new JObject
            {
                { "weight", "4" },
                { "dashArray", "0,0" },
                { "color", dS.color },
                { "fillColor", dS.fillColor },
                { "fillOpacity", polygon.backgroundColor.a },
                { "opacity", polygon.borderColor.a },
                { "id_point", dS.polyName },
                { "transform", true },
                { "draggable", true },
                { "interactive", true },
                { "className", "leaflet-path-draggable" }
            };

            dS.properties = JsonConvert.SerializeObject(new_properties);

            foreach(OnlineMapsMarker3D child in markers)
            OnlineMapsMarker3DManager.RemoveItem(child);

            markers.Clear();

            confirmMenu.GetComponent<Animator>().Play("FadeOut");
            m++;
            poly = null;
        }
    }
    
    public string ListVectorToText(List<Vector2> list)
    {
        string result = "";
        foreach(var listMember in list)
        {
            if(listMember != list[list.Count - 1])
            result += "[" + listMember.x.ToString() + "," + listMember.y.ToString() + "]" + ", ";
            else
            result += "[" + listMember.x.ToString() + "," + listMember.y.ToString() + "]";
        }
        return result;
    }

    public void Marker3DOnClick(OnlineMapsMarkerBase markerBase)
    {
        GameObject canvas = canvasForm;
        OnlineMapsMarker3D marker3D = markerBase as OnlineMapsMarker3D;
        editMarker = marker3D;

        ObstacleStat oS = marker3D.instance.GetComponent<ObstacleStat>();

        GameObject editObject = editMarker.instance;
        List<OnlineMapsMarker3D> markersInPoly = new List<OnlineMapsMarker3D>();

        foreach(OnlineMapsDrawingElement child in OnlineMapsDrawingElementManager.instance)
        if(child.name == oS.polygonName)
        polyEdit = child as OnlineMapsDrawingPoly;

        markerPositions = new List<Vector2>();
        markerPositions = polyEdit.points as List<Vector2>;

        polyPoints = new List<Vector2>();
        polyPoints = polyEdit.points as List<Vector2>;

        input_size.text = (oS.size / 5).ToString();
        
        if(oS.warna == ObstacleStat.warnaType.Biru)
        dropdownWarna.value = 0;
        else
        dropdownWarna.value = 1;

        // foreach(OnlineMapsMarker3D child in OnlineMapsMarker3DManager.instance)
        // Debug.Log();

        foreach(OnlineMapsMarker3D child in OnlineMapsMarker3DManager.instance)
        if(child.instance.GetComponent<ObstacleStat>() != null)
        if(child.instance.GetComponent<ObstacleStat>().polygonName == oS.polygonName)
        markersInPoly.Add(child);

        foreach(OnlineMapsMarker3D child in markersInPoly)
        if(child != editMarker)
        child.instance.GetComponent<BoxCollider>().enabled = false;

        OnObstacleSelected(oS.font, oS.index);

        canvas.SetActive(true);
        canvas.GetComponent<Animator>().Play("SlideIn");

        plotBtn.SetActive(false);
        inputJumlah.SetActive(false);
        edit.SetActive(true);
        editAll.SetActive(true);
        delete.SetActive(true);
        deleteAll.SetActive(true);
        move.SetActive(true);
        moveAll.SetActive(true);
        // scrollView.GetComponent<RectTransform>().localPosition = new Vector3(0f, -13f, 0f);
    }

    public void Edit()
    {
        GameObject editObject = editMarker.instance;
        ObstacleStat oS = editObject.GetComponent<ObstacleStat>();
        List<OnlineMapsMarker3D> markersInPoly = new List<OnlineMapsMarker3D>();
        List<OnlineMapsMarker3D> blueMarkers = new List<OnlineMapsMarker3D>();

        foreach(OnlineMapsMarker3D child in OnlineMapsMarker3DManager.instance)
        if(child.instance.GetComponent<ObstacleStat>() != null)
        if(child.instance.GetComponent<ObstacleStat>().polygonName == oS.polygonName)
        markersInPoly.Add(child);

        if(float.Parse(input_size.text) > 0)
        {
            editMarker.scale = float.Parse(input_size.text);
            oS.size = int.Parse(input_size.text) * 5;
        }
        else if(float.Parse(input_size.text) <= 0 || input_size.text == "")
        {
            editMarker.scale = oS.size;
            Debug.LogError("Ukuran Tidak Boleh Kurang Atau Sama Dengan 0!!!");
        }

        editObject.transform.Find("Canvas/Text").GetComponent<Text>().text = selectedObstacleGroup.transform.Find("Selected/Text").GetComponent<Text>().text;
        oS.font = selectedObstacleGroup.transform.Find("Selected/Text").GetComponent<Text>().text;
        oS.index = indexObs;

        var info = new Info_Obstacle();
        info.nama = oS.nama;
        info.font = oS.font;
        info.warna = oS.warnaInString;
        info.size = oS.size;
        info.info = oS.info;
        info.index = oS.index;
        
        oS.info_obstacle = Info_Obstacle.ToString(info);
        
        foreach(OnlineMapsMarker3D child in markersInPoly)
        child.instance.GetComponent<BoxCollider>().enabled = true;
        
        GetComponent<Animator>().Play("SlideOut");
        resetForm();
    }

    public void EditAll()
    {
        GameObject editObject = editMarker.instance;
        ObstacleStat oS = editObject.GetComponent<ObstacleStat>();
        List<OnlineMapsMarker3D> markersInPoly = new List<OnlineMapsMarker3D>();

        foreach(OnlineMapsMarker3D child in OnlineMapsMarker3DManager.instance)
        if(child.instance.GetComponent<ObstacleStat>() != null)
        if(child.instance.GetComponent<ObstacleStat>().polygonName == oS.polygonName)
        markersInPoly.Add(child);

        foreach(OnlineMapsMarker3D child in markersInPoly)
        {
            ObstacleStat oSS = child.instance.GetComponent<ObstacleStat>();

            if(float.Parse(input_size.text) > 0)
            {
                child.scale = float.Parse(input_size.text);
                oSS.size = int.Parse(input_size.text) * 5;
            }
            else if(float.Parse(input_size.text) <= 0 || input_size.text == "")
            {
                editMarker.scale = oS.size;
                Debug.LogError("Ukuran Tidak Boleh Kurang Atau Sama Dengan 0!!!");
            }

            oSS.index = indexObs;
            oSS.font = selectedObstacleGroup.transform.Find("Selected/Text").GetComponent<Text>().text;
            child.instance.transform.Find("Canvas/Text").GetComponent<Text>().text = selectedObstacleGroup.transform.Find("Selected/Text").GetComponent<Text>().text;

            var info = new Info_Obstacle();
            info.nama = oSS.nama;
            info.font = oSS.font;
            info.warna = oSS.warnaInString;
            info.size = oSS.size;
            info.info = oSS.info;
            info.index = oSS.index;

            oSS.info_obstacle = Info_Obstacle.ToString(info);
        }

        foreach(OnlineMapsMarker3D child in markersInPoly)
        child.instance.GetComponent<BoxCollider>().enabled = true;

        GetComponent<Animator>().Play("SlideOut");
        resetForm();
    }

    public void Delete()
    {
        GameObject editObject = editMarker.instance;
        ObstacleStat oS = editObject.GetComponent<ObstacleStat>();
        List<OnlineMapsMarker3D> markersInPoly = new List<OnlineMapsMarker3D>();
        
        var trash = GameObject.Find("Trash");
        if(trash == null)
        {
            trash = new GameObject("Trash");
            trash.AddComponent<TrashObj>();
        }

        var tObj = new Trash();
        tObj.id = editObject.name;
        tObj.type = "obstacle";

        var tScript = trash.GetComponent<TrashObj>();
        tScript.trash.Add(tObj);

        foreach(OnlineMapsMarker3D child in OnlineMapsMarker3DManager.instance)
        if(child.instance.GetComponent<ObstacleStat>() != null)
        if(child.instance.GetComponent<ObstacleStat>().polygonName == oS.polygonName)
        markersInPoly.Add(child);

        EntityEditorController.Instance.plotTrash.Add(
            new JObject()
            {
                { "id", editMarker.instance.GetComponent<ObstacleStat>().nama },
                { "type", "obstacle" }
            }
        );

        markersInPoly.Remove(editMarker);
        OnlineMapsMarker3DManager.RemoveItem(editMarker);

        if(markersInPoly.Count == 0)
        {
            if(DrawArrow.instance.drawingList.Contains(polyEdit))
            {
                var ttObj = new Trash();
                ttObj.id = polyEdit.name;
                ttObj.type = "polygon";
                tScript.trash.Add(ttObj);

                EntityEditorController.Instance.plotTrash.Add(
                    new JObject()
                    {
                        { "id", polyEdit.instance.GetComponent<DrawStat>().polyName },
                        { "type", "polygon" }
                    }
                );

                DrawArrow.instance.drawingList.Remove(polyEdit);
                polyEdit.Dispose();
            }
        }
        
        foreach(OnlineMapsMarker3D child in markersInPoly)
        child.instance.GetComponent<BoxCollider>().enabled = true;
        
        GetComponent<Animator>().Play("SlideOut");
        resetForm();
    }

    public void DeleteAll()
    {
        GameObject editObject = editMarker.instance;
        ObstacleStat oS = editObject.GetComponent<ObstacleStat>();
        List<OnlineMapsMarker3D> markersInPoly = new List<OnlineMapsMarker3D>();

        foreach(OnlineMapsMarker3D child in OnlineMapsMarker3DManager.instance)
        if(child.instance.GetComponent<ObstacleStat>() != null)
        if(child.instance.GetComponent<ObstacleStat>().polygonName == oS.polygonName)
        markersInPoly.Add(child);

        foreach(OnlineMapsMarker3D child in markersInPoly)
        {
            var trash = GameObject.Find("Trash");
            if(trash == null)
            {
                trash = new GameObject("Trash");
                trash.AddComponent<TrashObj>();
            }

            var tObj = new Trash();
            tObj.id = child.instance.name;
            tObj.type = "obstacle";

            var tScript = trash.GetComponent<TrashObj>();
            tScript.trash.Add(tObj);

            EntityEditorController.Instance.plotTrash.Add(
                new JObject()
                {
                    { "id", child.instance.GetComponent<ObstacleStat>().nama },
                    { "type", "obstacle" }
                }
            );

            OnlineMapsMarker3DManager.RemoveItem(child);
        }

        if(DrawArrow.instance.drawingList.Contains(polyEdit))
        {
            var trash = GameObject.Find("Trash");
            if(trash == null)
            {
                trash = new GameObject("Trash");
                trash.AddComponent<TrashObj>();
            }

            var tObj = new Trash();
            tObj.id = polyEdit.name;
            tObj.type = "obstacle";

            var tScript = trash.GetComponent<TrashObj>();
            tScript.trash.Add(tObj);

            DrawArrow.instance.drawingList.Remove(polyEdit);
        }

        EntityEditorController.Instance.plotTrash.Add(
            new JObject()
            {
                { "id", polyEdit.instance.GetComponent<DrawStat>().polyName },
                { "type", "polygon" }
            }
        );

        polyEdit.Dispose();
        GetComponent<Animator>().Play("SlideOut");
        resetForm();        
    }

    public void MoveOn()
    {
        if(isMoving)
        {
            if(editMarker.instance != null)
            {       
                editMarker.instance.GetComponent<CanvasGroup>().blocksRaycasts = false;
                
                ObstacleStat oS = editMarker.instance.GetComponent<ObstacleStat>();
                List<OnlineMapsMarker3D> markersInPoly = new List<OnlineMapsMarker3D>();

                foreach(OnlineMapsMarker3D child in OnlineMapsMarker3DManager.instance)
                if(child.instance.GetComponent<ObstacleStat>() != null)
                if(child.instance.GetComponent<ObstacleStat>().polygonName == oS.polygonName)
                markersInPoly.Add(child);

                foreach(OnlineMapsMarker3D child in markersInPoly)
                child.instance.GetComponent<BoxCollider>().enabled = true;
            }
        }

        GameObject editObject = editMarker.instance;
        isMoving = !isMoving;
        isMovingAll = false;
        editObject.GetComponent<CanvasGroup>().blocksRaycasts = !editObject.GetComponent<CanvasGroup>().blocksRaycasts;
    }

    public void Move()
    {
        if(isMoving)
        {
            GameObject editObject = editMarker.instance;
            ObstacleStat oS = editObject.GetComponent<ObstacleStat>();
            List<OnlineMapsMarker3D> markersInPoly = new List<OnlineMapsMarker3D>();

            foreach(OnlineMapsMarker3D child in OnlineMapsMarker3DManager.instance)
            if(child.instance.GetComponent<ObstacleStat>() != null)
            if(child.instance.GetComponent<ObstacleStat>().polygonName == oS.polygonName)
            markersInPoly.Add(child);

            if(Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                double lng, lat;

                if(Physics.Raycast(ray, out hit))
                {
                    if(hit.transform == editObject.transform)
                    if(OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
                    worldPos = new Vector2((float)lng, (float)lat);
                }
            }

            if(Input.GetMouseButton(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                double lng, lat;

                if(Physics.Raycast(ray, out hit))
                {
                    if(hit.transform == editObject.transform)
                    {
                        if(OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
                        nowWorldPos = new Vector2((float)lng, (float)lat);

                        Vector2 diffPos = nowWorldPos - worldPos;

                        if(nowWorldPos != worldPos)
                        if(OnlineMapsUtils.IsPointInPolygon(markerPositions, nowWorldPos.x, nowWorldPos.y))
                        editMarker.position += diffPos;

                        worldPos = nowWorldPos;
                    }
                }
            }
        }
    }

    public void MoveAllOn()
    {
        if(isMovingAll)
        {
            if(editMarker.instance != null)
            {       
                editMarker.instance.GetComponent<CanvasGroup>().blocksRaycasts = false;
                
                ObstacleStat oS = editMarker.instance.GetComponent<ObstacleStat>();
                List<OnlineMapsMarker3D> markersInPoly = new List<OnlineMapsMarker3D>();

                foreach(OnlineMapsMarker3D child in OnlineMapsMarker3DManager.instance)
                if(child.instance.GetComponent<ObstacleStat>() != null)
                if(child.instance.GetComponent<ObstacleStat>().polygonName == oS.polygonName)
                markersInPoly.Add(child);

                foreach(OnlineMapsMarker3D child in markersInPoly)
                child.instance.GetComponent<BoxCollider>().enabled = true;
            }
        }
        
        isMoving = false;
        isMovingAll = !isMovingAll;
    }

    public void MoveAll()
    {
        if(isMovingAll)
        {
            GameObject editObject = editMarker.instance;
            ObstacleStat oS = editObject.GetComponent<ObstacleStat>();
            List<OnlineMapsMarker3D> markersInPoly = new List<OnlineMapsMarker3D>();

            foreach(OnlineMapsMarker3D child in OnlineMapsMarker3DManager.instance)
            if(child.instance.GetComponent<ObstacleStat>() != null)
            if(child.instance.GetComponent<ObstacleStat>().polygonName == oS.polygonName)
            markersInPoly.Add(child);
            
            foreach(OnlineMapsDrawingElement child in OnlineMapsDrawingElementManager.instance)
            if(child.name == oS.polygonName)
            polyEdit = child as OnlineMapsDrawingPoly;
            
            editObject.GetComponent<BoxCollider>().enabled = false;

            if(isDragging)
            {
                double lng, lat;

                if(OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
                nowWorldPos = new Vector2((float)lng, (float)lat);      

                Vector2 diffPos = nowWorldPos - worldPos;

                if(nowWorldPos != worldPos)
                if(OnlineMapsUtils.IsPointInPolygon(markerPositions, nowWorldPos.x, nowWorldPos.y))     
                {
                    foreach(OnlineMapsMarker3D child in markersInPoly)
                    child.position += diffPos;

                    for(int i = 0; i < polyPoints.Count; i++)
                    polyPoints[i] += diffPos;
                }     

                JObject new_polygon = new JObject
                {
                    { "type", "Polygon" },
                    { "coordinates", "[[" + ListVectorToText(polyPoints) + "]]" }
                };
                
                var dS = poly.instance.GetComponent<DrawStat>();
                dS.geometry = JsonConvert.SerializeObject(new_polygon);

                OnlineMaps.instance.Redraw();

                worldPos = nowWorldPos;
            }
        }
    }

    public void OnPolyPress(OnlineMapsDrawingElement element)
    {
        if(isMovingAll)
        {
            poly = element;
            polygon = element as OnlineMapsDrawingPoly;

            double lng, lat;

            if(OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
            worldPos = new Vector2((float)lng, (float)lat);

            isDragging = true;
            OnlineMaps.instance.GetComponent<OnlineMapsTileSetControl>().allowUserControl = false;
        }
    }

    public void OnPolyRelease(OnlineMapsDrawingElement element)
    {
        if(isMovingAll)
        {
            isDragging = false;
            OnlineMaps.instance.GetComponent<OnlineMapsTileSetControl>().allowUserControl = true;
        }
    }
}
