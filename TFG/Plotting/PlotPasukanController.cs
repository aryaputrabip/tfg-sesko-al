using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using HelperEditor;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using GlobalVariables;
using ObjectStat;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using HelperPlotting;
using System.Net.Mime;
using Plot.FormasiPlot;

namespace Plot.Pasukan
{
    public class PlotPasukanController : MonoBehaviour
    {
        #region INSTANCE
        public static PlotPasukanController instance;
        [HideInInspector] public string dataConnection;
        [HideInInspector] public int indexSatuan;

        [Space(5)]
        [Header("Connection String")]
        ConnectionString connectionString;

        [Space(5)]
        [Header("Online Map")]
        public GameObject map;
        public OnlineMaps onlineMaps;

        [Space(5)]
        [Header("Canvas")]
        public GameObject canvas;
        public TMP_InputField inputNama;
        public TMP_InputField inputKeterangan;
        public TMP_InputField inputHeading;
        public TMP_InputField inputKecepatan;
        public TMP_Dropdown dropdownKecepatan;
        public TMP_InputField inputBahanBakar;
        public TMP_InputField searchBox;
        public GameObject selectedPasukanGroup;
        public ScrollRect scrollview;
        public Transform content;
        public Transform contentDarat;
        public Transform contentLaut;
        public Transform contentUdara;
        public Transform contentSearch;
        public GameObject listPrefab;
        public GameObject plotBtn;
        public GameObject editBtn;
        public GameObject moveBtn;
        public GameObject deleteBtn;
        public GameObject kegiatanBtn;
        public GameObject btnKegiatanSat;
        public GameObject btnKegiatanPas;
        public GameObject btnCloseKegiatanSat;
        public GameObject btnCloseKegiatanPas;
        public GameObject btnDeleteKegiatanSat;
        public GameObject btnDeleteKegiatanPas;
        public GameObject editMenu;
        public SidemenuToggler toggle;
        public Button formasiBtn;

        [Space(5)]
        [Header("Prefab")]
        public string nama;
        public string matra;
        public string keterangan;
        [HideInInspector] public string keteranganPas;
        public GameObject soldier;
        public GameObject soldier2D;
        [HideInInspector] public string fontPas = "";
        [HideInInspector] public string fontPasID = "";
        [HideInInspector] public string font;

        [Header("List")]
        [Space(5)]
        [HideInInspector] public List<ListPasukan> lPas;
        [HideInInspector] public List<string> listNamaPasukan;
        [HideInInspector] public List<string> listMatraPasukan;
        [HideInInspector] public List<string> listKeteranganPasukan;
        [HideInInspector] public List<string> listFont;
        [HideInInspector] public List<string> listSymbol;
        [HideInInspector] public List<string> listSymbolID;
        [HideInInspector] public List<GameObject> contentListInstantiated;
        [HideInInspector] public List<OnlineMapsMarker3D> listMarker2DOnMap = new List<OnlineMapsMarker3D>();
        [HideInInspector] public int indexPas;

        [Space(5)]
        [Header("Manager")]
        public OnlineMapsMarkerManager manager;
        public OnlineMapsMarker3DManager manager3D;
        OnlineMapsMarker3D this3D;
        OnlineMapsMarker3D this2D;
        public bool isPlottingInfan = false;
        bool haveSelected = false;

        [Header("Edit")]
        [Space(5)]
        public bool isMoving = false;
        bool isDragging = false;
        [HideInInspector] public OnlineMapsMarker3D edit2D;
        [HideInInspector] public OnlineMapsMarker3D edit3D;
        Vector2 prevWorldPos;
        Vector2 newWorldPos;
        [SerializeField] string hapusList;
        [SerializeField] List<string> hapus;
        public List<ListPasukan> listMPasukan;
        ListPasukan listPasukan;

        void Awake()
        {
            if (instance == null)
            {
                instance = this;
                // GrabListPasukan();
            }
            else
            {
                Destroy(gameObject);
            }
        }
        #endregion

        // Start is called before the first frame update
        void Start()
        {
            onlineMaps.OnChangeZoom += OnChangeZoom;
            OnChangeZoom();
            
            var cameraOrbit = OnlineMapsCameraOrbit.instance;
            OnlineMaps.instance.OnUpdateBefore += OnUpdateBefore;

            // var tesA = manager3D.Create( 104.844721f, -5.038617f, soldier2D);
            // var tesB = manager3D.Create( 104.844721f, -5.038617f, soldier2D);
            // var tesC = manager3D.Create( 104.844721f, -5.038617f, soldier2D);

            // var jarakLat = (float)(1.67f * Math.Cos(0 * (Math.PI / 180)));
            // var jarak2Lat = (float)(1.67f * Math.Cos(90 * (Math.PI / 180)));
            // var jarakLng = (float)(1.67f * Math.Sin(0 * (Math.PI / 180)));
            // var jarak2Lng = (float)(1.67f * Math.Sin(90 * (Math.PI / 180)));

            // var posisiB = new Vector2(tesB.position.x, tesB.position.y);
            // var posisiC = new Vector2(tesC.position.x, tesC.position.y);

            // posisiB.x += jarakLng;
            // posisiB.y += jarakLat;
            // posisiC.x += jarak2Lng;
            // posisiC.y += jarak2Lat;

            // tesB.position = posisiB;
            // tesC.position = posisiC;
            // Debug.Log(cameraOrbit.rotation);
        }

        // Update is called once per frame
        void Update()
        {
            Move();
        }

        public void GrabListViaEditor(string list)
        {
            listMPasukan = ListPasukan.FromJson(list);
            listMPasukan = listMPasukan.OrderBy(go => go.name).ToList();

            lPas = new List<ListPasukan>();
            var mDarat = new List<ListPasukan>();
            var mLaut = new List<ListPasukan>();
            var mUdara = new List<ListPasukan>();

            hapus = hapusList.Split(", ").ToList();

            for (int i = 0; i < listMPasukan.Count; i++)
                for (int j = 0; j < hapus.Count; j++)
                    if (listMPasukan[i].name.Contains(hapus[j]))
                        listMPasukan.Remove(listMPasukan[i]);

            for (int i = 0; i < listMPasukan.Count; i++)
            {
                ListPasukan now = listMPasukan[i];
                lPas.Add(now);

                if(now.matra_pasukan == "Darat")
                mDarat.Add(now);
                else if(now.matra_pasukan == "Laut")
                mLaut.Add(now);
                else if(now.matra_pasukan == "Udara")
                mUdara.Add(now);
            }

            for(int i = 0; i < mDarat.Count; i++)
            {
                var now = mDarat[i];
                GameObject copy = Instantiate(listPrefab, contentDarat);

                TMP_Text number = copy.transform.Find("No.").GetComponent<TMP_Text>();
                Text symbol = copy.transform.Find("Image/Text (Legacy)").GetComponent<Text>();
                TMP_Text namaObj = copy.transform.Find("Nama").GetComponent<TMP_Text>();
                TMP_Text matraPas = copy.transform.Find("Matra").GetComponent<TMP_Text>();
                Button selBtnCopy = copy.transform.Find("Btn - Select").GetComponent<Button>();

                string fontCopy = now.nama;
                string ketPas = now.keterangan;
                string symID = now.pasukan_sym_id;

                number.text = (i + 1).ToString();
                symbol.text = ((char)int.Parse(now.index)).ToString();
                symbol.font = Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + fontCopy);
                namaObj.text = now.name;
                matraPas.text = now.matra_pasukan;
                selBtnCopy.onClick.AddListener(delegate { OnPasukanSelected(symbol.text, int.Parse(number.text), namaObj.text, matraPas.text, ketPas, fontCopy, symID, now); });

                if (!contentListInstantiated.Contains(copy))
                    contentListInstantiated.Add(copy);
            }

            for(int i = 0; i < mLaut.Count; i++)
            {
                var now = mLaut[i];
                GameObject copy = Instantiate(listPrefab, contentLaut);

                TMP_Text number = copy.transform.Find("No.").GetComponent<TMP_Text>();
                Text symbol = copy.transform.Find("Image/Text (Legacy)").GetComponent<Text>();
                TMP_Text namaObj = copy.transform.Find("Nama").GetComponent<TMP_Text>();
                TMP_Text matraPas = copy.transform.Find("Matra").GetComponent<TMP_Text>();
                Button selBtnCopy = copy.transform.Find("Btn - Select").GetComponent<Button>();

                string fontCopy = now.nama;
                string ketPas = now.keterangan;
                string symID = now.pasukan_sym_id;

                number.text = (i + 1).ToString();
                symbol.text = ((char)int.Parse(now.index)).ToString();
                symbol.font = Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + fontCopy);
                namaObj.text = now.name;
                matraPas.text = now.matra_pasukan;
                selBtnCopy.onClick.AddListener(delegate { OnPasukanSelected(symbol.text, int.Parse(number.text), namaObj.text, matraPas.text, ketPas, fontCopy, symID, now); });

                if (!contentListInstantiated.Contains(copy))
                    contentListInstantiated.Add(copy);
            }

            for(int i = 0; i < mUdara.Count; i++)
            {
                var now = mUdara[i];
                GameObject copy = Instantiate(listPrefab, contentUdara);

                TMP_Text number = copy.transform.Find("No.").GetComponent<TMP_Text>();
                Text symbol = copy.transform.Find("Image/Text (Legacy)").GetComponent<Text>();
                TMP_Text namaObj = copy.transform.Find("Nama").GetComponent<TMP_Text>();
                TMP_Text matraPas = copy.transform.Find("Matra").GetComponent<TMP_Text>();
                Button selBtnCopy = copy.transform.Find("Btn - Select").GetComponent<Button>();

                string fontCopy = now.nama;
                string ketPas = now.keterangan;
                string symID = now.pasukan_sym_id;

                number.text = (i + 1).ToString();
                symbol.text = ((char)int.Parse(now.index)).ToString();
                symbol.font = Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + fontCopy);
                namaObj.text = now.name;
                matraPas.text = now.matra_pasukan;
                selBtnCopy.onClick.AddListener(delegate { OnPasukanSelected(symbol.text, int.Parse(number.text), namaObj.text, matraPas.text, ketPas, fontCopy, symID, now); });

                if (!contentListInstantiated.Contains(copy))
                    contentListInstantiated.Add(copy);
            }
        }

        public void Search()
        {
            var searchObj = searchBox.text;

            content = contentSearch;
            contentSearch.gameObject.SetActive(true);
            contentDarat.gameObject.SetActive(false);
            contentLaut.gameObject.SetActive(false);
            contentUdara.gameObject.SetActive(false);

            foreach(Transform item in content.transform)
            Destroy(item.gameObject);

            scrollview.content = content.GetComponent<RectTransform>();

            for(int i = 0; i < lPas.Count; i++)
            {
                var now = lPas[i];
                if(now.name.ToLower().Contains(searchObj.ToLower()))
                {
                    GameObject copy = Instantiate(listPrefab, contentSearch);

                    TMP_Text number = copy.transform.Find("No.").GetComponent<TMP_Text>();
                    Text symbol = copy.transform.Find("Image/Text (Legacy)").GetComponent<Text>();
                    TMP_Text namaObj = copy.transform.Find("Nama").GetComponent<TMP_Text>();
                    TMP_Text matraPas = copy.transform.Find("Matra").GetComponent<TMP_Text>();
                    Button selBtnCopy = copy.transform.Find("Btn - Select").GetComponent<Button>();

                    string fontCopy = now.nama;
                    string ketPas = now.keterangan;
                    string symID = now.pasukan_sym_id;

                    number.text = (i + 1).ToString();
                    symbol.text = ((char)int.Parse(now.index)).ToString();
                    symbol.font = Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + fontCopy);
                    namaObj.text = now.name;
                    matraPas.text = now.matra_pasukan;
                    selBtnCopy.onClick.AddListener(delegate { OnPasukanSelected(symbol.text, int.Parse(number.text), namaObj.text, matraPas.text, ketPas, fontCopy, symID, now); });
                }
            }
        }

        public void DaratList(bool active)
        {
            if(active)
            {
                content = contentDarat;
                contentDarat.gameObject.SetActive(true);
                contentLaut.gameObject.SetActive(false);
                contentUdara.gameObject.SetActive(false);
                contentSearch.gameObject.SetActive(false);

                scrollview.content = content.GetComponent<RectTransform>();
            }
            else
            contentDarat.gameObject.SetActive(false);
        }
        
        public void LautList(bool active)
        {
            if(active)
            {
                content = contentLaut;
                contentLaut.gameObject.SetActive(true);
                contentDarat.gameObject.SetActive(false);
                contentUdara.gameObject.SetActive(false);
                contentSearch.gameObject.SetActive(false);

                scrollview.content = content.GetComponent<RectTransform>();
            }
            else
            contentLaut.gameObject.SetActive(false);
        }
        
        public void UdaraList(bool active)
        {
            if(active)
            {
                content = contentUdara;
                contentUdara.gameObject.SetActive(true);
                contentLaut.gameObject.SetActive(false);
                contentDarat.gameObject.SetActive(false);
                contentSearch.gameObject.SetActive(false);

                scrollview.content = content.GetComponent<RectTransform>();
            }
            else
            contentUdara.gameObject.SetActive(false);
        }

        // public async Task AddToList(ListPasukan[] listSatuan, ListPasukanData.typeObject tipe_tni)
        // {
        //     if (listSatuan == null) return;

        //     for (int i = 0; i < listSatuan.Length; i++)
        //     {
        //         //Debug.Log(listSatuan.Length);
        //         if (listSatuan[i].ID == null) continue;

        //         //var objList = Instantiate(prefabList, listContainer);

        //         //var metadata = objList.GetComponent<Metadata>();
        //         var objData        = new ListPasukanData();
        //         objData.id         = listSatuan[i].ID;
        //         objData.name       = listSatuan[i].NAME;
        //         objData.tipe_obj   = tipe_tni;
        //         objData.fontFamily = listSatuan[i].fontFamily;
        //         objData.index      = ((char)listSatuan[i].fontIndex).ToString();
        //     }
        // }

        public void Kegiatan()
        {
            var pF = edit3D.instance.GetComponent<PFormasi>();

            if(pF.formasi != PFormasi.formasiType.None)
            RouteController.Instance.markerInstance = pF.listObjekFormasi[0].GetComponent<PFormasi>().marker3D;
            else
            RouteController.Instance.markerInstance = pF.marker3D;

            var pFF = RouteController.Instance.markerInstance.instance.GetComponent<PFormasi>();

            if(pFF.stat != null)
            {
                btnKegiatanSat.SetActive(false);
                btnKegiatanPas.SetActive(true);
                btnCloseKegiatanSat.SetActive(false);
                btnCloseKegiatanPas.SetActive(true);
                btnDeleteKegiatanSat.SetActive(false);
                btnDeleteKegiatanPas.SetActive(true);
            }
            else
            {
                btnKegiatanSat.SetActive(true);
                btnKegiatanPas.SetActive(false);
                btnCloseKegiatanSat.SetActive(true);
                btnCloseKegiatanPas.SetActive(false);
                btnDeleteKegiatanSat.SetActive(true);
                btnDeleteKegiatanPas.SetActive(false);
            }
        }

        // public void GrabListPasukan()
        // {
        //     PasukanList.GrabList();

        //     if(contentListInstantiated.Count > 0)
        //     {
        //         foreach(GameObject child in contentListInstantiated)
        //         GameObject.Destroy(child);

        //         contentListInstantiated.Clear();
        //     }

        //     for(int i = 0; i < listNamaPasukan.Count; i++)
        //     {
        //         GameObject copy = Instantiate(listPrefab, content);

        //         TMP_Text number = copy.transform.Find("No.").GetComponent<TMP_Text>();
        //         Text symbol = copy.transform.Find("Image/Text (Legacy)").GetComponent<Text>();
        //         TMP_Text namaObj = copy.transform.Find("Nama").GetComponent<TMP_Text>();
        //         TMP_Text matraPas = copy.transform.Find("Matra").GetComponent<TMP_Text>();
        //         Button selBtnCopy = copy.transform.Find("Btn - Select").GetComponent<Button>();

        //         string fontCopy = listFont[i];
        //         string ketPas = listKeteranganPasukan[i];
        //         string symID = listSymbolID[i];

        //         number.text = (i + 1).ToString();
        //         symbol.text = listSymbol[i];
        //         symbol.font = Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + listFont[i]);
        //         namaObj.text = listNamaPasukan[i];
        //         matraPas.text = listMatraPasukan[i];
        //         selBtnCopy.onClick.AddListener(delegate{OnPasukanSelected(symbol.text, int.Parse(number.text), namaObj.text, matraPas.text, ketPas, fontCopy, symID);});

        //         if(!contentListInstantiated.Contains(copy))
        //         contentListInstantiated.Add(copy);
        //     }
        // }

        public void OnPasukanSelected(string index, int pasukanIndex, string pasukanName, string pMatra, string pKeterangan, string symbolFont, string symbolID, ListPasukan lP)
        {
            if (!selectedPasukanGroup.activeSelf) selectedPasukanGroup.SetActive(true);

            fontPas = symbolFont;

            var metadata = selectedPasukanGroup.GetComponent<Metadata>();
            metadata.FindParameter("pasukan-symbol").parameter.GetComponent<Text>().text = index;
            metadata.FindParameter("pasukan-symbol").parameter.GetComponent<Text>().font = Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + fontPas);
            metadata.FindParameter("pasukan-name").parameter.GetComponent<TMP_Text>().text = pasukanName;
            metadata.FindParameter("pasukan-matra").parameter.GetComponent<TMP_Text>().text = pMatra;
            inputNama.text = pasukanName;
            inputKecepatan.text = lP.speed_pasukan;
            matra = pMatra;
            keteranganPas = pKeterangan;
            fontPasID = symbolID;
            listPasukan = lP;

            haveSelected = true;
            indexPas = pasukanIndex;
        }

        public void resetForm()
        {
            if (selectedPasukanGroup.activeSelf) selectedPasukanGroup.SetActive(false);
            inputNama.text = "";
            inputKeterangan.text = "";
            inputKecepatan.text = "";
            inputBahanBakar.text = "40";
            dropdownKecepatan.value = 0;

            if (edit2D != null && edit3D != null)
            {
                formasiBtn.onClick.RemoveAllListeners();
                edit2D = null;
                edit3D = null;
            }

            foreach (Transform child in map.transform)
            {
                if (child.GetComponentInChildren<BoxCollider>() != null)
                    child.GetComponentInChildren<BoxCollider>().enabled = true;
            }

            plotBtn.SetActive(true);
            editBtn.SetActive(false);
            moveBtn.SetActive(false);
            deleteBtn.SetActive(false);
            kegiatanBtn.SetActive(false);
            editMenu.SetActive(false);

            haveSelected = false;

            isMoving = false;
        }

        public void ResetFormKegiatan()
        {
            RouteController.Instance.markerInstance = null;
            btnKegiatanSat.SetActive(true);
            btnKegiatanPas.SetActive(false);
            btnCloseKegiatanSat.SetActive(true);
            btnCloseKegiatanPas.SetActive(false);
            btnDeleteKegiatanSat.SetActive(true);
            btnDeleteKegiatanPas.SetActive(false);
        }

        public void AddUnit()
        {
            if (inputNama.text == "")
                Debug.Log("Nama tidak boleh kosong!!");
            // else if(inputKeterangan.text == "")
            // Debug.Log("Keterangan tidak boleh kosong!!!");
            else if (inputKecepatan.text == "")
                Debug.Log("Kecepatan tidak boleh kosong!!!");
            else if (inputBahanBakar.text == "")
                Debug.Log("Bahan Bakar tidak boleh kosong!!!");
            else if (!haveSelected)
                Debug.Log("Anda Belum Memilih Pasukan!!!");
            else
            {
                isPlottingInfan = true;

                nama = inputNama.text;
                keterangan = inputKeterangan.text;
                font = selectedPasukanGroup.GetComponent<Metadata>().FindParameter("pasukan-symbol").parameter.GetComponent<Text>().text;

                inputNama.text = "";

                isPlottingInfan = true;
                OnlineMapsControlBase.instance.OnMapClick += Plot;
                toggle.toggleOff();
                Debug.Log("Unit sedang ditempatkan....");
            }
        }

        public void Plot()
        {
            double lng, lat;

            if (manager.map.control.GetCoords(out lng, out lat))
            {
                OnlineMapsMarker3D marker3D = manager3D.Create(lng, lat, soldier);
                OnlineMapsMarker3D marker = manager3D.Create(lng, lat, soldier2D);

                if (inputHeading.text == "" || inputHeading.text == "-" || inputHeading.text == ".")
                    inputHeading.text = "0";

                foreach (OnlineMapsMarker3D child in OnlineMapsMarker3DManager.instance)
                {
                    if (child == marker3D)
                        this3D = child;

                    if (child == marker)
                        this2D = child;
                }

                marker["markerSource"] = marker;
                marker.label = nama;
                marker["data"] = new ShowMarkerLabelsByZoomItem(marker.label, new OnlineMapsRange(0f, 16f));
                marker.scale = 1f;
                marker.range = new OnlineMapsRange(0f, 16f);
                marker.instance.name = "satuan_" + indexSatuan + "_" + SessionUser.id + "_" + UnityEngine.Random.Range(1, 1000);
                marker.instance.tag = "Plot_Pasukan";
                marker.instance.layer = LayerMask.NameToLayer("PlayerUnit");
                marker.instance.transform.parent = GameObject.Find("Marker 2D").transform;
                marker.OnClick += OnMarkerClick;
                marker.OnPress += OnMarkerPress;
                marker.OnRelease += OnMarkerRelease;

                char simbolIndex = char.Parse(font);
                int sIndex = simbolIndex;

                Stat oS = marker.instance.GetComponent<Stat>();
                if (SessionUser.id != null)
                    oS.userID = (int)SessionUser.id;
                oS.icon = null;
                oS.nama = nama;
                oS.namaSatuan = marker.instance.name;
                oS.kecepatan = inputKecepatan.text;
                oS.satuanKecepatan = dropdownKecepatan.options[dropdownKecepatan.value].text;
                oS.heading = inputHeading.text;
                oS.fuel = inputBahanBakar.text;
                oS.matra = matra;
                oS.type = Stat.unitType.Pasukan;
                oS.keterangan = keterangan;
                oS.keteranganPas = keteranganPas;
                oS.font = font;
                oS.fontType = fontPas;
                oS.fontTypeID = fontPasID;
                oS.index = indexPas;
                oS.marker2D = this2D;
                oS.marker3D = this3D;
                listMarker2DOnMap.Add(oS.marker2D);
                var mNamaObj = marker.instance.transform.Find("Canvas/Nama");
                var m_meta = marker.instance.GetComponent<Metadata>();

                m_meta.FindParameter("Simbol").parameter.GetComponent<Text>().text = font;
                m_meta.FindParameter("Simbol").parameter.GetComponent<Text>().font = Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + fontPas);

                m_meta.FindParameter("Nama").parameter.GetComponent<TMP_Text>().text = oS.nama;

                if (SessionUser.nama_asisten == "ASOPS")
                {
                    marker.instance.transform.Find("Canvas/Simbol").GetChild(0).GetComponent<Image>().color = Color.blue;
                    marker.instance.transform.Find("Canvas/Nama").GetChild(0).GetComponent<Image>().color = Color.blue;
                    // marker.instance.transform.Find("Canvas/Image").GetChild(0).GetComponent<Text>().color = Color.blue;
                    oS.warna = "blue";
                }
                else if (SessionUser.nama_asisten == "ASINTEL")
                {
                    marker.instance.transform.Find("Canvas/Simbol").GetChild(0).GetComponent<Image>().color = Color.red;
                    marker.instance.transform.Find("Canvas/Nama").GetChild(0).GetComponent<Image>().color = Color.red;
                    // marker.instance.transform.Find("Canvas/Image").GetChild(0).GetComponent<Text>().color = Color.red;
                    oS.warna = "red";
                }
                else
                {
                    marker.instance.transform.Find("Canvas/Simbol").GetChild(0).GetComponent<Image>().color = Color.blue;
                    marker.instance.transform.Find("Canvas/Nama").GetChild(0).GetComponent<Image>().color = Color.blue;
                    // marker.instance.transform.Find("Canvas/Image").GetChild(0).GetComponent<Text>().color = Color.blue;
                    oS.warna = "";
                }
                // mNamaObj.gameObject.SetActive(true);

                marker3D["markerSource"] = marker3D;
                marker3D.label = marker.label;
                marker3D.sizeType = OnlineMapsMarker3D.SizeType.meters;
                marker3D.scale = 5f;
                marker3D.sizeType = OnlineMapsMarker3D.SizeType.meters;
                marker3D.range = new OnlineMapsRange(16f, 20f);
                marker3D.rotation = Quaternion.Euler(Vector3.up * float.Parse(oS.heading));
                marker3D.instance.name = marker.instance.name;
                marker3D.instance.tag = "Plot_Pasukan";
                marker3D.instance.layer = LayerMask.NameToLayer("PlayerUnit");
                marker3D.OnClick += OnMarkerClick;
                marker3D.OnPress += OnMarkerPress;
                marker3D.OnRelease += OnMarkerRelease;

                var jM = marker3D.instance.GetComponent<JSONDataMisiSatuan>();
                if(jM == null)
                jM = marker3D.instance.AddComponent<JSONDataMisiSatuan>();

                Stat oSS = marker3D.instance.GetComponent<Stat>();
                if(oSS == null)
                oSS = marker3D.instance.AddComponent<Stat>();
                
                if (SessionUser.id != null)
                oSS.userID = (int)SessionUser.id;
                oSS.nama = oS.nama;
                oSS.namaSatuan = oS.namaSatuan;
                oSS.kecepatan = oS.kecepatan;
                oSS.satuanKecepatan = oS.satuanKecepatan;
                oSS.heading = oS.heading;
                oSS.fuel = oS.fuel;
                oSS.matra = oS.matra;
                oSS.warna = oS.warna;
                oSS.keterangan = oS.keterangan;
                oSS.keteranganPas = oS.keteranganPas;
                oSS.font = oS.font;
                oSS.fontType = oS.fontType;
                oSS.fontTypeID = oS.fontTypeID;
                oSS.type = oS.type;
                oSS.index = oS.index;
                oSS.marker2D = oS.marker2D;
                oSS.marker3D = oS.marker3D;
                
                var pF = marker3D.instance.GetComponent<PFormasi>();
                if(pF == null)
                pF = marker3D.instance.AddComponent<PFormasi>();
                pF.stat = oSS;
                pF.marker2D = marker;
                pF.marker3D = marker3D;

                JObject label = new JObject
                {
                    { "label_symbol", null },
                    { "margin_bottom", -10 },
                    { "margin_right", 0 },
                    { "margin_top", 0 },
                    { "label_symbol_tab2", null },
                    { "margin_bottom_tab2", -10 },
                    { "margin_right_tab2", 0 },
                    { "margin_top_tab2", 0 },
                    { "label_symbol_tab3", null },
                    { "margin_bottom_tab3", -10 },
                    { "margin_right_tab3", 0 },
                    { "margin_top_tab3", 0 },
                    { "label_symbol_tab4", null },
                    { "margin_bottom_tab4", -10 },
                    { "margin_right_tab4", 0 },
                    { "margin_top_tab4", 0 },
                };

                oS.listPasukan = listPasukan;
                oSS.listPasukan = listPasukan;

                JObject styleSat = new JObject
                {
                    { "nama", fontPas },
                    { "index", sIndex },
                    { "keterangan", keteranganPas },
                    { "grup", "10" },
                    { "label_style", oS.listPasukan.style_symbol }
                };

                var info = new Info_Pasukan();
                info.nama_satuan = oS.nama;
                info.kecepatan = oS.kecepatan + "|" + oS.satuanKecepatan;
                info.armor = 100;
                info.nomer_satuan = "NS" + UnityEngine.Random.Range(1, 999);
                info.nomer_atasan = "AT" + UnityEngine.Random.Range(1, 999);
                info.tgl_selesai = "-";
                info.warna = oS.warna;
                info.size = 30;
                info.weapon = "[]";
                info.waypoint = new List<string>();
                info.list_embarkasi = new List<string>();
                info.id_dislokasi = false;
                info.id_dislokasi_obj = false;
                info.bahan_bakar = int.Parse(oS.fuel);
                info.bahan_bakar_load = 0;
                info.kecepatan_maks = "undefined";
                info.heading = inputHeading.text;
                info.ket_satuan = oS.keterangan;
                info.nama_icon_satuan = "";
                info.width_icon_satuan = "";
                info.height_icon_satuan = "";
                info.personil = 1;
                info.tfg = true;
                info.hidewgs = true;

                // var lStyle = Label_Style_Pasukan.FromJson(styleSat["label_style"].ToString());
                // Debug.Log(JsonConvert.SerializeObject(lStyle));

                oS.style = JsonConvert.SerializeObject(styleSat);
                oS.info = Info_Pasukan.ToString(info);
                oS.symbol = "<div style='text-align:center'><span style='font-weight: bolder; color: " + oS.warna + "; font-size: 21px; font-family: " + oS.fontType + "'>" + oS.font + "</span></div>";

                oSS.style = oS.style;
                oSS.info = oS.info;
                oSS.symbol = oS.symbol;

                Debug.Log(nama + " telah ditempatkan pada koordinat = " + lng + " , " + lat);
                indexSatuan++;
                marker.instance.SetActive(true);

                isPlottingInfan = false;
                OnlineMapsControlBase.instance.OnMapClick -= Plot;
                resetForm();
            }
        }

        private void OnChangeZoom()
        {
            OnlineMaps map = OnlineMaps.instance;
            foreach (OnlineMapsMarker3D marker in OnlineMapsMarker3DManager.instance)
            {
                ShowMarkerLabelsByZoomItem item = marker["data"] as ShowMarkerLabelsByZoomItem;
                if (item == null) continue;

                // Update marker labels.
                marker.label = item.zoomRange.InRange(map.zoom) ? item.label : "";
            }
        }

        public class ShowMarkerLabelsByZoomItem
        {
            /// <summary>
            /// Zoom range where you need to show the label.
            /// </summary>
            public OnlineMapsRange zoomRange;

            /// <summary>
            /// Label.
            /// </summary>
            public string label;

            public ShowMarkerLabelsByZoomItem(string label, OnlineMapsRange zoomRange)
            {
                this.label = label;
                this.zoomRange = zoomRange;
            }
        }

        public void OnUpdateBefore()
        {
            var cameraOrbit = OnlineMapsCameraOrbit.instance;
            foreach (OnlineMapsMarker3D _marker in listMarker2DOnMap)
                _marker.rotation = Quaternion.Euler(cameraOrbit.rotation.x, cameraOrbit.rotation.y, 0);
        }

        public void OnMarkerClick(OnlineMapsMarkerBase markerBase)
        {
            resetForm();
            OnlineMapsMarker3D marker = markerBase as OnlineMapsMarker3D;

            formasiBtn.onClick.AddListener(delegate{PlotFormasiController.instance.EditMenu(marker, toggle);});

            Stat oS = marker.instance.GetComponent<Stat>();
            OnPasukanSelected(oS.font, oS.index, oS.nama, oS.matra, oS.keteranganPas, oS.fontType, oS.fontTypeID, oS.listPasukan);

            var info = Info_Pasukan.FromJson(oS.info);

            inputKecepatan.text = oS.kecepatan;
            inputKeterangan.text = oS.keterangan;
            inputBahanBakar.text = info.bahan_bakar.ToString();
            inputHeading.text = oS.heading;

            if (oS.satuanKecepatan == "Kilometer")
                dropdownKecepatan.value = 0;
            else if (oS.satuanKecepatan == "Miles")
                dropdownKecepatan.value = 1;
            else if (oS.satuanKecepatan == "Knot")
                dropdownKecepatan.value = 2;
            else if (oS.satuanKecepatan == "Mach")
                dropdownKecepatan.value = 3;


            edit2D = oS.marker2D;
            edit3D = oS.marker3D;

            plotBtn.SetActive(false);
            editBtn.SetActive(true);
            moveBtn.SetActive(true);
            deleteBtn.SetActive(true);
            
            if(marker.instance.GetComponent<PFormasi>().formasi != PFormasi.formasiType.None)
            editMenu.SetActive(true);
            else
            editMenu.SetActive(false);

            var pF = marker.instance.GetComponent<PFormasi>();

            if (pF.formasi != PFormasi.formasiType.None)
            {
                if (pF.urutanFormasi == 0)
                    kegiatanBtn.SetActive(true);
            }
            kegiatanBtn.SetActive(true);

            toggle.toggleOn();
        }

        public void OnMarkerPress(OnlineMapsMarkerBase markerBase)
        {
            if (isMoving)
            {
                isDragging = true;

                double lng, lat;

                if (OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
                    prevWorldPos = new Vector2((float)lng, (float)lat);

                OnlineMaps.instance.GetComponent<OnlineMapsTileSetControl>().allowUserControl = false;
            }
        }

        public void OnMarkerRelease(OnlineMapsMarkerBase markerBase)
        {
            if (isMoving)
            {
                isDragging = false;

                OnlineMaps.instance.GetComponent<OnlineMapsTileSetControl>().allowUserControl = true;
            }
        }

        public void Edit()
        {
            if (inputNama.text == "")
                Debug.Log("Nama tidak boleh kosong!!");
            else if (inputKeterangan.text == "")
                Debug.Log("Keterangan tidak boleh kosong!!!");
            else if (!haveSelected)
                Debug.Log("Anda Belum Memilih Pasukan!!!");
            else
            {
                nama = inputNama.text;
                keterangan = inputKeterangan.text;
                font = selectedPasukanGroup.GetComponent<Metadata>().FindParameter("pasukan-symbol").parameter.GetComponent<Text>().text;

                if (inputHeading.text == "" || inputHeading.text == "-" || inputHeading.text == ".")
                    inputHeading.text = "0";

                edit2D.instance.transform.Find("Canvas/Image").GetChild(0).GetComponent<Text>().text = font;
                edit2D.instance.transform.Find("Canvas/Image").GetChild(0).GetComponent<Text>().font = Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + fontPas);

                Stat oS = edit2D.instance.GetComponent<Stat>();
                Stat oSS = edit3D.instance.GetComponent<Stat>();

                oS.nama = nama;
                oS.kecepatan = inputKecepatan.text;
                oS.satuanKecepatan = dropdownKecepatan.options[dropdownKecepatan.value].text;
                oS.fuel = inputBahanBakar.text;
                oS.heading = inputHeading.text;
                oS.matra = matra;
                oS.type = Stat.unitType.Pasukan;
                oS.keterangan = keterangan;
                oS.keteranganPas = keteranganPas;
                oS.font = font;
                oS.fontType = fontPas;
                oS.fontTypeID = fontPasID;
                oS.index = indexPas;

                oSS.nama = oS.nama;
                oSS.namaSatuan = oS.namaSatuan;
                oSS.kecepatan = oS.kecepatan;
                oSS.satuanKecepatan = oS.satuanKecepatan;
                oSS.heading = oS.heading;
                oSS.fuel = oS.fuel;
                oSS.matra = oS.matra;
                oSS.warna = oS.warna;
                oSS.keterangan = oS.keterangan;
                oSS.keteranganPas = oS.keteranganPas;
                oSS.font = oS.font;
                oSS.fontType = oS.fontType;
                oSS.fontTypeID = oS.fontTypeID;
                oSS.type = oS.type;
                oSS.index = oS.index;
                oSS.marker3D.rotation = Quaternion.Euler(Vector3.up * float.Parse(inputHeading.text));

                char simbolIndex = char.Parse(font);
                int sIndex = simbolIndex;

                JObject label = new JObject
                {
                    { "label_symbol", null },
                    { "margin_bottom", -10 },
                    { "margin_right", 0 },
                    { "margin_top", 0 },
                    { "label_symbol_tab2", null },
                    { "margin_bottom_tab2", -10 },
                    { "margin_right_tab2", 0 },
                    { "margin_top_tab2", 0 },
                    { "label_symbol_tab3", null },
                    { "margin_bottom_tab3", -10 },
                    { "margin_right_tab3", 0 },
                    { "margin_top_tab3", 0 },
                    { "label_symbol_tab4", null },
                    { "margin_bottom_tab4", -10 },
                    { "margin_right_tab4", 0 },
                    { "margin_top_tab4", 0 },
                };

                oS.listPasukan = listPasukan;
                oSS.listPasukan = listPasukan;

                JObject styleSat = new JObject
                {
                    { "nama", fontPas },
                    { "index", sIndex },
                    { "keterangan", keteranganPas },
                    { "grup", "10" },
                    { "label_style", oS.listPasukan.style_symbol }
                };

                var info = new Info_Pasukan();
                info.nama_satuan = oS.nama;
                info.kecepatan = oS.kecepatan + "|" + oS.satuanKecepatan;
                info.armor = 100;
                info.nomer_satuan = oS.listPasukan.nomer_satuan;
                info.nomer_atasan = oS.listPasukan.nomer_atasan;
                info.tgl_selesai = "-";
                info.warna = oS.warna;
                info.size = 30;
                info.weapon = "[]";
                info.waypoint = new List<string>();
                info.list_embarkasi = new List<string>();
                info.id_dislokasi = false;
                info.id_dislokasi_obj = false;
                info.bahan_bakar = int.Parse(oS.fuel);
                info.bahan_bakar_load = 0;
                info.kecepatan_maks = "undefined";
                info.heading = inputHeading.text;
                info.ket_satuan = oS.keterangan;
                info.nama_icon_satuan = "";
                info.width_icon_satuan = "";
                info.height_icon_satuan = "";
                info.personil = 1;
                info.tfg = true;
                info.hidewgs = true;

                // var lStyle = Label_Style_Pasukan.FromJson(styleSat["label_style"].ToString());
                // Debug.Log(JsonConvert.SerializeObject(lStyle));

                oS.style = JsonConvert.SerializeObject(styleSat);
                oS.info = Info_Pasukan.ToString(info);
                oSS.style = oS.style;
                oSS.info = oS.info;
                
                edit2D.instance.SetActive(false);
                edit2D.instance.SetActive(true);

                resetForm();
                toggle.toggleOff();
            }
        }

        public void MoveOn()
        {
            if (edit2D.instance.GetComponent<PFormasi>().formasi == PFormasi.formasiType.None)
            {
                isMoving = !isMoving;

                foreach (Transform child in map.transform)
                {
                    if (child.GetComponentInChildren<BoxCollider>() != null)
                        child.GetComponentInChildren<BoxCollider>().enabled = !child.GetComponentInChildren<BoxCollider>().enabled;
                }

                edit2D.instance.GetComponent<BoxCollider>().enabled = true;
                edit3D.instance.GetComponent<BoxCollider>().enabled = true;
            }
            else
            {
                Debug.Log("Unit Ini Sedang Berada Dalam Formasi Dan Tidak Dapat Dipindah!!!");
            }
        }

        public void Move()
        {
            if (isMoving && isDragging)
            {

                double lng, lat;

                if (OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
                    newWorldPos = new Vector2((float)lng, (float)lat);

                if (prevWorldPos != newWorldPos)
                {
                    edit2D.position = newWorldPos;
                    edit3D.position = newWorldPos;
                }
            }
        }

        public void Delete()
        {
            if(edit3D.instance.GetComponent<PFormasi>().formasi == PFormasi.formasiType.None)
            {
                var trash = GameObject.Find("Trash");
                if(trash == null)
                {
                    trash = new GameObject("Trash");
                    trash.AddComponent<TrashObj>();
                }

                var tObj = new Trash();
                tObj.id = edit3D.instance.name;
                tObj.type = "satuan";

                var tScript = trash.GetComponent<TrashObj>();
                tScript.trash.Add(tObj);

                EntityEditorController.Instance.plotTrash.Add(
                    new JObject()
                    {
                        { "id", edit3D.instance.name },
                        { "type", "satuan" }
                    }
                );

                var jM2D = edit2D.instance.GetComponent<JSONDataMisiSatuan>();
                var jM3D = edit3D.instance.GetComponent<JSONDataMisiSatuan>();

                jM2D.listRoute = new List<PlotDataRoute>();

                for(int i = 0; i < jM3D.listRoute.Count; i++)
                {
                    var ttObj = new Trash();
                    ttObj.id = jM3D.listRoute[i].gameObject.name;
                    ttObj.type = "misi";
                    tScript.trash.Add(ttObj);

                    Destroy(jM3D.listRoute[i].gameObject);
                    jM3D.listRoute.RemoveAt(i);
                }
                jM3D.listRoute = new List<PlotDataRoute>();

                listMarker2DOnMap.Remove(edit2D);
                OnlineMapsMarker3DManager.RemoveItem(edit2D);
                OnlineMapsMarker3DManager.RemoveItem(edit3D);
                indexSatuan--;

                resetForm();
                toggle.toggleOff();
            }
            else
            Debug.Log("Unit ini sedang berada dalam formasi!!");
        }
    }
}
