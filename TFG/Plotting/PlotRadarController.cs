using System.Threading.Tasks;
using UnityEngine;

using TMPro;
using HelperEditor;
using UnityEngine.UI;
using GlobalVariables;

public class PlotRadarController : MonoBehaviour
{
    public enum plotMode { CREATE, EDIT, ONPLOT, ONMOVE }
    [Header("Plot Mode")]
    public plotMode mode;

    [Header("UI References")]
    public TextMeshProUGUI label_header;
    public TextMeshProUGUI label_btnPlot;
    public GameObject label_notification;
    public SidemenuToggler btnToggler;
    public GameObject container_selected;
    public GameObject edit_tools;

    [Header("Form Data")]
    public TMP_InputField input_nama;
    public TMP_InputField input_radius;
    public TMP_Dropdown dropdown_jenis;

    // TEMP DATA STORAGE
    private ListRadarData SELECTED_DATA;
    private OnlineMapsMarker3D SELECTED_MARKER;

    #region INSTANCE
    public static PlotRadarController Instance;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion
    
    void Start()
    {
        OnlineMapsControlBase.instance.OnMapClick += OnMapClick;
    }

    #region LIST RADAR
    // Add Data Radar ke Dalam ScrollView
    public async Task AddToList(GetListRadar[] listRadar)
    {
        if (listRadar == null) return;
        if (listRadar.Length <= 0) return;
        var scroller = GetComponent<PlotRadarScroller>();

        for (int i=0; i < listRadar.Length; i++)
        {
            // Set ListInfoRadar Data dari Data Service
            var objData = new ListRadarData();
            var objDataInfo = objData.info_radar = new ListInfoRadar();

            objDataInfo.judul = listRadar[i].RADAR_NAME;
            objDataInfo.radius = listRadar[i].RADAR_DET_RANGE;
            objDataInfo.size = "30"; // Unknown Service To Get This Data!


            objDataInfo.id_symbol = new ListIDSymbolInfoRadar();
            objDataInfo.id_symbol.id = listRadar[i].id;
            objDataInfo.id_symbol.nama = listRadar[i].nama;
            objDataInfo.id_symbol.index = listRadar[i].index;
            objDataInfo.id_symbol.keterangan = listRadar[i].keterangan;
            objDataInfo.id_symbol.grup = listRadar[i].grup;
            objDataInfo.id_symbol.entity_name = listRadar[i].entity_name;
            objDataInfo.id_symbol.entity_kategori = listRadar[i].entity_kategori;
            objDataInfo.id_symbol.jenis_role = listRadar[i].jenis_role;
            objDataInfo.id_symbol.health = listRadar[i].health;

            objData.nama_radar = listRadar[i].RADAR_NAME;
            objData.desc_radar = listRadar[i].keterangan;
            objData.index = listRadar[i].index;
            objData.fontFamily = Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + listRadar[i].nama);

            // Add Data Ke Dalam ListView
            scroller.AddData(objData);
        }
        scroller.scroller.ReloadData();
    }

    // Ketika List Pada ScrollView Dipilih
    public void OnListSelected(ListRadarData objData)
    {
        ShowSelectedContainer(objData.nama_radar, objData.index, objData.fontFamily);

        // Set Form Data Berdasarkan Data List yang di-select
        input_nama.text = objData.nama_radar;
        input_radius.text = (objData.info_radar.radius / 1852).ToString();
        dropdown_jenis.value = 0;

        SELECTED_DATA = objData;
    }
    #endregion

    #region PLOTTING
    /// <summary>
    /// Reset form ke posisi default
    /// </summary>
    public void ResetForm()
    {
        // Reset Form
        input_nama.text         = "";
        input_radius.text       = "1";
        dropdown_jenis.value    = 0;
        SELECTED_DATA           = null;
        SELECTED_MARKER         = null;

        // Reset Form Label
        label_header.text       = "PLOT RADAR";
        label_btnPlot.text      = "PLOT";
        container_selected.SetActive(false);
        edit_tools.SetActive(false);

        mode = plotMode.CREATE;

        PlotNotificationHelper.Instance.TogglePlotNotification(false);
    }

    /// <summary>
    /// Ketika Button Plot pada Form Di Klik
    /// (Bertindak sebagai plot object baru jika masuk ke dalam mode create dan save perubahan data object dipilih ketika masuk dalam mode edit)
    /// </summary>
    public void OnButtonPlotClick()
    {
        if(input_nama.text == "" || input_radius.text == "" || SELECTED_DATA == null)
        {
            // Notifikasi Data Form Belum Lengkap
            return;
        }

        // Jika Data Form Sudah Lengkap, Eksekusi Aksi Create / Save Object Aktif
        if (mode == plotMode.EDIT)
        {
            SaveData();
            return;
        }

        GetComponent<Animator>().Play("SlideOut");
        mode = plotMode.ONPLOT;
        PlotNotificationHelper.Instance.TogglePlotNotification(true);
    }

    /// <summary>
    /// Ketika Marker Di Klik
    /// (Bertindak untuk menampilkan form dengan data dari marker tersebut & juga mengubah mode ke dalam mode edit
    /// </summary>
    public void OnMarkerClick(OnlineMapsMarkerBase markerBase)
    {
        // Marker Click Tidak Akan Berpengaruh Jika Plotting Sedang Dalam Mode ONPLOT / ONMOVE
        if (mode == plotMode.ONPLOT || mode == plotMode.ONMOVE) return;
        btnToggler.toggleOn();

        mode = plotMode.EDIT;
        label_header.text       = "EDIT RADAR";
        label_btnPlot.text      = "SAVE";
        edit_tools.SetActive(true);

        OnlineMapsMarker3D marker = markerBase["markerSource"] as OnlineMapsMarker3D;
        var marker_data = marker.instance.GetComponent<JSONDataRadar>();

        SELECTED_DATA = new ListRadarData();
        SELECTED_DATA.info_radar = ListInfoRadar.FromJson(marker_data.info_radar);
        SELECTED_DATA.nama_radar = SELECTED_DATA.info_radar.judul;
        SELECTED_DATA.desc_radar = SELECTED_DATA.info_radar.id_symbol.keterangan;
        SELECTED_DATA.index = SELECTED_DATA.info_radar.id_symbol.index;
        SELECTED_DATA.fontFamily = Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + SELECTED_DATA.info_radar.id_symbol.nama);
        SELECTED_MARKER = marker;   

        var objDataInfo = ListInfoRadar.FromJson(marker_data.info_radar);
        input_nama.text = objDataInfo.judul;
        input_radius.text = (objDataInfo.radius / 1852).ToString();
        dropdown_jenis.value = (objDataInfo.jenis_radar == "Biasa") ? 0 : 1;

        ShowSelectedContainer(objDataInfo.judul, objDataInfo.id_symbol.index, Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + objDataInfo.id_symbol.nama));
    }

    /// <summary>
    /// Event Ketika Click pada Peta
    /// (Bertindak untuk Plot Object)
    /// </summary>
    public async void OnMapClick()
    {
        if (SELECTED_DATA == null) return;
        if (mode != plotMode.ONPLOT) return;

        // Get the coordinates under the cursor.
        double lng, lat;
        OnlineMapsControlBase.instance.GetCoords(out lng, out lat);

        Debug.Log("Plot New Radar At (" + lng + ", " + lat + ")");

        // Set Form Data Into Data Radar
        SELECTED_DATA.info_radar.nama = generateIDRadar();
        SELECTED_DATA.info_radar.judul = input_nama.text;
        SELECTED_DATA.info_radar.radius = (int.Parse(input_radius.text) > 0) ? int.Parse(input_radius.text) * 1852 : 1 * 1852;
        SELECTED_DATA.info_radar.jenis_radar = dropdown_jenis.options[dropdown_jenis.value].text;
        SELECTED_DATA.info_radar.warna = (SessionUser.nama_asisten == "ASOPS") ? "blue" : "red";
        SELECTED_DATA.info_radar.id_user = (int)SessionUser.id;
        SELECTED_DATA.info_radar.new_index = SELECTED_DATA.index.ToString();

        var symbol = "{\"className\":\"my-div-icon\",\"html\":\"<div class='radar-fix' style='margin-top:-15px; width: 30px;color: " + ((SessionUser.nama_asisten == "ASOPS") ? "blue" : "red") + "; font-size: 30px; font-family: "+ SELECTED_DATA.fontFamily.name +"'>"+ SELECTED_DATA.index +"</div> \",\"iconSize\":[20,20],\"iconAnchor\":[20,5],\"id_point\":\"\",\"id_radar\":\"\"}";
        var info_symbol = "";
        //await EntityEditorController.Instance.SpawnEntityRadar((float)lng, (float)lat, prefabRadar, SELECTED_DATA, symbol);

        var marker = OnlineMapsMarker3DManager.CreateItem(new Vector2((float)lng, (float)lat), AssetPackageManager.Instance.prefabRadar);
        marker.instance.name = SELECTED_DATA.info_radar.nama;

        await EntityEditorController.Instance.SetJSONRadarAndHUD(marker, SELECTED_DATA, symbol, info_symbol);

        ResetForm();
        btnToggler.toggleOff();

        PlotNotificationHelper.Instance.TogglePlotNotification(false);
    }

    /// <summary>
    /// Ketika Marker di Klik Dalam Mode OnMove
    /// </summary>
    /// <param name="markerBase"></param>
    private void ToggleDragMarker(OnlineMapsMarkerBase markerBase)
    {
        // Starts moving the marker.
        OnlineMapsControlBase.instance.dragMarker = markerBase;
        OnlineMapsControlBase.instance.isMapDrag = false;
    }

    /// <summary>
    /// Ketika Marker di Drag Dalam Mode OnMove
    /// </summary>
    /// <param name="markerBase"></param>
    private void OnMarkerDrag(OnlineMapsMarkerBase markerBase)
    {
        double lng, lat;
        markerBase.GetPosition(out lng, out lat);

        OnlineMapsMarker3D marker = markerBase["markerSource"] as OnlineMapsMarker3D;
        marker.label = lng + ", " + lat;
        marker.instance.GetComponent<JSONDataRadar>().lng_x = lng;
        marker.instance.GetComponent<JSONDataRadar>().lat_y = lat;
    }

    /// <summary>
    /// Save perubahan data pada object terpilih
    /// </summary>
    public async void SaveData()
    {
        var dataRadar = SELECTED_MARKER.instance.GetComponent<JSONDataRadar>();

        // Set Perubahan Data Radar Dari Form Data
        SELECTED_DATA.info_radar.judul = input_nama.text;
        SELECTED_DATA.info_radar.warna = (SessionUser.nama_asisten == "ASOPS") ? "blue" : "red";
        SELECTED_DATA.info_radar.radius = (int.Parse(input_radius.text) > 0) ? int.Parse(input_radius.text) * 1852 : 1 * 1852;
        SELECTED_DATA.info_radar.jenis_radar = dropdown_jenis.options[dropdown_jenis.value].text;

        dataRadar.info_radar = ListInfoRadar.ToString(SELECTED_DATA.info_radar);

        var marker = OnlineMapsMarker3DManager.CreateItem(new Vector2((float)dataRadar.lng_x, (float)dataRadar.lat_y), AssetPackageManager.Instance.prefabRadar);
        marker.instance.name = dataRadar.nama;
        await EntityEditorController.Instance.SetJSONRadarAndHUD(marker, SELECTED_DATA, dataRadar.symbol, dataRadar.info_radar);

        // Respawn Saved Object
        //await EntityEditorController.Instance.SpawnEntityRadar((float)dataRadar.lng_x, (float)dataRadar.lat_y, prefabRadar, SELECTED_DATA, dataRadar.symbol);

        // Delete Old Marker
        OnlineMapsMarker3DManager.RemoveItem(SELECTED_MARKER);

        btnToggler.toggleOff();
        ResetForm();
    }

    /// <summary>
    /// Ubah Lokasi Object Terpilih
    /// </summary>
    public void MoveObject()
    {
        if (SELECTED_MARKER == null) return;
        PlotNotificationHelper.Instance.TogglePlotNotification(true, "Drag the object to change position");

        double lng, lat;
        SELECTED_MARKER.GetPosition(out lng, out lat);
        SELECTED_MARKER.label = lng + ", " + lat;

        SELECTED_MARKER.OnPress = ToggleDragMarker;
        SELECTED_MARKER.OnDrag = OnMarkerDrag;
        mode = plotMode.ONMOVE;
        GetComponent<Animator>().Play("SlideOut");
    }

    /// <summary>
    /// Hapus Object Terpilih
    /// </summary>
    public void DeleteObject()
    {
        if (SELECTED_MARKER == null) return;
        OnlineMapsMarker3DManager.RemoveItem(SELECTED_MARKER);

        Debug.Log("Data Radar Berhasil Dihapus!");
        btnToggler.toggleOff();
    }

    #endregion

    #region HELPER
    private void ShowSelectedContainer(string nama_radar, int index, Font fontFamily)
    {
        container_selected.SetActive(true);

        var metadata = container_selected.GetComponent<Metadata>();
        metadata.FindParameter("nama-radar").parameter.GetComponent<TextMeshProUGUI>().text = nama_radar;
        metadata.FindParameter("simbol-radar").parameter.GetComponent<Text>().text = ((char) index).ToString();
        metadata.FindParameter("simbol-radar").parameter.GetComponent<Text>().font = fontFamily;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(mode == plotMode.ONPLOT)
            {
                mode = plotMode.CREATE;

                PlotNotificationHelper.Instance.TogglePlotNotification(false);
                GetComponent<Animator>().Play("SlideIn");
            }
            else if(mode == plotMode.ONMOVE)
            {
                mode = plotMode.EDIT;
                SELECTED_MARKER.OnPress = null;
                SELECTED_MARKER.OnDrag = null;
                SELECTED_MARKER.label = "";

                PlotNotificationHelper.Instance.TogglePlotNotification(false);
                GetComponent<Animator>().Play("SlideIn");
            }
        }
    }

    private string generateIDRadar()
    {
        var index_radar = GameObject.FindGameObjectsWithTag("entity-radar").Length;
        bool isExist = true;

        do
        {
            isExist = checkRadarIndex(index_radar);
            index_radar++;
        } while (isExist);

        return "radar_" + index_radar + "_" + SessionUser.id + "_" + UnityEngine.Random.Range(0, 999);
    }

    private bool checkRadarIndex(int index)
    {
        bool isExist = false;

        foreach (GameObject radar in GameObject.FindGameObjectsWithTag("entity-radar"))
        {
            if (radar.name.Contains("radar_" + index)) isExist = true;
        }

        return isExist;
    }
    #endregion
}
