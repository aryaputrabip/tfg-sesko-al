using System.Threading.Tasks;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using GlobalVariables;
using HelperEditor;
using HelperPlotting;
using TMPro;
using EnhancedUI.EnhancedScroller;
using SickscoreGames.HUDNavigationSystem;
using System;
using System.Linq;
using ObjectStat;
using Plot.Pasukan;
using Newtonsoft.Json.Linq;
using Plot.FormasiPlot;

using static Plot.FormasiPlot.PerhitunganFormasiV2;

public class PlotSatuanController : MonoBehaviour
{
    public enum plotMode { CREATE, EDIT, DELETE, ONPLOT, ONMOVE }

    [Header("Plot Mode")]
    public plotMode mode;

    [Header("UI References")]
    public TextMeshProUGUI label_header;
    public TextMeshProUGUI label_btnPlot;
    public GameObject selectedSatuanGroup;
    public GameObject editOptionGroup;

    [Header("Scrollview References")]
    //public GameObject prefabList;
    //public Transform listContainer;
    public EnhancedScroller listDarat;
    public EnhancedScroller listLaut;
    public EnhancedScroller listUdara;

    [Header("External - References")]
    public Animator plotStateLabel;
    public SidemenuToggler btnToggler;
    public SidemenuToggler formasiToggler;

    [Header("Form Data")]
    public TMP_InputField input_nama;
    public TMP_InputField input_noSatuan;
    public TMP_InputField input_noAtasan;
    public TMP_InputField input_kecepatan;
    public CircleSlider knobHeading;
    public TMP_InputField inputHeading;
    public TMP_Dropdown dropdown_typeKecepatan;
    public TMP_InputField input_fuel;
    public TMP_InputField input_keterangan;
    public TMP_InputField input_search;
    public Button formasiBtn;
    public SidemenuToggler toggler;

    [Header("References - Asset Satuan")]
    public GameObject ModalAssetSatuan;
    public EnhancedScroller AS_listDarat;
    public EnhancedScroller AS_listLaut;
    public EnhancedScroller AS_listUdara;

    [Header("Filter - Asset Satuan")]
    [SerializeField] private Metadata selectedFilterBtn;
    [SerializeField] private Metadata selectedAssetFilterBtn;

    // Selected Satuan Beserta Datanya Disimpan Sementara Disini:
    private AlutsistaBundleData selectedPrefabObj;
    private PlotSatuanCellView selectedDataObj;
    private InfoListSatuan selectedFormObj;
    private JSONDataSatuan selectedEditObj;
    public OnlineMapsMarker3D selectedEditMarker;
    // ----------------------------------------------------------

    #region INSTANCE
    public static PlotSatuanController Instance;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    void Start()
    {
        OnlineMapsControlBase.instance.OnMapClick += OnMapClick;
    }

    public async Task AddToList(ListSatuan[] listSatuan, StyleListSatuan[] styleListSatuan, ListSatuanData.typeObject tipe_tni)
    {
        if (listSatuan == null) return;

        var scrollerAD = GetComponent<PlotSatuanADScroller>();
        var scrollerAL = GetComponent<PlotSatuanALScroller>();
        var scrollerAU = GetComponent<PlotSatuanAUScroller>();
        var AS_scrollerAD = ModalAssetSatuan.GetComponent<PlotSatuanADScroller>();
        var AS_scrollerAL = ModalAssetSatuan.GetComponent<PlotSatuanALScroller>();
        var AS_scrollerAU = ModalAssetSatuan.GetComponent<PlotSatuanAUScroller>();

        for (int i = 0; i < listSatuan.Length; i++)
        {
            //Debug.Log(listSatuan.Length);
            if (listSatuan[i].ID == null) continue;

            //var objList = Instantiate(prefabList, listContainer);

            if (!AssetPackageManager.Instance.ASSETS_NAME.Contains(listSatuan[i].nama_object, StringComparer.OrdinalIgnoreCase)) continue;
            
            //var metadata = objList.GetComponent<Metadata>();
            var objData         = new ListSatuanData();
            objData.id          = listSatuan[i].ID;
            objData.name        = listSatuan[i].NAME;
            objData.tipe_obj    = tipe_tni;
            objData.fontFamily  = styleListSatuan[i].nama;
            objData.index       = ((char)styleListSatuan[i].index).ToString();
            objData.styleSatuan = styleListSatuan[i];
            objData.id_symbol   = listSatuan[i].ID;
            objData.nomor       = listSatuan[i].NO;
            objData.atasan      = null;
            //objData.jenis_obj   = (ListSatuanData.jenisObject)listSatuan[i].id_jenis_object.Value;
            //objData.objPrefab.id_symbol = listSatuan[i].ID;

            Sprite objIcon = null;
            if (listSatuan[i].path_object_3d != "" && listSatuan[i].path_object_3d != null)
            {
                var obj3DName = GetPathObj3DName(listSatuan[i].path_object_3d);
                objData.obj3D = obj3DName;
                objData.objPrefab = getAssetSatuan(objData.name, obj3DName, objData.tipe_obj).GetComponent<AlutsistaBundleData>();

                objIcon = objData.objPrefab.image;
            }
            else
            {
                objData.objPrefab = getAssetSatuan("", "", objData.tipe_obj).GetComponent<AlutsistaBundleData>();
            }

            objData.objPrefab.id_symbol = listSatuan[i].ID;

            // Add List Into ListView
            switch (tipe_tni)
            {
                case ListSatuanData.typeObject.Darat:
                    
                    scrollerAD.AddData(listSatuan[i].NAME, styleListSatuan[i].keterangan, objIcon, objData.index, Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + objData.fontFamily), objData);
                    //scrollerAD.scroller.ReloadData();
                    break;
                case ListSatuanData.typeObject.Laut:
                    scrollerAL.AddData(listSatuan[i].NAME, styleListSatuan[i].keterangan, objIcon, objData.index, Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + objData.fontFamily), objData);
                    //scrollerAL.scroller.ReloadData();
                    break;
                case ListSatuanData.typeObject.Udara:
                    scrollerAU.AddData(listSatuan[i].NAME, styleListSatuan[i].keterangan, objIcon, objData.index, Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + objData.fontFamily), objData);
                    //scrollerAU.scroller.ReloadData();
                    break;
                default: break;
            }

            // Add List Into ListView (Untuk List Alutsista di Modal "Asset Satuan"
            switch (tipe_tni)
            {
                case ListSatuanData.typeObject.Darat:
                    AS_scrollerAD.AddData(listSatuan[i].NAME, styleListSatuan[i].keterangan, objIcon, objData.index, Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + objData.fontFamily), objData);
                    //AS_scrollerAD.scroller.ReloadData();
                    break;
                case ListSatuanData.typeObject.Laut:
                    AS_scrollerAL.AddData(listSatuan[i].NAME, styleListSatuan[i].keterangan, objIcon, objData.index, Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + objData.fontFamily), objData);
                   // AS_scrollerAL.scroller.ReloadData();
                    break;
                case ListSatuanData.typeObject.Udara:
                    AS_scrollerAU.AddData(listSatuan[i].NAME, styleListSatuan[i].keterangan, objIcon, objData.index, Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + objData.fontFamily), objData);
                    //AS_scrollerAU.scroller.ReloadData();
                    break;
                default: break;
            }

            //LIST_DATA.Add(objData);
        }

        switch (tipe_tni)
        {
            case ListSatuanData.typeObject.Darat:
                scrollerAD.scroller.ReloadData();
                AS_scrollerAD.scroller.ReloadData();
                break;
            case ListSatuanData.typeObject.Laut:
                scrollerAL.scroller.ReloadData();
                AS_scrollerAL.scroller.ReloadData();
                break;
            case ListSatuanData.typeObject.Udara:
                scrollerAU.scroller.ReloadData();
                AS_scrollerAU.scroller.ReloadData();
                break;
            default: break;
        }
    }

    public void OnSatuanSelected(PlotSatuanScrollerData objData, PlotSatuanCellView cellData)
    {
        if(!selectedSatuanGroup.activeSelf) selectedSatuanGroup.SetActive(true);
        var metadata = selectedSatuanGroup.GetComponent<Metadata>();
        metadata.FindParameter("image-satuan").parameter.GetComponent<UnityEngine.UI.Image>().sprite = objData.image_satuan;
        metadata.FindParameter("nama-satuan").parameter.GetComponent<TextMeshProUGUI>().text = objData.nama_satuan;
        metadata.FindParameter("simbol-satuan").parameter.GetComponent<Text>().text = objData.index;
        metadata.FindParameter("simbol-satuan").parameter.GetComponent<Text>().font = objData.fontFamily;

        input_nama.text = objData.nama_satuan;
        input_noSatuan.text = (objData.nomor_satuan != null) ? objData.nomor_satuan : "NS" + UnityEngine.Random.Range(1, 999);
        input_noAtasan.text = (objData.nomor_atasan != null) ? objData.nomor_atasan : "ATS" + UnityEngine.Random.Range(1, 999);

        selectedPrefabObj = objData.ListSatuanData.objPrefab;
        selectedDataObj = cellData;
    }
    
    public void Formasi()
    {
        GetComponent<Animator>().Play("SlideOut");

        var pF = selectedEditMarker.instance.GetComponent<PFormasiV2>();
        var fS = pF.formasi;
        var fController = PlotFormasiControllerV2.instance;
        fController.selectedMarker = selectedEditMarker;
        fController.selectedUnits.AddRange(fS.unitsInFormasi);
        fController.canvas.toggler.toggleOn();
        fController.fromOtherCanvas = true;
        fController.isEditing = true;

        var fCanvas = fController.canvas;
        fCanvas.namaFormasi.text = fS.namaFormasi;
        fCanvas.formasiDropdown.value = JenisFormasi(fS.type);
        fCanvas.jarakDropdown.value = JenisSatuanJarak(fS.satuan, true) - 1;
        fCanvas.arah.text = fS.arahUtama.ToString();
        fCanvas.btnPlot.SetActive(false);
        fCanvas.btnEdit.SetActive(true);
        fCanvas.btnMove.SetActive(true);
        fCanvas.btnDelete.SetActive(true);
        fCanvas.btnKegiatan.SetActive(true);
    }
    
    public void resetForm()
    {
        if(selectedSatuanGroup.activeSelf) selectedSatuanGroup.SetActive(false);
        
        input_nama.text = "";
        input_noSatuan.text = "";
        input_noAtasan.text = "";
        input_kecepatan.text = "1";
        knobHeading.reset();
        inputHeading.text = "0";
        dropdown_typeKecepatan.value = 0;
        input_fuel.text = "40";
        input_keterangan.text = "";

        selectedDataObj = null;
        selectedPrefabObj = null;
        selectedFormObj = null;
        selectedEditObj = null;
        selectedEditMarker = null;

        label_header.text = "PLOT SATUAN";
        label_btnPlot.text = "PLOT";

        editOptionGroup.SetActive(false);

        var metadataEditOptions = editOptionGroup.GetComponent<Metadata>();
        metadataEditOptions.FindParameter("menu-move").parameter.GetComponent<Button>().interactable = true;
        metadataEditOptions.FindParameter("menu-misi").parameter.GetComponent<Button>().interactable = true;
        metadataEditOptions.FindParameter("menu-formasi").parameter.GetComponent<Button>().interactable = true;
        metadataEditOptions.FindParameter("menu-delete").parameter.GetComponent<Button>().interactable = true;

        mode = plotMode.CREATE;
    }

    public void resetSelectedEditMarker()
    {
        selectedEditObj = null;
        selectedEditMarker = null;
    }

    public void FilterButtonUpdate(GameObject button)
    {
        if(selectedFilterBtn != null)
        {
            selectedFilterBtn.FindParameter("selected-overlay").parameter.SetActive(false);
        }

        selectedFilterBtn = button.GetComponent<Metadata>();
        selectedFilterBtn.FindParameter("selected-overlay").parameter.SetActive(true);
    }

    public void FilterButtonAssetUpdate(GameObject button)
    {
        if (selectedAssetFilterBtn != null)
        {
            selectedAssetFilterBtn.FindParameter("selected-overlay").parameter.SetActive(false);
        }

        selectedAssetFilterBtn = button.GetComponent<Metadata>();
        selectedAssetFilterBtn.FindParameter("selected-overlay").parameter.SetActive(true);
    }

    public void ToggleByTipeTNI(string tipe)
    {
        toggleCanvasGroup(listDarat.gameObject, false);
        toggleCanvasGroup(listLaut.gameObject, false);
        toggleCanvasGroup(listUdara.gameObject, false);
        
        switch (tipe)
        {
            case "darat":
                toggleCanvasGroup(listDarat.gameObject, true);
                break;
            case "laut":
                toggleCanvasGroup(listLaut.gameObject, true);
                break;
            case "udara":
                toggleCanvasGroup(listUdara.gameObject, true);
                break;
            default: break;
        }
        
        /*var tipeTNI = (tipe == "darat") ? ListSatuanData.typeObject.Darat : (tipe == "laut") ? ListSatuanData.typeObject.Laut : (tipe == "udara") ? ListSatuanData.typeObject.Udara : ListSatuanData.typeObject.Darat;

        for (int i=0; i < LIST_DATA.Count; i++)
        {
            LIST_DATA[i].gameObject.SetActive((LIST_DATA[i].tipe_obj == tipeTNI) ? true : false);
        }*/
    }

    public void AS_ToggleByTipeTNI(string tipe)
    {
        toggleCanvasGroup(AS_listDarat.gameObject, false);
        toggleCanvasGroup(AS_listLaut.gameObject, false);
        toggleCanvasGroup(AS_listUdara.gameObject, false);

        switch (tipe)
        {
            case "darat":
                toggleCanvasGroup(AS_listDarat.gameObject, true);
                break;
            case "laut":
                toggleCanvasGroup(AS_listLaut.gameObject, true);
                break;
            case "udara":
                toggleCanvasGroup(AS_listUdara.gameObject, true);
                break;
            default: break;
        }

        /*var tipeTNI = (tipe == "darat") ? ListSatuanData.typeObject.Darat : (tipe == "laut") ? ListSatuanData.typeObject.Laut : (tipe == "udara") ? ListSatuanData.typeObject.Udara : ListSatuanData.typeObject.Darat;

        for (int i=0; i < LIST_DATA.Count; i++)
        {
            LIST_DATA[i].gameObject.SetActive((LIST_DATA[i].tipe_obj == tipeTNI) ? true : false);
        }*/
    }

    private void toggleCanvasGroup(GameObject obj, bool status)
    {
        var canvasGroup = obj.GetComponent<CanvasGroup>();
        canvasGroup.alpha = (status == true) ? 1 : 0;
        canvasGroup.interactable = status;
        canvasGroup.blocksRaycasts = status;
    }

    private string GetPathObj3DName(string path_object_3d)
    {
        var splitData = path_object_3d.Split("/");
        if (splitData.Length <= 0) return "";

        return splitData[splitData.Length - 1].Split(".FBX")[0];
    }

    private GameObject getAssetSatuan(string nama_satuan, string nama_obj3D, ListSatuanData.typeObject tipe_object)
    {
        // Dapatkan Asset 3D Satuan dari Asset Package Berdasarkan Nama Satuan atau Nama Object 3D Satuannya
        int index = AssetPackageManager.Instance.ASSETS_NAME.IndexOf(nama_satuan);
        if (index == -1) index = AssetPackageManager.Instance.ASSETS_NAME.IndexOf(nama_obj3D);

        // Jika Tidak Terdapat Asset 3D dalam Asset Package, Gunakan Asset Default Berdasarkan Jenis Satuannya
        if (index == -1) return tipe_object == ListSatuanData.typeObject.Darat ? AssetPackageManager.Instance.defaultVehicle :
                                tipe_object == ListSatuanData.typeObject.Laut ? AssetPackageManager.Instance.defaultShip :
                                tipe_object == ListSatuanData.typeObject.Udara ? AssetPackageManager.Instance.defaultAircraft :
                                AssetPackageManager.Instance.defaultVehicle;

        Debug.Log(AssetPackageManager.Instance.ASSETS[index].name);

        return AssetPackageManager.Instance.ASSETS[index];
    }

    #region PLOTTING METHOD
    public void PlotObject()
    {
        // Jalankan Fungsi Save (Instead of Plot) ketika mode plotting berada di "EDIT MODE"
        if (mode == plotMode.EDIT)
        {
            SaveObject();
            return;
        }

        // Check Required Form First
        if(input_nama.text != "" && input_noSatuan.text != "" && input_noAtasan.text != "" && input_kecepatan.text != "" && input_fuel.text != "" && selectedPrefabObj != null && selectedDataObj != null)
        {
            var dataInfo                = new InfoListSatuan();
            dataInfo.kecepatan          = input_kecepatan.text + "|" + (dropdown_typeKecepatan.options[dropdown_typeKecepatan.value].text).ToLower();
            dataInfo.nomer_satuan       = input_noSatuan.text;
            dataInfo.nomer_atasan       = input_noAtasan.text;
            dataInfo.nama_satuan        = input_nama.text;
            dataInfo.tgl_mulai          = "-"; // Belum Dapat Datanya
            dataInfo.tgl_selesai        = ""; // Belum Dapat Datanya
            dataInfo.warna              = (SessionUser.nama_asisten == "ASOPS") ? "blue" : "red";
            dataInfo.size               = "30";
            dataInfo.weapon             = null; // Belum Dapat Datanya
            dataInfo.waypoint           = new List<string>(); ; // Belum Dapat Datanya
            dataInfo.list_embarkasi     = new List<string>(); // Belum Dapat Datanya
            dataInfo.armor              = 500; // Belum Dapat Datanya
            dataInfo.id_dislokasi       = false; // Belum Dapat Datanya
            dataInfo.id_dislokasi_obj   = false; // Belum Dapat Datanya
            dataInfo.bahan_bakar        = input_fuel.text;
            dataInfo.kecepatan_maks     = "";
            dataInfo.heading            = knobHeading.heading.ToString();
            dataInfo.ket_satuan         = input_keterangan.text;
            dataInfo.nama_icon_satuan   = ""; // Belum Dapat Datanya
            dataInfo.width_icon_satuan  = ""; // Belum Dapat Datanya
            dataInfo.height_icon_satuan = ""; // Belum Dapat Datanya
            dataInfo.personil           = 0; // Belum Dapat Datanya
            dataInfo.tfg                = true;
            dataInfo.hidewgs            = true;
            dataInfo.kedalaman          = "0"; // Belum Dapat Datanya

            selectedFormObj = dataInfo;
            btnToggler.toggleOff();

            plotStateLabel.gameObject.SetActive(true);
            plotStateLabel.GetComponent<Metadata>().FindParameter("message").parameter.GetComponent<TextMeshProUGUI>().text = "Click on the Map to place the object...";
            plotStateLabel.Play("FadeIn");
        }
    }

    public void EditObject(OnlineMapsMarkerBase markerBase)
    {
        resetForm();

        if (selectedDataObj != null || selectedPrefabObj != null || selectedFormObj != null) return;
        btnToggler.toggleOn();

        mode = plotMode.EDIT;
        label_header.text = "EDIT SATUAN";
        label_btnPlot.text = "EDIT";

        OnlineMapsMarker3D marker = markerBase["markerSource"] as OnlineMapsMarker3D;
        // formasiBtn.onClick.AddListener(delegate{PlotFormasiController.instance.EditMenu(marker, toggler);});

        if(marker.instance.GetComponent<PFormasiV2>().formasi.type != FormasiType.formasiType.None)
        formasiBtn.gameObject.SetActive(true);
        else
        formasiBtn.gameObject.SetActive(false);

        editOptionGroup.SetActive(true);

        var marker_data = marker.instance.GetComponent<JSONDataSatuan>();
        selectedEditObj = marker_data;
        selectedEditMarker = marker;
        RouteController.Instance.markerInstance = marker;

        var pFormasi = marker.instance.GetComponent<PFormasiV2>();

        var style = EntitySatuanStyle.FromJson(marker_data.style);
        var info = InfoListSatuan.FromJson(marker_data.info);

        var info_kecepatan = info.kecepatan.Split("|");

        input_nama.text = info.nama_satuan;
        input_noSatuan.text = info.nomer_satuan;
        input_noAtasan.text = info.nomer_atasan;
        input_kecepatan.text = (info_kecepatan[0] != "") ? info_kecepatan[0] : "0";
        knobHeading.set((info.heading != "" || info.heading != null) ? int.Parse(info.heading) : 0);
        dropdown_typeKecepatan.value = (info_kecepatan.Length <= 0) ? 0 : (info_kecepatan[1] == "kilometer") ? 0 : (info_kecepatan[1] == "miles") ? 1 : (info_kecepatan[1] == "knot") ? 2 : (info_kecepatan[1] == "mach") ? 3 : 0;
        input_fuel.text = info.bahan_bakar;
        input_keterangan.text = info.ket_satuan;

        selectedSatuanGroup.SetActive(true);
        var metadata = selectedSatuanGroup.GetComponent<Metadata>();
        metadata.FindParameter("image-satuan").parameter.GetComponent<UnityEngine.UI.Image>().sprite = marker_data.icon;
        metadata.FindParameter("nama-satuan").parameter.GetComponent<TextMeshProUGUI>().text = info.nama_satuan;
        metadata.FindParameter("simbol-satuan").parameter.GetComponent<Text>().text = ((char)style.index).ToString();
        metadata.FindParameter("simbol-satuan").parameter.GetComponent<Text>().font = Resources.Load<UnityEngine.Font>("Fonts/Simbol Taktis/" + style.nama);
    }

    public void GetMarkerForKegiatan()
    {
        var pF = selectedEditMarker.instance.GetComponent<PFormasiV2>();
        var fS = pF.formasi;

        if(fS.type != FormasiType.formasiType.None)
        RouteController.Instance.markerInstance = fS.unitsInFormasi[0].GetComponent<PFormasiV2>().markers.marker3D;
        else
        RouteController.Instance.markerInstance = pF.markers.marker3D;

        var pFF = RouteController.Instance.markerInstance.instance.GetComponent<PFormasiV2>();
        var pP = PlotPasukanController.instance;

        if(pFF.stat != null)
        {
            pP.btnKegiatanSat.SetActive(false);
            pP.btnKegiatanPas.SetActive(true);
            pP.btnCloseKegiatanSat.SetActive(false);
            pP.btnCloseKegiatanPas.SetActive(true);
            pP.btnDeleteKegiatanSat.SetActive(false);
            pP.btnDeleteKegiatanPas.SetActive(true);
        }
        else
        {
            pP.btnKegiatanSat.SetActive(true);
            pP.btnKegiatanPas.SetActive(false);
            pP.btnCloseKegiatanSat.SetActive(true);
            pP.btnCloseKegiatanPas.SetActive(false);
            pP.btnDeleteKegiatanSat.SetActive(true);
            pP.btnDeleteKegiatanPas.SetActive(false);
        }
    }

    public void CloseKegiatan()
    {
        RouteController.Instance.markerInstance = null;
        var pP = PlotPasukanController.instance;

        pP.ResetFormKegiatan();
    }

    public async void SaveObject()
    {
        if (input_nama.text != "" && input_noSatuan.text != "" && input_noAtasan.text != "" && input_kecepatan.text != "" && input_fuel.text != "" && selectedEditObj != null)
        {
            var info = InfoListSatuan.FromJson(selectedEditObj.info);

            info.kecepatan          = input_kecepatan.text + "|" + dropdown_typeKecepatan.options[dropdown_typeKecepatan.value].text.ToLower();
            info.nomer_satuan       = input_noSatuan.text;
            info.nama_satuan        = input_nama.text;
            info.nomer_atasan       = input_noAtasan.text;
            info.bahan_bakar        = input_fuel.text;
            info.ket_satuan         = input_keterangan.text;
            info.heading            = knobHeading.heading.ToString();
            info.tfg = true;

            if(selectedPrefabObj == null)
            {
                selectedEditMarker.rotationY = knobHeading.heading - 180;
                selectedEditObj.info = InfoListSatuan.ToString(info);

                var HUDElement = selectedEditMarker.instance.transform.Find("HUD_ELEMENT").GetChild(0).GetComponent<HUDNavigationElement>();
                HUDElement.Prefabs.IndicatorPrefab.ChangeEntityInfo(info.nama_satuan);

                HUDElement.gameObject.SetActive(false);
                HUDElement.gameObject.SetActive(true);
            }
            else
            {
                await CreateMarkerSatuan(selectedEditMarker.position.x, selectedEditMarker.position.y, int.Parse(info.heading), selectedPrefabObj.gameObject, selectedPrefabObj.image, selectedEditObj, InfoListSatuan.ToString(info));
                OnlineMapsMarker3DManager.RemoveItem(selectedEditMarker);
            }

            Debug.Log("Data Satuan Berhasil Diubah!");
            btnToggler.toggleOff();
        }
    }

    public void DeleteObject()
    {
        if (selectedEditMarker == null) return;
        
        if(selectedEditMarker.instance.GetComponent<PFormasiV2>().formasi.type == FormasiType.formasiType.None)
        {
            var trash = GameObject.Find("Trash");
            if(trash == null)
            {
                trash = new GameObject("Trash");
                trash.AddComponent<TrashObj>();
            }

            var tObj = new Trash
            {
                id = selectedEditMarker.instance.name,
                type = "satuan"
            };

            var tScript = trash.GetComponent<TrashObj>();
            tScript.trash.Add(tObj);

            EntityEditorController.Instance.plotTrash.Add(
                new JObject()
                {
                    { "id", selectedEditMarker.instance.name },
                    { "type", "satuan" }
                }
            );

            var jM3D = selectedEditMarker.instance.GetComponent<JSONDataMisiSatuan>();
            for(int i = 0; i < jM3D.listRoute.Count; i++)
            {
                var ttObj = new Trash
                {
                    id = jM3D.listRoute[i].gameObject.name,
                    type = "misi"
                };
                tScript.trash.Add(ttObj);
                Destroy(jM3D.listRoute[i].gameObject);
                jM3D.listRoute.RemoveAt(i);
            }
            jM3D.listRoute = new();

            var ruteSatuan = selectedEditMarker.instance.GetComponent<JSONDataMisiSatuan>().listRoute;
            for(int i=0; i < ruteSatuan.Count; i++)
            {
                Destroy(ruteSatuan[i].gameObject);
            }

            OnlineMapsMarker3DManager.RemoveItem(selectedEditMarker);
            // PlotPasukanController.instance.indexSatuan--;
            
            Debug.Log("Data Satuan Berhasil Dihapus!");
            btnToggler.toggleOff();
        }
        else
        Debug.Log("Unit ini sedang berada dalam formasi!!");
    }

    private async void OnMapClick()
    {
        if (mode != plotMode.CREATE) return;

        if(selectedDataObj != null && selectedPrefabObj != null && selectedFormObj != null)
        {
            // Get the coordinates under the cursor.
            double lng, lat;
            OnlineMapsControlBase.instance.GetCoords(out lng, out lat);

            Debug.Log("Plot New Satuan At (" + lng + ", " + lat + ")");

            await CreateMarkerSatuan(lng, lat, knobHeading.heading, selectedPrefabObj.gameObject, selectedPrefabObj.image);

            resetForm();
            plotStateLabel.Play("FadeOut");
        }
    }

    private async Task CreateMarkerSatuan(double lng, double lat, float heading, GameObject asset3D, Sprite icon, JSONDataSatuan JSONData = null, string infoData = null)
    {
        // Create a new marker.
        var marker = OnlineMapsMarker3DManager.CreateItem(lng, lat, asset3D);
        marker["markerSource"] = marker;
        marker.instance.tag = "entity-satuan";
        marker.OnClick += EditObject;

        var marker_data = marker.instance.AddComponent<JSONDataSatuan>();
        marker.instance.AddComponent<PFormasi>();

        marker_data.icon = (JSONData == null) ? icon : JSONData.icon;

        marker_data.id_user     = (JSONData == null) ? SessionUser.id.ToString() : JSONData.id_user;
        marker_data.id_symbol   = (JSONData == null) ? selectedPrefabObj.id_symbol : JSONData.id_symbol; // Belum Dapat Datanya
        marker_data.dokumen     = (JSONData == null) ? SkenarioAktif.ID_DOCUMENT : JSONData.dokumen;
        marker_data.nama        = (JSONData == null) ? generateIDSatuan() : JSONData.nama;
        marker_data.lat_y       = lat;
        marker_data.lng_x       = lng;

        marker_data.style       = StyleListSatuan.ToString(selectedDataObj.StyleSatuan);
        marker_data.info        = (infoData == null) ? InfoListSatuan.ToString(selectedFormObj) : infoData;
        marker_data.symbol      = "<div style='text-align:center'><span style='font-weight: bolder;color: blue; font-size: 21px; font-family: " + selectedDataObj.StyleSatuan.nama + "'>" + ((char)selectedDataObj.StyleSatuan.index).ToString() + "</span></div>";

        marker.instance.name = marker_data.nama;
        marker.instance.layer = LayerMask.NameToLayer("PlayerUnit");
        marker.rotationY = heading;
        marker.scale = 1;
        marker.sizeType = OnlineMapsMarker3D.SizeType.meters;
        PlotPasukanController.instance.indexSatuan++;

        await EntityEditorController.Instance.SetJSONSatuanAndHUD(marker, null, EntitySatuanStyle.FromJson(marker_data.style), EntitySatuanInfo.FromJson(marker_data.info), marker_data, (JSONData == null) ? false : true);
    }

    /// <summary>
    /// Ubah Lokasi Object Terpilih
    /// </summary>
    public void MoveObject()
    {
        if (selectedEditMarker == null) return;
        
        if(selectedEditMarker.instance.GetComponent<PFormasi>().formasi == PFormasi.formasiType.None)
        {
            PlotNotificationHelper.Instance.TogglePlotNotification(true, "Drag the object to change position");

            double lng, lat;
            selectedEditMarker.GetPosition(out lng, out lat);
            selectedEditMarker.label = lng + ", " + lat;

            selectedEditMarker.OnPress = ToggleDragMarker;
            selectedEditMarker.OnDrag = OnMarkerDrag;
            mode = plotMode.ONMOVE;

            GetComponent<Animator>().Play("SlideOutNoTrigger");
        }
        else
        Debug.Log("Unit ini sedang berada dalam formasi!!");
    }

    public void OnInputHeadingChange()
    {
        if (inputHeading.text == "") return;
        if (int.Parse(inputHeading.text) < 0 || int.Parse(inputHeading.text) > 359)
        {
            inputHeading.text = "0";
        }

        knobHeading.set(int.Parse(inputHeading.text));
    }

    /// <summary>
    /// Ketika Marker di Klik Dalam Mode OnMove
    /// </summary>
    /// <param name="markerBase"></param>
    private void ToggleDragMarker(OnlineMapsMarkerBase markerBase)
    {
        // Starts moving the marker.
        OnlineMapsControlBase.instance.dragMarker = markerBase;
        OnlineMapsControlBase.instance.isMapDrag = false;
    }

    /// <summary>
    /// Ketika Marker di Drag Dalam Mode OnMove
    /// </summary>
    /// <param name="markerBase"></param>
    private void OnMarkerDrag(OnlineMapsMarkerBase markerBase)
    {
        double lng, lat;
        markerBase.GetPosition(out lng, out lat);

        OnlineMapsMarker3D marker = markerBase["markerSource"] as OnlineMapsMarker3D;
        marker.label = lng + ", " + lat;
        marker.instance.GetComponent<JSONDataSatuan>().lng_x = lng;
        marker.instance.GetComponent<JSONDataSatuan>().lat_y = lat;
    }
    #endregion

    #region HELPER
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (mode == plotMode.ONPLOT)
            {
                mode = plotMode.CREATE;

                PlotNotificationHelper.Instance.TogglePlotNotification(false);
                GetComponent<Animator>().Play("SlideIn");
            }
            else if (mode == plotMode.ONMOVE)
            {
                mode = plotMode.EDIT;
                selectedEditMarker.OnPress = null;
                selectedEditMarker.OnDrag = null;
                selectedEditMarker.label = "";

                PlotNotificationHelper.Instance.TogglePlotNotification(false);
                GetComponent<Animator>().Play("SlideIn");
            }
        }
    }

    private string generateIDSatuan()
    {
        // var index_satuan = GameObject.FindGameObjectsWithTag("entity-satuan").Length
        var index_satuan = PlotPasukanController.instance.indexSatuan;
        bool isExist = true;

        // do
        // {
        //     isExist = checkSatuanIndex(index_satuan);
        //     index_satuan++;
        // } while (isExist);

        return "satuan_" + index_satuan + "_" + SessionUser.id + "_" + UnityEngine.Random.Range(0, 999);
    }

    private bool checkSatuanIndex(int index)
    {
        bool isExist = false;

        foreach (GameObject satuan in GameObject.FindGameObjectsWithTag("entity-satuan"))
        {
            if (satuan.name.Contains("satuan_" + index)) isExist = true;
        }

        return isExist;
    }
    #endregion
}