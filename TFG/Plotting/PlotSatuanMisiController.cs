using System;
using UnityEngine;

using TMPro;

public class PlotSatuanMisiController : MonoBehaviour
{
    public enum drawMode { CREATE, EDIT, DELETE }

    [Header("Plot Mode")]
    public drawMode mode;

    [Header("UI References")]
    public GameObject FormGroup;
    public GameObject ScrollView;
    public GameObject editActionGroup;

    [Header("External References")]
    public Metadata drawStateLabel;

    [Header("Form Data")]
    public TMP_InputField inputNamaRoute;
    public TMP_Dropdown dropdownJenisKegiatan;
    public TMP_InputField inputNamaKegiatan;
    public TMP_InputField inputKecepatan;
    public TMP_Dropdown dropdownTypeKecepatan;

    [Header("Datetime Form")]
    public TextMeshProUGUI labelDate;
    public TextMeshProUGUI labelTime;
    public TMP_InputField inputDay;
    public TMP_InputField inputMonth;
    public TMP_InputField inputYear;
    public TMP_InputField inputHour;
    public TMP_InputField inputMinute;
    public TMP_InputField inputSecond;

    private string selectedKegiatanID;
    private GameObject selectedKegiatanObj;

    private DateTime _WAKTU;

    private void Start()
    {
        resetTime();
    }

    public void drawNewRoute()
    {
        drawStateLabel.gameObject.SetActive(true);
        drawStateLabel.FindParameter("message").parameter.GetComponent<TextMeshProUGUI>().text = "Click on the map to draw route. Press ENTER to finish.";
        drawStateLabel.GetComponent<Animator>().Play("FadeIn");

        GetComponent<Animator>().Play("FadeOut");
    }

    public void selectKegiatanFromList(GameObject obj)
    {
        selectedKegiatanObj = obj;
        editActionGroup.SetActive(true);
    }

    public void toggleRouteMode(string state)
    {
        if(state == "create")
        {
            mode = drawMode.CREATE;
        }else if(state == "edit")
        {
            mode = drawMode.EDIT;
        }
    }

    public void editRouteMode()
    {
        mode = drawMode.EDIT;
        resetForm();

        ScrollView.GetComponent<Animator>().Play("FadeIn");
        ScrollView.GetComponent<Animator>().Play("FadeOut");

        editActionGroup.SetActive(true);
    }

    public void listRouteMode()
    {
        ScrollView.GetComponent<Animator>().Play("FadeIn");
        FormGroup.GetComponent<Animator>().Play("FadeOut");

        editActionGroup.SetActive(false);
    }

    public void resetForm()
    {
        mode = drawMode.CREATE;

        inputNamaRoute.text = "";
        dropdownJenisKegiatan.value = 0;
        inputNamaKegiatan.text = "";
        inputKecepatan.text = "";
        dropdownTypeKecepatan.value = 0;

        resetTime();

        selectedKegiatanID = null;
        selectedKegiatanObj = null;
    }

    public void resetTime()
    {
        _WAKTU = DateTime.Now;
        refreshInputWaktuUI();
    }

    public void increaseTime(string data)
    {
        switch (data)
        {
            case "day":
                _WAKTU = _WAKTU.AddDays(1);
                break;
            case "month":
                _WAKTU = _WAKTU.AddMonths(1);
                break;
            case "year":
                _WAKTU = _WAKTU.AddYears(1);
                break;
            case "hour":
                _WAKTU = _WAKTU.AddHours(1);
                break;
            case "minute":
                _WAKTU = _WAKTU.AddMinutes(1);
                break;
            case "second":
                _WAKTU = _WAKTU.AddSeconds(1);
                break;
            default: break;
        }

        refreshInputWaktuUI();
    }

    public void decreaseTime(string data)
    {
        switch (data)
        {
            case "day":
                _WAKTU = _WAKTU.AddDays(-1);
                break;
            case "month":
                _WAKTU = _WAKTU.AddMonths(-1);
                break;
            case "year":
                _WAKTU = _WAKTU.AddYears(-1);
                break;
            case "hour":
                _WAKTU = _WAKTU.AddHours(-1);
                break;
            case "minute":
                _WAKTU = _WAKTU.AddMinutes(-1);
                break;
            case "second":
                _WAKTU = _WAKTU.AddSeconds(-1);
                break;
            default: break;
        }

        refreshInputWaktuUI();
    }

    private void refreshInputWaktuUI()
    {
        inputDay.text      = _WAKTU.Day.ToString();
        inputMonth.text    = _WAKTU.Month.ToString();
        inputYear.text     = _WAKTU.Year.ToString();

        inputHour.text     = _WAKTU.Hour.ToString();
        inputMinute.text   = _WAKTU.Minute.ToString();
        inputSecond.text   = _WAKTU.Second.ToString();

        labelDate.text     = _WAKTU.ToString("");
        labelTime.text     = _WAKTU.ToString("[HH:mm:ss]");
    }
}
