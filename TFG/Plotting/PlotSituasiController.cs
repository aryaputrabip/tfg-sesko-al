using System;
using UnityEngine;

using TMPro;
using System.Globalization;
using HelperEditor;
using GlobalVariables;
using Newtonsoft.Json.Linq;

public class PlotSituasiController : MonoBehaviour
{
    public enum plotMode { CREATE, EDIT, ONPLOT, ONMOVE }
    [Header("Plot Mode")]
    public plotMode mode;

    [Header("UI References")]
    public TextMeshProUGUI label_header;
    public TextMeshProUGUI label_btnPlot;
    public GameObject label_notification;
    public SidemenuToggler btnToggler;
    public GameObject edit_tools;
    public DateTime date;

    [Header("Form Data")]
    public TMP_InputField inputKeterangan;
    public DateInputHelper dateInput;

    // TEMP DATA STORAGE
    private InfoSituasi SELECTED_DATA;
    private OnlineMapsMarker3D SELECTED_MARKER;

    #region INSTANCE
    public static PlotSituasiController Instance;
    #endregion

    private void Start()
    {
        if (Instance == null)
        {
            OnlineMapsControlBase.instance.OnMapClick += OnMapClick;
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        date = DateTime.Now;
    }

    public void SetTime(DateInputHelper dateInput)
    {
        date = dateInput.date;
    }

    #region PLOTTING
    /// <summary>
    /// Reset form ke posisi default
    /// </summary>
    public void ResetForm()
    {
        // Reset Form Data
        inputKeterangan.text = "";
        dateInput.resetTime();

        // Reset Form Label
        label_header.text = "PLOT SITUASI";
        label_btnPlot.text = "PLOT";
        edit_tools.SetActive(false);

        mode = plotMode.CREATE;

        PlotNotificationHelper.Instance.TogglePlotNotification(false);
    }

    /// <summary>
    /// Ketika Button Plot pada Form Di Klik
    /// (Bertindak sebagai plot object baru jika masuk ke dalam mode create dan save perubahan data object dipilih ketika masuk dalam mode edit)
    /// </summary>
    public void OnButtonPlotClick()
    {
        if (dateInput.inputDay.text == "" || dateInput.inputMonth.text == "" || dateInput.inputYear.text == "" ||
            dateInput.inputHour.text == "" || dateInput.inputMinute.text == "" || dateInput.inputSecond.text == "" || inputKeterangan.text == "")
        {
            // Notifikasi Data Form Belum Lengkap
            return;
        }

        // Jika Data Form Sudah Lengkap, Eksekusi Aksi Create / Save Object Aktif
        if (mode == plotMode.EDIT)
        {
            SaveData();
            return;
        }

        GetComponent<Animator>().Play("SlideOutNoTrigger");
        mode = plotMode.ONPLOT;
        PlotNotificationHelper.Instance.TogglePlotNotification(true);
    }

    /// <summary>
    /// Ketika Marker Di Klik
    /// (Bertindak untuk menampilkan form dengan data dari marker tersebut & juga mengubah mode ke dalam mode edit
    /// </summary>
    public void OnMarkerClick(OnlineMapsMarkerBase markerBase)
    {
        // Marker Click Tidak Akan Berpengaruh Jika Plotting Sedang Dalam Mode ONPLOT / ONMOVE
        if (mode == plotMode.ONPLOT || mode == plotMode.ONMOVE) return;
        btnToggler.toggleOn();

        mode = plotMode.EDIT;
        label_header.text = "EDIT SITUASI";
        label_btnPlot.text = "SAVE";
        edit_tools.SetActive(true);

        OnlineMapsMarker3D marker = markerBase["markerSource"] as OnlineMapsMarker3D;
        var marker_data = marker.instance.GetComponent<JSONDataSituasi>();

        SELECTED_DATA = new InfoSituasi();
        SELECTED_DATA = InfoSituasi.FromJson(marker_data.info_situasi);
        SELECTED_MARKER = marker;

        inputKeterangan.text = SELECTED_DATA.isi_situasi;

        dateInput.date = DateTime.ParseExact(SELECTED_DATA.tgl_situasi + " " + SELECTED_DATA.waktu_situasi, "ddMMyyyy HHmm", CultureInfo.InvariantCulture);
        date = dateInput.date;

        dateInput.inputDay.text = date.ToString("dd");
        dateInput.inputMonth.text = date.ToString("MM");
        dateInput.inputYear.text = date.ToString("yyyy");
        dateInput.inputHour.text = date.ToString("HH");
        dateInput.inputMinute.text = date.ToString("mm");
        dateInput.inputSecond.text = date.ToString("ss");

        SELECTED_MARKER = marker;

        dateInput.refreshUI();
    }

    /// <summary>
    /// Event Ketika Click pada Peta
    /// (Bertindak untuk Plot Object)
    /// </summary>
    public async void OnMapClick()
    {
        if (mode != plotMode.ONPLOT) return;

        // Get the coordinates under the cursor.
        double lng, lat;
        OnlineMapsControlBase.instance.GetCoords(out lng, out lat);

        var marker = OnlineMapsMarker3DManager.CreateItem(new Vector2((float)lng, (float)lat), AssetPackageManager.Instance.prefabSituasi);
        marker.instance.name = generateID();

        Debug.Log("Plot New Situasi At (" + lng + ", " + lat + ")");

        // Set Form Data Into Data Situasi
        var info_situasi = new InfoSituasi();
        info_situasi.isi_situasi        = inputKeterangan.text;
        info_situasi.tgl_situasi        = date.ToString("ddMMyyyy");
        info_situasi.waktu_situasi      = date.ToString("HHmm");
        info_situasi.size               = 17; // Unknown Source, Input Default  
        info_situasi.warna              = (SessionUser.nama_asisten == "ASOPS") ? "blue" : "red";
        info_situasi.property           = "{\"distance_\":20,\"degree_\":45,\"width_\":0,\"height_\":100}"; // Unknown Source, Input Default
            
        var symbol = "{\"iconUrl\":\"image/plotting/warning_blue.png\",\"iconSize\":[17,17],\"id_point\":\"" + marker.instance.name + "\"}";

        await EntityEditorController.Instance.SetJSONSituasiAndHUD(marker, marker.instance.name, info_situasi, symbol);

        ResetForm();
        btnToggler.toggleOff();

        PlotNotificationHelper.Instance.TogglePlotNotification(false);
    }

    /// <summary>
    /// Ketika Marker di Klik Dalam Mode OnMove
    /// </summary>
    /// <param name="markerBase"></param>
    private void ToggleDragMarker(OnlineMapsMarkerBase markerBase)
    {
        // Starts moving the marker.
        OnlineMapsControlBase.instance.dragMarker = markerBase;
        OnlineMapsControlBase.instance.isMapDrag = false;
    }

    /// <summary>
    /// Ketika Marker di Drag Dalam Mode OnMove
    /// </summary>
    /// <param name="markerBase"></param>
    private void OnMarkerDrag(OnlineMapsMarkerBase markerBase)
    {
        double lng, lat;
        markerBase.GetPosition(out lng, out lat);

        OnlineMapsMarker3D marker = markerBase["markerSource"] as OnlineMapsMarker3D;
        marker.label = lng + ", " + lat;
        marker.instance.GetComponent<JSONDataSituasi>().lng_x = lng;
        marker.instance.GetComponent<JSONDataSituasi>().lat_y = lat;
    }

    /// <summary>
    /// Save perubahan data pada object terpilih
    /// </summary>
    public async void SaveData()
    {
        var dataSituasi = SELECTED_MARKER.instance.GetComponent<JSONDataSituasi>();

        // Set Perubahan Data Situasi Dari Form Data
        SELECTED_DATA.isi_situasi = inputKeterangan.text;
        SELECTED_DATA.tgl_situasi = date.ToString("ddMMyyyy");
        SELECTED_DATA.waktu_situasi = date.ToString("HHmm");

        dataSituasi.info_situasi = InfoSituasi.ToString(SELECTED_DATA);

        // Respawn Saved Object
        var marker = OnlineMapsMarker3DManager.CreateItem(new Vector2((float)dataSituasi.lng_x, (float)dataSituasi.lat_y), AssetPackageManager.Instance.prefabSituasi);
        marker.instance.name = dataSituasi.nama;
        await EntityEditorController.Instance.SetJSONSituasiAndHUD(marker, dataSituasi.nama, SELECTED_DATA, dataSituasi.symbol_situasi);

        // Delete Old Marker
        OnlineMapsMarker3DManager.RemoveItem(SELECTED_MARKER);

        btnToggler.toggleOff();
        ResetForm();
    }

    /// <summary>
    /// Ubah Lokasi Object Terpilih
    /// </summary>
    public void MoveObject()
    {
        if (SELECTED_MARKER == null) return;
        PlotNotificationHelper.Instance.TogglePlotNotification(true, "Drag the object to change position");

        double lng, lat;
        SELECTED_MARKER.GetPosition(out lng, out lat);
        SELECTED_MARKER.label = lng + ", " + lat;

        SELECTED_MARKER.OnPress = ToggleDragMarker;
        SELECTED_MARKER.OnDrag = OnMarkerDrag;
        mode = plotMode.ONMOVE;

        GetComponent<Animator>().Play("SlideOutNoTrigger");
    }

    /// <summary>
    /// Hapus Object Terpilih
    /// </summary>
    public void DeleteObject()
    {
        if (SELECTED_MARKER == null) return;

        EntityEditorController.Instance.plotTrash.Add(
            new JObject()
            {
                { "id", SELECTED_MARKER.instance.name },
                { "type", "situasi" }
            }
        );

        OnlineMapsMarker3DManager.RemoveItem(SELECTED_MARKER);

        Debug.Log("Data Situasi Berhasil Dihapus!");
        btnToggler.toggleOff();
    }

    #region HELPER
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (mode == plotMode.ONPLOT)
            {
                mode = plotMode.CREATE;

                PlotNotificationHelper.Instance.TogglePlotNotification(false);
                GetComponent<Animator>().Play("SlideIn");
            }
            else if (mode == plotMode.ONMOVE)
            {
                mode = plotMode.EDIT;
                SELECTED_MARKER.OnPress = null;
                SELECTED_MARKER.OnDrag = null;
                SELECTED_MARKER.label = "";

                PlotNotificationHelper.Instance.TogglePlotNotification(false);
                GetComponent<Animator>().Play("SlideIn");
            }
        }
    }

     private string generateID()
    {
        var index_situasi = GameObject.FindGameObjectsWithTag("entity-situasi").Length;
        bool isExist = true;

        do
        {
            isExist = checkIndex(index_situasi);
            index_situasi++;
        } while (isExist);

        return "situasi_" + index_situasi + "_" + SessionUser.id + "_" + UnityEngine.Random.Range(0, 999);
    }

    private bool checkIndex(int index)
    {
        bool isExist = false;

        foreach (GameObject situasi in GameObject.FindGameObjectsWithTag("entity-situasi"))
        {
            if (situasi.name.Contains("situasi_" + index)) isExist = true;
        }

        return isExist;
    }
    #endregion
    #endregion
}
