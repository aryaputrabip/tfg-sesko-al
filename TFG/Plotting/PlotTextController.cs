using UnityEngine;
using UnityEngine.UI;

using TMPro;
using HelperEditor;
using System.Linq;
using GlobalVariables;
using Newtonsoft.Json.Linq;

public class PlotTextController : MonoBehaviour
{
    public enum plotMode { CREATE, EDIT, ONPLOT, ONMOVE }
    [Header("Plot Mode")]
    public plotMode mode;

    [Header("UI References")]
    public TextMeshProUGUI label_header;
    public TextMeshProUGUI label_btnPlot;
    public GameObject label_notification;
    public SidemenuToggler btnToggler;
    public GameObject edit_tools;

    [Header("Prefab Ref")]
    public GameObject prefabText;

    [Header("Form Data")]
    public TMP_InputField inputText;
    public TMP_InputField inputAngle;
    public TMP_InputField inputSize;
    public TMP_Dropdown DDWarna;
    public TMP_Dropdown DDFontStyle;
    public Image colorBtn;
    public FlexibleColorPicker colorPicker;

    // TEMP DATA STORAGE
    private SymbolText SELECTED_DATA;
    private OnlineMapsMarker3D SELECTED_MARKER;
    private int currentZoom;
    private int prevZoom;

    public int nText;

    #region INSTANCE
    public static PlotTextController Instance;
    void Awake()
    {
        if (Instance == null)
        {
            OnlineMapsControlBase.instance.OnMapClick += OnMapClick;
            Instance = this;
            this.nText = 0;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    void Start()
    {
        currentZoom = OnlineMaps.instance.zoom;
        prevZoom = currentZoom;
        OnlineMaps.instance.OnChangeZoom += OnUpdateZoom;
    }

    #region PLOTTING
    /// <summary>
    /// Reset form ke posisi default
    /// </summary>
    public void ResetForm()
    {
        // Reset Form Data
        inputText.text = "";
        inputAngle.text = "0";
        inputSize.text = "12";
        DDWarna.value = 0;
        DDFontStyle.value = 1;

        // Reset Form Label
        label_header.text = "PLOT TEXT";
        label_btnPlot.text = "PLOT";
        edit_tools.SetActive(false);

        mode = plotMode.CREATE;
        PlotNotificationHelper.Instance.TogglePlotNotification(false);
    }

    /// <summary>
    /// Ketika Button Plot pada Form Di Klik
    /// (Bertindak sebagai plot object baru jika masuk ke dalam mode create dan save perubahan data object dipilih ketika masuk dalam mode edit)
    /// </summary>
    public void OnButtonPlotClick()
    {
        if(inputAngle.text == "" || inputSize.text == "")
        {
            // Notifikasi Data Form Belum Lengkap
            return;
        }

        // Jika Data Form Sudah Lengkap, Eksekusi Aksi Create / Save Object Aktif
        if (mode == plotMode.EDIT)
        {
            SaveData();
            return;
        }

        GetComponent<Animator>().Play("SlideOutNoTrigger");
        mode = plotMode.ONPLOT;
        PlotNotificationHelper.Instance.TogglePlotNotification(true);
    }

    /// <summary>
    /// Ketika Marker Di Klik
    /// (Bertindak untuk menampilkan form dengan data dari marker tersebut & juga mengubah mode ke dalam mode edit
    /// </summary>
    public void OnMarkerClick(OnlineMapsMarkerBase markerBase)
    {
        // Marker Click Tidak Akan Berpengaruh Jika Plotting Sedang Dalam Mode ONPLOT / ONMOVE
        if (mode == plotMode.ONPLOT || mode == plotMode.ONMOVE) return;
        btnToggler.toggleOn();

        mode = plotMode.EDIT;
        label_header.text = "EDIT TEXT";
        label_btnPlot.text = "SAVE";
        edit_tools.SetActive(true);

        OnlineMapsMarker3D marker = markerBase["markerSource"] as OnlineMapsMarker3D;
        var marker_data = marker.instance.GetComponent<JSONDataText>();

        SELECTED_DATA = new SymbolText();
        SELECTED_DATA = SymbolText.FromJson(marker_data.symbol);
        SELECTED_MARKER = marker;

        inputText.text = SELECTED_DATA.text;
        inputAngle.text = SELECTED_DATA.angel;
        inputSize.text = SELECTED_DATA.size;
        DDWarna.value = (SELECTED_DATA.warna == "#0000FF") ? 0 : 1;

        var DDFontStrings = DDFontStyle.options.Select(option => option.text).ToList();
        DDFontStyle.value = DDFontStrings.IndexOf(SELECTED_DATA.weight);

        SELECTED_MARKER = marker;
    }

    /// <summary>
    /// Event Ketika Click pada Peta
    /// (Bertindak untuk Plot Object)
    /// </summary>
    public async void OnMapClick()
    {
        if (mode != plotMode.ONPLOT) return;

        // Get the coordinates under the cursor.
        double lng, lat;
        OnlineMapsControlBase.instance.GetCoords(out lng, out lat);

        Debug.Log("Plot New Text At (" + lng + ", " + lat + ")");

        // Set Form Data Into Data Text
        var symbol_text = new SymbolText();
        symbol_text.text = inputText.text;
        symbol_text.angel = inputAngle.text;
        symbol_text.size = inputSize.text;
        symbol_text.weight = DDFontStyle.options[DDFontStyle.value].text.ToLower();
        symbol_text.warna = "#" + ColorUtility.ToHtmlStringRGB(colorBtn.color);
        symbol_text.zoom = OnlineMaps.instance.zoom;

        var info_text = inputSize.text;
        var marker = OnlineMapsMarker3DManager.CreateItem(new Vector2((float)lng, (float)lat), AssetPackageManager.Instance.prefabText);
        marker.ifText = true;
        marker.instance.name = generateIDtext();
        nText++;
        marker.instance.tag = "entity-text";
        await EntityEditorController.Instance.SetJSONTextAndHUD(marker, symbol_text, info_text);

        var _text = GameObject.Find("TextMarker");
        if(_text == null)
        {
            _text = new GameObject("TextMarker");
            _text.transform.parent = OnlineMaps.instance.transform;
            _text.transform.localPosition = Vector3.zero;
            _text.transform.localRotation = Quaternion.identity;
            _text.transform.localScale = Vector3.one;
        }
        marker.instance.transform.parent = _text.transform;
        // marker.transform.position = Vector3.zero;

        //await EntityEditorController.Instance.SpawnEntityText((float)lng, (float)lat, prefabText, symbol_text, info_text);

        ResetForm();
        btnToggler.toggleOff();

        PlotNotificationHelper.Instance.TogglePlotNotification(false);
    }

    void OnUpdateZoom()
    {
        currentZoom = OnlineMaps.instance.zoom;

        if(currentZoom != prevZoom)
        {
            var diffZoom = currentZoom - prevZoom;
            
            foreach(OnlineMapsMarker3D _marker in OnlineMapsMarker3DManager.instance)
            if(_marker.ifText)
            _marker.scale *= Mathf.Pow(2, diffZoom);
            
            prevZoom = currentZoom;
        }
    }

    /// <summary>
    /// Ketika Marker di Klik Dalam Mode OnMove
    /// </summary>
    /// <param name="markerBase"></param>
    private void ToggleDragMarker(OnlineMapsMarkerBase markerBase)
    {
        // Starts moving the marker.
        OnlineMapsControlBase.instance.dragMarker = markerBase;
        OnlineMapsControlBase.instance.isMapDrag = false;
    }

    /// <summary>
    /// Ketika Marker di Drag Dalam Mode OnMove
    /// </summary>
    /// <param name="markerBase"></param>
    private void OnMarkerDrag(OnlineMapsMarkerBase markerBase)
    {
        double lng, lat;
        markerBase.GetPosition(out lng, out lat);

        OnlineMapsMarker3D marker = markerBase["markerSource"] as OnlineMapsMarker3D;
        marker.label = lng + ", " + lat;
        marker.instance.GetComponent<JSONDataText>().lng_x = lng;
        marker.instance.GetComponent<JSONDataText>().lat_y = lat;
    }

    /// <summary>
    /// Save perubahan data pada object terpilih
    /// </summary>
    public async void SaveData()
    {
        var dataText = SELECTED_MARKER.instance.GetComponent<JSONDataText>();

        // Set Perubahan Data Text Dari Form Data
        SELECTED_DATA.text = inputText.text;
        SELECTED_DATA.angel = inputAngle.text;
        SELECTED_DATA.size = inputSize.text;
        SELECTED_DATA.weight = DDFontStyle.options[DDFontStyle.value].text.ToLower();
        SELECTED_DATA.warna = "#" + ColorUtility.ToHtmlStringRGB(colorBtn.color);

        dataText.symbol = SymbolText.ToString(SELECTED_DATA);

        // Respawn Saved Object
        var marker = OnlineMapsMarker3DManager.CreateItem(new Vector2((float)dataText.lng_x, (float)dataText.lat_y), AssetPackageManager.Instance.prefabText);
        marker.instance.name = dataText.nama;
        await EntityEditorController.Instance.SetJSONTextAndHUD(marker, SELECTED_DATA, dataText.info_text); 

        //await EntityEditorController.Instance.SpawnEntityText((float)dataText.lng_x, (float)dataText.lat_y, prefabText, SELECTED_DATA, dataText.info_text);

        // Delete Old Marker
        OnlineMapsMarker3DManager.RemoveItem(SELECTED_MARKER);

        btnToggler.toggleOff();
        ResetForm();
    }

    /// <summary>
    /// Ubah Lokasi Object Terpilih
    /// </summary>
    public void MoveObject()
    {
        if (SELECTED_MARKER == null) return;
        PlotNotificationHelper.Instance.TogglePlotNotification(true, "Drag the object to change position");

        double lng, lat;
        SELECTED_MARKER.GetPosition(out lng, out lat);
        SELECTED_MARKER.label = lng + ", " + lat;

        SELECTED_MARKER.OnPress = ToggleDragMarker;
        SELECTED_MARKER.OnDrag = OnMarkerDrag;
        mode = plotMode.ONMOVE;

        GetComponent<Animator>().Play("SlideOutNoTrigger");
    }

    private string generateIDtext()
    {
        var index_text = GameObject.FindGameObjectsWithTag("entity-text").Length;
        bool isExist = true;

        do
        {
            isExist = checkPolylineIndex(index_text);
            index_text++;
        } while (isExist);

        return "text_" + index_text + "_" + SessionUser.id + "_" + Random.Range(0, 999);
    }

    private bool checkPolylineIndex(int index)
    {
        bool isExist = false;

        foreach (GameObject text in GameObject.FindGameObjectsWithTag("entity-text"))
        {
            if (text.name.Contains("text_" + index)) isExist = true;
        }

        return isExist;
    }

    /// <summary>
    /// Hapus Object Terpilih
    /// </summary>
    public void DeleteObject()
    {
        if (SELECTED_MARKER == null) return;

        EntityEditorController.Instance.plotTrash.Add(
            new JObject()
            {
                { "id", SELECTED_MARKER.instance.name },
                { "type", "text" }
            }
        );

        OnlineMapsMarker3DManager.RemoveItem(SELECTED_MARKER);

        Debug.Log("Data Text Berhasil Dihapus!");
        btnToggler.toggleOff();
    }
    #endregion

    #region HELPER
    private void Update()
    {
        colorBtn.color = colorPicker.color;
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (mode == plotMode.ONPLOT)
            {
                mode = plotMode.CREATE;

                PlotNotificationHelper.Instance.TogglePlotNotification(false);
                GetComponent<Animator>().Play("SlideIn");
            }
            else if (mode == plotMode.ONMOVE)
            {
                mode = plotMode.EDIT;
                SELECTED_MARKER.OnPress = null;
                SELECTED_MARKER.OnDrag = null;
                SELECTED_MARKER.label = "";

                PlotNotificationHelper.Instance.TogglePlotNotification(false);
                GetComponent<Animator>().Play("SlideIn");
            }
        }
    }
    #endregion
}
