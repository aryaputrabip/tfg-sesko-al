using UnityEngine;
using System.Collections.Generic;
using GlobalVariables;
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ObjectStat;
using HelperPlotting;
using Plot.FormasiPlot;

public class SavePlotController : MonoBehaviour
{
    public Animator loadingOverlay;
    PlotSatuanController _sat = PlotSatuanController.Instance;
    public async void savePlot()
    {
        loadingOverlay.Play("ServiceSlideIn");

        // Mulai Obstacle
        List<jsonObstacle> obs = new List<jsonObstacle>();
        foreach (GameObject x in GameObject.FindGameObjectsWithTag("Plot_Obstacle"))
        {
           ObstacleStat ds = x.GetComponent<ObstacleStat>();
           if (ds != null)
           {
               jsonObstacle al = new jsonObstacle();
               al.id_user = SessionUser.id;
               al.dokumen = SkenarioAktif.ID_DOCUMENT;
               al.nama = ds.nama;
               al.lat_y = ds.lngLat.y;
               al.lng_x = ds.lngLat.x;
               al.symbol = SymbolObstacle.FromJson(ds.symbol);
               al.info_obstacle = Info_Obstacle.FromJson(ds.info_obstacle);

               obs.Add(al);
           }
        }
        string JSONObs = (obs.Count > 0) ? JsonConvert.SerializeObject(obs) : null;

        // Mulai Satuan
        //List<jsonSatuan> ls = new List<jsonSatuan>();
        List<jsonSatuan> ls = new List<jsonSatuan>();
        foreach (GameObject gp in GameObject.FindGameObjectsWithTag("entity-satuan"))
        {
            JSONDataSatuan ds = gp.GetComponent<JSONDataSatuan>();
            if (ds != null)
            {
                var bun = gp.GetComponent<AlutsistaBundleData>();

                jsonSatuan jsat = new jsonSatuan();
                jsat.id_user = SessionUser.id;
                jsat.dokumen = SkenarioAktif.ID_DOCUMENT;
                jsat.id_symbol = (bun.id_symbol != null && bun.id_symbol != "") ? Int32.Parse(bun.id_symbol) : Int32.Parse(ds.id_symbol);
                //jsat.dokumen = SkenarioAktif.ID_DOCUMENT;
                jsat.nama = ds.nama;
                jsat.lat_y = ds.marker.position.y;
                jsat.lng_x = ds.marker.position.x;
                jsat.style = ds.style;
                jsat.info = ds.info;
                jsat.symbol = ds.symbol;

                ls.Add(jsat);
            }
        }

        // List Json Formasi
        List<jsonFormasi> lf = new List<jsonFormasi>();
        foreach (GameObject gp in GameObject.FindGameObjectsWithTag("entity-formasi"))
        {
            EntityFormasiObj pf = gp.GetComponent<EntityFormasiObj>();
            if (pf != null)
            {
                EntityFormasi ef = EntityFormasi.FromJson(pf.json);
                Debug.Log(ef.info_formasi);
                //Info_Formasi inf = Info_Formasi.FromJson(ef.info_formasi);

                jsonFormasi jf = new jsonFormasi();
                jf.id_user = SessionUser.id;
                jf.dokumen = SkenarioAktif.ID_DOCUMENT;
                jf.nama = ef.nama;
                jf.lat_y = 0;
                jf.lng_x = 0;
                jf.info_formasi = ef.info_formasi;
                jf.tgl_formasi = null;
                jf.kecepatan = null;
                jf.size = 20;
                jf.jenis_formasi = ef.jenis_formasi;
                jf.warna = ef.warna;
                
                jf.symbol = null;
                //jf.warna = inf.warna;

                lf.Add(jf);
            }
        }
        string JSONForm = (lf.Count > 0) ? JsonConvert.SerializeObject(lf) : null;

        var formasiV2   = GameObject.FindGameObjectsWithTag("entity-formasi");
        // var listFormasi = new List<EntityFormasiV2>();
        // foreach(var child in formasiV2)
        // {
        //     var data = child.GetComponent<DataFormasi>();
        //     listFormasi.Add(data.entity);
        // }
        // JSONForm = listFormasi.Count > 0 ? EntityFormasiV2.ToString(listFormasi) : string.Empty;
        // Debug.Log(JSONForm);

        foreach (GameObject gp in GameObject.FindGameObjectsWithTag("Plot_Pasukan"))
        {
            Stat stat = gp.GetComponent<Stat>();
            if(stat != null)
            {
                jsonSatuan jsat = new jsonSatuan
                {
                    id_user = SessionUser.id,
                    dokumen = SkenarioAktif.ID_DOCUMENT,
                    id_symbol = int.Parse(stat.fontTypeID),
                    //jsat.dokumen = SkenarioAktif.ID_DOCUMENT;
                    nama = stat.namaSatuan,
                    lat_y = stat.marker3D.position.y,
                    lng_x = stat.marker3D.position.x,
                    style = stat.style,
                    info = stat.info,
                    symbol = stat.symbol
                };

                ls.Add(jsat);
            }
        }

        var pasukanV2 = GameObject.FindGameObjectsWithTag("Plot_Pasukan");
        var listPas   = new List<EntityPasukanV2>();
        foreach(var child in pasukanV2)
        {
            var stat = child.GetComponent<StatV2>();
            var entity = stat.pasukan;
            listPas.Add(entity);

            var styleForSave = new StylePasukanForSave()
            {
                nama  = entity.style.nama,
                index = entity.style.index,
                keterangan = entity.style.keterangan,
                grup  = entity.style.grup,
                label_style = LabelStylePasukan.ToString(entity.style.label_style)
            };
            
            var jS = new jsonSatuan()
            {
                id_user = entity.id_user,
                dokumen = entity.dokumen,
                id_symbol = entity.id_symbol,
                nama = entity.nama,
                lat_y = entity.lat_y,
                lng_x = entity.lng_x,
                style = StylePasukanForSave.ToString(styleForSave),
                info  = InfoPasukan.ToString(entity.info),
                symbol = entity.symbol
            };
            ls.Add(jS);
        }
        string JSONSat = (ls.Count > 0) ? JsonConvert.SerializeObject(ls, Formatting.Indented) : string.Empty;
        // JSONSat = listPas.Count > 0 ? EntityPasukanV2.ToString(listPas) : string.Empty;
        Debug.Log(JSONSat);

        // Mulai Trash
        string JSONTrash = (EntityEditorController.Instance.plotTrash.Count > 0) ? JsonConvert.SerializeObject(EntityEditorController.Instance.plotTrash) : null;
        Debug.Log("Trash : " + JSONTrash);

        // Mulai Rute Misi
        List<jsonMisi> lm = new List<jsonMisi>();
        foreach (GameObject gp in GameObject.FindGameObjectsWithTag("entity-route"))
        {
            PlotDataRoute mr = gp.GetComponent<PlotDataRoute>();
            if (mr != null)
            {
                jsonMisi jm = new jsonMisi();

                jm.id_user = SessionUser.id;
                jm.id_object = mr.id_satuan;
                jm.properties = mr.properties;
                jm.tgl_mulai = mr.routeStart.ToString("MM-dd-yyyy HH:mm");
                jm.dokumen = SkenarioAktif.ID_DOCUMENT;
                jm.jenis = mr.routeType;
                jm.id_mission = mr.id_mission;

                lm.Add(jm);
            }
        }
        string JSONMis = (lm.Count > 0) ? JsonConvert.SerializeObject(lm): null;

        // Mulai Bungus
        List<jsonBungus> lb = new List<jsonBungus>();
        foreach (GameObject gp in GameObject.FindGameObjectsWithTag("entity-bungus"))
        {
            JSONDataBungus ds = gp.GetComponent<JSONDataBungus>();
            if (ds != null)
            {
                jsonBungus jb = new jsonBungus();
                jb.id_user = SessionUser.id;
                jb.dokumen = SkenarioAktif.ID_DOCUMENT;
                jb.nama = ds.nama;
                jb.lat_y = (float)ds.lat_y;
                jb.lng_x = (float)ds.lng_x;
                jb.info_bungus = ds.info_bungus;
                jb.symbol = ds.symbol;

                lb.Add(jb);
            }
        }
        string JSONBun = (lb.Count > 0) ? JsonConvert.SerializeObject(lb) : null;

        // Mulai Logistik
        List<jsonLogistik> ll = new List<jsonLogistik>();
        foreach (GameObject gp in GameObject.FindGameObjectsWithTag("entity-logistik"))
        {
            JSONDataLogistik ds = gp.GetComponent<JSONDataLogistik>();
            if(ds != null)
            {
                jsonLogistik jl = new jsonLogistik();
                jl.id_user = SessionUser.id;
                jl.dokumen = SkenarioAktif.ID_DOCUMENT;
                jl.nama = ds.nama;
                jl.lat_y = (float)ds.lat_y;
                jl.lng_x = (float)ds.lng_x;
                jl.info_logistik = ds.info_logistik;
                jl.isi_logistik = ds.isi_logistik;
                jl.jenis = ds.jenis;
                jl.warna = ds.warna;
                jl.symbol = ds.symbol;
                //jl.size = gp.transform.localScale.x;

                ll.Add(jl);
            }
        }
        string JSONLog = (ll.Count > 0) ? JsonConvert.SerializeObject(ll) : null;

        // Mulai Situasi
        List<jsonSituasi> lsit = new List<jsonSituasi>();
        foreach (GameObject gp in GameObject.FindGameObjectsWithTag("entity-situasi"))
        {
            JSONDataSituasi ds = gp.GetComponent<JSONDataSituasi>();
            if(ds != null)
            {
                jsonSituasi js = new jsonSituasi();
                js.id_user = (int)SessionUser.id;
                js.dokumen = SkenarioAktif.ID_DOCUMENT;
                js.nama = ds.nama;
                js.lat_y = (float) ds.lat_y;
                js.lng_x = (float) ds.lng_x;
                js.info_situasi = ds.info_situasi;
                js.symbol_situasi = ds.symbol_situasi;

                lsit.Add(js);
            }
        }
        string JSONSit = (lsit.Count > 0) ? JsonConvert.SerializeObject(lsit) : null;

        // Mulai Plot Radar
        List<jsonRadar> rad = new List<jsonRadar>();
        foreach (GameObject gr in GameObject.FindGameObjectsWithTag("entity-radar"))
        {
            JSONDataRadar dr = gr.GetComponent<JSONDataRadar>();
            if(dr != null)
            {
                jsonRadar jdr = new jsonRadar();
                jdr.id_user = (int)SessionUser.id;
                jdr.dokumen = SkenarioAktif.ID_DOCUMENT;
                jdr.nama = dr.nama;
                jdr.lat_y = (float)dr.lat_y;
                jdr.lng_x = (float)dr.lng_x;
                jdr.info_radar = dr.info_radar;
                jdr.symbol = dr.symbol;
                jdr.info_symbol = dr.info_symbol;

                rad.Add(jdr);
            }
        }
        string JSONRad = (rad.Count > 0) ? JsonConvert.SerializeObject(rad) : null;


        // Mulai Plot Polyline (LineString)
        List<jsonTool> tl = new List<jsonTool>();
        foreach (GameObject gd in GameObject.FindGameObjectsWithTag("draw-polyline"))
        {
            DrawStat ds = gd.GetComponent<DrawStat>();
            if (ds != null)
            {
                jsonTool jsonTool = new jsonTool();
                jsonTool.id_user = (int)SessionUser.id;
                jsonTool.dokumen = SkenarioAktif.ID_DOCUMENT;
                jsonTool.nama = ds.name;
                jsonTool.geometry = ds.geometry;
                jsonTool.type = "LineString";
                jsonTool.properties = ds.properties;

                tl.Add(jsonTool);
            }
        }
        //string JSONTool = (tl.Count > 0) ? JsonConvert.SerializeObject(tl) : null;

        // Mulai Plot Polygon
        foreach (GameObject gd in GameObject.FindGameObjectsWithTag("draw-polygon"))
        {
            DrawStat ds = gd.GetComponent<DrawStat>();
            if (ds != null)
            {
                jsonTool jsonTool = new jsonTool();
                jsonTool.id_user = (int)SessionUser.id;
                jsonTool.dokumen = SkenarioAktif.ID_DOCUMENT;
                jsonTool.nama = ds.name;
                jsonTool.geometry = ds.geometry;
                jsonTool.type = "Polygon";
                jsonTool.properties = ds.properties;

                tl.Add(jsonTool);
            }
        }

        // Mulai Plot Polygon
        foreach (GameObject gd in GameObject.FindGameObjectsWithTag("draw-circle"))
        {
            DrawStat ds = gd.GetComponent<DrawStat>();
            if (ds != null)
            {
                jsonTool jsonTool = new jsonTool();
                jsonTool.id_user = (int)SessionUser.id;
                jsonTool.dokumen = SkenarioAktif.ID_DOCUMENT;
                jsonTool.nama = ds.name;
                jsonTool.geometry = ds.geometry;
                jsonTool.type = "circle";
                jsonTool.properties = ds.properties;

                tl.Add(jsonTool);
            }
        }
        string JSONTool = (tl.Count > 0) ? JsonConvert.SerializeObject(tl) : null;

        // Mulai Teks
        List<jsonText> jt = new List<jsonText>();
        foreach (GameObject gp in GameObject.FindGameObjectsWithTag("entity-text"))
        {
            JSONDataText dt = gp.GetComponent<JSONDataText>();
            if (dt != null)
            {
                jsonText jText = new jsonText();
                jText.id_user = (int)SessionUser.id;
                jText.dokumen = SkenarioAktif.ID_DOCUMENT;
                jText.nama = dt.nama;
                jText.lat_y = dt.lat_y;
                jText.lng_x = dt.lng_x;
                jText.info_text = dt.info_text;
                jText.symbol = dt.symbol;

                jt.Add(jText);
            }      
                
        }
        string JSONText = (jt.Count > 0) ? JsonConvert.SerializeObject(jt) : null;


        // Mulai Kekuatan
        List<jsonKekuatan> jk = new List<jsonKekuatan>();
        foreach (GameObject gp in GameObject.FindGameObjectsWithTag("Plot_Kekuatan"))
        {
            KekuatanStat ks = gp.GetComponent<KekuatanStat>();
            if (ks != null)
            {
                jsonKekuatan jKek = new jsonKekuatan();
                jKek.id_user = (int)SessionUser.id;
                jKek.dokumen = SkenarioAktif.ID_DOCUMENT;
                jKek.nama = ks.namaSatuan;
                jKek.lat_y = (ks.marker3D.position.y > 0) ? ks.marker3D.position.y : ks.lat_y;
                jKek.lng_x = (ks.marker3D.position.x > 0) ? ks.marker3D.position.x : ks.lng_x;
                jKek.info_kekuatan = ks.infoKekuatan;
                jKek.rincian = ks.rincian;
                jKek.size = 20;

                jk.Add(jKek);
            }

        }
        string JSONKek = (jk.Count > 0) ? JsonConvert.SerializeObject(jk) : null;

        //if (obs.Count > 0 || ls.Count > 0)
        //{
        Debug.Log(await API.Instance.simpanDokumen(JSONObs, JSONSat, JSONBun, JSONLog, JSONSit, JSONRad, JSONTool, JSONText, JSONMis, JSONForm, JSONKek, JSONTrash));
        loadingOverlay.Play("ServiceSlideOut");
        //}
    }

    public class jsonObstacle
    {
        public long? id_user;
        public string dokumen;
        public string nama;
        public float lat_y;
        public float lng_x;
        public Info_Obstacle info_obstacle;
        public SymbolObstacle symbol;
    }

    public class jsonSatuan
    {
        public long? id_user;
        public int id_symbol;
        public string dokumen;
        public string nama;
        public float lat_y;
        public float lng_x;
        public string style;
        public string info;
        public string symbol;
    }

    public class jsonBungus
    {
        public long? id_user;
        public string dokumen;
        public string nama;
        public float lat_y;
        public float lng_x;
        public string info_bungus;
        public string symbol;
    }

    public class jsonLogistik
    {
        public long? id_user;
        public string dokumen;
        public string nama;
        public float lat_y;
        public float lng_x;
        public string info_logistik;
        public string isi_logistik;
        public string jenis;
        public string warna;
        public string symbol;
        public float size;
    }

    public class jsonSituasi
    {
        public long? id_user;
        public string dokumen;
        public string nama;
        public float lat_y;
        public float lng_x;
        public string info_situasi;
        public string symbol_situasi;
    }

    public class jsonRadar
    {
        public long? id_user;
        public string dokumen;
        public string nama;
        public float lat_y;
        public float lng_x;
        public string info_radar;
        public string symbol;
        public string info_symbol;
    }

    public class jsonTool
    {
        public long? id_user;
        public string dokumen;
        public string nama;
        public string geometry;
        public string properties;
        public string type;
    }

    public class jsonMisi
    {
        public long? id_user;
        public string id_object;
        public string properties;
        public string tgl_mulai;
        public string dokumen;
        public string jenis;
        public string id_mission;
    }

    public class jsonText
    {
        public long? id_user;
        public string dokumen;
        public string nama;
        public double lat_y;
        public double lng_x;
        public string info_text;
        public string symbol;
    }

    public class jsonFormasi
    {
        public long? id_user;
        public string dokumen;
        public string nama;
        public double lat_y;
        public double lng_x;
        public string info_formasi;
        public string tgl_formasi;
        public string kecepatan;
        public int size;
        public string jenis_formasi;
        public string symbol;
        public string warna;
    }

    public class jsonKekuatan
    {
        public long? id_user;
        public string dokumen;
        public string nama;
        public double lat_y;
        public double lng_x;
        public string info_kekuatan;
        public string rincian;
        public int size;
    }
}