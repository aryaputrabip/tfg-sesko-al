using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class CircleSlider : MonoBehaviour
{
    [SerializeField] Transform handle;
    [SerializeField] Image fill;
    public int heading;
    public int snapRotation = 30;

    public UnityEvent OnValueChange;

    Vector3 mousePos;

    public void onHandleDrag()
    {
        mousePos = Input.mousePosition;
        Vector2 dir = mousePos - handle.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        angle = (angle <= 0) ? (360 + angle) : angle;

        Quaternion r = Quaternion.identity;

        if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
        {
            int divided = (int)(angle - 90) / snapRotation;
            r = Quaternion.AngleAxis((divided * snapRotation), Vector3.forward);
        }
        else
        {
            r = Quaternion.AngleAxis(angle - 90, Vector3.forward);
        }
        handle.rotation = r;

        heading = (int) (handle.localRotation.eulerAngles.z -360) * -1;
        OnValueChange.Invoke();
    }

    public void set(int value)
    {
        Quaternion r = Quaternion.AngleAxis((value - 360) * -1, Vector3.forward);
        handle.rotation = r;

        heading = value;
        OnValueChange.Invoke();
    }

    public void reset()
    {
        heading = 0;
        fill.fillAmount = 0;
        handle.rotation = Quaternion.Euler(Vector3.zero);
        OnValueChange.Invoke();
    }

    public void OnKnobHeadingChange(TMP_InputField input)
    {
        if (input == null) return;
        input.text = heading.ToString();
    }
}
