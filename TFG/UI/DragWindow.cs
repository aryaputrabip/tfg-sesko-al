﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragWindow : MonoBehaviour, IDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private RectTransform dragRectTransform;
    [SerializeField] private Canvas canvas;
    [SerializeField] private Vector3 originalPos;
    private RuntimeAnimatorController animBackup;
    private CanvasGroup canvasGroupBackup;

    private void Start()
    {
        animBackup = dragRectTransform.GetComponent<Animator>().runtimeAnimatorController;
        canvasGroupBackup = dragRectTransform.GetComponent<CanvasGroup>();
    }

    private void Awake()
    {
        if (dragRectTransform == null) dragRectTransform = transform.parent.GetComponent<RectTransform>();
        originalPos = dragRectTransform.GetComponent<RectTransform>().anchoredPosition;

        if (canvas == null)
        {
            Transform canvasTransform = transform.parent;
            while(canvasTransform != null)
            {
                canvas = canvasTransform.GetComponent<Canvas>();
                if (canvas != null) break;
            }

            canvasTransform = canvasTransform.parent;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        dragRectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        dragRectTransform.GetComponent<Animator>().runtimeAnimatorController = null;

        var canvasGroup = dragRectTransform.GetComponent<CanvasGroup>();
        canvasGroup.alpha = 1;
        canvasGroup.interactable = true;
        canvasGroup.blocksRaycasts = true;
    }

    public async void OnPointerUp(PointerEventData eventData)
    {
        dragRectTransform.gameObject.GetComponent<Animator>().runtimeAnimatorController = animBackup;

        dragRectTransform.GetComponent<CanvasGroup>().alpha = canvasGroupBackup.alpha;
        dragRectTransform.GetComponent<CanvasGroup>().interactable = canvasGroupBackup.interactable;
        dragRectTransform.GetComponent<CanvasGroup>().blocksRaycasts = canvasGroupBackup.blocksRaycasts;
    }

    public async void OnEndDrag(PointerEventData eventData)
    {
        
    }

    public void resetToOriginalPosition()
    {
        dragRectTransform.anchoredPosition = originalPos;
    }
}
