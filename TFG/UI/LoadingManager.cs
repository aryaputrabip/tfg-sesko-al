using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

using TMPro;

public class LoadingManager : MonoBehaviour
{
    private Metadata metadata;
    private GameObject activeLoading;

    #region INSTANCE
    public static LoadingManager instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    private void Start()
    {
        metadata = GetComponent<Metadata>();
    }

    /// <summary>
    /// Play or Show Selected Loading Component
    /// </summary>
    /// <param name="name">Name of Loading Component you want to load</param>
    /// <param name="message">Custom Message on Loading Component (optional)</param>
    /// <param name="animation">Custom Animation to toggle Loading Component (optional)</param>
    /// <param name="messageComponent">Custom Message Component on Loading Component Metadata (optional)</param>
    public void ShowLoading(string name, string message = null, string animation = null, string messageComponent = null)
    {
        if(metadata != null)
        {
            activeLoading = metadata.FindParameter(name).parameter;
            activeLoading.name = name;

            activeLoading.GetComponent<Animator>().Play((animation != null) ? animation : "FadeIn");

            if(activeLoading.GetComponent<Metadata>() != null)
            {
                var objMessage = activeLoading.GetComponent<Metadata>().FindParameter((messageComponent != null) ? messageComponent : "message").parameter;

                if (message != null)
                {
                    objMessage.SetActive(true);
                    objMessage.GetComponent<TextMeshProUGUI>().text = message;
                }
                else
                {
                    objMessage.SetActive(false);
                }
            }
        }
        else
        {
            Debug.LogError("Loading Manager doesn't have any Metadata. Failed To Show Loading Component!");
        }
    }

    public async void HideLoading(string animation = null)
    {
        if (activeLoading == null) return;

        activeLoading.GetComponent<Animator>().Play((animation != null) ? animation : "FadeOut");
        activeLoading = null;
    }

    //private void Update()
    //{
    //    if (changeBlurIteration)
    //    {
    //        if (blurIteration <= 0 || blurIteration >= 15) changeBlurIteration = false;

    //        blurIteration = blurIteration + blurChange;
    //        activeLoading.GetComponent<Image>().material.SetFloat("_Iterations", blurIteration * (Time.deltaTime * 50));
    //    }
    //}
}
