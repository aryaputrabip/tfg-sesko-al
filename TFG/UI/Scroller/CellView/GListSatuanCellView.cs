using UnityEngine;
using UnityEngine.UI;

using EnhancedUI.EnhancedScroller;
using TMPro;

public class GListSatuanCellView : EnhancedScrollerCellView
{
    [Header("References")]
    public Image satuanImage;
    public TextMeshProUGUI satuanName;
    public TextMeshProUGUI satuanDesc;
    public Text satuanSymbol;
    public Button locateBtn;

    [Header("Satuan Data")]
    public OnlineMapsMarker3D marker;

    public void SetData(GListSatuanScrollerData data)
    {
        satuanImage.sprite = data.image_satuan;
        satuanName.text = data.nama_satuan;
        satuanDesc.text = data.desc_satuan;
        satuanSymbol.text = data.index;
        satuanSymbol.font = data.fontFamily;

        marker = data.marker;
        locateBtn.onClick.AddListener(delegate { ListEntityController.Instance.LocateEntity(marker); });

        GetComponent<Button>().onClick.AddListener(delegate { ListEntityController.Instance.OnEntitySelected(marker); });
    }
}
