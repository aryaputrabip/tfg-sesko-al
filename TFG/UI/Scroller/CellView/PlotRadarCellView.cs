using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using TMPro;
using EnhancedUI.EnhancedScroller;
using HelperEditor;

public class PlotRadarCellView : EnhancedScrollerCellView
{
    [Header("References")]
    public TextMeshProUGUI radarName;
    public TextMeshProUGUI radarDesc;
    public Text radarSymbol;

    public ListRadarData listRadarData;

    public void SetData(PlotRadarScrollerData data)
    {
        radarName.text = data.nama_radar;
        radarDesc.text = data.desc_radar;
        radarSymbol.text = data.index;
        radarSymbol.font = data.fontFamily;

        // Data Dari Radar Pada List Disimpan Disini
        listRadarData = data.ListRadarData;

        GetComponent<Button>().onClick.AddListener(delegate { PlotRadarController.Instance.OnListSelected(listRadarData); });
    }
}
