using System.Collections;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

using EnhancedUI.EnhancedScroller;
using TMPro;

using HelperEditor;

public class PlotSatuanCellView : EnhancedScrollerCellView
{
    // Plot Satuan Cell View Juga Digunakan di Setiap Slot Satuan
    // - Set to "true" jika object ini adalah slot satuan
    public bool isSlot = false;

    public enum typeObject { Darat = 1, Laut = 2, Udara = 3 }
    public enum jenisObject { Static = 1, Wheeled = 2, Tracked = 3, Surface = 4, Sub = 5, Amphibi = 6, Rotary = 7, Fixed = 8, Space = 9}

    [Header("References")]
    public Image satuanImage;
    public TextMeshProUGUI satuanName;
    public TextMeshProUGUI satuanDesc;
    public Text satuanSymbol;

    [Header("Satuan Data")]
    //public string id;
    //public string name;

    //public Font fontFamily;
    //public string index;

    //public ListSatuanData.typeObject tipe_obj;
    //public ListSatuanData.jenisObject jenis_obj;

    [Header("Obj Components")]
    public AlutsistaBundleData objPrefab;
    public string obj3D;

    [Header("Data Components")]
    public StyleListSatuan StyleSatuan;
    public string InfoSatuan;
    public string SymbolSatuan;

    private void Start()
    {
        if (isSlot)
        {
            RegisterEventHandler();
        }
    }

    public void SetData(PlotSatuanScrollerData data)
    {
        GetComponent<Button>().onClick.RemoveAllListeners();

        satuanImage.sprite = data.image_satuan;
        satuanName.text = data.nama_satuan;
        satuanDesc.text = data.desc_satuan;
        satuanSymbol.text = data.index;
        satuanSymbol.font = data.fontFamily;

        objPrefab = data.ListSatuanData.objPrefab;
        obj3D = data.ListSatuanData.obj3D;

        StyleSatuan = data.ListSatuanData.styleSatuan;
        //id = data.ListSatuanData.id;
        //name = data.ListSatuanData.name;
        //fontFamily = data.fontFamily;
        //index = data.index;
        //tipe_obj = data.ListSatuanData.tipe_obj;
        //jenis_obj = data.ListSatuanData.jenis_obj;

        if(transform.parent.parent.parent.parent.name == "Modal - Asset Satuan")
        {
            GetComponent<Button>().onClick.AddListener(delegate { PlotKekuatanController.Instance.OnSatuanSelected(data, this); });
        }
        else
        {
            GetComponent<Button>().onClick.AddListener(delegate { PlotSatuanController.Instance.OnSatuanSelected(data, this); });

            RegisterEventHandler();
        }
    }

    public void RegisterEventHandler()
    {
        // Event Trigger
        var eventTrigger = GetComponent<EventTrigger>();

        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.BeginDrag;
        entry.callback.AddListener(delegate { OnBeginDrag(this); });
        eventTrigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.Drag;
        entry.callback.AddListener((eventData) => { OnDrag((PointerEventData)eventData); });
        eventTrigger.triggers.Add(entry);

        if (!isSlot)
        {
            // Event End Drag To Slot Satuan
            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.EndDrag;
            entry.callback.AddListener((eventData) => { OnEndDrag((PointerEventData)eventData); });
            eventTrigger.triggers.Add(entry);
        }
        else
        {
            // Event End Drag From Slot Satuan
            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.EndDrag;
            entry.callback.AddListener((eventData) => { OnEndSlotDrag((PointerEventData)eventData, this); });
            eventTrigger.triggers.Add(entry);
        }
    }

    public void reset()
    {
        // Reset Cell View (Untuk Slot Satuan ketika di drag keluar)
        satuanImage.sprite = null;

        var color = Color.black;
        color.a = 0;
        satuanImage.color = color;
        objPrefab = null;
        obj3D = "";
        StyleSatuan = null;
        InfoSatuan = "";
        SymbolSatuan = "";
    }

    public void OnBeginDrag(PlotSatuanCellView cellData)
    {
        SlotSatuanController.Instance.BeginDragSatuan(cellData.gameObject);
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (SlotSatuanController.Instance == null) return;
        SlotSatuanController.Instance.OnDrag(eventData);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        SlotSatuanController.Instance.EndDragSatuan(eventData);
    }

    public void OnEndSlotDrag(PointerEventData eventData, PlotSatuanCellView cellData)
    {
        SlotSatuanController.Instance.EndDragSlotSatuan(eventData, cellData);
    }
}
