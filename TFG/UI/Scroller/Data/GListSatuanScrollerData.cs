using UnityEngine;

public class GListSatuanScrollerData
{
    public string nama_satuan;
    public string desc_satuan;
    public Sprite image_satuan;

    public string index;
    public Font fontFamily;

    public OnlineMapsMarker3D marker;
}