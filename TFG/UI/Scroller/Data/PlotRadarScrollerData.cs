using UnityEngine;

public class PlotRadarScrollerData
{
    public string nama_radar;
    public string desc_radar;

    public string index;
    public Font fontFamily;

    public ListRadarData ListRadarData;
}
