using UnityEngine;

public class PlotSatuanScrollerData
{
    public string id_satuan;
    public string matra_satuan;

    public string nomor_satuan;
    public string nomor_atasan;

    public string nama_satuan;
    public string desc_satuan;
    public Sprite image_satuan;

    public string index;
    public Font fontFamily;

    public ListSatuanData ListSatuanData;
}
