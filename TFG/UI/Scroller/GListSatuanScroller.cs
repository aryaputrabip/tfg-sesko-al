using System.Collections.Generic;
using UnityEngine;

using EnhancedUI.EnhancedScroller;
using Unity.VisualScripting;
using UnityEngine.UI;

public class GListSatuanScroller : MonoBehaviour, IEnhancedScrollerDelegate
{
    public List<GListSatuanScrollerData> _data;
    public EnhancedScroller scroller;
    public GListSatuanCellView cellViewPrefab;

    void Start()
    {
        _data = new List<GListSatuanScrollerData>();

        var fitter = scroller.transform.Find("Container").AddComponent<ContentSizeFitter>();
        fitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
        RectTransformExtension.SetLeft(fitter.GetComponent<RectTransform>(), 8);
        RectTransformExtension.SetRight(fitter.GetComponent<RectTransform>(), 15);
        RectTransformExtension.SetTop(fitter.GetComponent<RectTransform>(), 10);
        RectTransformExtension.SetBottom(fitter.GetComponent<RectTransform>(), 10);
        fitter.GetComponent<VerticalLayoutGroup>().spacing = 5f;

        scroller.Delegate = this;
        scroller.ReloadData();
    }

    public void AddData(OnlineMapsMarker3D marker, string nama_satuan, string desc_satuan, Sprite image_satuan, string index, Font fontFamily)
    {
        _data.Add(new GListSatuanScrollerData()
        {
            nama_satuan = nama_satuan,
            desc_satuan = desc_satuan,
            image_satuan = image_satuan,
            index = index,
            fontFamily = fontFamily,
            marker = marker
        });
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return _data.Count;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 66f;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        GListSatuanCellView cellView = scroller.GetCellView(cellViewPrefab) as GListSatuanCellView;
        cellView.SetData(_data[dataIndex]);
        return cellView;
    }
}
