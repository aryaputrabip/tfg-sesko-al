using System.Collections.Generic;
using UnityEngine;

using EnhancedUI.EnhancedScroller;
using Unity.VisualScripting;
using UnityEngine.UI;
using HelperEditor;

public class PlotRadarScroller : MonoBehaviour, IEnhancedScrollerDelegate
{
    private List<PlotRadarScrollerData> _data;
    public EnhancedScroller scroller;
    public PlotRadarCellView cellViewPrefab;

    void Start()
    {
        _data = new List<PlotRadarScrollerData>();

        var fitter = scroller.transform.Find("Container").AddComponent<ContentSizeFitter>();
        fitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
        RectTransformExtension.SetLeft(fitter.GetComponent<RectTransform>(), 8);
        RectTransformExtension.SetRight(fitter.GetComponent<RectTransform>(), 15);
        RectTransformExtension.SetTop(fitter.GetComponent<RectTransform>(), 10);
        RectTransformExtension.SetBottom(fitter.GetComponent<RectTransform>(), 10);
        fitter.GetComponent<VerticalLayoutGroup>().spacing = 5f;

        scroller.Delegate = this;
        scroller.ReloadData();
    }

    public void AddData(ListRadarData listRadar)
    {
        _data.Add(new PlotRadarScrollerData()
        {
            nama_radar = listRadar.nama_radar,
            desc_radar = listRadar.desc_radar,
            index = ((char)listRadar.index).ToString(),
            fontFamily = listRadar.fontFamily,
            ListRadarData = listRadar
        });
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return _data.Count;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 66f;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        PlotRadarCellView cellView = scroller.GetCellView(cellViewPrefab) as PlotRadarCellView;
        cellView.SetData(_data[dataIndex]);
        return cellView;
    }
}
