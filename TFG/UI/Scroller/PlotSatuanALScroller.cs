using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using EnhancedUI.EnhancedScroller;
using Unity.VisualScripting;
using UnityEngine.UI;
using System.Threading.Tasks;

public class PlotSatuanALScroller : MonoBehaviour, IEnhancedScrollerDelegate
{
    private List<PlotSatuanScrollerData> _data;
    public EnhancedScroller scroller;
    public PlotSatuanCellView cellViewPrefab;
    
    void Start()
    {
        _data = new List<PlotSatuanScrollerData>();

        var fitter = scroller.transform.Find("Container").AddComponent<ContentSizeFitter>();
        fitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
        RectTransformExtension.SetLeft(fitter.GetComponent<RectTransform>(), 8);
        RectTransformExtension.SetRight(fitter.GetComponent<RectTransform>(), 15);
        RectTransformExtension.SetTop(fitter.GetComponent<RectTransform>(), 10);
        RectTransformExtension.SetBottom(fitter.GetComponent<RectTransform>(), 10);
        fitter.GetComponent<VerticalLayoutGroup>().spacing = 5f;
        
        scroller.Delegate = this;
        scroller.ReloadData();
    }

    public void AddData(string nama_satuan, string desc_satuan, Sprite image_satuan, string index, Font fontFamily, ListSatuanData listSatuanData)
    {
        _data.Add(new PlotSatuanScrollerData()
        {
            nama_satuan = nama_satuan,
            desc_satuan = desc_satuan,
            image_satuan = image_satuan,
            index = index,
            fontFamily = fontFamily,
            ListSatuanData = listSatuanData
        });
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return _data.Count;
    }

    
    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 66f;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        PlotSatuanCellView cellView = scroller.GetCellView(cellViewPrefab) as PlotSatuanCellView;
        cellView.SetData(_data[dataIndex]);
        return cellView;
    }
}
