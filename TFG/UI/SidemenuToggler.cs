using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class SidemenuToggler : MonoBehaviour
{
    [Header("Menu Color")]
    public Color activeMenu;
    public Color disableMenu = Color.white;
    public string iconName = "Icon";

    public UnityEvent onToggleOn;
    public UnityEvent onToggleOff;

    private bool isActive = false;

    public void toggleIcon()
    {
        var icon = transform.Find(iconName).GetComponent<Image>();
        icon.color = icon.color != activeMenu ? activeMenu : disableMenu;

        if(icon.color == activeMenu)
        {
            onToggleOn.Invoke();
        }
        else
        {
            onToggleOff.Invoke();
        }
    }

    public void toggleOff()
    {
        onToggleOff.Invoke();

        var icon = transform.Find(iconName).GetComponent<Image>();
        icon.color = disableMenu;
    }

    public void toggleOn()
    {
        onToggleOn.Invoke();

        var icon = transform.Find(iconName).GetComponent<Image>();
        icon.color = activeMenu;
    }

    public void toggleIconV2()
    {
        if (!isActive)
        {
            onToggleOn.Invoke();
        }
        else
        {
            onToggleOff.Invoke();
        }

        isActive = (isActive) ? false : true;
    }

    public void toggleOnV2()
    {
        isActive = true;
        onToggleOn.Invoke();
    }

    public void toggleOffV2 ()
    {
        isActive = false;
        onToggleOff.Invoke();
    }
}
