using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class SidemenuTogglerMultistep : MonoBehaviour
{
    [Header("Menu Color")]
    public Color activeMenu;
    public Color disableMenu = Color.white;
    public string iconName = "Icon";
    public int stepIndex = 0;

    [Header("Step Icon")]
    public List<Sprite> stepIcon;

    public UnityEvent onToggleOn;
    public UnityEvent onToggleOff;
    public List<UnityEvent> onToggleStep;

    public void toggleIcon()
    {
        stepIndex++;
        var icon = transform.Find(iconName).GetComponent<Image>();

        if(stepIndex == 0)
        {
            icon.color = activeMenu;
            icon.sprite = stepIcon[stepIndex - 1];
            onToggleOn.Invoke();
            stepIndex = 1;
        }else if(stepIndex > onToggleStep.Count)
        {
            icon.color = disableMenu;
            icon.sprite = stepIcon[0];
            onToggleOff.Invoke();
            stepIndex = 0;
        }
        else
        {
            icon.color = activeMenu;
            icon.sprite = stepIcon[stepIndex - 1];
            onToggleStep[stepIndex - 1].Invoke();
        }

    }

    public void toggleOff()
    {
        onToggleOff.Invoke();

        var icon = transform.Find(iconName).GetComponent<Image>();
        icon.color = disableMenu;
        icon.sprite = stepIcon[0];

        stepIndex = 0;
    }

    public void toggleOn()
    {
        onToggleOn.Invoke();

        var icon = transform.Find(iconName).GetComponent<Image>();
        icon.color = activeMenu;
        icon.sprite = stepIcon[0];

        stepIndex = 1;
    }

    public void toggleStep(int step)
    {
        onToggleStep[step].Invoke();
        var icon = transform.Find(iconName).GetComponent<Image>();
        icon.sprite = stepIcon[step];

        if (stepIndex > onToggleStep.Count)
        {
            toggleOff();
        }
        else
        {
            stepIndex++;
        }
    }
}
