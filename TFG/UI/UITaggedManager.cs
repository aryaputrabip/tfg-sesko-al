using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

public class UITaggedManager : MonoBehaviour
{
    [Header("Label - Time")]
    public List<TextMeshProUGUI> labelTimes = new List<TextMeshProUGUI>();
    [Header("Label - Date")]
    public List<TextMeshProUGUI> labelDates = new List<TextMeshProUGUI>();
    [Header("Label - HariH")]
    public List<TextMeshProUGUI> labelHariH = new List<TextMeshProUGUI>();
    
    private void Start()
    {
        InitListData(labelTimes, "label-time");
        InitListData(labelDates, "label-date");
        InitListData(labelHariH, "label-hariH");
    }

    private void InitListData(List<TextMeshProUGUI> selectedList, string tagname)
    {
        foreach (var item in GameObject.FindGameObjectsWithTag(tagname))
        {
            var objData = item.GetComponent<TextMeshProUGUI>();
            if(objData != null) selectedList.Add(objData);
        }
    }

    #region INSTANCE MANAGER
    // INSTANCE VARIABLE
    public static UITaggedManager Instance;
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion
}
