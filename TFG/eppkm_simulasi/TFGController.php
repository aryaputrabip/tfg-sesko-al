<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use DateTime;
Use Exception;

class TFGController extends Controller
{
	public function getWaktuAsumsi(Request $r){
		$date = $r->date;
		//$date = date('Y-m-d H:i:s');
		$id_skenario = $this->getSkenario()->ID;

		$ardata = null;

		$data = DB::table("master_skenario as a")
				->join("jadwal_pelaksanaan as b", "a.ID", "=", "b.id_skenario")
				->select("b.*")
				->where("a.status", 1)
				->get();


			for($i=0; $i<count($data); $i++){
				if( $data[$i]->waktu_sebenarnya <= $date && $data[$i]->waktu_sebenarnya_akhir >= $date){
					$ardata = array($data[$i]);
				}

			}
		return $ardata;

	}

	private function getSkenario(){
		$data = DB::table('master_skenario')->select('*')->where('status',1)->first();
		return $data;
	}

	private function getSkenarioAll(){
		$data = DB::table('master_skenario')->select('*')->get();
		return $data;
	}

	public function getKegiatanAll(Request $r){
		$data = DB::table('TFG.kegiatan')->select('*')->get();
		//$data = DB::table('p_master_kegiatan')->select('*')->whereIn('id_dokumen',json_decode($r->id))->get()->groupBy('id_dokumen');
		return $data;
	}

	public function getObjectFromDocument(Request $r){
		// try {
			//code...
			$kegiatan_data = DB::table('TFG.kegiatan as k')
				->join('TFG.cb as c', 'c.id', '=', 'k.id_cb')
				->select('k.*', 'c.skenario')
				->where('k.id_cb', $r->id_cb)
				->where('k.id_user', $r->id_user)
				->where('c.skenario', $r->skenario);

			$data = $kegiatan_data
				->where('k.id_obj_kegiatan', '!=', 0)
				->whereNotNull('k.id_obj_kegiatan')
				->orderBy('k.tanggal')
				->get();

			$obj_kegiatan = $data->unique('id_obj_kegiatan')->values()->all();

			$response = [];
			foreach ($obj_kegiatan as $obj) {
				$result = DB::table('TFG.obj_kegiatan')->select('*')->where('id', $obj->id_obj_kegiatan);
				if ($result->exists()) { $result = $result->first(); } 
				// else if (!$result->exists()) { $result = DB::table('object_alutsista_3d')->where('id_object_alutsista', $result->id_obj)->first(); }
				// else { $result = DB::table('TFG.obj_kegiatan')->where('id_object_alutsista', $result->id_obj)->first(); }

				$obj_3d = DB::table('object_alutsista_3d_2021')->where('id_object_alutsista', $result->id_obj);
				if ($obj_3d->exists()) { $obj_3d = $obj_3d->first(); } 
				// else if (!$obj_3d->exists()) { $obj_3d = DB::table('object_alutsista_3d')->where('id_object_alutsista', $result->id_obj)->first(); }
				else { $obj_3d = DB::table('object_alutsista_3d')->where('id_object_alutsista', $result->id_obj)->first(); }
				// else { $obj_3d = DB::table('object_alutsista_3d')->where('id_object_alutsista', 1)->first(); }

				$font_taktis = DB::table('font_taktis')->where('id',$obj_3d->id_font_taktis)->first();
				// if ($font_taktis->exists()) { $font_taktis = $font_taktis->first(); }
				// else if (!$font_taktis->exists()) { $font_taktis = DB::table('font_taktis')->where('grup', $obj_3d->tipe_tni)->first(); }
				// else { $font_taktis = DB::table('font_taktis')->where('grup', $obj_3d->tipe_tni)->first(); }	

				$response[] = [
					'obj_kegiatan' => $result,
					'obj_3d' => $obj_3d,
					'font_taktis' => $font_taktis,
					'schedule' => [
						'kegiatan' => DB::table('TFG.kegiatan')->select('*')
										->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
										->where('type', 'kegiatan')
										->orderBy('tanggal')
										->get(),
						'situasi' => DB::table('TFG.kegiatan')->select('*')
										->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
										->where('type', 'situasi')
										->orderBy('tanggal')
										->get(),
						'waypoint' => DB::table('TFG.kegiatan')->select('*')
										->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
										->where('type', 'waypoint')
										->orderBy('tanggal')
										->get(),
										//meidi
						'embarkasi' => DB::table('TFG.kegiatan')->select('*')
										->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
										->where('type', 'embarkasi')
										->orderBy('tanggal')
										->get(),
						'debarkasi' => DB::table('TFG.kegiatan')->select('*')
										->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
										->where('type', 'debarkasi')
										->orderBy('tanggal')
										->get(),
						'kekuatan' => DB::table('TFG.kegiatan')->select('*')
										->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
										->where('type', 'kekuatan')
										->orderBy('tanggal')
										->get(),
						'bungus' => DB::table('TFG.kegiatan')->select('*')
										->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
										->where('type', 'bungus')
										->orderBy('tanggal')
										->get(),
						'obstacle' => DB::table('TFG.kegiatan')->select('*')
										->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
										->where('type', 'obstacle')
										->orderBy('tanggal')
										->get(),
						'passen' => DB::table('TFG.kegiatan')->select('*')
										->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
										->where('type', 'passen')
										->orderBy('tanggal')
										->get(),
						'manuever' => DB::table('TFG.kegiatan')->select('*')
										->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
										->where('type', 'manuever')
										->orderBy('tanggal')
										->get(),
						'radar' => DB::table('TFG.kegiatan')->select('*')
										->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
										->where('type', 'radar')
										->orderBy('tanggal')
										->get(),
						'logis' => DB::table('TFG.kegiatan')->select('*')
										->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
										->where('type', 'logis')
										->orderBy('tanggal')
										->get(),
						'tool-linestring' => DB::table('TFG.kegiatan')->select('*')
										->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
										->where('type', 'tool-linestring')
										->orderBy('tanggal')
										->get(),
						'tool-polygon' => DB::table('TFG.kegiatan')->select('*')
										->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
										->where('type', 'tool-polygon')
										->orderBy('tanggal')
										->get(),
						'tool-circle' => DB::table('TFG.kegiatan')->select('*')
										->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
										->where('type', 'tool-circle')
										->orderBy('tanggal')
										->get(),
						'tool-arrowatasman' => DB::table('TFG.kegiatan')->select('*')
										->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
										->where('type', 'tool-arrowatasman')
										->orderBy('tanggal')
										->get(),
						'tool-arrowbawahman' => DB::table('TFG.kegiatan')->select('*')
										->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
										->where('type', 'tool-arrowbawahman')
										->orderBy('tanggal')
										->get(),
						'tool-arrowatas' => DB::table('TFG.kegiatan')->select('*')
										->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
										->where('type', 'tool-arrowatas')
										->orderBy('tanggal')
										->get(),
						'tool-arrowbawah' => DB::table('TFG.kegiatan')->select('*')
										->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
										->where('type', 'tool-arrowbawah')
										->orderBy('tanggal')
										->get(),
						'tool-arrowlurus' => DB::table('TFG.kegiatan')->select('*')
										->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
										->where('type', 'tool-arrowlurus')
										->orderBy('tanggal')
										->get(),
						'formasi' => DB::table('TFG.kegiatan')->select('*')
										->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
										->where('type', 'formasi')
										->orderBy('tanggal')
										->get(),
						'text' => DB::table('TFG.kegiatan')->select('*')
										->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
										->where('type', 'text')
										->orderBy('tanggal')
										->get(),
						// 'weapon' => DB::table('TFG.kegiatan')->select('*',DB::raw("REPLACE (keterangan, '\"', '''') AS keterangan"))
						// 				->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
						// 				->where('type', 'weapon')
						// 				->orderBy('tanggal')
						// 				->get()
					]
				];
			}

			return response()->json($response);
		// } catch (Exception $th) {
		// 	//throw $th;
		// 	return response()->json(['error' => 'Server Error!, saat getObjectFromDocument.']);
		// }
	}

	public function getKogas(Request $r){
		$data = DB::table('bagian');
		if(isset($r->kogas) and $r->kogas!=null){
			$data = $data->where('ID',$r->kogas);
		}
		$data = $data->orderBy('ID','ASC')
			->get();
		return response()->json($data);
	}

	public function selectedKogas(){
		$skenario = $this->getSkenario()->ID;
		$data = DB::table("unit_pelatihan AS u")
			->join("bagian AS b", "u.divisi", "=", "b.ID")
			->select("b.*")
			->where("u.id_skenario", $skenario)
			->get();
		return response()->json($data);
	}

	public function getDDay(Request $r){
		$skenario = $this->getSkenario()->ID;
		$cb = DB::table('TFG.cb')
			->where('skenario',$skenario);
		if(isset($r->kogas) and $r->kogas!=null){
			$cb = $cb->where('kogas',$r->kogas);
		}
		if(isset($r->id_cb) and $r->id_cb!=null){
			$cb = $cb->where('id',$r->id_cb);
		}
		$cb = $cb->get();
		$tmp = [];
		foreach($cb as $a){
			$d_day = date("Y-m-d", strtotime($r->d_day));
			$kegiatan = DB::table('TFG.kegiatan')
				->where('id_cb',$a->id)
				->get();
			foreach($kegiatan as $k){
				$dateDifference = strtotime($k->tanggal) - strtotime($d_day);
				$tmp[] = round($dateDifference / (60*60*24) );
			}
		}
		$tmp[] = 0;
		$result = array_unique($tmp);
		sort($result);
		return response()->json($result);
	}
	public function getKegiatan(Request $r){ // r [ kogas(int), hari_h(date), id_cb(int) ]
		$skenario = $this->getSkenario()->ID;
		$cb = DB::table('TFG.cb')
			->where('skenario',$skenario);
		if(isset($r->kogas) and $r->kogas!=null){
			$cb = $cb->where('kogas',$r->kogas);
		}
		if(isset($r->id_cb) and $r->id_cb!=null){
			$cb = $cb->where('id',$r->id_cb);
		}
		$cb = $cb->get();
		$result = [];
		foreach($cb as $a){
			$d_day = date("Y-m-d", strtotime($r->d_day));
			$kegiatan = DB::table('TFG.kegiatan')
				->where('id_cb',$a->id)
				->where('type', 'kegiatan');
			if(isset($r->kogas_keg) and $r->kogas_keg!=null){
				$kegiatan = $kegiatan->where('kogas_keg',$r->kogas_keg);
			}
			if(isset($r->waktu) && !empty($r->waktu)){
				$kegiatan = $kegiatan->where('tanggal', '<=', $r->waktu)
									-> where('selesai', '>', $r->waktu);
			}
			$kegiatan = $kegiatan->get();

			// return response()->json($kegiatan);
			foreach($kegiatan as $k){
				$dateDifference = strtotime($k->tanggal) - strtotime($d_day);
				$dateDifference = round($dateDifference / (60*60*24) );
				if(isset($r->id_cb) and $r->id_cb!=null){
					if($dateDifference == $r->hari_h){
						$result[] = $k;
					}
				}else{
					$result[] = $k;
				}
			}
		}
		return response()->json($result);
	}

	public function getTimeTable(Request $r){ // r [ kogas(int) ]
		// return response()->json();
		$skenario = $this->getSkenario()->ID;
		$cb = DB::table('TFG.cb as c')
			->leftJoin('TFG.kegiatan as k', 'k.id_cb', '=', 'c.id')
			->where('c.skenario',$skenario)
			// ->whereIn('id',json_decode($r->id_cb));
			->where('k.type', 'kegiatan')
			->select('id_cb');

		if(isset($r->kogas) and $r->kogas!=null){
			$cb = $cb->where('kogas',$r->kogas);
		}		

		$grup_by = $cb->groupBy('id_cb')->get();
		$id_cbs = [];

		foreach ($grup_by as $key => $value) {
			# code...
			array_push($id_cbs, $value->id_cb);
		}
		
		$cb_data = DB::table('TFG.cb as c')->whereIn('c.id', $id_cbs);
		$cb_data = $cb_data->get();
		$id_cbs = [];

		$data = [];
		$amount_data = 0;
		$filter_dday = [];
		$tmp_keg = [];
		foreach($cb_data as $a){
			$d_day = date("Y-m-d", strtotime($r->hari_h));
			// $kegiatan = DB::table('TFG.kegiatan AS k')
			$kegiatan = DB::table('TFG.kegiatan AS k')->leftJoin('TFG.obj_kegiatan AS ok', 'k.id_obj_kegiatan', '=', 'ok.id')->selectRaw('k.*, ok.nama AS nama_object')
				->where('id_cb',$a->id)
				// ->whereIn('type', ['kegiatan','embarkasi','debarkasi'])
				->where('type', 'kegiatan')
				->get();
			foreach($kegiatan as $k){
				$k_day = date("Y-m-d", strtotime($k->tanggal));
				$dateDifference = strtotime($k_day) - strtotime($d_day);
				$dateDifference = (string)round($dateDifference / (60*60*24) );
				if(
					($r->d_day!=null and $r->d_day==$dateDifference)
					or ($r->d_day==null)
					){
						if(!in_array($dateDifference,$filter_dday)){
							$data[] = (object) array(
								"d_day" => $dateDifference,
								"date" => $k_day
							);
							$tmp_keg[$dateDifference] = [];
							$filter_dday[] = $dateDifference;
							$amount_data += 1;
						}
					}
				$tmp_keg[$dateDifference][] = (object) array(
					"detail" => $k,
					"name" => $k->nama,
					"desc" => $k->keterangan,
					"hour" => date("G", strtotime($k->tanggal)),
					"kogas" => DB::table('bagian')->where('ID', $k->kogas_keg)->first()->nama_bagian
				);
			}
		}
		$tmp_jam = [];
		for($a=1; $a<=24; $a++){
			$tmp_jam[] = [];
		}
		for($a=0; $a<$amount_data; $a++){
			$d_day = $data[$a]->d_day;
			$data_keg = $tmp_keg[$d_day];
			$kegiatan = $tmp_jam;
			foreach($data_keg as $keg){
				$index = $keg->hour;
				$kegiatan[$index][] = $keg;
			}
			$data[$a]->kegiatan = $kegiatan;
		}
		return response()->json($data);
	}
	private function getLastID($table, $primary){
		$data = DB::table($table)
			->max($primary);
		return $data;
	}
	private function getData($table, $col, $val){
		$data = DB::table($table);
		for($a=0; $a<count($col); $a++){
			$data = $data->where($col[$a],$val[$a]);
		}
		$data = $data->get();
		return $data;
	}

	public function getMaxKecepatan(Request $r){
		$query = DB::table('public.p_data_satuan')->where('nama', $r->nama)->first()->info;
		$c = json_decode($query, true);
		$data = 0;
		if(isset($c['kecepatan_maks'])){
			$data = $c['kecepatan_maks'];
		}
		
		return $data;
	}

	public function getIdDokumen($user_id)
	{
		# code...
		$s = $this->getSkenario()->ID;

		$mu = DB::table('public.master_users')->where('id', $user_id)->first();
		$us = DB::table('public.master_users')->where(['bagian' => $mu->bagian, 'jabatan' => 1])->first();
		$mct = DB::table('master_cb_terbaik')->where('skenario', $s)->where('id_user', $us->id)->first();
		$mc = DB::table('public.master_cb')->where('ID', $mct->cb)->first();
		$pd = DB::table('public.p_dokumen')->where('nama_dokumen', $mc->judul_cb)->where('scenario', $s)->where('id_user', $mc->create_by)->where('type_login', 'menu_cb')->first();

		return $pd->id;
	}

	public function insertCB(Request $r){
		$id = $this->getLastID('TFG.cb','id') + 1;
		DB::table('TFG.cb')->insert([
			'id' => $id,
			'nama' => $r->nama,
			'skenario' => $r->skenario,
			'kogas' => $r->kogas,
			'hari_h' => $r->hari_h
		]);
		$data = $this->getData('TFG.cb',['id'],[$id])[0]->id;
		return response()->json($data);
	}

	public function insertKekuatan(Request $r)
	{
		# code...
		return response()->json($r);

/* ------------------------- INISIAL VARIABLE START ------------------------- */

		$digits = 2;
		$random_number = rand(pow(10, $digits-1), pow(10, $digits)-1);
		$random_number2 = rand(pow(10, $digits-1), pow(10, $digits)-1);

		$mi = $this->getLastID('public.p_data_kekuatan', 'id') + 1;
		$ui = $r->user_id;
		$idok = $this->getIdDokumen($ui);
		$nama = "kekuatan_".$random_number."_".$random_number2;
		$lat_y = $r->lat_y;
		$lng_x = $r->lng_x;
		$info_kekuatan = new \stdClass();
		$rincian = new \stdClass();


		
/* -------------------------- INISIAL VARIABLE END -------------------------- */

		// $insert = DB::table('public.p_data_kekuatan')->insert([
		// 	'id' => $mi,
		// 	'id_user' => $ui,
		// 	'dokumen' => $idok,
		// 	'nama' => $nama,
		// 	'lat_y' => $lat_y,
		// 	'lng_x' => $lng_x,
		// 	'info_kekuatan' => ,
		// 	'rincian' => ,
		// 	'size' => 20
		// ]);
	}

	public function insertbungus(Request $r)
	{
		//23/03/2021 20:53:09
		$tanggal = $r->tanggal;
		$ex = explode(' ', $tanggal);
		$tg = explode('/', $ex[0]);
		$wak = explode(':', $ex[1]);
		$waktu = $wak[0].':'.$wak[1];

		$tgl = [ $tg[0].'-'.$tg[1].'-'.$tg[2],  $waktu ];

		$digits = 2;
		$random_number = rand(pow(10, $digits-1), pow(10, $digits)-1);
		$random_number2 = rand(pow(10, $digits-1), pow(10, $digits)-1);

		$ui = $r->id_user;
		$id_dokumen = $this->getIdDokumen($ui);
		$nama = "bungus_".$random_number."_".$random_number2;

		$info_bungus = new \stdClass();
		$info_bungus->date = $tgl;
		$info_bungus->ukuran = 31;
		$jsonsbungus = json_encode($info_bungus);
		
		$simbol = new \stdClass();
		$simbol->iconUrl = "image/plotting/bungus_red.png";
		$simbol->iconSize = [31,31];
		$simbol->id_point = $nama;
		$simbol->waktu = $tgl;
		$jsonsimbol = json_encode($simbol);

		$insert = DB::table('public.p_data_bungus')->insert([
			'id_user' => $ui,
			'dokumen' => $id_dokumen,
			'nama' => $nama,
			'lat_y' => isset($r->lat_y) ? $r->lat_y : "",
			'lng_x' => isset($r->lng_x) ? $r->lng_x : "",
			'info_bungus' => $jsonsbungus,
			'symbol' => $jsonsimbol
		]);

		return response()->json($insert);

	}

	public function insertObstacle(Request $r)
	{
		$digits = 2;
		$random_number = rand(pow(10, $digits-1), pow(10, $digits)-1);
		$random_number2 = rand(pow(10, $digits-1), pow(10, $digits)-1);

		$ui = $r->id_user;
		$id_dokumen = $this->getIdDokumen($ui);
		$nama = "obstacle_".$random_number."_".$random_number2;
		$info_obstacle = [$nama, $r->indexObstacle, $r->color, 40 , $r->FontObstacle];

		$simbol = new \stdClass();
		$simbol->className = "my-div-icon";
		$simbol->html = "<div style='margin-top:-15px; width: 40px;color:".$r->color."; font-size: 40px; font-family: ".$r->FontObstacle."'>".$r->indexObstacle."</div>";
		$simbol->iconSize = null;
		$simbol->id_point = $nama;
		$jsonsimbol = json_encode($simbol);


		$insert = DB::table('public.p_data_obstacle')->insert([
			'id_user' => $ui,
			'dokumen' => $id_dokumen,
			'nama' => $nama,
			'lng_x' => isset($r->lng_x) ? $r->lng_x : "",
			'lat_y' => isset($r->lat_y) ? $r->lat_y : "",
			'info_obstacle' => json_encode($info_obstacle),
			'symbol' => $jsonsimbol
		]);

		return response()->json($insert);

	}
	
	
	public function insertSituasi(Request $r)
	{
		$digits = 2;
		$random_number = rand(pow(10, $digits-1), pow(10, $digits)-1);
		$random_number2 = rand(pow(10, $digits-1), pow(10, $digits)-1);

		$ui = $r->id_user;
		$id_dokumen = $this->getIdDokumen($ui);
		$nama = "situasi_".$random_number."_".$random_number2;

		//24/03/2021 18:13:57
		$tanggal = $r->tanggal;
		$ex = explode(' ', $tanggal);
		$tg = explode('/', $ex[0]);
		$tgl = $tg[0].$tg[1].$tg[2];

		$wak = explode(':', $ex[1]);
		$waktu = $wak[0].$wak[1];

		$warna ='blue';
		$img = "image/plotting/warning_blue.png";
		if($r->warna == 'Biru'){
			$warna = 'blue';
			$img = "image/plotting/warning_blue.png";
		}else{
			$warna = 'red';
			$img = "image/plotting/warning_red.png";
		}

		$info_situasi = new \stdClass();
		$info_situasi->isi_situasi = $r->keterangan;
		$info_situasi->tgl_situasi = $tgl;
		$info_situasi->waktu_situasi = $waktu;
		$info_situasi->size = 20;
		$info_situasi->warna = $warna;
		$info_situasi->property = "{\"distance_\":20,\"degree_\":45,\"width_\":0,\"height_\":100}";
		$jsoninfoSituasi = json_encode($info_situasi);

		$simbol = new \stdClass();
		$simbol->iconUrl = $img;
		$simbol->iconSize = [20,20];
		$simbol->id_point = $nama;
		$jsonsimbol = json_encode($simbol);


		$insert = DB::table('public.p_data_situasi')->insert([
			'id_user' => $ui,
			'dokumen' => $id_dokumen,
			'nama' => $nama,
			'lat_y' => isset($r->lat_y) ? $r->lat_y : "",
			'lng_x' => isset($r->lng_x) ? $r->lng_x : "",
			'info_situasi' => $jsoninfoSituasi,
			'symbol_situasi' => $jsonsimbol
		]);

		return response()->json($insert);

	}

	public function insertSroute(Request $r)
	{
		$digits = 2;
		$random_number = rand(pow(10, $digits-1), pow(10, $digits)-1);
		$random_number2 = rand(pow(10, $digits-1), pow(10, $digits)-1);

		$ui = $r->id_user;
		$id_dokumen = $this->getIdDokumen($ui);
		$nama = "standard_route_".$random_number."_".$random_number2;

		$geometry = new \stdClass();
		$geometry->type = "LineString";
		$geometry->coordinates = json_decode($r->coord);
		$jsonGeometry = json_encode($geometry);




		$properties = new \stdClass();
		$label_distance = new \stdClass();

		


		$insert = DB::table('public.p_data_standard_route')->insert([
			'id_user' => $ui,
			'dokumen' => $id_dokumen,
			'nama' => $nama,
			'geometry' => $geometry,
			'properties' => $properties,
			'status' => 0,
			'label_distance' => $label_distance
		]);

		return response()->json($insert);

	}


	public function insertTool2D(Request $r)
	{
		# code...
		// return response()->json($r);
		
		$digits = 2;
		$random_number = rand(pow(10, $digits-1), pow(10, $digits)-1);
		$random_number2 = rand(pow(10, $digits-1), pow(10, $digits)-1);

		$ui = $r->id_user;
		$id_dokumen = $this->getIdDokumen($ui);
		$nama = null;
		$geometry = new \stdClass();
		$properties = new \stdClass();
		$type = null;
		
		switch ($r->type) {
			case 'LineString':
				# code...
				$nama = "polyline_".$random_number."_".$random_number2;

				$geometry->type = $r->type;
				$geometry->coordinates = json_decode($r->coord);
				$jsonGeometry = json_encode($geometry);

				$properties->stroke = true;
				$properties->color = $r->color;
				$properties->weight = 5;
				$properties->opacity = 0.7;
				$properties->fill = false;
				$properties->clickable = true;
				$properties->dashArray = "0,0";
				$properties->lineJoin = "round";
				$properties->id_point = $nama;
				$jsonProperties = json_encode($properties);

				$type = "LineString";
				break;

			case 'Polygon':
				# code...
				$nama = "polygon_".$random_number."_".$random_number2;

				$geometry->type = $r->type;
				$geometry->coordinates = [json_decode($r->coord)];
				$jsonGeometry = json_encode($geometry);

				$properties->stroke = true;
				$properties->color = $r->color;
				$properties->weight = 5;
				$properties->opacity = 0.5;
				$properties->fill = true;
				$properties->fillColor = $r->color;
				$properties->fillOpacity = 0.2;
				$properties->clickable = true;
				$properties->dashArray = "0,0";
				$jsonProperties = json_encode($properties);

				$type = "Polygon";
				break;

			case 'Circle':
				# code...
				$nama = "circle_".$random_number."_".$random_number2;

				$geometry->type = "Point";
				$geometry->coordinates = json_decode($r->coord);
				$jsonGeometry = json_encode($geometry);

				$properties->stroke = true;
				$properties->color = $r->color;
				$properties->weight = 5;
				$properties->opacity = 0.5;
				$properties->fill = true;
				$properties->fillColor = $r->color;
				$properties->fillOpacity = 0.2;
				$properties->clickable = true;
				$properties->dashArray = "0,0";
				$properties->radius = (double)$r->radius;
				$jsonProperties = json_encode($properties);

				$type = "circle";
				break;

			case 'ArrowStraight':
				# code...
				$nama = "arrowlurus_".$random_number."_".$random_number2;

				$geometry->type = "Polygon";
				$geometry->coordinates = [json_decode($r->coord)];
				$jsonGeometry = json_encode($geometry);

				$properties->stroke = true;
				$properties->color = $r->color;
				$properties->weight = 5;
				$properties->opacity = 0.5;
				$properties->fill = true;
				$properties->fillColor = $r->color;
				$properties->fillOpacity = 0.2;
				$properties->clickable = true;
				$jsonProperties = json_encode($properties);

				$type = "Polygon";
				break;

			case 'ArrowDown':
				# code...
				$nama = "arrowbawah_".$random_number."_".$random_number2;

				$geometry->type = "Polygon";
				$geometry->coordinates = [json_decode($r->coord)];
				$jsonGeometry = json_encode($geometry);

				$properties->stroke = true;
				$properties->color = $r->color;
				$properties->weight = 5;
				$properties->opacity = 0.5;
				$properties->fill = true;
				$properties->fillColor = $r->color;
				$properties->fillOpacity = 0.2;
				$properties->clickable = true;
				$jsonProperties = json_encode($properties);

				$type = "Polygon";
				break;
			
			case 'ArrowUp':
				# code...
				$nama = "arrowatas_".$random_number."_".$random_number2;

				$geometry->type = "Polygon";
				$geometry->coordinates = [json_decode($r->coord)];
				$jsonGeometry = json_encode($geometry);

				$properties->stroke = true;
				$properties->color = $r->color;
				$properties->weight = 5;
				$properties->opacity = 0.5;
				$properties->fill = true;
				$properties->fillColor = $r->color;
				$properties->fillOpacity = 0.2;
				$properties->clickable = true;
				$jsonProperties = json_encode($properties);

				$type = "Polygon";
				break;
			default:
				# code...
				break;
		}

		$insert = DB::table('public.p_data_tool')->insert([
			'id_user' => $ui,
			'dokumen' => $id_dokumen,
			'nama' => $nama,
			'geometry' => $jsonGeometry,
			'properties' => $jsonProperties,
			'type' => $type
		]);

		return response()->json($insert);
	}

	public function insertKegiatan(Request $r){
		// return response()->json($r);

		$urlvideo = null;
		if ($r->hasFile('file')) {
			$path = "\assets\\video\\kegiatan_tfg";
			$filename =  $r->file('file');
			$filename->move(public_path($path), $filename->getClientOriginalName());

			$sPath = str_replace("\\", "/", $path);
			$urlvideo = $sPath."/".$filename->getClientOriginalName();
		}

		date_default_timezone_set('Asia/Jakarta');
		$eDT = explode(" ", $r->tanggal);
		$D = $eDT[0];
		$T = $eDT[1];

		$eD = explode("-", $D);
		$t = $eD[0];
		$b = $eD[1];
		$ta = $eD[2];
		
		$date = $t."-".$b."-".$ta." ".$T;
		$tanggal = date('Y-m-d H:i:s', strtotime($date));

		$digits = 3;
		$random_number = rand(pow(10, $digits-1), pow(10, $digits)-1);
		$id_kegiatan = "kegiatan_".$t."_".$random_number;

		$id = $this->getLastID('TFG.kegiatan','id') + 1;
		DB::table('TFG.kegiatan')->insert([
			'id' => $id,
			'id_cb' => $r->id_cb,
			'nama' => $r->nama,
			'keterangan' => $r->keterangan,
			'tanggal' => $tanggal,
			'type' => $r->type,
			'image' => $r->image,
			'kogas_keg' => $r->kogas_keg,
			// 'id_obj_kegiatan' => $r->id_obj_kegiatan,
			// 'long_x' => $r->long_x,
			// 'lat_y' => $r->lat_y,
			'id_obj_kegiatan' => isset($r->id) ? $r->id : 0,
			'long_x' => isset($r->long_x) ? $r->long_x : "",
			'lat_y' => isset($r->lat_y) ? $r->lat_y : "",
			'type_kegiatan' => isset($r->type_kegiatan) ? $r->type_kegiatan : "",
			'url_video' => $urlvideo,
			'id_kegiatan' => $id_kegiatan
		]);

		
		$id_dokumen = $this->getIdDokumen($r->id_user);
		$waktu = date('m-d-Y H:i', strtotime($date));
		$skenario = $this->getSkenario()->ID;

		DB::table('public.p_master_kegiatan')->insert([
			'id_kegiatan' => $id_kegiatan,
			// 'id_dokumen' => $id_dokumen,
			'kegiatan' => $r->nama,
			'x' => "",
			'y' => "",
			'waktu' => $waktu,
			'keterangan' => $r->keterangan,
			'id_user' => $r->id_user,
			'id_satuan' => "",
			'waypoint' => "",
			'id_type_kegiatan' => isset($r->type_kegiatan) ? $r->type_kegiatan : null,
			'url_video' => $urlvideo,
			'id_scenario' => $skenario
		]);
		
		$data = $this->getData('TFG.kegiatan',['id_cb'],[$r->id_cb]);
		return response()->json($data);
	}

	public function insertObjectKegiatan(Request $r){
		$id = $this->getLastID('TFG.obj_kegiatan','id') + 1;
		DB::table('TFG.obj_kegiatan')->insert([
			'id' => $id,
			'id_obj' => $r->id_obj,
			//'id_kegiatan' => $r->id_kegiatan,
			'kecepatan' => $r->kecepatan,
			'satuan_kecepatan' => $r->satuan_kecepatan,
			'berangkat' => $r->berangkat,
			'sampai' => $r->sampai,
			'nama' => $r->nama,
			'color' => $r->color,
			'size' => $r->size,
			'long_x' => $r->long_x,
			'lat_y' => $r->lat_y,
		]);
		$data = $this->getData('TFG.obj_kegiatan',['id'],[$id]);
		return response()->json($data);
	}

	public function deleteObjectKegiatan(Request $r){
		DB::table('TFG.obj_kegiatan')
			->where('id',$r->id)
			->where('id_kegiatan',$r->id_kegiatan)
			->delete();
		$data = $this->getData('TFG.obj_kegiatan',['id_kegiatan'],[$r->id_kegiatan]);
		return response()->json($data);
	}

	public function deleteKegiatan(Request $r){
		DB::table('TFG.obj_kegiatan')
			->where('id_kegiatan',$r->id)
			->delete();
		DB::table('TFG.kegiatan')
			->where('id',$r->id)
			->where('id_cb',$r->id_cb)
			->delete();
		$data = $this->getData('TFG.kegiatan',['id_cb'],[$r->id_cb]);
		return response()->json($data);
	}

	public function updateObjectkegiatan(Request $r){
		DB::table('TFG.obj_kegiatan')
            ->where('id',$r->id)
            ->update([
				'kecepatan' => $r->kecepatan,
				'satuan_kecepatan' => $r->satuan_kecepatan,
				'berangkat' => $r->tanggal,
				'sampai' => $r->sampai,
				'nama' => $r->nama,
				'color' => $r->color,
				'size' => $r->size,
			]);
		$data = $this->getData('TFG.obj_kegiatan',['id'],[$r->id]);
		return response()->json($data);
	}

	public function updateKegiatanDate(Request $r){
		$id = DB::table('TFG.kegiatan')->where('id_obj_kegiatan',$r->id)->first();

		DB::table('TFG.kegiatan')
			->where('id',$id->id)
			->where('type', 'kegiatan')
            ->update([
				'tanggal' => $r->tanggal,
				'selesai' => $r->selesai
			]);
		$data = $this->getData('TFG.kegiatan',['id'],[$r->id]);
		return response()->json($data);
	}

	public function updatekegiatan(Request $r){
		$urlvideo = null;
		if ($r->hasFile('file')) {
			$path = "\assets\\video\\kegiatan_tfg";
			$filename =  $r->file('file');
			$filename->move(public_path($path), $filename->getClientOriginalName());

			$sPath = str_replace("\\", "/", $path);
			$urlvideo = $sPath."/".$filename->getClientOriginalName();
		}
		
		DB::table('TFG.kegiatan')->where('id',$r->id)->where('type', 'kegiatan')->update([
			'nama' => $r->nama,
			'keterangan' => $r->keterangan,
			'tanggal' => $r->tanggal,
			'type' => $r->type,
			'image' => $r->image,
			'type_kegiatan' => isset($r->type_kegiatan) ? $r->type_kegiatan : "",
			'url_video' => $urlvideo
		]);

		DB::table('public.p_master_kegiatan')->where('id_kegiatan', $r->id_kegiatan)->update([
			'kegiatan' => $r->nama,
			'keterangan' => $r->keterangan,
			'waktu' => date('m-d-Y H:i', strtotime($r->tanggal)),
			'url_video' => $urlvideo
		]);

		$data = $this->getData('TFG.kegiatan',['id'],[$r->id]);
		return response()->json($data);
	}

	public function updateCB(Request $r){
		DB::table('TFG.cb')
            ->where('id',$r->id)
            ->update([
				'nama' => $r->nama,
				'skenario' => $r->skenario,
				'kogas' => $r->kogas,
				'hari_h' => $r->hari_h
			]);
		$data = $this->getData('TFG.cb',['id'],[$r->id]);
		return response()->json($data);
	}

	public function getCBTerbaik2(Request $r)
	{
		$skenario = $this->getSkenario()->ID;
		
		$dataString = DB::table("master_cb_terbaik as b")
					->join("master_cb as cb", "cb.ID", "=", "b.cb")
					->select("b.*","cb.judul_cb","cb.create_by","cb.ID")
					->where("b.skenario",$skenario);
		if(isset($r->kogas) and $r->kogas!=null){
			$dataString = $dataString->where("b.kogas", $r->kogas);
		}
		$dataString = $dataString->get();

		return response()->json($dataString);
		
	}

	public function getIDCbEppkm(Request $r){
		$skenario = $this->getSkenario()->ID;
		$data = DB::table('public.master_cb as a')
					->join('TFG.cb as b', 'a.ID', 'b.id_cb_eppkm')
					->where('a.create_by',$r->id_user)
					->where('b.skenario',$skenario)
					->select('b.id')
					->first();
		$id_data=0;
		if($data){
			$id_data = $data->id;
		}
					
		return response()->json($id_data);

	}

	public function getHariHSkenario(Request $r){
		$skenario = $this->getSkenario()->ID;
		$data = DB::table('public.master_skenario')
				->where('ID',$skenario)
				->where('status', 1)
				->select('harih')
				->first();
		$hari = null;
		if($data){
			$hari = $data->harih;	
		}

		return response()->json($hari);
	}

	public function getCB(Request $r){
		$skenario = $this->getSkenario()->ID;
		// $data = DB::table('TFG.cb AS cb');
		// $data = $data->where('skenario',$skenario);
		// if(isset($r->kogas) and $r->kogas!=null){
		// 	$data = $data->where('kogas',$r->kogas);
		// }
		// if(isset($r->hari_h) and $r->hari_h!=null){
		// 	$data = $data->where('hari_h',$r->hari_h);
		// }
		// $data = $data->get();

		if (isset($r->id_user)) {
			# code...
			$dataString = "SELECT cb.*, mc.create_by FROM \"TFG\".cb AS cb";
			$dataString .= " LEFT JOIN public.master_cb AS mc ON cb.id_cb_eppkm = mc.\"ID\" WHERE mc.create_by = ".$r->id_user."";
			$dataString .= " AND cb.skenario = ".$skenario."";
			if(isset($r->kogas) and $r->kogas!=null){
				$dataString .= " AND cb.kogas = ".$r->kogas."";
			}
			if(isset($r->hari_h) and $r->hari_h!=null){
				$dataString .= " AND cb.hari_h = ".$r->hari_h."";
			}	
		} else {
			$dataString = "SELECT * FROM \"TFG\".cb AS cb";
			$dataString .= " AND cb.skenario = ".$skenario."";
			if(isset($r->kogas) and $r->kogas!=null){
				$dataString .= " AND kogas = ".$r->kogas."";
			}
			if(isset($r->hari_h) and $r->hari_h!=null){
				$dataString .= " AND hari_h = ".$r->kogas."";
			}	
		}

		$data = DB::select($dataString);

		return response()->json($data);
	}

	public function createNewDocument(Request $r)
	{
		$id = $this->getLastID('TFG.cb','id') + 1;
		DB::table('TFG.cb')->insert([
			'id' => $id,
			'nama' => $r->nama,
			'skenario' => $r->skenario,
			'kogas' => $r->kogas,
			'hari_h' => $r->hari_h
		]);
		$data = $this->getData('TFG.cb',['id'],[$id])[0]->id;
		return response()->json($data);
	}

	public function createObjectKegiatan2D(Request $r)
	{
		$id = $this->getLastID('TFG.obj_kegiatan','id') + 1;
		DB::table('TFG.obj_kegiatan')->insert([
			'id' => $id,
			'id_obj' => $r->id_obj,
			'kecepatan' => $r->kecepatan,
			'satuan_kecepatan' => $r->satuan_kecepatan,
			'berangkat' => $r->berangkat,
			'sampai' => $r->sampai,
			'nama' => $r->nama,
			'color' => $r->color,
			'size' => $r->size,
			'long_x' => $r->long_x,
			'lat_y' => $r->lat_y,
		]);
		$data = $this->getData('TFG.obj_kegiatan',['id'],[$id]);
		return response()->json($data);
	}

	public function createKegiatan2D(Request $r)
	{
		$id = $this->getLastID('TFG.kegiatan','id') + 1;
		DB::table('TFG.kegiatan')->insert([
			'id' => $id,
			'id_cb' => $r->id_cb,
			'nama' => $r->nama,
			'keterangan' => $r->keterangan,
			'tanggal' => $r->tanggal,
			'type' => $r->type,
			'image' => $r->image,
			'kogas_keg' => $r->kogas_keg,
			'id_obj_kegiatan' => $r->id_obj_kegiatan,
			'long_x' => $r->long_x,
			'lat_y' => $r->lat_y
		]);
		$data = $this->getData('TFG.kegiatan',['id_cb'],[$r->id_cb]);
		return response()->json($data);
	}

	public function getCB2D(Request $r)
	{
		$skenario = $this->getSkenario()->ID;
		$data = DB::table('p_dokumen')->where('scenario',$skenario)->get();

		$res = [];
		foreach($data as $item)
		{
			$res[] = [
				'document' => $item,
				'user' => DB::table('master_users')->where('id',$item->id_user)->first()
			];
		}

		if(isset($r->kogas) and $r->kogas!=null){
			foreach($res as $item)
			{
				if($item['user']->bagian == $r->kogas)
				{
					$result[] = $item;
				}
			}
		}else{
			$result = $res;
		}

		return response()->json($result);
	}

	public function truncateTable() {
		$cb = DB::table('TFG.cb')->truncate();
		$kegiatan = DB::table('TFG.kegiatan')->truncate();
		$obj_kegiatan = DB::table('TFG.obj_kegiatan')->truncate();

		return "true";
	}

	public function truncateTableRev(Request $r) {
		$kogas = $r->kogas;
		$skenario = $this->getSkenario()->ID;

		$cb = DB::table('TFG.cb')->where(['skenario' => $skenario, 'kogas' => $kogas]);

		$cb_delete = false;
		$kegiatan_delete = false;
		$obj_kegiatan_delete = false;

		if ($cb->exists()) {
			# code...
			$cb_get = $cb->get();
			$cb_count = count($cb_get);

			if ($cb_count > 0) {
				# code...
				$id_cb = [];

				for ($i=0; $i < $cb_count; $i++) { 
					# code...
					if (!in_array($cb_get[$i]->id, $id_cb)) {
						# code...
						array_push($id_cb, $cb_get[$i]->id);
					}
				}

				$kegiatan = DB::table('TFG.kegiatan')->whereIn('id_cb', $id_cb);

				if ($kegiatan->exists()) {
					# code...
					$kegiatan_get = $kegiatan->get();
					$kegiatan_count = count($kegiatan_get);

					if ($kegiatan_count > 0) {
						# code...
						$id_obj_kegiatan = [];

						for ($j=0; $j < $kegiatan_count; $j++) { 
							# code...
							if (!in_array($kegiatan_get[$j]->id_obj_kegiatan, $id_obj_kegiatan)) {
								# code...
								array_push($id_obj_kegiatan, $kegiatan_get[$j]->id_obj_kegiatan); 
							}
						}

						$obj_kegiatan_delete = DB::table('TFG.obj_kegiatan')->whereIn('id', $id_obj_kegiatan)->delete();
						$kegiatan_delete = $kegiatan->delete();
						$cb_delete = $cb->delete();
					
						$id_cb = [];
						$id_obj_kegiatan = [];
					}
				}
			}
		}

		// dd($kegiatan_delete);

		return "true";
	}

	public function loadOrInsertCB(Request $r)
	{
		// try {
			//code...
			$cb = DB::table('TFG.cb as c');
			$data = $cb->where([
				'c.id' => $r->id_cb_tfg,
				'c.nama' => $r->nama,
				'c.id_cb_eppkm' => $r->id_cb_eppkm,
				'c.skenario' => $r->skenario,
				'c.kogas' => $r->kogas,
				'id_user' => $r->id_user,
			]);

			$res = "";

			if($data->exists()){
				$res = $data->get();
				$id = $res[0]->id;

				$kegiatan = DB::table('TFG.kegiatan')->where('id_cb', $id)->where('id_obj_kegiatan', '!=', 0)->whereNotNull('id_obj_kegiatan');

				$item = $kegiatan->select('id_obj_kegiatan')->groupBy('id_obj_kegiatan')->get();

				if($item != null){
					foreach($item as $obj_keg)
					{
						// DB::table('TFG.obj_kegiatan')->where('id',$obj_keg->id_obj_kegiatan)->delete();
					}
				}

				// $kegiatan->delete();
			// } else {
			// 	$res = [[
			// 		'id' => $r->id_cb_tfg,
			// 		'nama' => $r->nama,
			// 		'id_cb_eppkm' => $r->id_cb_eppkm,
			// 		'skenario' => $r->skenario,
			// 		'kogas' => $r->kogas,
			// 		'id_user' => $r->id_user,
			// 	]];
			}
			else{
				$date = date('Y-m-d');

				if(isset($r->hari_h) and $r->hari_h!=null){
					$date = $r->hari_h;
				}

				// $id = $this->getLastID('TFG.cb','id') + 1;
				$id = $r->id_cb_tfg;
				DB::table('TFG.cb')->insert([
					'id' => $id,
					'nama' => $r->nama,
					'skenario' => $r->skenario,
					'id_cb_eppkm' => $r->id_cb_eppkm,
					'id_user' => $r->id_user,
					'kogas' => $r->kogas,
					'hari_h' => $date
				]);

				// $id = $r->id_cb_tfg;
				// DB::table('TFG.cb')->updateOrInsert(
				// 	['id' => $id],
				// 	[
				// 		'id' => $id,
				// 		'nama' => $r->nama,
				// 		'skenario' => $r->skenario,
				// 		'id_cb_eppkm' => $r->id_cb_eppkm,
				// 		'kogas' => $r->kogas,
				// 		'hari_h' => $date
				// 	]
				// );
			}

			$res = $this->getData('TFG.cb',['id'],[$id]);

			return response()->json($res);
		// } catch (Exception $th) {
		// 	//throw $th;
		// 	return response()->json(['error' => 'Server Error!, saat loadOrInsertCB.']);
		// }
	}

	private function getObjectRanpur($id_symbol, $tipe_tni)
	{
		// $data = DB::table('object_alutsista_3d')
		// 			->where([
		// 				'id_font_taktis' => $id_font_taktis,
		// 				'tipe_tni' => $tipe_tni
		// 			]);

		$data = "";

		switch($tipe_tni)
		{
			case 1:
				$data = DB::table('M_VEHICLE')->where(['VEHICLE_ID' => $id_symbol])->first();
				break;
			case 2:
				$data = DB::table('M_SHIP')->where(['SHIP_ID' => $id_symbol])->first();
				break;
			case 3:
				$data = DB::table('M_AIRCRAFT')->where(['AIRCRAFT_ID' => $id_symbol])->first();
				break;
			default:
				break;
		}

		$res = "";

		if(!empty($data->id_object_alutsista)){
			$res = $data;
		}else{
			switch($tipe_tni)
			{
				case 1:
					$res = DB::table('object_alutsista_3d')
							->where([
								'tipe_tni' => $tipe_tni,
								'id_object_alutsista' => '754',
								// 'id_jenis_object' => 2
							// ])->inRandomOrder()->first();
							])->first();
					break;
				case 2:
					$res = DB::table('object_alutsista_3d')
							->where([
								'tipe_tni' => $tipe_tni,
								'id_jenis_object' => 4
							// ])->inRandomOrder()->first();
							])->first();
					break;
				case 3:
					$res = DB::table('object_alutsista_3d')
							->where([
								'tipe_tni' => $tipe_tni,
								'id_jenis_object' => 8
							// ])->inRandomOrder()->first();
							])->first();
					break;
				default:
					break;
			}
		}

		return $res;
	}

	public function InsertKegiatanNew(Request $r){
		// try {
			//code...
			$kegiatan = json_decode($r->kegiatann);

			// dd($kegiatan);

			// return response()->json($kegiatan);

			$res = [];
			if(is_array($kegiatan))
			{
				if(count($kegiatan) > 0){
					foreach ($kegiatan as $dokumen)
					{
						$obj_kegiatan = DB::table('TFG.obj_kegiatan')->where('nama', $dokumen->nama_object)->first();

						$id = $this->getLastID('TFG.kegiatan','id') + 1;
					
						date_default_timezone_set('Asia/Jakarta');
						$eDT = explode(" ", $dokumen->waktu);
						$D = $eDT[0];
						$T = $eDT[1];

						$eD = explode("-", $D);
						$t = $eD[1];
						$b = $eD[0];
						$ta = $eD[2];
						
						$date = $t."-".$b."-".$ta." ".$T;
						$tanggal = date('Y-m-d H:i:s', strtotime($date));

						DB::table('TFG.kegiatan')->where('id_kegiatan', $dokumen->id_kegiatan)->delete();

						$id_kogas = DB::table('public.bagian')->where('nama_bagian', $dokumen->kogas)->first();

						$res[] = DB::table('TFG.kegiatan')->insert([
							'id' => $id,
							'id_cb' => $r->id_cb,
							'nama' => $dokumen->kegiatan,
							'keterangan' => $dokumen->keterangan,
							'tanggal' => $tanggal,
							'type' => 'kegiatan',
							'image' => '-',
							'kogas_keg' => $id_kogas->ID,
							// 'id_obj_kegiatan' => 0,
							// 'long_x' => $dokumen->x,
							// 'lat_y' => $dokumen->y
							'id_obj_kegiatan' => isset($obj_kegiatan->id) ? $obj_kegiatan->id : 0,
							'long_x' => isset($obj_kegiatan->long_x) ? $obj_kegiatan->long_x : $dokumen->x,
							'lat_y' => isset($obj_kegiatan->lat_y) ? $obj_kegiatan->lat_y : $dokumen->y,
							'type_kegiatan' => isset($dokumen->type_kegiatan) ? $dokumen->type_kegiatan : "",
							'url_video' => isset($dokumen->url_video) ? $dokumen->url_video : "",
							'id_kegiatan' => $dokumen->id_kegiatan
						]);
					}
				}
			}

			return response()->json($res);
		// } catch (Exception $th) {
		// 	//throw $th;
		// 	return response()->json(['error' => 'Server Error!, saat InsertKegiatanNew.']);
		// }
	}

	private function GetKecepatanMisi($id){
		$data = DB::table('p_data_misi')
			->where('id_object', $id)
			->first();

		// $kecepatan = "0|kilometer";
		$kecepatan = 0;
		if($data)
		{
			$pro = json_decode($data->properties);
			$kecepatan = $pro->kecepatan."|".$pro->type;
		}
		return $kecepatan;
	}


	public function loadFromEPPKM2D(Request $r)
	{
		// try {
			//code...
			$id_user = $r->id_user;

			$arraySatuan = json_decode($r->arraySatuan);
			$arrayKekuatan = json_decode($r->arrayKekuatan);
			$arraySituasi = json_decode($r->arraySituasi);
			// $arrayObstacle = json_decode($r->arrayObstacle);
			// $arrayManuver = json_decode($r->arrayManuver);
			$arraySRoute = json_decode($r->arraySRoute);
			$arrayBungus = json_decode($r->arrayBungus);
			$arrayPassen = json_decode($r->arrayPassen);
			$arrayRadar = json_decode($r->arrayRadar);
			$arrayLogis = json_decode($r->arrayLogis);
			$arrayFormasi = json_decode($r->arrayFormasi);
			$arrayTool = json_decode($r->arrayTool);
			$arrayPolyline = json_decode($r->arrayPolyline);
			$arrayText = json_decode($r->arrayText);
			$arrayDokumen = json_decode($r->arrayDokumen);

			if(is_array($arrayDokumen)){
				if(count($arrayDokumen) > 0){

				}
			}

			if (is_array($arraySituasi)) {
				if(count($arraySituasi) > 0){
					foreach ($arraySituasi as $situasi)
					{
						$info_situasi = json_decode($situasi->info_situasi);
						$symbol_situasi = json_decode($situasi->symbol_situasi);

						$id = $this->getLastID('TFG.kegiatan','id') + 1;

						$tgl = substr($info_situasi->tgl_situasi,0,2);
						$bln = substr($info_situasi->tgl_situasi,2,2);
						$thn = substr($info_situasi->tgl_situasi,4,4);

						$jam = substr($info_situasi->waktu_situasi,0,2);
						$menit = substr($info_situasi->waktu_situasi,2,2);


						$tanggal = date('Y-m-d H:i', strtotime($thn.'-'.$bln.'-'.$tgl.' '.$jam.':'.$menit));

						// return response()->json([
						// 	'tanggal' => $tgl,
						// 	'bulan' => $bln,
						// 	'tahun' => $thn,
						// 	'jam' => $jam,
						// 	'menit' => $menit
						// ]);

						if($symbol_situasi->iconUrl == "image/plotting/warning_red.png"){
							$warna = 'Merah';
						}else{
							$warna = 'Biru';
						}

						$id_obj = $this->getLastID('TFG.obj_kegiatan','id') + 1;
						DB::table('TFG.obj_kegiatan')->insert([
							'id' => $id_obj,
							'id_obj' => 90002,
							'kecepatan' => 0,
							'satuan_kecepatan' => "",
							'berangkat' => $tanggal,
							'sampai' => $tanggal,
							'nama' => $situasi->nama,
							'color' => $warna,
							'size' => 1,
							'lat_y' => $situasi->lat_y,
							'long_x' => $situasi->lng_x,
						]);

						$id = $this->getLastID('TFG.kegiatan','id') + 1;
						DB::table('TFG.kegiatan')->insert([
							'id' => $id,
							'id_cb' => $r->id_cb,
							'nama' => $situasi->nama,
							'keterangan' => $info_situasi->isi_situasi,
							'tanggal' => $tanggal,
							'type' => 'situasi',
							'image' => '-',
							'kogas_keg' => $r->kogas,
							'id_obj_kegiatan' => $id_obj,
							'long_x' => $situasi->lng_x,
							'lat_y' => $situasi->lat_y,
							'id_user' => $id_user
						]);
					}
				}
			}

			if (is_array($arraySatuan)) {
				if(count($arraySatuan) > 0){
					foreach($arraySatuan as $satuan)
					{
						$style = json_decode($satuan->style);
						$info = json_decode($satuan->info);

						$objRanpur = $this->getObjectRanpur($satuan->id_symbol, $style->grup);
						//$kecepatan = explode('|', $info->kecepatan);
						$kecepatan = $this->GetKecepatanMisi($satuan->nama);
						if($kecepatan == 0){
							$kecepatan = explode('|', $info->kecepatan);
						}

						date_default_timezone_set('Asia/Jakarta');
						//Tanggal
						$tgl = explode(" ", $info->tgl_mulai);
						$tgl_a = explode('-', $tgl[0]);
						$wkt = $tgl[1].':00';
						$tanggal = "".$tgl_a[2].'-'.$tgl_a[0].'-'.$tgl_a[1].' '.$wkt."";

						$warna = "Biru";
						if($info->warna == "red")
						{
							$warna = "Merah";
						}

						$sampai = null;
						if(isset($info->tgl_selesai) and $info->tgl_selesai!=null){
							$tgls = explode(" ", $info->tgl_selesai);
							$tgl_b = explode('-', $tgls[0]);
							$wktb = $tgl[1].':00';
							$sampai = "".$tgl_b[2].'-'.$tgl_b[0].'-'.$tgl_b[1].' '.$wktb."";
						}
						$cepat = $kecepatan[0];

						if($kecepatan[1] == "miles")
						{
							// $cepat = $kecepatan[0] * 1.60934;
							$cepat = $kecepatan[0] * 1.609344;
							
							// return response()->json([
							// 	"satuan" => $kecepatan[1],
							// 	"cepat" => $kecepatan[0],
							// 	"miles" => $cepat
							// ]);
						}
						if($kecepatan[1] == "knot")
						{
							$cepat = $kecepatan[0] * 1.852;
							// return response()->json([
							// 	"satuan" => $kecepatan[1],
							// 	"cepat" => $kecepatan[0],
							// 	"knot" => $cepat
							// ]);
						}
						if ($kecepatan[1] == "mach") {
							# code...
							$cepat = $kecepatan[0] * 1234.8;
						}

						$satuan_kecepatan = "kilometer";

						$size = 5;
						if($style->grup == 1){
							$size = 10;
						}else if($style->grup == 2){
							$size = 20;
						}

						$id = $this->getLastID('TFG.obj_kegiatan','id') + 1;

						DB::table('TFG.obj_kegiatan')->insert([
							'id' => $id,
							'id_obj' => $objRanpur->id_object_alutsista,
							'kecepatan' => $cepat,
							'satuan_kecepatan' => $satuan_kecepatan,
							'berangkat' => $tanggal,
							'sampai' => $sampai,
							'nama' => $satuan->nama,
							'color' => $warna,
							'size' => $size,
							'lat_y' => $satuan->lat_y,
							'long_x' => $satuan->lng_x,
						]);

						// $id_kegiatan = $this->getLastID('TFG.kegiatan','id') + 1;
						// DB::table('TFG.kegiatan')->insert([
						// 	'id' => $id_kegiatan,
						// 	'id_cb' => $r->id_cb,
						// 	'nama' => $satuan->nama,
						// 	'keterangan' => $info->weapon,
						// 	'tanggal' => $tanggal,
						// 	'type' => 'weapon',
						// 	'image' => '-',
						// 	'kogas_keg' => $r->kogas,
						// 	'id_obj_kegiatan' => $id,
						// 	'long_x' => $satuan->lat_y,
						// 	'lat_y' => $satuan->lng_x,
						// 	'id_user' => $id_user
						// ]);
					}
				}
			}

			if (is_array($arraySRoute)) {
				if(count($arraySRoute) > 0){
					$keterangan = new \stdClass();
					foreach ($arraySRoute as $asr)
					{
						$keterangan->properties = json_decode($asr->properties);
						$keterangan->geometry = json_decode($asr->geometry);

						$properties = json_decode($asr->properties);
						$geometry = json_decode($asr->geometry);


						date_default_timezone_set('Asia/Jakarta');
						$tanggal = date('Y-m-d H:i:s');

						$namaTool = explode('_', $asr->nama);
						$nameToolLower = strtolower($namaTool[0]);


						$long = $geometry->coordinates[0][0];
						$lat = $geometry->coordinates[0][1];

						$id_obj = $this->getLastID('TFG.obj_kegiatan','id') + 1;
						DB::table('TFG.obj_kegiatan')->insert([
							'id' => $id_obj,
							'id_obj' => 90008,
							'kecepatan' => 0,
							'satuan_kecepatan' => "",
							'berangkat' => NULL,
							'sampai' => NULL,
							'nama' => $asr->nama,
							'color' => $properties->color,
							'size' => $properties->weight,
							'lat_y' => $lat,
							'long_x' => $long,
						]);

						$obj_kegiatan = DB::table('TFG.obj_kegiatan')->where('nama',$asr->nama);

						if($obj_kegiatan->exists())
						{
							foreach ($geometry->coordinates as $coordinate) {
								# code...
								$long = $coordinate[0];
								$lat = $coordinate[1];

								$id = $this->getLastID('TFG.kegiatan','id') + 1;
								DB::table('TFG.kegiatan')->insert([
									'id' => $id,
									'id_cb' => $r->id_cb,
									'nama' => $asr->nama,
									'keterangan' => "Titik (".$long.",".$lat.")",
									'tanggal' => $tanggal,
									'type' => 'manuever',
									'image' => '-',
									'kogas_keg' => $r->kogas,
									'id_obj_kegiatan' => $obj_kegiatan->first()->id,
									'long_x' => $long,
									'lat_y' => $lat,
									'id_user' => $id_user
								]);
							}
						}
					}
				}
			}

			if (is_array($arrayPolyline)) {
				if(count($arrayPolyline) > 0){
					foreach($arrayPolyline as $polyline)
					{
						if($polyline->type == "LineString")
						{
							$obj_kegiatan = DB::table('TFG.obj_kegiatan')->where('nama',$polyline->nama);

							if($obj_kegiatan->exists())
							{
								$objRanpur = $obj_kegiatan->first();
								$geometry = json_decode($polyline->geometry);
								$properties = json_decode($polyline->properties);

								$count = 1;
								$id_pertama = 0;
								$tmpLong = 0;
								$tmpLat = 0;
								$tmpDate = $objRanpur->berangkat;

								foreach($geometry->coordinates as $coordinate)
								{
									$long = $coordinate[0];
									$lat = $coordinate[1];

									$id = $this->getLastID('TFG.kegiatan','id') + 1;
									if($count == 1)
									{
										$id_pertama = $id;
										DB::table('TFG.kegiatan')->insert([
											'id' => $id,
											'id_cb' => $r->id_cb,
											'nama' => $polyline->nama,
											'keterangan' => "Titik pertama object. (".$long.",".$lat.")",
											'tanggal' => $objRanpur->berangkat,
											'type' => 'waypoint',
											'image' => '-',
											'kogas_keg' => $r->kogas,
											'id_obj_kegiatan' => $objRanpur->id,
											'long_x' => $long,
											'lat_y' => $lat,
											'id_user' => $id_user
										]);
										$tmpLong = $long;
										$tmpLat = $lat;
									}else if($count == count($geometry->coordinates)){
										$calculateDate = $this->CalculateDate($lat, $long, $tmpLat, $tmpLong, $objRanpur->kecepatan, $tmpDate);
										DB::table('TFG.kegiatan')->insert([
											'id' => $id,
											'id_cb' => $r->id_cb,
											'nama' => "Tujuan terakhir ".$polyline->nama,
											'keterangan' => $polyline->nama . " telah sampai Di Tujuan (".$long.",".$lat.")",
											'tanggal' => $calculateDate,
											'type' => 'waypoint',
											'image' => '-',
											'kogas_keg' => $r->kogas,
											'id_obj_kegiatan' => $obj_kegiatan->first()->id,
											'long_x' => $long,
											'lat_y' => $lat,
											'id_user' => $id_user
										]);

										DB::table('TFG.kegiatan')->where('id', $id_pertama)->update([
											'selesai' => $calculateDate
										]);

										DB::table('TFG.obj_kegiatan')->where('id',$objRanpur->id)->update([
											'sampai' => $calculateDate
										]);
									}else{
										$calculateDate = $this->CalculateDate($lat, $long, $tmpLat, $tmpLong, $objRanpur->kecepatan, $tmpDate);
										DB::table('TFG.kegiatan')->insert([
											'id' => $id,
											'id_cb' => $r->id_cb,
											'nama' => "Point ke-".$count." dari object ".$polyline->nama,
											'keterangan' => "Object sampai ke titik (".$long.",".$lat.")",
											'tanggal' => $calculateDate,
											'type' => 'waypoint',
											'image' => '-',
											'kogas_keg' => $r->kogas,
											'id_obj_kegiatan' => $obj_kegiatan->first()->id,
											'long_x' => $long,
											'lat_y' => $lat,
											'id_user' => $id_user
										]);
										$tmpLong = $long;
										$tmpLat = $lat;
										$tmpDate = $calculateDate;
									}

									$count++;
								}

							}
							//meidi
							$name_p = explode("|", $polyline->nama);
							if($name_p[0]){

								if($name_p[0] == "Embarkasi"){
									$obj_kegiatan_e = DB::table('TFG.obj_kegiatan')->where('nama',$name_p[1]);
									$objRanpur = $obj_kegiatan_e->first();
									$geometry = json_decode($polyline->geometry);
									$properties = json_decode($polyline->properties);

									$obj_kegiatan = DB::table('TFG.obj_kegiatan')->where('nama',$name_p[2])->first();

									if($obj_kegiatan_e->exists()){
										$tmpLong = 0;
										$tmpLat = 0;
										$tmpDate = "";

										for ($e=1; $e >= 0; $e--) {
											$ket = "";
											if($e == 0){
												$ket = "Titik pertama object Embarkasi.";
											}else{
												$ket = "Titik Pertemuan dengan Object Utama Embarkasi.";
											}
											$coordinate = $geometry->coordinates[$e];

											$long = $coordinate[0];
											$lat = $coordinate[1];

											$kegiatan = DB::table('TFG.kegiatan')->where('id_obj_kegiatan', $obj_kegiatan->id)->where('long_x', $long)->where('lat_y', $lat)->first();

											if ($kegiatan != NULL) {
												$objDate = $kegiatan->tanggal;
											}else{
												$objDate = $this->CalculateDateMin($lat, $long, $tmpLat, $tmpLong, $objRanpur->kecepatan, $tmpDate);
											}

											$id = $this->getLastID('TFG.kegiatan','id') + 1;
											DB::table('TFG.kegiatan')->insert([
												'id' => $id,
												'id_cb' => $r->id_cb,
												'nama' => "Embarkasi data ".$name_p[1]." dari object |".$name_p[2],
												'keterangan' => $ket." (".$long.",".$lat.")",
												'tanggal' => $objDate,
												'type' => 'embarkasi',
												'image' => '-',
												'kogas_keg' => $r->kogas,
												'id_obj_kegiatan' => $obj_kegiatan_e->first()->id,
												'long_x' => $long,
												'lat_y' => $lat,
												'id_user' => $id_user
											]);

											$tmpLong = $long;
											$tmpLat = $lat;
											$tmpDate = $objDate;
										}
									}
								}else if($name_p[0] == "Debarkasi"){
									$obj_kegiatan_d = DB::table('TFG.obj_kegiatan')->where('nama',$name_p[3]);
									$objRanpur = $obj_kegiatan_d->first();
									$geometry = json_decode($polyline->geometry);
									$properties = json_decode($polyline->properties);

									$obj_kegiatan = DB::table('TFG.obj_kegiatan')->where('nama',$name_p[2])->first();

									if($obj_kegiatan_d->exists()){
										$count = 1;
										$id_pertama = 0;
										$tmpLong = 0;
										$tmpLat = 0;
										$tmpDate = "";

										for ($d=0; $d < count($geometry->coordinates); $d++) {
											$ket = "";
											if($d == 0){
												$ket = "Titik Perpisahan object Debarkasi.";
											}else if($d == (count($geometry->coordinates) - 1)){
												$ket = "Titik akhir object Debarkasi.";
											}else{
												$ket = "Titik ke-".($d + 1)." object Debarkasi.";
											}

											$coordinate = $geometry->coordinates[$d];

											$long = $coordinate[0];
											$lat = $coordinate[1];

											$kegiatan = DB::table('TFG.kegiatan')->where('id_obj_kegiatan', $obj_kegiatan->id)->where('long_x', $long)->where('lat_y', $lat)->first();

											if ($kegiatan != NULL) {
												$objDate = $kegiatan->tanggal;
											}else{
												$objDate = $this->CalculateDate($lat, $long, $tmpLat, $tmpLong, $objRanpur->kecepatan, $tmpDate);
											}

											$id = $this->getLastID('TFG.kegiatan','id') + 1;
											DB::table('TFG.kegiatan')->insert([
												'id' => $id,
												'id_cb' => $r->id_cb,
												'nama' => "Debarkasi data ".$name_p[3]." dari object ".$name_p[2],
												'keterangan' => $ket." (".$long.",".$lat.")",
												'tanggal' => $objDate,
												'type' => 'debarkasi',
												'image' => '-',
												'kogas_keg' => $r->kogas,
												'id_obj_kegiatan' => $obj_kegiatan_d->first()->id,
												'long_x' => $long,
												'lat_y' => $lat,
												'id_user' => $id_user
											]);
											$tmpLong = $long;
											$tmpLat = $lat;
											$tmpDate = $objDate;
										}
									}
								}
							}
						}
					}
				}
			}

			if (is_array($arrayKekuatan)) {
				if(count($arrayKekuatan) > 0){
					$keterangan = new \stdClass();

					foreach ($arrayKekuatan as $kekuatan)
					{
						$keterangan->info_kekuatan = json_decode($kekuatan->info_kekuatan);
						$keterangan->rincian = json_decode($kekuatan->rincian);

						$rincian = json_decode($kekuatan->rincian);

						//Kecepatan
						$kec = explode('|', $rincian->kecepatan);
						$kecepatan = $kec[0];

						if($kec[1] == "miles")
						{
							$kecepatan = $kec[0] * 1.60934;
						}
						if($kec[1] == "knot")
						{
							$kecepatan = $kec[0] * 1.852;
						}
						$satuan_kecepatan = "kilometer";

						//Warna
						if($rincian->warna == "red"){
							$warna = 'Merah';
						}else{
							$warna = 'Biru';
						}

						//Tanggal
						$tgl = explode(" ", $rincian->tgl_mulai);
						$tgl_a = explode('-', $tgl[0]);
						$wkt = $tgl[1].':00';
						$tanggal = "".$tgl_a[2].'-'.$tgl_a[0].'-'.$tgl_a[1].' '.$wkt."";

						$id_obj = $this->getLastID('TFG.obj_kegiatan','id') + 1;
						DB::table('TFG.obj_kegiatan')->insert([
							'id' => $id_obj,
							'id_obj' => 90001,
							'kecepatan' => $kecepatan,
							'satuan_kecepatan' => $satuan_kecepatan,
							'berangkat' => $tanggal,
							'sampai' => $tanggal,
							'nama' => $kekuatan->nama,
							'color' => $warna,
							'size' => $kekuatan->size,
							'lat_y' => $kekuatan->lat_y,
							'long_x' => $kekuatan->lng_x,
						]);

						$id = $this->getLastID('TFG.kegiatan','id') + 1;
						DB::table('TFG.kegiatan')->insert([
							'id' => $id,
							'id_cb' => $r->id_cb,
							'nama' => $kekuatan->nama,
							'keterangan' => json_encode($keterangan),
							'tanggal' => $tanggal,
							'type' => 'kekuatan',
							'image' => '-',
							'kogas_keg' => $r->kogas,
							'id_obj_kegiatan' => $id_obj,
							'long_x' => $kekuatan->lng_x,
							'lat_y' => $kekuatan->lat_y,
							'id_user' => $id_user
						]);
					}
				}
			}
			
			if (is_array($arrayBungus)) {
				# code...
				if(count($arrayBungus) > 0){
					foreach ($arrayBungus as $bungus)
					{
						$info_bungus = json_decode($bungus->info_bungus);
						$tgl = $info_bungus->date;

						//Tanggal
						$tgl_a = explode('-', $tgl[0]);
						$wkt = $tgl[1].':00';
						$tanggal = $tgl_a[2].'-'.$tgl_a[0].'-'.$tgl_a[1].' '.$wkt;

						//return response()->json($bungus->info_bungus);

						$id_obj = $this->getLastID('TFG.obj_kegiatan','id') + 1;
						$bungus_insert = DB::table('TFG.obj_kegiatan')->insert([
							'id' => $id_obj,
							'id_obj' => 90005,
							'kecepatan' => 0,
							'satuan_kecepatan' => "",
							'berangkat' => $tanggal,
							'sampai' => $tanggal,
							'nama' => $bungus->nama,
							'color' => null,
							'size' => $info_bungus->ukuran,
							'lat_y' => $bungus->lat_y,
							'long_x' => $bungus->lng_x,
						]);

						$id = $this->getLastID('TFG.kegiatan','id') + 1;
						DB::table('TFG.kegiatan')->insert([
							'id' => $id,
							'id_cb' => $r->id_cb,
							'nama' => $bungus->nama,
							'keterangan' => $bungus->info_bungus,
							'tanggal' => $tanggal,
							'type' => 'bungus',
							'image' => '-',
							'kogas_keg' => $r->kogas,
							'id_obj_kegiatan' => $id_obj,
							'long_x' => $bungus->lng_x,
							'lat_y' => $bungus->lat_y,
							'id_user' => $id_user
						]);
					}
				}
			}

			// if (is_array($arrayObstacle)) {
			// 	# code...
			// 	if(count($arrayObstacle) > 0){
			// 		foreach ($arrayObstacle as $obstacle)
			// 		{
			// 			$info_obstacle = json_decode($obstacle->info_obstacle);

			// 			//Warna
			// 			if($info_obstacle[2] == "red"){
			// 				$warna = 'Merah';
			// 			}else{
			// 				$warna = 'Biru';
			// 			}

			// 			date_default_timezone_set('Asia/Jakarta');
			// 			$tanggal = date('Y-m-d H:i:s');

			// 			$id_obj = $this->getLastID('TFG.obj_kegiatan','id') + 1;
			// 			DB::table('TFG.obj_kegiatan')->insert([
			// 				'id' => $id_obj,
			// 				'id_obj' => 90007,
			// 				'kecepatan' => 0,
			// 				'satuan_kecepatan' => "",
			// 				'berangkat' => NULL,
			// 				'sampai' => NULL,
			// 				'nama' => $obstacle->nama,
			// 				'color' => $warna,
			// 				'size' => $info_obstacle[3],
			// 				'lat_y' => $obstacle->lat_y,
			// 				'long_x' => $obstacle->lng_x,
			// 			]);

			// 			$id = $this->getLastID('TFG.kegiatan','id') + 1;
			// 			DB::table('TFG.kegiatan')->insert([
			// 				'id' => $id,
			// 				'id_cb' => $r->id_cb,
			// 				'nama' => $obstacle->nama,
			// 				'keterangan' => $obstacle->info_obstacle,
			// 				'tanggal' => $tanggal,
			// 				'type' => 'obstacle',
			// 				'image' => '-',
			// 				'kogas_keg' => $r->kogas,
			// 				'id_obj_kegiatan' => $id_obj,
			// 				'long_x' => $obstacle->lng_x,
			// 				'lat_y' => $obstacle->lat_y,
			// 				'id_user' => $id_user
			// 			]);
			// 		}
			// 	}
			// }

			if (is_array($arrayPassen)) {
				if(count($arrayPassen) > 0){
					$keterangan = new \stdClass();

					foreach ($arrayPassen as $passen)
					{
						$keterangan->nama_passen = $passen->nama_passen;
						$keterangan->info_passen = json_decode($passen->info_passen);

						$symbol = json_decode($passen->symbol);

						//warna
						if($passen->warna == "blue"){
							$warna = 'Biru';
						}else{
							$warna = 'Merah';
						}

						date_default_timezone_set('Asia/Jakarta');
						$tanggal = date('Y-m-d H:i:s');

						$id_obj = $this->getLastID('TFG.obj_kegiatan','id') + 1;
						DB::table('TFG.obj_kegiatan')->insert([
							'id' => $id_obj,
							'id_obj' => 90006,
							'kecepatan' => 0,
							'satuan_kecepatan' => "",
							'berangkat' => NULL,
							'sampai' => NULL,
							'nama' => $passen->nama,
							'color' => $warna,
							'size' => $passen->size,
							'lat_y' => $passen->lat_y,
							'long_x' => $passen->lng_x,
						]);

						$id = $this->getLastID('TFG.kegiatan','id') + 1;
						DB::table('TFG.kegiatan')->insert([
							'id' => $id,
							'id_cb' => $r->id_cb,
							'nama' => $passen->nama,
							'keterangan' => json_encode($keterangan),
							'tanggal' => $tanggal,
							'type' => 'passen',
							'image' => '-',
							'kogas_keg' => $r->kogas,
							'id_obj_kegiatan' => $id_obj,
							'long_x' => $passen->lng_x,
							'lat_y' => $passen->lat_y,
							'id_user' => $id_user
						]);
					}
				}
			}

			//if (is_array($arrayManuver)) {
				//if(count($arrayManuver) > 0){
					//foreach ($arrayManuver as $manuver)
					//{
						//Warna
						// if($manuver->warna == "red"){
						// 	$warna = 'Merah';
						// }else{
						// 	$warna = 'Biru';
						// }

						// date_default_timezone_set('Asia/Jakarta');
						// $tanggal = date('Y-m-d H:i:s');

						// $id_obj = $this->getLastID('TFG.obj_kegiatan','id') + 1;
						// DB::table('TFG.obj_kegiatan')->insert([
						// 	'id' => $id_obj,
						// 	'id_obj' => 90008,
						// 	'kecepatan' => 0,
						// 	'satuan_kecepatan' => "",
						// 	'berangkat' => NULL,
						// 	'sampai' => NULL,
						// 	'nama' => $manuver->nama,
						// 	'color' => $warna,
						// 	'size' => $manuver->size,
						// 	'lat_y' => $manuver->lat_y,
						// 	'long_x' => $manuver->lng_x,
						// ]);

						// $id = $this->getLastID('TFG.kegiatan','id') + 1;
						// DB::table('TFG.kegiatan')->insert([
						// 	'id' => $id,
						// 	'id_cb' => $r->id_cb,
						// 	'nama' => $manuver->nama,
						// 	'keterangan' => $manuver->info_manuver,
						// 	'tanggal' => $tanggal,
						// 	'type' => 'manuever',
						// 	'image' => '-',
						// 	'kogas_keg' => $r->kogas,
						// 	'id_obj_kegiatan' => $id_obj,
						// 	'long_x' => $manuver->lng_x,
						// 	'lat_y' => $manuver->lat_y,
						// 	'id_user' => $id_user
						// ]);
					//}
				//}
			//}

			if (is_array($arrayRadar)) {
				if(count($arrayRadar) > 0){
					$keterangan = new \stdClass();

					foreach ($arrayRadar as $radar)
					{
						$keterangan->info_radar = json_decode($radar->info_radar);
						$keterangan->info_symbol = json_decode($radar->info_symbol);

						$info_radar = json_decode($radar->info_radar);
						$symbol_radar = json_decode($radar->symbol);
						$info_symbol = json_decode($radar->info_symbol);

						if($info_radar->warna == "red"){
							$warna = "Merah";
						}else{
							$warna = "Biru";
						}

						date_default_timezone_set('Asia/Jakarta');
						$tanggal = date('Y-m-d H:i:s');

						$id_obj = $this->getLastID('TFG.obj_kegiatan','id') + 1;
						DB::table('TFG.obj_kegiatan')->insert([
							'id' => $id_obj,
							'id_obj' => 90003,
							'kecepatan' => 0,
							'satuan_kecepatan' => "",
							'berangkat' => NULL,
							'sampai' => NULL,
							'nama' => $radar->nama,
							'color' => $warna,
							'size' => $info_radar->size,
							'lat_y' => $radar->lat_y,
							'long_x' => $radar->lng_x,
						]);

						$id = $this->getLastID('TFG.kegiatan','id') + 1;
						DB::table('TFG.kegiatan')->insert([
							'id' => $id,
							'id_cb' => $r->id_cb,
							'nama' => $radar->nama,
							'keterangan' => json_encode($keterangan),
							'tanggal' => $tanggal,
							'type' => 'radar',
							'image' => '-',
							'kogas_keg' => $r->kogas,
							'id_obj_kegiatan' => $id_obj,
							'long_x' => $radar->lng_x,
							'lat_y' => $radar->lat_y,
							'id_user' => $id_user
						]);

					}
				}
			}

			if (is_array($arrayLogis)) {
				if(count($arrayLogis) > 0){
					$keterangan = new \stdClass();

					foreach ($arrayLogis as $logis)
					{
						$keterangan->info_logistik = json_decode($logis->info_logistik);
						$keterangan->isi_logistik = json_decode($logis->isi_logistik);

						$isi_logistik = json_decode($logis->isi_logistik);
						$symbol_logis = json_decode($logis->symbol);

						if($logis->warna == "red"){
							$warna = 'Merah';
						}else{
							$warna = 'Biru';
						}

						date_default_timezone_set('Asia/Jakarta');
						$tanggal = date('Y-m-d H:i:s');

						if ($logis->jenis == "Bebas") {
							# code...
							$id_obj = $this->getLastID('TFG.obj_kegiatan','id') + 1;
							DB::table('TFG.obj_kegiatan')->insert([
								'id' => $id_obj,
								'id_obj' => 90010, // object gambar
								'kecepatan' => 0,
								'satuan_kecepatan' => "",
								'berangkat' =>NULL,
								'sampai' =>NULL,
								'nama' => $logis->nama,
								'color' => $warna,
								'size' => $logis->size,
								'lat_y' => $logis->lat_y,
								'long_x' => $logis->lng_x,
							]);

							$id = $this->getLastID('TFG.kegiatan','id') + 1;
							DB::table('TFG.kegiatan')->insert([
								'id' => $id,
								'id_cb' => $r->id_cb,
								'nama' => $logis->nama,
								'keterangan' => json_encode($keterangan),
								'tanggal' => $tanggal,
								'type' => 'logis',
								'image' => '-',
								'kogas_keg' => $r->kogas,
								'id_obj_kegiatan' => $id_obj,
								'long_x' => $logis->lng_x,
								'lat_y' => $logis->lat_y,
								'id_user' => $id_user
							]);
						} else {
							$id_obj = $this->getLastID('TFG.obj_kegiatan','id') + 1;
							DB::table('TFG.obj_kegiatan')->insert([
								'id' => $id_obj,
								'id_obj' => 90011, // object font
								'kecepatan' => 0,
								'satuan_kecepatan' => "",
								'berangkat' =>NULL,
								'sampai' =>NULL,
								'nama' => $logis->nama,
								'color' => $warna,
								'size' => $logis->size,
								'lat_y' => $logis->lat_y,
								'long_x' => $logis->lng_x,
							]);

							$id = $this->getLastID('TFG.kegiatan','id') + 1;
							DB::table('TFG.kegiatan')->insert([
								'id' => $id,
								'id_cb' => $r->id_cb,
								'nama' => $logis->nama,
								'keterangan' => json_encode($keterangan),
								'tanggal' => $tanggal,
								'type' => 'logis',
								'image' => '-',
								'kogas_keg' => $r->kogas,
								'id_obj_kegiatan' => $id_obj,
								'long_x' => $logis->lng_x,
								'lat_y' => $logis->lat_y,
								'id_user' => $id_user
							]);
						}
					}
				}
			}

			if (is_array($arrayTool)) {
				if(count($arrayTool) > 0){
					$keterangan = new \stdClass();

					foreach ($arrayTool as $tool)
					{
						$keterangan->properties = json_decode($tool->properties);
						$keterangan->geometry = json_decode($tool->geometry);

						$properties = json_decode($tool->properties);
						$geometry = json_decode($tool->geometry);


						date_default_timezone_set('Asia/Jakarta');
						$tanggal = date('Y-m-d H:i:s');

						$namaTool = explode('_', $tool->nama);
						$nameToolLower = strtolower($namaTool[0]);

						if ($tool->type == "LineString" && $nameToolLower == "polyline") {
							# code...
							$long = $geometry->coordinates[0][0];
							$lat = $geometry->coordinates[0][1];

							$id_obj = $this->getLastID('TFG.obj_kegiatan','id') + 1;
							DB::table('TFG.obj_kegiatan')->insert([
								'id' => $id_obj,
								'id_obj' => 90004,
								'kecepatan' => 0,
								'satuan_kecepatan' => "",
								'berangkat' => NULL,
								'sampai' => NULL,
								'nama' => $tool->nama,
								'color' => $properties->color,
								'size' => $properties->weight,
								'lat_y' => $lat,
								'long_x' => $long,
							]);

							$obj_kegiatan = DB::table('TFG.obj_kegiatan')->where('nama',$tool->nama);

							if($obj_kegiatan->exists())
							{
								foreach ($geometry->coordinates as $coordinate) {
									# code...
									$long = $coordinate[0];
									$lat = $coordinate[1];

									$id = $this->getLastID('TFG.kegiatan','id') + 1;
									DB::table('TFG.kegiatan')->insert([
										'id' => $id,
										'id_cb' => $r->id_cb,
										'nama' => $tool->nama,
										'keterangan' => "Titik (".$long.",".$lat.")",
										'tanggal' => $tanggal,
										'type' => 'tool-linestring',
										'image' => '-',
										'kogas_keg' => $r->kogas,
										'id_obj_kegiatan' => $obj_kegiatan->first()->id,
										'long_x' => $long,
										'lat_y' => $lat,
										'id_user' => $id_user
									]);
								}
							}
						} else if ($tool->type == "Polygon" && $nameToolLower == "polygon") {
							# code...
							$long = $geometry->coordinates[0][0][0];
							$lat = $geometry->coordinates[0][0][1];

							$id_obj = $this->getLastID('TFG.obj_kegiatan','id') + 1;
							DB::table('TFG.obj_kegiatan')->insert([
								'id' => $id_obj,
								'id_obj' => 90004,
								'kecepatan' => 0,
								'satuan_kecepatan' => "",
								'berangkat' => NULL,
								'sampai' => NULL,
								'nama' => $tool->nama,
								'color' => $properties->color,
								'size' => $properties->weight,
								'lat_y' => $lat,
								'long_x' => $long,
							]);

							$obj_kegiatan = DB::table('TFG.obj_kegiatan')->where('nama',$tool->nama);

							if($obj_kegiatan->exists())
							{
								foreach ($geometry->coordinates[0] as $coordinate) {
									# code...
									$long = $coordinate[0];
									$lat = $coordinate[1];

									$id = $this->getLastID('TFG.kegiatan','id') + 1;
									DB::table('TFG.kegiatan')->insert([
										'id' => $id,
										'id_cb' => $r->id_cb,
										'nama' => $tool->nama,
										'keterangan' => "Titik (".$long.",".$lat.")",
										'tanggal' => $tanggal,
										'type' => 'tool-polygon',
										'image' => '-',
										'kogas_keg' => $r->kogas,
										'id_obj_kegiatan' => $obj_kegiatan->first()->id,
										'long_x' => $long,
										'lat_y' => $lat,
										'id_user' => $id_user
									]);
								}
							}
						} else if ($tool->type == "circle" && $nameToolLower == "circle") {
							# code...
							$long = $geometry->coordinates[0];
							$lat = $geometry->coordinates[1];

							$id_obj = $this->getLastID('TFG.obj_kegiatan','id') + 1;
							DB::table('TFG.obj_kegiatan')->insert([
								'id' => $id_obj,
								'id_obj' => 90013,
								'kecepatan' => 0,
								'satuan_kecepatan' => "",
								'berangkat' => NULL,
								'sampai' => NULL,
								'nama' => $tool->nama,
								'color' => $properties->color,
								'size' => $properties->weight,
								'lat_y' => $lat,
								'long_x' => $long,
							]);

							$obj_kegiatan = DB::table('TFG.obj_kegiatan')->where('nama',$tool->nama);

							if($obj_kegiatan->exists())
							{
								$id = $this->getLastID('TFG.kegiatan','id') + 1;
								DB::table('TFG.kegiatan')->insert([
									'id' => $id,
									'id_cb' => $r->id_cb,
									'nama' => $tool->nama,
									// 'keterangan' => "Radius:".$properties->radius,
									'keterangan' => json_encode($keterangan),
									'tanggal' => $tanggal,
									'type' => 'tool-circle',
									'image' => '-',
									'kogas_keg' => $r->kogas,
									'id_obj_kegiatan' => $obj_kegiatan->first()->id,
									'long_x' => $long,
									'lat_y' => $lat,
									'id_user' => $id_user
								]);
							}
						} else if ($tool->type == "Polygon" && $nameToolLower == "arrowatasman") {
							# code...
							$long = $geometry->coordinates[0][0][0];
							$lat = $geometry->coordinates[0][0][1];

							$id_obj = $this->getLastID('TFG.obj_kegiatan','id') + 1;
							DB::table('TFG.obj_kegiatan')->insert([
								'id' => $id_obj,
								'id_obj' => 90004,
								'kecepatan' => 0,
								'satuan_kecepatan' => "",
								'berangkat' => NULL,
								'sampai' => NULL,
								'nama' => $tool->nama,
								'color' => $properties->color,
								'size' => $properties->weight,
								'lat_y' => $lat,
								'long_x' => $long,
							]);

							$obj_kegiatan = DB::table('TFG.obj_kegiatan')->where('nama',$tool->nama);

							if($obj_kegiatan->exists())
							{
								foreach ($geometry->coordinates[0] as $coordinate) {
									# code...
									$long = $coordinate[0];
									$lat = $coordinate[1];

									$id = $this->getLastID('TFG.kegiatan','id') + 1;
									DB::table('TFG.kegiatan')->insert([
										'id' => $id,
										'id_cb' => $r->id_cb,
										'nama' => $tool->nama,
										'keterangan' => "Titik (".$long.",".$lat.")",
										'tanggal' => $tanggal,
										'type' => 'tool-arrowatasman',
										'image' => '-',
										'kogas_keg' => $r->kogas,
										'id_obj_kegiatan' => $obj_kegiatan->first()->id,
										'long_x' => $long,
										'lat_y' => $lat,
										'id_user' => $id_user
									]);
								}
							}
						} else if ($tool->type == "Polygon" && $nameToolLower == "arrowbawahman") {
							# code...
							$long = $geometry->coordinates[0][0][0];
							$lat = $geometry->coordinates[0][0][1];

							$id_obj = $this->getLastID('TFG.obj_kegiatan','id') + 1;
							DB::table('TFG.obj_kegiatan')->insert([
								'id' => $id_obj,
								'id_obj' => 90004,
								'kecepatan' => 0,
								'satuan_kecepatan' => "",
								'berangkat' => NULL,
								'sampai' => NULL,
								'nama' => $tool->nama,
								'color' => $properties->color,
								'size' => $properties->weight,
								'lat_y' => $lat,
								'long_x' => $long,
							]);

							$obj_kegiatan = DB::table('TFG.obj_kegiatan')->where('nama',$tool->nama);

							if($obj_kegiatan->exists())
							{
								foreach ($geometry->coordinates[0] as $coordinate) {
									# code...
									$long = $coordinate[0];
									$lat = $coordinate[1];

									$id = $this->getLastID('TFG.kegiatan','id') + 1;
									DB::table('TFG.kegiatan')->insert([
										'id' => $id,
										'id_cb' => $r->id_cb,
										'nama' => $tool->nama,
										'keterangan' => "Titik (".$long.",".$lat.")",
										'tanggal' => $tanggal,
										'type' => 'tool-arrowbawahman',
										'image' => '-',
										'kogas_keg' => $r->kogas,
										'id_obj_kegiatan' => $obj_kegiatan->first()->id,
										'long_x' => $long,
										'lat_y' => $lat,
										'id_user' => $id_user
									]);
								}
							}
						} else if ($tool->type == "Polygon" && $nameToolLower == "arrowatas") {
							# code...
							$long = $geometry->coordinates[0][0][0];
							$lat = $geometry->coordinates[0][0][1];

							$id_obj = $this->getLastID('TFG.obj_kegiatan','id') + 1;
							DB::table('TFG.obj_kegiatan')->insert([
								'id' => $id_obj,
								'id_obj' => 90004,
								'kecepatan' => 0,
								'satuan_kecepatan' => "",
								'berangkat' => NULL,
								'sampai' => NULL,
								'nama' => $tool->nama,
								'color' => $properties->color,
								'size' => $properties->weight,
								'lat_y' => $lat,
								'long_x' => $long,
							]);

							$obj_kegiatan = DB::table('TFG.obj_kegiatan')->where('nama',$tool->nama);

							if($obj_kegiatan->exists())
							{
								foreach ($geometry->coordinates[0] as $coordinate) {
									# code...
									$long = $coordinate[0];
									$lat = $coordinate[1];

									$id = $this->getLastID('TFG.kegiatan','id') + 1;
									DB::table('TFG.kegiatan')->insert([
										'id' => $id,
										'id_cb' => $r->id_cb,
										'nama' => $tool->nama,
										'keterangan' => "Titik (".$long.",".$lat.")",
										'tanggal' => $tanggal,
										'type' => 'tool-arrowatas',
										'image' => '-',
										'kogas_keg' => $r->kogas,
										'id_obj_kegiatan' => $obj_kegiatan->first()->id,
										'long_x' => $long,
										'lat_y' => $lat,
										'id_user' => $id_user
									]);
								}
							}
						} else if ($tool->type == "Polygon" && $nameToolLower == "arrowbawah") {
							# code...
							$long = $geometry->coordinates[0][0][0];
							$lat = $geometry->coordinates[0][0][1];

							$id_obj = $this->getLastID('TFG.obj_kegiatan','id') + 1;
							DB::table('TFG.obj_kegiatan')->insert([
								'id' => $id_obj,
								'id_obj' => 90004,
								'kecepatan' => 0,
								'satuan_kecepatan' => "",
								'berangkat' => NULL,
								'sampai' => NULL,
								'nama' => $tool->nama,
								'color' => $properties->color,
								'size' => $properties->weight,
								'lat_y' => $lat,
								'long_x' => $long,
							]);

							$obj_kegiatan = DB::table('TFG.obj_kegiatan')->where('nama',$tool->nama);

							if($obj_kegiatan->exists())
							{
								foreach ($geometry->coordinates[0] as $coordinate) {
									# code...
									$long = $coordinate[0];
									$lat = $coordinate[1];

									$id = $this->getLastID('TFG.kegiatan','id') + 1;
									DB::table('TFG.kegiatan')->insert([
										'id' => $id,
										'id_cb' => $r->id_cb,
										'nama' => $tool->nama,
										'keterangan' => "Titik (".$long.",".$lat.")",
										'tanggal' => $tanggal,
										'type' => 'tool-arrowbawah',
										'image' => '-',
										'kogas_keg' => $r->kogas,
										'id_obj_kegiatan' => $obj_kegiatan->first()->id,
										'long_x' => $long,
										'lat_y' => $lat,
										'id_user' => $id_user
									]);
								}
							}
						} else if ($tool->type == "Polygon" && $nameToolLower == "arrowlurus") {
							# code...
							$long = $geometry->coordinates[0][0][0];
							$lat = $geometry->coordinates[0][0][1];

							$id_obj = $this->getLastID('TFG.obj_kegiatan','id') + 1;
							DB::table('TFG.obj_kegiatan')->insert([
								'id' => $id_obj,
								'id_obj' => 90004,
								'kecepatan' => 0,
								'satuan_kecepatan' => "",
								'berangkat' => NULL,
								'sampai' => NULL,
								'nama' => $tool->nama,
								'color' => $properties->color,
								'size' => $properties->weight,
								'lat_y' => $lat,
								'long_x' => $long,
							]);

							$obj_kegiatan = DB::table('TFG.obj_kegiatan')->where('nama',$tool->nama);

							if($obj_kegiatan->exists())
							{
								foreach ($geometry->coordinates[0] as $coordinate) {
									# code...
									$long = $coordinate[0];
									$lat = $coordinate[1];

									$id = $this->getLastID('TFG.kegiatan','id') + 1;
									DB::table('TFG.kegiatan')->insert([
										'id' => $id,
										'id_cb' => $r->id_cb,
										'nama' => $tool->nama,
										'keterangan' => "Titik (".$long.",".$lat.")",
										'tanggal' => $tanggal,
										'type' => 'tool-arrowlurus',
										'image' => '-',
										'kogas_keg' => $r->kogas,
										'id_obj_kegiatan' => $obj_kegiatan->first()->id,
										'long_x' => $long,
										'lat_y' => $lat,
										'id_user' => $id_user
									]);
								}
							}
						}
					}
				}
			}

			if (is_array($arrayFormasi)) {
				if(count($arrayFormasi) > 0){
					foreach ($arrayFormasi as $formasi)
					{
						$info_formasi = json_decode($formasi->info_formasi);
						$symbol_formasi = json_decode($formasi->symbol);

						date_default_timezone_set('Asia/Jakarta');
						$tanggal = date('Y-m-d H:i:s');

						if($formasi->warna == "red"){
							$warna = 'Merah';
						}else{
							$warna = 'Biru';
						}

						$size = 20;
						// if($style->grup == 3){
						// 	$size = 5;
						// }

						$id_obj = $this->getLastID('TFG.obj_kegiatan','id') + 1;

							DB::table('TFG.obj_kegiatan')->insert([
								'id' => $id_obj,
								'id_obj' => 90012,
								'kecepatan' => 0,
								'satuan_kecepatan' => "",
								'berangkat' => NULL,
								'sampai' => NULL,
								'nama' => $formasi->nama,
								'color' => $warna,
								'size' => $size,
								'lat_y' => $formasi->lat_y,
								'long_x' => $formasi->lng_x
							]);

						$id = $this->getLastID('TFG.kegiatan','id') + 1;
						DB::table('TFG.kegiatan')->insert([
							'id' => $id,
							'id_cb' => $r->id_cb,
							'nama' => $formasi->nama,
							'keterangan' => $formasi->info_formasi,
							'tanggal' => $tanggal,
							'type' => 'formasi',
							'image' => '-',
							'kogas_keg' => $r->kogas,
							'id_obj_kegiatan' => $id_obj,
							'long_x' => $formasi->lng_x,
							'lat_y' => $formasi->lat_y,
							'id_user' => $id_user
						]);

						// foreach($info_formasi as $info){
						// 	$id_obj = $this->getLastID('TFG.obj_kegiatan','id') + 1;

						// 	DB::table('TFG.obj_kegiatan')->insert([
						// 		'id' => $id_obj,
						// 		'id_obj' => 90012,
						// 		'kecepatan' => 0,
						// 		'satuan_kecepatan' => "",
						// 		'berangkat' => NULL,
						// 		'sampai' => NULL,
						// 		'nama' => $info->symbol->options->id_point,
						// 		'color' => $warna,
						// 		'size' => 10,
						// 		'lat_y' => $info->lat,
						// 		'long_x' => $info->lng,
						// 	]);

						// 	$id = $this->getLastID('TFG.kegiatan','id') + 1;
						// 	$keterangan = [
						// 			$info->index_symbol,
						// 			$info->family_symbol
						// 	];

						// 	DB::table('TFG.kegiatan')->insert([
						// 		'id' => $id,
						// 		'id_cb' => $r->id_cb,
						// 		'nama' => $info->symbol->options->id_point,
						// 		'keterangan' => implode(',', $keterangan),
						// 		'tanggal' => $tanggal,
						// 		'type' => 'formasi',
						// 		'image' => '-',
						// 		'kogas_keg' => $r->kogas,
						// 		'id_obj_kegiatan' => $id_obj,
						// 		'long_x' => $info->lng,
						// 		'lat_y' => $info->lat,
						// 'id_user' => $id_user
						// 	]);
						// }
					}
				}
			}

			if (is_array($arrayText)) {
				# code...
				if(count($arrayText) > 0){
					foreach ($arrayText as $text)
					{
						// $info_text = json_decode($text->info_text);
						// $symbol_text = json_decode($text->symbol);

						// date_default_timezone_set('Asia/Jakarta');
						// $tanggal = date('Y-m-d H:i:s');

						// $id_obj = $this->getLastID('TFG.obj_kegiatan','id') + 1;
						// DB::table('TFG.obj_kegiatan')->insert([
						// 	'id' => $id_obj,
						// 	'id_obj' => 90009,
						// 	'kecepatan' => 0,
						// 	'satuan_kecepatan' => "",
						// 	'berangkat' =>NULL,
						// 	'sampai' =>NULL,
						// 	'nama' => $text->nama,
						// 	'color' => $info_text->warna,
						// 	'size' => $info_text->size,
						// 	'lat_y' => $text->lat_y,
						// 	'long_x' => $text->lng_x,
						// ]);

						// $id = $this->getLastID('TFG.kegiatan','id') + 1;
						// DB::table('TFG.kegiatan')->insert([
						// 	'id' => $id,
						// 	'id_cb' => $r->id_cb,
						// 	'nama' => $text->nama,
						// 	'keterangan' => $text->info_text,
						// 	'tanggal' => $tanggal,
						// 	'type' => 'text',
						// 	'image' => '-',
						// 	'kogas_keg' => $r->kogas,
						// 	'id_obj_kegiatan' => $id_obj,
						// 	'long_x' => $text->lng_x,
						// 	'lat_y' => $text->lat_y,
						// 	'id_user' => $id_user
						// ]);

					}
				}
			}

			return response()->json(true);	
		// } catch (Exception $th) {
		// 	//throw $th;
		// 	return response()->json(['error' => 'Server Error!, saat loadFromEPPKM2D.']);
		// }
	}

	// Misi
	public function insert2dMisi(Request $r)
	{
		# code...
		$id_cb = $r->id_cb;
		$kogas = $r->kogas;
		$id_user = $r->id_user;
		$arrayMisi = json_decode($r->arrayMisi);
		
		if (is_array($arrayMisi)) {
			if(count($arrayMisi) > 0){
				foreach($arrayMisi as $misi)
				{
					$obj_kegiatan = DB::table('TFG.obj_kegiatan')->where('nama',$misi->id_object);
					
					if($obj_kegiatan->exists())
					{
						$objRanpur = $obj_kegiatan->first();
						$properties = json_decode($misi->properties);
						$cepat = $properties->kecepatan;

						if($properties->type == "miles")
						{
							$cepat = $properties->kecepatan * 1.609344;
						}
						if($properties->type == "knot")
						{
							$cepat = $properties->kecepatan * 1.852;
						}
						if ($properties->type == "mach") {
							# code...
							$cepat = $properties->kecepatan * 1234.8;
						}
						
						$count = 1;
						$id_pertama = 0;
						$tmpLong = 0;
						$tmpLat = 0;
						$tmpDate = '0000-00-00 00:00:00';
						$tglMulai = '0000-00-00 00:00:00';

						if($misi->tgl_mulai) {
							$tglData = explode(" ", $misi->tgl_mulai);
							$tgl = explode('-', $tglData[0]);
							$wkt = $tglData[1].':00';
							$tmpDate = "".$tgl[2].'-'.$tgl[0].'-'.$tgl[1].' '.$wkt."";
							$tglMulai = "".$tgl[2].'-'.$tgl[0].'-'.$tgl[1].' '.$wkt."";
						}
						
						foreach($properties->jalur as $coordinate)
						{
							$long = $coordinate->lng;
							$lat = $coordinate->lat;
							
							$id = $this->getLastID('TFG.kegiatan','id') + 1;
							if($count == 1)
							{
								$id_pertama = $id;
								$insertKegiatan = DB::table('TFG.kegiatan')->insert([
									'id' => $id,
									'id_cb' => $id_cb,
									'nama' => $properties->nama_misi,
									'keterangan' => "Titik pertama object. (".$long.",".$lat.")",
									'tanggal' => $tglMulai,
									'type' => 'misi',
									'image' => '-',
									'kogas_keg' => $kogas,
									'id_obj_kegiatan' => $objRanpur->id,
									'long_x' => $long,
									'lat_y' => $lat,
									'id_user' => $id_user,
									'kecepatan_misi' => $cepat,
									// 'id_kegiatan' => $properties->id_kegiatan
									'id_misi' => $misi->id_mission,
								]);
								$tmpLong = $long;
								$tmpLat = $lat;
							}else if($count == count($properties->jalur)){
								$calculateDate = $this->CalculateDate($lat, $long, $tmpLat, $tmpLong, $cepat, $tmpDate);
								$insertKegiatan = DB::table('TFG.kegiatan')->insert([
									'id' => $id,
									'id_cb' => $id_cb,
									'nama' => "Tujuan terakhir ".$properties->nama_misi,
									'keterangan' => $properties->nama_misi . " telah sampai Di Tujuan (".$long.",".$lat.")",
									'tanggal' => $calculateDate,
									'type' => 'misi',
									'image' => '-',
									'kogas_keg' => $kogas,
									'id_obj_kegiatan' => $obj_kegiatan->first()->id,
									'long_x' => $long,
									'lat_y' => $lat,
									'id_user' => $id_user,
									'kecepatan_misi' => $cepat,
									// 'id_kegiatan' => $properties->id_kegiatan
									'id_misi' => $misi->id_mission,
								]);

								DB::table('TFG.kegiatan')->where('id', $id_pertama)->update([
									'selesai' => $calculateDate
								]);

								DB::table('TFG.obj_kegiatan')->where('id',$objRanpur->id)->update([
									'sampai' => $calculateDate
								]);
							}else{
								$calculateDate = $this->CalculateDate($lat, $long, $tmpLat, $tmpLong, $cepat, $tmpDate);
								$insertKegiatan = DB::table('TFG.kegiatan')->insert([
									'id' => $id,
									'id_cb' => $id_cb,
									'nama' => "Point ke-".$count." dari object ".$properties->nama_misi,
									'keterangan' => "Object sampai ke titik (".$long.",".$lat.")",
									'tanggal' => $calculateDate,
									'type' => 'misi',
									'image' => '-',
									'kogas_keg' => $kogas,
									'id_obj_kegiatan' => $obj_kegiatan->first()->id,
									'long_x' => $long,
									'lat_y' => $lat,
									'id_user' => $id_user,
									'kecepatan_misi' => $cepat,
									// 'id_kegiatan' => $properties->id_kegiatan
									'id_misi' => $misi->id_mission,
								]);
								$tmpLong = $long;
								$tmpLat = $lat;
								$tmpDate = $calculateDate;
							}

							$count++;
						}
					}
				}
			}
		}

		return response()->json(true);	
	}

	public function getObjectFromMisi(Request $r)
	{
		$kegiatan_data = DB::table('TFG.kegiatan as k')
			->join('TFG.cb as c', 'c.id', '=', 'k.id_cb')
			->select('k.*', 'c.skenario')
			->where('k.id_cb', $r->id_cb)
			->where('k.id_user', $r->id_user)
			->where('c.skenario', $r->skenario);

		$data = $kegiatan_data
			->where('k.id_obj_kegiatan', '!=', 0)
			->whereNotNull('k.id_obj_kegiatan')
			->whereNotNull('k.selesai')
			->orderBy('k.id')
			->orderBy('k.tanggal')
			// ->orderBy('k.id_obj_kegiatan')
			->get();

		$obj_kegiatan = $data->all();

		$v = [];
		$response = [];
		foreach ($obj_kegiatan as $key => $obj) {
			$result = DB::table('TFG.obj_kegiatan')->select('*')->where('id', $obj->id_obj_kegiatan)->first();
			$obj_3d = DB::table('object_alutsista_3d_2021')->where('id_object_alutsista', $result->id_obj);
			$obj_3d = $obj_3d->exists() ? $obj_3d->first() : DB::table('object_alutsista_3d')->where('id_object_alutsista', $result->id_obj)->first();
			$font_taktis = DB::table('font_taktis')->where('id',$obj_3d->id_font_taktis)->first();

			$misi = DB::table('TFG.kegiatan')->select('*')
				->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
				->where('nama', 'like', '%'.$obj->nama.'%')
				->where('type', 'misi')
				->orderBy('id')
				->get();

			if (array_key_exists($obj->id_obj_kegiatan, $v)) {
				$tmp = $v[$obj->id_obj_kegiatan];
				$response[$tmp]['schedule']['misi'][0]->tgl_misi_lanjut = $misi[0]->tanggal;
			}

			$v[$obj->id_obj_kegiatan] = $key;

			$response[] = [
				'obj_kegiatan' => $result,
				'obj_3d' => $obj_3d,
				'font_taktis' => $font_taktis,
				'schedule' => [
					'misi' => $misi,
				]
			];
		}

		return response()->json($response);
	}

	public function getObjFromMisi(Request $r)
	{

		$kegiatan_data = DB::table('TFG.kegiatan as k')
			->join('TFG.cb as c', 'c.id', '=', 'k.id_cb')
			->select('k.*', 'c.skenario')
			->where('k.id_cb', $r->id_cb)
			->where('k.id_user', $r->id_user)
			->where('c.skenario', $r->skenario);

		$data = $kegiatan_data
			->where('k.id_obj_kegiatan', '!=', 0)
			->whereNotNull('k.id_obj_kegiatan')
			->orderBy('k.tanggal')
			->get();

		$obj_kegiatan = $data->unique('id_obj_kegiatan')->values()->all();

		$response = [];
		foreach ($obj_kegiatan as $obj) {
			$result = DB::table('TFG.obj_kegiatan')->select('*')->where('id', $obj->id_obj_kegiatan);
			if ($result->exists()) { $result = $result->first(); } 

			$obj_3d = DB::table('object_alutsista_3d_2021')->where('id_object_alutsista', $result->id_obj);
			if ($obj_3d->exists()) { $obj_3d = $obj_3d->first(); } 
			else { $obj_3d = DB::table('object_alutsista_3d')->where('id_object_alutsista', $result->id_obj)->first(); }

			$font_taktis = DB::table('font_taktis')->where('id',$obj_3d->id_font_taktis)->first();

			$misiTP = [];

			$misi = DB::table('TFG.kegiatan')->select('*')
						->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
						->where('type', 'misi')
						->orderBy('tanggal')
						->orderBy('id')
						->get();

			foreach($misi as $m) {
				if (str_contains($m->keterangan, "Titik pertama object.")) {
					# code...
					$urut_misi = DB::table('TFG.kegiatan')->select('*')
						->where('id_obj_kegiatan', $obj->id_obj_kegiatan)
						->where('id_misi', $m->id_misi)
						->where('type', 'misi')
						->orderBy('tanggal')
						->orderBy('id')
						->get();

					array_push($misiTP, $urut_misi);
				}
			}

			for ($i=0; $i < count($misiTP); $i++) { 
				# code...
				$tanggalNextMisi = null;
				if (isset($misiTP[$i+1])) {
					# code...
					$nextMisi = $misiTP[$i+1][0];	
					$tanggalNextMisi = $nextMisi->tanggal;
					
					for ($j=0; $j < count($misiTP[$i]); $j++) { 
						# code...
						$misiTP[$i][$j]->nextMisi = $tanggalNextMisi;
					}
				} else {
					# code...
					for ($j=0; $j < count($misiTP[$i]); $j++) { 
						# code...
						$misiTP[$i][$j]->nextMisi = $tanggalNextMisi;
					}
				}
			}

			$response[] = [
				'obj_kegiatan' => $result,
				'obj_3d' => $obj_3d,
				'font_taktis' => $font_taktis,
				'schedule' => [
					'misi' => $misiTP
				]
			];
		}
		return response()->json($response);
	}

	public function getWaktuMisi(Request $r) 
	{
		$data = DB::table('TFG.kegiatan')->select('tanggal')->where('type', 'misi')->orderBy('tanggal')->first();
		$hasil = $data->tanggal;

		return response()->json($hasil);
	}
	// End

	/**
	 * Calculates the distance between two points, given their
	 * latitude and longitude, and returns an array of values
	 * of the most common distance units
	 *
	 * @param  {coord} $lat1 Latitude of the first point
	 * @param  {coord} $lon1 Longitude of the first point
	 * @param  {coord} $lat2 Latitude of the second point
	 * @param  {coord} $lon2 Longitude of the second point
	 * @return {array}       Array of values in many distance units
	 */
	function getDistanceBetweenPoints($lat1, $lon1, $lat2, $lon2) {
		$theta = $lon1 - $lon2;
		$miles = (sin(deg2rad($lat1)) * sin(deg2rad($lat2))) + (cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta)));
		$miles = acos($miles);
		$miles = rad2deg($miles);
		$miles = $miles * 60 * 1.1515;
		$feet = $miles * 5280;
		$yards = $feet / 3;
		$kilometers = $miles * 1.609344;
		$meters = $kilometers * 1000;
		return compact('miles','feet','yards','kilometers','meters');
	}

	public function CalculateDate($lat1, $lon1, $lat2, $lon2, $speed, $current)
    {
		$distance = $this->getDistanceBetweenPoints($lat1, $lon1, $lat2, $lon2);

        $km = $distance['kilometers'] / $speed;

		$date = date('Y-m-d H:i:s', strtotime($current)+ (60 * 60 * $km));

        return $date;
	}

	public function CalculateDateMin($lat1, $lon1, $lat2, $lon2, $speed, $current)
    {//meidi
		$distance = $this->getDistanceBetweenPoints($lat1, $lon1, $lat2, $lon2);

        $km = $distance['kilometers'] / $speed;

		$date = date('Y-m-d H:i:s', strtotime($current)- (60 * 60 * $km));

        return $date;
    }

	public function getUsersFromKogas(Request $request)
	{
		// return DB::table('master_users')->select('id')->where('bagian',$request->kogas)->get();
		return DB::table('master_users')->where('bagian',$request->kogas)->get();
	}

	public function GetIdKogasFromKogas(Request $request)
	{
		return DB::table('bagian')->where('nama_bagian', $request->kogas)->first()->ID;
	}

	// public function getCBTerbaik(Request $r){
	// 	$skenario = $this->getSkenario()->ID;
	// 	$best_cb = [];

	// 	$response = [];

	// 	// cb sendiri
	// 	$tmp = DB::table("master_cb_terbaik as b")
	// 		->join("master_cb as cb", "cb.ID", "=", "b.cb")
	// 		->select("b.*","cb.judul_cb","cb.create_by","cb.ID")
	// 		->where("b.skenario",$skenario)
	// 		->get();

	// 	foreach($tmp as $t){
	// 		$best_cb[] = [
	// 			$t->judul_cb,
	// 			$t->skenario,
	// 			$t->kogas
	// 		];

	// 		$object = new Request();
	// 		$object->merge(
	// 			[
	// 				'nama' => $t->judul_cb,
	// 				'skenario' => $t->skenario,
	// 				'kogas' => $t->kogas,
	// 				"id_cb_eppkm" => $t->ID,
	// 			]
	// 		);

	// 		$res = $this->loadOrInsertCB($object)->original[0];

	// 		$kogas = new Request();
	// 		$kogas->merge(
	// 			[
	// 				'kogas' => $t->kogas
	// 			]
	// 		);

	// 		$id_user = new Request();
	// 		$id_user->merge(
	// 			[
	// 				'id_user' => $t->create_by
	// 			]
	// 		);


	// 		if($this->isAsops($id_user))
	// 		{
	// 			$users = $this->getUsersFromKogas($kogas);

	// 			foreach ($users as $user) {
	// 				if(DB::table('master_cb_staf')->where([
	// 					'create_by' => $user->id,
	// 					'judul_cb' => $t->judul_cb,
	// 					'id_skenario' => $t->skenario
	// 				])->exists())
	// 				{
	// 					$response[] = [
	// 						"id_cb" => $res->id,
	// 						"id_cb_eppkm" => $t->ID,
	// 						"id_kogas" => $res->kogas,
	// 						"id_user" => $user->id,
	// 						"nama_kogas" => $this->getKogas($kogas)->original[0]->nama_bagian,
	// 						"tipe_cb" => "CB Sendiri",
	// 						"nama_document" => $t->judul_cb,
	// 						"hari" => $res->hari_h
	// 					];
	// 				}
	// 			}
	// 		}else{
	// 			$response[] = [
	// 				"id_cb" => $res->id,
	// 				"id_cb_eppkm" => $t->ID,
	// 				"id_kogas" => $res->kogas,
	// 				"id_user" => $t->create_by,
	// 				"nama_kogas" => $this->getKogas($kogas)->original[0]->nama_bagian,
	// 				"tipe_cb" => "CB Sendiri",
	// 				"nama_document" => $t->judul_cb,
	// 				"hari" => $res->hari_h
	// 			];
	// 		}
	// 	}

	// 	// cb musuh
	// 	$tmp = DB::table("master_cb_musuh_terbaik as b")
	// 		->join("master_cb as cb", "cb.ID", "=", "b.cb")
	// 		->select("b.*","cb.judul_cb","cb.create_by","cb.ID")
	// 		->where("b.skenario",$skenario)
	// 		->get();
	// 	foreach($tmp as $t){
	// 		$best_cb[] = [
	// 			$t->judul_cb,
	// 			$t->skenario,
	// 			$t->kogas
	// 		];

	// 		$object = new Request();
	// 		$object->merge(
	// 			[
	// 				'nama' => $t->judul_cb,
	// 				'skenario' => $t->skenario,
	// 				'kogas' => $t->kogas,
	// 				"id_cb_eppkm" => $t->ID,
	// 			]
	// 		);

	// 		$res = $this->loadOrInsertCB($object)->original[0];

	// 		$kogas = new Request();
	// 		$kogas->merge(
	// 			[
	// 				'kogas' => $t->kogas
	// 			]
	// 		);

	// 		$response[] = [
	// 			"id_cb" => $res->id,
	// 			"id_cb_eppkm" => $t->ID,
	// 			"id_kogas" => $res->kogas,
	// 			"id_user" => $t->create_by,
	// 			"nama_kogas" => $this->getKogas($kogas)->original[0]->nama_bagian,
	// 			"tipe_cb" => "CB Musuh",
	// 			"nama_document" => $t->judul_cb,
	// 			"hari" => $res->hari_h
	// 		];
	// 	}

	// 	// return response()->json($best_cb);

	// 	return response()->json($response);
	// 	// for($i=0; $i<count($best_cb); $i++){
	// 		// $tmp = DB::table("TFG.cb as tcb")
	// 			// ->join("master_cb as cb", function($join){
	// 				// $join->on("cb.jenis_cb","=","tcb.nama");
	// 				// $join->on("cb.id_skenario","=","tcb.skenario");
	// 			// })
	// 			// ->join("master_useers as u", function($join){
	// 				// $join->on("cb.create_by","=","u.id");
	// 				// $join->on("tcb.kogas","=","u.bagian");
	// 			// })
	// 			// ->where("tcb.nama", $best_cb[$i]["nama"])
	// 			// ->where("tcb.skenario", $best_cb[$i]["skenario"])
	// 			// ->where("tcb.kogas", $best_cb[$i]["kogas"])
	// 	// }
	// }

	public function getCBTerbaikRev(){
		$master_skenario = $this->getSkenario();
		$response = [];
		$data_users_panglima = DB::table('unit_pelatihan')
			->join('master_users', 'master_users.bagian', '=', 'unit_pelatihan.divisi')
			->where('unit_pelatihan.id_skenario', $master_skenario->ID)
			->where('master_users.jabatan', 1)->get();
		$index = 0;
		foreach ($data_users_panglima as $key => $value) {
			$cb_terbaik = $this->get_cb_terbaik_per_kogas($value->bagian);
			if(!isset($cb_terbaik['error'])){
				foreach ($cb_terbaik as $key => $value) {
					$isDuplicate = false;
					foreach ($response as $key_ => $value_) {
						if($value['id_user'] == $value_['id_user'] && $value['nama_document'] == $value_['nama_document']){
							$isDuplicate = true;
							if($value['id_kogas'] == 0){
								// $value['id_kogas']
							}
						}
					}
					if(!$isDuplicate){
						array_push($response, $value);
					}
				}
			}
		}
		
		return response()->json($response);
	}
	public function my_array_unique($array, $keep_key_assoc = false){
		$duplicate_keys = array();
		$tmp = array();       
	
		foreach ($array as $key => $val){
			// convert objects to arrays, in_array() does not support objects
			if (is_object($val))
				$val = (array)$val;
	
			if (!in_array($val, $tmp))
				$tmp[] = $val;
			else
				$duplicate_keys[] = $key;
		}
	
		foreach ($duplicate_keys as $key)
			unset($array[$key]);
	
		return $keep_key_assoc ? $array : array_values($array);
	}

	public function isAsops(Request $request)
	{
		if(DB::table('master_users')->where('id',$request->id_user)->first()->asisten == 6)
		{
			return true;
		}else{
			return false;
		}
	}

	public function tanggalTerkecil(Request $request)
	{
		return response()->json($request);
	}

	public function get_cb_terbaik_patun($id_kogas) {
		$data_kogas = DB::table('bagian')->select('nama_bagian')->where('ID',$id_kogas)->first();
		//dd($data_kogas);

		$master_skenario = DB::table('master_skenario')->select('ID','harih','pilihan_cb_terbaik')->where('status','1')->first();
		// dd($master_skenario);
		$id = DB::table('TFG.cb')->max('id');
		if($id == null){
			$no = 1;
		}else{
			$no = $id+1;
		}

		$patun = DB::table("master_cb_terbaik AS mct")
					->join("master_users AS mu","mu.id","=","mct.id_user")
					->join("master_cb AS mcb","mcb.ID","=","mct.cb")
					->select("mct.*","mcb.judul_cb")
					->where("mct.skenario",$master_skenario->ID)
					->where("mu.jenis_user",2)
					->where("mu.id",116)
					->first();
		$response[] = [
			// "id_cb" => $res_patun->id,
			"id_cb" => $no,
			"id_cb_eppkm" => $patun->cb,
			"id_kogas" => 0,
			"id_user" => $patun->id_user,
			'nama_kogas' => null,
			"tipe_cb" => "CB Musuh",
			"nama_document" => $patun->judul_cb,
			"hari" => $master_skenario->harih
		];
		return $response;
	}

	public function get_cb_terbaik_per_kogas($id_kogas)
	{
		
		// try {
			//code...
			$data_kogas = DB::table('bagian')->select('nama_bagian')->where('ID',$id_kogas)->first();
			//dd($data_kogas);

			$master_skenario = DB::table('master_skenario')->select('ID','harih','pilihan_cb_terbaik')->where('status','1')->first();
			// dd($master_skenario);
			$metode_db = DB::table("master_metode")
				->join('master_skenario','master_skenario.ID','=','master_metode.id_skenario')
				->where([['master_skenario.status',1],['master_metode.bagian', $id_kogas]])
				->first();
			//dd($metode_db);
			$id = DB::table('TFG.cb')->max('id');
			if($id == null){
				$no = 1;
			}else{
				$no = $id+1;
			}
			// dd($no);
			if($metode_db != null){
				$cb_terbaik_musuh = DB::table("master_cb_musuh_terbaik")
					->join('master_users', 'master_users.id','=','master_cb_musuh_terbaik.id_user')
					->join('master_cb','master_cb.ID','=','master_cb_musuh_terbaik.cb')
					->where('skenario',$master_skenario->ID)
					->where('kogas',$id_kogas)
					->where('metode_analisa_cb', $metode_db->metode)
					->first();
				
				$cb_terbaik_all = DB::table('master_cb_terbaik')
					->join('master_users', 'master_users.id','=','master_cb_terbaik.id_user')
					->join('master_cb','master_cb.ID','=','master_cb_terbaik.cb')
					->where('skenario',$master_skenario->ID)
					->where('kogas',$id_kogas)
					->where('metode_analisa_cb', $metode_db->metode)
					->where('jabatan',1)
					// ->where('id_user', Auth::guard('webmaster')->user()->id)
					->first();
				
				$patun = DB::table("master_cb_terbaik AS mct")
					->join("master_users AS mu","mu.id","=","mct.id_user")
					->join("master_cb AS mcb","mcb.ID","=","mct.cb")
					->select("mct.*","mcb.judul_cb")
					->where("mct.skenario",$master_skenario->ID)
					->where("mu.jenis_user",2)
					->where("mu.id",116)
					->first();
				//dd($patun);
				//dd($patun);
				//dd($cb_terbaik_all);
				//dd($cb_terbaik_musuh);

				if($cb_terbaik_all != null && $cb_terbaik_musuh != null){
					$data_asops = DB::table('master_users')->where('id',$cb_terbaik_all->create_by)->first();
					//dd($data_asops);
					if($data_asops->asisten == 6){
						$cb_terbaik_asops = DB::table('master_cb_terbaik')
							->join('master_users', 'master_users.id','=','master_cb_terbaik.id_user')
							->join('master_cb','master_cb.ID','=','master_cb_terbaik.cb')
							->where('skenario',$master_skenario->ID)
							->where('kogas',$id_kogas)
							->where('metode_analisa_cb', $metode_db->metode)
							->where('jabatan',2)
							->where('judul_cb',$cb_terbaik_all->judul_cb)
							->get();
						
						$response[0] = [
								"id_cb" => $no++,
								"id_cb_eppkm" => $cb_terbaik_all->cb, //dari master_cb
								"id_kogas" => $cb_terbaik_all->kogas, 
								"id_user" => $cb_terbaik_all->create_by,
								'nama_kogas' => $data_kogas->nama_bagian,
								"tipe_cb" => "CB Sendiri",
								"nama_document" => $cb_terbaik_all->judul_cb,
								"hari" => $master_skenario->harih
							];
						
						for ($i=0; $i < count($cb_terbaik_asops); $i++) { 
							
							$response[$i+1] = [
								// "id_cb" => $res_asops->id,
								"id_cb" => $no++,
								"id_cb_eppkm" => $cb_terbaik_asops[$i]->cb,
								"id_kogas" => $cb_terbaik_asops[$i]->kogas,
								"id_user" => $cb_terbaik_asops[$i]->id_user,
								'nama_kogas' => $data_kogas->nama_bagian,
								"tipe_cb" => "CB Sendiri",
								"nama_document" => $cb_terbaik_asops[$i]->judul_cb,
								"hari" => $master_skenario->harih
							];
						}
						if($master_skenario->pilihan_cb_terbaik == true && $patun != null){
						
							$response[] = [
								// "id_cb" => $res_patun->id,
								"id_cb" =>$no++,
								"id_cb_eppkm" => $patun->cb,
								"id_kogas" => 0,
								"id_user" => $patun->id_user,
								'nama_kogas' => $data_kogas->nama_bagian,
								"tipe_cb" => "CB Musuh",
								"nama_document" => $patun->judul_cb,
								"hari" => $master_skenario->harih
							];
						}else{
						

							$response[] = [
								// "id_cb" => $res_musuh->id,
								"id_cb" => $no++,
								"id_cb_eppkm" => $cb_terbaik_musuh->cb,
								"id_kogas" => $cb_terbaik_musuh->kogas,
								"id_user" => $cb_terbaik_musuh->id_user,
								'nama_kogas' => $data_kogas->nama_bagian,
								"tipe_cb" => "CB Musuh",
								"nama_document" => $cb_terbaik_musuh->judul_cb,
								"hari" => $master_skenario->harih
							];
						}
						
					}else{
						
						
						$response[0] = [
							"id_cb" => $no++,
							"id_cb_eppkm" => $cb_terbaik_all->cb,
							"id_kogas" => $cb_terbaik_all->kogas,
							"id_user" => $cb_terbaik_all->id_user,
							'nama_kogas' => $data_kogas->nama_bagian,
							"tipe_cb" => "CB Sendiri",
							"nama_document" => $cb_terbaik_all->judul_cb,
							"hari" => $master_skenario->harih
						];
						
						if($master_skenario->pilihan_cb_terbaik == true && $patun != null){
							// $object_musuh = new Request();
							
							
							$response[1] = [
								"id_cb" => $no++,
								"id_cb_eppkm" => $patun->cb,
								"id_kogas" => 0,
								"id_user" => $patun->id_user,
								'nama_kogas' => $data_kogas->nama_bagian,
								"tipe_cb" => "CB Musuh",
								"nama_document" => $patun->judul_cb,
								"hari" => $master_skenario->harih
							];
						}else{
							// $object_musuh = new Request();
						
							$response[1] = [
								"id_cb" => $no++,
								"id_cb_eppkm" => $cb_terbaik_musuh->cb,
								"id_kogas" => $cb_terbaik_musuh->kogas,
								"id_user" => $cb_terbaik_musuh->id_user,
								'nama_kogas' => $data_kogas->nama_bagian,
								"tipe_cb" => "CB Musuh",
								"nama_document" => $cb_terbaik_musuh->judul_cb,
								"hari" => $master_skenario->harih
							];
						}
					}
						// dd($data_asops);

					// for ($i=0; $i < count($response); $i++) { 
					// 	# code...
					// 	// dd($response[$i]);
					// 	$object = new Request();
					// 	$object->merge([
					// 		'id_cb_tfg' => $response[$i]['id_cb'],
					// 		'nama' => $response[$i]['nama_document'],
					// 		'skenario' => $master_skenario->ID,
					// 		'kogas' => $response[$i]['id_kogas'],
					// 		"id_cb_eppkm" => $response[$i]['id_cb_eppkm'],
					// 		"id_user" => $response[$i]['id_user'],
					// 	]);

					// 	$this->loadOrInsertCB($object)->original[0];
					// }
					
					return $response;
				}else{
					return ['error' => 'cb terbaik belum lengkap'];
				}
			}else{
				return ['error' => 'Panglima belum memilih metode analisa'];
			}
		// } catch (Exception $th) {
		// 	//throw $th;
		// 	return ['error' => 'Server Error!, saat get_cb_terbaik_per_kogas.'];
		// }
	}

	public function getCB_Terbaik_all(Request $request){
		
		
		// $get_cb = $this->get_cb_terbaik_per_kogas($request->ID_KOGAS);
		if($request->bagian != 0) {
			$get_cb = $this->get_cb_terbaik_per_kogas($request->bagian);
		} else {
			$get_cb = $this->get_cb_terbaik_patun($request->bagian);
		}
		
		return response()->json($get_cb);
	}
	
	public function getCB_Terbaik(Request $request){
		// dd($request->kogas);
		$res = [];
		$filter = [
			"cb" => [
				"sendiri" => [],
				"musuh" => []
			],
			"patun" => []
		];
		$data_kogas = DB::table('bagian')->whereIn('ID',$request->kogas)->get();
		$master_skenario = DB::table('master_skenario')->where('status','1')->first();
		// dd($data_kogas);
		foreach($data_kogas AS $d){
			$response = [
				"sendiri" => [],
				"musuh" => [],
				"division" => []
			];
			$metode_db = DB::table("master_metode")
				->join('master_skenario','master_skenario.ID','=','master_metode.id_skenario')
				->where([['master_skenario.status',1],['master_metode.bagian',$d->ID]])
				->first();
			if($metode_db != null){
				$cb_terbaik_musuh = DB::table("master_cb_musuh_terbaik")
					->join('master_users', 'master_users.id','=','master_cb_musuh_terbaik.id_user')
					->join('master_cb','master_cb.ID','=','master_cb_musuh_terbaik.cb')
					->where('skenario',$master_skenario->ID)
					->where('kogas',$d->ID)
					->where('metode_analisa_cb', $metode_db->metode)
					->first();
				$cb_terbaik_all = DB::table('master_cb_terbaik')
					->join('master_users', 'master_users.id','=','master_cb_terbaik.id_user')
					->join('master_cb','master_cb.ID','=','master_cb_terbaik.cb')
					->where('skenario',$master_skenario->ID)
					->where('kogas',$d->ID)
					->where('metode_analisa_cb', $metode_db->metode)
					->where('jabatan',1)
					->first();
		
				if($cb_terbaik_all != null){
					$data_asops = DB::table('master_users')->where('id',$cb_terbaik_all->create_by)->first();
					if($data_asops->asisten == 6){
						$cb_terbaik_asops = DB::table('master_cb_terbaik')
							->join('master_users', 'master_users.id','=','master_cb_terbaik.id_user')
							->join('master_cb','master_cb.ID','=','master_cb_terbaik.cb')
							->where('skenario',$master_skenario->ID)
							->where('kogas',$d->ID)
							->where('metode_analisa_cb', $metode_db->metode)
							->get();
						for($i=0; $i < count($cb_terbaik_asops); $i++){ 
							$response["division"][] = $cb_terbaik_asops[$i]->id_user;
							if($cb_terbaik_asops[$i]->asisten!=6){ continue; }
							if(in_array($cb_terbaik_asops[$i]->cb,$filter["cb"]["sendiri"])){ continue; }
							$filter["cb"]["sendiri"][] = $cb_terbaik_asops[$i]->cb;
							$response["sendiri"][] = [
								"id_cb" => $cb_terbaik_asops[$i]->id,
								"id_cb_eppkm" => $cb_terbaik_asops[$i]->cb,
								"id_kogas" => $cb_terbaik_asops[$i]->kogas,
								"id_user" => $cb_terbaik_asops[$i]->create_by,
								'nama_kogas' => $d->nama_bagian,
								"tipe_cb" => "CB Sendiri",
								"nama_document" => $cb_terbaik_asops[$i]->judul_cb,
								"hari" => $master_skenario->harih
							];
						}
					}else{						
						if(!in_array($cb_terbaik_all->cb,$filter["cb"]["sendiri"])){
							$response["division"][] = $cb_terbaik_all->id_user;
							$filter["cb"]["sendiri"][] = $cb_terbaik_all->cb;
							$response["sendiri"][] = [
								"id_cb" => $cb_terbaik_all->id,
								"id_cb_eppkm" => $cb_terbaik_all->cb,
								"id_kogas" => $cb_terbaik_all->kogas,
								"id_user" => $cb_terbaik_all->create_by,
								'nama_kogas' => $d->nama_bagian,
								"tipe_cb" => "CB Sendiri",
								"nama_document" => $cb_terbaik_all->judul_cb,
								"hari" => $master_skenario->harih
							];
						}
					}
				}
				if($cb_terbaik_musuh != null && !in_array($cb_terbaik_musuh->cb,$filter["cb"]["musuh"])){
					$filter["cb"]["musuh"][] = $cb_terbaik_musuh->cb;
					$response["musuh"][] = [
						"id_cb" => $cb_terbaik_musuh->id,
						"id_cb_eppkm" => $cb_terbaik_musuh->cb,
						"id_kogas" => $cb_terbaik_musuh->kogas,
						"id_user" => $cb_terbaik_musuh->create_by,
						'nama_kogas' => $d->nama_bagian,
						"tipe_cb" => "CB Musuh",
						"nama_document" => $cb_terbaik_musuh->judul_cb,
						"hari" => $master_skenario->harih
					];
				}
			}
			$res[$d->ID] = $response;
		}
		$patun = DB::table("master_cb_terbaik AS mct")
			->join("master_users AS mu","mu.id","=","mct.id_user")
			->join("master_cb AS mcb","mcb.ID","=","mct.cb")
			->select(
				"mct.*",
				"mcb.judul_cb AS nama_document",
				"mct.cb AS id_cb_eppkm"
			)
			->where("mct.skenario",$master_skenario->ID)
			->where("mu.id",116)
			->first();
		return response()->json([
			"musuh" => $master_skenario->pilihan_cb_terbaik,
			"cb" => $res,
			"patun" => $patun
		]);
	}
	public function get_misi_all(){
		$skenario_aktif = DB::table('master_skenario')->where('status',1)->first();
		$data_misi_all = DB::table('p_data_misi')->join('p_dokumen', 'p_data_misi.dokumen','=','p_dokumen.id')
						->where('p_dokumen.scenario',$skenario_aktif->ID)->get();

		$data_kegiatan_all = DB::table('p_master_kegiatan')->where('id_scenario',$skenario_aktif->ID)->get();
		$data_misi = [];
		for ($i=0; $i < count($data_misi_all); $i++) {
			$data_per_misi[$i] = json_decode($data_misi_all[$i]->properties);
			$data_per_kegiatan[$i] = DB::table('p_master_kegiatan')->where('id_kegiatan',$data_per_misi[$i]->id_kegiatan)->get();
			//	 dd($data_per_misi[$i]);

			array_push($data_misi, [
				'id_object' => $data_misi_all[$i]->id_object,
				'nama_misi' => $data_per_misi[$i]->nama_misi,
				'data_kegiatan' => $data_per_kegiatan[$i],
				'data_per_misi' => $data_misi_all[$i]
			]);
		}
		return response()->json($data_misi);
	}

}
