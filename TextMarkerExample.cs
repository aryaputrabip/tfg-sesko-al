using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextMarkerExample : MonoBehaviour
{
    [SerializeField] bool isCreate;
    [SerializeField] GameObject prefab;
    [SerializeField] float zoomNow;
    [SerializeField] float zoomPrev;

    public int origin = 5;

    private void Start()
    {
        var ocz1 = zoomNow - 7;

        var marker = OnlineMapsMarker3DManager.instance.Create(106.74316f, -5.98761f, prefab);
        marker.scale = 12f * Mathf.Pow(2, ocz1);

        var tS = marker.instance.AddComponent<TextStatExample>();
        tS.lng = marker.position.x;
        tS.lat = marker.position.y;

        var ocz2 = zoomNow - 8;

        var marker2 = OnlineMapsMarker3DManager.instance.Create(106.65527f, -6.59913f, prefab);
        marker2.scale = 12f * Mathf.Pow(2, ocz2);

        var tS2 = marker2.instance.AddComponent<TextStatExample>();
        tS2.lng = marker2.position.x;
        tS2.lat = marker2.position.y;


        //var marker3 = OnlineMapsMarker3DManager.instance.Create(108.23730f, 06.59913f, prefab);
        //marker3.scale = 8f;

        //var tS3 = marker3.instance.AddComponent<TextStatExample>();
        //tS3.originZoom = OnlineMaps.instance.zoom;
        //tS3.lng = marker3.position.x;
        //tS3.lat = marker3.position.y;

        zoomNow = OnlineMaps.instance.zoom;
        zoomPrev = OnlineMaps.instance.zoom;

        OnlineMapsControlBase.instance.OnMapClick += CreateMarker;
        OnlineMaps.instance.OnChangeZoom += OnChangeZoom;
    }

    // Update is called once per frame
    void Update()
    {
        zoomNow = OnlineMaps.instance.zoom;

    }

    private void CreateMarker()
    {
        double lng, lat;

        if(isCreate)
        if(OnlineMapsControlBase.instance.GetCoords(out lng, out lat))
        {
            var marker = OnlineMapsMarker3DManager.instance.Create(lng, lat, prefab);
            var tS = marker.instance.AddComponent<TextStatExample>();
            tS.originZoom = OnlineMaps.instance.zoom;
            tS.lng = marker.position.x;
            tS.lat = marker.position.y;

            //isCreate = false;
            //OnlineMapsControlBase.instance.OnMapClick -= CreateMarker;
        }
    }

    private void OnChangeZoom()
    {
        foreach(OnlineMapsMarker3D item in OnlineMapsMarker3DManager.instance)
        {
            var tS = item.instance.GetComponent<TextStatExample>();

            if(tS != null)
            {
                var onChangeZoom = zoomNow - zoomPrev;

                if(zoomNow > zoomPrev)
                {
                    item.scale = item.scale * Mathf.Pow(2, onChangeZoom);
                }
                else if (zoomNow < zoomPrev)
                {
                    item.scale = item.scale * Mathf.Pow(2, onChangeZoom);
                }
            }
        }

        if (zoomPrev != zoomNow)
        {
            Debug.Log(zoomPrev);
            zoomPrev = zoomNow;
        }
    }
}
