using System.Collections.Generic;
using HelperPlotting;
using Newtonsoft.Json;
using UnityEngine;

public class TrashObj : MonoBehaviour
{
    public static TrashObj instance;
    public List<Trash> trash = new();
    [JsonIgnore] [TextArea(15,20)] public string json;

    void Awake()
    {
        if(instance == null)
        instance = this;
        else
        Destroy(gameObject);
    }

    void Update()
    {
        json = Trash.ToString(trash);
    }
}
