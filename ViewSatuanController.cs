using System.Collections;
using System.Collections.Generic;
using HelperPlotting;
using ObjectStat;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using CoordinateSharp;
using System;
using Unity.VisualScripting;

namespace WGS.View
{
    public class ViewSatuanController : MonoBehaviour
    {
        public static ViewSatuanController instance;
        
        [Header ("Prefab")]
        public GameObject prefabListRadar;
        
        [Header ("Markers")]
        public Markers markers = new();
        
        [Header ("Canvas")]
        public GameObject detailSatuanCanvas;
        Component FindPara<Component>(string src) => detailSatuanCanvas.GetComponent<Metadata>().FindParameter(src).parameter.GetComponent<Component>();
        public GameObject detailSatuanRadarCanvas;
        Component FindParaRadarCanva<Component>(string src) => detailSatuanRadarCanvas.GetComponent<Metadata>().FindParameter(src).parameter.GetComponent<Component>();
        public Transform ContentRadar;

        [Header ("List")]
        private List<GameObject> instantiatedRadar = new();
        private EagerLoad el = new(false);

        void Awake()
        {
            if(instance == null)instance = this;
            else Destroy(gameObject);
        }

        void Update()
        {
            if(markers.marker3D.instance != null)
            {
                var stat   = markers.marker3D.instance.GetComponent<StatV2>();
                var marker = stat.markers.marker3D;
                
                Coordinate coords = new(marker.position.y, marker.position.x, DateTime.Now, el);
                coords.LoadUTM_MGRS_Info();

                FindPara<TextMeshProUGUI>("latitude").text  = coords.Latitude.ToString();
                FindPara<TextMeshProUGUI>("longitude").text = coords.Longitude.ToString();
                FindPara<TextMeshProUGUI>("mgrs").text      = coords.MGRS.ToString();
                FindPara<TextMeshProUGUI>("utm").text       = coords.UTM.ToString();
                FindParaRadarCanva<TextMeshProUGUI>("latitude").text  = coords.Latitude.ToString();
                FindParaRadarCanva<TextMeshProUGUI>("longitude").text = coords.Longitude.ToString();
                FindParaRadarCanva<TextMeshProUGUI>("mgrs").text      = coords.MGRS.ToString();
                FindParaRadarCanva<TextMeshProUGUI>("utm").text       = coords.UTM.ToString();
            }
        }

        public void OnMarkerClick(OnlineMapsMarkerBase markerBase)
        {
            markers.marker3D  = markerBase as OnlineMapsMarker3D;
            var stat          = markers.marker3D.instance.GetComponent<StatV2>();
            detailSatuanCanvas.GetComponent<Animator>().Play("FadeIn");
            
            FindPara<Image>("icon").sprite                = null;
            FindPara<TextMeshProUGUI>("nama_satuan").text = stat.pasukan.info.nama_satuan;
            FindPara<TextMeshProUGUI>("id_satuan").text   = stat.pasukan.nama;
            FindPara<Text>("font_taktis").text            = ((char)int.Parse(stat.pasukan.style.index)).ToString();
            FindPara<Text>("font_taktis").font            = Resources.Load<Font>("Fonts/Simbol Taktis/" + stat.pasukan.style.nama);
            FindPara<TextMeshProUGUI>("tipe_tni").text    = stat.pasukan.matra;
            FindPara<TextMeshProUGUI>("tipe").text        = stat.pasukan.tipe_tni;
            FindPara<TextMeshProUGUI>("jenis").text       = "null";
            FindPara<TextMeshProUGUI>("warna").text       = stat.isEnemy ? "Merah" : "Biru";
            FindPara<TextMeshProUGUI>("ukuran").text      = "meters";
        }

        public void ToggleRadarList(bool toggle)
        {
            if(toggle)
            {
                var stat = markers.marker3D.instance.GetComponent<StatV2>();
                if(stat.radars != null)
                for(int i = 0; i < stat.radars.Count; i++)
                {
                    var radar   = stat.radars[i];
                    var radar3D = stat.radars3D.Find(child => child.name == "radar3D_" + stat.name + "_" + radar.RADAR_NAME);
                    var detail  = radar3D.GetComponent<DetailRadar>();

                    var copy   = Instantiate(prefabListRadar, ContentRadar);
                    Component FindParaRadar<Component>(string src) => copy.GetComponent<Metadata>().FindParameter(src).parameter.GetComponent<Component>();
                    var nomor  = FindParaRadar<TextMeshProUGUI>("Nomor");
                    var nama   = FindParaRadar<TextMeshProUGUI>("Nama");
                    var show   = FindParaRadar<Toggle>("Toggle");
                    var _2D    = FindParaRadar<Toggle>("2D");
                    var _3D    = FindParaRadar<Toggle>("3D");
                    var view   = FindParaRadar<Button>("View");

                    nomor.text = (i + 1).ToString();
                    nama.text  = radar.RADAR_NAME;
                    show.isOn  = radar.isOn;
                    show.onValueChanged.AddListener(delegate{ToggleRadar(radar, detail, _2D, _3D);});
                    _2D.isOn   = detail.showRadar2D;
                    _2D.onValueChanged.AddListener(delegate{Toggle2D(detail, _3D);});
                    _3D.isOn   = detail.showRadar3D;
                    _3D.onValueChanged.AddListener(delegate{Toggle3D(detail, _2D);});
                    
                    var sM = view.GetComponent<SidemenuToggler>();
                    sM.onToggleOn.AddListener(delegate{ViewRadar(stat, radar, detail, true);});
                    sM.onToggleOff.AddListener(delegate{ViewRadar(stat, radar, detail, false);});
                    FindParaRadarCanva<Button>("exit_button").onClick.AddListener(delegate{sM.toggleOff();});

                    instantiatedRadar.Add(copy);
                }
            }
            else
            {
                foreach(var child in instantiatedRadar)
                Destroy(child);
                instantiatedRadar.Clear();
            }
        }

        private void ViewRadar(StatV2 stat, Radar radar, DetailRadar detail, bool toggle)
        {
            if(toggle) detailSatuanRadarCanvas.GetComponent<Animator>().Play("FadeIn");
            else detailSatuanRadarCanvas.GetComponent<Animator>().Play("FadeOut");
            
            FindParaRadarCanva<Image>("icon").sprite                 = null;
            FindParaRadarCanva<TextMeshProUGUI>("nama_satuan").text  = toggle ? radar.RADAR_NAME : string.Empty;
            FindParaRadarCanva<TextMeshProUGUI>("id_satuan").text    = toggle ? detail.name : string.Empty;
            FindParaRadarCanva<Text>("font_taktis").text             = toggle ? ((char)163).ToString() : string.Empty;
            FindParaRadarCanva<Text>("font_taktis").font             = Resources.Load<Font>("Fonts/Simbol Taktis/TAKTIS_GABUNGAN");
            FindParaRadarCanva<TextMeshProUGUI>("nama_pemilik").text = toggle ? stat.pasukan.info.nama_satuan : string.Empty;
            FindParaRadarCanva<TextMeshProUGUI>("tipe_tni").text     = toggle ? stat.pasukan.matra : string.Empty;
            FindParaRadarCanva<TextMeshProUGUI>("tipe").text         = toggle ? stat.pasukan.tipe_tni : string.Empty;
            FindParaRadarCanva<TextMeshProUGUI>("jenis").text        = toggle ? "null" : string.Empty;
            FindParaRadarCanva<TextMeshProUGUI>("warna").text        = toggle ? (stat.isEnemy ? "Merah" : "Biru") : string.Empty;
            FindParaRadarCanva<TextMeshProUGUI>("radius").text       = toggle ? ((int)(radar.RADAR_DET_RANGE / 2)).ToString() + "|Km" : string.Empty;
        }
        
        private void ToggleRadar(Radar radar, DetailRadar detail, Toggle toggle2D, Toggle toggle3D)
        {
            detail.showRadar = !detail.showRadar;
            radar.isOn = detail.showRadar;
            toggle2D.interactable = detail.showRadar;
            toggle3D.interactable = detail.showRadar;
        }

        private void Toggle2D(DetailRadar detail, Toggle toggle3D)
        {
            detail.showRadar2D = !detail.showRadar2D;
            toggle3D.isOn = !detail.showRadar2D;
        }
        
        private void Toggle3D(DetailRadar detail, Toggle toggle2D)
        {
            detail.showRadar3D = !detail.showRadar3D;
            toggle2D.isOn = !detail.showRadar3D;
        }
    }
}
