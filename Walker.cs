using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Walker
{
    public static class Path
    {   
        ///<summary>
        ///Smoothing a path consisting of Vector3
        ///</summary>
        public static List<Vector3> SmoothPath(List<Vector3> path)
        {
            var output = new List<Vector3>();

            if (path.Count > 0)
            {
                output.Add(path[0]);
            }

            for (var i = 0; i < (path.Count - 1); i++)
            {
                var p0 = path[i];
                var p1 = path[i + 1];
                var p0x = p0.x;
                var p0y = p0.y;
                var p1x = p1.x;
                var p1y = p1.y;

                var qx = 0.75f * p0x + 0.25f * p1x;
                var qy = 0.75f * p0y + 0.25f * p1y;
                var Q = new Vector3(qx, qy, 0);

                var rx = 0.25f * p0x + 0.75f * p1x;
                var ry = 0.25f * p0y + 0.75f * p1y;
                var R = new Vector3(rx, ry, 0);

                output.Add(Q);
                output.Add(R);
            }

            if (path.Count > 1)
            {
                output.Add(path[path.Count - 1]);
            }

            return output;
        }
        
        ///<summary>
        ///Smoothing a path consisting of Vector3
        ///</summary>
        public static List<Vector2> SmoothPath(List<Vector2> path)
        {
            var output = new List<Vector2>();

            if (path.Count > 0)
            {
                output.Add(path[0]);
            }

            for (var i = 0; i < (path.Count - 1); i++)
            {
                var p0 = path[i];
                var p1 = path[i + 1];
                var p0x = p0.x;
                var p0y = p0.y;
                var p1x = p1.x;
                var p1y = p1.y;

                var qx = 0.75f * p0x + 0.25f * p1x;
                var qy = 0.75f * p0y + 0.25f * p1y;
                var Q = new Vector2(qx, qy);

                var rx = 0.25f * p0x + 0.75f * p1x;
                var ry = 0.25f * p0y + 0.75f * p1y;
                var R = new Vector2(rx, ry);

                output.Add(Q);
                output.Add(R);
            }

            if (path.Count > 1)
            {
                output.Add(path[path.Count - 1]);
            }

            return output;
        }

        ///<summary>
        ///Get the Distance between an array of points
        ///</summary>
        public static double GetDistance(Vector2[] points, double distance = 0)
        {
            for(int i = 0; i < points.Length - 1; i++)
            distance += OnlineMapsUtils.DistanceBetweenPointsD(points[i], points[i + 1]);
            
            return distance;
        }

        ///<summary>
        ///Get the Distance between a List of points
        ///</summary>
        public static double GetDistance(List<Vector2> points, double distance = 0)
        {
            for(int i = 0; i < points.Count - 1; i++)
            distance += OnlineMapsUtils.DistanceBetweenPointsD(points[i], points[i + 1]);
            
            return distance;
        }

        ///<summary>
        ///Get the Distance between an array of points
        ///</summary>
        ///<param name="points">x => Longitude, y => Latitude</param>
        public static double GetDistance(Vector3[] points, double distance = 0)
        {
            var pointsVec2 = new Vector2[points.Length];
            for(int i = 0; i < pointsVec2.Length; i++)
            pointsVec2[i] = new Vector2(points[i].x, points[i].y);
            
            for(int i = 0; i < pointsVec2.Length - 1; i++)
            distance += OnlineMapsUtils.DistanceBetweenPointsD(points[i], points[i + 1]);
            
            return distance;
        }

        ///<summary>
        ///Get the Distance between a List of points
        ///</summary>
        ///<param name="points">x => Longitude, y => Latitude</param>
        public static double GetDistance(List<Vector3> points, double distance = 0)
        {
            var pointsVec2 = new Vector2[points.Count];
            for(int i = 0; i < pointsVec2.Length; i++)
            pointsVec2[i] = new Vector2(points[i].x, points[i].y);
            
            for(int i = 0; i < pointsVec2.Length - 1; i++)
            distance += OnlineMapsUtils.DistanceBetweenPointsD(points[i], points[i + 1]);
            
            return distance;
        }
    }
}
