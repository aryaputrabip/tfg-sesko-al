using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomLimit : MonoBehaviour
{
    public float minZoom;
    public float maxZoom;

    // Start is called before the first frame update
    void Start()
    {
        OnlineMaps.instance.zoomRange = new OnlineMapsRange(minZoom, maxZoom);
    }
}
